package com.apalacios.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Representa al servidor de juego, se encarga intercambiar datos con los jugadores
 */
class Servidor {
    private int[][] tablero;
    private Socket socket;

    /**
     * Constructor de la clase, inicializa el tablero de juego
     */
    Servidor(){
        tablero = new int[7][6];
    }

    /**
     * Inicia el servidor y crea los canales de comunicaciones. Espera y acepta las conexiones de dos clientes
     */
    void iniciarServidor(){
        ServerSocket socketServidor;
        System.out.println("[INFO] Iniciando servidor...");
        try {
            socketServidor = new ServerSocket(10578);
            System.out.println("[ OK ] Servidor iniciado!");

            //Aceptar la petición del primer cliente
            socket = socketServidor.accept();
            System.out.println("[ OK ] Conexión establecida con jugador 1");
            Cliente jugador1 = new Cliente(socket);

            //Aceptar la petición del segundo cliente
            socket = socketServidor.accept();
            System.out.println("[ OK ] Conexión establecida con jugador 2");
            Cliente jugador2 = new Cliente(socket);

            iniciarPartida(jugador1, jugador2);

        } catch (IOException ex) {
            System.out.println("[FAIL] Error en el servidor");
        }
    }

    /**
     * Bucle principal de ejecución, gestiona el trascurso de la partida
     *
     * @param jugador1 Representa al jugador 1 (Color rojo)
     * @param jugador2 Representa al jugador 2 (Color amarillo)
     */
    private void iniciarPartida(Cliente jugador1, Cliente jugador2){
        inicializarTablero();

        while (!socket.isClosed()){
            jugador1.enviarTablero(tablero);
            jugador2.enviarTablero(tablero);

            jugador1.recibirConfirmacion();
            jugador2.recibirConfirmacion();

            comprobarVictoria(jugador1, jugador2);

            jugador1.tieneTurno(true);
            jugador2.tieneTurno(false);

            jugador2.recibirConfirmacion();
            colocarFicha(jugador1.recibirFicha(), 1);

            jugador1.enviarTablero(tablero);
            jugador2.enviarTablero(tablero);

            jugador1.recibirConfirmacion();
            jugador2.recibirConfirmacion();

            comprobarVictoria(jugador1, jugador2);

            jugador1.tieneTurno(false);
            jugador2.tieneTurno(true);

            jugador1.recibirConfirmacion();
            colocarFicha(jugador2.recibirFicha(), 2);
        }
    }

    /**
     * Inicializa todas las casillas del tablero a 0
     */
    private void inicializarTablero(){
        for (int i = 0; i < 7; i++){
            for (int j = 0; j < 6; j++){
                tablero[i][j] = 0;
            }
        }
    }

    /**
     * Recibe una ficha del cliente y la coloca en la primera casilla disponible (empezando desde abajo) en la
     * columna seleccionada
     *
     * @param columna Representa la columna donde se posicionará la ficha
     * @param tipoFicha Representa el tipo de ficha a colocar (1 = jugador 1 (rojo), 2 = jugador 2 (amarillo))
     */
    private void colocarFicha(int columna, int tipoFicha){
        boolean fichacolocada = false;
        for (int i = 5; i >= 0; i--){
            if(!fichacolocada && tablero[columna][i] == 0){
                tablero[columna][i] = tipoFicha;
                fichacolocada = true;
            }
        }
    }

    /**
     * Determina si ha ganado algun jugador o si ha habido un empate
     *
     * @param jugador1 Representa al jugador 1 (Color rojo)
     * @param jugador2 Representa al jugador 2 (Color amarillo)
     */
    private void comprobarVictoria(Cliente jugador1, Cliente jugador2){
        boolean victoria = false;
        int ganador = 0;

        //Comprobar horizontales
        for (int i = 0; i < 7; i++){
            for (int j = 0; j < 3; j++){
                for (int k = 1; k < 3; k++){
                    if(tablero[i][j] == k && tablero[i][j + 1] == k && tablero[i][j + 2] == k && tablero[i][j + 3] == k){
                        victoria = true;
                        if(k == 1){
                            ganador = 1;
                        }else {
                            ganador = 2;
                        }
                    }
                }
            }
        }

        //Comprobar verticales
        for (int i = 0; i < 6; i++){
            for (int j = 0; j < 4; j++){
                for (int k = 1; k < 3; k++){
                    if (tablero[j][i] == k && tablero[j + 1][i] == k && tablero[j + 2][i] == k && tablero[j + 3][i] == k){
                        victoria = true;
                        if(k == 1){
                            ganador = 1;
                        }else {
                            ganador = 2;
                        }
                    }
                }
            }
        }

        //Comprobar empate
        boolean empate = true;
        for (int i = 0; i < 7; i++){
            for (int j = 0; j < 6; j++){
                if (tablero[i][j] == 0) {
                    empate = false;
                    break;
                }
            }
        }

        if(victoria || empate){
            terminarPartida(ganador, jugador1, jugador2);
        }
    }

    /**
     * Envia un mensaje de victoria, derrota o empate a los jugadores en función de que es lo que ha hecho
     * terminar la partida. Termina las conexiones con los clientes, y detiene el servidor
     *
     * @param ganador Determina la condición de fin de partida (0 = empate, 1 = ganador J1, 2 = ganador J2)
     * @param jugador1 Representa al jugador 1 (Color rojo)
     * @param jugador2 Representa al jugador 2 (Color amarillo)
     */
    private void terminarPartida(int ganador, Cliente jugador1, Cliente jugador2){
        if (ganador == 0){
            jugador1.enviarFinPartida("empate");
            jugador2.enviarFinPartida("empate");

        } else if(ganador == 1){
            jugador1.enviarFinPartida("victoria");
            jugador2.enviarFinPartida("derrota");
        }else{
            jugador1.enviarFinPartida("derrota");
            jugador2.enviarFinPartida("victoria");
        }
        jugador1.desconnectar();
        jugador2.desconnectar();
        try {
            socket.close();
            System.out.println("[INFO] Apagando servidor... 3");
            Thread.sleep(1000);
            System.out.println("[INFO] Apagando servidor... 2");
            Thread.sleep(1000);
            System.out.println("[INFO] Apagando servidor... 1");
            Thread.sleep(1000);
        } catch (IOException | InterruptedException e) {
            System.out.println("[FAIL] Error al cerrar el canal de comunicaciones");
        }
        System.exit(0);
    }
}

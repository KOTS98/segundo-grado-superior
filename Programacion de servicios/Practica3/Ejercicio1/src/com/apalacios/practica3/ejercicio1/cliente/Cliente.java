package com.apalacios.practica3.ejercicio1.cliente;

import java.io.*;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Scanner;

/**
 * Clase que representa al cliente que solicita los datos al servidor
 */
class Cliente {
    private int puerto;
    private String host;
    private Properties conf = new Properties();

    /**
     * Constructor de la clase, llama al método obtenerPuertoYHost
     */
    Cliente(){
        obtenerPuertoYHost();
    }

    /**
     * Lee el archivo de configuración "properties.conf" y obtiene de el los datos necesarios para la conexión con el
     * servidor. En caso de no encontrar el archivo, o de detectar un fallo en su lectura, abortará el arranque del
     * programa
     */
    private void obtenerPuertoYHost(){
        FileReader fr;

        //Lee el archivo y obtiene los valores de puerto y host
        try {
            fr = new FileReader("properties.conf");
            conf.load(fr);
            this.puerto = Integer.parseInt(conf.getProperty("server_port"));
            this.host = conf.getProperty("host");
            fr.close();

        //Aborta el arranque del programa si ocurre algún error
        } catch (FileNotFoundException e) {
            System.out.println("[FAIL] Archivo de configuración no encontrado.\n" +
                    "Reiniciar el servidor generará un nuevo archivo de configuración.");
            System.exit(0);
        } catch (IOException e) {
            System.out.println("[FAIL] Fallo al leer el archivo de configuración.");
            System.exit(0);
        } catch (InputMismatchException e){
            System.out.println("[FAIL] El valor \"server_port\" en el archivo de configuración,\n" +
                    "no es un número. Modifique este valor antes de proceder.");
            System.exit(0);
        }
    }

    /**
     * Inicia la conexión con el servidor utilizando los parámetros de conexión host y puerto y establece los
     * canales de comunicación con el servidor. Despliega un menú por terminal que permite al usuario solicitar una
     * serie de 8 datos al servidor. Una vez que el usuario ha seleccionado el dato, envía la petición correspondiente
     * al servidor, espera su respuesta y la muestra por terminal.
     */
    @SuppressWarnings({"InfiniteLoopStatement", "deprecation"})
    void conectar(){
        try {
            //Establece la conexión con el servidor
            Socket socketCliente = new Socket(host, puerto);

            // Establece los canales de comunicación
            DataInputStream entrada = new DataInputStream(socketCliente.getInputStream());
            PrintStream salida = new PrintStream(socketCliente.getOutputStream());
            String respuestaServer;

            Scanner in = new Scanner(System.in);
            boolean errorMenu;

            //Bucle principal de ejecucción. Permite enviar peticiones y recibir resultados de manera indefinida
            //mientras el servidor esté funcionando
            while (true){
                int opcionMenu = 0;
                do{
                    errorMenu = false;

                    //Menú de opciones
                    System.out.println("-= CONCENTRACIÓN DE CO2 =-\n____________________________\n" +
                            "1. Año con mayor CO2 registrado en Suiza\n" +
                            "2. Año con mayor CO2 registrado\n" +
                            "3. Media de CO2 por milenio en Suiza\n" +
                            "4. Media de CO2 por milenio global\n" +
                            "5. Aumento de CO2 en este siglo  Suiza\n" +
                            "6. Aumento de CO2 en este siglo global\n" +
                            "7. Aumento de CO2 en los últimos 100 años en Suiza\n" +
                            "8. Aumento de CO2 en los últimos 100 años global\n\n" +
                            "¿Que desea comprobar? (1-8):");
                    try{

                        //Recibe la entrada de teclado del usuario
                        opcionMenu = in.nextInt();

                        //Comprueba si es un número entero entre 1 y 8
                        if (opcionMenu < 1 || opcionMenu > 8){
                            errorMenu = true;
                            System.out.println("[FAIL] Solo se admiten valores numéricos de entre 1 y 8.");
                        }

                    //Controla la posibilidad de que el usuario no introduzca un número entero
                    }catch (InputMismatchException e){
                        errorMenu = true;
                        System.out.println("[FAIL] Tipo de dato introducido incorrecto, asegúrese de\n" +
                                "introducir un número.");
                    }
                }while (errorMenu);

                //Determina que opción a seleccionado el usuario, y envía al servidor una petición en consecuencia
                switch (opcionMenu){
                    case 1:
                        salida.println("AMS");
                        break;
                    case 2:
                        salida.println("AMG");
                        break;
                    case 3:
                        salida.println("MMS");
                        break;
                    case 4:
                        salida.println("MMG");
                        break;
                    case 5:
                        salida.println("ISS");
                        break;
                    case 6:
                        salida.println("ISG");
                        break;
                    case 7:
                        salida.println("I100S");
                        break;
                    case 8:
                        salida.println("I100G");
                        break;
                }

                //Espera la respuesta del servidor, la muestra por terminal y espera a que el usuario pulse "ENTER"
                respuestaServer = entrada.readLine();
                System.out.println("Respuesta del servidor:\n" + respuestaServer
                        + "\n\n Pulsa \"ENTER\" para continuar...");
                in.nextLine();
                in.nextLine();

            }

        //Controla un posible fallo de conexión con el servidor
        } catch (IOException e) {
            System.out.println("[FAIL] Ha ocurrido un problema durante la conexión con el servidor." +
                    "\nError detectado en:\n");
            e.printStackTrace();
        }
    }
}

package com.apalacios.practica3.ejercicio1.servidor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * Clase de lanzamiento del servidor
 */
public class Main {

    /**
     * Método principal, ejecuta el método cargarPreferencias, crea e inicia el servidor
     * @param args
     */
    public static void main(String[] args) {
        crearPreferencias();
        Servidor server = Servidor.obtenerInstancia();
        server.iniciarServidor();

    }

    /**
     * Determina si existe o no un archivo de configuración "properties.conf", en caso de no existir, lo crea e
     * inicializa los parámetros de conexión a los valores por defecto (puerto 4444, host localhost)
     */
    private static void crearPreferencias(){
        Properties conf = new Properties();

        //Determina si existe o no el archivo de configuración
        if (!new File("properties.conf").exists()) {
            System.out.println("[INFO] Archivo de configuración no encontrado, creando...");

            //Si no existe, crea un nuevo archivo de configuración con los parámetros por defecto
            try {
                conf.setProperty("server_port", "4444");
                conf.setProperty("host", "localhost");
                FileWriter fw;
                fw = new FileWriter("properties.conf");
                conf.store(fw, "");

            //Controla un posible fallo de lectura / escritura del archivo de configuración
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("[FAIL] Fallo al crear el archivo de configuración.");
            }
        }
    }
}

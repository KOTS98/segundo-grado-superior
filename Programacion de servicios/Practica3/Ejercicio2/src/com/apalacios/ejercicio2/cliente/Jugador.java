package com.apalacios.ejercicio2.cliente;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Clase que representa a un jugador en la partida, despliega una interfaz gráfica y permite intercambiar datos con el
 * servidor de juego
 */
public class Jugador implements ActionListener {
    private JButton[][] matrizBotones;
    private Vista vista;
    private DataInputStream entrada;
    private PrintStream salida;
    private Socket socket;

    /**
     * Constructor de la clase. Establece la conexión con el servidor y crea los canales de comunicación para
     * intercambiar datos con el mismo
     */
    Jugador() {
        vista = new Vista();
        matrizBotones = vista.getMatrizBotones();
        addActionListeners(this);

        socket = null;
        try {
            socket = new Socket("localhost", 10578);
            entrada = new DataInputStream(socket.getInputStream());
            salida = new PrintStream(socket.getOutputStream());
            vista.getTxtResultados().append("[  OK   ] Conexión establecida con el servidor!");
            recibirTablero();

            while (!socket.isClosed()){
                actualizarTurno();
                recibirTablero();
            }
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Recorre la matriz de botones y dota a cada uno de ellos de un ActionListener, que permitirá saber
     * cuándo el botón ha sido pulsado.
     *
     * @param listener Escuchador de eventos que determina cuándo el botón ha sido pulsado.
     */
    private void addActionListeners(ActionListener listener){
        //Recorre la matriz  de izquierda a derecha
        for (int i = 0; i < 3; i++){

            //Recorre la matriz de arriba a abajo
            for (int j = 0; j < 3; j++){

                //Añade un ActionListener al botón
                matrizBotones[i][j].addActionListener(listener);
            }
        }
    }

    /**
     * Obtiene los datos del tablero desde el servidor. Estos datos se reciben como un único String que contiene
     * una secuencia de 9 números de entre 0 y 2. Cada número representa el estado de una casilla en el tablero,
     * donde 0 indica que esta vacía, 1 indica que pertenece al jugador 1, y 2 que pertenece al jugador 2.
     * Cada número en la secuencia, representa a una casilla del tablero, los números leidos de izquierda a derecha,
     * representan la casilla en el tablero leido de izquierda a derecha, y de arriba a abajo, formando una estructura
     * similar a esta:
     *
     * [A][B][C][D][E][F][G][H][I] <-- String recibido.
     *
     * [A][B][C]
     * [D][E][F] <-- Casillas del tablero.
     * [G][H][I]
     */
    private void recibirTablero() throws IOException {
        String strTablero;
        try{
            strTablero = entrada.readUTF();
            if(strTablero.equalsIgnoreCase("desconectar")){
                desconectar();
            }else{
                String[] datosTablero = strTablero.split("");
                int contador = 0;

                //Recorre la matriz de botones de izquierda a derecha
                for (int i = 0; i < 3; i++){

                    //Recorre la matriz de arriba a abajo
                    for (int j = 0; j < 3; j++){

                        //Determina si el contenido del dato recibido para la matriz es una casilla vacía, del jugador 1 o
                        //del jugador 2, en caso de pertenecer a un jugador, reemplaza el contenido del botón correspondiente
                        //por la letra representativa del jugador (X para el jugador 1, O para el jugador 2)
                        if(datosTablero[contador].equalsIgnoreCase("1")){
                            matrizBotones[i][j].setText("X");
                        }else if(datosTablero[contador].equalsIgnoreCase("2")){
                            matrizBotones[i][j].setText("O");
                        }
                        contador++;
                    }
                }
            }
        }catch(IOException ex){
            vista.getTxtResultados().append("\n[FAIL] Error de conexión con el servidor.");
            desconectar();
            ex.printStackTrace();
        }
    }

    /**
     * Recibe desde el servidor un String que determina si es o no el turno del jugador.
     * El String recibido puede ser "comenzar" (Indica que es el turno del jugador), o
     * "esperar" (Indica que NO es el turno del jugador).
     * Tras recibir el String y determinar su contenido, recorre la matriz de botones y
     * los habilita o deshabilita en función de si es o no el turno del jugador.
     */
    private void actualizarTurno() throws IOException {
        switch (entrada.readUTF()){
            //Permite al usuario hacer un movimiento
            case "comenzar":
                habilitarBotones();
                vista.getTxtResultados().append("\n[GAME] Es tu turno!");
                break;

            //Mantiene en espera al usuario
            case "esperar":
                deshabilitarBotones();
                vista.getTxtResultados().append("\n[GAME] Turno del contrincante...");
                break;

            //Finaliza la partida e indica al usuario que es el ganador
            case "victoria":
                deshabilitarBotones();
                vista.getTxtResultados().append("\n[GAME] Enhorabuena! Has ganado!");
                break;

            //Finaliza la partida e indica al usuario que es el perdedor
            case "derrota":
                deshabilitarBotones();
                vista.getTxtResultados().append("\n[GAME] Mala suerte, has perdido...");
                break;

            //Finaliza la partida e indica al usuario que ha habido un empate
            case "empate":
                deshabilitarBotones();
                vista.getTxtResultados().append("\n[GAME] Tenemos un empate!");
                break;

            //Termina la conexión con el servidor
            case "desconectar":
                desconectar();
                break;
        }
    }

    /**
     * Permite a todos los botones ser pulsados, permitiendo que el jugador haga su movimiento
     */
    private void habilitarBotones(){
        //Recorre la matriz de botones de izquierda a derecha
        for (int i = 0; i < 3; i++){

            //Recorre la matriz de botones de arriba a abajo
            for (int j = 0; j < 3; j++){

                //Habilita el botón en la matriz
                matrizBotones[i][j].setEnabled(true);
            }
        }
    }

    /**
     * Evita que los botones puedan ser pulsados, impidiendo que el jugador haga movimientos fuera de su turno
     */
    private void deshabilitarBotones(){
        //Recorre la matriz de botones de izquierda a derecha
        for (int i = 0; i < 3; i++){

            //Recorre la matriz de botones de arriba a abajo
            for (int j = 0; j < 3; j++){

                //Deshabilita el botón en la matriz
                matrizBotones[i][j].setEnabled(false);
            }
        }
    }

    /**
     * Cierra los canales de comunicaciones e interrumpe la conexión con el servidor
     *
     * @throws IOException Excepción de socket
     */
    private void desconectar() throws IOException {
        entrada.close();
        salida.close();
        socket.close();
        vista.getTxtResultados().append("\n[GAME] Fin de la partida!");
    }

    /**
     * Determina cuándo y que botón ha sido pulsado y actúa en consecuencia enviando al servidor las coordenadas
     * del botón
     *
     * @param e ActionEvent que representa las coordenadas del botón pulsado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        salida.println(e.getActionCommand());
    }
}
package com.apalacios.ejercicio2.cliente;

import javax.swing.*;

public class Vista {
    private JPanel panelPrincipal;
    private JButton btn0_0;
    private JButton btn0_1;
    private JButton btn0_2;
    private JButton btn1_0;
    private JButton btn2_0;
    private JButton btn1_1;
    private JButton btn2_1;
    private JButton btn1_2;
    private JButton btn2_2;
    private JButton[][] matrizBotones;
    private JTextArea txtResultados;

    Vista(){
        JFrame frame = new JFrame();
        frame.setContentPane(panelPrincipal);
        frame.setResizable(false);
        frame.setTitle("3 En raya multijugador");
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        matrizBotones = new JButton[3][3];
        addBotones();
    }

    JButton[][] getMatrizBotones(){
        return matrizBotones;
    }

    JTextArea getTxtResultados(){
        return txtResultados;
    }

    private void addBotones(){
        matrizBotones[0][0] = btn0_0;
        matrizBotones[0][1] = btn0_1;
        matrizBotones[0][2] = btn0_2;
        matrizBotones[1][0] = btn1_0;
        matrizBotones[1][1] = btn1_1;
        matrizBotones[1][2] = btn1_2;
        matrizBotones[2][0] = btn2_0;
        matrizBotones[2][1] = btn2_1;
        matrizBotones[2][2] = btn2_2;
    }
}

package com.apalacios.ejercicio2.servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Servidor {
    static int[][] tablero = new int[3][3];
    private static boolean turnoJ1;
    private static Socket socket;

    /**
     * Método principal de la clase, arranca el servidor y espera las conexiones de los clientes
     * @param args args
     */
    public static void main(String[] args) {
        ServerSocket socketServidor;
        System.out.println("[INFO] Iniciando servidor...");
        try {
            socketServidor = new ServerSocket(10578);
            System.out.println("[ OK ] Servidor iniciado!");

            //Aceptar la petición del primer cliente
            socket = socketServidor.accept();
            System.out.println("[ OK ] Conexión establecida con jugador 1");
            Jugador jugador1 = new Jugador(socket);

            //Aceptar la petición del segundo cliente
            socket = socketServidor.accept();
            System.out.println("[ OK ] Conexión establecida con jugador 2");
            Jugador jugador2 = new Jugador(socket);

            iniciarPartida(jugador1, jugador2);

        } catch (IOException ex) {
            System.out.println("[FAIL] Error en el servidor");
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Bucle principal de ejecución. Controla el desarrollo de la partida. Esto incluye los turnos de los usuarios,
     * las comprobaciones del tablero, y el envío y recepción de datos de los usuarios
     *
     * @param jugador1 El jugador representado por la letra X en el tablero
     * @param jugador2 El jugadr representado por la letra O en el tablero
     * @throws IOException Excepción de socket
     */
    private static void iniciarPartida(Jugador jugador1, Jugador jugador2) throws IOException {
        inicializarTablero();
        jugador1.enviarEstadoTablero();
        jugador2.enviarEstadoTablero();

        while (!socket.isClosed()){
            jugador1.tieneTurno(true);
            jugador2.tieneTurno(false);
            turnoJ1 = true;

            colocarFicha(jugador1.recibirFicha());
            jugador1.enviarEstadoTablero();
            jugador2.enviarEstadoTablero();
            comprobarTablero(jugador1, jugador2);

            if(!socket.isClosed()){
                jugador1.tieneTurno(false);
                jugador2.tieneTurno(true);
                turnoJ1 = false;

                colocarFicha(jugador2.recibirFicha());
                jugador1.enviarEstadoTablero();
                jugador2.enviarEstadoTablero();
                comprobarTablero(jugador1, jugador2);
            }
        }
    }

    /**
     * Inicializa todas las casillas del tablero a un espacio vacío (contiene 0).
     */
    private static void inicializarTablero(){
        for (int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                tablero[i][j] = 0;
            }
        }
    }

    /**
     * Coloca una ficha en las coordenadas especificadas. Para ello convierte el string de datos que recibe, a un
     * vector del cual obtiene la cordenada X del primer campo, la cordenada Y del segundo campo y el tipo de ficha
     * a colocar del tercer campo.
     *
     * @param cords String que debe contener una serie de 3 números enteros separados por comas.
     */
    private static void colocarFicha(String cords){
        String[] vectorDatos = cords.split("");
        int cordX = Integer.parseInt(vectorDatos[0]);
        int cordY = Integer.parseInt(vectorDatos[1]);
        if(turnoJ1){
            tablero[cordX][cordY] = 1;
        }else{
            tablero[cordX][cordY] = 2;
        }
    }

    /**
     * Determina si hay un vencedor  en la partida comprobando el estado de todas las casillas del tablero.
     * En caso de haber 3 casillas con el mismo contenido diferente de 0, ya sea de forma vertical, horizontal o
     * diagonal, se determinará quíen es el ganador.
     *
     * @return int que representa el resultado de la comrpobación. (0 = sin ganador, 1 = ganador J1, 2 = ganador J2).
     */
    private static int comprobarVictoria(){
        boolean victoriaJ1 = false;
        boolean victoriaJ2 = false;

        for (int i = 0; i < 3; i++){

            //Comprobar verticales
            if(tablero[i][0] == tablero[i][1] && tablero[i][1] == tablero[i][2] &&
                    !victoriaJ1 && !victoriaJ2){
                if(tablero[i][0] == 1){
                    victoriaJ1 = true;
                }else if(tablero[i][0] == 2){
                    victoriaJ2 = true;
                }
            }

            //Comprobar horizontales
            if(tablero[0][i] == tablero[1][i] && tablero[1][i] == tablero[2][i] &&
                    !victoriaJ1 && !victoriaJ2){
                if(tablero[0][i] == 1){
                    victoriaJ1 = true;
                }else if(tablero[0][i] == 2){
                    victoriaJ2 = true;
                }
            }
        }

        //Comprobar diagonales
        if ((tablero[0][0] == tablero[1][1] && tablero[1][1] == tablero[2][2]) ||
                (tablero[0][2] == tablero[1][1] && tablero[1][1] == tablero[2][0])){
            if(!victoriaJ1 && !victoriaJ2){
                if(tablero[1][1] == 1){
                    victoriaJ1 = true;
                }else if(tablero[1][1] == 2){
                    victoriaJ2 = true;
                }
            }
        }

        //Devolver resultado
        if(victoriaJ1){
            return 1;
        }else if(victoriaJ2){
            return 2;
        }else{
            return 0;
        }
    }

    /**
     * Determina si la partida a terminado en empate comprobando si alguna de las casillas del tablero sigue estando
     * vacía (contiene 0).
     *
     * @return boolean que representa el estado del empate. (true = empate, false = aún se puede jugar)..
     */
    private static boolean comprobarEmpate(){
        boolean empate = true;
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                if (tablero[i][j] == 0) {
                    empate = false;
                    break;
                }
            }
        }
        return empate;
    }

    /**
     * Ejecuta los métodos para comprobar si alguien ha ganado o si hay un empate y notifica a los jugadores en caso
     * de que se cumpla una de estas dos condiciones
     *
     * @param jugador1 El jugador representado por la letra X en el tablero
     * @param jugador2 El jugadr representado por la letra O en el tablero
     * @throws IOException Excepción de socket
     */
    private static void comprobarTablero(Jugador jugador1, Jugador jugador2) throws IOException {
        if(comprobarVictoria() == 1){
            jugador1.enviarVictoria(true);
            jugador2.enviarVictoria(false);
            desconectarClientes(jugador1, jugador2);
        }else if(comprobarVictoria() == 2){
            jugador1.enviarVictoria(false);
            jugador2.enviarVictoria(true);
            desconectarClientes(jugador1, jugador2);
        }else if(comprobarEmpate()){
            jugador1.enviarEmpate();
            jugador2.enviarEmpate();
            desconectarClientes(jugador1, jugador2);
        }
    }

    /**
     * Interrumpe la conexión con ambos clientes
     *
     * @param jugador1 El jugador representado por la letra X en el tablero
     * @param jugador2 El jugadr representado por la letra O en el tablero
     * @throws IOException Excepción de socket
     */
    private static void desconectarClientes(Jugador jugador1, Jugador jugador2) throws IOException {
        jugador1.desconnectar();
        jugador2.desconnectar();
    }
}

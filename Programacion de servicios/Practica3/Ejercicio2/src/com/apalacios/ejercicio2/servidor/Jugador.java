package com.apalacios.ejercicio2.servidor;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Clase que representa a un jugador en la partida
 */
class Jugador{
    private Socket socket;
    private DataOutputStream dos;
    private DataInputStream dis;

    /**
     * Constructor de la clase
     *
     * @param socket socket de conexión con el cliente
     */
    Jugador(Socket socket) {
        try {
            this.socket = socket;
            //Se crean los canales de comunicación con el cliente
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Jugador.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Espera a recibir las coordenadas donde se desea posicionar una ficha desde el cliente
     *
     * @return String que contiene el dato recibido
     * @throws IOException Excepción de socket
     */
    @SuppressWarnings("deprecation")
    String recibirFicha() throws IOException {
        return dis.readLine();
    }

    /**
     * Interrumpe la conexión con el servidor
     * @throws IOException Exepción de socket
     */
    void desconnectar() throws IOException {
        dos.writeUTF("desconectar");
        dis.close();
        dos.close();
        socket.close();
    }

    /**
     * Obtiene el estado actual de todas las casillas del tablero, y convierte la información a una cadena de texto.
     * La cadena resultante son una serie de números enteros de entre 0 y 2 separados por comas, cada número representa
     * el estado de una casilla del tablero. Los números se encuentran en orden de izquierda a derecha y de arriba a
     * abajo en el tablero.
     *
     * @throws IOException Exepción de socket
     */
    void enviarEstadoTablero() throws IOException {
        //Obtiene el tablero desde el servidor
        int[][] tablero = Servidor.tablero;

        //Crea un string que contendrá los datos del tablero
        StringBuilder tableroStr = new StringBuilder();

        //Itera el tablero de izquierda a derecha
        for (int i = 0; i < 3; i++){

            //Itera el tablero de arriba a abajo
            for (int j = 0;j < 3; j++){

                //Concatena el contenido de la casilla al String de datos
                tableroStr.append(tablero[i][j]);
            }
        }

        //Devuelve el String con los datos del tablero
        dos.writeUTF(tableroStr.toString());
    }

    /**
     * Envía un mensaje al cliente indicándole si tiene o no el turno, evitando así que los jugadores posicionen
     * fichas fuera de su turno
     *
     * @param turno boolean que determina si el cliente tiene o no el turno. (true = tiene turno, false = NO tiene turno)
     * @throws IOException Excepción de socket
     */
    void tieneTurno(boolean turno) throws IOException {
        if(turno){
            dos.writeUTF("comenzar");
        }else{
            dos.writeUTF("esperar");
        }
    }

    /**
     * Indica al usuario si ha ganado o perdido la partida, enviandole un mensaje que puede ser "victoria" o "derrota"
     *
     * @param victoria boolean que determina si se ha ganado o no (true = ha ganado, false = ha perdido)
     * @throws IOException Excepción de socket
     */
    void enviarVictoria(boolean victoria) throws IOException {
        if(victoria){
            dos.writeUTF("victoria");
        }else{
            dos.writeUTF("derrota");
        }
    }

    /**
     * Indica al usuario si la partida ha terminado en empate, enviandole un mensaje "empate"
     *
     * @throws IOException Excepción de socket
     */
    void enviarEmpate() throws IOException {
        dos.writeUTF("empate");
    }
}
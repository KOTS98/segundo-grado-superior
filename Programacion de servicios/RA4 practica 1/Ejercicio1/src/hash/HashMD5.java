package hash;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Permite realizar hashing siguiendo el algoritmo MD5.
 *
 * @author KOTS98
 */
public class HashMD5 {

    private MessageDigest md;

    /**
     * Instancia el MessageDigest, encargado de realizar el proceso de hashing.
     */
    public HashMD5() {
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Realiza el hashing al mensaje recibido como parámetro.
     *
     * @param mensaje Representa el mensaje a hashear.
     * @return Vector de bytes con el resultado del hashing.
     */
    public byte[] hashear(String mensaje) {
        return md.digest(mensaje.getBytes());
    }

    /**
     * Convierte un mensaje hasheado a una cadena en hexadecimal (base 16).
     *
     * @param bytes Representa el mensaje hasheado a transformar.
     * @return Cadena de texto resultado de convertir el mensaje hasheado a hexadecimal.
     */
    public String hashToString(byte[] bytes) {
        BigInteger nResultado = new BigInteger(1, bytes);

        StringBuilder hashStr = new StringBuilder(nResultado.toString(16));
        while (hashStr.length() < 32) {
            hashStr.insert(0, "0");
        }

        return hashStr.toString();
    }

    /**
     * Determina si los dos mensajes hasheados son iguales.
     *
     * @param bytes Representa el mensaje hasheado.
     * @param bytes2 Representa el segundo mensaje hasheado.
     * @return booleano que indica si los hashes coinciden o no.
     */
    public boolean compararHash(byte[] bytes, byte[] bytes2) {
        String newHashStr = hashToString(bytes);
        String newHashStr2 = hashToString(bytes2);

        return newHashStr.equals(newHashStr2);
    }

    /**
     * Determina si los dos mensajes hasheados son iguales.
     *
     * @param hashStr Representa el mensaje hasheado.
     * @param hashStr2 Representa el segundo mensaje hasheado.
     * @return booleano que indica si los hashes coinciden o no.
     */
    public boolean compararHash(String hashStr, String hashStr2) {

        return hashStr.equals(hashStr2);
    }

    /**
     * Determina si el hasheo sin formatear de un mensaje, y un hasheo ya formateado, son iguales.
     *
     * @param bytes Representa el mensaje hasheado sin formatear.
     * @param hashStr Representa el mensaje hasheado en hexadecimal.
     * @return booleano que indica si los hashes coinciden o no.
     */
    public boolean compararHash(byte[] bytes, String hashStr) {
        String newHashStr = hashToString(bytes);

        return newHashStr.equals(hashStr);
    }
}

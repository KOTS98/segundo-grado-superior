package hash;

import org.apache.commons.codec.binary.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Permite realizar hashing bajo el algoritmo SHA en sus diferentes versiones (1, 384, 512).
 *
 * @author KOTS98
 */
public class HashSHA {

    public enum VersionSHA {
        SHA1,
        SHA384,
        SHA512
    }

    private MessageDigest md;
    private byte[] bytes;

    public HashSHA() {
        md = null;
        bytes = null;
    }

    /**
     * Hashea el mensaje recibido como parámetro, utilizando la versión de SHA especificada.
     *
     * @param version Representa la versión de SHA que desea utilizarse (1, 384, 512).
     * @param mensaje Representa el mensaje que desea hashearse.
     * @return Vector de bytes que contiene el mensaje hasheado.
     */
    public byte[] hashear(VersionSHA version, String mensaje) {
        try {
            String versionStr = "";

            // Determinar la versión a utilizar del algoritmo SHA
            switch (version) {
                case SHA1:
                    versionStr = "SHA-1";
                    break;
                case SHA384:
                    versionStr = "SHA-384";
                    break;
                case SHA512:
                    versionStr = "SHA-512";
                    break;
            }

            // Instaciar el MessageDigest encargado de realizar el hashing
            md = MessageDigest.getInstance(versionStr);
            md.update(mensaje.getBytes());

            // Realizar el hashing
            bytes = md.digest();

            return bytes;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Convierte un mensaje hasheado a una cadena de texto (String) en hexadecimal (base 16).
     *
     * @param bytes Representa el mensaje hasehado.
     * @return String que contiene el valor hexadecimal del mensaje hasheado.
     */
    public String hashToHexString(byte[] bytes) {
        return String.valueOf(Hex.encodeHex(bytes));
    }

    /**
     * Determina si los dos mensajes hasheados son iguales.
     *
     * @param bytes Representa el mensaje hasheado.
     * @param bytes2 Representa el segundo mensaje hasheado.
     * @return booleano que indica si los hashes coinciden o no.
     */
    public boolean compararHash(byte[] bytes, byte[] bytes2) {
        String newHashStr = hashToHexString(bytes);
        String newHashStr2 = hashToHexString(bytes2);

        return newHashStr.equals(newHashStr2);
    }

    /**
     * Determina si los dos mensajes hasheados son iguales.
     *
     * @param hashStr Representa el mensaje hasheado.
     * @param hashStr2 Representa el segundo mensaje hasheado.
     * @return booleano que indica si los hashes coinciden o no.
     */
    public boolean compararHash(String hashStr, String hashStr2) {

        return hashStr.equals(hashStr2);
    }

    /**
     * Determina si el hasheo sin formatear de un mensaje, y un hasheo ya formateado, son iguales.
     *
     * @param bytes Representa el mensaje hasheado sin formatear.
     * @param hashStr Representa el mensaje hasheado en hexadecimal.
     * @return booleano que indica si los hashes coinciden o no.
     */
    public boolean compararHash(byte[] bytes, String hashStr) {
        String newHashStr = hashToHexString(bytes);

        return newHashStr.equals(hashStr);
    }
}

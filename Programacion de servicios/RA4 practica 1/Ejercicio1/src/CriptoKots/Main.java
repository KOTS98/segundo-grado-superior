package CriptoKots;

import java.io.File;
import java.util.Scanner;

public class Main {
    static Scanner in = new Scanner(System.in);
    static Cifrador cifrador = new Cifrador();

    public static void main(String[] args) {
        while (true) {
            mostrarMenu();
        }
    }

    private static void mostrarMenu() {
        System.out.println("-=CriptoKots=-\n\n1. Cifrar fichero\n2. Descifrar fichero\nQue deseas hacer?");
        int opcionMenu = in.nextInt();

        if (opcionMenu != 1 && opcionMenu != 2) {
            System.out.println("[FAIL] No se a introducido una opción válida.\nPulsa 'ENTER' para continuar.");
            in.nextLine();
        } else {
            limpiar();
            cifrador.setFichero(obtenerFichero());
            if (opcionMenu == 1) {
                cifrador.cifrar();
            } else {
                cifrador.descifrar();
            }
        }
    }

    private static File obtenerFichero() {
        in.nextLine();
        System.out.println("Introduce la ruta absoluta del fichero:");
        String ruta = in.nextLine();
        File fichero;

        fichero = new File(ruta);
        if (!fichero.exists()) {
            System.out.println("[FAIL] Archivo no encontrado.");
            return null;
        } else if (!fichero.canRead()) {
            System.out.println("[FAIL] Error de lectura del fichero.");
            return null;
        }
        System.out.println("[DEBUG] Todo correcto con el fichero.");
        return fichero;
    }

    private static void limpiar() {
        for (int i = 0; i < 50; i++) {
            System.out.println("");
        }
    }
}

package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

/**
 * Conjunto de utilidades para trabajar con los diferentes tipos de cifrado y hashing
 *
 * @author KOTS98
 */
public class Util {

    /**
     * Convierte un array de bytes a String
     * @param bytes Representa el array de bytes que se desea convertir
     *
     * @return String que representa el array de bytes convertido a String
     */
    public static String bytesToString(byte[] bytes) {
        byte[] bytesAux = new byte[bytes.length + 1];
        bytesAux[0] = 1;
        System.arraycopy(bytes, 0, bytesAux, 1, bytes.length);
        return new BigInteger(bytesAux).toString(36);
    }

    /**
     * Convierte un String en un array de bytes
     *
     * @param cadena Representa el String que se desea convertir en un
     *          array de bytes
     *
     * @return Array de bytes que representa el String convertido
     */
    public static byte[] stringToBytes(String cadena) {
        byte[] b2 = new BigInteger(cadena, 36).toByteArray();
        return Arrays.copyOfRange(b2, 1, b2.length);
    }

    /**
     * Lee el contenido de un fichero, devolviendo un string con
     * el contenido del mismo
     *
     * @param filePath Representa la ruta del fichero que se desea leer
     * @return String que representa el contenido del fichero indicado
     */
    public static String readFileAsString(String filePath) throws IOException {

        // Se crea la estructura para leer del fichero
        StringBuilder fileData = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead;

        // Se lee el contenido de el fichero entero
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);

            // Se almacena el contenido en el String
            fileData.append(readData);
        }
        reader.close();

        return fileData.toString();
    }
}

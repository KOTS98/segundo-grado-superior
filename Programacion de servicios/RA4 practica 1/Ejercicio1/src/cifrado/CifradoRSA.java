package cifrado;

import util.Util;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Permite realizar cifrado mediante el algoritmo RSA, utilizando claves de
 * 512, 1024, 2048 y 4096 bits.
 *
 * @author KOTS98
 */
public class CifradoRSA {

    private PublicKey publicKey;
    private PrivateKey privateKey;
    public enum bitsKey {
        BITS512,
        BITS1024,
        BITS2048,
        BITS4096
    }

    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }

    /**
     * Genera la clave pública para el algoritmo RSA
     *
     * @param key String que representa la clave almacenada en el fichero
     */
    public void setPublicKeyString(String key) throws NoSuchAlgorithmException, InvalidKeySpecException{
        byte[] encodedPublicKey = Util.stringToBytes(key);

        // Se instancia el KeyFactory para obtener una contraseña valida para RSA
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(encodedPublicKey);

        // Se asigna la clave publica
        this.publicKey = keyFactory.generatePublic(publicKeySpec);
    }

    /**
     * Genera la clave privada para el algoritmo RSA
     *
     * @param key String que representa la clave almacenada en el fichero
     */
    public void setPrivateKeyString(String key) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] encodedPrivateKey = Util.stringToBytes(key);

        // Se instancia el KeyFactory para obtener una contraseña valida para RSA
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(encodedPrivateKey);

        // Se asigna la clave privada
        this.privateKey = keyFactory.generatePrivate(privateKeySpec);
    }

    /**
     * Obtiene la clase pública.
     *
     * @return String que representa la clave publica utilizada en el algoritmo
     */
    public String getPublicKeyString(){
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(this.publicKey.getEncoded());
        return Util.bytesToString(x509EncodedKeySpec.getEncoded());
    }

    /**
     * Obtiene la clave privada
     *
     * @return String que representa la clave privada utilizada en el algoritmo
     */
    public String getPrivateKeyString(){
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(this.privateKey.getEncoded());
        return Util.bytesToString(pkcs8EncodedKeySpec.getEncoded());
    }

    /**
     * Genera el par de claves que será utizado por el algoritmo RSA
     *
     * @param size int que representa el número de bytes que se utilizarán para
     *             crear las contraseñas de cifrado
     */
    public void generarKeyPair(bitsKey size) {
        try {

            // Se crea el KeyPairGenerator con el número de bytes introducido por el usuario
            KeyPairGenerator kpg = null;
            kpg = KeyPairGenerator.getInstance("RSA");
            int bits = 0;

            switch (size) {
                case BITS512:
                    bits = 512;
                    break;
                case BITS1024:
                    bits = 1024;
                    break;
                case BITS2048:
                    bits = 2048;
                    break;
                case BITS4096:
                    bits = 4096;
                    break;
            }

            kpg.initialize(bits);

            // Se crean KeyPair que generará las contraseñas
            KeyPair kp = kpg.genKeyPair();

            // Se asignan las claves
            this.privateKey = kp.getPrivate();
            this.publicKey = kp.getPublic();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    /**
     * Cifra un mnesaje recibido como parámetro
     *
     * @param plain String que representa el mensaje a cifrar
     * @return String que representa el mensaje cifrado
     */
    public String cifrar(String plain) {
        try {
            byte[] encryptedBytes;

            // Se indica que se va a cifrar utilizando RSA
            Cipher cipher = null;
            cipher = Cipher.getInstance("RSA");

            // Se indica que se va a cifrar, y para ello se utiliza la clave publica
            cipher.init(Cipher.ENCRYPT_MODE, this.publicKey);
            encryptedBytes = cipher.doFinal(plain.getBytes());

            // Se devuelve el texto cifrado
            return Util.bytesToString(encryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Desencripta el mensaje cifrado recibido como parámetro
     *
     * @param result Representa el texto que se desea desencriptar mediante
     *               la utilización del algoritmo RSA
     * @return String que representa el mensaje desencriptado
     */
    public String descifrar(String result) {
        try {
            byte[] decryptedBytes;

            // Se indica que se va a utilizar el algoritmo RSA
            Cipher cipher = null;
            cipher = Cipher.getInstance("RSA");

            // Se indica que se va a desencriptar, y para ello se utiliza la clave privada
            cipher.init(Cipher.DECRYPT_MODE, this.privateKey);

            // Se desencripta
            decryptedBytes = cipher.doFinal(Util.stringToBytes(result));

            // Se devuelve el texto desencritado
            return new String(decryptedBytes);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException
                | InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Almacena en el fichero en la rúta recibida como parámetro, la clave pública.
     *
     * @param path Representa la ruta del fichero en la que
     *             se desea almacenar la clave pública
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void guardarPublicKey(String path) {
        try {

            // Se crea el fichero que va  utilizar en la ruta indicada
            File directorio = new File(path);
            directorio.createNewFile();

            // Se escribe el contenido en el fichero
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
            out.write(this.getPublicKeyString());

            // Se fuerza la salida en el fichero
            out.flush();

            // Se cierra el fichero
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Almacena en el fichero en la rúta recibida como parámetro, la clave privada.
     *
     * @param path Representa la ruta del fichero en la que
     *             se desea almacenar la clave privada
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public void guardarPrivateKey(String path) {
        try {

            // Se crea el fichero que va  utilizar en la ruta indicada
            File directorio = new File(path);
            directorio.createNewFile();

            // Se escribe el contenido en el fichero
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8));
            out.write(this.getPrivateKeyString());

            // Se fuerza la salida en el fichero
            out.flush();

            // Se cierra el fichero
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Obtiene la clave pública desde el fichero cuya ruta se recibe como parámetro
     *
     * @param path Representa la ruta del fichero que contiene la clave
     *             pública que se utilizará en el algoritmo
     */
    public void abrirPublicKey(String path) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {

        // Abre y lee el fichero
        String content = Util.readFileAsString(path);

        // Asigna la clave pública
        this.setPublicKeyString(content);
    }

    /**
     * Obtiene la clave privada desde el fichero cuya ruta se recibe como parámetro
     *
     * @param path Representa la ruta del fichero que contiene la clave
     *             privada que se utilizará en el algoritmo
     */
    public void abrirPrivateKey(String path) throws IOException, NoSuchAlgorithmException,
            InvalidKeySpecException {

        // Abre y lee el fichero
        String content = Util.readFileAsString(path);

        // Asigna la clave pública
        this.setPrivateKeyString(content);
    }
}

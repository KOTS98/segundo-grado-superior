package cifrado;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

/**
 * Permite realizar procesos de encriptación y desencriptación mediante el algoritmo AES.
 * Puede utilizar 128, 192, 256 para completar estos procesos.
 *
 * @author KOTS98
 */
public class CifradoAES {

    public enum BitsClave {
        BITS_128,
        BITS_192,
        BITS_256
    }

    private Key key;
    private String secretKey;
    private Cipher aesCipher;

    /**
     * Instancia el cifrador de la clase y genera una clave para llevar a cabo el cifrado.
     *
     * @param bitsClave Determina los bits de longitud para la clave 128 / 192 / 256.
     * @param secretKey Representa la clave en formato String que se utilizará para el cifrado (mínimo 16 carácteres).
     */
    public CifradoAES(BitsClave bitsClave, String secretKey) {
        this.secretKey = secretKey;

        try {

            // Instancia el cifrador de AES
            aesCipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

            // Generar clave para AES
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            int bytesClave = 0;
            int bits = 0;
            switch (bitsClave) {
                case BITS_128:
                    bits = 128;
                    bytesClave = 16;
                    break;
                case BITS_192:
                    bits = 192;
                    bytesClave = 24;
                    break;
                case BITS_256:
                    bits = 256;
                    bytesClave = 32;
                    break;
            }
            keyGenerator.init(bits);

            // Generar la clave secreta (Debe contener como mínimo 16 carácteres)
            key = new SecretKeySpec(secretKey.getBytes(), 0, bytesClave, "AES");

        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Devuelve la actual clave secreta o 'secret key'.
     *
     * @return String que contiene la clave secreta.
     */
    public String getScretKey() {
        return secretKey;
    }

    /**
     * Cambia la clave secreta o 'secret key' a la recibida como parámetro.
     *
     * @param secretKey Representa la clave secreta deseada para el cifrado AES,
     *                  debe contener al menos 16 carácteres.
     */
    public void setKey(String secretKey) {
        this.secretKey = secretKey;
        key = new SecretKeySpec(secretKey.getBytes(), 0, 16, "AES");
    }

    /**
     * Cifra el mensaje recibido como parámeto, utilizando el número de bits y la clave secreta
     * recibidos como parámetros en el constructor.
     *
     * @param mensaje Representa el mensaje que se desea encriptar.
     * @return String que contiene el mensaje encriptado.
     */
    public byte[] cifrar(String mensaje) {
        try {

            // Se inicializa el cifrador y se realiza la encriptación
            aesCipher.init(Cipher.ENCRYPT_MODE, key);

            // Devolver el mensaje cifrado
            return aesCipher.doFinal(mensaje.getBytes());

        } catch (InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Descifra el mensaje recibido como parámetro. Es de suma importancia que tanto la
     * clave secreta o 'secret key' como el número de bits utilizados para cifrar, sean
     * idénticos a cuándo se realizo el cifrado, de lo contrario será imposible descifrar
     * el mensaje.
     *
     * @param msgEncriptado Representa el mensaje que desea descifrar.
     * @return String que contiene el mensaje descifrado.
     */
    public String descifrar(byte[] msgEncriptado) {
        try {

            // Se inicializa el cifrador y se descifra el mensaje
            aesCipher.init(Cipher.DECRYPT_MODE, key);
            byte[] msgDesencriptado = aesCipher.doFinal(msgEncriptado);

            // Se devuelve el resultado como una cadena de texto
            return new String(msgDesencriptado);

        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Transofra el mensaje cifrado recibido como parámetro, en una cadena de texto en
     * hexadecimal (base 16).
     *
     * @param msgEncriptado Representa el mensaje que se desea transformar a hexadecimal.
     * @return String que contiene el mensaje en hexadecimal.
     */
    public String toHexString(byte[] msgEncriptado) {
        StringBuilder msgHex = new StringBuilder();
        for (byte b : msgEncriptado) {
            msgHex.append(Integer.toHexString(0xFF & b));
        }
        return msgHex.toString();
    }
}

package pruebas;

import cifrado.CifradoAES;
import cifrado.CifradoRSA;
import hash.HashMD5;
import hash.HashSHA;

public class PruebasMain {
    public static void main(String[] args) {
        pruebaAES();
        pruebaRSA();
        pruebaMD5();
        pruebaSHA();
    }

    private static void pruebaAES() {
        CifradoAES aes128 = new CifradoAES(CifradoAES.BitsClave.BITS_128, "ContraseñaDe16Bytes");
        CifradoAES aes192 = new CifradoAES(CifradoAES.BitsClave.BITS_192, "ContraseñaSeguraDe24Bytes");
        CifradoAES aes256 = new CifradoAES(CifradoAES.BitsClave.BITS_256, ">ContraseñaSuperSeguraDe32Bytes<");

        String mensaje = "José Adrián Palacios Romo - Colegio Montessori";
        byte[] msgEncriptado128 = aes128.cifrar(mensaje);
        byte[] msgEncriptado192 = aes192.cifrar(mensaje);
        byte[] msgEncriptado256 = aes256.cifrar(mensaje);

        String msgDesc128 = aes128.descifrar(msgEncriptado128);
        String msgDesc192 = aes192.descifrar(msgEncriptado192);
        String msgDesc256 = aes256.descifrar(msgEncriptado256);

        System.out.println("========== Cifrado AES ==========");
        System.out.println("Original:\t\t" + mensaje);
        System.out.println("Cifrado 128:\t" + aes128.toHexString(msgEncriptado128));
        System.out.println("Cifrado 192:\t" + aes192.toHexString(msgEncriptado192));
        System.out.println("Cifrado 256:\t" + aes256.toHexString(msgEncriptado256));
        System.out.println("Desenc. 128:\t" + msgDesc128);
        System.out.println("Desenc. 192:\t" + msgDesc192);
        System.out.println("Desenc. 256:\t" + msgDesc256);
    }

    private static void pruebaRSA() {
        CifradoRSA rsa = new CifradoRSA();

        rsa.generarKeyPair(CifradoRSA.bitsKey.BITS512);
        rsa.guardarPublicKey("C:\\tmp\\publicKeyPrueba.key");
        rsa.guardarPrivateKey("C:\\tmp\\privateKeyPrueba.key");

        String mensaje = "José Adrián Palacios Romo - Colegio Montessori";
        String msgCifrado = rsa.cifrar(mensaje);
        String msgDesc = rsa.descifrar(msgCifrado);

        System.out.println("\n========== Cifrado RSA ==========");
        System.out.println("Original:\t\t" + mensaje);
        System.out.println("Cifrado:\t\t" + msgCifrado);
        System.out.println("Desenc:\t\t\t" + msgDesc);
    }

    private static void pruebaMD5() {
        HashMD5 md5 = new HashMD5();

        String mensaje = "José Adrián Palacios Romo - Colegio Montessori";
        byte[] mensajeHasheado = md5.hashear(mensaje);

        // Este es el resultado de hashear el mensaje de arriba, alterando este valor, el método compararHash detectará
        // que el hash del mensaje original, y el que le has proporcionado no coinciden
        String hashCheckSum = "584e8eafee9424673ad52c0b5b69395e";

        System.out.println("\n========== Hashing MD5 ==========");
        System.out.println("Original:\t" + mensaje);
        System.out.println("Hasheado:\t" + md5.hashToString(mensajeHasheado));

        if (md5.compararHash(mensajeHasheado, hashCheckSum))
            System.out.println("Integridad del hash: >CORRECTA<");
        else
            System.out.println("Integridad del hash: >DISCREPANCIA ENCONTRADA<");
    }

    private static void pruebaSHA() {
        HashSHA sha = new HashSHA();

        String mensaje = "José Adrián Palacios Romo - Colegio Montessori";
        byte[] mensajeSHA1 = sha.hashear(HashSHA.VersionSHA.SHA1, mensaje);
        byte[] mensajeSHA384 = sha.hashear(HashSHA.VersionSHA.SHA384, mensaje);
        byte[] mensajeSHA512 = sha.hashear(HashSHA.VersionSHA.SHA512, mensaje);

        // Este es el resultado de hashear el mensaje de arriba con SHA-1, alterando este valor o alterando el tipo la
        // versión de SHA, el método compararHash detectará que el hash del mensaje original, y el que le has
        // proporcionado no coinciden
        String hashCheckSum = "ff9c742796045ffefa3e9156f03582c5dad53110";

        System.out.println("\n========== Hashing SHA ==========");
        System.out.println("Original:\t" + mensaje);
        System.out.println("SHA-1:  \t" + sha.hashToHexString(mensajeSHA1));
        System.out.println("SHA-384:\t" + sha.hashToHexString(mensajeSHA384));
        System.out.println("SHA-512:\t" + sha.hashToHexString(mensajeSHA512));

        if (sha.compararHash(mensajeSHA1, hashCheckSum))
            System.out.println("Integridad del hash: >CORRECTA<");
        else
            System.out.println("Integridad del hash: >DISCREPANCIA ENCONTRADA<");
    }
}
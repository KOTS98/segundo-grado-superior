package comprimir_string;

public class ComprimirString {
    public static void main(String[] args) {
        char[] arrayDesc = {'a', 'a', 'b', 'c', 'c', 'c'};
        System.out.println(comprimirString(arrayDesc));
    }

    private static char[] comprimirString(char[] arrayDesc) {
        StringBuilder arrayComp = new StringBuilder();
        int charRep = 1;

        for (char c : arrayDesc) {
            boolean rep = arrayComp.toString().endsWith(String.valueOf(c));
            if (charRep > 1)
                if (rep)
                    charRep++;
                else {
                    arrayComp.append(charRep);
                    arrayComp.append(c);
                    charRep = 1;
                }
            else
                if (rep)
                    charRep++;
                else
                    arrayComp.append(c);
        }
        if (charRep > 1)
            arrayComp.append(charRep);
        return arrayComp.toString().toCharArray();
    }
}
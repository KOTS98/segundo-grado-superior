package apalacios.login.cliente;

import cifrado.CifradoAES;
import com.google.gson.Gson;
import hash.HashSHA;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Base64;
import java.util.Scanner;

class Cliente {

    /**
     * CREDENCIALES:
     *    Nick          Contraseña
     * ============================
     * 1. pepe          123456789
     * 2. manolo        firulais
     * 3. juan          montessori
     * 4. maria         contraseña
     * 5. patricia      prueba123
     */
    Cliente() {
        String pass = "UnaContraseñaEnormeYSuperMolonaQueMeAcaboDeInventar";
        CifradoAES aes = new CifradoAES(CifradoAES.BitsClave.BITS_256, pass);
        HashSHA sha = new HashSHA();
        Base64.Decoder decoder = Base64.getDecoder();
        Base64.Encoder encoder = Base64.getEncoder();

        try {

            // Establecer el canal de comunicaciones
            Socket socket = new Socket("localhost", 10578);
            DataInputStream dis = new DataInputStream(socket.getInputStream());
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            System.out.println("[ OK ] Conexión establecida con el servidor!");

            // Pedir credenciales
            Scanner in = new Scanner(System.in);
            System.out.println("LOGIN:\nNick > ");
            String nick = in.nextLine();
            System.out.println("Pass > ");
            String password = in.nextLine();
            Usuario credenciales = new Usuario(nick, sha.hashToHexString(sha.hashear(HashSHA.VersionSHA.SHA512, password)));

            // Enviar mensaje al servidor
            dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(credenciales)))));

            // Recibir respuesta del servidor
            String respuestaServer = new Gson().fromJson(aes.descifrar(decoder.decode(dis.readUTF())), Mensaje.class).getMsg();

            // Mostrar el estado del inicio de sesión
            switch (respuestaServer) {
                case "inicio":
                    System.out.println("Inicio de sesión exitoso! Bienvenido, " + nick);
                    break;
                case "error":
                    System.out.println("Error al iniciar sesión, compruebe las credenciales.");
                    break;
            }

        }catch (IOException ex){
            ex.printStackTrace();
        }
    }
}

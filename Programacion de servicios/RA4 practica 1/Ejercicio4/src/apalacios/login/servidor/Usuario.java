package apalacios.login.servidor;

class Usuario {
    private String nick;
    private String password;

    Usuario(String nick, String password) {
        this.nick = nick;
        this.password = password;
    }

    String getNick() {
        return nick;
    }

    String getPassword() {
        return password;
    }
}

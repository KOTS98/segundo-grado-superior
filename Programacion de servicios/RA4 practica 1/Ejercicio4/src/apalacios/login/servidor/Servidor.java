package apalacios.login.servidor;

import cifrado.CifradoAES;
import com.google.gson.Gson;
import hash.HashSHA;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

class Servidor {

    private Socket socket;
    private DataOutputStream dos;
    private DataInputStream dis;
    private Base64.Encoder encoder;
    private CifradoAES aes;
    private HashSHA sha;
    private ArrayList<Usuario> usuarios;


    Servidor() {
        ServerSocket socketServidor;
        System.out.println("[INFO] Iniciando servidor...");

        // Preparar para AES, SHA y BASE64
        String pass = "UnaContraseñaEnormeYSuperMolonaQueMeAcaboDeInventar";
        aes = new CifradoAES(CifradoAES.BitsClave.BITS_256, pass);
        sha = new HashSHA();
        Base64.Decoder decoder = Base64.getDecoder();
        encoder = Base64.getEncoder();

        crearUsuarios();

        try {
            socketServidor = new ServerSocket(10578);
            System.out.println("[ OK ] Servidor iniciado!");

            //Aceptar la petición del cliente
            socket = socketServidor.accept();
            System.out.println("[ OK ] Conexión establecida con el cliente!\n" +
                    "[INFO] Esperando autenticación...");

            //Se crean los canales de comunicación con el cliente
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());

            // Esperar credenciales
            Usuario usuario = new Gson().fromJson(aes.descifrar(decoder.decode(dis.readUTF())), Usuario.class);
            System.out.println("[INFO] Credenciales recibidas:" +
                    "\n\tNick:\t\t" + usuario.getNick() +
                    "\n\tContraseña:\t" + usuario.getPassword());


            comprobarCredenciales(usuario.getNick(), usuario.getPassword());

        } catch (IOException ex) {
            System.out.println("[FAIL] Error en el servidor");
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void crearUsuarios() {
        usuarios = new ArrayList<>();
        usuarios.add(new Usuario("pepe", "d9e6762dd1c8eaf6d61b3c6192fc408d4d6d5f1176d0c29169bc24e71c3f274ad27fcd5811b313d681f7e55ec02d73d499c95455b6b5bb503acf574fba8ffe85"));
        usuarios.add(new Usuario("manolo", "9e9eca333e2a701d350c2531a26ea7a8db617456b733ca9813a77f86b47e0e2665638d5c38ab499be7246630e8aaf022132ca7af9b15c9f7dc2d03f74296010e"));
        usuarios.add(new Usuario("juan", "db23f47208e9ae8790860d7a67ea238c959e82befa9320cf4c23718a5326e593d55763983bf5d8155c6e0ea6ed4ca399b9925e01e22373578938c6318e4b4f8c"));
        usuarios.add(new Usuario("maria", "559a0612917c8c516c7980c38b376cf2bb2387ae1b6944b3b32245ff4f2bad4d5db3811f02cff394fb62fa3feefd03e2eb83ba7bcca72a912bb87ea609c424da"));
        usuarios.add(new Usuario("patricia", "cfbd3e3a8adc49b9e0061ade86c84b499012048c903185821f9428dd981c4610b6d660d484a88641bf81b5b840422ea9e3a5da29c12821eed60b0d281e5f43db"));
    }

    private void comprobarCredenciales(String nick, String password) throws IOException {
        for (Usuario user : usuarios) {
            if (nick.equalsIgnoreCase(user.getNick())){
                iniciarSesion(sha.compararHash(password, user.getPassword()));
                System.out.println("[INFO] Credenciales correctas, inicio de sesión exitoso!");
                return;
            }
        }
        System.out.println("[INFO] Credenciales incorrectas, inicio de sesión denegado.");
        dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(new Mensaje("error"))))));
        dis.close();
        dos.close();
        socket.close();
    }

    private void iniciarSesion(boolean iniciar) throws IOException {
        if (iniciar) {
            dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(new Mensaje("inicio"))))));
        } else {
            dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(new Mensaje("error"))))));
        }
        dis.close();
        dos.close();
        socket.close();
    }
}

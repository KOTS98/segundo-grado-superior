package com.apalacios.ejercicio2.cliente;

class Comando {
    private String msg;

    Comando(String msg){
        this.msg = msg;
    }

    String getMsg() {
        return msg;
    }

    void setMsg(String msg) {
        this.msg = msg;
    }
}

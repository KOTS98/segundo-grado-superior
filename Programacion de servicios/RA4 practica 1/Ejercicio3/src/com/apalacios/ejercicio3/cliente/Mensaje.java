package com.apalacios.ejercicio3.cliente;

class Mensaje {
    private String msg;

    Mensaje(String msg){
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
package com.apalacios.ejercicio3.servidor;

import cifrado.CifradoAES;
import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Base64;

/**
 * Representa a un cliente en el servidor. Establece conexión con una aplicación cliente e itercambia datos
 */
class Cliente {
    private Socket socket;
    private DataOutputStream dos;
    private DataInputStream dis;

    private Base64.Decoder decoder;
    private Base64.Encoder encoder;
    private CifradoAES aes;

    /**
     * Constructor de la clase,
     *
     * @param socket El socket del que obtiene los canales de comunicaciones
     */
    Cliente(Socket socket){
        try {
            this.socket = socket;
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());

            String pass = "UnaContraseñaEnormeYSuperMolonaQueMeAcaboDeInventar";
            aes = new CifradoAES(CifradoAES.BitsClave.BITS_256, pass);
            decoder = Base64.getDecoder();
            encoder = Base64.getEncoder();

        } catch (IOException e) {
            System.out.println("[FAIL] Error al establecer los canales de comunicación");
        }
    }

    /**
     * Recibe un número de entre 0 y 6 que representa la columna donde el jugador desea colocar su ficha
     *
     * @return int que representa la columna en la que se colocará la ficha
     */
    @SuppressWarnings("deprecation")
    int recibirFicha(){
        int ficha = 0;
        try {
            ficha = Integer.parseInt(new Gson().fromJson(aes.descifrar(decoder.decode(dis.readLine())), Mensaje.class).getMsg());
        } catch (IOException e) {
            System.out.println("[FAIL] Error al recibir movimiento del cliente");
        }
        return ficha;
    }

    /**
     * Envía un mensaje al jugador para indicarle si es su turno o el del contrincante
     *
     * @param turno (true = su turno, false = turno del contrincante)
     */
    void tieneTurno(boolean turno){
        try {
            if(turno){
                dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(new Mensaje("comenzar"))))));
            }else{
                dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(new Mensaje("esperar"))))));
            }
        }catch (IOException e) {
            System.out.println("[FAIL] Error al enviar el turno al cliente");
        }
    }

    /**
     * Recibe el mensaje de confirmación "ok" del cliente
     */
    @SuppressWarnings("deprecation")
    void recibirConfirmacion(){
        try{
            String mensaje = new Gson().fromJson(aes.descifrar(decoder.decode(dis.readLine())), Mensaje.class).getMsg();
            if(!mensaje.equalsIgnoreCase("ok")){
                System.out.println("[FAIL] Se ha recibido un dato no esperado: \"" + mensaje + "\"");
            }
        }catch(IOException e){
            System.out.println("[FAIL] Error al confirmar con el cliente");
        }
    }

    /**
     * envia al jugador el estado actual del tablero
     *
     * @param tablero Representa el tablero de juego
     */
    void enviarTablero(int[][] tablero){
        StringBuilder tableroStr = new StringBuilder();
        for (int i = 0; i < 7; i++){
            for (int j = 0; j < 6; j++){
                tableroStr.append(tablero[i][j]);
            }
        }
        try {
            dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(new Mensaje(tableroStr.toString()))))));
        } catch (IOException e) {
            System.out.println("[FAIL] Error al enviar el estado del tablero");
        }
    }

    /**
     * Envía al jugador un mensaje de fin de partida, que puede ser "empate", "victoria" o "derrota"
     *
     * @param mensajeFinPartida Mensaje de fin de partida
     */
    void enviarFinPartida(String mensajeFinPartida){
        try {
            dos.writeUTF(new String(encoder.encode(aes.cifrar(new Gson().toJson(new Mensaje(mensajeFinPartida))))));
        } catch (IOException e) {
            System.out.println("[FAIL] Error al enviar el mensaje de fin de partida");
        }
    }

    /**
     * Interrumpe  la conexión con el jugador y cierra los canales de comunicación
     */
    void desconnectar() {
        try {
            dis.close();
            dos.close();
            socket.close();
        } catch (IOException e) {
            System.out.println("[FAIL] Error al cerrar el enlace de comunicaciones");
        }
    }
}

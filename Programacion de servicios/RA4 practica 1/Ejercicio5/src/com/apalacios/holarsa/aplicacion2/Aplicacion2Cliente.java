package com.apalacios.holarsa.aplicacion2;

import cifrado.CifradoAES;
import cifrado.CifradoRSA;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

class Aplicacion2Cliente {

    private DataOutputStream dos;
    private DataInputStream dis;
    private Base64.Decoder decoder;
    private Base64.Encoder encoder;
    private CifradoAES aes;
    private CifradoRSA rsa;
    private String clavePublicaServer;

    Aplicacion2Cliente() {

        // Preparar para AES, RSA y BASE64
        String pass = "UnaContraseñaEnormeYSuperMolonaQueMeAcaboDeInventar";
        aes = new CifradoAES(CifradoAES.BitsClave.BITS_256, pass);

        rsa = new CifradoRSA();
        rsa.generarKeyPair(CifradoRSA.bitsKey.BITS4096);

        decoder = Base64.getDecoder();
        encoder = Base64.getEncoder();

        try {

            // Establecer los canales de comunicación
            Socket socket = new Socket("localhost", 10578);
            dis = new DataInputStream(socket.getInputStream());
            dos = new DataOutputStream(socket.getOutputStream());
            System.out.println("[ OK ] Conexión establecida con el servidor!");

            // Se hace el intercambio de claves públicas RSA
            intercambiarClaves();

            // Se encripta el mensaje con la clave pública del servidor
            String mensaje = "Hola aplicación 1!";
            String msgCifrado = encriptarMensaje(mensaje);

            // Se envía el mensaje cifrado al servidor
            enviarMensaje(msgCifrado);

            // Se descifra el mensaje recivido desde el servidor
            System.out.println(recibirMensaje());

        } catch (IOException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void intercambiarClaves() throws IOException {
        dos.writeUTF(new String(encoder.encode(aes.cifrar(rsa.getPublicKeyString()))));
        clavePublicaServer = aes.descifrar(decoder.decode(dis.readUTF()));

        System.out.println("[ OK ] Intercambio de claves públicas RSA realizado con exito!");
    }

    private String encriptarMensaje(String mensaje) throws InvalidKeySpecException, NoSuchAlgorithmException {
        rsa.setPublicKeyString(clavePublicaServer);
        return rsa.cifrar(mensaje);
    }

    private void enviarMensaje(String msgCifrado) throws IOException {
        dos.writeUTF(msgCifrado);
    }

    private String recibirMensaje() throws IOException {
        return rsa.descifrar(dis.readUTF());
    }
}

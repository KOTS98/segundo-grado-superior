package com.apalacios.holarsa.aplicacion1;

import cifrado.CifradoAES;
import cifrado.CifradoRSA;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

class Aplicacion1Server {

    private DataOutputStream dos;
    private DataInputStream dis;
    private Base64.Decoder decoder;
    private Base64.Encoder encoder;
    private CifradoAES aes;
    private CifradoRSA rsa;
    private String clavePublicaCliente;

    Aplicacion1Server() {
        ServerSocket serverSocket;
        System.out.println("[INFO] Iniciando servidor...");

        // Preparar para AES, RSA y BASE64
        String pass = "UnaContraseñaEnormeYSuperMolonaQueMeAcaboDeInventar";
        aes = new CifradoAES(CifradoAES.BitsClave.BITS_256, pass);

        rsa = new CifradoRSA();
        rsa.generarKeyPair(CifradoRSA.bitsKey.BITS4096);

        decoder = Base64.getDecoder();
        encoder = Base64.getEncoder();

        try {
            serverSocket = new ServerSocket(10578);
            System.out.println("[ OK ] Servidor iniciado!");

            // Aceptar la conexión del cliente (Aplicación 2)
            Socket socket = serverSocket.accept();
            System.out.println("[ OK ] Conexión establecida con el cliente!");

            // Se crean los canales de comunicación
            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());

            // Se hace el intercambio de claves públicas RSA
            intercambiarClaves();

            // Se descifra el mensaje recivido desde el cliente
            System.out.println(recibirMensaje());

            // Se encripta el mensaje con la clave pública del servidor
            String mensaje = "Hola aplicación 2!";
            String msgCifrado = encriptarMensaje(mensaje);

            // Se envía el mensaje cifrado al cliente
            enviarMensaje(msgCifrado);

        } catch (IOException | InvalidKeySpecException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void intercambiarClaves() throws IOException {
        clavePublicaCliente = aes.descifrar(decoder.decode(dis.readUTF()));
        dos.writeUTF(new String(encoder.encode(aes.cifrar(rsa.getPublicKeyString()))));

        System.out.println("[ OK ] Intercambio de claves públicas RSA realizado con exito!");
    }

    private String encriptarMensaje(String mensaje) throws InvalidKeySpecException, NoSuchAlgorithmException {
        rsa.setPublicKeyString(clavePublicaCliente);
        return rsa.cifrar(mensaje);
    }

    private void enviarMensaje(String msgCifrado) throws IOException {
        dos.writeUTF(msgCifrado);
    }

    private String recibirMensaje() throws IOException {
        return rsa.descifrar(dis.readUTF());
    }
}

package apalacios.biblioteca.util;

import java.io.IOException;

public class Util {
	public static void limpiarTerminal() {
		for (int i = 0; i < 50; i++) {
			System.out.println("");
		}
	}

	public static void enterParaContinuar() {
		System.out.println("Pulsa \"ENTER\" para continuar...");
		try {
			System.in.read();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

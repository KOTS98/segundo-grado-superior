package apalacios.biblioteca.usuarios;

import java.util.ArrayList;
import java.util.Scanner;

import apalacios.biblioteca.libros.ContenedorLibros;
import apalacios.biblioteca.libros.Libro;
import apalacios.biblioteca.util.Util;

public class Cliente {
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String dni;
	private ArrayList<Libro> librosTomados;

	public Cliente(String nombre, String apellido1, String apellido2, String dni) {
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.dni = dni;
		this.librosTomados = new ArrayList<>();
	}

	public String getDni() {
		return this.dni;
	}

	@Override
	public String toString() {
		return "Nombre:" + this.nombre + "\nApellidos:" + this.apellido1 + " " + this.apellido2 + "\nDNI:" + this.dni;
	}

	public void tomarLibro(Scanner in) {
		Util.limpiarTerminal();
		ContenedorLibros.librosDisponibles();
		System.out.println("\nSelecciona un libro (ISBN):");
		in.nextLine();
		String isbn = in.nextLine();
		if (ContenedorLibros.comprobarLibro(isbn)) {
			ContenedorLibros.libroTomado(isbn);
			librosTomados.add(ContenedorLibros.getLibro(isbn));
			System.out.println("[INFO] Libro tomado.");
			Util.enterParaContinuar();
		} else {
			System.out.println("[ERROR] No hay ning�n libro disponible con ese ISBN.");
			Util.enterParaContinuar();
		}
	}

	public void devolverLibro(Scanner in) {
		Util.limpiarTerminal();
		in.nextLine();
		if (librosTomados.size() > 0) {
			verLibrosTomados(in);
			System.out.println("\n�Que libro deseas devolver? (ISBN):");
			String isbn = in.nextLine();
			boolean existe = false;
			for (Libro libro : librosTomados) {
				if (libro.getIsbn().equalsIgnoreCase(isbn)) {
					existe = true;
				}
			}
			if (existe == true) {
				ContenedorLibros.libroDevuelto(isbn);
				librosTomados.removeIf(libro -> libro.getIsbn().equalsIgnoreCase(isbn));
				System.out.println("[INFO] Libro devuelto.");
				Util.enterParaContinuar();
			} else {
				System.out.println("[ERROR] No posees ning�n libro con ese ISBN.");
				Util.enterParaContinuar();
			}
		}
	}

	public void verLibrosTomados(Scanner in) {
		Util.limpiarTerminal();
		for (Libro libro : librosTomados) {
			System.out.println(libro.toString() + "\n==================");
		}
		if (librosTomados.size() == 0) {
			System.out.println("[INFO] No has tomado ning�n libro.");
			Util.enterParaContinuar();
		}
		in.nextLine();
	}
}

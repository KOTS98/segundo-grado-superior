package apalacios.biblioteca.usuarios;

import java.util.ArrayList;

import apalacios.biblioteca.util.Util;

public class ContenedorClientes {
	public static ArrayList<Cliente> listaClientes = new ArrayList<>();

	public static boolean comprobarCliente(String dni) {
		boolean registrado = false;
		for (Cliente cliente : listaClientes) {
			if (cliente.getDni().equalsIgnoreCase(dni)) {
				registrado = true;
			}
		}
		return registrado;
	}

	public static void mostrarClientes() {
		Util.limpiarTerminal();
		for (Cliente cliente : listaClientes) {
			System.out.println(cliente.toString() + "\n==================");
		}
	}

	public static void altaCliente(Cliente nuevoCliente) {
		listaClientes.add(nuevoCliente);
	}

	public static void bajaCliente(String dni) {
		listaClientes.removeIf(cliente -> cliente.getDni().equalsIgnoreCase(dni));
	}

	public static int contarClientes() {
		System.out.println(listaClientes.size());
		return listaClientes.size();
	}

	public static Cliente getCliente(String dni) {
		for (Cliente cliente : listaClientes) {
			if (cliente.getDni().equalsIgnoreCase(dni)) {
				return cliente;
			}
		}
		return null;
	}
}

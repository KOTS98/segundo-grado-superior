package apalacios.biblioteca.usuarios;

import java.util.Scanner;

import apalacios.biblioteca.libros.ContenedorLibros;
import apalacios.biblioteca.libros.Libro;
import apalacios.biblioteca.libros.Seccion;
import apalacios.biblioteca.util.Util;

public class Administrador {

	public static void altaLibro(Scanner in) {
		Util.limpiarTerminal();
		in.nextLine();
		System.out.println("Nuevo libro:\n==============\nT�tulo:");
		String titulo = in.nextLine();
		System.out.println("Autor:");
		String autor = in.nextLine();
		System.out.println("ISBN:");
		String isbn = in.nextLine();
		System.out.println("Secci�n:");
		String seccion;
		do {
			Util.limpiarTerminal();
			Seccion.listarSecciones();
			System.out.println("Selecciona una secci�n:");
			seccion = in.nextLine();
		} while (!Seccion.comprobarSeccion(seccion, in));
		Libro nuevoLibro = new Libro(titulo, autor, isbn, Seccion.valueOf(seccion));
		if (ContenedorLibros.comprobarLibro(isbn)) {
			System.out.println("[ERROR] Ya existe un libro con ese ISBN.");
			Util.enterParaContinuar();
		} else {
			ContenedorLibros.altaLibro(nuevoLibro);
			System.out.println("[INFO] Libro dado de alta.");
			Util.enterParaContinuar();
		}
	}

	public static void bajaLibro(Scanner in) {
		Util.limpiarTerminal();
		in.nextLine();
		ContenedorLibros.mostrarLibros();
		System.out.println("\n�Que libro deseas eliminar?(ISBN):");
		String isbn = in.nextLine();
		if (ContenedorLibros.comprobarLibro(isbn)) {
			ContenedorLibros.bajaLibro(isbn);
			System.out.println("[INFO] Libro dado de baja.");
			Util.enterParaContinuar();
		} else {
			System.out.println("[ERROR] El libro especificado no existe.");
			Util.enterParaContinuar();
		}
	}

	public static void altaCliente(Scanner in) {
		Util.limpiarTerminal();
		in.nextLine();
		System.out.println("Nuevo cliente:\n=============\nNombre:");
		String nombre = in.nextLine();
		System.out.println("Primer apellido:");
		String apellido1 = in.nextLine();
		System.out.println("Segundo apellido:");
		String apellido2 = in.nextLine();
		System.out.println("DNI:");
		String dni = in.nextLine();
		Cliente nuevoCliente = new Cliente(nombre, apellido1, apellido2, dni);
		if (ContenedorClientes.comprobarCliente(nuevoCliente.getDni())) {
			System.out.println("[ERROR] Ya existe un usuario con ese DNI.");
			Util.enterParaContinuar();
		} else {
			ContenedorClientes.altaCliente(nuevoCliente);
			System.out.println("[INFO] Usuario dado alta.");
			Util.enterParaContinuar();
		}
	}

	public static void bajaCliente(Scanner in) {
		Util.limpiarTerminal();
		in.nextLine();
		ContenedorClientes.mostrarClientes();
		System.out.println("\n�Que usuario deseas eliminar?(DNI):");
		String dni = in.nextLine();
		if (ContenedorClientes.comprobarCliente(dni)) {
			ContenedorClientes.bajaCliente(dni);
			System.out.println("[INFO] Usuario dado de baja.");
			Util.enterParaContinuar();
		} else {
			System.out.println("[ERROR] El usuario indicado no existe.");
			Util.enterParaContinuar();
		}
	}
}

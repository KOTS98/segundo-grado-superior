package apalacios.biblioteca.main;

import java.util.Scanner;

import apalacios.biblioteca.libros.ContenedorLibros;
import apalacios.biblioteca.usuarios.Administrador;
import apalacios.biblioteca.usuarios.Cliente;
import apalacios.biblioteca.usuarios.ContenedorClientes;
import apalacios.biblioteca.util.Util;

public class Main {
	private static int opcionMenu;
	private static boolean error;
	private static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		do {
			menuPrincipal();
		} while (true);
	}

	private static void menuPrincipal() {
		do {
			Util.limpiarTerminal();
			System.out.println("BIBLIOTECA:\n==================\n" + "1. Administrador\n" + "2. Cliente\n"
					+ "3. Salir\n\n" + "�C�mo deseas iniciar sesi�n?:");
			error = false;
			opcionMenu = in.nextInt();
			switch (opcionMenu) {
			case 1:
				accesoAdmin();
				break;
			case 2:
				accesoCliente();
				break;
			case 3:
				Util.limpiarTerminal();
				System.out.println("[INFO] Fin del programa.");
				System.exit(0);
				break;
			default:
				error = true;
				System.out.println("[ERROR] La opci�n seleccionada no existe.");
				Util.enterParaContinuar();
				break;
			}

		} while (error == true);
	}

	private static void accesoAdmin() {
		do {
			do {
				Util.limpiarTerminal();
				System.out.println("ADMINISTRADOR:\n================\n" + "1. Dar de alta un libro\n"
						+ "2. Dar de baja un libro\n" + "3. Dar de alta un cliente\n" + "4. Dar de baja un cliente\n"
						+ "5. Cerrar sesi�n\n\n" + "�Que deseas hacer?:");
				error = false;
				opcionMenu = in.nextInt();
				switch (opcionMenu) {
				case 1:
					Administrador.altaLibro(in);
					break;
				case 2:
					Administrador.bajaLibro(in);
					break;
				case 3:
					Administrador.altaCliente(in);
					break;
				case 4:
					Administrador.bajaCliente(in);
					break;
				case 5:
					menuPrincipal();
					break;
				default:
					error = true;
					System.out.println("[ERROR] La opci�n seleccionada no existe.");
					Util.enterParaContinuar();
					break;
				}
			} while (error == true);
		} while (true);
	}

	private static void accesoCliente() {
		if (ContenedorClientes.contarClientes() > 0) {
			in.nextLine();
			ContenedorClientes.mostrarClientes();
			System.out.println("\n�Que cliente desea inciar sesi�n? (DNI):");
			String dni = in.nextLine();
			if (ContenedorClientes.comprobarCliente(dni)) {
				Cliente cliente = ContenedorClientes.getCliente(dni);
				do {
					do {
						Util.limpiarTerminal();
						System.out.println("CLIENTE:\n================\n" + "1. Ver libros disponibles\n"
								+ "2. Ver libros en posesi�n\n" + "3. Retirar un libro\n" + "4. Devolver un libro\n"
								+ "5. Cerrar sesi�n\n\n" + "�Que deseas hacer?:");
						error = false;
						opcionMenu = in.nextInt();
						switch (opcionMenu) {
						case 1:
							ContenedorLibros.librosDisponibles();
							break;
						case 2:
							cliente.verLibrosTomados(in);
							Util.enterParaContinuar();
							break;
						case 3:
							cliente.tomarLibro(in);
							break;
						case 4:
							cliente.devolverLibro(in);
							break;
						case 5:
							menuPrincipal();
							break;
						default:
							error = true;
							System.out.println("[ERROR] La opci�n seleccionada no existe.");
							Util.enterParaContinuar();
							break;
						}
					} while (error == true);
				} while (true);
			} else {
				System.out.println("[ERROR] No existe ning�n cliente con ese DNI.");
				Util.enterParaContinuar();
			}
		} else {
			System.out.println("[ERROR] No se ha registrado ning�n cliente (Pueden ser creados por el administrador).");
			Util.enterParaContinuar();
		}
	}
}

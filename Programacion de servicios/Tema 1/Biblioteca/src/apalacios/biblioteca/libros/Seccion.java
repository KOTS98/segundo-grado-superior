package apalacios.biblioteca.libros;

import java.util.Scanner;

import apalacios.biblioteca.util.Util;

public enum Seccion {
	economia("Economia"), derecho("Derecho"), historia("Historia"), informatica("Informatica");

	private String nombre;

	private Seccion(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public static void listarSecciones() {
		for (Seccion seccion : Seccion.values()) {
			System.out.println(seccion.getNombre());
		}
		System.out.println("");
	}

	public static boolean comprobarSeccion(String seccionComprobar, Scanner in) {
		boolean existe = false;
		for (Seccion seccion : Seccion.values()) {
			if (seccion.getNombre().equalsIgnoreCase(seccionComprobar)) {
				existe = true;
			}
		}
		if (existe == false) {
			System.out.println("[ERROR] La seccion especificada no existe.");
			Util.enterParaContinuar();
			in.nextLine();
		}
		return existe;
	}
}

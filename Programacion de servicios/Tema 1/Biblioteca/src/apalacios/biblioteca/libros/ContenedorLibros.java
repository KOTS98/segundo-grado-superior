package apalacios.biblioteca.libros;

import java.util.ArrayList;

import apalacios.biblioteca.util.Util;

public class ContenedorLibros {

	private static ArrayList<Libro> listaLibros = new ArrayList<>();

	public static boolean comprobarLibro(String isbn) {
		boolean existe = false;
		for (Libro libro : listaLibros) {
			if (libro.getIsbn().equalsIgnoreCase(isbn)) {
				existe = true;
			}
		}
		return existe;
	}

	public static boolean disponibilidadLibro(String isbn) {
		boolean disponible = false;
		for (Libro libro : listaLibros) {
			if (libro.getDisponible()) {
				disponible = true;
			}
		}
		return disponible;
	}

	public static void librosDisponibles() {
		Util.limpiarTerminal();
		boolean disponible = false;
		for (Libro libro : listaLibros) {
			if (libro.getDisponible()) {
				disponible = true;
				System.out.println(libro.toString() + "\n==================");
			}
		}
		if (disponible == false) {
			System.out.println("[INFO] No hay ning�n libro dispobile.");
		}
	}

	public static void mostrarLibros() {
		for (Libro libro : listaLibros) {
			System.out.println(libro.toString() + "\n==================");
		}
	}

	public static void altaLibro(Libro nuevoLibro) {
		listaLibros.add(nuevoLibro);
	}

	public static void bajaLibro(String isbn) {
		listaLibros.removeIf(libro -> libro.getIsbn().equalsIgnoreCase(isbn));
	}

	public static void libroTomado(String isbn) {
		for (Libro libro : listaLibros) {
			if (libro.getIsbn().equalsIgnoreCase(isbn)) {
				libro.setDisponible(false);
			}
		}
	}

	public static void libroDevuelto(String isbn) {
		for (Libro libro : listaLibros) {
			if (libro.getIsbn().equalsIgnoreCase(isbn)) {
				libro.setDisponible(true);
			}
		}
	}

	public static Libro getLibro(String isbn) {
		for (Libro libro : listaLibros) {
			if (libro.getIsbn().equalsIgnoreCase(isbn)) {
				return libro;
			}
		}
		return null;
	}
}

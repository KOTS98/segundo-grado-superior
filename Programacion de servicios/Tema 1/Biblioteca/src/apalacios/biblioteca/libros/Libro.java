package apalacios.biblioteca.libros;

public class Libro {
	private String titulo;
	private String autor;
	private String isbn;
	private Seccion seccion;
	private boolean disponible;

	public Libro(String titulo, String autor, String isbn, Seccion seccion) {
		this.titulo = titulo;
		this.autor = autor;
		this.isbn = isbn;
		this.seccion = seccion;
		this.disponible = true;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getAutor() {
		return autor;
	}

	public String getIsbn() {
		return isbn;
	}

	public String getSeccion() {
		return seccion.getNombre();
	}

	public boolean getDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	@Override
	public String toString() {
		return "T�tulo: " + titulo + "\nAutor: " + autor + "\nISBN: " + isbn + "\nSecci�n: " + seccion.getNombre();
	}

}

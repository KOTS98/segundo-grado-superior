import java.util.Observable;
import java.util.Observer;

//Clase observadora que representa a un cliente del servidor de alarmas.
class Cliente implements Observer {

    private String nombre;

    //Constructor de la clase, recive un String que representa el nombre del cliente.
    Cliente(String nombre){
        this.nombre = nombre;
    }

    //Sobreescribe el método update de la clase Observer. Este método responde a las actualizaciones del objeto al que
    //observa (el servidor de alarmas) y muestra un mensaje por terminal indicando su nombre y que ha sido informado.
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("¡Soy el cliente " + this.nombre + " y he sido informado!");
    }
}

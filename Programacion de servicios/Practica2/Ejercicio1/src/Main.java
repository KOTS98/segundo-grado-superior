import java.util.InputMismatchException;
import java.util.Scanner;

//Clase principal del programa.
public class Main {

    //Método principal del programa, se encarga de crear los objetos necesarios para el funcionamiento del mismo,
    //después, pide al usuario un número por terminal que será usado para una cuenta atrás. Cuándo la cuenta atrás
    //llegue a 0, se llamará al método activarAlarma en el objeto de tipo ServidorAlarmas.
    public static void main(String[] args) throws InterruptedException {
        ServidorAlarmas servidor = ServidorAlarmas.obtenerInstancia();
        Cliente cliente1 = new Cliente("Pedro");
        Cliente cliente2 = new Cliente("Manolo");
        servidor.addObserver(cliente1);
        servidor.addObserver(cliente2);
        boolean error;
        int segundos = 0;
        do{
            error = false;
            System.out.println("¿Cuánto segundos quieres para la cuenta antrás?:");
            Scanner in = new Scanner(System.in);
            try{
                segundos = in.nextInt();
            }catch(InputMismatchException e){
                error = true;
                System.out.println("[ERROR] Tipo de dato incorrecto, asegurate de introducir un número entero.");
            }
        }while(error);
        for(int i = segundos; i >= 0; i--){
            Thread.sleep(1000);
            System.out.println("Cuenta atrás: " + i);
        }
        servidor.activarAlarma();
    }
}

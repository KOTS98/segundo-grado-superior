import java.util.Observable;

//Clase observable que representa a un servidor de alarmas.
class ServidorAlarmas extends Observable{
    private static ServidorAlarmas instanciaUnica;

    private ServidorAlarmas(){
    }

    //Este método evita que se pueda crear más de una instancia de esta clase, cumpliendo con el patrón de diseño
    //Singleton.
    private synchronized static void crearInstancia(){
        if(instanciaUnica == null){
            instanciaUnica = new ServidorAlarmas();
        }
    }

    //Crea y devuelve la instancia de esta clase.
    static ServidorAlarmas obtenerInstancia(){
        crearInstancia();
        return instanciaUnica;
    }

    //Notifica a los observers (objetos de tipo cliente) que ha ocurrido un cambio.
    void activarAlarma() {
        this.setChanged();
        this.notifyObservers();
    }
}

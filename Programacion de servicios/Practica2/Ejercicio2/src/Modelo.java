//Esta clase recive los 4 objetos de tipo Aparato que representan los 4 aparatos de aire acondicionado, posee un
//único método que comprueba si están o no funcionando los aparatos de aire acondicionado. Aunque pueda parecer
//innecesario una clase para llevar a cabo esta función, el uso del patrón de diseño MVC (Modelo Vista Controlador),
//hace al programa más fácilmente expandible.

class Modelo {
    private Aparato aparato1;
    private Aparato aparato2;
    private Aparato aparato3;
    private Aparato aparato4;

    Modelo(Aparato aparato1, Aparato aparato2, Aparato aparato3, Aparato aparato4){
        this.aparato1 = aparato1;
        this.aparato2 = aparato2;
        this.aparato3 = aparato3;
        this.aparato4 = aparato4;
    }

    //Este metodo devuelve un vector de 4 booleanos, cada uno contiene el estado de la variable "funcionando" en su
    //respectivo objeto de tipo Aparato.
    boolean[] comprobarEstadoAparatos() {
        boolean[] estadoAparatos = new boolean[4];
        estadoAparatos[0] = aparato1.getFuncionando();
        estadoAparatos[1] = aparato2.getFuncionando();
        estadoAparatos[2] = aparato3.getFuncionando();
        estadoAparatos[3] = aparato4.getFuncionando();
        return estadoAparatos;
    }
}

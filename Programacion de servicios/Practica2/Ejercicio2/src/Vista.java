import javax.swing.*;
import java.util.Observable;

//Esta clase forma la interfaz gráfica del programa. Se trata de una clase Observable, observada por los 4 objetos de
//tipo Aparato. A nivel de usuario, cuenta con un "spinner" que permite ajustar el valor de temperatura ambiente,
//un "slider" que representa el termostato y permite ajustar la temperatura deseada a la que los aparatos de aire
//acondicionado deben de empezar a funcionar y 4 "labels" que indican si están o no funcionando dichos aparatos.

public class Vista extends Observable {
    private static Vista instanciaUnica;
    JSpinner spTempAmbiente;
    JSlider slTermostato;
    JLabel lblAparato1;
    JLabel lblAparato2;
    JLabel lblAparato3;
    JLabel lblAparato4;
    JPanel panel;

    //El constructor de la clase. Aqui se establecen valores como el título de la ventana, su tamaño, y su localización
    //en la pantalla. También se dota de opacidad a las etiquetas de funcionamiento de los aparatos de aire
    //acondicionado, lo que hace visible el color de fondo de la etiqueta.
    private Vista(){
        JFrame frame = new JFrame("Ejercicio 2 - Termostato");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);
        lblAparato1.setOpaque(true);
        lblAparato2.setOpaque(true);
        lblAparato3.setOpaque(true);
        lblAparato4.setOpaque(true);
    }

    //Este método evita que se cree más de una instancia de esta clase, cumpliendo con el patrón de diseño Singleton.
    private synchronized static void crearInstancia(){
        if(instanciaUnica == null){
            instanciaUnica = new Vista();
        }
    }

    //Este metodo crea y devuelve la instancia del objeto.
    static Vista getInstancia(){
        crearInstancia();
        return instanciaUnica;
    }

    //Este método comprueba la diferencia entre la temperatura ambiente, y la temperatura fijada por el termostato,
    //y en caso de que la temperatura ambiente sea superior a la del termostato, notifica a los observadores
    //(los aparatos de aire acondicionado) para que comiencen a funcionar, en caso contrario, les notifica que dejen
    //de funcionar.
    void actualizarEstado(){
        boolean encender;
        if ((int) spTempAmbiente.getValue() > slTermostato.getValue()) {
            encender = true;
        } else encender = false;
        this.notifyObservers(encender);
        this.setChanged();
    }
}

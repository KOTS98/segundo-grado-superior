//Clase principal del programa, encargada de generar los objetos necesarios para el funcionamiento del mismo.
public class Main {

    //Método principal del programa, genera 4 objetos de tipo Aparato, que representan los 4 aparatos de aire
    //acondicionado, y los establece como observadores del objeto de tipo Vista.
    public static void main(String[] args) {
        Aparato aparato1 = new Aparato();
        Aparato aparato2 = new Aparato();
        Aparato aparato3 = new Aparato();
        Aparato aparato4 = new Aparato();
        Vista vista = Vista.getInstancia();
        vista.addObserver(aparato1);
        vista.addObserver(aparato2);
        vista.addObserver(aparato3);
        vista.addObserver(aparato4);
        Modelo modelo = new Modelo(aparato1, aparato2, aparato3, aparato4);
        Controlador controlador = new Controlador(vista, modelo);
    }
}

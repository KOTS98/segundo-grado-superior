import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

//Esta clase actúa como un intermediario entre la vista y el modelo. Establece los escuchadores de cambio provenientes
//de la clase vista y ejecuta el método en la clase modelo que determina el estado de funcionamiento de los aparatos
//de aire acondicionado.
public class Controlador implements ChangeListener {
    private Vista vista;
    private Modelo modelo;

    Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        addChangeListener(this);
    }

    //Este metodo añade los escuchadores de cambio "ChangeListener" a los elementos de la vista que representan
    //la temperatura ambiente y el termostato.
    private void addChangeListener(ChangeListener listener){
        vista.spTempAmbiente.addChangeListener(listener);
        vista.slTermostato.addChangeListener(listener);
    }

    //Sobreescribe el metodo stateChanged de la clase ChangeListener. Reacciona a cualquier cambio que ocurra en uno de
    //los dos elementos de la vista afectados por este escuchador de eventos. LLama al método actualizarEstado en el
    //objeto de Vista, y crea un vector de booleanos del que recibe los datos através del método estadoAparatos en el
    //objeto de Modelo y representan el estado de funcionamiento de los aparatos de aire acondicionado. En caso de que
    //un aparato esté encendido, cambia el texto y el color del mismo de la etiqueta que representa a ese aparato, en
    //caso contrario, la devuelve a su estado original.
    @Override
    public void stateChanged(ChangeEvent e) {
        vista.actualizarEstado();
        boolean[] estadoAparatos = modelo.comprobarEstadoAparatos();
        if(estadoAparatos[0]){
            vista.lblAparato1.setText("Encendido");
            vista.lblAparato1.setForeground(Color.GREEN);
        }else{
            vista.lblAparato1.setText("  Apagado ");
            vista.lblAparato1.setForeground(Color.RED);
        }
        if(estadoAparatos[1]){
            vista.lblAparato2.setText("Encendido");
            vista.lblAparato2.setForeground(Color.GREEN);
        }else{
            vista.lblAparato2.setText("  Apagado ");
            vista.lblAparato2.setForeground(Color.RED);
        }
        if(estadoAparatos[2]){
            vista.lblAparato3.setText("Encendido");
            vista.lblAparato3.setForeground(Color.GREEN);
        }else{
            vista.lblAparato3.setText("  Apagado ");
            vista.lblAparato3.setForeground(Color.RED);
        }
        if(estadoAparatos[3]){
            vista.lblAparato4.setText("Encendido");
            vista.lblAparato4.setForeground(Color.GREEN);
        }else{
            vista.lblAparato4.setText("  Apagado ");
            vista.lblAparato4.setForeground(Color.RED);
        }
    }
}

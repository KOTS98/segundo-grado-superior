import java.util.Observable;
import java.util.Observer;

//Este es un tipo de objeto observador, que responde a los cambios ocurridos en el objeto Vista, ajustando su variable
//booleana "funcionando" que determina si el aire acondicionado debe o no activarse y devuelve este valor a la vista.
public class Aparato implements Observer {

    private Boolean funcionando = false;

    public Aparato(){}

    //Devuelve el estado de la variable "funcionando", que representa si el aire acondicionado está o no encendido.
    Boolean getFuncionando() {
        return funcionando;
    }

    //Sobreescribe el metodo update de la clase Observer, que responde a las actualizaciones del objeto al que observa,
    //en este caso al objeto de tipo Vista. Al recibir la notificación, recive también un booleano arg que determina
    //el estado de funcionamiento del aparato de aire acondicionado.
    @Override
    public void update(Observable o, Object arg) {
        this.funcionando = (boolean) arg;
    }
}

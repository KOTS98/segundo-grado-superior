package Ejercicio1;

public class Hilo extends Thread{
    private String nombre;
    private int inicioContador;

    public Hilo(String nombre, int inicioContador){
        this.nombre = nombre;
        this.inicioContador = inicioContador;
    }

    @Override
    public void run(){
        for (int i = inicioContador; i  < (inicioContador + 10); i++){
            System.out.println(nombre + " vale: " + i);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

package Ejercicio1;

import static java.lang.Thread.sleep;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Hilo hilo1 = new Hilo("Hilo 1", 1);
        Hilo hilo2 = new Hilo("Hilo 2", 11);
        Hilo hilo3 = new Hilo("Hilo 3", 21);

        hilo1.start();
        while(hilo1.isAlive()){
            sleep(10);
        }
        hilo2.start();
        while(hilo2.isAlive()){
            sleep(10);
        }
        hilo3.start();
    }
}

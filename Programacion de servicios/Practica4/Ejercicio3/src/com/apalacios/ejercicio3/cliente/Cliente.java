package com.apalacios.ejercicio3.cliente;

import com.google.gson.Gson;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Clase que representa a un jugador en la partida, despliega una interfaz gráfica y permite intercambiar datos con el
 * servidor de juego
 */
class Cliente implements ActionListener {
    private JButton[] botones;
    private JButton[][] tablero;
    private Vista vista;
    private DataInputStream entrada;
    private PrintStream salida;
    private Socket socket;

    /**
     * Constructor de la clase. Inicializa la matriz de casillas y el vector de botones
     */
    Cliente(Vista vista){
        this.vista = vista;
        botones = vista.getVectorBotones();
        tablero = vista.getMatrizCasillas();
        socket = null;
        addActionListeners(this);
        if(conectar()){
            iniciarJuego();
        }
    }

    /**
     * Recorre el vector de botones y dota a cada uno de ellos de un ActionListener, que permitirá saber
     * cuándo el botón ha sido pulsado.
     *
     * @param listener Escuchador de eventos que determina cuándo el botón ha sido pulsado.
     */
    private void addActionListeners(ActionListener listener){
        //Recorre el vector de botones
        for (int i = 0; i < 7; i++){
            botones[i].addActionListener(listener);
            //botones[i].setOpaque(true);
        }
    }

    /**
     * Establece la conexión con el servidor de juego y crea los canales de comunicaciones.
     *
     * @return boolean que determina si la conexión se ha producido o no con éxito (true = conectado con éxito,
     * false = error de conexión)
     */
    private boolean conectar(){
        boolean conectado = true;
        try{
            socket = new Socket("localhost", 10578);
            entrada = new DataInputStream(socket.getInputStream());
            salida = new PrintStream(socket.getOutputStream());
            vista.getTxtRegistro().append("[  OK   ] Conexión establecida con el servidor!");
        }catch (IOException e){
            conectado = false;
            vista.getTxtRegistro().append("[FAIL] Error de conexión con el servidor");
        }
        return conectado;
    }

    /**
     * Bucle principal de ejecución, se encarga de intercambiar datos con el servidor de juego
     */
    private void iniciarJuego(){
        while (!socket.isClosed()){
            try {
                recibirTablero();
                enviarConfirmacion();
                actualizarTurno();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Obtiene los datos del tablero desde el servidor. Estos datos se reciben como un único String que contiene
     * una secuencia de 42 números de entre 0 y 2. Cada número representa el estado de una casilla en el tablero,
     * donde 0 indica que esta vacía, 1 indica que pertenece al jugador 1, y 2 que pertenece al jugador 2.
     * Cada número en la secuencia, representa a una casilla del tablero, los números leidos de izquierda a derecha,
     * representan la casilla en el tablero leido de izquierda a derecha, y de arriba a abajo, formando una estructura
     * similar a esta:
     *
     * [A][B][C][D][E][F][G][H][I] <-- String recivido.
     *
     * [A][B][C]
     * [D][E][F] <-- Casillas del tablero.
     * [G][H][I]
     */
    private void recibirTablero(){
        String strTablero;
        try{
            strTablero = new Gson().fromJson(entrada.readUTF(), Mensaje.class).getMsg();
            String[] datosTablero = strTablero.split("");
            int contador = 0;

            //Recorre la matriz de casillas de izquierda a derecha
            for (int i = 0; i < 7; i++){

                //Recorre la matriz de casillas de arriba a abajo
                for (int j = 0; j < 6; j++){

                    //Determina si el contenido del dato recibido para la matriz es una casilla vacía, del jugador 1 o
                    //del jugador 2, en caso de pertenecer a un jugador, cambia el color de la casilla al que representa
                    //a cada jugador (rojo para el jugador 1, amarillo para el jugador 2)
                    if(datosTablero[contador].equalsIgnoreCase("1")){
                        tablero[i][j].setBackground(Color.RED);
                    }else if(datosTablero[contador].equalsIgnoreCase("2")){
                        tablero[i][j].setBackground(Color.YELLOW);
                    }
                    contador++;
                }
            }
        }catch (IOException e){
            vista.getTxtRegistro().append("\n[FAIL] Error al recibir el tablero");
            System.exit(0);
        }
    }

    /**
     * Envía al servidor un mensaje para confirmar que ha recibido los datos
     */
    private void enviarConfirmacion(){
        salida.println(new Gson().toJson(new Mensaje("ok")));
    }

    /**
     * Recibe desde el servidor un String que determina si es o no el turno del jugador.
     * El String recibido puede ser "comenzar" (Indica que es el turno del jugador), o
     * "esperar" (Indica que NO es el turno del jugador).
     * Tras recibir el String y determinar su contenido, recorre la matriz de botones y
     * los habilita o deshabilita en función de si es o no el turno del jugador.
     */
    private void actualizarTurno() throws IOException {
        String mensaje = new Gson().fromJson(entrada.readUTF(), Mensaje.class).getMsg();
        switch (mensaje) {
            //Permite al usuario hacer un movimiento
            case "comenzar":
                habilitarBotones(true);
                vista.getTxtRegistro().append("\n[GAME] Es tu turno!");
                break;

            //Mantiene en espera al usuario
            case "esperar":
                habilitarBotones(false);
                enviarConfirmacion();
                vista.getTxtRegistro().append("\n[GAME] Turno del contrincante...");
                break;

            //Finaliza la partida e indica al usuario que es el ganador
            case "victoria":
                habilitarBotones(false);
                vista.getTxtRegistro().append("\n[GAME] Enhorabuena! Has ganado!");
                desconectar();
                break;

            //Finaliza la partida e indica al usuario que es el perdedor
            case "derrota":
                habilitarBotones(false);
                vista.getTxtRegistro().append("\n[GAME] Mala suerte, has perdido...");
                desconectar();
                break;

            //Finaliza la partida e indica al usuario que ha habido un empate
            case "empate":
                habilitarBotones(false);
                vista.getTxtRegistro().append("\n[GAME] Tenemos un empate!");
                desconectar();
                break;
        }
    }

    /**
     * Habilita o deshabilita todos los botones en función del parámetro recibido
     *
     * @param habilitar Determina si se quieren habilitar o deshabilitar los botones (true = habilitar,
     *                  false = deshabilitar)
     */
    private void habilitarBotones(boolean habilitar){
        //Recorre el vector de botones
        for (int i = 0; i < 7; i++){
            botones[i].setEnabled(habilitar);
        }
    }

    /**
     * Cierra los canales de comunicaciones e interrumpe la conexión con el servidor
     *
     * @throws IOException Excepción de socket
     */
    private void desconectar() throws IOException {
        entrada.close();
        salida.close();
        socket.close();
        vista.getTxtRegistro().append("\n[GAME] Fin de la partida!");
    }

    /**
     * Determina cuándo y que botón ha sido pulsado y actúa en consecuencia enviando al servidor las coordenadas
     * del botón
     *
     * @param e ActionEvent que representa las coordenadas del botón pulsado
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        salida.println(new Gson().toJson(new Mensaje(e.getActionCommand())));
    }
}
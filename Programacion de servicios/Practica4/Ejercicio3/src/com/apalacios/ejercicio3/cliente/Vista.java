package com.apalacios.ejercicio3.cliente;

import javax.swing.*;
import java.awt.*;

class Vista {
    private JPanel panelPrincipal;
    private JButton casilla06;
    private JButton casilla05;
    private JButton casilla04;
    private JButton casilla03;
    private JButton casilla02;
    private JButton casilla01;
    private JButton casilla00;
    private JButton casilla50;
    private JButton casilla40;
    private JButton casilla30;
    private JButton casilla20;
    private JButton casilla10;
    private JButton casilla11;
    private JButton casilla21;
    private JButton casilla31;
    private JButton casilla41;
    private JButton casilla51;
    private JButton casilla12;
    private JButton casilla22;
    private JButton casilla32;
    private JButton casilla42;
    private JButton casilla52;
    private JButton casilla13;
    private JButton casilla14;
    private JButton casilla15;
    private JButton casilla16;
    private JButton casilla26;
    private JButton casilla25;
    private JButton casilla24;
    private JButton casilla23;
    private JButton casilla33;
    private JButton casilla34;
    private JButton casilla35;
    private JButton casilla36;
    private JButton casilla43;
    private JButton casilla44;
    private JButton casilla46;
    private JButton casilla45;
    private JButton casilla53;
    private JButton casilla54;
    private JButton casilla55;
    private JButton casilla56;
    private JButton btnColumna0;
    private JButton btnColumna1;
    private JButton btnColumna2;
    private JButton btnColumna3;
    private JButton btnColumna4;
    private JButton btnColumna5;
    private JButton btnColumna6;
    private JTextArea txtRegistro;

    private JButton[] vectorBotones;
    private JButton[][] matrizCasillas;

    Vista(){
        JFrame frame = new JFrame();
        frame.setContentPane(panelPrincipal);
        frame.setSize(new Dimension(750, 450));
        frame.setResizable(false);
        frame.setTitle("Conecta 4 Multijugador");
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        vectorBotones = new JButton[7];
        matrizCasillas = new JButton[7][6];
        addBotones();
        addCasillas();
    }

    JButton[] getVectorBotones(){
        return vectorBotones;
    }

    JButton[][] getMatrizCasillas(){
        return matrizCasillas;
    }

    JTextArea getTxtRegistro(){
        return txtRegistro;
    }

    private void addBotones(){
        vectorBotones[0] = btnColumna0;
        vectorBotones[1] = btnColumna1;
        vectorBotones[2] = btnColumna2;
        vectorBotones[3] = btnColumna3;
        vectorBotones[4] = btnColumna4;
        vectorBotones[5] = btnColumna5;
        vectorBotones[6] = btnColumna6;
    }

    private void addCasillas(){
        matrizCasillas[0][0] = casilla00;
        matrizCasillas[1][0] = casilla01;
        matrizCasillas[2][0] = casilla02;
        matrizCasillas[3][0] = casilla03;
        matrizCasillas[4][0] = casilla04;
        matrizCasillas[5][0] = casilla05;
        matrizCasillas[6][0] = casilla06;

        matrizCasillas[0][1] = casilla10;
        matrizCasillas[1][1] = casilla11;
        matrizCasillas[2][1] = casilla12;
        matrizCasillas[3][1] = casilla13;
        matrizCasillas[4][1] = casilla14;
        matrizCasillas[5][1] = casilla15;
        matrizCasillas[6][1] = casilla16;

        matrizCasillas[0][2] = casilla20;
        matrizCasillas[1][2] = casilla21;
        matrizCasillas[2][2] = casilla22;
        matrizCasillas[3][2] = casilla23;
        matrizCasillas[4][2] = casilla24;
        matrizCasillas[5][2] = casilla25;
        matrizCasillas[6][2] = casilla26;

        matrizCasillas[0][3] = casilla30;
        matrizCasillas[1][3] = casilla31;
        matrizCasillas[2][3] = casilla32;
        matrizCasillas[3][3] = casilla33;
        matrizCasillas[4][3] = casilla34;
        matrizCasillas[5][3] = casilla35;
        matrizCasillas[6][3] = casilla36;

        matrizCasillas[0][4] = casilla40;
        matrizCasillas[1][4] = casilla41;
        matrizCasillas[2][4] = casilla42;
        matrizCasillas[3][4] = casilla43;
        matrizCasillas[4][4] = casilla44;
        matrizCasillas[5][4] = casilla45;
        matrizCasillas[6][4] = casilla46;

        matrizCasillas[0][5] = casilla50;
        matrizCasillas[1][5] = casilla51;
        matrizCasillas[2][5] = casilla52;
        matrizCasillas[3][5] = casilla53;
        matrizCasillas[4][5] = casilla54;
        matrizCasillas[5][5] = casilla55;
        matrizCasillas[6][5] = casilla56;
    }
}

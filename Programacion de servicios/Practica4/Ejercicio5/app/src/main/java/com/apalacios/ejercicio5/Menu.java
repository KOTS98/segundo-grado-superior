package com.apalacios.ejercicio5;

class Menu {
    private String[] primeros;
    private String[] segundos;
    private String[] postres;
    private Integer precio;
    private String extra;

    Menu(String[] primeros, String[] segundos, String[] postres, Integer precio, String extra) {
        this.primeros = primeros;
        this.segundos = segundos;
        this.postres = postres;
        this.precio = precio;
        this.extra = extra;
    }

    String[] getPrimeros() {
        return primeros;
    }

    String[] getSegundos() {
        return segundos;
    }

    String[] getPostres() {
        return postres;
    }

    Integer getPrecio() {
        return precio;
    }

    String getExtra() {
        return extra;
    }
}

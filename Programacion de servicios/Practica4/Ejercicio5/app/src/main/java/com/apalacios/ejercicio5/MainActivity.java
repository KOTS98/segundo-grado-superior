package com.apalacios.ejercicio5;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class MainActivity extends AppCompatActivity {

    private TextView txtPrimeros;
    private TextView txtSegundos;
    private TextView txtPostres;
    private TextView txtExtra;
    private TextView txtPrecio;

    /**
     * Se ejecuta al iniciar la aplicación, asocia los atributos de la clase a los elementos de
     * la interfaz mediante su id y llama a una tarea asíncrona que realiza la descarga
     * de internet.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtPrimeros = findViewById(R.id.txtPrimeros);
        txtSegundos = findViewById(R.id.txtSegundos);
        txtPostres = findViewById(R.id.txtPostres);
        txtExtra = findViewById(R.id.txtExtra);
        txtPrecio = findViewById(R.id.txtPrecio);

        new Conexion().execute();
    }

    /**
     * Clase asíncrona que obtiene datos de internet, los convierte a un objeto de java mediante
     * la librería Gson y muestra los datos de este objeto por pantalla.
     */
    private class Conexion extends AsyncTask<String, Void, Void>{
        Menu menu;

        /**
         * Establece una conexión a la página que contiene los datos del menú y crea un objeto
         * con dichos datos.
         *
         * @param strings
         * @return
         */
        @Override
        protected Void doInBackground(String... strings) {
            try {
                String direccion_url = "https://pastebin.com/raw/zkwghUup";
                URL pagina = new URL(direccion_url);
                URLConnection uc = pagina.openConnection();
                uc.connect();
                BufferedReader in;
                in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
                String inputLine;
                StringBuilder contenido = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    contenido.append(inputLine).append("\n");
                }
                in.close();
                menu = new Gson().fromJson(contenido.toString(), Menu.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        /**
         * Muestra por pantalla los datos del objeto menu.
         * @param aVoid
         */
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            for(int i = 0; i < menu.getPrimeros().length; i++){
                txtPrimeros.append(menu.getPrimeros()[i] + "\n");
            }
            for(int i = 0; i < menu.getSegundos().length; i++){
                txtSegundos.append(menu.getSegundos()[i] + "\n");
            }
            for(int i = 0; i < menu.getPostres().length; i++){
                txtPostres.append(menu.getPostres()[i] + "\n");
            }
            txtExtra.setText(menu.getExtra());
            txtPrecio.setText(String.valueOf(menu.getPrecio()));
        }
    }
}

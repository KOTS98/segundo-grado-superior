package com.apalacios.practica4.ejercicio1.cliente;

/**
 * Clase de lanzamiento del cliente, crea un objeto de tipo cliente
 */
public class Main {

    /**
     * Crea un objeto cliente
     * @param args argumentos
     */
    public static void main(String[] args) {
        new Cliente().conectar();
    }
}
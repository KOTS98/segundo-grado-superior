package com.apalacios.practica4.ejercicio1.servidor;

import com.google.gson.Gson;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Properties;

/**
 * Clase que representa al servidor, utiliza el patrón de diseño Singleton
 */
class Servidor {
    private static Servidor instancia;
    private int puerto;
    private Properties conf = new Properties();
    private Registro[] datosCO2;

    /**
     * Constructor de la clase, llama al método obtenerPuerto
     */
    private Servidor(){
        obtenerPuerto();
    }

    /**
     * Instancia el servidor si no ha sido previamente instanciado
     */
    private synchronized static void crearInstancia(){
        if(instancia == null){
            instancia = new Servidor();
        }
    }

    /**
     * Devuelve la instancia del servidor
     *
     * @return Objeto de tipo servidor que representa la actual instancia del mismo
     */
    static Servidor obtenerInstancia(){
        crearInstancia();
        return instancia;
    }

    /**
     * Obtiene el puerto por el que debe de comunicarse desde el archivo de configuración "properties.conf"
     */
    private void obtenerPuerto(){
        FileReader fr;

        //Lee el archivo de configuración y obtiene el puerto
        try {
            fr = new FileReader("properties.conf");
            conf.load(fr);
            this.puerto = Integer.parseInt(conf.getProperty("server_port"));
            fr.close();

        //Controla la posibilidad de que el archivo no exista
        } catch (FileNotFoundException e) {
            System.out.println("[FAIL] Archivo de configuración no encontrado.\n" +
                    "Reiniciar el servidor generará un nuevo archivo de configuración.");
            System.exit(0);

        //Controla la posibilidad de que ocurra un fallo de lectura / escritura
        } catch (IOException e) {
            System.out.println("[FAIL] Fallo al leer el archivo de configuración.");
            System.exit(0);

        //Controla la posibilidad de que el contenido de "server_port" en el archivo de configuración no sea un número
        } catch (InputMismatchException e){
            System.out.println("[FAIL] El valor \"server_port\" en el archivo de configuración,\n" +
                    "no es un número. Modifique este valor antes de proceder.");
            System.exit(0);
        }
        System.out.println("[ OK ] Archivo de configuración cargado!");
    }

    /**
     * Inicia el servidor, llama al método que obtiene los datos necesarios del archivo "carbon_dioxide_in_air.csv",
     * espera y acepta la conexión del cliente, y ejecuta un bucle en el que recibe peticiones del cliente y las
     * responde
     */
    @SuppressWarnings({"InfiniteLoopStatement", "deprecation"})
    void iniciarServidor(){
        System.out.println("[INFO] Iniciando servidor...");
        try {

            //Instancia el socket del servidor con el puerto
            ServerSocket serverSocket = new ServerSocket(this.puerto);
            System.out.println("[ OK ] Servidor iniciado!");

            //Obtiene los datos del archivo
            obtenerDatos();
            System.out.println("[ OK ] Datos cargados!");

            //Crea el socket del cliente y los canales de comunicación
            Socket clienteSocket = serverSocket.accept();
            System.out.println("[INFO] Conexión establecida con cliente");
            PrintStream salida = new PrintStream(clienteSocket.getOutputStream());
            DataInputStream entrada = new DataInputStream(clienteSocket.getInputStream());
            String peticionCliente;

            //Bucle principal de ejecución, espera peticiones del cliente y envia las respuestas correspondientes
            while (true){

                //Espera petición del cliente
                peticionCliente = entrada.readLine();
                System.out.println("[INFO] Petición del cliente recibida:");

                //Obtiene el dato solicitado
                Registro respuesta = obtenerRespuesta(peticionCliente);

                //Envia los datos al cliente
                salida.println(new Gson().toJson(respuesta));
                System.out.println("Respuesta enviada: " + new Gson().toJson(respuesta));
                System.out.println("[INFO] Respuesta enviada al cliente");
            }

        //Controla posibles fallos de conexión
        } catch (IOException e) {
            System.out.println("[FAIL] Ha ocurrido un problema durante la ejecución del servidor." +
                    "\nError detectado en:\n");
            e.printStackTrace();
        }
    }

    /**
     * Crea un objeto Lector mandándole el archivo de datos a procesar, y obtiene los datos resultantes
     *
     * @throws IOException Excepción resultante de un fallo de lectura en el archivo
     */
    private void obtenerDatos() throws IOException {
        File dir = new File(".");
        Lector lector = new Lector(dir.getCanonicalPath() +
                "/src/com/apalacios/practica4/ejercicio1/servidor/carbon_dioxide_in_air.csv");
        lector.leerArchivo();
        datosCO2 = lector.getDatos();
    }

    /**
     * Determina la petición recibida del cliente y envia una respuesta en consecuencia, el dato a responder lo obtiene
     * del vector de datosCO2
     *
     * @param peticion String que representa la petición realizada por el cliente
     * @return String que representa la respuesta generada por el servidor
     */
    private Registro obtenerRespuesta(String peticion){
        Mensaje mensaje = new Gson().fromJson(peticion, Mensaje.class);
        switch (mensaje.getMsg()){
            case "AMS": //Año con mayor CO2 registrado en Suiza
                return datosCO2[0];
            case "AMG": //Año  con  mayor  CO2  registrado
                return datosCO2[1];
            case "MMS": //Media de CO2 por milenio en Suiza
                return datosCO2[2];
            case "MMG": //Media de CO2 por milenio global
                return datosCO2[3];
            case "ISS": //Aumento  de  CO2  en  este  siglo  Suiza
                return datosCO2[4];
            case "ISG": //Aumento de CO2 en este siglo global
                return datosCO2[5];
            case "I100S": //Aumento de CO2 en los últimos 100 años en Suiza
                return datosCO2[6];
            case "I100G": //Aumento de CO2 en los últimos 100 años global
                return datosCO2[7];
        }
        return null;
    }
}
package com.apalacios.practica4.ejercicio1.servidor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Clase que lee y obtiene los datos del archivo que recibe como parámetro
 */
class Lector {
    private Registro[] datosCO2;
    private String archivo;

    /**
     * Constructor de la clase instancia el vector de datosCO2
     *
     * @param archivo String que representa la ruta y el nombre del archivo a leer
     */
    Lector(String archivo) {
        datosCO2 = new Registro[8];
        this.archivo = archivo;
    }

    /**
     * Devuelve el vector de datosCO2
     *
     * @return vector de String que contiene todos los datos obtenidos del archivo
     */
    Registro[] getDatos(){
        return datosCO2;
    }

    /**
     * Ejecuta los cuatro métodos de lectura
     *
     * @throws IOException Excepción de lectura de archivo
     */
    void leerArchivo() throws IOException {
        obtenerAnyoMayorCO2();
        obtenerMediaCO2Milenio();
        obtenerAumentoCO2Siglo();
        obtenerAumentoCO2En100Anyos();
    }

    /**
     * Obtiene el resultado para las peticiones 1 y 2 de la lista (El año con mayor CO2 globalmente y en Suiza)
     *
     * @throws IOException Excepción de lectura de archivo
     */
    private void obtenerAnyoMayorCO2() throws IOException {

        //Crea el lector e ignora la primera línea
        BufferedReader br = new BufferedReader(new FileReader(archivo));
        br.readLine();

        //Crea las variables que contendrán el año con mayor nivel de CO2 y la cantidad de CO2 registrado tanto
        //globalmente como en Suiza
        double mayorValorSuiza = 0.0;
        int mayorAnyoSuiza = 0;

        double mayorValorGlobal = 0.0;
        int mayorAnyoGlobal = 0;

        //Vector que almacenara el contenido de una línea
        String[] camposLinea;

        //Bucle que lee el archivo línea a línea
        for(String linea = br.readLine(); linea != null; linea = br.readLine()){

            //Separa por comas "," los campos de la línea actual y los carga al vector camposLinea
            camposLinea = linea.split(",");

            //Si el nivel de CO2 obtenido en la línea actual del archivo, es mayor que el ya existente en la variable
            //de mayor valor, actualiza el año y el valor más altos registrados
            if (Double.parseDouble(camposLinea[3]) > mayorValorSuiza){
                mayorValorSuiza = Double.parseDouble(camposLinea[3]);
                mayorAnyoSuiza = Integer.parseInt(camposLinea[0]);
            }
            if (Double.parseDouble(camposLinea[1]) > mayorValorGlobal){
                mayorValorGlobal = Double.parseDouble(camposLinea[1]);
                mayorAnyoGlobal = Integer.parseInt(camposLinea[0]);
            }
        }

        //Carga al vector de datosCO2 el año y el nivel de CO2 más altos registrados
        datosCO2[0] = new Registro("Suiza", String.valueOf(mayorAnyoSuiza), String.valueOf(mayorValorSuiza));
        datosCO2[1] = new Registro("Global", String.valueOf(mayorAnyoGlobal), String.valueOf(mayorValorGlobal));

        //Cierra el lector del archivo
        br.close();
    }

    /**
     * Obtiene el resultado para las peticiones 3 y 4 de la lista (La media de CO2 por milenio globalmente y en Suiza)
     *
     * @throws IOException Excepción de lectura de archivo
     */
    private void obtenerMediaCO2Milenio() throws IOException {

        //Crea el lector e ignora la primera línea
        BufferedReader br = new BufferedReader(new FileReader(archivo));
        br.readLine();

        //Variables donde se sumará secuencialmente la cantidad de CO2 registrada cada milenio
        double mediaMilenioSuiza = 0.0;
        double mediaMilenioGlobal = 0.0;

        //Vector que almacenara el contenido de una línea
        String[] camposLinea;

        //Contadores para los años y los milenios transcurridos en el archivo
        int contadorAnyos = 0;

        //Bucle que lee el archivo línea a línea
        for(String linea = br.readLine(); linea != null; linea = br.readLine()){

            //Separa por comas "," los campos de la línea actual y los carga al vector camposLinea
            camposLinea = linea.split(",");

            //Suma a las variables de medias la cantidad de CO2 registrada en el año actual (línea del archivo actual)
            mediaMilenioSuiza += Double.parseDouble(camposLinea[3]);
            mediaMilenioGlobal += Double.parseDouble(camposLinea[1]);

            //Incrementa el contador de años
            contadorAnyos++;

            //Cuándo el contador de años alcanza 1000 (un milenio) concatena a los campos del vector de datosCO2 la
            //media de CO2 registrada en el milenio actual. Calcula este dato dividiendo los valores de media por el
            //contador de años (1000)
            if(contadorAnyos == 1000){

                //Concatena los resultados al vector de datosCO2
                datosCO2[2] = new Registro("Suiza", "Este milenio",
                        String.valueOf(mediaMilenioSuiza / contadorAnyos));
                datosCO2[3] = new Registro("Global", "Este milenio",
                        String.valueOf(mediaMilenioGlobal / contadorAnyos));

                //Reinicia el contador de años y las variables de medias
                contadorAnyos = 0;
                mediaMilenioSuiza = 0.0;
                mediaMilenioGlobal = 0.0;
            }
        }

        //Si el contador de años es menor a 1000 (milenio incompleto) concatena a los campos del vector de datosCO2 la
        //media de CO2 registrada en el milenio actual. Calcula este dato dividiendo los valores de media por el
        //contador de años
        if(contadorAnyos < 1000){

            //Concatena los resultados al vector de datosCO2
            datosCO2[2] = new Registro("Suiza", "Este milenio",
                    String.valueOf(mediaMilenioSuiza / contadorAnyos));
            datosCO2[3] = new Registro("Global", "Este milenio",
                    String.valueOf(mediaMilenioGlobal / contadorAnyos));
        }

        //Cierra el lector del archivo
        br.close();
    }

    /**
     * Obtiene el resultado para las peticiones 5 y 6 de la lista (El aumento de CO2 en el último siglo globalmente y
     * en Suiza)
     *
     * @throws IOException Excepción de lectura de archivo
     */
    private void obtenerAumentoCO2Siglo() throws IOException {

        //Crea el lector e ignora la primera línea
        BufferedReader br =  new BufferedReader(new FileReader(archivo));
        br.readLine();

        //Variables donde se guardará la cantidad de CO2 al inicio del siglo
        double valorInicialSuiza = 0.0;
        double valorInicialGlobal = 0.0;

        //Vector que almacenara el contenido de una línea
        String[] camposLinea = new String[3];

        //Bucle que lee el archivo línea a línea
        for(String linea = br.readLine(); linea != null; linea = br.readLine()){

            //Separa por comas "," los campos de la línea actual y los carga al vector camposLinea
            camposLinea = linea.split(",");

            //Cuándo se alcanza el año 2000 (Inicio de siglo) registra los valores de ese año en las variables de valor
            //inicial
            if(Integer.parseInt(camposLinea[0]) == 2000){
                valorInicialSuiza = Double.parseDouble(camposLinea[3]);
                valorInicialGlobal = Double.parseDouble(camposLinea[1]);
            }
        }

        //Carga al vector de datosCO2 el resultado de restar a la actual cantidad de CO2 la registrada en el año 2000
        //(El aumento de CO2 este siglo)
        datosCO2[4] = new Registro("Suiza", "Este siglo",
                String.valueOf(Double.parseDouble(camposLinea[3]) - valorInicialSuiza));
        datosCO2[5] = new Registro("Global", "Este siglo",
                String.valueOf(Double.parseDouble(camposLinea[1]) - valorInicialGlobal));

        //Cierra el lector del archivo
        br.close();
    }

    /**
     * Obtiene el resultado para las peticiones 7 y 8 de la lista (El aumento de CO2 en los últimos 100 años globalmente
     * y en Suiza)
     *
     * @throws IOException Excepción de lectura de archivo
     */
    private void obtenerAumentoCO2En100Anyos() throws IOException {

        //Crea el lector e ignora la primera línea
        BufferedReader br =  new BufferedReader(new FileReader(archivo));
        br.readLine();

        //Variables donde se guardará la cantidad de CO2 de hace 100 años
        double valorInicialSuiza = 0.0;
        double valorInicialGlobal = 0.0;

        //Vector que almacenara el contenido de una línea
        String[] camposLinea = new String[3];

        //Bucle que lee el archivo línea a línea
        for(String linea = br.readLine(); linea != null; linea = br.readLine()){

            //Separa por comas "," los campos de la línea actual y los carga al vector camposLinea
            camposLinea = linea.split(",");

            //Cuándo se alcanza el año 1919 (Hace cien años) registra los valores de ese año en las variables de valor
            //inicial
            if(Integer.parseInt(camposLinea[0]) == 1919){
                valorInicialSuiza = Double.parseDouble(camposLinea[3]);
                valorInicialGlobal = Double.parseDouble(camposLinea[1]);
            }
        }

        //Carga al vector de datosCO2 el resultado de restar a la actual cantidad de CO2 la registrada hace 100 años
        //(El aumento de CO2 en 100 años)
        datosCO2[6] = new Registro("Suiza", "100 anyos",
                String.valueOf(Double.parseDouble(camposLinea[3]) - valorInicialSuiza));
        datosCO2[7] = new Registro("Global", "100 anyos",
                String.valueOf(Double.parseDouble(camposLinea[1]) - valorInicialGlobal));

        //Cierra el lector del archivo
        br.close();
    }
}
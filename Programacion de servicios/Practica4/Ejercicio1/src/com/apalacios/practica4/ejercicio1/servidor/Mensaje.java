package com.apalacios.practica4.ejercicio1.servidor;

public class Mensaje {
    private String msg;

    public Mensaje(String msg){
        this.msg = msg;
    }

    String getMsg() {
        return msg;
    }
}

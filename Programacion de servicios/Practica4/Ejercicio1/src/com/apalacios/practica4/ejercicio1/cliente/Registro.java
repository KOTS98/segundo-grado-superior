package com.apalacios.practica4.ejercicio1.cliente;

public class Registro {
    private String ubicacion;
    private String tiempo;
    private String ppm;

    public Registro(String ubicacion, String tiempo, String ppm){
        this.ubicacion = ubicacion;
        this.tiempo = tiempo;
        this.ppm = ppm;
    }

    @Override
    public String toString(){
        String cadena = ubicacion + ", " + tiempo + ", Concentración: " + ppm + " ppm.";
        cadena = cadena.replaceAll("anyos", "años");
        return cadena;
    }
}

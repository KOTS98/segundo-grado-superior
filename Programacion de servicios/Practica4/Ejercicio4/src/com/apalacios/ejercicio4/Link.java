package com.apalacios.ejercicio4;

class Link {
    private String description;
    private String url;

    Link(String description, String url) {
        this.description = description;
        this.url = url;
    }

    String getDescription() {
        return description;
    }

    String getUrl() {
        return url;
    }

    void setDescription(String description) {
        this.description = description;
    }

    void setUrl(String url) {
        this.url = url;
    }
}

package com.apalacios.ejercicio4;

class Geometry {
    private String type;
    private Float[] coordinates;

    Geometry(String type, Float[] coordinates) {
        this.type = type;
        this.coordinates = coordinates;
    }

    String getType() {
        return type;
    }

    Float[] getCoordinates() {
        return coordinates;
    }

    void setType(String type) {
        this.type = type;
    }

    void setCoordinates(Float[] coordinates) {
        this.coordinates = coordinates;
    }
}

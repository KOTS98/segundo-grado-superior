package com.apalacios.ejercicio4;

class MuseoJSON {
    private Integer id;
    private String title;
    private String description;
    private String estilo;
    private String address;
    private String horario;
    private String phone;
    private String datacion;
    private String pois;
    private String datos;
    private String price;
    private String visita;
    private String image;
    private String top;
    private String foursquare;
    private String lastUpdated;
    private Geometry geometry;
    private Link[] links;
    private String sameAs;

    MuseoJSON(Integer id, String title, String description, String estilo, String address, String horario,
                     String phone, String datacion, String pois, String datos, String price, String visita,
                     String image, String top, String foursquare, String lastUpdated, Geometry geometry, Link[] links,
                     String sameAs) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.estilo = estilo;
        this.address = address;
        this.horario = horario;
        this.phone = phone;
        this.datacion = datacion;
        this.pois = pois;
        this.datos = datos;
        this.price = price;
        this.visita = visita;
        this.image = image;
        this.top = top;
        this.foursquare = foursquare;
        this.lastUpdated = lastUpdated;
        this.geometry = geometry;
        this.links = links;
        this.sameAs = sameAs;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getEstilo() {
        return estilo;
    }

    public String getAddress() {
        return address;
    }

    public String getHorario() {
        return horario;
    }

    public String getPhone() {
        return phone;
    }

    public String getDatacion() {
        return datacion;
    }

    public String getPois() {
        return pois;
    }

    public String getDatos() {
        return datos;
    }

    public String getPrice() {
        return price;
    }

    public String getVisita() {
        return visita;
    }

    public String getImage() {
        return image;
    }

    public String getTop() {
        return top;
    }

    public String getFoursquare() {
        return foursquare;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public Link[] getLinks() {
        return links;
    }

    public String getSameAs() {
        return sameAs;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEstilo(String estilo) {
        this.estilo = estilo;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setDatacion(String datacion) {
        this.datacion = datacion;
    }

    public void setPois(String pois) {
        this.pois = pois;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setVisita(String visita) {
        this.visita = visita;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public void setFoursquare(String foursquare) {
        this.foursquare = foursquare;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public void setLinks(Link[] links) {
        this.links = links;
    }

    public void setSameAs(String sameAs) {
        this.sameAs = sameAs;
    }
}
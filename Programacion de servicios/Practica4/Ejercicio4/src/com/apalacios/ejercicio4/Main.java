package com.apalacios.ejercicio4;

/**
 * Clase principal del programa, crea una instancia de la Vista (Interfaz gráfica).
 */
public class Main {
    public static void main(String[] args) {
        new Vista();
    }
}

package com.apalacios.ejercicio4;

import com.google.gson.Gson;
import org.jsoup.Jsoup;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Representa la interfaz gráfica del programa, se encarga de obtener datos de internet y mostrarlos en campos.
 */
public class Vista extends JFrame{
    private JPanel panelPrincipal;
    private JTextField txtNombre;
    private JTextArea txtDescripcion;
    private JTextField txtEstilo;
    private JTextField txtDatacion;
    private JTextField txtTelefono;
    private JTextField txtWeb;
    private JTextField txtDireccion;
    private JTextField txtHorario;
    private JTextArea txtOtrosDatos;
    private JTextArea txtAccesibilidad;

    /**
     * Constructor de la clase.
     */
    Vista(){
        this.setTitle("Ejercicio 4");
        this.setContentPane(panelPrincipal);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setPreferredSize(new Dimension(600, 450));
        this.setResizable(false);
        this.pack();
        this.setVisible(true);
        setLineWraps();
        try {
            mostrarDatos(obtenerDatos());
        } catch (IOException e) {
            System.out.println("[FAIL] Error al obtener datos. Compruebe la conexión a internet.");
        }
    }

    /**
     * Obliga a los textArea a realizar un salto de línea cuándo el texto llega al límite horizontal del mismo.
     */
    private void setLineWraps(){
        txtDescripcion.setLineWrap(true);
        txtOtrosDatos.setLineWrap(true);
        txtAccesibilidad.setLineWrap(true);
    }

    /**
     * Establece una conexión con un web service del ayuntamiento de Zaragoza, obtiene todos los datos en formato
     * JSON y construye un objeto MuseoJSON con dichos datos.
     *
     * @return Objeto MuseoJSON que representa un monumento.
     * @throws IOException Excepción de entrada / salida de datos.
     */
    private MuseoJSON obtenerDatos() throws IOException {
        String direccion_url = "https://www.zaragoza.es/sede/servicio/monumento/2.json";
        URL pagina = new URL(direccion_url);
        URLConnection uc = pagina.openConnection();
        uc.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
        String inputLine;
        StringBuilder contenido = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            contenido.append(inputLine).append("\n");
        }
        in.close();
        return new Gson().fromJson(contenido.toString(), MuseoJSON.class);
    }

    /**
     * Obtiene los datos de un objeto MuseoJSON y los carga a los diferentes campos de texto del programa.
     * De ser necesario, también parsea los datos eliminando las etiquetas HTML haciendo uso de la librería Jsoup.
     *
     * @param museo objeto MuseoJSON que representa a un museo.
     */
    private void mostrarDatos(MuseoJSON museo){
        txtNombre.setText(museo.getTitle());
        txtEstilo.setText(museo.getEstilo());
        txtDatacion.setText(museo.getDatacion());
        txtDescripcion.setText(museo.getDescription());
        txtAccesibilidad.setText(Jsoup.parse(museo.getVisita()).text());
        txtOtrosDatos.setText(museo.getDatos());
        txtTelefono.setText(museo.getPhone());
        txtWeb.setText(museo.getLinks()[0].getUrl());
        txtDireccion.setText(museo.getAddress());
        txtHorario.setText(museo.getHorario());
    }
}

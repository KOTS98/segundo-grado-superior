package com.apalacios.ej1.main;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        //Creación de los hilos
        Hilo hiloSi = new Hilo("Si");
        Hilo hiloNo = new Hilo("No");

        //Se inician los hilos
        hiloSi.start();
        hiloSi.join();
        hiloNo.start();
    }
}

class Hilo extends Thread{
    //Cada hilo cuenta con un bucle de 10 iteraciones, en el que muestra su nombre por terminal, el
    //resultado deseado se obtiene mediante el uso del metodo join, de forma que el segundo hilo, espera
    //a que termine la ejecución del primero antes de ejecutarse.

    public Hilo(String nombre){
        this.setName(nombre);
    }

    public void run(){
        for(int i = 0; i < 10; i++){
            System.out.println(this.getName());
        }
    }
}

package com.apalacios.ej1_2.main;

public class Main {
    public static void main(String[] args) {
        //Creación de los hilos
        Hilo hiloSi = new Hilo();
        Hilo hiloNo = new Hilo();

        //Se establece el nombre de los hilos
        hiloSi.setName("SI");
        hiloNo.setName("NO");

        //Se inician los hilos
        hiloSi.start();
        hiloNo.start();
    }
}

class Hilo extends Thread{
    //Cada hilo cuenta con un bucle de 10 iteraciones en el que muestra su nombre. Para intercalar los
    //resultados de los hilos, cada hilo se ejecuta, se duerme, y despierta al siguiente hilo.

    public void run(){
       synchronized (getClass()){
           for (int i = 0; i < 10; i++){
               System.out.println(this.getName());
               getClass().notifyAll();
               try {
                   getClass().wait();
               } catch (InterruptedException e) {
                   e.printStackTrace();
               }
               getClass().notifyAll();
           }
       }
    }
}

package com.apalacios.ejercicio3.main;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    //Método principal, se encarga de crear los hilos respectivos a cada continente, añadirlos a un ArrayList y
    //controlar el menu de opciones
    public static void main(String[] args) throws InterruptedException {

        //Variable que almacena la opción de menú seleccionada por el usuario
        int opcionMenu = 0;

        //ArrayList que almacena todos los hilos de continentes
        ArrayList<Continente> listaContinentes = new ArrayList<>();
        Continente continente1 = new Continente("Europa");
        Continente continente2 = new Continente("Asia");
        Continente continente3 = new Continente("Oceania");
        Continente continente4 = new Continente("Africa");
        Continente continente5 = new Continente("Norteamerica");
        Continente continente6 = new Continente("Sudamerica");
        listaContinentes.add(continente1);
        listaContinentes.add(continente2);
        listaContinentes.add(continente3);
        listaContinentes.add(continente4);
        listaContinentes.add(continente5);
        listaContinentes.add(continente6);

        //Bucle principal de ejecución, muestra el menú al usuario para que seleccione
        while (true){
            mostrarMenu();
            Scanner in = new Scanner(System.in);
            opcionMenu = in.nextInt();

            //Control de las diferentes funciones del programa
            switch (opcionMenu){

                //Este bucle recorre el ArrayList de hilos y los inicia secuencialmente, de una u otra forma
                //dependiendo de si ya han sido lanzados o no.
                case 1:
                    for (Continente continente : listaContinentes) {
                        if (continente.getEjecucion() == false){
                            continente.setEjecucion(true);
                            Thread.sleep(10);
                            continente.run();
                        }else{
                            continente.start();
                        }
                    }
                    break;

                //Recorre y detiene secuencialmente todos los hilos del ArrayList
                case 2:
                    for (Continente continente : listaContinentes) {
                        continente.detener();
                    }
                    break;

                //Recorre el ArrayList de hilos y establece en 0 los contadores de los hilos
                case 3:
                    for (Continente continente : listaContinentes) {
                        continente.setCantidad0(0);
                        continente.setCantidad1(0);
                    }
                    break;

                //Obtiene y muestra por pantalla el estado actual de los contadores de los hilos
                case 4:
                    for (Continente continente : listaContinentes) {
                        System.out.println(continente.getName() + " cantidad 0: " + continente.getCantidad0() +
                                "\n" + continente.getName() + " cantidad 1: " + continente.getCantidad1() +
                                "\n");
                    }
                    break;

                //Detiene la ejecución del programa
                case 5:
                    System.out.println("[INFO] Fin del programa.");
                    System.exit(0);
                    break;

                //Controla la posibilidad de que el usuario introduzca un número que no corresponde con
                //ninguna opción
                default:
                    System.out.println("[FAIL] La opcion seleccionada no existe, selecciona" +
                            "\n un valor entre 1 y 5.");
                    in.nextLine();
                    break;
            }
        }
    }

    //Este método muestra por pantalla el menú de opciones para el usuario
    private static void mostrarMenu(){
        System.out.println("CONSCIENCIA GLOBAL:\n=================" +
                "\n1. Iniciar contadores." +
                "\n2. Detener contadores." +
                "\n3. Reiniciar contadores." +
                "\n4. Ver Resultados." +
                "\n5. Salir del programa.");
    }
}

//Esta es la clase para los continentes, posee dos variables long para los contadores y un boolean, para
//controlar la ejecución del método principal del hilo
class Continente extends Thread {
    private long cantidad0 = 0;
    private long cantidad1 = 0;
    private boolean ejecucion = true;

    //Constructor de la clase, recibe un String para establecer el nombre del hilo
    public Continente(String nombre){
        this.setName(nombre);
    }

    //Getters y Setters
    public long getCantidad0() {
        return cantidad0;
    }

    public long getCantidad1() {
        return cantidad1;
    }

    public boolean getEjecucion() {
        return ejecucion;
    }

    public void setCantidad0(long cantidad0) {
        this.cantidad0 = cantidad0;
    }

    public void setCantidad1(long cantidad1) {
        this.cantidad1 = cantidad1;
    }

    public void setEjecucion(boolean ejecucion) {
        this.ejecucion = ejecucion;
    }

    //Método principal del hilo, cuenta con un bucle que se ejecuta dependiendo del estado de la variable "ejecucion" y
    //genera aleatoriamente números que pueden ser 1 o 0, con una diferencia de 10 milisegundos entre número y número
    @Override
    public void run() {
        while(ejecucion == true){
            float nAleatorio = (float) Math.random();
            if(nAleatorio < 0.5){
                this.cantidad0++;
            }else{
                this.cantidad1++;
            }
            try {
                sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //Este método detiene la ejecución del metodo principal alterando el estado de la variable "ejecucion"
    public void detener() {
        ejecucion = false;
    }
}
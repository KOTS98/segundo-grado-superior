package com.apalacios.ejercicio4.main;

import static java.lang.Thread.sleep;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        //Esta variable determina la longitud máxima del tablero:
        final int CASILLAS_TABLERO = 100;

        //Creación y lanzamiento de los hilos de la tortuga y la liebre:
        Tortuga tortuga = new Tortuga();
        Liebre liebre = new Liebre();
        tortuga.start();
        liebre.start();

        //Bucle principal, comprueba y muestra por terminal cada segundo el estado de la carrera:
        while (true){
            System.out.println("CARRERA ILEGAL DE ANIMALES:");
            System.out.println(verPosicion(tortuga, liebre));
            sleep(1000);
            limpiarTerminal();

            //En caso de que uno de los dos animales, llegue al final de la carrera, se muestra un
            //mensaje indicando el ganador y se finaliza el programa.
            if(tortuga.getPosicion() >= CASILLAS_TABLERO){
                System.out.println("LA TORTUGA GANA!");
                System.exit(0);
            }
            if (liebre.getPosicion() >= CASILLAS_TABLERO){
                System.out.println("LA LIEBRE GANA!");
                System.exit(0);
            }
        }
    }

    //Este método se encarga de generar el String que muestra la información de la carrera:
    private static String verPosicion(Tortuga tortuga, Liebre liebre){

        //Estos son los String iniciales, a los que se les concatenará más información:
        String posTortuga = "TORTUGA: [";
        String posLiebre = "LIEBRE:  [";

        //En estos bucles, se concatena a los String iniciales (posTortuga, posLiebre), una serie de
        //almohadillas representando la progresión en la carrera, junto a un cierre de corchete (]),
        //que muestra el final de la carrera:
        for (int i = 1; i <= tortuga.getPosicion()/2; i++){
            posTortuga += "#";
        }
        while(posTortuga.length() < 60){
            posTortuga += " ";
        }
        posTortuga += "]";
        for (int i = 1; i <= liebre.getPosicion()/2; i++){
            posLiebre += "#";
        }
        while(posLiebre.length() < 60){
            posLiebre += " ";
        }
        posLiebre += "]";
        return posTortuga + "\n" + posLiebre;
    }

    //Este método se encarga de limpiar la terminal añadiendo 50 líneas vacías:
    private static void limpiarTerminal(){
        for (int i = 0; i < 50; i++){
            System.out.println("");
        }
    }
}

//Este hilo representa a la tortuga en la carrera.
class Tortuga extends Thread {

    //La constante "CASILLAS_TABLERO" indica el número máximo de avance que puede tenerse en la carrera,
    //mientras que la variable "posicion", representa la posición actual de la tortuga.
    private final int CASILLAS_TABLERO = 100;
    private int posicion;

    public Tortuga() {
        this.setName("Tortuga");
        this.posicion = 1;
    }

    public int getPosicion() {
        return posicion;
    }

    //Método principal del hilo, genera números aleatorios entre 1 y 10, y dependiendo del resultado, realiza
    //una acción u otra.
    public void run(){
      int nAleatorio;
      while(posicion <= CASILLAS_TABLERO){

        // Esto genera los números aleatorios entre 0 y 9, y le suma un 1 al resultado:
        nAleatorio =  1 + (int)(Math.random() * (9));

        // Aquí se comprueba el resultado del número aleatorio, y se determina que hacer ante cada
        // resultado posible.
        if (nAleatorio >= 1 && nAleatorio <= 5){ //Avanza 3 casillas (avance rapido)
            posicion += 3;
        }else if(nAleatorio == 6 || nAleatorio == 7){ //Avanza 6 casillas (resvaló)
            posicion += 6;
        }else{ //Avanza 1 casilla (avance lento)
            posicion++;
        }

        // Después de cada resultado, el hilo se duerme durante un segundo:
         try {
             sleep(1000);
         } catch (InterruptedException e) {
             e.printStackTrace();
         }

         // Aquí se controla si se ha avanzado más aya del límite del tablero en el último turno,
         // de ser así, se establece la posición en el tablero en la última casilla del mismo y
         // se detiene la ejecución del proceso.
         if(posicion > CASILLAS_TABLERO){
             posicion = CASILLAS_TABLERO;
             try {
                 this.finalize();
             } catch (Throwable throwable) {
                 throwable.printStackTrace();
             }
         }
      }
    }
}

//Este hilo representa a la liebre en la carrera, su comportamiento es exactamente el mismo que en el anterior
//hilo (Tortuga), a excepción de los resultados del número aleatorio, que realizan diferentes operaciones y
//de un control de posición adicional, que se comenta más abajo.
class Liebre extends Thread {
    private final int CASILLAS_TABLERO = 100;
    private int posicion;

    public Liebre() {
        this.setName("Liebre");
        this.posicion = 1;
    }

    public int getPosicion() {
        return posicion;
    }

    public void run(){
        int nAleatorio;
        while(posicion <= CASILLAS_TABLERO){
            nAleatorio =  1 + (int)(Math.random() * (9));
            if (nAleatorio == 1 || nAleatorio == 2){ //No avanza (duerme 1 segundo)

            }else if(nAleatorio == 3 || nAleatorio == 4){ //Avanza 9 casillas (gran salto)
                posicion += 9;
            }else if(nAleatorio == 5){ //Retrocede 12 casillas (resvalón grande)
                posicion -= 12;
            }else if (nAleatorio >= 6 && nAleatorio <= 8){ //Avanza 1 casilla (pequeño salto)
                posicion++;
            }else{ //Retrocede 2 casillas (resvalón pequeño)
                posicion -= 2;
            }
            try {
                sleep(100); //Si el contador se reduce a 500 (medio segundo) la batalla es más ajustada.
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //Este control de posición, se encarga de evitar que la liebre retroceda a una posición menor que la
            //casilla inicial del tablero, de esta forma la liebre siempre estará como mínimo en la casilla
            //de partida.
            if (posicion < 1){
                posicion = 1;
            }
            if(posicion > CASILLAS_TABLERO){
                posicion = CASILLAS_TABLERO;
                try {
                    this.finalize();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        }
    }
}

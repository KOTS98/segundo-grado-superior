package com.apalacios.ej2.main;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        //Se obtiene el tiempo de ejecución actual
        long Tinicio = System.currentTimeMillis();

        //Se llama al método para leer el fichero, copntrolando excepciones
        try {
            leerFichero();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error de lectura.");
        }

        //Se obtiene el tiempo final de ejecución y se calcula el tiempo total
        long Tfin = System.currentTimeMillis();
        double tiempo = (double) ((Tfin - Tinicio)/1000);
        System.out.println("Tiempo de ejecución total: " + tiempo + " segundos.");
    }

    public static void leerFichero() throws IOException {
        //Creación del lector de fichero
        BufferedReader br = new BufferedReader(new FileReader("Barcelona.tcle"));

        //Creación de las variables necesarias para el cálculo
        String[] nivelBat;
        int nLineas = 0;
        Double batPromedio = Double.valueOf(0);

        //Se omiten las dos primeras líneas del fichero
        br.readLine();
        br.readLine();

        //Bucle de iteración del fichero
        for(String linea = br.readLine(); linea != null; linea = br.readLine()){
            nLineas++;

            //Se obtiene el contenido de cada línea individual, se separa cada uno de los valores y se
            //almacenan en un vector.
            nivelBat = linea.split(" ");

            //Se obtiene el elemento del vector que contiene el nivel de batería, y se suma con cada
            //iteración.
            batPromedio += Double.valueOf(nivelBat[3]);
        }
        br.close();

        //El resultado se obtiene dividiendo la suma total de la batería de cada coche entre
        //la cantidad de coches comprobados.
        System.out.println("El nivel promedio de batería es: "+batPromedio / nLineas);
    }
}

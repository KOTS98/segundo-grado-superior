package com.apalacios.ejercicio2_2.main;

import java.io.*;

public class Main {

    public static void main(String[] args) {
        //Dividir fichero en 10 partes
        try {
            dividirFichero();
        } catch (IOException e) {
            System.out.println("[FAIL] Error de lectura de archivo.");
            System.exit(0);
        }

        //Crear hilos y enviarles su fichero correspondiente
        System.out.println("[INFO] Lanzando hilos...");
        Hilo hilo0 = new Hilo("Barcelona0.tcle");
        Hilo hilo1 = new Hilo("Barcelona1.tcle");
        Hilo hilo2 = new Hilo("Barcelona2.tcle");
        Hilo hilo3 = new Hilo("Barcelona3.tcle");
        Hilo hilo4 = new Hilo("Barcelona4.tcle");
        Hilo hilo5 = new Hilo("Barcelona5.tcle");
        Hilo hilo6 = new Hilo("Barcelona6.tcle");
        Hilo hilo7 = new Hilo("Barcelona7.tcle");
        Hilo hilo8 = new Hilo("Barcelona8.tcle");
        Hilo hilo9 = new Hilo("Barcelona9.tcle");
        long Tinicio = System.currentTimeMillis();
        hilo0.start();
        hilo1.start();
        hilo2.start();
        hilo3.start();
        hilo4.start();
        hilo5.start();
        hilo6.start();
        hilo7.start();
        hilo8.start();
        hilo9.start();
        System.out.println("[INFO] Calculando...");

        //Comprobar que todos los hilos han terminado de ejecutarse antes de obtener los datos.
        while(hilo0.isAlive() || hilo1.isAlive() || hilo2.isAlive() || hilo3.isAlive() || hilo4.isAlive() ||
                hilo5.isAlive() || hilo6.isAlive() || hilo7.isAlive() || hilo8.isAlive() || hilo9.isAlive()){
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //Calcular el nivel promedio de bateria con los datos de todos los hilos.
        double batPromedio = hilo0.getBatPromedio() + hilo1.getBatPromedio() + hilo2.getBatPromedio() +
                hilo3.getBatPromedio() + hilo4.getBatPromedio() + hilo5.getBatPromedio() +
                hilo6.getBatPromedio() + hilo7.getBatPromedio() + hilo8.getBatPromedio() +
                hilo9.getBatPromedio();
        final int N_HILOS = 10;
        System.out.println("[ OK ] Cálculo finalizado! Nivel de batería promedio: " + batPromedio / N_HILOS);
        long Tfin = System.currentTimeMillis();
        double tiempo = (double) (Tfin - Tinicio);
        System.out.println("Tiempo de ejecución total: " + tiempo + " milisegundos.");
    }

    //Se divide el archivo en 10 partes de 50.000 líneas cada una, esto se hace escribiendo 10.000 lineas a la vez
    //un total de 5 veces, para evitar sobrepasar el buffer del escritor (br).
    private static void dividirFichero() throws IOException {
        System.out.println("[INFO] Dividiendo archivo...");
        final int N_ARCHIVOS = 10;
        BufferedReader br = new BufferedReader(new FileReader("Barcelona.tcle"));
        br.readLine();
        br.readLine();
        for(int i = 0; i < N_ARCHIVOS; i++){
            BufferedWriter bw = new BufferedWriter(new FileWriter("Barcelona" + i + ".tcle"));
            final int LINEAS_ITERACION = 10000;
            final int ITERACIONES_ARCHIVO = 5;
            for (int j = 0; j < ITERACIONES_ARCHIVO; j++){
                for(int k = 0; k < LINEAS_ITERACION; k++){
                    bw.write(br.readLine());
                    bw.newLine();
                }
                bw.flush();
            }
        }
        System.out.println("[ OK ] Archivo dividido!");
    }
}

class Hilo extends Thread{
    private String archivo;
    private Double batPromedio;
    Hilo(String archivo){
        this.archivo = archivo;
    }

    Double getBatPromedio() {
        return batPromedio;
    }

    public void run() {
        //Creación del lector de fichero
        try {
            BufferedReader br = new BufferedReader(new FileReader(archivo));

            //Creación de las variables necesarias para el cálculo
            String[] nivelBat;
            int nLineas = 0;
            double sumaBat = (double) 0;

            //Se omiten las dos primeras líneas del fichero
            br.readLine();
            br.readLine();

            //Bucle de iteración del fichero
            for (String linea = br.readLine(); linea != null; linea = br.readLine()) {
                nLineas++;

                //Se obtiene el contenido de cada línea individual, se separa cada uno de los valores y se
                //almacenan en un vector.
                nivelBat = linea.split(" ");

                //Se obtiene el elemento del vector que contiene el nivel de batería, y se suma con cada
                //iteración.
                sumaBat += Double.parseDouble(nivelBat[3]);
            }
            br.close();
            batPromedio = sumaBat / nLineas;
        }catch(IOException e){
            System.out.println("[FAIL] Error de lectura de archivo en hilo " + this.getName());
            System.exit(0);
        }
    }
}
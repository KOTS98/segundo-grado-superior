package apalacios.ejercicio2.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		ArrayList<Integer> numeros = new ArrayList<>();
		pedirNumeros(numeros);
		System.out.println("N�meros ordenados:");
		Collections.sort(numeros);
		mostrarNumeros(numeros);
	}

	public static void pedirNumeros(ArrayList<Integer> numeros) {
		Scanner in = new Scanner(System.in);
		int num = 0;
		for (int i = 0; i < 6; i++) {
			boolean error = false;
			do {
				System.out.println("Introduce el " + (i + 1) + "� n�mero:");
				try {
					error = false;
					num = in.nextInt();
				} catch (InputMismatchException e) {
					error = true;
					System.out.println("[ERROR] Tipo de dato introducido incorrecto. Introduce un valor num�rico\n"
							+ "sin decimales.");
					in.nextLine();
				}
			} while (error == true);
			numeros.add(num);
		}
	}

	public static void mostrarNumeros(ArrayList<Integer> numeros) {
		System.out.println("N�meros ordenados:");
		for (Integer unNumero : numeros) {
			System.out.println(unNumero);
		}
	}
}

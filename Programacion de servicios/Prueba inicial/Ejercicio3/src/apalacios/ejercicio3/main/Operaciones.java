package apalacios.ejercicio3.main;

public class Operaciones {
	public static int suma(int num1, int num2) {
		return num1 + num2;
	}

	public static int resta(int num1, int num2) {
		return num1 - num2;
	}

	public static int multiplicacion(int num1, int num2) {
		return num1 * num2;
	}

	public static float division(int num1, int num2) {
		return num1 / num2;
	}

	public static int potencia(int num1, int num2) {
		return (int) Math.pow(num1, num2);
	}
}

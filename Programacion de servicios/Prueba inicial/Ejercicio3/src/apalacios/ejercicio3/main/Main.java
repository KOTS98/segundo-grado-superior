package apalacios.ejercicio3.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		boolean error = false;
		int opcionMenu = 0;
		int num1 = 0;
		int num2 = 0;
		do {
			try {
				error = false;
				System.out.println("Introduce el primer n�mero:");
				num1 = in.nextInt();
				System.out.println("Introduce el segundo n�mero:");
				num2 = in.nextInt();
				System.out.println("�Que operaci�n deseas hacer?:\n"
						+ "1. Suma\n2. Resta\n3. Multiplicaci�n\n4. Divisi�n\n5. Potencia");
				opcionMenu = in.nextInt();
			} catch (InputMismatchException e) {
				error = true;
				System.out.println("[ERROR] Tipo de dato introducido incorrecto, introduce un valor num�rico.");
				in.nextLine();
			}
		} while (error == true);
		switch (opcionMenu) {
		case 1:
			System.out.println("Resultado: " + Operaciones.suma(num1, num2));
			break;
		case 2:
			System.out.println("Resultado: " + Operaciones.resta(num1, num2));
			break;
		case 3:
			System.out.println("Resultado: " + Operaciones.multiplicacion(num1, num2));
			break;
		case 4:
			System.out.println("Resultado: " + Operaciones.division(num1, num2));
			break;
		case 5:
			System.out.println("Resultado: " + Operaciones.potencia(num1, num2));
			break;
		default:
			System.out.println("[ERROR] La opcion seleccionada no existe.");
			System.exit(0);
		}
	}
}
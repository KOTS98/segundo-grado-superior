package apalacios.ejercicio1.main;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int b500 = 0;
		int b200 = 0;
		int b100 = 0;
		int b50 = 0;
		int b20 = 0;
		int b10 = 0;
		int b5 = 0;
		int m2 = 0;
		int m1 = 0;
		int c50 = 0;
		int c20 = 0;
		int c10 = 0;
		int c5 = 0;
		int c2 = 0;
		int c1 = 0;
		float dinero = 0;
		boolean error = false;
		do {
			error = false;
			System.out.println("�Cu�nto dinero deseas contar?: ");
			try {
				dinero = in.nextFloat();
			} catch (InputMismatchException e) {
				System.out.println("[ERROR] Tipo de dato introducido incorrecto. Debes introducir\n"
						+ "un valor n�merico con hasta dos decimales. Los decimales se separar�n con una coma.\n"
						+ "Ejemplo: 367,53");
				in.nextLine();
				error = true;
			}
		} while (error == true);

		while (dinero >= 500) {
			dinero -= 500;
			b500++;
		}
		if (b500 > 0) {
			System.out.println(b500 + " Billetes de 500�");
		}
		while (dinero >= 200) {
			dinero -= 200;
			b200++;
		}
		if (b200 > 0) {
			System.out.println(b200 + " Billetes de 200�");
		}
		while (dinero >= 100) {
			dinero -= 100;
			b100++;
		}
		if (b100 > 0) {
			System.out.println(b100 + " Billetes de 100�");
		}
		while (dinero >= 50) {
			dinero -= 50;
			b50++;
		}
		if (b50 > 0) {
			System.out.println(b50 + " Billetes de 50�");
		}
		while (dinero >= 20) {
			dinero -= 20;
			b20++;
		}
		if (b20 > 0) {
			System.out.println(b20 + " Billetes de 20�");
		}
		while (dinero >= 10) {
			dinero -= 10;
			b10++;
		}
		if (b10 > 0) {
			System.out.println(b10 + " Billetes de 10�");
		}
		while (dinero >= 5) {
			dinero -= 5;
			b5++;
		}
		if (b5 > 0) {
			System.out.println(b5 + " Billetes de 5�");
		}
		while (dinero >= 2) {
			dinero -= 2;
			m2++;
		}
		if (m2 > 0) {
			System.out.println(m2 + " Monedas de 2�");
		}
		while (dinero >= 1) {
			dinero -= 1;
			m1++;
		}
		if (m1 > 0) {
			System.out.println(m1 + " Monedas de 1�");
		}
		while (dinero >= 0.50) {
			dinero -= 0.50;
			c50++;
		}
		if (c50 > 0) {
			System.out.println(c50 + " Monedas de 50 cent");
		}
		while (dinero >= 0.20) {
			dinero -= 0.20;
			c20++;
		}
		if (c20 > 0) {
			System.out.println(c20 + " Monedas de 20 cent");
		}
		while (dinero >= 0.10) {
			dinero -= 0.10;
			c10++;
		}
		if (c10 > 0) {
			System.out.println(c10 + " Monedas de 10 cent");
		}
		while (dinero >= 0.05) {
			dinero -= 0.05;
			c5++;
		}
		if (c5 > 0) {
			System.out.println(c5 + " Monedas de 5 cent");
		}
		while (dinero >= 0.02) {
			dinero -= 0.02;
			c2++;
		}
		if (c2 > 0) {
			System.out.println(c2 + " Monedas de 2 cent");
		}
		while (dinero >= 0.001) {
			dinero -= 0.01;
			c1++;
		}
		if (c1 > 0) {
			System.out.println(c1 + " Monedas de 1 cent");
		}
	}
}

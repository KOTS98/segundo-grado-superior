CREATE DATABASE if not exists barManolo;
USE barManolo;

CREATE TABLE producto(
id_producto int primary key auto_increment,
nombre varchar(50),
precio int unsigned,
grados int
);

CREATE TABLE oferta(
id_oferta int primary key auto_increment,
nombre varchar(50),
descripcion varchar(50),
porcentajeDescuento int unsigned,
id_empleado int
);

CREATE TABLE empleado(
id_empleado int primary key auto_increment,
nombre varchar(50),
apellidos varchar(100),
dni varchar(10),
fechaIncorporacion date,
salario int unsigned,
id_departamento int
);

CREATE TABLE departamento(
id_departamento int primary key auto_increment,
departamento varchar(50)
);

CREATE TABLE cliente(
id_cliente int primary key auto_increment,
nick varchar(50),
fechaNacimiento date,
id_categoria int,
id_oferta int
);

CREATE TABLE categoria(
id_categoria int primary key auto_increment,
categoria varchar(50),
nivel int unsigned
);

CREATE TABLE productos_ofertas(
id_producto int,
id_oferta int,
primary key(id_producto, id_oferta)
);

ALTER TABLE empleado
add foreign key (id_departamento) references departamento(id_departamento);

ALTER TABLE cliente
add foreign key (id_categoria) references categoria(id_categoria),
add foreign key (id_oferta) references oferta(id_oferta);

ALTER TABLE oferta
add foreign key (id_empleado) references empleado(id_empleado);

ALTER TABLE productos_ofertas
add foreign key (id_producto) references producto(id_producto),
add foreign key (id_oferta) references oferta(id_oferta);
package apalacios.ejerciciocursos.cursos;

public class Curso {
	private String nombre;
	private int nivel;
	private String profesor;

	public Curso(String nombre, int nivel, String profesor) {
		this.nombre = nombre;
		this.nivel = nivel;
		this.profesor = profesor;
	}

	public Curso() {
		this.nombre = "";
		this.nivel = 0;
		this.profesor = "";
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNivel() {
		return nivel;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public String getProfesor() {
		return profesor;
	}

	public void setProfesor(String profesor) {
		this.profesor = profesor;
	}

	@Override
	public String toString() {
		return nombre + " - " + nivel + ": " + profesor;
	}
}

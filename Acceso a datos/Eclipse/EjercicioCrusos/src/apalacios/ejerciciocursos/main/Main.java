package apalacios.ejerciciocursos.main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import apalacios.ejerciciocursos.cursos.Curso;

public class Main {

	public static void main(String[] args) {
		ArrayList<Curso> listaCursos = new ArrayList<Curso>();

		Curso curso1 = new Curso("DAM", 1, "Fernando");
		listaCursos.add(curso1);
		Curso curso2 = new Curso("DAW", 2, "Monica");
		listaCursos.add(curso2);
		Curso curso3 = new Curso("ASIR", 3, "Javier");
		listaCursos.add(curso3);

		for (Curso curso : listaCursos) {
			System.out.println(curso.toString());
		}
		try {
			RandomAccessFile raf = new RandomAccessFile("archivo.txt", "rw");
			raf.seek(0);
			String datos = "";
			for (Curso curso : listaCursos) {
				datos += curso.toString() + "\n";
			}
			raf.writeUTF(datos);
			raf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
package fichero_conf;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Properties;

public class Ventana {
    private JTextField txtHost;
    private JCheckBox cbAnonimo;
    private JRadioButton rbtnMysql;
    private JRadioButton rbtnPostgresql;
    private JTextField txtNClientes;
    private JButton btnGuardar;
    private JLabel lblHost;
    private JLabel lblAnonimo;
    private JLabel lblServidor;
    private JLabel lblNClientes;
    private JLabel lblGuardado;
    private JPanel panel;

    public Ventana() {
        cargarConfiguracion();
        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Properties conf = new Properties();
                conf.setProperty("host", txtHost.getText());
                conf.setProperty("anon", String.valueOf(cbAnonimo.isSelected()));
                if(rbtnMysql.isSelected()){
                    conf.setProperty("srvr", rbtnMysql.getText());
                }else{
                    conf.setProperty("srvr", rbtnPostgresql.getText());
                }
                conf.setProperty("ncli", txtNClientes.getText().toString());
                try {
                    conf.store(new FileWriter("src/fichero_conf/conf.properties"), "");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(new Ventana().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public  void cargarConfiguracion(){
        Properties conf = new Properties();
        try {
            conf.load(new FileReader("src/fichero_conf/conf.properties"));
            txtHost.setText(conf.getProperty("host"));
            if(conf.getProperty("anon").equalsIgnoreCase("true")){
                cbAnonimo.setSelected(true);
            }
            if(conf.getProperty("srvr").equalsIgnoreCase("mySQL")){
                rbtnMysql.setSelected(true);
            }else{
                rbtnPostgresql.setSelected(true);
            }
            txtNClientes.setText(conf.getProperty("ncli"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package Ejercicio1;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Ventana {
    private JTextField txtIP;
    private JTextField txtPuerto;
    private JTextField txtPass;
    private JButton btnGuardar;
    private JButton btnCargar;
    private JLabel lblIP;
    private JLabel lblPuerto;
    private JLabel lblPass;
    private JPanel panel;

    public Ventana() {
        btnGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Properties conf = new Properties();
                conf.setProperty("ip", txtIP.getText());
                conf.setProperty("puerto", txtPuerto.getText());
                conf.setProperty("pass", txtPass.getText());
                try {
                    conf.store(new FileWriter("src/Ejercicio1/conf.properties"), "");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnCargar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Properties conf = new Properties();
                try {
                    conf.load(new FileReader("src/Ejercicio1/conf.properties"));
                    txtIP.setText(conf.getProperty("ip"));
                    txtPuerto.setText(conf.getProperty("puerto"));
                    txtPass.setText(conf.getProperty("pass"));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(new Ventana().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

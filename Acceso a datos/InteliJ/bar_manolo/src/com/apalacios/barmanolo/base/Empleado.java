package com.apalacios.barmanolo.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Empleado {
    private int idEmpleado;
    private String nombre;
    private String apellidos;
    private String dni;
    private Date fechaIncorporacion;
    private int salario;
    private Departamento departamento;
    private List<Oferta> ofertas;

    @Id
    @Column(name = "id_empleado")
    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "fechaIncorporacion")
    public Date getFechaIncorporacion() {
        return fechaIncorporacion;
    }

    public void setFechaIncorporacion(Date fechaIncorporacion) {
        this.fechaIncorporacion = fechaIncorporacion;
    }

    @Basic
    @Column(name = "salario")
    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleado empleado = (Empleado) o;
        return idEmpleado == empleado.idEmpleado &&
                salario == empleado.salario &&
                Objects.equals(nombre, empleado.nombre) &&
                Objects.equals(apellidos, empleado.apellidos) &&
                Objects.equals(dni, empleado.dni) &&
                Objects.equals(fechaIncorporacion, empleado.fechaIncorporacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEmpleado, nombre, apellidos, dni, fechaIncorporacion, salario);
    }

    @ManyToOne
    @JoinColumn(name = "id_departamento", referencedColumnName = "id_departamento")
    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @OneToMany(mappedBy = "empleado")
    public List<Oferta> getOfertas() {
        return ofertas;
    }

    public void setOfertas(List<Oferta> ofertas) {
        this.ofertas = ofertas;
    }

    @Override
    public String toString() {
        if (departamento != null)
            return nombre + " " + apellidos + "   -   " + dni + "   -   " + departamento.getDepartamento();
        else
            return nombre + " " + apellidos + "   -   " + dni;
    }
}

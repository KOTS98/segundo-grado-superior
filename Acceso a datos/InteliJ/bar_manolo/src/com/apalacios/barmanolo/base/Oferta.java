package com.apalacios.barmanolo.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Oferta {
    private int idOferta;
    private String nombre;
    private String descripcion;
    private int porcentajeDescuento;
    private List<Cliente> clientes;
    private Empleado empleado;
    private List<Producto> productos;

    @Id
    @Column(name = "id_oferta")
    public int getIdOferta() {
        return idOferta;
    }

    public void setIdOferta(int idOferta) {
        this.idOferta = idOferta;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "porcentajeDescuento")
    public int getPorcentajeDescuento() {
        return porcentajeDescuento;
    }

    public void setPorcentajeDescuento(int porcentajeDescuento) {
        this.porcentajeDescuento = porcentajeDescuento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Oferta oferta = (Oferta) o;
        return idOferta == oferta.idOferta &&
                porcentajeDescuento == oferta.porcentajeDescuento &&
                Objects.equals(nombre, oferta.nombre) &&
                Objects.equals(descripcion, oferta.descripcion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idOferta, nombre, descripcion, porcentajeDescuento);
    }

    @OneToMany(mappedBy = "oferta")
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    @ManyToOne
    @JoinColumn(name = "id_empleado", referencedColumnName = "id_empleado")
    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @ManyToMany
    @JoinTable(name = "productos_ofertas", catalog = "", schema = "barmanolo", joinColumns =
    @JoinColumn(name = "id_producto", referencedColumnName = "id_oferta", nullable = false), inverseJoinColumns =
    @JoinColumn(name = "id_oferta", referencedColumnName = "id_producto", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        if (empleado != null)
            return nombre + "   -   Descuento " + porcentajeDescuento + "%   -   Creada por " + empleado.getNombre();
        else
            return nombre + "   -   Descuento " + porcentajeDescuento + "%";
    }
}

package com.apalacios.barmanolo.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Departamento {
    private int idDepartamento;
    private String departamento;
    private List<Empleado> empleados;

    @Id
    @Column(name = "id_departamento")
    public int getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    @Basic
    @Column(name = "departamento")
    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Departamento that = (Departamento) o;
        return idDepartamento == that.idDepartamento &&
                Objects.equals(departamento, that.departamento);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDepartamento, departamento);
    }

    @OneToMany(mappedBy = "departamento")
    public List<Empleado> getEmpleados() {
        return empleados;
    }

    public void setEmpleados(List<Empleado> empleados) {
        this.empleados = empleados;
    }

    @Override
    public String toString() {
        return departamento;
    }
}

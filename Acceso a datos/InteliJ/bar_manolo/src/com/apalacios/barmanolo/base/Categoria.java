package com.apalacios.barmanolo.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Categoria {
    private int idCategoria;
    private String categoria;
    private int nivel;
    private List<Cliente> clientes;

    @Id
    @Column(name = "id_categoria")
    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Basic
    @Column(name = "categoria")
    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Basic
    @Column(name = "nivel")
    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria1 = (Categoria) o;
        return idCategoria == categoria1.idCategoria &&
                nivel == categoria1.nivel &&
                Objects.equals(categoria, categoria1.categoria);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCategoria, categoria, nivel);
    }

    @OneToMany(mappedBy = "categoria")
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    @Override
    public String toString() {
        return categoria + "   -   Nivel " + nivel;
    }
}

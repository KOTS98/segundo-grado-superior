package com.apalacios.barmanolo.main;

import com.apalacios.barmanolo.gui.Controlador;
import com.apalacios.barmanolo.gui.Modelo;
import com.apalacios.barmanolo.gui.Vista;

public class Main {

    public static void main(final String[] args) {
        new Controlador(new Modelo(), new Vista());
    }
}
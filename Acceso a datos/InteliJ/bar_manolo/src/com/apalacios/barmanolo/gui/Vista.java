package com.apalacios.barmanolo.gui;

import com.apalacios.barmanolo.base.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame {

    // Ventana
    private JPanel panelPrincipal;

    // Productos
    JTextField txtNombreProducto;
    JTextField txtPrecioProducto;
    JTextField txtGradosBebida;

    JList<Producto> listProductos;

    JButton btnDelProducto;
    JButton btnAddProducto;
    JButton btnModProducto;

    // Ofertas
    JTextField txtNombreOferta;
    JTextField txtDescOferta;
    JTextField txtDescuentoOferta;
    JComboBox<Empleado> cBoxEmpleadoOferta;

    JList<Oferta> listOfertas;
    JList<Cliente> listClientesOferta;
    JList<Oferta> listOfertasProducto;
    JList<Producto> listProductosOferta;

    JButton btnAddOferta;
    JButton btnDelOferta;
    JButton btnModOferta;

    // Empleados
    JTextField txtNombreEmpleado;
    JTextField txtApellidosEmpleado;
    JTextField txtDniEmpleado;
    JTextField txtSalarioEmpleado;
    DatePicker datePickerFechaEmpleado;
    JComboBox<Departamento> cBoxDepartamentoEmpleado;

    JList<Empleado> listEmpleados;
    JList<Oferta> listOfertasEmpleado;

    JButton btnAddEmpleado;
    JButton btnModEmpleado;
    JButton btnDelEmpleado;

    // Departamentos
    JTextField txtNombreDepartamento;

    JList<Departamento> listDepartamentos;
    JList<Empleado> listEmpleadosDepartamento;

    JButton btnAddDepartamento;
    JButton btnModDepartamento;
    JButton btnDelDepartamento;

    // Clientes
    JTextField txtNickCliente;
    DatePicker datePickerNacimientoCliente;
    JComboBox<Categoria> cBoxCategoriaCliente;
    JComboBox<Oferta> cBoxOfertaCliente;

    JList<Cliente> listClientes;

    JButton btnAddCliente;
    JButton btnModCliente;
    JButton btnDelCliente;

    // Categorías
    JTextField txtNombreCategoria;
    JTextField txtNivelCategoria;

    JList<Categoria> listCategorias;
    JList<Cliente> listClientesCategoria;

    JButton btnAddCategoria;
    JButton btnModCategoria;
    JButton btnDelCategoria;

    // Modelos
    DefaultListModel<Producto> dlmProductos;
    DefaultListModel<Oferta> dlmOfertasProducto;

    DefaultListModel<Oferta> dlmOfertas;
    DefaultListModel<Cliente> dlmClientesOferta;
    DefaultListModel<Producto> dlmProductosOferta;

    DefaultListModel<Empleado> dlmEmpleados;
    DefaultListModel<Oferta> dlmOfertasEmpleado;

    DefaultListModel<Departamento> dlmDepartamento;
    DefaultListModel<Empleado> dlmEmpleadosDepartamento;

    DefaultListModel<Cliente> dlmClientes;

    DefaultListModel<Categoria> dlmCategorias;
    DefaultListModel<Cliente> dlmClientesCategoria;

    DefaultComboBoxModel<Empleado> dcbmEmpleadoOferta;

    DefaultComboBoxModel<Departamento> dcbmDepartamentoEmpleado;

    DefaultComboBoxModel<Categoria> dcbmCategoriaCliente;
    DefaultComboBoxModel<Oferta> dcbmOfertaCliente;

    // Menú
    JMenuItem mItemConexion;
    JMenuItem mItemSalir;
    JMenuItem mItemRegistro;

    public Vista() {
        setTitle("Bar Manolo         <SIN CONEXIÓN>");
        setContentPane(panelPrincipal);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(800, 600));
        setResizable(false);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);

        crearMenu();
        crearModelos();
    }

    @SuppressWarnings("DuplicatedCode")
    private void crearModelos() {
        dlmProductos = new DefaultListModel<>();
        listProductos.setModel(dlmProductos);
        dlmOfertasProducto = new DefaultListModel<>();
        listOfertasProducto.setModel(dlmOfertasProducto);

        dlmOfertas = new DefaultListModel<>();
        listOfertas.setModel(dlmOfertas);
        dlmClientesOferta = new DefaultListModel<>();
        listClientesOferta.setModel(dlmClientesOferta);
        dlmProductosOferta = new DefaultListModel<>();
        listProductosOferta.setModel(dlmProductosOferta);

        dlmEmpleados = new DefaultListModel<>();
        listEmpleados.setModel(dlmEmpleados);
        dlmOfertasEmpleado = new DefaultListModel<>();
        listOfertasEmpleado.setModel(dlmOfertasEmpleado);

        dlmDepartamento = new DefaultListModel<>();
        listDepartamentos.setModel(dlmDepartamento);
        dlmEmpleadosDepartamento = new DefaultListModel<>();
        listEmpleadosDepartamento.setModel(dlmEmpleadosDepartamento);

        dlmClientes = new DefaultListModel<>();
        listClientes.setModel(dlmClientes);

        dlmCategorias = new DefaultListModel<>();
        listCategorias.setModel(dlmCategorias);
        dlmClientesCategoria = new DefaultListModel<>();
        listClientesCategoria.setModel(dlmClientesCategoria);

        dcbmEmpleadoOferta = new DefaultComboBoxModel<>();
        cBoxEmpleadoOferta.setModel(dcbmEmpleadoOferta);

        dcbmDepartamentoEmpleado = new DefaultComboBoxModel<>();
        cBoxDepartamentoEmpleado.setModel(dcbmDepartamentoEmpleado);

        dcbmCategoriaCliente = new DefaultComboBoxModel<>();
        cBoxCategoriaCliente.setModel(dcbmCategoriaCliente);
        dcbmOfertaCliente = new DefaultComboBoxModel<>();
        cBoxOfertaCliente.setModel(dcbmOfertaCliente);
    }

    private void crearMenu() {
        JMenuBar barraMenu = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenu mRegistro = new JMenu("Registro");

        mItemConexion = new JMenuItem("Conectar");
        mItemConexion.setActionCommand("conexion");

        mItemSalir = new JMenuItem("Salir");
        mItemSalir.setActionCommand("salir");

        mItemRegistro = new JMenuItem("Ver registro");
        mItemRegistro.setActionCommand("registro");

        menu.add(mItemConexion);
        menu.add(mItemSalir);
        mRegistro.add(mItemRegistro);
        barraMenu.add(menu);
        barraMenu.add(mRegistro);

        setJMenuBar(barraMenu);
    }
}

package com.apalacios.barmanolo.gui;

import com.apalacios.barmanolo.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {
    private SessionFactory sessionFactory;
    private Session sesion;
    private boolean conectado;

    public Modelo() {
        conectado = false;
    }

    boolean getConectado() {
        return conectado;
    }

    void conectar() {
        try {
            Configuration configuracion = new Configuration();
            configuracion.configure("hibernate.cfg.xml");

            configuracion.addAnnotatedClass(Producto.class);
            configuracion.addAnnotatedClass(Oferta.class);
            configuracion.addAnnotatedClass(Empleado.class);
            configuracion.addAnnotatedClass(Departamento.class);
            configuracion.addAnnotatedClass(Cliente.class);
            configuracion.addAnnotatedClass(Categoria.class);

            StandardServiceRegistry ssr = new StandardServiceRegistryBuilder()
                    .applySettings(configuracion.getProperties()).build();

            sessionFactory = configuracion.buildSessionFactory(ssr);

            conectado = true;
        } catch (Exception ex) {
            // Mostrar error
        }
    }

    void desconectar() {
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
        conectado = false;
    }

    ArrayList<?> getObjeto(Object objeto) {
        sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + objeto.getClass().getSimpleName());
        ArrayList<?> lista = (ArrayList<?>) query.getResultList();
        sesion.close();
        return lista;
    }

    ArrayList<?> getObjeto(Object objeto, Object comparador) {
        sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM " + objeto.getClass().getSimpleName() +
                " WHERE " + comparador.getClass().getSimpleName().toLowerCase() + "=:obj");

        query.setParameter("obj", comparador);
        ArrayList<?> lista = (ArrayList<?>) query.getResultList();
        sesion.close();
        return lista;
    }

    void altaObjeto(Object obj) {
        sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(obj);
        sesion.getTransaction().commit();

        sesion.close();
    }

    void modificarObjeto(Object obj) {
        sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(obj);
        sesion.getTransaction().commit();

        sesion.close();
    }

    void eliminarObjeto(Object obj) {
        sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.delete(obj);
        sesion.getTransaction().commit();

        sesion.close();
    }
}

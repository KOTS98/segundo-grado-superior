package com.apalacios.barmanolo.gui;

import com.apalacios.barmanolo.base.*;
import com.apalacios.barmanolo.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;

public class Controlador implements ActionListener, ListSelectionListener {

    private Modelo modelo;
    private Vista vista;
    private RegistroDialog registro;

    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        this.registro = new RegistroDialog();

        addActionListeners(this);
        addListSelectionListeners(this);
    }

    private void addActionListeners(ActionListener listener) {

        //Menu
        vista.mItemConexion.addActionListener(listener);
        vista.mItemSalir.addActionListener(listener);
        vista.mItemRegistro.addActionListener(listener);

        // Botones
        vista.btnDelProducto.addActionListener(listener);
        vista.btnAddProducto.addActionListener(listener);
        vista.btnModProducto.addActionListener(listener);
        vista.btnAddOferta.addActionListener(listener);
        vista.btnDelOferta.addActionListener(listener);
        vista.btnModOferta.addActionListener(listener);
        vista.btnAddEmpleado.addActionListener(listener);
        vista.btnModEmpleado.addActionListener(listener);
        vista.btnDelEmpleado.addActionListener(listener);
        vista.btnAddDepartamento.addActionListener(listener);
        vista.btnModDepartamento.addActionListener(listener);
        vista.btnDelDepartamento.addActionListener(listener);
        vista.btnAddCliente.addActionListener(listener);
        vista.btnModCliente.addActionListener(listener);
        vista.btnDelCliente.addActionListener(listener);
        vista.btnAddCategoria.addActionListener(listener);
        vista.btnModCategoria.addActionListener(listener);
        vista.btnDelCategoria.addActionListener(listener);
    }

    private void addListSelectionListeners(ListSelectionListener listener) {
        vista.listProductos.addListSelectionListener(listener);
        vista.listOfertasProducto.addListSelectionListener(listener);
        vista.listOfertas.addListSelectionListener(listener);
        vista.listClientesOferta.addListSelectionListener(listener);
        vista.listProductosOferta.addListSelectionListener(listener);
        vista.listEmpleados.addListSelectionListener(listener);
        vista.listOfertasEmpleado.addListSelectionListener(listener);
        vista.listDepartamentos.addListSelectionListener(listener);
        vista.listEmpleadosDepartamento.addListSelectionListener(listener);
        vista.listClientes.addListSelectionListener(listener);
        vista.listCategorias.addListSelectionListener(listener);
        vista.listClientesCategoria.addListSelectionListener(listener);
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            switch (e.getActionCommand()) {

                // Menu
                case "conexion":
                    if (modelo.getConectado()) {
                        registro.addRegistro(RegistroDialog.TipoRegistro.INFO, "Cerrando conexión...");
                        modelo.desconectar();
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK, "Conexión finalizada!");
                        botonesActivos(false);
                        vista.setTitle("Bar Manolo         <SIN CONEXIÓN>");
                        vista.mItemConexion.setText("Conectar");
                    } else {
                        registro.addRegistro(RegistroDialog.TipoRegistro.INFO, "Iniciando conexión...");
                        modelo.conectar();
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK, "Conexión establecida!");
                        registro.addRegistro(RegistroDialog.TipoRegistro.INFO, "Actualizando tablas...");
                        listarProductos((ArrayList<Producto>) modelo.getObjeto(new Producto()));
                        listarOfertas((ArrayList<Oferta>) modelo.getObjeto(new Oferta()));
                        listarEmpleados((ArrayList<Empleado>) modelo.getObjeto(new Empleado()));
                        listarDepartamentos((ArrayList<Departamento>) modelo.getObjeto(new Departamento()));
                        listarClientes((ArrayList<Cliente>) modelo.getObjeto(new Cliente()));
                        listarCategorias((ArrayList<Categoria>) modelo.getObjeto(new Categoria()));
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK, "Tablas cargadas!");
                        botonesActivos(true);
                        vista.setTitle("Bar Manolo         <  CONECTADO >");
                        vista.mItemConexion.setText("Desconectar");
                    }
                    break;

                case "salir":
                    registro.addRegistro(RegistroDialog.TipoRegistro.INFO, "Cerrando conexión...");
                    modelo.desconectar();
                    registro.addRegistro(RegistroDialog.TipoRegistro.OK, "Conexión finalizada!");
                    System.exit(0);
                    break;

                case "registro":
                    registro.setVisibleDialog(true);
                    break;

                // Productos
                case "addProducto": {
                    if (comprobarProducto()) {
                        Producto producto = new Producto();
                        producto.setNombre(vista.txtNombreProducto.getText());
                        producto.setPrecio(Integer.parseInt(vista.txtPrecioProducto.getText()));
                        producto.setGrados(Integer.parseInt(vista.txtGradosBebida.getText()));
                        modelo.altaObjeto(producto);
                        listarProductos((ArrayList<Producto>) modelo.getObjeto(new Producto()));
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                "Elemento añadido a 'productos' bajo ID# " +
                                        vista.dlmProductos.getElementAt(vista.dlmProductos.getSize() - 1).getIdProducto());
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;
                }

                case "modProducto": {
                    if (comprobarProducto()) {
                        if (vista.listProductos.getSelectedIndex() != -1) {
                            Producto producto = vista.listProductos.getSelectedValue();
                            producto.setNombre(vista.txtNombreProducto.getText());
                            producto.setPrecio(Integer.parseInt(vista.txtPrecioProducto.getText()));
                            producto.setGrados(Integer.parseInt(vista.txtGradosBebida.getText()));
                            modelo.modificarObjeto(producto);registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento modificado en 'productos' bajo ID# " +
                                            vista.listProductos.getSelectedValue().getIdProducto());
                            listarProductos((ArrayList<Producto>) modelo.getObjeto(new Producto()));
                        } else {
                            Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                        }
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;
                }

                case "delProducto": {
                    if (vista.listProductos.getSelectedValue() != null) {
                        try {
                            modelo.eliminarObjeto(vista.listProductos.getSelectedValue());
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento eliminado en 'productos' bajo ID# " +
                                            vista.listProductos.getSelectedValue().getIdProducto());
                            listarProductos((ArrayList<Producto>) modelo.getObjeto(new Producto()));
                        } catch (Exception ex) {
                            Util.mostrarMensajeError("Otros elementos dependen del elemento seleccionado." +
                                    "\nCompruebe las dependencias antes de eliminar un elemento.");
                        }
                    } else {
                        Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                    }
                    break;
                }

                // Ofertas
                case "addOferta":
                    if (comprobarOferta()) {
                        Oferta oferta = new Oferta();
                        oferta.setNombre(vista.txtNombreOferta.getText());
                        oferta.setDescripcion(vista.txtDescOferta.getText());
                        oferta.setPorcentajeDescuento(Integer.parseInt(vista.txtDescuentoOferta.getText()));
                        if (vista.cBoxEmpleadoOferta.getSelectedIndex() != -1)
                            oferta.setEmpleado((Empleado) vista.cBoxEmpleadoOferta.getSelectedItem());
                        modelo.altaObjeto(oferta);
                        listarOfertas((ArrayList<Oferta>) modelo.getObjeto(new Oferta()));
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                "Elemento añadido a 'ofertas' bajo ID# " +
                                        vista.dlmOfertas.getElementAt(vista.dlmOfertas.getSize() - 1).getIdOferta());
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "modOferta":
                    if (comprobarOferta()) {
                        if (vista.listOfertas.getSelectedIndex() != -1) {
                            Oferta oferta = vista.listOfertas.getSelectedValue();
                            oferta.setNombre(vista.txtNombreOferta.getText());
                            oferta.setDescripcion(vista.txtDescOferta.getText());
                            oferta.setPorcentajeDescuento(Integer.parseInt(vista.txtDescuentoOferta.getText()));
                            if (vista.cBoxEmpleadoOferta.getSelectedIndex() != -1)
                                oferta.setEmpleado((Empleado) vista.cBoxEmpleadoOferta.getSelectedItem());
                            modelo.modificarObjeto(oferta);
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento modificado en 'ofertas' bajo ID# " +
                                            vista.listOfertas.getSelectedValue().getIdOferta());
                            listarOfertas((ArrayList<Oferta>) modelo.getObjeto(new Oferta()));
                        } else {
                            Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                        }
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "delOferta":
                    if (vista.listOfertas.getSelectedIndex() != -1) {
                        try {
                            modelo.eliminarObjeto(vista.listOfertas.getSelectedValue());
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento eliminado en 'ofertas' bajo ID# " +
                                            vista.listOfertas.getSelectedValue().getIdOferta());
                            listarOfertas((ArrayList<Oferta>) modelo.getObjeto(new Oferta()));
                        } catch (Exception ex) {
                            Util.mostrarMensajeError("Otros elementos dependen del elemento seleccionado." +
                                    "\nCompruebe las dependencias antes de eliminar un elemento.");
                        }
                    } else {
                        Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                    }
                    break;

                // Empleados
                case "addEmpleado":
                    if (comprobarEmpleado()) {
                        Empleado empleado = new Empleado();
                        empleado.setNombre(vista.txtNombreEmpleado.getText());
                        empleado.setApellidos(vista.txtApellidosEmpleado.getText());
                        empleado.setDni(vista.txtDniEmpleado.getText());
                        empleado.setFechaIncorporacion(Date.valueOf(vista.datePickerFechaEmpleado.getDate()));
                        empleado.setSalario(Integer.parseInt(vista.txtSalarioEmpleado.getText()));
                        if (vista.cBoxDepartamentoEmpleado.getSelectedIndex() != -1)
                            empleado.setDepartamento((Departamento) vista.cBoxDepartamentoEmpleado.getSelectedItem());
                        modelo.altaObjeto(empleado);
                        listarEmpleados((ArrayList<Empleado>) modelo.getObjeto(new Empleado()));
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                "Elemento añadido a 'empleados' bajo ID# " +
                                        vista.dlmEmpleados.getElementAt(vista.dlmEmpleados.getSize() - 1).getIdEmpleado());
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "modEmpleado":
                    if (comprobarEmpleado()) {
                        if (vista.listEmpleados.getSelectedIndex() != -1) {
                            Empleado empleado = vista.listEmpleados.getSelectedValue();
                            empleado.setNombre(vista.txtNombreEmpleado.getText());
                            empleado.setApellidos(vista.txtApellidosEmpleado.getText());
                            empleado.setDni(vista.txtDniEmpleado.getText());
                            empleado.setFechaIncorporacion(Date.valueOf(vista.datePickerFechaEmpleado.getDate()));
                            empleado.setSalario(Integer.parseInt(vista.txtSalarioEmpleado.getText()));
                            if (vista.cBoxDepartamentoEmpleado.getSelectedIndex() != -1)
                                empleado.setDepartamento((Departamento) vista.cBoxDepartamentoEmpleado.getSelectedItem());
                            modelo.modificarObjeto(empleado);
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento modificado en 'empleados' bajo ID# " +
                                            vista.listEmpleados.getSelectedValue().getIdEmpleado());
                            listarEmpleados((ArrayList<Empleado>) modelo.getObjeto(new Empleado()));
                        } else {
                            Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                        }
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "delEmpleado":
                    if (vista.listEmpleados.getSelectedIndex() != -1) {
                        try {
                            modelo.eliminarObjeto(vista.listEmpleados.getSelectedValue());
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento eliminado en 'empleados' bajo ID# " +
                                            vista.listEmpleados.getSelectedValue().getIdEmpleado());
                            listarEmpleados((ArrayList<Empleado>) modelo.getObjeto(new Empleado()));
                        } catch (Exception ex) {
                            Util.mostrarMensajeError("Otros elementos dependen del elemento seleccionado." +
                                    "\nCompruebe las dependencias antes de eliminar un elemento.");
                        }
                    } else {
                        Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                    }
                    break;

                // Departamentos
                case "addDepartamento":
                    if (comprobarDepartamento()) {
                        Departamento departamento = new Departamento();
                        departamento.setDepartamento(vista.txtNombreDepartamento.getText());
                        modelo.altaObjeto(departamento);
                        listarDepartamentos((ArrayList<Departamento>) modelo.getObjeto(new Departamento()));
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                "Elemento añadido a 'departamento' bajo ID# " +
                                        vista.dlmDepartamento.getElementAt(vista.dlmDepartamento.getSize() - 1)
                                                .getIdDepartamento());
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "modDepartamento":
                    if (comprobarDepartamento()) {
                        if (vista.listDepartamentos.getSelectedIndex() != -1) {
                            Departamento departamento = vista.listDepartamentos.getSelectedValue();
                            departamento.setDepartamento(vista.txtNombreDepartamento.getText());
                            modelo.modificarObjeto(departamento);
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento modificado en 'departamento' bajo ID# " +
                                            vista.listDepartamentos.getSelectedValue().getIdDepartamento());
                            listarDepartamentos((ArrayList<Departamento>) modelo.getObjeto(new Departamento()));
                        } else {
                            Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                        }
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "delDepartamento":
                    if (vista.listDepartamentos.getSelectedIndex() != -1) {
                        try {
                            modelo.eliminarObjeto(vista.listDepartamentos.getSelectedValue());
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento eliminado en 'departamento' bajo ID# " +
                                            vista.listDepartamentos.getSelectedValue().getIdDepartamento());
                            listarDepartamentos((ArrayList<Departamento>) modelo.getObjeto(new Departamento()));
                        } catch (Exception ex) {
                            Util.mostrarMensajeError("Otros elementos dependen del elemento seleccionado." +
                                    "\nCompruebe las dependencias antes de eliminar un elemento.");
                        }
                    } else {
                        Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                    }
                    break;

                // Clientes
                case "addCliente":
                    if (comprobarCliente()) {
                        Cliente cliente = new Cliente();
                        cliente.setNick(vista.txtNickCliente.getText());
                        cliente.setFechaNacimiento(Date.valueOf(vista.datePickerNacimientoCliente.getDate()));
                        cliente.setCategoria((Categoria) vista.cBoxCategoriaCliente.getSelectedItem());
                        if (vista.cBoxOfertaCliente != null)
                            cliente.setOferta((Oferta) vista.cBoxOfertaCliente.getSelectedItem());
                        modelo.altaObjeto(cliente);
                        listarClientes((ArrayList<Cliente>) modelo.getObjeto(new Cliente()));
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                "Elemento añadido a 'clientes' bajo ID# " +
                                        vista.dlmClientes.getElementAt(vista.dlmClientes.getSize() - 1).getIdCliente());
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "modCliente":
                    if (comprobarCliente()) {
                        if (vista.listClientes.getSelectedIndex() != -1) {
                            Cliente cliente = vista.listClientes.getSelectedValue();
                            cliente.setNick(vista.txtNickCliente.getText());
                            cliente.setFechaNacimiento(Date.valueOf(vista.datePickerNacimientoCliente.getDate()));
                            cliente.setCategoria((Categoria) vista.cBoxCategoriaCliente.getSelectedItem());
                            if (vista.cBoxOfertaCliente != null)
                                cliente.setOferta((Oferta) vista.cBoxOfertaCliente.getSelectedItem());
                            modelo.modificarObjeto(cliente);
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento modificado en 'clientes' bajo ID# " +
                                            vista.listClientes.getSelectedValue().getIdCliente());
                            listarClientes((ArrayList<Cliente>) modelo.getObjeto(new Cliente()));
                        } else {
                            Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                        }
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "delCliente":
                    if (vista.listClientes.getSelectedIndex() != -1) {
                        modelo.eliminarObjeto(vista.listClientes.getSelectedValue());
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                "Elemento eliminado en 'clientes' bajo ID# " +
                                        vista.listClientes.getSelectedValue().getIdCliente());
                        listarClientes((ArrayList<Cliente>) modelo.getObjeto(new Cliente()));
                    } else {
                        Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                    }
                    break;

                // Categorias
                case "addCategoria":
                    if (comprobarCategoria()) {
                        Categoria categoria = new Categoria();
                        categoria.setCategoria(vista.txtNombreCategoria.getText());
                        if (!vista.txtNivelCategoria.getText().isEmpty() && comprobarInt(vista.txtNivelCategoria.getText()))
                            categoria.setNivel(Integer.parseInt(vista.txtNivelCategoria.getText()));
                        modelo.altaObjeto(categoria);
                        listarCategorias((ArrayList<Categoria>) modelo.getObjeto(new Categoria()));
                        registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                "Elemento añadido a 'categorias' bajo ID# " +
                                        vista.dlmCategorias.getElementAt(vista.dlmCategorias.getSize() - 1)
                                                .getIdCategoria());
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "modCategoria":
                    if (comprobarCategoria()) {
                        if (vista.listCategorias.getSelectedIndex() != -1) {
                            Categoria categoria = vista.listCategorias.getSelectedValue();
                            categoria.setCategoria(vista.txtNombreCategoria.getText());
                            if (!vista.txtNivelCategoria.getText().isEmpty() &&
                                    comprobarInt(vista.txtNivelCategoria.getText()))
                                categoria.setNivel(Integer.parseInt(vista.txtNivelCategoria.getText()));
                            modelo.modificarObjeto(categoria);
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento modificado en 'categorias' bajo ID# " +
                                            vista.listCategorias.getSelectedValue().getIdCategoria());
                            listarCategorias((ArrayList<Categoria>) modelo.getObjeto(new Categoria()));
                        } else {
                            Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                        }
                    } else {
                        Util.mostrarMensajeError("Debes rellenar todos los campos marcados con ( * ).\n" +
                                "Algunos campos solo admiten números enteros.");
                    }
                    break;

                case "delCategoria":
                    if (vista.listCategorias.getSelectedIndex() != -1) {
                        try {
                            modelo.eliminarObjeto(vista.listCategorias.getSelectedValue());
                            registro.addRegistro(RegistroDialog.TipoRegistro.OK,
                                    "Elemento eliminado en 'categorias' bajo ID# " +
                                            vista.listCategorias.getSelectedValue().getIdCategoria());
                            listarCategorias((ArrayList<Categoria>) modelo.getObjeto(new Categoria()));
                        } catch (Exception ex) {
                            Util.mostrarMensajeError("Otros elementos dependen del elemento seleccionado." +
                                    "\nCompruebe las dependencias antes de eliminar un elemento.");
                        }
                    } else {
                        Util.mostrarMensajeError("No hay ningún elemento seleccionado");
                    }
                    break;
            }
        }catch (NullPointerException ex) {
            Util.mostrarMensajeError("Fallo en el enlace de comunicaciones.\n" +
                    "Compruebe el estado del servidor.");
        }
        limpiarCampos();
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listProductos) {
            if (vista.listProductos.getSelectedIndex() != -1) {
                Producto producto = vista.listProductos.getSelectedValue();
                vista.txtNombreProducto.setText(producto.getNombre());
                vista.txtGradosBebida.setText(String.valueOf(producto.getGrados()));
                vista.txtPrecioProducto.setText(String.valueOf(producto.getPrecio()));
            }
        } else if (e.getSource() == vista.listOfertas) {
            if (vista.listOfertas.getSelectedIndex() != -1) {
                Oferta oferta = vista.listOfertas.getSelectedValue();
                vista.txtNombreOferta.setText(oferta.getNombre());
                vista.txtDescOferta.setText(oferta.getDescripcion());
                vista.txtDescuentoOferta.setText(String.valueOf(oferta.getPorcentajeDescuento()));
                vista.cBoxEmpleadoOferta.setSelectedItem(oferta.getEmpleado());
                listarClientesOferta((ArrayList<Cliente>) modelo.getObjeto(new Cliente(), oferta));
            }
        } else if (e.getSource() == vista.listEmpleados) {
            if (vista.listEmpleados.getSelectedIndex() != -1) {
                Empleado empleado = vista.listEmpleados.getSelectedValue();
                vista.txtNombreEmpleado.setText(empleado.getNombre());
                vista.txtApellidosEmpleado.setText(empleado.getApellidos());
                vista.txtSalarioEmpleado.setText(String.valueOf(empleado.getSalario()));
                vista.txtDniEmpleado.setText(empleado.getDni());
                vista.datePickerFechaEmpleado.setDate(empleado.getFechaIncorporacion().toLocalDate());
                listarOfertasEmpleado((ArrayList<Oferta>) modelo.getObjeto(new Oferta(), empleado));
                if (empleado.getDepartamento() != null)
                    vista.cBoxDepartamentoEmpleado.setSelectedItem(empleado.getDepartamento());
                else
                    vista.cBoxDepartamentoEmpleado.setSelectedIndex(-1);
            }
        } else if (e.getSource() == vista.listDepartamentos) {
            if (vista.listDepartamentos.getSelectedIndex() != -1) {
                Departamento departamento = vista.listDepartamentos.getSelectedValue();
                vista.txtNombreDepartamento.setText(departamento.getDepartamento());
                listarEmpleadosDepartamento((ArrayList<Empleado>) modelo.getObjeto(new Empleado(), departamento));
            }
        } else if (e.getSource() == vista.listClientes) {
            if (vista.listClientes.getSelectedIndex() != -1) {
                Cliente cliente = vista.listClientes.getSelectedValue();
                vista.txtNickCliente.setText(cliente.getNick());
                vista.datePickerNacimientoCliente.setDate(cliente.getFechaNacimiento().toLocalDate());
                vista.cBoxCategoriaCliente.setSelectedItem(cliente.getCategoria());
                if (cliente.getOferta() != null)
                    vista.cBoxOfertaCliente.setSelectedItem(cliente.getOferta());
                else
                    vista.cBoxOfertaCliente.setSelectedIndex(-1);
            }
        } else if (e.getSource() == vista.listCategorias) {
            if (vista.listCategorias.getSelectedIndex() != -1) {
                Categoria categoria = vista.listCategorias.getSelectedValue();
                vista.txtNombreCategoria.setText(categoria.getCategoria());
                vista.txtNivelCategoria.setText(String.valueOf(categoria.getNivel()));
                listarClientesCategoria((ArrayList<Cliente>) modelo.getObjeto(new Cliente(), categoria));
            }
        }
    }

    private void listarProductos(ArrayList<Producto> lista) {
        vista.dlmProductos.clear();
        for (Producto i : lista) {
            vista.dlmProductos.addElement(i);
        }
    }

    private void listarOfertas(ArrayList<Oferta> lista) {
        vista.dlmOfertas.clear();
        vista.dcbmOfertaCliente.removeAllElements();
        for (Oferta i : lista) {
            vista.dlmOfertas.addElement(i);
            vista.dcbmOfertaCliente.addElement(i);
        }
    }

    private void listarClientesOferta(ArrayList<Cliente> lista) {
        vista.dlmClientesOferta.clear();
        for (Cliente i : lista) {
            vista.dlmClientesOferta.addElement(i);
        }
    }

    private void listarEmpleados(ArrayList<Empleado> lista) {
        vista.dlmEmpleados.clear();
        vista.dcbmEmpleadoOferta.removeAllElements();
        for (Empleado i : lista) {
            vista.dlmEmpleados.addElement(i);
            vista.dcbmEmpleadoOferta.addElement(i);
        }
    }

    private void listarOfertasEmpleado(ArrayList<Oferta> lista) {
        vista.dlmOfertasEmpleado.clear();
        for (Oferta i : lista) {
            vista.dlmOfertasEmpleado.addElement(i);
        }
    }

    private void listarDepartamentos(ArrayList<Departamento> lista) {
        vista.dlmDepartamento.clear();
        vista.dcbmDepartamentoEmpleado.removeAllElements();
        for (Departamento i : lista) {
            vista.dlmDepartamento.addElement(i);
            vista.dcbmDepartamentoEmpleado.addElement(i);
        }
    }

    private void listarEmpleadosDepartamento(ArrayList<Empleado> lista) {
        vista.dlmEmpleadosDepartamento.clear();
        for (Empleado i : lista) {
            vista.dlmEmpleadosDepartamento.addElement(i);
        }
    }

    private void listarClientes(ArrayList<Cliente> lista) {
        vista.dlmClientes.clear();
        for (Cliente i : lista) {
            vista.dlmClientes.addElement(i);
        }
    }

    private void listarCategorias(ArrayList<Categoria> lista) {
        vista.dlmCategorias.clear();
        vista.dcbmCategoriaCliente.removeAllElements();
        for (Categoria i : lista) {
            vista.dlmCategorias.addElement(i);
            vista.dcbmCategoriaCliente.addElement(i);
        }
    }

    private void listarClientesCategoria(ArrayList<Cliente> lista) {
        vista.dlmClientesCategoria.clear();
        for (Cliente i : lista) {
            vista.dlmClientesCategoria.addElement(i);
        }
    }

    private boolean comprobarProducto() {
        return !vista.txtNombreProducto.getText().isEmpty() &&
                !vista.txtGradosBebida.getText().isEmpty() &&
                !vista.txtPrecioProducto.getText().isEmpty() &&
                comprobarInt(vista.txtPrecioProducto.getText()) &&
                comprobarInt(vista.txtGradosBebida.getText());
    }

    private boolean comprobarOferta() {
        return !vista.txtNombreOferta.getText().isEmpty() &&
                !vista.txtDescOferta.getText().isEmpty() &&
                !vista.txtDescuentoOferta.getText().isEmpty() &&
                comprobarInt(vista.txtDescuentoOferta.getText()) &&
                vista.cBoxEmpleadoOferta.getSelectedItem() != null;
    }

    private boolean comprobarEmpleado() {
        return !vista.txtNombreEmpleado.getText().isEmpty() &&
                !vista.txtApellidosEmpleado.getText().isEmpty() &&
                !vista.txtDniEmpleado.getText().isEmpty() &&
                !vista.datePickerFechaEmpleado.getText().isEmpty() &&
                !vista.txtSalarioEmpleado.getText().isEmpty() &&
                comprobarInt(vista.txtSalarioEmpleado.getText());
    }

    private boolean comprobarDepartamento() {
        return !vista.txtNombreDepartamento.getText().isEmpty();
    }

    private boolean comprobarCliente() {
        return !vista.txtNickCliente.getText().isEmpty() &&
                !vista.datePickerNacimientoCliente.getText().isEmpty() &&
                vista.cBoxCategoriaCliente.getSelectedItem() != null;
    }

    private boolean comprobarCategoria() {
        return !vista.txtNombreCategoria.getText().isEmpty() &&
                !vista.txtNivelCategoria.getText().isEmpty() &&
                comprobarInt(vista.txtNivelCategoria.getText());
    }

    private boolean comprobarInt(String texto) {
        try {
            Integer.parseInt(texto);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    private void limpiarCampos() {
        vista.txtNombreProducto.setText("");
        vista.txtGradosBebida.setText("");
        vista.txtPrecioProducto.setText("");
        vista.listProductos.clearSelection();

        vista.txtNombreOferta.setText("");
        vista.txtDescOferta.setText("");
        vista.txtDescuentoOferta.setText("");
        vista.cBoxEmpleadoOferta.setSelectedIndex(-1);
        vista.listOfertas.clearSelection();
        vista.listClientesOferta.clearSelection();

        vista.txtNombreEmpleado.setText("");
        vista.txtApellidosEmpleado.setText("");
        vista.txtDniEmpleado.setText("");
        vista.datePickerFechaEmpleado.setText("");
        vista.txtSalarioEmpleado.setText("");
        vista.dcbmDepartamentoEmpleado.setSelectedItem(-1);
        vista.listEmpleados.clearSelection();
        vista.listOfertasEmpleado.clearSelection();

        vista.txtNombreDepartamento.setText("");
        vista.listEmpleadosDepartamento.clearSelection();
        vista.listDepartamentos.clearSelection();

        vista.txtNickCliente.setText("");
        vista.datePickerNacimientoCliente.setText("");
        vista.cBoxCategoriaCliente.setSelectedIndex(-1);
        vista.cBoxOfertaCliente.setSelectedIndex(-1);
        vista.listClientes.clearSelection();

        vista.txtNombreCategoria.setText("");
        vista.txtNivelCategoria.setText("");
        vista.listCategorias.clearSelection();
        vista.listClientesCategoria.clearSelection();
    }

    private void botonesActivos(boolean activos){
        vista.btnAddProducto.setEnabled(activos);
        vista.btnModProducto.setEnabled(activos);
        vista.btnDelProducto.setEnabled(activos);

        vista.btnAddOferta.setEnabled(activos);
        vista.btnModOferta.setEnabled(activos);
        vista.btnDelOferta.setEnabled(activos);

        vista.btnAddEmpleado.setEnabled(activos);
        vista.btnModEmpleado.setEnabled(activos);
        vista.btnDelEmpleado.setEnabled(activos);

        vista.btnAddDepartamento.setEnabled(activos);
        vista.btnModDepartamento.setEnabled(activos);
        vista.btnDelDepartamento.setEnabled(activos);

        vista.btnAddCliente.setEnabled(activos);
        vista.btnModCliente.setEnabled(activos);
        vista.btnDelCliente.setEnabled(activos);

        vista.btnAddCategoria.setEnabled(activos);
        vista.btnModCategoria.setEnabled(activos);
        vista.btnDelCategoria.setEnabled(activos);
    }
}

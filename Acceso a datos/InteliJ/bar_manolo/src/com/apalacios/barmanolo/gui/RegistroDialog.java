package com.apalacios.barmanolo.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RegistroDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonCancel;
    private JTextPane txtRegistro;
    public enum TipoRegistro {
        OK, INFO, FAIL;
    }

    public RegistroDialog() {
        setTitle("Registro");
        txtRegistro.setText(">>> Registro de operaciones contra BBDD <<<");
        setContentPane(contentPane);
        setModal(false);
        setPreferredSize(new Dimension(400, 250));
        setLocationRelativeTo(null);
        pack();
        setVisible(false);

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    public void setVisibleDialog(boolean visible) {
        setVisible(visible);
    }

    public void addRegistro(TipoRegistro tipoRegistro, String mensaje){
        String registro = "";
        switch (tipoRegistro) {
            case OK:
                registro = "[  OK  ]  ";
                break;
            case INFO:
                registro = "[INFO]  ";
                break;
            case FAIL:
                registro = "[ FAIL ]  ";
                break;
        }
        registro += mensaje;
        txtRegistro.setText(txtRegistro.getText() + "\n" + registro);
    }

    private void onCancel() {
        dispose();
    }
}

package gui;

import base.enums.GenerosLibros;
import base.enums.TiposEditoriales;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private JPanel panelPrincipal;
    /*Libro*/
    JTextField txtTitulo;
    JComboBox cBoxAutor;
    JComboBox cBoxEditorial;
    JComboBox<String> cBoxGenero;
    JTextField txtISBN;
    JTextField txtPrecioLibro;
    JTable tableLibros;
    JButton btnModLibro;
    JButton btnAddLibro;
    JButton btnDelLibro;
    /*Autor*/
    JTextField txtNombre;
    JTextField txtApellidos;
    DatePicker dPickerNacimiento;
    JTextField txtPais;
    JTable tableAutores;
    JButton btnModAutor;
    JButton btnAddAutor;
    JButton btnDelAutor;
    /*Editorial*/
    JTextField txtNombreEditorial;
    JTextField txtEmail;
    JTextField txtTelefono;
    JComboBox<String> cBoxTipoEditorial;
    JTextField txtWeb;
    JTable tableEditoriales;
    JButton btnModEditorial;
    JButton btnAddEditorial;
    JButton btnDelEditorial;
    DatePicker dPickerEdicion;
    /*Default Table Models*/
    DefaultTableModel dtmLibros;
    DefaultTableModel dtmAutores;
    DefaultTableModel dtmEditoriales;
    /*Menu*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    /*Estado*/
    JLabel etiquetaEstado1;
    JLabel etiquetaEstado2;
    JLabel etiquetaEstado3;
    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    //Constructor
    public Vista(){
        initFrame();
    }

    private void initFrame(){
        this.setTitle("Librería - Adrián Palacios");
        this.setContentPane(panelPrincipal);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    private void setAdminDialog(){
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

    private void setEnumComboBox(){
        for(TiposEditoriales constant : TiposEditoriales.values()){
            cBoxTipoEditorial.addItem(constant.getValor());
        }
        cBoxTipoEditorial.setSelectedIndex(-1);

        for(GenerosLibros constant : GenerosLibros.values()){
            cBoxGenero.addItem(constant.getValor());
        }
        cBoxGenero.setSelectedIndex(-1);
    }

    private void setTableModels(){
        this.dtmLibros = new DefaultTableModel();
        this.tableLibros.setModel(dtmLibros);
        this.dtmAutores = new DefaultTableModel();
        this.tableAutores.setModel(dtmAutores);
        this.dtmEditoriales = new DefaultTableModel();
        this.tableEditoriales.setModel(dtmEditoriales);
    }
}

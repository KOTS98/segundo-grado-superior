package gui;

import com.mysql.jdbc.*;

import java.io.*;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import util.Util;

public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public Modelo(){
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    private Connection conexion;

    void conectar(){
        try {
            conexion = (Connection) DriverManager.getConnection("jdbc:mysql://" + ip + ":3306/BD_varias_tablas", user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PreparedStatement statement = null;
        String code = leerFichero();
        String[] query = code.split("--");
        for (String aQuery : query){
            try {
                statement = (PreparedStatement) conexion.prepareStatement(aQuery);
                statement.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            assert statement != null;
        }
    }

    void desconectar() {
        try{
            conexion.close();
        }catch (SQLException e){
            e.printStackTrace();
        }
        conexion = null;
    }

    void insertarEditorial(String editorial, String email, String telefono, String tipoEditorial, String web){
        String sentenciaSql = "INSERT INTO editoriales (editorial, email, telefono, tipoEditorial, web) VALUES" +
                " (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = (PreparedStatement) conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, editorial);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEditorial);
            sentencia.setString(5, web);
            sentencia.executeQuery();
            sentencia.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void borrarEditorial (int idEditorial) {
        String sentenciaSql = "DELETE FROM editoriales WHERE ideditorial = ?";
        PreparedStatement sentencia;
        try {
            sentencia = (PreparedStatement) conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, idEditorial);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    ResultSet consultarEditorial() {
        String sentenciaSql = "SELET concat(ideditorial) as 'ID', concat(editorial) as 'Nombre editorial', " +
                "concat(email) as 'email', concat(telefono) as 'Teléfono', concat(tipoEditorial) as 'Tipo', " +
                "conncat(web) as 'Web' FROM editoriales";
        PreparedStatement sentencia;
        ResultSet resultado = null;
        try {
            sentencia = (PreparedStatement) conexion.prepareStatement(sentenciaSql);
            resultado = sentencia.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultado;
    }

    void modificarEditorial(String editorial, String email, String telefono, String tipoEditorial, String web,
                            int ideditorial){
        String sentenciaSql = "UPDATE editoriales SET editorial = ?, email = ?, telefono = ?, tipoEditorial = ?," +
                "web = ? WHERE ideditorial = ?";
        PreparedStatement sentencia;
        try {
            sentencia = (PreparedStatement) conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, editorial);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEditorial);
            sentencia.setString(5, web);
            sentencia.setInt(6, ideditorial);
            sentencia.executeQuery();
            sentencia.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String leerFichero(){
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader("BD_Varias_Tablas_Sin_Delimitadores.sql"));
            String linea;
            while((linea = br.readLine()) != null){
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }
        } catch (IOException e) {
            Util.showErrorAlert("Error de lectura de archivo");
        }
        return stringBuilder.toString();
    }

    public boolean editorialNombreYaExiste(String nombre) {
        String editorialNameConsult = "SELECT existeNombreEditorial(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = (PreparedStatement) conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();
            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }

    void insertarAutor(){

    }

    void insertarLibro(){

    }

    void borrarAutor(){

    }

    void borrarLibro(){

    }

    void modificarAutor(){

    }

    void modificarLibro(){

    }

    void consultarLibro(){

    }

    void consultarAutor(){

    }

    void libroIsbnYaExiste(String isbn){

    }

    void autorNombreYaExiste(String nombre){

    }

    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }
}

CREATE DATABASE IF NOT EXISTS bd_varias_tablas;
USE bd_varias_tablas;

CREATE TABLE IF NOT EXISTS autores (
idautor int auto_increment primary key,
nombre varchar(50) not null,
apellidos varchar(150) not null,
fechanacimiento date,
pais varchar(50)
);

CREATE TABLE IF NOT EXISTS editoriales (
ideditorial int auto_increment primary key,
editorial varchar(50) not null,
email varchar(100) not null,
telefono varchar(9),
tipoeditorial varchar(50),
web varchar(100)
);

CREATE TABLE IF NOT EXISTS libros (
idlibro int auto_increment primary key,
titulo varchar(50) not null,
isbn varchar(40) not null UNIQUE,
ideditorial int not null,
genero varchar(30),
idautor int not null,
precio float not null,
fechalanzamiento date
);

alter table libros
add foreign key (ideditorial) references editoriales(ideditorial),
add foreign key (idautor) references autores(idautor);

-- Función para determinar si existe el Isbn especificado
delimiter ||
create function existeIsbn(f_isbn varchar(40))
returns bit
begin
	declare i int;
    set i = 0;
    -- Itera sobre todos los libros en la tabla libros
    while (i < (select max(idlibro) from libros)) do
		-- Comprueba si el isbn del libro actual coincide con el isbn recibido como parámetro de la función
		if ((select isbn from libros where idlibro = (i + 1)) like f_isbn)
			-- Si encuentra una coincidencia, devuelve 1 (true)
			then return 1;
		end if;
        set i = i + 1;
	end while;
    -- Si no ha encontrado coincidencias, devuelve un 0 (false)
    return 0;
end; ||
delimiter ;

-- Función que determina si existe una editorial con el nombre especificado
delimiter ||
create function existeNombreEditorial (f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    -- Itera sobre todas las editoriales de la tabla editoriales
    while (i < (select max (ideditorial) from editoriales)) do
		-- Comprueba si el nombre de la editorial actual coincide con el nombre recibido como parámetro de la función
		if ((select editorial from editoriales where ideditorial = (i + 1)) like f_name)
			-- Si encuentra una coincidencia, devuelve 1 (true)
			then return 1;
		end if;
        set i = i + 1;
	end while;
    -- Si no ha encontrado coincidencias, devuelve un 0 (false)
    return 0;
end; ||
delimiter ;

-- Función para determinar si existe el autor especificado
delimiter ||
create function existeNombreAutor (f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    -- Itera sobre todos los autores de la tabla autores
    while (i < (select max(idautor) from autores)) do
		-- Comprueba si el nombre del autor actual coincide con el nombre recibido como parámetro de la función
		if ((select concat(apellidos, ', ', nombre) from autores where idautor = (i + 1)) like f_name)
			-- Si encuentra una coincidencia, devuelve 1 (true)
			then return 1;
		end if;
		set i = i + 1;
	end while;
    -- Si no ha encontrado coincidencias, devuelve un 0 (false)
    return 0;
end; ||
delimiter ;
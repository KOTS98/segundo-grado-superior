package com.apalacios.basedatos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "coches", schema = "vehiculos", catalog = "")
public class Coche {
    private int id;
    private String matricula;
    private String marca;
    private String modelo;
    private Date fechaMatriculacion;
    private int idPropietario;
    private Propietario propietario;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "matricula")
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "modelo")
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "fecha_matriculacion")
    public Date getFechaMatriculacion() {
        return fechaMatriculacion;
    }

    public void setFechaMatriculacion(Date fechaMatriculacion) {
        this.fechaMatriculacion = fechaMatriculacion;
    }

    @Basic
    @Column(name = "id_propietario")
    public int getIdPropietario() {
        return idPropietario;
    }

    public void setIdPropietario(int idPropietario) {
        this.idPropietario = idPropietario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coche coche = (Coche) o;
        return id == coche.id &&
                idPropietario == coche.idPropietario &&
                Objects.equals(matricula, coche.matricula) &&
                Objects.equals(marca, coche.marca) &&
                Objects.equals(modelo, coche.modelo) &&
                Objects.equals(fechaMatriculacion, coche.fechaMatriculacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, matricula, marca, modelo, fechaMatriculacion, idPropietario);
    }

    @ManyToOne
    @JoinColumn(name = "id_propietario", referencedColumnName = "id")
    public Propietario getPropietario() {
        return propietario;
    }

    public void setPropietario(Propietario propietario) {
        this.propietario = propietario;
    }
}

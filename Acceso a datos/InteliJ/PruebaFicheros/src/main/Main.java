package main;

import java.awt.image.ImagingOpException;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Main {
    public static void main(String[] args) {
        escribirFichero();
        leerFichero();
    }

    public static void escribirFichero(){
        System.out.println("Se va a escribir en el fichero:");
        try {
            RandomAccessFile fichero = new RandomAccessFile("texto.txt",  "rw");
            fichero.writeUTF("primera linea");
            fichero.writeUTF("segunda linea");
            fichero.close();
        }catch(IOException e){
            System.out.println("[ERROR] Fallo al escribir el fichero.");
        }
    }

    public static void leerFichero(){
        System.out.println("Este es el contenido del fichero:");
        try {
            RandomAccessFile fichero = new RandomAccessFile("texto.txt", "r");
            boolean finArchivo = false;
            do{
                try{
                    String linea = fichero.readUTF();
                    System.out.println(linea);
                }catch(EOFException e){
                    finArchivo = true;
                }
            }while(finArchivo == false);
            fichero.close();
        }catch(FileNotFoundException e){
            System.out.println("[ERROR] Archivo no encontrado.");
        }catch(IOException e) {
            System.out.println("[ERROR] Fallo al leer el archivo.");
        }
    }
}

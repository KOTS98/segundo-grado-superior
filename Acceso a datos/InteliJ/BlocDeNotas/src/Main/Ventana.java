package Main;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Properties;

public class Ventana {
    private JFrame frame;
    private JPanel panel1;
    private JTextArea textArea1;

    public Ventana(){
        frame = new JFrame("Ventana");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        iniciarMenu();
        cargarFicheroAnterior();
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
    }

    private void cargarFicheroAnterior(){
        Properties conf = new Properties();
        try {
            conf.load(new FileReader("conf.properties"));
            BufferedReader br = new BufferedReader(new FileReader(conf.getProperty("ultimo_archivo")));
            String linea;
            while((linea = br.readLine()) != null){
                textArea1.append(br.readLine());
            }
            br.close();
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado.");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void iniciarMenu(){
        JMenuBar barraMenu = new JMenuBar();
        JMenu menuArchivo = new JMenu("Archivo");
        JMenuItem itemGuardar = new JMenuItem("Guardar");
        menuArchivo.add(itemGuardar);
        barraMenu.add(menuArchivo);
        frame.setJMenuBar(barraMenu);
        itemGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selector = new JFileChooser();
                int opcion = selector.showSaveDialog(null);
                if(opcion == JFileChooser.APPROVE_OPTION){
                    try {
                        Properties conf = new Properties();
                        conf.setProperty("ultimo_archivo", selector.getSelectedFile().getPath());
                        FileWriter fw = new FileWriter("conf.properties");
                        conf.store(fw, "");
                        fw = new FileWriter(selector.getSelectedFile().getPath());
                        fw.write(textArea1.getText());
                        fw.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }
}

package apalacios.barmanolo.main;

import apalacios.barmanolo.gui.Controlador;
import apalacios.barmanolo.gui.Modelo;
import apalacios.barmanolo.gui.Vista;

public class Main {
    public static void main(String[] args) {
        new Controlador(new Modelo(), new Vista());
    }
}

package com.apalacios.javaception.base;

import java.io.Serializable;

/**
 * Se trata de un objeto que representa una función dentro de un programa. Puede ser una función de tipo mensaje,
 * operación, condición o bucle. Dependiendo del tipo de función que se cree, se utilizarán unos parámetros u
 * otros.
 *
 * @author José Adrián Palacios Romo
 */
public class Funcion implements Serializable {
    private String nombre;
    private String tipoFuncion;
    private String cadenaMensaje;
    private String tipoOperacion;
    private String tipoCondicion;
    private int contadorBucle;
    private Variable variable;
    private Variable variable2;
    private Variable variable3;

    /**
     * Constructor de clase, inicializa todos los atributos de la clase.
     */
    public Funcion(){
        this.nombre = "";
        this.tipoFuncion = "";
        this.cadenaMensaje = "";
        this.tipoOperacion = "";
        this.tipoCondicion = "";
        this.contadorBucle = 0;
        this.variable = new Variable();
        this.variable2 = new Variable();
        this.variable3 = new Variable();
    }

    /**
     * @return String nombre - El nombre de la función.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return Variable variable - La primera variable de la función.
     */
    public Variable getVariable(){
        return variable;
    }

    /**
     * @return Variable variable2 - La segunda variable de la función.
     */
    public Variable getVariable2() {
        return variable2;
    }

    /**
     * @return Variable variable3 - La tercera variable de la función.
     */
    public Variable getVariable3() {
        return variable3;
    }

    /**
     * @return String tipoFuncion - Devuelve el tipo de función actual, solo puede ser:
     * - mensaje, - operacion, - condicion, - bucle.
     */
    public String getTipoFuncion() {
        return tipoFuncion;
    }

    /**
     * @return String cadenaMensaje - Devuelve el contenido del mensaje en caso de que se trate de una función de tipo
     * mensaje.
     */
    public String getCadenaMensaje() {
        return cadenaMensaje;
    }

    /**
     * @return String tipoOperacion - Devuelve, en caso de que se trate de una función de tipo operación, el tipo de
     * operación actual, solo puede ser:
     * - suma, - resta, - mult, - div.
     */
    public String getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * @return String tipoCondicion - Devuelve, en caso de que se trate de una función de tipo condición, el tipo de
     * condición actual, solo puede ser:
     * - mayor, - menor, - igual, - diferente.
     */
    public String getTipoCondicion() {
        return tipoCondicion;
    }

    /**
     * @return int contadorBucle - Devuelve, en caso de que se trate de una función de tipo bucle, la cantidad de
     * iteraciónes de bucle.
     */
    public int getContadorBucle() {
        return contadorBucle;
    }

    /**
     * @param nombre Determina el nombre de la función.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @param tipoFuncion Determina el tipo de función.
     */
    public void setTipoFuncion(String tipoFuncion) {
        this.tipoFuncion = tipoFuncion;
    }

    /**
     * @param cadenaMensaje Establece el mensaje que contendrá en caso de ser una función de tipo mensaje.
     */
    public void setCadenaMensaje(String cadenaMensaje) {
        this.cadenaMensaje = cadenaMensaje;
    }

    /**
     * @param tipoOperacion Establece el tipo de operación en caso de que se trate de una función de tipo operación.
     */
    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    /**
     * @param tipoCondicion Establece el tipo de condición en caso de que se trate de una función de tipo condición.
     */
    public void setTipoCondicion(String tipoCondicion) {
        this.tipoCondicion = tipoCondicion;
    }

    /**
     * @param contadorBucle Establece la cantidad de iteraciones del bucle en caso de que se trate de una función de
     * tipo bucle.
     */
    public void setContadorBucle(int contadorBucle) {
        this.contadorBucle = contadorBucle;
    }

    /**
     * @param variable Establece la primera variable de la función.
     */
    public void setVariable(Variable variable) {
        this.variable = variable;
    }

    /**
     * @param variable2 Establece la segunda variable de la función.
     */
    public void setVariable2(Variable variable2) {
        this.variable2 = variable2;
    }

    /**
     * @param variable3 Establece la tercera variable de la función.
     */
    public void setVariable3(Variable variable3) {
        this.variable3 = variable3;
    }

    /**
     * Genera la sintaxis necesaria para generar una funcion (Mensaje por terminal, operación
     * matemática, condicional o bucle) en Java.
     *
     * @return String cadena - Representa la cadena de texto necesaria para escribir la función deseada.
     */
    public String funcionTexto(){
        String cadena = "";
        switch (this.tipoFuncion){
            case "mensaje":
                if(this.variable == null){
                    cadena = "System.out.println (\"" + this.cadenaMensaje + "\");";
                }else{
                    cadena = "System.out.println (\"" + this.cadenaMensaje + "\" + " + variable.getNombre() + ");";
                }
                break;

            case "operacion":
                switch (tipoOperacion){
                    case "suma":
                        cadena = variable3.getNombre() + " = " + variable.getNombre() + " + "
                                + variable2.getNombre() + ";";
                        break;

                    case "resta":
                        cadena = variable3.getNombre() + " = " + variable.getNombre() + " - "
                                + variable2.getNombre() + ";";
                        break;

                    case "mult":
                        cadena = variable3.getNombre() + " = " + variable.getNombre() + " * "
                                + variable2.getNombre() + ";";
                        break;

                    case "div":
                        cadena = variable3.getNombre() + " = " + variable.getNombre() + " / "
                                + variable2.getNombre() + ";";
                        break;
                }
                break;

            case "condicion":
                switch (tipoCondicion){
                    case "mayor":
                        cadena = "if (" + variable.getNombre() + " > " + variable2.getNombre() + ") \\{";
                        break;

                    case "menor":
                        cadena = "if (" + variable.getNombre() + " < " + variable2.getNombre() + ") \\{";
                        break;

                    case "igual":
                        cadena = "if (" + variable.getNombre() + " == " + variable2.getNombre() + ") \\{";
                        break;

                    case "diferente":
                        cadena = "if (" + variable.getNombre() + " != " + variable2.getNombre() + ") \\{";
                        break;
                }
                break;

            case "bucle":
                cadena = "for (int i = 0; i < " + contadorBucle + "; i++) \\{";
                break;
        }
        return cadena;
    }

    /**
     * Sobreescribe el método toString().
     *
     * @return String cadena - Representa la cadena deseada para plasmar la información de la función, dependiendo de
     * su tipo.
     */
    @Override
    public String toString() {
        String cadena = "";
        switch (tipoFuncion){
            case "mensaje":
                cadena = "MENSAJE   - " + nombre + " = " + cadenaMensaje;

                break;
            case "operacion":
                cadena = "OPERACION - " + nombre + " = " + tipoOperacion.toUpperCase();
                break;

            case "condicion":
                cadena = "CONDICION - " + nombre + " = " + tipoCondicion.toUpperCase();
                break;

            case "bucle":
                cadena = "BUCLE     - " + nombre + " = " + contadorBucle;
                break;
        }
        return cadena;
    }
}

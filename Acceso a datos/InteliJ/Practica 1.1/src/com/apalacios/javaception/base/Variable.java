package com.apalacios.javaception.base;

import java.io.Serializable;

/**
 * Se trata de un objeto que representa una variable dentro de un programa. Puede ser una variable de tipo texto,
 * numero o booleano. Dependiendo del tipo de variable que se cree, se utilizarán unos parámetros u
 * otros.
 *
 * @author José Adrián Palacios Romo
 */
public class Variable implements Serializable {
    private String nombre;
    private String tipoVar;
    private String contenidoTxt;
    private int contenidoNum;
    private boolean contenidoBool;

    /**
     * Constructor de la clase. Se encarga de inicializar todos los parámetros de la variable.
     */
    public Variable(){
        nombre = "";
        tipoVar = "";
        contenidoTxt = "";
        contenidoNum = 0;
        contenidoBool = false;
    }

    /**
     * @return String nombre - Devuelve el nombre de la variable.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return String tipoVar - Devuelve el tipo de variable. Solo puede ser:
     * - texto, - numero, - booleano.
     */
    public String getTipoVar() {
        return tipoVar;
    }

    /**
     * @return int contenidoNum - Devuelve el contenido númerico de la variable, en caso de que se trate de una
     * variable de tipo número.
     */
    public int getContenidoNum() {
        return contenidoNum;
    }

    /**
     * @return String contenidoTxt - Devuelve el contenido de texto de la variable, en caso de que se trate de una
     * variable de tipo texto.
     */
    public String getContenidoTexto() {
        return contenidoTxt;
    }

    /**
     * @return boolean contenidoBool - Devuelve el contenido booleano de la variable, en caso de que se trate de una
     * variable de tipo booleano.
     */
    public boolean getContenidoBooleano() {
        return contenidoBool;
    }

    /**
     * @param nombre Representa el nombre de la variable.
     */
    public void setNombre(String nombre) {
        this.nombre = formatearNombre(nombre);
    }

    /**
     * @param tipoVar Representa el tipo de variable deseado. Solo puede ser:
     *                - texto, - numero, - booleano.
     */
    public void setTipoVar(String tipoVar) {
        this.tipoVar = tipoVar;
    }

    /**
     * @param contenidoTxt Representa el contenido de texto de la variable.
     */
    public void setContenidoTxt(String contenidoTxt) {
        this.contenidoTxt = contenidoTxt;
    }

    /**
     * @param contenidoNum Representa el contenido númerico de la variable.
     */
    public void setContenidoNum(int contenidoNum) {
        this.contenidoNum = contenidoNum;
    }

    /**
     * @param contenidoBool Representa el contenido booleano de la variable.
     */
    public void setContenidoBool(boolean contenidoBool) {
        this.contenidoBool = contenidoBool;
    }

    /**
     * Genera la sintaxis necesaria para declarar la variable en Java,
     * dependiendo del tipo de variable (String, int o boolean).
     *
     * @return String cadena - Devuelve la cadena de texto necesaria para plasmar la variable en código Java.
     */
    public String variableTexto(){
        String cadena = "";
        switch (this.tipoVar){
            case "texto":
                cadena = "String " + nombre + " = \"" + contenidoTxt + "\";";
                break;
            case "numero":
                cadena = "int " + nombre + " = " + contenidoNum + ";";
                break;
            case "booleano":
                cadena = "boolean " + nombre + " = " + contenidoBool + ";";
                break;
        }
        return cadena;
    }

    /**
     * Sobreescribe el método toString().
     * Convierte el tipo, el nombre y el valor de la variable a una cadena de texto
     * fácilmente legible para el usuario.
     *
     * @return String cadena - Devuelve la cadena de texto que contiene los datos de la variable.
     */
    @Override
    public String toString() {
        String cadena = "";
        switch (tipoVar){
            case "texto":
                cadena = "TEXTO   - " + this.nombre + "  =  " + this.contenidoTxt;
                break;

            case "numero":
                cadena = "NÚMERO  - " + this.nombre + "  =  " + this.contenidoNum;
                break;

            case "booleano":
                cadena = "BOLEANO - " + this.nombre + "  =  " + this.contenidoBool;
                break;
        }
        return cadena;
    }

    /**
     * Recibe el nombre deseado para la variable antes de establecerlo y de cargarla al
     * código o a cualquier lista, en caso de que el nombre esté compuesto por dos o
     * más palabras separadas por espacios, elimina los espacios entre las palabras y
     * y convierte a mayúsculas la primera letra de cada palabra. También elimina
     * ualquier espacio que se haya dejado detrás del nombre de la variable.
     *
     * @param nombre Representa el nombre de la variable.
     * @return String nombreFormateado - Devuelve el nombre introducido, tras formatearlo.
     */
    private String formatearNombre(String nombre){
        String[] caracteres = nombre.split("");
        StringBuilder nombreFormateado = new StringBuilder();
        for (int i = 0; i < caracteres.length; i++){
            if (caracteres[i].equalsIgnoreCase(" ")){
                if((i + 1) < caracteres.length){
                    caracteres[i + 1] = caracteres[i + 1].toUpperCase();
                }
            }
        }
        for (String caracter : caracteres) {
            if (!caracter.equalsIgnoreCase(" ")) {
                nombreFormateado.append(caracter);
            }
        }
        return nombreFormateado.toString();
    }
}
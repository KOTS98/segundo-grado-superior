package com.apalacios.javaception.base;

import java.io.Serializable;
import java.util.*;
import java.util.HashSet;

/**
 * Tipo de objeto principal de la aplicación, representa un programa escrito en Java, en el se almacenan los datos
 * de fecha, la lista de variables, la lista de funciones y el código en sí mismo.
 *
 * @author José Adrián Palacios Romo
 */
public class Programa implements Serializable {
    private Date fecha;
    private HashSet<Variable> listaVariables;
    private ArrayList<Funcion> listaFunciones;
    private String codigo;

    /**
     * Constructor de la clase, inicializa todos los campos de la misma.
     * @param codigo Representa el código Java generado por el usuario.
     * @param listaVariables Representa el listado de variables del programa.
     * @param listaFunciones Representa el listado de funciones del programa.
     */
    public Programa(String codigo, HashSet<Variable> listaVariables, ArrayList<Funcion> listaFunciones){
        this.fecha = new Date(System.currentTimeMillis());
        this.listaVariables = listaVariables;
        this.listaFunciones = listaFunciones;
        this.codigo = codigo;
    }

    /**
     * @return String codigo - Devuelve el código Java creado por el usuario.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @return HashSet<Variable> listaVariables - Devuelve el HashSet que contiene todas las variables del programa.
     */
    public HashSet<Variable> getListaVariables(){
        return listaVariables;
    }

    /**
     * @return ArrayList<Funcion> listaFunciones - Devuelve el ArrayList que contiene todas las funciones del programa.
     */
    public ArrayList<Funcion> getListaFunciones(){
        return listaFunciones;
    }
}

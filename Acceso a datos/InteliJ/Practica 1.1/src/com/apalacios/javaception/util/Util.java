package com.apalacios.javaception.util;

import javax.swing.*;

/**
 * Clase que contiene métodos que pueden ser necesitados desde cualquier otra clase.
 *
 * @author José Adrián Palacios Romo
 */
public class Util {

    /**
     * @return String - Devuelve el texto por defecto para un nuevo archivo.
     */
    public static String textoPorDefecto(){
        return "public class Main { \n        public static void main(String[] args) {";
    }

    /**
     * Muestra el mensaje de error deseado en una ventana emergente.
            *
            * @param mensaje Representa el texto deseado para el mensaje de error.
            */
    public static void mostrarMensajeError(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }
}

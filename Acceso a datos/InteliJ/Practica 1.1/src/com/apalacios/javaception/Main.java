package com.apalacios.javaception;

import com.apalacios.javaception.gui.Controlador;
import com.apalacios.javaception.gui.Modelo;
import com.apalacios.javaception.gui.Vista;

/**
 * Clase princpipal del programa
 *
 * @author José Adrián Palacios Romo
 */
public class Main {

    /**
     * Método principal del programa, se encarga de crear el modelo, la vista y el controlador, cumpliendo con el
     * patrón de diseño MVC.
     *
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}

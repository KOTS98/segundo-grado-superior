package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ModificarFuncionBucleDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton btnSelecV1;
    private JTextField txtNombreFuncion;
    private Funcion funcion;
    private Variable variable1;

    public ModificarFuncionBucleDialog(Funcion funcion, DefaultListModel<Variable> dlmVariables) {
        this.funcion = funcion;
        txtNombreFuncion.setText(funcion.getNombre());
        variable1 = funcion.getVariable();
        if (!funcion.getVariable().getNombre().equalsIgnoreCase("")){
            btnSelecV1.setText(funcion.getVariable().getNombre());
        }

        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(450, 250));
        setLocationRelativeTo(null);
        setTitle("Modificar función de bucle");
        setResizable(false);
        getRootPane().setDefaultButton(buttonOK);

        btnSelecV1.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(this.funcion, dlmVariables);
            dialog.mostrarDialogo();
            if(!funcion.getVariable().getNombre().equalsIgnoreCase("")){
                variable1 = funcion.getVariable();
                btnSelecV1.setText(variable1.getNombre());
            }else{
                btnSelecV1.setText("Seleccionar variable...");
            }
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        if(!variable1.getNombre().equalsIgnoreCase("")){
            funcion.setTipoFuncion("bucle");
            funcion.setNombre(txtNombreFuncion.getText());
            funcion.setVariable(variable1);
        }else{
            Util.mostrarMensajeError("Debes establecer las dos variables (Primer\n" +
                    "valor, segundo valor) para\n" +
                    "hacer la condición.");
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public void mostrarDialogo(){
        setVisible(true);
    }
}

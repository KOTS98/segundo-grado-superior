package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrearVariableNumDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtNombreV;
    private JTextField txtTextoV;
    private Variable variable;

    CrearVariableNumDialog(Variable variable) {
        this.variable = variable;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);
        setTitle("Variable numérica");
        setSize(new Dimension(350, 140));
        setResizable(false);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        try{
            variable.setNombre(txtNombreV.getText());
            variable.setContenidoNum(Integer.parseInt(txtTextoV.getText()));
            variable.setTipoVar("numero");
        }catch(Exception e){
            Util.mostrarMensajeError("Solo puedes introducir carácteres númericos.");
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo() {
        setVisible(true);
    }
}
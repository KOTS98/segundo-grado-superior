package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrearFuncionMensajeDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtNombreF;
    private JTextField txtContenidoF;
    private JButton btnSeleccionarV;
    private Funcion funcion;

    CrearFuncionMensajeDialog(Funcion funcion, DefaultListModel dlmVariables) {
        this.funcion = funcion;
        setContentPane(contentPane);
        setTitle("Mensaje de terminal");
        setSize(new Dimension(360, 170));
        setResizable(false);
        setLocationRelativeTo(null);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        btnSeleccionarV.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            btnSeleccionarV.setText(funcion.getVariable().getNombre());
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        funcion.setTipoFuncion("mensaje");
        funcion.setNombre(txtNombreF.getText());
        funcion.setCadenaMensaje(txtContenidoF.getText());
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo() {
        setVisible(true);
    }
}

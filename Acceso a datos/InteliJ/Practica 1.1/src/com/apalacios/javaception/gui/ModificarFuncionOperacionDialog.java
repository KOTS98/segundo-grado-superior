package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ModificarFuncionOperacionDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton btnSelecV1;
    private JButton btnSelecV2;
    private JButton btnSelecVResultado;
    private JRadioButton rBtnSumar;
    private JRadioButton rBtnRestar;
    private JRadioButton rBtnMult;
    private JRadioButton rBtnDiv;
    private JTextField txtNombreFuncion;
    private Funcion funcion;
    private Variable variable1;
    private Variable variable2;
    private Variable resultado;

    ModificarFuncionOperacionDialog(Funcion funcion, DefaultListModel<Variable> dlmVariables) {
        this.funcion = funcion;
        txtNombreFuncion.setText(funcion.getNombre());
        variable1 = funcion.getVariable();
        variable2 = funcion.getVariable2();
        resultado = funcion.getVariable3();
        if (!funcion.getVariable().getNombre().equalsIgnoreCase("")){
            btnSelecV1.setText(funcion.getVariable().getNombre());
        }
        if (!funcion.getVariable2().getNombre().equalsIgnoreCase("")){
            btnSelecV2.setText(funcion.getVariable().getNombre());
        }
        if (!funcion.getVariable3().getNombre().equalsIgnoreCase("")){
            btnSelecVResultado.setText(funcion.getVariable().getNombre());
        }

        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(450,250));
        setLocationRelativeTo(null);
        setTitle("Modificar función de operación");
        setResizable(false);
        getRootPane().setDefaultButton(buttonOK);

        btnSelecV1.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(this.funcion, dlmVariables);
            dialog.mostrarDialogo();
            if(!funcion.getVariable().getNombre().equalsIgnoreCase("")){
                variable1 = funcion.getVariable();
                btnSelecV1.setText(variable1.getNombre());
            }else{
                btnSelecV1.setText("Seleccionar variable...");
            }
        });

        btnSelecV2.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(this.funcion, dlmVariables);
            dialog.mostrarDialogo();
            if(!funcion.getVariable().getNombre().equalsIgnoreCase("")){
                variable2 = funcion.getVariable();
                btnSelecV2.setText(variable2.getNombre());
            }else{
                btnSelecV2.setText("Seleccionar variable...");
            }
        });

        btnSelecVResultado.addActionListener( e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(this.funcion, dlmVariables);
            dialog.mostrarDialogo();
            if(!funcion.getVariable().getNombre().equalsIgnoreCase("")){
                resultado = funcion.getVariable();
                btnSelecVResultado.setText(resultado.getNombre());
            }else{
                btnSelecVResultado.setText("Seleccionar variable...");
            }
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        if(!variable1.getNombre().equalsIgnoreCase("") &&
                !variable2.getNombre().equalsIgnoreCase("") &&
                !resultado.getNombre().equalsIgnoreCase("")){
            funcion.setTipoFuncion("operacion");
            funcion.setNombre(txtNombreFuncion.getText());
            funcion.setVariable(variable1);
            funcion.setVariable2(variable2);
            funcion.setVariable3(resultado);
            if (rBtnSumar.isSelected()){
                funcion.setTipoOperacion("suma");
            }else if(rBtnRestar.isSelected()){
                funcion.setTipoOperacion("resta");
            }else if(rBtnMult.isSelected()){
                funcion.setTipoOperacion("mult");
            }else if(rBtnDiv.isSelected()){
                funcion.setTipoOperacion("div");
            }
        }else{
            Util.mostrarMensajeError("Debes establecer las tres variables (Primer\n" +
                    "valor, segundo valor y resultado) para\n" +
                    "hacer la operación.");
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public void mostrarDialogo(){
        setVisible(true);
    }
}

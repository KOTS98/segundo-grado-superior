package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.InputMismatchException;

public class ModificarVariableDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtVarModNumero;
    private JRadioButton rBtnVarModTrue;
    private JRadioButton rBtnVarModFalse;
    private JTextField txtVarModTexto;
    private JTextField txtVarModNombre;
    private Variable variable;

    public ModificarVariableDialog(Variable variable) {
        this.variable = variable;
        txtVarModNombre.setText(variable.getNombre());
        switch (variable.getTipoVar()){
            case "texto":
                txtVarModNumero.setEnabled(false);
                rBtnVarModTrue.setEnabled(false);
                rBtnVarModFalse.setEnabled(false);
                txtVarModTexto.setText(variable.getContenidoTexto());
                break;
            case "numero":
                txtVarModTexto.setEnabled(false);
                rBtnVarModTrue.setEnabled(false);
                rBtnVarModFalse.setEnabled(false);
                txtVarModNumero.setText(String.valueOf(variable.getContenidoNum()));
                break;
            case "booleano":
                txtVarModTexto.setEnabled(false);
                txtVarModNumero.setEnabled(false);
                if(!variable.getContenidoBooleano()){
                    rBtnVarModFalse.setSelected(true);
                }
                break;
        }
        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(450,200));
        setLocationRelativeTo(null);
        setTitle("Modificar variable");
        setResizable(false);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        variable.setNombre(txtVarModNombre.getText());
        switch (this.variable.getTipoVar()){
            case "texto":
                variable.setContenidoTxt(txtVarModTexto.getText());
                break;
            case "numero":
                boolean error;
                do{
                    error = false;
                    try{
                        variable.setContenidoNum(Integer.parseInt(txtVarModNumero.getText()));
                    }catch(InputMismatchException e){
                        error = true;
                        System.out.println("ERROR DE ENTRADA DE DATOS");
                    }

                }while(error);
                break;
            case "booleano":
                if(rBtnVarModTrue.isSelected()){
                    variable.setContenidoBool(true);
                }else{
                    variable.setContenidoBool(false);
                }
                break;
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public void mostrarDialogo(){
        setVisible(true);
    }
}

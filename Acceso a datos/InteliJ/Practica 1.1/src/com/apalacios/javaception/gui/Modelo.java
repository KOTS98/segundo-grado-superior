package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Programa;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;

/**
 * Esta clase se encarga de realizar las principales funciones del programa. Sus métodos son accesados desde el
 * controlador, quién le provee con datos de la vista.
 *
 * @author José Adrián Palacios Romo
 */
public class Modelo {
    private HashSet<Variable> variables;
    private ArrayList<Funcion> funciones;
    private DefaultListModel<Variable> dlmVariables;
    private ArrayList<String> modificaciones;
    private boolean condicionAbierta;
    private boolean bucleAbierto;
    private int contador;
    private Properties conf = new Properties();

    /**
     * Constructor de la clase, inicializa las variables de la misma, y ejecuta el método que crea el archivo de
     * configuración.
     */
    public Modelo(){
        this.variables = new HashSet<>();
        this.funciones = new ArrayList<>();
        this.dlmVariables = new DefaultListModel<>();
        this.modificaciones = new ArrayList<>();
        this.condicionAbierta = false;
        this.bucleAbierto = false;
        this.contador = 0;
        crearProperties();
    }

    /**
     * Comprueba si existe o no un archivo de configuración y en caso de que no exista, lo crea y lo dota de los
     * parámetros necesarios.
     */
    private void crearProperties(){
        if (!new File("properties.conf").exists()){
            try {
                conf.setProperty("ultimo_archivo", "");
                conf.setProperty("guardar_auto", "");
                conf.setProperty("cargar_auto", "");
                conf.setProperty("ruta_defecto", "");
                FileWriter fw;
                fw = new FileWriter("properties.conf");
                conf.store(fw, "");
            } catch (IOException e) {
                e.printStackTrace();
                Util.mostrarMensajeError("Error al crear el archivo de configuración.\n Los parámetros de configuración" +
                        "no serán utilizados.");
            }
        }
    }

    /**
     * Devuelve el ArrayList<String> Que contiene las modificaciones que se realizan durante la ejecución del programa.
     *
     * @return modificaciones Representa el ArrayList de String donde se guardan los cambios al programa.
     */
    ArrayList<String> getModificaciones(){
        return modificaciones;
    }

    /**
     * Crea en el editor de texto la sintaxis necesaria para declarar una variable de tipo String
     * en Java, desplegando ante el usuario un menú que le permite establecer el nombre de la
     * variable y su contenido.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void nuevaVariableTexto(JTextArea txtCodigo){
        Variable variable = new Variable();
        CrearVariablieTextDialog dialog = new CrearVariablieTextDialog(variable);
        dialog.mostrarDialogo();
        if(!variable.getNombre().equalsIgnoreCase("")){
            variables.add(variable);
            txtCodigo.setText(txtCodigo.getText() + "\n" + calcularTabulacion(txtCodigo) + variable.variableTexto());
        }
        addModificaciones(txtCodigo);
    }

    /**
     * Crea en el editor de texto la sintaxis necesaria para declarar una variable de tipo int
     * en Java, desplegando ante el usuario un menú que le permite establecer el nombre de la
     * variable y su contenido.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void nuevaVariableNumero(JTextArea txtCodigo){
        Variable variable = new Variable();
        CrearVariableNumDialog dialog = new CrearVariableNumDialog(variable);
        dialog.mostrarDialogo();
        if(!variable.getNombre().equalsIgnoreCase("")){
            variables.add(variable);
            txtCodigo.setText(txtCodigo.getText() + "\n" + calcularTabulacion(txtCodigo) + variable.variableTexto());
        }
        addModificaciones(txtCodigo);
    }

    /**
     * Crea en el editor de texto la sintaxis necesaria para declarar una variable de tipo boolean
     * en Java, desplegando ante el usuario un menú que le permite establecer el nombre de la
     * variable y su estado (true o false).
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void nuevaVariableBoolean(JTextArea txtCodigo){
        Variable variable = new Variable();
        CrearVariableBoolDialog dialog = new CrearVariableBoolDialog(variable);
        dialog.mostrarDialogo();
        if(!variable.getNombre().equalsIgnoreCase("")){
            variables.add(variable);
            txtCodigo.setText(txtCodigo.getText() + "\n" + calcularTabulacion(txtCodigo) + variable.variableTexto());
        }
        addModificaciones(txtCodigo);
    }

    /**
     * Crea en el editor de texto la sintaxis necesaria para mostrar un mensaje por terminal en
     * Java, desplegando ante el usuario un menú que le permite establecer el nombre de la función,
     * (no visible en el editor, pero si en el listado de funciones), el mensaje que se mostrará por
     * terminal y la posibilidad de concatenar dicho mensaje con el contenido de una variable.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void nuevaFuncionMensaje(JTextArea txtCodigo){
        dlmVariables.clear();
        for (Variable variable : variables) {
            dlmVariables.addElement(variable);
        }
        Funcion funcion = new Funcion();
        CrearFuncionMensajeDialog dialog = new CrearFuncionMensajeDialog(funcion, dlmVariables);
        dialog.mostrarDialogo();
        if (!funcion.getNombre().equalsIgnoreCase("")) {
            txtCodigo.setText(txtCodigo.getText() + "\n" + calcularTabulacion(txtCodigo) + funcion.funcionTexto());
            funciones.add(funcion);
        }
        addModificaciones(txtCodigo);
    }

    /**
     * Crea en el editor de texto la sintaxis necesaria para mostrar un mensaje por terminal en
     * Java, desplegando ante el usuario un menú que le permite establecer el nombre de la función,
     * (no visible en el editor, pero si en el listado de funciones), las dos variables (de tipo int)
     * con las que se desea operar, y el tipo de operación a realizar (suma, resta, multiplicación
     * o división).
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void nuevaFuncionOperacion(JTextArea txtCodigo){
        formatearDLMVariables();
        Funcion funcion = new Funcion();
        CrearFuncionOperacionDialog dialog = new CrearFuncionOperacionDialog(funcion, dlmVariables);
        dialog.mostrarDialogo();
        if (!funcion.getNombre().equalsIgnoreCase("")) {
            txtCodigo.setText(txtCodigo.getText() + "\n" + calcularTabulacion(txtCodigo) + funcion.funcionTexto());
            funciones.add(funcion);
        }
        addModificaciones(txtCodigo);
    }

    /**
     * Crea en el editor de texto la sintaxis necesaria para mostrar un mensaje por terminal en
     * Java, desplegando ante el usuario un menú que le permite establecer el nombre de la función,
     * (no visible en el editor, pero si en el listado de funciones), las variables (de tipo int)
     * con las que se desea operar, y el condicionante a realizar ("mayor que", "menor que", "igual a"
     * o "diferente de").
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void nuevaFuncionCondicional(JTextArea txtCodigo){
        if(!condicionAbierta){
            formatearDLMVariables();
            Funcion funcion = new Funcion();
            CrearFuncionCondicionDialog dialog = new CrearFuncionCondicionDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            if (!funcion.getNombre().equalsIgnoreCase("")){
                txtCodigo.setText(txtCodigo.getText() + "\n" + calcularTabulacion(txtCodigo) + funcion.funcionTexto());
                funciones.add(funcion);
            }
            condicionAbierta = true;

        //En caso de que exista una función condicional abierta, en lugar de abrir una nueva, cierra la
        //existente.
        }else{
            txtCodigo.setText(cerrarLlave(txtCodigo));
            condicionAbierta = false;
        }
        addModificaciones(txtCodigo);
    }

    /**
     * Crea en el editor de texto la sintaxis necesaria para mostrar un mensaje por terminal en
     * Java, desplegando ante el usuario un menú que le permite establecer el nombre de la función,
     * (no visible en el editor, pero si en el listado de funciones) y la variable (de tipo int)
     * de la que se obtendrá el número de iteraciones del bucle.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void nuevaFuncionBucle(JTextArea txtCodigo){
        if(!bucleAbierto){
            formatearDLMVariables();
            Funcion funcion = new Funcion();
            CrearFuncionBucleDialog dialog = new CrearFuncionBucleDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            if (!funcion.getNombre().equalsIgnoreCase("")){
                txtCodigo.setText(txtCodigo.getText() + "\n" + calcularTabulacion(txtCodigo) + funcion.funcionTexto());
                funciones.add(funcion);
            }
            bucleAbierto = true;

        //En caso de que exista una función de bucle abierta, en lugar de abrir una nueva, cierra la
        //existente.
        }else{
            txtCodigo.setText(cerrarLlave(txtCodigo));
            bucleAbierto = false;
        }
        addModificaciones(txtCodigo);
    }

    /**
     * Deshace la última acción realizada por el usuario retrocediendo una posición en
     * el ArrayList de modificaciones y cargando su contenido.
     * En caso de que el usuario desee deshacer múltiples acciones consecutivas,
     * este método permite retroceder en sus acciones una una.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void deshacer(JTextArea txtCodigo){
        if(contador > 0){
            contador--;
            txtCodigo.setText(modificaciones.get(contador));
        }
    }

    /**
     * Rehace la última acción deshecha por el usuario avanzando una posición en
     * el ArrayList de modificaciones y cargando su contenido.
     * En caso de que el usuario haya deshecho múltiples acciones consecutivas,
     * este método permite restaurarlas una a una.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void rehacer(JTextArea txtCodigo){
        if(contador < modificaciones.size() - 1){
            contador++;
            txtCodigo.setText(modificaciones.get(contador));
        }
    }

    /**
     * Calcula la tabulación necesaria para el siguiente elemento que se vaya a introducir.
     * Esto se consigue leyendo el código y buscando las llaves de apertura y cierre.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     * @return String Representa el espacio necesario para plasmar la tabulación correcta
     * basándose en el número de llaves de apertura y cierre.
     */
    private String calcularTabulacion(JTextArea txtCodigo){
        final String TABULADOR = "        ";
        String[] caracteres = txtCodigo.getText().split("");
        StringBuilder tabulacion = new StringBuilder();
        String[] caracteresTab;
        for(String caracter : caracteres){
            if(caracter.equalsIgnoreCase("{")){
                tabulacion.append(TABULADOR);
            }
            if(caracter.equalsIgnoreCase("}")){
                caracteresTab = tabulacion.toString().split("");
                tabulacion = new StringBuilder();
                for(int j = 0; j < (caracteresTab.length - TABULADOR.length()); j++){
                    tabulacion.append(" ");
                }
            }
        }
        return tabulacion.toString();
    }

    /**
     * Calcula la tabulación correcta para la llave de cierre de una función condicional o
     * de bucle, haciendo más legible el código.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     * @return String Representa una llave de cierre con su correspondiente tabulación.
     */
    private String cerrarLlave(JTextArea txtCodigo){
        final String TABULADOR = "        ";
        StringBuilder tabulacion = new StringBuilder(calcularTabulacion(txtCodigo));
        String[] caracteresTab;
        caracteresTab = tabulacion.toString().split("");
        tabulacion = new StringBuilder();
        for(int j = 0; j < (caracteresTab.length - TABULADOR.length()); j++){
            tabulacion.append(" ");
        }
        tabulacion.append("}");
        return txtCodigo.getText() + "\n" + tabulacion.toString();
    }

    /**
     * Limpia la lista de variables visible para el usuario, y carga únicamente con
     * las variables númericas, evitando conflictos como "InputMissMatchException",
     * y simplificando la selección de variable para el usuario.
     */
    private void formatearDLMVariables(){
        dlmVariables.clear();
        for (Variable variable : variables) {
            if(variable.getTipoVar().equalsIgnoreCase("numero")) {
                dlmVariables.addElement(variable);
            }
        }
    }

    /**
     * Carga la última modificación realizada a un ArrayList y actualiza el contador
     * que controla el estado actual del proyecto.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    private void addModificaciones(JTextArea txtCodigo){
        modificaciones.add(txtCodigo.getText());
        contador++;
    }

    /**
     * Recibe el archivo de configuracion con las opciones del programa,
     * despliega un meno que permite modificar todas estas opciones y permite
     * exportar los proyectos a formato XML y SQL.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void menuOpciones(JTextArea txtCodigo){
        MenuOpcionesDialog dialog = new MenuOpcionesDialog(conf, variables, funciones, txtCodigo);
        dialog.mostrarDialogo();
    }

    /**
     * Crea un objeto de tipo programa que contiene el codigo generado por la aplicación y un listado de las
     * variables y funciones utilizadas. Después guarda el programa con el nombre y ubicación deseada por el usuario.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void guardarArchivo(JTextArea txtCodigo){
        JFileChooser selector = new JFileChooser();
        int opcion = selector.showSaveDialog(null);
        if(opcion == JFileChooser.APPROVE_OPTION){
            try {
                conf.setProperty("ultimo_archivo", selector.getSelectedFile().getPath());
                FileWriter fw = new FileWriter("properties.conf");
                conf.store(fw, "");
                fw.close();
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(selector.getSelectedFile().getPath()));
                Programa programa = new Programa(txtCodigo.getText(), variables, funciones);
                oos.writeObject(programa);
                oos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                Util.mostrarMensajeError("Error de escritura de archivo.");
            }
        }
    }

    /**
     * Crea un objeto de tipo programa que es igual a los datos recibidos del fichero que se decide cargar.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void cargarArchivo(JTextArea txtCodigo){
        JFileChooser selector = new JFileChooser();
        int opcion = selector.showOpenDialog(null);
        if (opcion == JFileChooser.APPROVE_OPTION){
            try{
                conf.setProperty("ultimo_archivo", selector.getSelectedFile().getPath());
                FileWriter fw = new FileWriter("properties.conf");
                conf.store(fw, "");
                fw.close();
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(selector.getSelectedFile().getPath()));
                Programa programa = (Programa) ois.readObject();
                ois.close();
                txtCodigo.setText(programa.getCodigo());
                variables = programa.getListaVariables();
                funciones = programa.getListaFunciones();
            }catch (FileNotFoundException e){
                e.printStackTrace();
                Util.mostrarMensajeError("Error. El archivo especificado no existe.");
            }catch (IOException | ClassNotFoundException e){
                e.printStackTrace();
                Util.mostrarMensajeError("Error de lectura de archivo.");
            }
        }
    }

    /**
     * Muestra un diálogo con todas las variables del programa el cuál permite modificarlas y eliminarlas.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void verVariables(JTextArea txtCodigo){
        VerVariablesDialog dialog = new VerVariablesDialog(txtCodigo, variables);
        dialog.mostrarDialogo();
    }

    /**
     * Muestra un diálogo con todas las funciones del programa el cuál permite modificarlas y eliminarlas.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void verFunciones(JTextArea txtCodigo){
        dlmVariables.clear();
        for (Variable variable : variables) {
            dlmVariables.addElement(variable);
        }
        VerFuncionesDialog dialog = new VerFuncionesDialog(txtCodigo, funciones, variables, dlmVariables);
        dialog.mostrarDialogo();
    }

    /**
     * Carga el último archivo generado por el programa, leyendo la ruta desde el archivo de configuración. En caso
     * de que no haya una ruta guardada, o no se encuentre el archivo en la ruta especificada, se generará un nuevo
     * archivo.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void cargarUltimoArchivo(JTextArea txtCodigo) {
        try{
            FileReader fr = new FileReader("conf.properties");
            conf.load(fr);
            if(!conf.getProperty("ultimo_archivo").equalsIgnoreCase("")){
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(conf.getProperty("ultimo_archivo")));
                fr.close();
                Programa programa = (Programa) ois.readObject();
                ois.close();
                txtCodigo.setText(programa.getCodigo());
                variables = programa.getListaVariables();
                funciones = programa.getListaFunciones();
            }
            fr.close();
        }catch (FileNotFoundException e){
            Util.mostrarMensajeError("Error. No se encuentra el último archivo, se cargará un archivo nuevo.");
            txtCodigo.setText(Util.textoPorDefecto());
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
            Util.mostrarMensajeError("Error de lectura de archivo. Se cargará un archivo nuevo.");
            txtCodigo.setText(Util.textoPorDefecto());
        }
    }

    /**
     * Determina basándose en el archivo de configuración, si debe o no de guardar automáticamente el archivo con
     * el que se está trabajando, en caso afirmativo, determina la ruta de guardado por defecto especificada en el
     * archivo de configuración.
     *
     * @param txtCodigo Representa el codigo que se genera en la vista principal del programa.
     */
    void guardarDefault(JTextArea txtCodigo) {
        try {
            FileReader fr = new FileReader("properties.conf");
            conf.load(fr);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(conf.getProperty("ruta_defecto")));
            fr.close();
            Programa programa = new Programa(txtCodigo.getText(), variables, funciones);
            oos.writeObject(programa);
            oos.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            Util.mostrarMensajeError("Error de escritura de archivo.");
        }
    }
}
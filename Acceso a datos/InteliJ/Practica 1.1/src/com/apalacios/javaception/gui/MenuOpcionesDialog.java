package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Programa;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;

public class MenuOpcionesDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JCheckBox cbGuardarAuto;
    private JCheckBox cbCargarAuto;
    private JButton btnSelectRutaDefecto;
    private String ruta_defecto;
    private JButton btnExportarXML;
    private JButton btnExportarSQL;
    private Properties conf;


    MenuOpcionesDialog(Properties conf, HashSet<Variable> variables, ArrayList<Funcion> funciones, JTextArea txtCodigo) {
        ruta_defecto = "";
        try {
            FileReader fr = new FileReader("properties.conf");
            conf.load(fr);
            cbGuardarAuto.setSelected(Boolean.parseBoolean(conf.getProperty("guardar_auto")));
            cbCargarAuto.setSelected(Boolean.parseBoolean(conf.getProperty("cargar_auto")));
        } catch (FileNotFoundException e) {
            Util.mostrarMensajeError("Error. Archivo de configuración \"properties.conf\" no encontrado");
        } catch (IOException e) {
            Util.mostrarMensajeError("Error al leer el archivo de configuración");
        }

        this.conf = conf;
        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(385, 250));
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Opciones");
        getRootPane().setDefaultButton(buttonOK);

        btnSelectRutaDefecto.addActionListener(e -> {
            JFileChooser selector = new JFileChooser();
            int opcion = selector.showOpenDialog(null);
            if (opcion == JFileChooser.APPROVE_OPTION){
                this.ruta_defecto = selector.getSelectedFile().getPath();
            }
        });

        btnExportarXML.addActionListener(e -> {
            JFileChooser selector = new JFileChooser();
            int opcion = selector.showSaveDialog(null);
            if(opcion == JFileChooser.APPROVE_OPTION){
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = null;
                try {
                    db = dbf.newDocumentBuilder();
                } catch (ParserConfigurationException ex) {
                    Util.mostrarMensajeError("Error de conversión de datos");
                }
                assert db != null;
                DOMImplementation dom = db.getDOMImplementation();

                //Crear el documento XML y el nodo raiz "programas"
                Document doc = dom.createDocument(null, "xml", null);
                Element raiz = doc.createElement("programa");
                doc.getDocumentElement().appendChild(raiz);

                //Crear el resto de nodos y el texto del documento
                Element nodoFecha;

                Element nodoVariable;
                Element nodoDatosV;

                Element nodoFuncion;
                Element nodoDatosF ;

                Element nodoCodigo;

                Text texto;

                //Obtengo la fecha actual
                Date fecha = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("E dd.MM.yyyy 'a las' hh:mm:ss a");
                String strFecha = ft.format(fecha);

                //Añado la fecha al XML
                nodoFecha = doc.createElement("fecha");
                raiz.appendChild(nodoFecha);
                texto = doc.createTextNode(strFecha);
                nodoFecha.appendChild(texto);

                //Recorrer la lista de variables y crear un subnodo para cada elemento
                for (Variable variable : variables) {
                    nodoVariable = doc.createElement("variable");
                    raiz.appendChild(nodoVariable);

                    //Dotar de un nombre y un valor a cada variable dependiendo del tipo de dato que contiene creando
                    //un nuevo subnodo
                    nodoDatosV = doc.createElement("nombre");
                    nodoVariable.appendChild(nodoDatosV);
                    texto = doc.createTextNode(variable.getNombre());
                    nodoDatosV.appendChild(texto);

                    nodoDatosV = doc.createElement("valor");
                    nodoVariable.appendChild(nodoDatosV);
                    switch (variable.getTipoVar()){
                        case "texto":
                            texto = doc.createTextNode(variable.getContenidoTexto());
                            break;
                        case "numero":
                            texto = doc.createTextNode(String.valueOf(variable.getContenidoNum()));
                            break;
                        case "booleano":
                            texto = doc.createTextNode(String.valueOf(variable.getContenidoBooleano()));
                            break;
                    }
                    nodoDatosV.appendChild(texto);
                }

                //Recorrer la lista de funciones y crear un subnodo para cada elemento
                for (Funcion funcion : funciones){
                    nodoFuncion = doc.createElement("funcion");
                    raiz.appendChild(nodoFuncion);

                    //Dotar de un nombre y un contenido a cada función dependiendo del tipo creando
                    //un nuevo subnodo
                    nodoDatosF = doc.createElement("nombre");
                    nodoFuncion.appendChild(nodoDatosF);
                    texto = doc.createTextNode(funcion.getNombre());
                    nodoDatosF.appendChild(texto);

                    nodoDatosF = doc.createElement("contenido");
                    nodoFuncion.appendChild(nodoDatosF);
                    switch (funcion.getTipoFuncion()){
                        case "mensaje":
                            texto = doc.createTextNode(funcion.getCadenaMensaje());
                            break;
                        case "operacion":
                            texto = doc.createTextNode(funcion.getTipoOperacion());
                            break;
                        case "condicion":
                            texto = doc.createTextNode(funcion.getTipoCondicion());
                            break;
                        case "bucle":
                            texto = doc.createTextNode("Contador = " + funcion.getContadorBucle());
                            break;
                    }
                    nodoDatosF.appendChild(texto);
                }

                //Añado el nodo que contiene el codigo del programa
                nodoCodigo = doc.createElement("codigo");
                raiz.appendChild(nodoCodigo);
                texto = doc.createTextNode(txtCodigo.getText());
                nodoCodigo.appendChild(texto);

                //Finaliza el documento XML y lo guarda en un fichero de texto
                Source src = new DOMSource(doc);
                Result resultado = new StreamResult(new File(selector.getSelectedFile().getPath() + ".xml"));

                Transformer transformer;
                try {
                    transformer = TransformerFactory.newInstance().newTransformer();
                    transformer.transform(src, resultado);
                } catch (TransformerException ex) {
                    Util.mostrarMensajeError("Error al transformar el fichero XML");
                }
            }
        });

        btnExportarSQL.addActionListener(e -> {
            String sentenciaSQL = "CREATE DATABASE programa;\n" +
                    "USE programa;\n" +
                    "CREATE TABLE fecha (\n" +
                    "   fehca_creacion DATE\n" +
                    ");\n" +
                    "CREATE TABLE variables (\n" +
                    "   nombre VARCHAR(50),\n" +
                    "   valor VARCHAR(50)\n" +
                    ");\n" +
                    "CREATE TABLE funciones (\n" +
                    "   nombre VARCHAR(50),\n" +
                    "   contenido VARCHAR(50)\n" +
                    ");\n";
            for (Variable variable : variables) {
                switch (variable.getTipoVar()){
                    case "texto":
                        sentenciaSQL += "INSERT INTO variables(nombre, valor)\n" +
                                "VALUES('" + variable.getNombre() + "', '" + variable.getContenidoTexto() + "');\n";
                        break;
                    case "numero":
                        sentenciaSQL += "INSERT INTO variables(nombre, valor)\n" +
                                "VALUES('" + variable.getNombre() + "', '" + variable.getContenidoNum() + "');\n";
                        break;
                    case "booleano":
                        sentenciaSQL += "INSERT INTO variables(nombre, valor)\n" +
                                "VALUES('" + variable.getNombre() + "', '" + variable.getContenidoBooleano() + "');\n";
                        break;
                }
            }
            for (Funcion funcion : funciones) {
                switch (funcion.getTipoFuncion()){
                    case "mensaje":
                        sentenciaSQL += "INSERT INTO funciones(nombre, contenido)\n" +
                                "VALUES('" + funcion.getNombre() + "', '" + funcion.getCadenaMensaje() +"');\n";
                        break;
                    case "operacion":
                        sentenciaSQL += "INSERT INTO funciones(nombre, contenido)\n" +
                                "VALUES('" + funcion.getNombre() + "', '" + funcion.getTipoOperacion() +"');\n";
                        break;
                    case "condicion":
                        sentenciaSQL += "INSERT INTO funciones(nombre, contenido)\n" +
                                "VALUES('" + funcion.getNombre() + "', '" + funcion.getTipoCondicion() +"');\n";
                        break;
                    case "bucle":
                        sentenciaSQL += "INSERT INTO funciones(nombre, contenido)\n" +
                                "VALUES('" + funcion.getNombre() + "', 'Contador: " + funcion.getContadorBucle() +"')\n;";
                        break;
                }
            }

            JFileChooser selector = new JFileChooser();
            int opcion = selector.showSaveDialog(null);
            if(opcion == JFileChooser.APPROVE_OPTION){
                try {
                    BufferedWriter bw = new BufferedWriter(new FileWriter(selector.getSelectedFile().getPath() + ".sql"));
                    bw.write(sentenciaSQL);
                    bw.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                    Util.mostrarMensajeError("Error de escritura de setntencia SQL.");
                }
            }
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        conf.setProperty("guardar_auto", String.valueOf(cbGuardarAuto.isSelected()));
        conf.setProperty("cargar_auto", String.valueOf(cbCargarAuto.isSelected()));
        conf.setProperty("ruta_defecto", ruta_defecto);
        try {
            FileWriter fw = new FileWriter("properties.conf");
            conf.store(fw, "");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public void mostrarDialogo() {
        setVisible(true);
    }
}

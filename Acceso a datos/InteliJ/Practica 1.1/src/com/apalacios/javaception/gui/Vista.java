package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Programa;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;

/**
 * Esta clase representa la interfaz principal del programa.
 * @author José Adrián Palacios Romo
 */
public class Vista {
    JFrame frame;
    JTextArea txtCodigo;
    JButton btnVTexto;
    JButton btnVNumero;
    JButton btnVBooleano;
    JButton btnFMensaje;
    JButton btnFOperacion;
    JButton btnFCondicion;
    JButton btnFBucle;
    JButton btnPOpciones;
    JPanel panelPrincipal;
    JButton btnDeshacer;
    JButton btnRehacer;
    DefaultListModel<Programa> dlmProgramas;
    DefaultListModel<Funcion> dlmFuncionesDePrograma;
    DefaultListModel<Variable> dlmVariablesDePrograma;
    JMenuItem itemGuardar;
    JMenuItem itemCargar;
    JMenuItem itemVerVar;
    JMenuItem itemVerFunc;

    /**
     * Constructor de la clase, instancia el frame y ajusta sus parámetros, llama al método para crear el menú
     * superior y al método para inicializar los modelos.
     */
    public Vista() {
        frame = new JFrame("JavaCeption - Adrián Palacios");
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        crearMenu();
        frame.pack();
        frame.setVisible(true);
        inicializarModelos();
        txtCodigo.setText(Util.textoPorDefecto());
    }

    /**
     * Inicializa todas los DefaultListModel para su posterior uso en objetos de
     * tipo JList, de forma que el usuario pueda ver y operar con los datos
     * almacenados.
     */
    private void inicializarModelos() {
        dlmProgramas = new DefaultListModel<>();
        dlmFuncionesDePrograma = new DefaultListModel<>();
        dlmVariablesDePrograma = new DefaultListModel<>();
    }

    /**
     * Genera el menú superior de la interfaz y lo añade a la misma.
     */
    private void crearMenu(){
        JMenuBar barra = new JMenuBar();

        JMenu menuArchivo = new JMenu("Archivo");
        itemGuardar = new JMenuItem("Guardar");
        menuArchivo.add(itemGuardar);
        itemCargar = new JMenuItem("Cargar");
        menuArchivo.add(itemCargar);
        barra.add(menuArchivo);

        JMenu menuVer = new JMenu("Ver");
        itemVerVar = new JMenuItem("Variables");
        menuVer.add(itemVerVar);
        itemVerFunc = new JMenuItem("Funciones");
        menuVer.add(itemVerFunc);
        barra.add(menuVer);

        frame.setJMenuBar(barra);
    }
}

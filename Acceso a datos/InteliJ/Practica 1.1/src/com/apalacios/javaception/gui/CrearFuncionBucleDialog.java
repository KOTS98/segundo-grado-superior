package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrearFuncionBucleDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtNombre;
    private JButton btnSeleccionarV;
    private Variable variable;
    private Funcion funcion;
    private DefaultListModel dlmVariables;

    public CrearFuncionBucleDialog(Funcion funcion, DefaultListModel dlmVariables) {
        this.variable = new Variable();
        this.funcion = funcion;
        this.dlmVariables = dlmVariables;
        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(400, 150));
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Bucle");
        getRootPane().setDefaultButton(buttonOK);

        btnSeleccionarV.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            variable = funcion.getVariable();
            btnSeleccionarV.setText(variable.getNombre());
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        if(!variable.getNombre().equalsIgnoreCase("")){
            funcion.setNombre(txtNombre.getText());
            funcion.setContadorBucle(variable.getContenidoNum());
            funcion.setTipoFuncion("bucle");
        }else{
            Util.mostrarMensajeError("Debes seleccionar una variable.");
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public void mostrarDialogo(){
        setVisible(true);
    }
}

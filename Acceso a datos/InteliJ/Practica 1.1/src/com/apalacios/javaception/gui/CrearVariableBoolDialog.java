package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrearVariableBoolDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtNombreV;
    private JRadioButton rBtnFalso;
    private JRadioButton rBtnVerdadero;
    private Variable variable;

    CrearVariableBoolDialog(Variable variable) {
        this.variable = variable;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);
        setTitle("Variable booleana");
        setSize(new Dimension(350, 140));
        setResizable(false);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        variable.setNombre(txtNombreV.getText());
        if (rBtnVerdadero.isSelected()){
            variable.setContenidoBool(true);
        }else if (rBtnFalso.isSelected()){
            variable.setContenidoBool(false);
        }
        variable.setTipoVar("booleano");
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo() {
        setVisible(true);
    }
}
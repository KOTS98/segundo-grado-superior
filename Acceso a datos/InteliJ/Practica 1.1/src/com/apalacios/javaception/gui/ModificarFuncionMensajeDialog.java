package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ModificarFuncionMensajeDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtMensajeFuncion;
    private JTextField txtNombreFuncion;
    private JButton btnSelectVariable;
    private Funcion funcion;
    private Variable variable;

    public ModificarFuncionMensajeDialog(Funcion funcion, DefaultListModel<Variable> dlmVariables) {
        this.funcion = funcion;
        txtNombreFuncion.setText(funcion.getNombre());
        txtMensajeFuncion.setText(funcion.getCadenaMensaje());
        variable = funcion.getVariable();
        if (!funcion.getVariable().getNombre().equalsIgnoreCase("")){
            btnSelectVariable.setText(funcion.getVariable().getNombre());
        }

        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(450,200));
        setLocationRelativeTo(null);
        setTitle("Modificar función de mensaje");
        setResizable(false);
        getRootPane().setDefaultButton(buttonOK);

        btnSelectVariable.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(this.funcion, dlmVariables);
            dialog.mostrarDialogo();
            if(!funcion.getVariable().getNombre().equalsIgnoreCase("")){
                variable = funcion.getVariable();
                btnSelectVariable.setText(variable.getNombre());
            }else{
                btnSelectVariable.setText("Seleccionar variable...");
            }
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        funcion.setVariable(variable);
        funcion.setNombre(txtNombreFuncion.getText());
        funcion.setCadenaMensaje(txtMensajeFuncion.getText());
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public void mostrarDialogo() {
        setVisible(true);
    }
}

package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashSet;

public class VerFuncionesDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JList<Funcion> listFunciones;
    private JButton btnModificarFunc;
    private JButton btnEliminarFunc;
    private DefaultListModel<Funcion> dlmFunciones;
    private JTextArea txtCodigo;
    private ArrayList<Funcion> funciones;

    VerFuncionesDialog(JTextArea txtCodigo, ArrayList<Funcion> funciones, HashSet<Variable> variables, DefaultListModel<Variable> dlmVariables) {
        this.txtCodigo = txtCodigo;
        this.funciones = funciones;
        dlmFunciones = new DefaultListModel<>();
        for (Funcion funcion : funciones) {
            dlmFunciones.addElement(funcion);
        }
        listFunciones.setModel(dlmFunciones);
        setContentPane(contentPane);
        setSize(new Dimension(450,200));
        setModal(true);
        setLocationRelativeTo(null);
        setTitle("Lista de funciones");
        setResizable(false);
        getRootPane().setDefaultButton(buttonOK);

        btnModificarFunc.addActionListener(e -> {
            if(listFunciones.getSelectedValue() != null){
                String cadenaOriginal = listFunciones.getSelectedValue().funcionTexto();
                switch(listFunciones.getSelectedValue().getTipoFuncion()){
                    case "mensaje": {
                        ModificarFuncionMensajeDialog dialog = new ModificarFuncionMensajeDialog(listFunciones.getSelectedValue(), dlmVariables);
                        dialog.mostrarDialogo();
                    }
                        break;
                    case "operacion": {
                        ModificarFuncionOperacionDialog dialog = new ModificarFuncionOperacionDialog(listFunciones.getSelectedValue(), dlmVariables);
                        dialog.mostrarDialogo();
                    }
                        break;
                    case "condicion": {
                        dlmVariables.clear();
                        for (Variable variable : variables) {
                            if(variable.getTipoVar().equalsIgnoreCase("numero")) {
                                dlmVariables.addElement(variable);
                            }
                        }
                        ModificarFuncionCondicionDialog dialog = new ModificarFuncionCondicionDialog(listFunciones.getSelectedValue(), dlmVariables);
                        dialog.mostrarDialogo();
                    }
                        break;
                    case "bucle": {
                        dlmVariables.clear();
                        for (Variable variable : variables) {
                            if(variable.getTipoVar().equalsIgnoreCase("numero")) {
                                dlmVariables.addElement(variable);
                            }
                        }
                        ModificarFuncionBucleDialog dialog = new ModificarFuncionBucleDialog(listFunciones.getSelectedValue(), dlmVariables);
                        dialog.mostrarDialogo();
                    }
                        break;
                }
                Funcion funcionMod = listFunciones.getSelectedValue();
                String codigo = this.txtCodigo.getText();
                String cadenaRemplazar = funcionMod.funcionTexto();
                this.txtCodigo.setText(codigo.replaceFirst(cadenaOriginal, cadenaRemplazar));
                reiniciarModelo();
            }
        });

        btnEliminarFunc.addActionListener(e -> {
            if (listFunciones.getSelectedValue() != null){
                String cadenaOriginal = listFunciones.getSelectedValue().funcionTexto();
                String codigo = this.txtCodigo.getText();
                this.txtCodigo.setText(codigo.replaceAll(cadenaOriginal, ""));
                this.funciones.remove(listFunciones.getSelectedValue());
                reiniciarModelo();
            }
        });

        buttonOK.addActionListener(e -> onOK());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onOK();
            }
        });
    }

    private void reiniciarModelo(){
        dlmFunciones.clear();
        for (Funcion funcion : this.funciones) {
            dlmFunciones.addElement(funcion);
        }
    }

    private void onOK() {

        dispose();
    }

    public void mostrarDialogo() {
        setVisible(true);
    }
}

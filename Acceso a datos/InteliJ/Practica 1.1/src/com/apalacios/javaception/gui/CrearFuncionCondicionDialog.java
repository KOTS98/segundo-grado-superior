package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrearFuncionCondicionDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton btnSeleccionarV1;
    private JButton btnSeleccionarV2;
    private JRadioButton rBtnMayor;
    private JRadioButton rBtnMenor;
    private JRadioButton rBtnIgual;
    private JRadioButton rBtnDiferente;
    private JTextField txtNombre;
    private Variable variable1;
    private Variable variable2;
    private Funcion funcion;

    CrearFuncionCondicionDialog(Funcion funcion, DefaultListModel dlmVariables) {
        this.variable1 = new Variable();
        this.variable2 = new Variable();
        this.funcion = funcion;
        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(415, 250));
        setResizable(false);
        setTitle("Condicional");
        setLocationRelativeTo(null);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        btnSeleccionarV1.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            variable1 = funcion.getVariable();
            btnSeleccionarV1.setText(variable1.getNombre());
        });

        btnSeleccionarV2.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            variable2 = funcion.getVariable();
            btnSeleccionarV2.setText(variable2.getNombre());
        });

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    void mostrarDialogo(){
        setVisible(true);
    }

    private void onOK() {
        if(!variable1.getNombre().equalsIgnoreCase("") &&
                !variable2.getNombre().equalsIgnoreCase("")){
            funcion.setTipoFuncion("condicion");
            funcion.setNombre(txtNombre.getText());
            funcion.setVariable(variable1);
            funcion.setVariable2(variable2);
            if (rBtnMayor.isSelected()){
                funcion.setTipoCondicion("mayor");
            }else if(rBtnMenor.isSelected()){
                funcion.setTipoCondicion("menor");
            }else if(rBtnIgual.isSelected()){
                funcion.setTipoCondicion("igual");
            }else if(rBtnDiferente.isSelected()){
                funcion.setTipoCondicion("diferente");
            }
        }else{
            Util.mostrarMensajeError("Debes establecer las dos variables (Primer\n" +
                    "valor y segundo valor) para hacer la operación.");
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }
}

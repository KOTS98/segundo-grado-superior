package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;

public class VerVariablesDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JList<Variable> listVariables;
    private JButton btnModificarVar;
    private JButton btnEliminarVar;
    private DefaultListModel<Variable> dlmVariables;
    private JTextArea txtCodigo;
    private HashSet<Variable> variables;

    VerVariablesDialog(JTextArea txtCodigo, HashSet<Variable> variables) {
        this.txtCodigo = txtCodigo;
        this.variables = variables;
        dlmVariables = new DefaultListModel<>();
        for (Variable variable : variables) {
            dlmVariables.addElement(variable);
        }
        listVariables.setModel(dlmVariables);
        setContentPane(contentPane);
        setSize(new Dimension(450,200));
        setModal(true);
        setLocationRelativeTo(null);
        setTitle("Lista de variables");
        setResizable(false);
        getRootPane().setDefaultButton(buttonOK);

        btnModificarVar.addActionListener(e -> {
            if(listVariables.getSelectedValue() != null){
                String cadenaOriginal = listVariables.getSelectedValue().variableTexto();
                ModificarVariableDialog dialog = new ModificarVariableDialog(listVariables.getSelectedValue());
                dialog.mostrarDialogo();
                Variable variableMod = listVariables.getSelectedValue();
                String codigo = this.txtCodigo.getText();
                String cadenaRemplazar = variableMod.variableTexto();
                this.txtCodigo.setText(codigo.replaceAll(cadenaOriginal, cadenaRemplazar));
                reiniciarModelo();
            }
        });

        btnEliminarVar.addActionListener(e -> {
            if(listVariables.getSelectedValue() != null){
                String cadenaOriginal = listVariables.getSelectedValue().variableTexto();
                String codigo = this.txtCodigo.getText();
                this.txtCodigo.setText(codigo.replaceAll(cadenaOriginal, ""));
                this.variables.remove(listVariables.getSelectedValue());
                reiniciarModelo();
            }
        });

        buttonOK.addActionListener(e -> onOK());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onOK();
            }
        });
    }

    private void reiniciarModelo(){
        dlmVariables.clear();
        for (Variable variable : this.variables) {
            dlmVariables.addElement(variable);
        }
    }

    private void onOK() {
        dispose();
    }

    public void mostrarDialogo() {
        setVisible(true);
    }
}

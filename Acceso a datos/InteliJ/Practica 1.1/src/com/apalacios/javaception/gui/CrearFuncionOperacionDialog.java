package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;
import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrearFuncionOperacionDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtNombre;
    private JRadioButton rBtnSuma;
    private JRadioButton rBtnResta;
    private JRadioButton rBtnMult;
    private JRadioButton rBtnDiv;
    private JButton btnSeleccionarV1;
    private JButton btnSeleccionarV2;
    private JButton btnSeleccionarVR;
    private Funcion funcion;
    private Variable variable1;
    private Variable variable2;
    private Variable resultado;

    CrearFuncionOperacionDialog(Funcion funcion, DefaultListModel dlmVariables) {
        this.variable1 = new Variable();
        this.variable2 = new Variable();
        this.resultado = new Variable();
        this.funcion = funcion;
        setContentPane(contentPane);
        setTitle("Operación matemática");
        setSize(new Dimension(445  , 290));
        setLocationRelativeTo(null);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        btnSeleccionarV1.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            variable1 = funcion.getVariable();
            btnSeleccionarV1.setText(variable1.getNombre());
        });

        btnSeleccionarV2.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            variable2 = funcion.getVariable();
            btnSeleccionarV2.setText(variable2.getNombre());
        });

        btnSeleccionarVR.addActionListener(e -> {
            SeleccionarVariableDialog dialog = new SeleccionarVariableDialog(funcion, dlmVariables);
            dialog.mostrarDialogo();
            resultado = funcion.getVariable();
            btnSeleccionarVR.setText(resultado.getNombre());
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        if(!variable1.getNombre().equalsIgnoreCase("") &&
        !variable2.getNombre().equalsIgnoreCase("") &&
        !resultado.getNombre().equalsIgnoreCase("")){
            funcion.setTipoFuncion("operacion");
            funcion.setNombre(txtNombre.getText());
            funcion.setVariable(variable1);
            funcion.setVariable2(variable2);
            funcion.setVariable3(resultado);
            if (rBtnSuma.isSelected()){
                funcion.setTipoOperacion("suma");
            }else if(rBtnResta.isSelected()){
                funcion.setTipoOperacion("resta");
            }else if(rBtnMult.isSelected()){
                funcion.setTipoOperacion("mult");
            }else if(rBtnDiv.isSelected()){
                funcion.setTipoOperacion("div");
            }
        }else{
            Util.mostrarMensajeError("Debes establecer las tres variables (Primer\n" +
                    "valor, segundo valor y resultado) para\n" +
                    "hacer la operación.");
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo() {
        setVisible(true);
    }
}

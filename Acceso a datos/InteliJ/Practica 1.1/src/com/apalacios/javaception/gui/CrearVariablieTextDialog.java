package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CrearVariablieTextDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtNombreV;
    private JTextField txtTextoV;
    private Variable variable;

    CrearVariablieTextDialog(Variable variable) {
        this.variable = variable;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);
        setTitle("Variable de texto");
        setSize(new Dimension(350, 140));
        setResizable(false);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        variable.setNombre(txtNombreV.getText());
        variable.setContenidoTxt(txtTextoV.getText());
        variable.setTipoVar("texto");
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo() {
        setVisible(true);
    }
}

package com.apalacios.javaception.gui;

import com.apalacios.javaception.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Clase Controlador, recibe una clase Vista y una clase Modelo, actúa como intermediario
 * para comunicar la vista y el modelo. También crea un ArrayList de botones donde se
 * almacenarán los botones del objeto vista.
 */
public class Controlador implements ActionListener, WindowListener {
    private Vista vista;
    private Modelo modelo;
    private ArrayList<JButton> listaBotones;
    private ArrayList<JMenuItem> listaItems;
    private Properties conf = new Properties();

    /**
     * Constructor de la clase, instancia la vista, el modelo, el ArrayList de botones y
     * añade el estado actual del area de texto "txtCodigo" al ArrayList de modificaciones
     * en el objeto modelo (Esto se usa para poder deshacer y rehacer cambios).
     * @param vista Representa la iterfaz gráfica del programa
     * @param modelo Es clase que se encarga de realizar las principales funciones del programa
     */
    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        this.modelo.getModificaciones().add(vista.txtCodigo.getText());
        this.listaBotones = new ArrayList<>();
        this.listaItems = new ArrayList<>();
        addActionListener(this);
    }

    /**
     * Añade todos los botones de la vista al ArrayList de botones, les añade un listener y
     * les establece un action command igual al nombre del boton.
     * @param listener Envía una señal cada vez que uno de los objetos que escucha, cambia de estado
     */
    private void addActionListener(ActionListener listener){

        //Se añaden todos los botones de la vista principal al ArrayList de botones.
        listaBotones.add(vista.btnVTexto);
        listaBotones.add(vista.btnVNumero);
        listaBotones.add(vista.btnVBooleano);
        listaBotones.add(vista.btnFMensaje);
        listaBotones.add(vista.btnFOperacion);
        listaBotones.add(vista.btnFCondicion);
        listaBotones.add(vista.btnFBucle);
        listaBotones.add(vista.btnDeshacer);
        listaBotones.add(vista.btnRehacer);
        listaBotones.add(vista.btnPOpciones);

        //Se aladen todos los items de la vista principal al ArrayList de items.
        listaItems.add(vista.itemGuardar);
        listaItems.add(vista.itemCargar);
        listaItems.add(vista.itemVerVar);
        listaItems.add(vista.itemVerFunc);

        //Recorre el ArrayList de botones, les añade un listener y les establece un action command.
        for (JButton boton : listaBotones) {
            boton.addActionListener(listener);
            boton.setActionCommand(boton.getName());
        }

        //Recorre el ArrayList de items, les añade un listener y les establece un action command.
        for (JMenuItem item : listaItems) {
            item.addActionListener(listener);
            item.setActionCommand(item.getName());
        }

        //Añade el listener a la Vista.
        vista.frame.addWindowListener(new WindowListener() {

            /**
             * Este método se activa al abrirse la ventana principal del programa, es decir, al cargar la aplicación.
             * Lee el archivo de propiedades para determinar si debe o no realizar la carga del último archivo.
             * @param e Evento de ventana que detecta cualquier interacción con la misma.
             */
            @Override
            public void windowOpened(WindowEvent e) {
                try {
                    FileReader fr = new FileReader("properties.conf");
                    conf.load(fr);
                    if(conf.getProperty("cargar_auto").equalsIgnoreCase("true")){
                        modelo.cargarUltimoArchivo(vista.txtCodigo);
                    }else{
                        vista.txtCodigo.setText(Util.textoPorDefecto());
                    }
                } catch (FileNotFoundException ex) {
                    //En caso de que no se encuentre el archivo de configuración, el modelo se encargará de crearlo
                    //de forma automática.
                } catch (IOException ex) {
                    Util.mostrarMensajeError("Error de lectura de archivo. Se cargará un archivo nuevo.");
                    vista.txtCodigo.setText(Util.textoPorDefecto());
                }
            }

            /**
             * Lee el archivo de configuración y determina si debe guardar automáticamente el archivo actual, y en que
             * ruta debería de guardarlo.
             * @param e Evento de ventana que detecta cualquier interacción con la misma.
             */
            @Override
            public void windowClosing(WindowEvent e) {
                try{
                    FileReader fr = new FileReader("properties.conf");
                    conf.load(fr);
                    if(conf.getProperty("guardar_auto").equalsIgnoreCase("true") &&
                            !conf.getProperty("ruta_defecto").equalsIgnoreCase("")){
                        modelo.guardarDefault(vista.txtCodigo);
                    }
                } catch (FileNotFoundException ex) {
                    //En caso de que no se encuentre el archivo de configuración, el modelo se encargará de crearlo
                    //de forma automática.
                } catch (IOException ex) {
                    Util.mostrarMensajeError("Error al hacer el guardado automático.");
                }
            }

            //Métodos no necesarios.
            @Override
            public void windowClosed(WindowEvent e) {}
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
    }

    /**
     * Recibe un objeto de tipo ActionEvent cada vez que se pulsa un botón de la vista, después
     * ejecuta la acción correspondiente al botón pulsado.
     * @param e Detecta si se ha interacturado con algún elemento de la interfaz, como los botones o los menus.
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        //Se compara el action command recibido al pulsar un botón hasta encontrar una coincidencia,
        //una vez encontrada, se llama al método correspondiente en la clase modelo.
        String comando = e.getActionCommand();
        switch (comando){
            case "Texto" :
                modelo.nuevaVariableTexto(vista.txtCodigo);
                break;
            case "Número" :
                modelo.nuevaVariableNumero(vista.txtCodigo);
                break;
            case "Booleano" :
                modelo.nuevaVariableBoolean(vista.txtCodigo);
                break;
            case "Mensaje" :
                modelo.nuevaFuncionMensaje(vista.txtCodigo);
                break;
            case "Operación":
                modelo.nuevaFuncionOperacion(vista.txtCodigo);
                break;
            case "Condición" :
                modelo.nuevaFuncionCondicional(vista.txtCodigo);
                break;
            case "Bucle":
                modelo.nuevaFuncionBucle(vista.txtCodigo);
                break;
            case "Deshacer":
                modelo.deshacer(vista.txtCodigo);
                break;
            case "Rehacer":
                modelo.rehacer(vista.txtCodigo);
                break;
            case "Opciones" :
                modelo.menuOpciones(vista.txtCodigo);
                break;
            case "Guardar" :
                modelo.guardarArchivo(vista.txtCodigo);
                break;
            case "Cargar" :
                modelo.cargarArchivo(vista.txtCodigo);
                break;
            case "Variables" :
                modelo.verVariables(vista.txtCodigo);
                break;
            case "Funciones" :
                modelo.verFunciones(vista.txtCodigo);
                break;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {}
    @Override
    public void windowClosing(WindowEvent e) {}
    @Override
    public void windowClosed(WindowEvent e) {}
    @Override
    public void windowIconified(WindowEvent e) {}
    @Override
    public void windowDeiconified(WindowEvent e) {}
    @Override
    public void windowActivated(WindowEvent e) {}
    @Override
    public void windowDeactivated(WindowEvent e) {}
}

package com.apalacios.javaception.gui;

import com.apalacios.javaception.base.Funcion;
import com.apalacios.javaception.base.Variable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SeleccionarVariableDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList listVariables;
    private Funcion funcion;

    SeleccionarVariableDialog(Funcion funcion, DefaultListModel<Variable> dlmVariables) {
        this.funcion = funcion;
        listVariables.setModel(dlmVariables);
        setContentPane(contentPane);
        setSize(new Dimension(400, 400));
        setTitle("Seleccionar variable");
        setResizable(false);
        setLocationRelativeTo(null);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        if (!listVariables.isSelectionEmpty()){
            funcion.setVariable((Variable) listVariables.getSelectedValue());
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo(){
        setVisible(true);
    }
}

package Ejercicio3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Vista implements ActionListener {
    JFrame frame;
    JPanel panelPrincipal;
    JTextField txtUsuario;
    JTextField txtBaseDeDatos;
    JTextField txtServidor;
    JTextField txtRegistros;
    JButton btnCargarConf;
    JButton btnGuardarConf;
    Properties conf = new Properties();

    public Vista(){
        frame = new JFrame();
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("Ejercicio 3 - Adrián Palacios");
        frame.pack();
        frame.setVisible(true);

        btnCargarConf.addActionListener(this);
        btnCargarConf.setActionCommand("btnCargarConf");
        btnGuardarConf.addActionListener(this);
        btnGuardarConf.setActionCommand("btnGuardarConf");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "btnCargarConf": {
                try {
                    FileReader fr = new FileReader("Ejercicio3.conf");
                    conf.load(fr);
                    txtUsuario.setText(conf.getProperty("usuario"));
                    txtBaseDeDatos.setText(conf.getProperty("bbdd"));
                    txtServidor.setText(conf.getProperty("servidor"));
                    txtRegistros.setText(conf.getProperty("registros"));
                    fr.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
                break;
            case "btnGuardarConf": {
                try{
                    FileWriter fw = new FileWriter("Ejercicio3.conf");
                    conf.setProperty("usuario", txtUsuario.getText());
                    conf.setProperty("bbdd", txtBaseDeDatos.getText());
                    conf.setProperty("servidor", txtServidor.getText());
                    conf.setProperty("registros", txtRegistros.getText());
                    conf.store(fw, ");
                    fw.close();
                }catch (IOException ex){
                    ex.printStackTrace();
                }
            }
                break;
        }
    }
}

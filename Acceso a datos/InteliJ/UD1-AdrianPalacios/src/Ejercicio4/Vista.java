package Ejercicio4;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Vista implements ActionListener {
    JFrame frame;
    JPanel panelPrincipal;
    JComboBox<Carrera> cBoxCarreras;
    DefaultComboBoxModel<Carrera> dcbmCarreras;
    JTextField txtNombreCarrera;
    JTextField txtDistanciaCarrera;
    JTextField txtDesnivelCarrera;
    JButton btnAddCarrera;
    JButton btnGuardarFichero;
    JButton btnCargarFichero;
    DatePicker datePickerFechaCarrera;

    public Vista(){
        frame = new JFrame();
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("Ejercicio 4 - Adrián Palacios");
        frame.pack();
        frame.setVisible(true);

        dcbmCarreras = new DefaultComboBoxModel<Carrera>();
        cBoxCarreras.setModel(dcbmCarreras);

        btnAddCarrera.addActionListener(this);
        btnAddCarrera.setActionCommand("btnAddCarrera");
        btnCargarFichero.addActionListener(this);
        btnCargarFichero.setActionCommand("btnCargarFichero");
        btnGuardarFichero.addActionListener(this);
        btnGuardarFichero.setActionCommand("btnGuardarFichero");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "btnAddCarrera":{
                Carrera carrera = new Carrera(txtNombreCarrera.getText(), Float.valueOf(txtDistanciaCarrera.getText()),
                        Float.valueOf(txtDesnivelCarrera.getText()), datePickerFechaCarrera.getDate());
                dcbmCarreras.addElement(carrera);
            }
                break;
            case "btnCargarFichero":{
                System.out.println("Hey");
                try {
                    ObjectInputStream ois = new ObjectInputStream(new FileInputStream("Ejercicio4.bin"));
                    DefaultComboBoxModel<Carrera> model = (DefaultComboBoxModel<Carrera>)ois.readObject();
                    ois.close();
                    cBoxCarreras.setModel(model);
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex){
                    ex.printStackTrace();
                    System.out.println("ARCHIVO NO ENCONTRADO");
                }
            }
            break;
            case "btnGuardarFichero":{
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("Ejercicio4.bin"));
                    oos.writeObject(dcbmCarreras);
                    oos.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            break;
        }
    }
}

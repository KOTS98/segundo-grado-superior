package Ejercicio4;

import java.io.Serializable;
import java.time.LocalDate;

public class Carrera implements Serializable {
    private String nombre;
    private Float distancia;
    private Float desnivel;
    private LocalDate fecha;

    public Carrera(String nombre, Float distancia, Float desnivel, LocalDate fecha) {
        this.nombre = nombre;
        this.distancia = distancia;
        this.desnivel = desnivel;
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return nombre + " - " + fecha;
    }
}

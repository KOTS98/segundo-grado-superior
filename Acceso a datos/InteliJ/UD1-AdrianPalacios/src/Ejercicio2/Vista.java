package Ejercicio2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Vista implements ActionListener {
    JFrame frame;
    JPanel panelPrincipal;
    JComboBox cBoxPalabras;
    DefaultComboBoxModel<String> dcbmPalabras;
    JButton btnCargarPalabrasFichero;

    public Vista(){
        frame = new JFrame();
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setTitle("Ejercicio 2 - Adrián Palacios");
        frame.pack();
        frame.setVisible(true);

        dcbmPalabras = new DefaultComboBoxModel<>();
        cBoxPalabras.setModel(dcbmPalabras);

        btnCargarPalabrasFichero.addActionListener(this);
        btnCargarPalabrasFichero.setActionCommand("btnCargar");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getActionCommand().equalsIgnoreCase("btnCargar")){
            try {
                BufferedReader br = new BufferedReader(new FileReader("Ejercicio2.txt"));
                int contador = 0;
                while (br.readLine() != null){
                    contador++;
                }
                br.close();
                br = new BufferedReader(new FileReader("Ejercicio2.txt"));
                for (int i = 0; i < contador; i++){
                    dcbmPalabras.addElement(br.readLine());
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}

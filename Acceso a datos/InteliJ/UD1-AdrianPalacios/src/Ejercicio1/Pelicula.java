package Ejercicio1;

import java.time.LocalDate;

public class Pelicula {
    private String titulo;
    private String estilo;
    private Float duracion;
    private LocalDate fechaEstreno;

    public Pelicula(String titulo, String estilo, Float duracion, LocalDate fechaEstreno) {
        this.titulo = titulo;
        this.estilo = estilo;
        this.duracion = duracion;
        this.fechaEstreno = fechaEstreno;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getEstilo() {
        return estilo;
    }

    public Float getDuracion() {
        return duracion;
    }

    public LocalDate getFechaEstreno() {
        return fechaEstreno;
    }

    @Override
    public String toString() {
        return titulo + " - " + estilo + " - " + duracion + " - " + fechaEstreno;
    }
}

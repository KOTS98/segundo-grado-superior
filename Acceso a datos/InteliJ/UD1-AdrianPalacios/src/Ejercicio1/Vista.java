package Ejercicio1;

import com.github.lgooddatepicker.components.DatePicker;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class Vista implements ActionListener{
    JFrame frame;
    JList listPeliculas;
    DefaultListModel<Pelicula> dlmPeliculas;
    JButton btnAddPelicula;
    JButton btnExportarXML;
    JTextField txtTituloPelicula;
    JTextField txtEstiloPelicula;
    JTextField txtDuraciónPelicula;
    DatePicker datePickerFechaEstrenoPelicula;
    JPanel panelPrincipal;

    public Vista(){
        frame = new JFrame();
        frame.setContentPane(panelPrincipal);
        frame.setTitle("Ejercicio 1 - Adrián Palacios");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        dlmPeliculas = new DefaultListModel<>();
        listPeliculas.setModel(dlmPeliculas);
        frame.pack();
        frame.setVisible(true);
        addActionListeners(this);
    }

    private void addActionListeners(ActionListener listener){
        btnAddPelicula.addActionListener(listener);
        btnAddPelicula.setActionCommand("btnAddPelicula");
        btnExportarXML.addActionListener(listener);
        btnExportarXML.setActionCommand("btnExportarXML");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "btnAddPelicula":
                Pelicula pelicula = new Pelicula(txtTituloPelicula.getText(), txtEstiloPelicula.getText(),
                        Float.valueOf(txtDuraciónPelicula.getText()), datePickerFechaEstrenoPelicula.getDate());
                dlmPeliculas.addElement(pelicula);
                break;
            case "btnExportarXML":
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = null;
                try {
                    builder = factory.newDocumentBuilder();
                DOMImplementation dom = builder.getDOMImplementation();

                Document documento = dom.createDocument(null, "xml", null);
                Element raiz = documento.createElement("Peliculas");
                documento.getDocumentElement().appendChild(raiz);

                Element nodoPelicula = null;
                Element nodoDatos = null;
                Text texto = null;

                for (int i = 0; i < dlmPeliculas.size(); i++){
                    nodoPelicula = documento.createElement("Pelicula");
                    raiz.appendChild(nodoPelicula);

                    nodoDatos = documento.createElement("Titulo");
                    nodoPelicula.appendChild(nodoDatos);
                    texto = documento.createTextNode(dlmPeliculas.get(i).getTitulo());
                    nodoDatos.appendChild(texto);

                    nodoDatos = documento.createElement("Estilo");
                    nodoPelicula.appendChild(nodoDatos);
                    texto = documento.createTextNode(dlmPeliculas.get(i).getEstilo());
                    nodoDatos.appendChild(texto);

                    nodoDatos = documento.createElement("Duracion");
                    nodoPelicula.appendChild(nodoDatos);
                    texto = documento.createTextNode(String.valueOf(dlmPeliculas.get(i).getDuracion()));
                    nodoDatos.appendChild(texto);

                    nodoDatos = documento.createElement("FechaEstreno");
                    nodoPelicula.appendChild(nodoDatos);
                    texto = documento.createTextNode(String.valueOf(dlmPeliculas.get(i).getFechaEstreno()));
                    nodoDatos.appendChild(texto);
                }

                Source src = new DOMSource(documento);
                Result resultado = new StreamResult(new File("fichero.xml"));

                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.transform(src, resultado);
                } catch (ParserConfigurationException | TransformerException ex) {
                    ex.printStackTrace();
                }
                break;
        }
    }
}

DROP DATABASE matriculacion;
CREATE DATABASE IF NOT EXISTS matriculacion;
USE matriculacion;

CREATE TABLE alumnos(
idAlumno int auto_increment primary key,
nombreAlumno varchar(50) not null,
apellidosAlumno varchar(50) not null,
fechaNacimiento date,
direccion varchar(100),
telefono varchar(9)
);

CREATE TABLE asignaturas(
idAsignatura int auto_increment primary key,
asignatura varchar(50) not null,
curso int,
especialidad varchar(50)
);

CREATE TABLE matriculas(
idMatricula int auto_increment primary key,
fechaMatricula date,
idAlumno int not null,
idAsignatura int not null
);

--

ALTER TABLE matriculas
add foreign key (idAlumno) references alumnos(idAlumno),
add foreign key (idAsignatura) references asignaturas(idAsignatura);

--

delimiter ||
CREATE FUNCTION existeNombreAlumno (f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while (i < (select max(idAlumno) from alumnos)) do
		if ((select concat(apellidosAlumno, ', ', nombreAlumno) from alumnos
			where idAlumno = (i + 1)) like f_name)
			then return 1;
		end if;
        set i = i + 1;
	end while;
    return 0;
end; ||
delimiter ;

--

delimiter ||
CREATE FUNCTION existeAsignatura (f_name varchar(50))
returns bit
begin
	declare i int;
    set i = 0;
    while (i < (select max(idAsignatura) from asignaturas)) do
		if ((select asignatura from asignatura
			where idAsignatura = (i + 1)) like f_name)
			then return 1;
		end if;
        set i = i + 1;
	end while;
    return 0;
end; ||
delimiter ;
package gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.sun.org.apache.xml.internal.dtm.DTM;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private JPanel panelPrincipal;

    // Matrícula
    JComboBox cBoxAlumno;
    JComboBox cBoxAsignatura;
    JButton btnAddMatricula;
    JButton btnModMatricula;
    JButton btnDelMatricula;
    JTable tableMatriculas;
    DatePicker dateFechaMatricula;

    // Asignatura
    JTextField txtAsignatura;
    JTextField txtCurso;
    JTextField txtEspecialidad;
    JButton btnAddAsignatura;
    JButton btnModAsignatura;
    JButton btnDelAsignatura;
    JTable tableAsignaturas;

    // Alumno
    JTextField txtNombre;
    JTextField txtApellidos;
    JTextField txtDireccion;
    JTextField txtTelefono;
    JButton btnAddAlumno;
    JButton btnModAlumno;
    JButton btnDelAlumno;
    JTable tableAlumnos;
    DatePicker dateFechaNacimiento;

    // Default Table Models
    DefaultTableModel dtmMatriculas;
    DefaultTableModel dtmAsignaturas;
    DefaultTableModel dtmAlumnos;

    // Elementos del menú
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;

    public Vista() {
        this.setTitle("Matriculación - Adrián Palacios");
        this.setContentPane(panelPrincipal);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(500, 400));
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.pack();
        this.setVisible(true);

        setTableModels();
        setMenu();
    }

    private void setTableModels() {
        this.dtmMatriculas = new DefaultTableModel();
        this.tableMatriculas.setModel(dtmMatriculas);

        this.dtmAsignaturas = new DefaultTableModel();
        this.tableAsignaturas.setModel(dtmAsignaturas);

        this.dtmAlumnos = new DefaultTableModel();
        this.tableAlumnos.setModel(dtmAlumnos);
    }

    private void setMenu() {
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("opciones");
        menu.add(itemOpciones);

        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("desconectar");
        menu.add(itemDesconectar);

        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");
        menu.add(itemSalir);

        mbBar.add(menu);
        mbBar.add((Box.createHorizontalGlue()));
        this.setJMenuBar(mbBar);
    }
}

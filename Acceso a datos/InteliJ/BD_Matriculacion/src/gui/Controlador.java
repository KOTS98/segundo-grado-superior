package gui;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ListSelectionListener, WindowListener {
    private Vista vista;
    private Modelo modelo;
    private boolean refrescar;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        setOptions();
        // modelo.conectar();
        addActionListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void setOptions() {

    }

    private void addActionListeners(ActionListener listener) {
        vista.btnAddMatricula.addActionListener(listener);
        vista.btnModMatricula.addActionListener(listener);
        vista.btnDelMatricula.addActionListener(listener);
        vista.btnAddAsignatura.addActionListener(listener);
        vista.btnModAsignatura.addActionListener(listener);
        vista.btnDelAsignatura.addActionListener(listener);
        vista.btnAddAlumno.addActionListener(listener);
        vista.btnModAlumno.addActionListener(listener);
        vista.btnDelAlumno.addActionListener(listener);

        vista.itemOpciones.addActionListener(listener);
        vista.itemDesconectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    private void refrescarTodo() {
        refrescarMatriculas();
        refrescarAsignaturas();
        refrescarAlumnos();
        refrescar = false;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "addMatricula":

                break;
            case "modMatricula":

                break;
            case "delMatricula":

                break;
            case "addAsignatura":

                break;
            case "modAsignatura":

                break;
            case "delAsignatura":

                break;
            case "addAlumno":

                break;
            case "modAlumno":

                break;
            case "delAlumno":

                break;
            case "opciones":

                break;
            case "desconectar":

                break;
            case "salir":

                break;
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    private boolean comprobarMatriculaVacia() {
        return vista.dateFechaMatricula.getText().isEmpty() ||
                vista.cBoxAsignatura.getSelectedIndex() == -1 ||
                vista.cBoxAlumno.getSelectedIndex() == -1;
    }

    private boolean comprobarAsignaturaVacia() {
        return vista.txtAsignatura.getText().isEmpty() ||
                vista.txtCurso.getText().isEmpty() ||
                vista.txtEspecialidad.getText().isEmpty();
    }

    private boolean comprobarAlumnoVacio() {
        return vista.txtNombre.getText().isEmpty() ||
                vista.txtApellidos.getText().isEmpty() ||
                vista.dateFechaNacimiento.getText().isEmpty() ||
                vista.txtDireccion.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty();
    }

    private void refrescarMatriculas() {

    }

    private void refrescarAsignaturas() {

    }

    private void refrescarAlumnos() {

    }

    private DefaultTableModel construirTableModel(ResultSet rs, DefaultTableModel dtm) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++)
            columnNames.add(metaData.getColumnName(column));

        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        dtm.setDataVector(data, columnNames);
        return dtm;
    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++)
                vector.add(rs.getObject(columnIndex));
            data.add(vector);
        }
    }

    private void borrarCamposMatriculas() {
        vista.dateFechaMatricula.clear();
        vista.cBoxAsignatura.setSelectedIndex(-1);
        vista.cBoxAlumno.setSelectedIndex(-1);
    }

    private void borrarCamposAsignaturas() {
        vista.txtAsignatura.setText("");
        vista.txtCurso.setText("");
        vista.txtEspecialidad.setText("");
    }

    private void borrarCamposAlumnos() {
        vista.txtNombre.setText("");
        vista.txtApellidos.setText("");
        vista.dateFechaNacimiento.clear();
        vista.txtDireccion.setText("");
        vista.txtTelefono.setText("");
    }

    // Métodos innecesarios
    @Override
    public void windowOpened(WindowEvent e) {}
    @Override
    public void windowClosed(WindowEvent e) {}
    @Override
    public void windowIconified(WindowEvent e) {}
    @Override
    public void windowDeiconified(WindowEvent e) {}
    @Override
    public void windowActivated(WindowEvent e) {}
    @Override
    public void windowDeactivated(WindowEvent e) {}
}

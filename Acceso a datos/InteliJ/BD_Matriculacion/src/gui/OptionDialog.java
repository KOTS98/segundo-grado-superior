package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class OptionDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtConfDireccion;
    private JTextField txtConfPuerto;
    private JTextField txtConfBBDD;
    private JTextField txtConfUsuario;
    private JTextField txtConfPass;
    private JTextField txtConfAdminPass;

    public OptionDialog() {
        setContentPane(contentPane);
        setTitle("Opciones de conexión");
        setSize(new Dimension(290, 280));
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {

        dispose();
    }

    private void onCancel() {
        dispose();
    }
}

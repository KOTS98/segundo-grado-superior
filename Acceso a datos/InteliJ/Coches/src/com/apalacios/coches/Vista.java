package com.apalacios.coches;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.ClientInfoStatus;
import java.util.LinkedList;

public class Vista {
    private JTextField txtMatricula;
    private JTextField txtModelo;
    private JTextField txtDatosCoche;
    private JComboBox cbListaCoches;
    private JButton btnAltaCoche;
    private JButton btnMostrarCoche;
    private JPanel panelPrincipal;
    private LinkedList<Coche> lista;
    private DefaultComboBoxModel<Coche> dcbmListaCoches;
    private JFrame frame;

    public Vista(){
        frame = new JFrame("Gestor de coches");
        frame.setSize(500, 500);
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        lista = new LinkedList<>();
        dcbmListaCoches = new DefaultComboBoxModel<>();
        cbListaCoches.setModel(dcbmListaCoches);
        crearMenu();
        frame.pack();
        frame.setVisible(true);
        btnAltaCoche.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                altaCoche();
                listar();
            }
        });
        btnMostrarCoche.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                verDatosCoche();
            }
        });
        cbListaCoches.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE){
                    dcbmListaCoches.removeElementAt(cbListaCoches.getSelectedIndex());
                }
            }
        });
    }

    public void altaCoche(){
        lista.add(new Coche(txtMatricula.getText(), txtModelo.getText()));
    }

    public void listar(){
        dcbmListaCoches.removeAllElements();
        for (Coche coche : lista) {
            dcbmListaCoches.addElement(coche);
        }
    }

    public void verDatosCoche(){
        txtDatosCoche.setText(cbListaCoches.getSelectedItem().toString());
    }

    public void crearMenu(){
        JMenuBar barraMenu = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar");
        JMenuItem itemimportarXML = new JMenuItem("Importar");
        menu.add(itemExportarXML);
        menu.add(itemimportarXML);
        barraMenu.add(menu);
        frame.setJMenuBar(barraMenu);

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    try {
                        exportarXML(fichero);
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        itemimportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showOpenDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    try {
                        importarXML(fichero);
                    } catch (ParserConfigurationException | IOException | SAXException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    private void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento =  builder.parse(new File(fichero.getAbsolutePath()));
        NodeList productos = documento.getElementsByTagName("producto");
        for (int i = 0; i < productos.getLength(); i++) {
            Node producto = productos.item( i );
            Element elemento = ( Element ) producto;
        }
    }

    private void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();

        Document documento = dom.createDocument(null, "xml", null);
        Element raiz = documento.createElement("coches");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoCoche;
        Element nodoDatos;
        Text dato;

        for (Coche coche : lista) {
            nodoCoche = documento.createElement("coche");
            raiz.appendChild(nodoCoche);

            nodoDatos = documento.createElement("matricula");
            nodoCoche.appendChild(nodoDatos);
            dato = documento.createTextNode(coche.getMatricula());
            nodoDatos.appendChild(dato);

            nodoDatos = documento.createElement("modelo");
            nodoCoche.appendChild(nodoDatos);
            dato = documento.createTextNode(coche.getModelo());
            nodoDatos.appendChild(dato);
        }
        Source src = new DOMSource(documento);
        Result result = new StreamResult(fichero);
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(src, result);
    }
}

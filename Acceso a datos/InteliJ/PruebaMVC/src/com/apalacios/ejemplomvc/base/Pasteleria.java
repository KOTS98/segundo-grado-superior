package com.apalacios.ejemplomvc.base;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;

public class Pasteleria implements Serializable {
    private String nombre;
    private String direccion;
    private LocalDate fechaApertura;
    private HashSet<Bizcocho> listaBizcochos;

    public Pasteleria(String nombre, String direccion, LocalDate fechaApertura){
        this.nombre = nombre;
        this.direccion = direccion;
        this.fechaApertura = fechaApertura;
        this.listaBizcochos = new HashSet<>();
    }

    public HashSet<Bizcocho> getListaBizcochos() {
        return listaBizcochos;
    }

    @Override
    public String toString(){
        return this.nombre + " " + this.direccion + " " + this.fechaApertura;
    }
}

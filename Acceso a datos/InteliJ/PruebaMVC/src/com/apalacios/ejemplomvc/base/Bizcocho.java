package com.apalacios.ejemplomvc.base;

import java.io.Serializable;

public class Bizcocho implements Serializable {
    private String nombre;
    private String ingredientes;
    private double valorEnergetico;

    public Bizcocho(String nombre, String ingredientes, double valorEnergetico) {
        this.nombre = nombre;
        this.ingredientes = ingredientes;
        this.valorEnergetico = valorEnergetico;
    }

    public String getNombre() {
        return nombre;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public double getValorEnergetico() {
        return valorEnergetico;
    }

    @Override
    public String toString(){
        return this.nombre + " " + this.ingredientes + " " + this.valorEnergetico;
    }
}

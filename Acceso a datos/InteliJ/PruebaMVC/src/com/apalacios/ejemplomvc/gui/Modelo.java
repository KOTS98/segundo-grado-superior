package com.apalacios.ejemplomvc.gui;

import com.apalacios.ejemplomvc.base.Bizcocho;
import com.apalacios.ejemplomvc.base.Pasteleria;

import java.io.*;
import java.time.LocalDate;
import java.util.HashSet;

public class Modelo {
    private HashSet<Pasteleria> pastelerias;
    private HashSet<Bizcocho> bizcochos;

    public Modelo(){
        this.bizcochos = new HashSet<>();
        this.pastelerias = new HashSet<>();
    }

    public HashSet<Pasteleria> getPastelerias() {
        return pastelerias;
    }

    public HashSet<Bizcocho> getBizcochos() {
        return bizcochos;
    }

    public void nuevaPasteleria(String nombre, String direccion, LocalDate fechaApertura){
        Pasteleria past = new Pasteleria(nombre, direccion, fechaApertura);
        pastelerias.add(past);
    }

    public void nuevoBizcocho(String nombre, String ingredientes, double valorEnergetico){
        Bizcocho bizc = new Bizcocho(nombre, ingredientes, valorEnergetico);
        bizcochos.add(bizc);
    }

    public void eliminarPasteleria(Pasteleria past){
        pastelerias.remove(past);
    }

    public void eliminarBizcocho(Bizcocho bizc){
        bizcochos.remove(bizc);
    }

    public void guardarDatos() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("datos.bin"));
        oos.writeObject(pastelerias);
        oos.writeObject(bizcochos);
        oos.close();
    }

    public void cargarDatos() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("datos.bin"));
        pastelerias = (HashSet<Pasteleria>) ois.readObject();
        bizcochos = (HashSet<Bizcocho>) ois.readObject();
        ois.close();
    }
}
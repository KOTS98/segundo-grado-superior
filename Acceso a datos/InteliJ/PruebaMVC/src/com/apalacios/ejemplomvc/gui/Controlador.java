package com.apalacios.ejemplomvc.gui;

import com.apalacios.ejemplomvc.base.Bizcocho;
import com.apalacios.ejemplomvc.base.Pasteleria;
import com.apalacios.ejemplomvc.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.io.IOException;
import java.time.LocalDate;
import java.util.InputMismatchException;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener, WindowListener {

    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        addActionListener(this);
        addKeyListener(this);
        addSelectionListener(this);
        addWindowListener(this);
    }

    private void addActionListener(ActionListener listener){
        vista.btnEliminar.addActionListener(listener);
        vista.btnEliminarPasteleria.addActionListener(listener);
        vista.btnInsertar.addActionListener(listener);
        vista.btnInsertarPasteleria.addActionListener(listener);
        vista.btnSeleccionarBizcochos.addActionListener(listener);
        vista.btnSeleccionarBizcochos.setActionCommand("seleccionarBizcochos");
    }

    private void addKeyListener(KeyListener listener){
        vista.listListaPasteleria.addKeyListener(listener);
        vista.listLista.addKeyListener(listener);
        vista.txtBuscar.addKeyListener(listener);
    }

    private void addSelectionListener(ListSelectionListener listener){
        vista.listListaPasteleria.addListSelectionListener(listener);
    }

    private void addWindowListener(WindowListener listener){
        vista.frame.addWindowListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch(comando){
            case "newPasteleria":
                String nombrePast = vista.txtNombrePasteleria.getText();
                String direccion = vista.txtDireccion.getText();
                LocalDate fechaApertura = vista.dtpFecha.getDate();
                modelo.nuevaPasteleria(nombrePast, direccion, fechaApertura);
                vista.txtNombrePasteleria.setText("");
                vista.txtDireccion.setText("");
                vista.dtpFecha.setDate(null);
                listarPastelerias();
                break;
            case "delPasteleria":
                eliminarPasteleria();
                break;
            case "newBizcocho":
                String nombreBizc = vista.txtNombre.getText();
                String ingredientes = vista.txtIngredientes.getText();
                double valorEnergetico = Double.parseDouble(vista.txtValorEnergetico.getText());
                modelo.nuevoBizcocho(nombreBizc, ingredientes, valorEnergetico);
                vista.txtNombre.setText("");
                vista.txtIngredientes.setText("");
                vista.txtValorEnergetico.setText(null);
                listarBizcocho();
                break;
            case "delBizcocho":
                Bizcocho bizcocho = vista.listLista.getSelectedValue();
                modelo.eliminarBizcocho(bizcocho);
                listarBizcocho();
                break;
            case "seleccionarBizcochos":{
                if(vista.listListaPasteleria.isSelectionEmpty()){
                    Util.mostrarMensajeError("Debes seleccionar una pasteleria");
                }else{
                    Pasteleria pasteleria = vista.listListaPasteleria.getSelectedValue();
                    AdministrarBizcochosDialog dialog = new AdministrarBizcochosDialog(pasteleria, modelo.getBizcochos());
                    dialog.mostrarDialogo();
                    listarBizcochosDePasteleria(pasteleria);
                }
                break;
            }
        }
    }

    private void listarBizcochosDePasteleria(Pasteleria seleccionada){
        vista.dlmBizcochoDePasteleria.clear();
        for (Bizcocho bizcocho : seleccionada.getListaBizcochos()) {
            vista.dlmBizcochoDePasteleria.addElement(bizcocho);
        }
    }

    private void listarPastelerias(){
        vista.dlmPasteleria.clear();
        for (Pasteleria pasteleria : modelo.getPastelerias()) {
            vista.dlmPasteleria.addElement(pasteleria);
        }
    }

    private void listarBizcocho(){
        vista.dlmBizcocho.clear();
        for (Bizcocho bizcocho : modelo.getBizcochos()) {
            vista.dlmBizcocho.addElement(bizcocho);
        }
    }

    private void listarBizcocho(String valor, String campo){
        vista.dlmBizcocho.clear();
        for (Bizcocho bizcocho : modelo.getBizcochos()) {
            if(campo.equals("Nombre")){
                if(bizcocho.getNombre().contains(valor)){
                    vista.dlmBizcocho.addElement(bizcocho);
                }
            }else if (campo.equals("Ingredientes")){
                if(bizcocho.getIngredientes().contains(valor)){
                    vista.dlmBizcocho.addElement(bizcocho);
                }
            }else{
                try{
                    if(bizcocho.getValorEnergetico() == Double.parseDouble(valor)){
                        vista.dlmBizcocho.addElement(bizcocho);
                    }
                }catch (InputMismatchException e){
                    e.printStackTrace();
                }
            }
            vista.dlmBizcocho.addElement(bizcocho);
        }
    }

    public void eliminarPasteleria(){
        Pasteleria pasteleria = vista.listListaPasteleria.getSelectedValue();
        modelo.eliminarPasteleria(pasteleria);
        listarPastelerias();
    }

    public void eliminarBizcocho(){
        Bizcocho bizcocho = vista.listLista.getSelectedValue();
        modelo.eliminarBizcocho(bizcocho);
        listarBizcocho();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscar){
            if(vista.comboBox1.getSelectedItem().equals("Nombre") || vista.comboBox1.getSelectedItem()
                    .equals("Ingredientes")){
                if(vista.txtBuscar.getText().length() > 3){
                    String valorBusqueda = vista.txtBuscar.getText();
                    String campo = (String) vista.comboBox1.getSelectedItem();
                    listarBizcocho(valorBusqueda, campo);
                }
            }else{

            }
        }else{
            if (e.getKeyCode() == KeyEvent.VK_DELETE){
                if(e.getSource() == vista.listLista){
                    eliminarBizcocho();
                }else{
                    eliminarPasteleria();
                }
            }
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(!vista.listListaPasteleria.isSelectionEmpty()){
            Pasteleria pasteleria = vista.listListaPasteleria.getSelectedValue();
            listarBizcochosDePasteleria(pasteleria);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        //Cuando se cierra
        try {
            modelo.guardarDatos();
        } catch (IOException ex) {
            ex.printStackTrace();
            Util.mostrarMensajeError("Error al guardar datos");
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}

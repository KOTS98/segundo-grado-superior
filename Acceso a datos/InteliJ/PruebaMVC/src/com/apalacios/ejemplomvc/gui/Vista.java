package com.apalacios.ejemplomvc.gui;

import com.apalacios.ejemplomvc.base.Bizcocho;
import com.apalacios.ejemplomvc.base.Pasteleria;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Vista {
    private JPanel panelPrincipal;
    private JTabbedPane panelTabs;
    JPanel panelPasteleria;
    JPanel panelBizcocho;
    JTextField txtNombre;
    JTextField txtIngredientes;
    JTextField txtValorEnergetico;
    JButton btnInsertar;
    JButton btnEliminar;
    JTextField txtNombrePasteleria;
    JTextField txtDireccion;
    JList<Bizcocho> listLista;
    JLabel lblNombre;
    JLabel lblIngredientes;
    JLabel lblValorEnergtico;
    JScrollPane scrollPanel;
    JLabel lblNombrePasteleria;
    JLabel lblDireccion;
    JLabel lblFechaApertura;
    JButton btnInsertarPasteleria;
    JButton btnEliminarPasteleria;
    JList<Pasteleria> listListaPasteleria;
    JList<Bizcocho> bizcochosDePastelerias;
    JScrollPane scrollPanelPasteleria;
    DatePicker dtpFecha;
    JButton btnSeleccionarBizcochos;
    JList listListaBizcochosPasteleria;
    JTextField txtBuscar;
    JComboBox<String> comboBox1;
    DefaultListModel<Pasteleria> dlmPasteleria;
    DefaultListModel<Bizcocho> dlmBizcocho;
    DefaultListModel<Bizcocho> dlmBizcochoDePasteleria;
    JFrame frame;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        inicializarModelos();
    }

    private void inicializarModelos(){
        dlmPasteleria = new DefaultListModel<>();
        dlmBizcocho = new DefaultListModel<>();
        dlmBizcochoDePasteleria = new DefaultListModel<>();
        listListaPasteleria.setModel(dlmPasteleria);
        listLista.setModel(dlmBizcocho);
        listListaBizcochosPasteleria.setModel(dlmBizcochoDePasteleria);
    }
}

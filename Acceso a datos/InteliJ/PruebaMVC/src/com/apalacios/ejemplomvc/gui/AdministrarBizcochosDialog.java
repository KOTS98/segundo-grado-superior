package com.apalacios.ejemplomvc.gui;

import com.apalacios.ejemplomvc.base.Bizcocho;
import com.apalacios.ejemplomvc.base.Pasteleria;

import javax.swing.*;
import java.awt.event.*;
import java.util.HashSet;

public class AdministrarBizcochosDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JList<Bizcocho> listListaBizcochosDisponibles;
    private JButton btnAnyadirBizc;
    private JButton btnQuitarBizc;
    private JList<Bizcocho> listListaBizcochosActuales;
    private DefaultListModel<Bizcocho> dlmBizcochosDisponibles;
    private DefaultListModel<Bizcocho> dlmBizcochosActuales;
    private Pasteleria pasteleria;
    private HashSet<Bizcocho> bizcochosDisponibles;

    public AdministrarBizcochosDialog(Pasteleria pasteleria, HashSet<Bizcocho> bizcochos) {
        this.pasteleria = pasteleria;
        this.bizcochosDisponibles = bizcochos;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        pack();
        iniciarListas();
        listarBizcochosActuales();
        setTitle("Pasteleria " + pasteleria);
        btnAnyadirBizc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Bizcocho seleccionado = listListaBizcochosDisponibles.getSelectedValue();
                pasteleria.getListaBizcochos().add(seleccionado);
                listarBizcochosActuales();
            }
        });
        btnQuitarBizc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Bizcocho seleccionado = listListaBizcochosActuales.getSelectedValue();
                pasteleria.getListaBizcochos().remove(seleccionado);
                listarBizcochosActuales();
            }
        });
    }

    private void onOK() {
        dispose();
    }

    public void mostrarDialogo(){
        listarDatos();
        setVisible(true);
    }

    public void iniciarListas(){
        dlmBizcochosActuales = new DefaultListModel<>();
        dlmBizcochosDisponibles = new DefaultListModel<>();
        listListaBizcochosActuales.setModel(dlmBizcochosActuales);
        listListaBizcochosDisponibles.setModel(dlmBizcochosDisponibles);
    }

    public void listarDatos(){
        //Listo  los bizcochos disponibles
        for (Bizcocho bizcocho : bizcochosDisponibles){
            dlmBizcochosDisponibles.addElement(bizcocho);
        }
    }

    public void listarBizcochosActuales(){
        //Listo los bizcochos de la pasteleria actual
        dlmBizcochosActuales.clear();
        for(Bizcocho bizcocho : pasteleria.getListaBizcochos()){
            dlmBizcochosActuales.addElement(bizcocho);
        }
    }
}

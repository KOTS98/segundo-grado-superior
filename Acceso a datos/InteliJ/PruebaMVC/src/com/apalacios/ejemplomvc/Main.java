package com.apalacios.ejemplomvc;

import com.apalacios.ejemplomvc.gui.Controlador;
import com.apalacios.ejemplomvc.gui.Modelo;
import com.apalacios.ejemplomvc.gui.Vista;

public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}

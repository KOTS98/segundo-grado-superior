package com.apalacios.practica.v2.gui;

import com.apalacios.practica.v2.base.Animal;
import com.apalacios.practica.v2.base.Veterinario;
import com.apalacios.practica.v2.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;

public class AnyadirAnimalDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList<Animal> listAnimalesActuales;
    private DefaultListModel<Animal> dlmAnimalesActuales;
    private JList<Animal> listAnimalesDisponibles;
    private DefaultListModel<Animal> dlmAnimalesDiponibles;
    private JButton btnRetirarAnimal;
    private JButton btnAnyadirAnimal;

    private Veterinario veterinario;

    AnyadirAnimalDialog(Veterinario veterinario, HashSet<Animal> animales) {
        this.veterinario = veterinario;

        this.dlmAnimalesActuales = new DefaultListModel<>();
        this.listAnimalesActuales.setModel(dlmAnimalesActuales);
        this.dlmAnimalesDiponibles = new DefaultListModel<>();
        this.listAnimalesDisponibles.setModel(dlmAnimalesDiponibles);

        for (Animal animal : animales) {
            dlmAnimalesDiponibles.addElement(animal);
        }

        for (Animal animal : veterinario.getAnimalesACargo()) {
            dlmAnimalesActuales.addElement(animal);
        }

        setSize(new Dimension(400, 300));
        setResizable(false);
        setTitle("Añadir animales a veterinario");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        btnAnyadirAnimal.addActionListener(e -> anyadirAnimal());

        btnRetirarAnimal.addActionListener(e -> retirarAnimal());

        buttonOK.addActionListener(e -> onOK(animales));

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void anyadirAnimal(){
        if(!listAnimalesDisponibles.isSelectionEmpty()){
            dlmAnimalesActuales.addElement(dlmAnimalesDiponibles.get(listAnimalesDisponibles.getSelectedIndex()));
            dlmAnimalesDiponibles.remove(listAnimalesDisponibles.getSelectedIndex());
        }else{
            Util.mostrarMensajeError("Debes seleccionar un animal para añadir");
        }
    }

    private void retirarAnimal(){
        if(!listAnimalesActuales.isSelectionEmpty()){
            dlmAnimalesDiponibles.addElement(dlmAnimalesActuales.get(listAnimalesActuales.getSelectedIndex()));
            veterinario.getAnimalesACargo().remove(dlmAnimalesActuales.get(listAnimalesActuales.getSelectedIndex()));
            dlmAnimalesActuales.clear();
            for (Animal animal : veterinario.getAnimalesACargo()){
                dlmAnimalesActuales.addElement(animal);
            }
        }else{
            Util.mostrarMensajeError("Debes seleccionar un animal para retirar");
        }
    }

    private void onOK(HashSet<Animal> animales) {
        veterinario.getAnimalesACargo().clear();
        for (int i = 0; i < dlmAnimalesActuales.size(); i++){
            veterinario.getAnimalesACargo().add(dlmAnimalesActuales.get(i));
        }

        animales.clear();
        for (int i = 0; i <dlmAnimalesDiponibles.size(); i++){
            animales.add(dlmAnimalesDiponibles.get(i));
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo(){
        setVisible(true);
    }
}

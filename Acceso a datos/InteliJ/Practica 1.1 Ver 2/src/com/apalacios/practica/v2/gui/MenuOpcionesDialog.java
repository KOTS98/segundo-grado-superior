package com.apalacios.practica.v2.gui;

import com.apalacios.practica.v2.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class MenuOpcionesDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JCheckBox cBoxGuardarAuto;
    private JCheckBox cBoxCargarAuto;
    private JButton btnSeleccionarRuta;
    private String rutaDefecto;
    Properties conf = new Properties();

    public MenuOpcionesDialog() {
        rutaDefecto = "";
        try {
            FileReader fr = new FileReader("properties.conf");
            conf.load(fr);
            cBoxGuardarAuto.setSelected(Boolean.parseBoolean(conf.getProperty("guardar_auto")));
            cBoxCargarAuto.setSelected(Boolean.parseBoolean(conf.getProperty("cargar_auto")));
            fr.close();
        } catch (FileNotFoundException e) {
            Util.mostrarMensajeError("Error. Archivo de configuración \"properties.conf\" no encontrado");
        } catch (IOException e) {
            Util.mostrarMensajeError("Error al leer el archivo de configuración");
        }
        setSize(new Dimension(400, 200));
        setResizable(false);
        setTitle("Opciones");
        setContentPane(contentPane);
        setModal(true);
        setLocationRelativeTo(null);
        getRootPane().setDefaultButton(buttonOK);

        btnSeleccionarRuta.addActionListener(e -> {
            JFileChooser selector = new JFileChooser();
            int opcion = selector.showOpenDialog(null);
            if (opcion == JFileChooser.APPROVE_OPTION){
                this.rutaDefecto = selector.getSelectedFile().getPath();
            }
        });

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        try {
            FileWriter fw = new FileWriter("properties.conf");
            conf.setProperty("guardar_auto", String.valueOf(cBoxGuardarAuto.isSelected()));
            conf.setProperty("cargar_auto", String.valueOf(cBoxCargarAuto.isSelected()));
            conf.setProperty("ruta_defecto", rutaDefecto);
            conf.store(fw, "");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo(){
        setVisible(true);
    }
}

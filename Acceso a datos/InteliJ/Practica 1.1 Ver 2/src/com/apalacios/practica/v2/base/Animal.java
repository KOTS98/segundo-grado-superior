package com.apalacios.practica.v2.base;

import java.io.Serializable;
import java.util.HashSet;

/**
 * Clase que representa a un veterinario.
 * @author José Adrián Palacios Romo
 */
public class Animal implements Serializable {
    private String nombre;
    private String especie;
    private String sexo;
    private Float peso;
    private String duenyo;
    private HashSet<Medicamento> medicamentosRecetados;

    /**
     * Constructor de la clase, instancia todos los atributos de la misma.
     * @param nombre String que representa el nombre del animal
     * @param especie String que representa la especie del animal
     * @param sexo String que representa el sexo del animal
     * @param peso Float que representa el peso del animal
     * @param duenyo String que representa el dueño del animal
     */
    public Animal(String nombre, String especie, String sexo, Float peso, String duenyo) {
        this.nombre = nombre;
        this.especie = especie;
        this.sexo = sexo;
        this.peso = peso;
        this.duenyo = duenyo;
        this.medicamentosRecetados = new HashSet<>();
    }

    /**
     * Obtiene el HashSet que contiene los medicamento a los que esta sujeto el animal
     * @return HashSet<Medicamento>
     */
    public HashSet<Medicamento> getMedicamentosRecetados() {
        return medicamentosRecetados;
    }

    /**
     * Establece el nombre del animal
     * @param nombre String que representa el nombre del animal
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Establece la especie del animal
     * @param especie String que representa la especie del animal
     */
    public void setEspecie(String especie) {
        this.especie = especie;
    }

    /**
     * Establece el sexo del animal
     * @param sexo String que representa el sexo del animal
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * Establece el peso del animal
     * @param peso Float que representa el peso del animal
     */
    public void setPeso(Float peso) {
        this.peso = peso;
    }

    /**
     * Establece el dueño del animal
     * @param duenyo String que representa el dueño del animal
     */
    public void setDuenyo(String duenyo) {
        this.duenyo = duenyo;
    }

    /**
     * Formatea los datos del animal a un String fácilmente legible.
     * @return String que representa todos los datos del animal
     */
    @Override
    public String toString() {
        return nombre + " " + especie + " " + sexo + " " + peso + "kg " + duenyo;
    }
}

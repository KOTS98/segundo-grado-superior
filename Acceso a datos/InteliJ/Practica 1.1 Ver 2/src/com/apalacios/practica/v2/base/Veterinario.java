package com.apalacios.practica.v2.base;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;

/**
 * Clase que representa a un veterinario.
 * @author José Adrián Palacios Romo
 */
public class Veterinario implements Serializable {
    private String nombre;
    private String apellido;
    private String sexo;
    private LocalDate fechaNacimiento;
    private String dni;
    private HashSet<Animal> animalesACargo;

    /**
     * Constructor de la clase, instancia todos los atributos de la misma.
     * @param nombre String que representa el nombre del veterinario
     * @param apellido String que representa el apellido del veterinario
     * @param sexo String que representa el sexo del veterinario
     * @param fechaNacimiento LocalDate que representa la fecha de nacimiento del veterinario
     * @param dni String que representa el D.N.I. del veterinario
     */
    public Veterinario(String nombre, String apellido, String sexo, LocalDate fechaNacimiento, String dni){
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
        this.fechaNacimiento = fechaNacimiento;
        this.dni = dni;
        this.animalesACargo = new HashSet<>();
    }

    /**
     * Obtiene el HashSet que contiene los animales a los que esta acargo el veterinario
     * @return HashSet<Animal>
     */
    public HashSet<Animal> getAnimalesACargo() {
        return animalesACargo;
    }

    /**
     * Establece el nombre del veterinario
     * @param nombre String que representa el nombre del veterinario
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Establece el apellido del veterinario
     * @param apellido String que representa el apellido del veterinario
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * Establece el sexo del veterinario
     * @param sexo String que representa el sexo del veterinario
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * Establece la fecha de nacimiento del veterinario
     * @param fechaNacimiento LocalDate que representa la fecha de nacimiento del veterinario
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * Establece el D.N.I. del veterinario
     * @param dni String que representa el D.N.I. del veterinario
     */
    public void setDni(String dni) {
        this.dni = dni;
    }

    /**
     * Formatea los datos del veterinario a un String fácilmente legible.
     * @return String que representa todos los datos del veterinario
     */
    @Override
    public String toString() {
        return nombre + " " + apellido + " " + sexo + " " + fechaNacimiento + " " + dni;
    }
}

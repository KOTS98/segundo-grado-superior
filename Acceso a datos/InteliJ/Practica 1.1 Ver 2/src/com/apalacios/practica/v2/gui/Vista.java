package com.apalacios.practica.v2.gui;

import com.apalacios.practica.v2.base.Animal;
import com.apalacios.practica.v2.base.Medicamento;
import com.apalacios.practica.v2.base.Veterinario;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

/**
 * Esta clase representa la interfaz principal del programa.
 * @author José Adrián Palacios Romo
 */
public class Vista {
    JFrame frame;
    JTabbedPane tabbedPane1;
    JPanel panelPrincipal;
    JTextField txtNombreVeterinario;
    JTextField txtApellidoVeterinario;
    JRadioButton rBtnMasculinoVeterinario;
    JRadioButton rBtnFemeninoVeterinario;
    JTextField txtDNIVeterinario;
    JList listVeterinarios;
    JButton btnAltaVeterinario;
    JButton btnEliminarVeterinario;
    JButton btnModificarVeterinario;
    JTextField txtNombreAnimal;
    JRadioButton rBtnMachoAnimal;
    JRadioButton rBtnHembraAnimal;
    JTextField txtPesoAnimal;
    JTextField txtDuenyoAnimal;
    JList listAnimales;
    JButton btnAltaAnimal;
    JButton btnModificarAnimal;
    JButton btnEliminarAnimal;
    JTextField txtNombreMedicamento;
    JTextField txtFabricanteMedicamento;
    JTextField txtDosisMedicamento;
    JTextField txtPrecioMedicamento;
    JTextField txtCompuestoMedicamento;
    JButton btnAltaMedicamento;
    JButton btnEliminarMedicamento;
    JList listMedicamentos;
    JButton btnModificarMedicamento;
    JButton btnAnyadirAnimal;
    JButton btnAnyadirMedicamento;
    DatePicker datePickerFechaNacimientoVeterinario;
    JTextField txtEspecieAnimal;

    DefaultListModel<Veterinario> dlmVeterinarios;
    DefaultListModel<Animal> dlmAnimales;
    DefaultListModel<Medicamento> dlmMedicamentos;

    JMenuItem itemGuardar;
    JMenuItem itemCargar;
    JMenuItem itemOpciones;

    /**
     * Constructor de la clase, instancia el frame y ajusta sus parámetros, llama al método para crear el menú
     * superior y al método para inicializar los modelos.
     */
    public Vista(){
        frame = new JFrame("Clínica Veterinaria - Adrián Palacios");
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setSize(new Dimension(550, 300));
        frame.setResizable(false);
        crearMenu();
        frame.pack();
        frame.setVisible(true);
        inicializarModelos();
    }

    /**
     * Genera el menú superior de la interfaz y lo añade a la misma.
     */
    private void crearMenu(){
        //Se crea la barra de menu
        JMenuBar barraMenu = new JMenuBar();

        //Se crea el menu de archivo
        JMenu menuArchivo = new JMenu("Archivo");
        itemGuardar = new JMenuItem("Guardar");
        itemGuardar.setActionCommand("itemGuardar");
        menuArchivo.add(itemGuardar);
        itemCargar = new JMenuItem("Cargar");
        itemCargar.setActionCommand("itemCargar");
        menuArchivo.add(itemCargar);
        barraMenu.add(menuArchivo);

        //Se crea el boton de opciones
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("itemOpciones");
        barraMenu.add(itemOpciones);

        //Se establece la barra de menu del frame
        frame.setJMenuBar(barraMenu);
    }

    /**
     * Inicializa todas los DefaultListModel para su posterior uso en objetos de
     * tipo JList, de forma que el usuario pueda ver y operar con los datos
     * almacenados.
     */
    private void inicializarModelos(){
        dlmVeterinarios = new DefaultListModel<>();
        listVeterinarios.setModel(dlmVeterinarios);
        dlmAnimales = new DefaultListModel<>();
        listAnimales.setModel(dlmAnimales);
        dlmMedicamentos = new DefaultListModel<>();
        listMedicamentos.setModel(dlmMedicamentos);
    }
}

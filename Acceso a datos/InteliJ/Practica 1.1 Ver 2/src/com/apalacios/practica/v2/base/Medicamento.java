package com.apalacios.practica.v2.base;

import java.io.Serializable;

/**
 * Clase que representa un medicamento.
 * @author José Adrián Palacios Romo
 */
public class Medicamento implements Serializable {
    private String nombre;
    private String fabricante;
    private Float dosis;
    private Float precio;
    private String compuesto;

    /**
     * Constructor de la clase, instancia todos los atributos de la misma.
     * @param nombre String que representa el nombre del medicamento
     * @param fabricante String que representa el fabricante del medicamento
     * @param dosis Float que representa la dosis del medicamento
     * @param precio Float que representa el precio del medicamento
     * @param compuesto String que representa el compuesto principal del medicamento
     */
    public Medicamento(String nombre, String fabricante, Float dosis, Float precio, String compuesto) {
        this.nombre = nombre;
        this.fabricante = fabricante;
        this.dosis = dosis;
        this.precio = precio;
        this.compuesto = compuesto;
    }

    /**
     * Establece el nombre del medicamento
     * @param nombre String que representa el nombre del medicamento
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Establece el fabricante del medicamento
     * @param fabricante String que representa el fabricante del medicamento
     */
    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    /**
     * Establece la dosis del medicamento
     * @param dosis Float que representa la dosis del medicamento
     */
    public void setDosis(Float dosis) {
        this.dosis = dosis;
    }

    /**
     * Establece el precio del medicamento
     * @param precio Float que representa el precio del medicamento
     */
    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    /**
     * Establece el compuesto del medicamento
     * @param compuesto String que representa el compuesto principal del medicamento
     */
    public void setCompuesto(String compuesto) {
        this.compuesto = compuesto;
    }

    /**
     * Formatea los datos del medicamento a un String fácilmente legible.
     * @return String que representa todos los datos del medicamento
     */
    @Override
    public String toString() {
        return nombre + " " + fabricante + " " + dosis + "mg " + precio + "€ " + compuesto;
    }
}

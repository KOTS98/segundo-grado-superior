package com.apalacios.practica.v2;

import com.apalacios.practica.v2.gui.Controlador;
import com.apalacios.practica.v2.gui.Modelo;
import com.apalacios.practica.v2.gui.Vista;
import com.apalacios.practica.v2.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LoginDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtUsuario;
    private JTextField txtPass;
    private String usuario = "admin";
    private String pass = "admin";

    public LoginDialog() {
        setContentPane(contentPane);
        setModal(true);
        setSize(new Dimension(400, 200));
        setResizable(false);
        setTitle("Iniciar sesión");
        setLocationRelativeTo(null);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void onOK() {
        dispose();
        if(txtUsuario.getText().equalsIgnoreCase(usuario) && txtPass.getText().equalsIgnoreCase(pass)){
            Vista vista = new Vista();
            Modelo modelo = new Modelo();
            Controlador controlador = new Controlador(vista, modelo);
        }else{
            Util.mostrarMensajeError("Credenciales incorrectas");
            setVisible(true);
        }
    }

    private void onCancel() {
        dispose();
        System.exit(0);
    }

    public void mostrarDialogo(){
        setVisible(true);
    }
}

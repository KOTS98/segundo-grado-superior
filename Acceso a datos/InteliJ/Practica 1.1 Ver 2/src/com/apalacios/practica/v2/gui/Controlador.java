package com.apalacios.practica.v2.gui;
import com.apalacios.practica.v2.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;

/**
 * Clase Controlador, recibe una clase Vista y una clase Modelo, actúa como intermediario
 * para comunicar la vista y el modelo. También crea un ArrayList de botones donde se
 * almacenarán los botones del objeto vista.
 */
public class Controlador implements ActionListener{

    private Vista vista;
    private Modelo modelo;
    private HashSet<JButton> listaBotones;
    private HashSet<JMenuItem> listaItemsMenu;
    private Properties conf = new Properties();

    /**
     * Constructor de la clase, instancia la vista, el modelo y el HashSet de botones.
     * @param vista Representa la iterfaz gráfica del programa
     * @param modelo Es clase que se encarga de realizar las principales funciones del programa
     */
    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        this.listaBotones = new HashSet<>();
        this.listaItemsMenu = new HashSet<>();
        addActionListener(this);
    }

    /**
     * Añade todos los botones de la vista al ArrayList de botones, les añade un listener.
     * @param listener Envía una señal cada vez que uno de los objetos que escucha, cambia de estado
     */
    private void addActionListener(ActionListener listener) {
        //Se añaden todos los botones de la vista al HashSet de botones.
        listaBotones.add(vista.btnAltaVeterinario);
        listaBotones.add(vista.btnAltaAnimal);
        listaBotones.add(vista.btnAltaMedicamento);
        listaBotones.add(vista.btnModificarVeterinario);
        listaBotones.add(vista.btnModificarAnimal);
        listaBotones.add(vista.btnModificarMedicamento);
        listaBotones.add(vista.btnEliminarVeterinario);
        listaBotones.add(vista.btnEliminarAnimal);
        listaBotones.add(vista.btnEliminarMedicamento);
        listaBotones.add(vista.btnAnyadirAnimal);
        listaBotones.add(vista.btnAnyadirMedicamento);

        //Recorre el HashSet de botones y les añade un listener
        for (JButton boton : listaBotones) {
            boton.addActionListener(listener);
        }

        //Se añaden los JMenuItem de la vista al HashSet de JMenuItem
        listaItemsMenu.add(vista.itemGuardar);
        listaItemsMenu.add(vista.itemCargar);
        listaItemsMenu.add(vista.itemOpciones);

        //Recorre el HashSet de JMenuItem y les añade un listener
        for (JMenuItem item : listaItemsMenu) {
            item.addActionListener(listener);
        }

        //Añade el listener a la vista
        vista.frame.addWindowListener(new WindowListener() {
            /**
             * Este método se activa al abrirse la ventana principal del programa, es decir, al cargar la aplicación.
             * Lee el archivo de propiedades para determinar si debe o no realizar la carga del último archivo.
             * @param e Evento de ventana que detecta cualquier interacción con la misma.
             */
            @Override
            public void windowOpened(WindowEvent e) {
                try {
                    FileReader fr = new FileReader("properties.conf");
                    conf.load(fr);
                    if(conf.getProperty("cargar_auto").equalsIgnoreCase("true")){
                        modelo.cargarUltimoArchivo(vista.dlmVeterinarios, vista.dlmAnimales, vista.dlmMedicamentos);
                    }
                } catch (FileNotFoundException ex) {
                    //En caso de que no se encuentre el archivo de configuración, el modelo se encargará de crearlo
                    //de forma automática.
                } catch (IOException ex) {
                    Util.mostrarMensajeError("Error de lectura de archivo. Se cargará un archivo nuevo.");
                }
            }

            /**
             * Lee el archivo de configuración y determina si debe guardar automáticamente el archivo actual, y en que
             * ruta debería de guardarlo.
             * @param e Evento de ventana que detecta cualquier interacción con la misma.
             */
            @Override
            public void windowClosing(WindowEvent e) {
                try{
                    FileReader fr = new FileReader("properties.conf");
                    conf.load(fr);
                    if(conf.getProperty("guardar_auto").equalsIgnoreCase("true") &&
                            !conf.getProperty("ruta_defecto").equalsIgnoreCase("")){
                        modelo.guardarDefault();
                    }
                } catch (FileNotFoundException ex) {
                    //En caso de que no se encuentre el archivo de configuración, el modelo se encargará de crearlo
                    //de forma automática.
                } catch (IOException ex) {
                    Util.mostrarMensajeError("Error al hacer el guardado automático.");
                }
            }

            //Métodos no necesarios.
            @Override
            public void windowClosed(WindowEvent e) {}
            @Override
            public void windowIconified(WindowEvent e) {}
            @Override
            public void windowDeiconified(WindowEvent e) {}
            @Override
            public void windowActivated(WindowEvent e) {}
            @Override
            public void windowDeactivated(WindowEvent e) {}
        });
    }

    /**
     * Recibe un objeto de tipo ActionEvent cada vez que se pulsa un botón de la vista, después
     * ejecuta la acción correspondiente al botón pulsado.
     * @param e Detecta si se ha interacturado con algún elemento de la interfaz, como los botones o los menus.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //Se compara el action command recibido al pulsar un botón hasta encontrar una coincidencia,
        //una vez encontrada, se llama al método correspondiente en la clase modelo.
        switch (e.getActionCommand()){
            case "btnAltaVeterinario":
                modelo.altaVeterinario(vista.dlmVeterinarios, vista.txtNombreVeterinario.getText(),
                        vista.txtApellidoVeterinario.getText(), vista.rBtnMasculinoVeterinario.isSelected(),
                        vista.datePickerFechaNacimientoVeterinario.getDate(), vista.txtDNIVeterinario.getText());
                break;
            case "btnAltaAnimal":
                modelo.altaAnimal(vista.dlmAnimales, vista.txtNombreAnimal.getText(), vista.txtEspecieAnimal.getText(),
                        vista.rBtnMachoAnimal.isSelected(), vista.txtPesoAnimal.getText(), vista.txtDuenyoAnimal.getText());
                break;
            case "btnAltaMedicamento":
                modelo.altaMedicamento(vista.dlmMedicamentos, vista.txtNombreMedicamento.getText(),
                        vista.txtFabricanteMedicamento.getText(), vista.txtDosisMedicamento.getText(),
                        vista.txtPrecioMedicamento.getText(), vista.txtCompuestoMedicamento.getText());
                break;
            case "btnModificarVeterinario":
               modelo.modificarVeterinario(vista.dlmVeterinarios, vista.listVeterinarios,
                       vista.txtNombreVeterinario.getText(), vista.txtApellidoVeterinario.getText(),
                       vista.rBtnMasculinoVeterinario.isSelected(), vista.datePickerFechaNacimientoVeterinario.getDate(),
                       vista.txtDNIVeterinario.getText());
                break;
            case "btnModificarAnimal":
                modelo.modificarAnimal(vista.dlmAnimales, vista.listAnimales, vista.txtNombreAnimal.getText(),
                        vista.txtEspecieAnimal.getText(), vista.rBtnMachoAnimal.isSelected(),
                        vista.txtPesoAnimal.getText(), vista.txtDuenyoAnimal.getText());
                break;
            case "btnModificarMedicamento":
                modelo.modificarMedicamento(vista.dlmMedicamentos, vista.listMedicamentos, vista.txtNombreMedicamento.getText(),
                        vista.txtFabricanteMedicamento.getText(), vista.txtDosisMedicamento.getText(),
                        vista.txtPrecioMedicamento.getText(), vista.txtCompuestoMedicamento.getText());
                break;
            case "btnEliminarVeterinario":
                modelo.eliminarVeterinario(vista.dlmVeterinarios, vista.listVeterinarios);
                break;
            case "btnEliminarAnimal":
                modelo.eliminarAnimal(vista.dlmAnimales, vista.listAnimales);
                break;
            case "btnEliminarMedicamento":
                modelo.eliminarMedicamento(vista.dlmMedicamentos, vista.listMedicamentos);
                break;
            case "btnAnyadirAnimal":
                modelo.anyadirAnimal(vista.dlmVeterinarios, vista.listVeterinarios);
                break;
            case "btnAnyadirMedicamento":
                modelo.anyadirMedicamento(vista.dlmAnimales, vista.listAnimales);
                break;
            case "itemGuardar":
                modelo.guardarArchivo();
                break;
            case "itemCargar":
                modelo.cargarArchivo(vista.dlmVeterinarios, vista.dlmAnimales, vista.dlmMedicamentos);
                break;
            case "itemOpciones":
                modelo.menuOpciones();
                break;
        }
    }
}

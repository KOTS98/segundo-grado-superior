package com.apalacios.practica.v2.gui;

import com.apalacios.practica.v2.base.Animal;
import com.apalacios.practica.v2.base.Medicamento;
import com.apalacios.practica.v2.base.Veterinario;
import com.apalacios.practica.v2.util.Util;

import javax.swing.*;
import java.io.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Properties;

/**
 * Esta clase se encarga de realizar las principales funciones del programa. Sus métodos son accesados desde el
 * controlador, quién le provee con datos de la vista.
 *
 * @author José Adrián Palacios Romo
 */
public class Modelo {
    private HashSet<Veterinario> veterinarios;
    private HashSet<Animal> animales;
    private HashSet<Medicamento> medicamentos;
    private Properties conf = new Properties();

    /**
     * Constructor de la clase, inicializa las variables de la misma, y ejecuta el método que crea el archivo de
     * configuración.
     */
    public Modelo(){
        this.veterinarios = new HashSet<>();
        this.animales = new HashSet<>();
        this.medicamentos = new HashSet<>();
        crearProperties();
    }

    /**
     * Comprueba si existe o no un archivo de configuración y en caso de que no exista, lo crea y lo dota de los
     * parámetros necesarios.
     */
    private void crearProperties(){
        if (!new File("properties.conf").exists()){
            try {
                conf.setProperty("ultimo_archivo", "");
                conf.setProperty("guardar_auto", "");
                conf.setProperty("cargar_auto", "");
                conf.setProperty("ruta_defecto", "");
                FileWriter fw;
                fw = new FileWriter("properties.conf");
                conf.store(fw, "");
            } catch (IOException e) {
                e.printStackTrace();
                Util.mostrarMensajeError("Error al crear el archivo de configuración.\n Los parámetros de configuración");
            }
        }
    }

    /**
     * Da de alta un nuevo veterinario y lo plasma en en JList de veterinarios en la lista.
     * @param dlmVeterinarios Representa el modelo del JList de veterinarios
     * @param nombre Representa el nombre del veterinario
     * @param apellido Representa el apellido del veterinario
     * @param masculino Representa el sexo del veterinario
     * @param fechaNac Representa la fecha de nacimiento del veterinario
     * @param dni Representa el dni del veterinario
     */
    void altaVeterinario(DefaultListModel<Veterinario> dlmVeterinarios, String nombre, String apellido,
                         Boolean masculino, LocalDate fechaNac, String dni){
        String sexo = "masculino";
        if(!masculino){
            sexo = "femenino";
        }
        if(!nombre.equalsIgnoreCase("") && !apellido.equalsIgnoreCase("") && fechaNac != null &&
                !dni.equalsIgnoreCase("")){
            Veterinario veterinario = new Veterinario(nombre, apellido, sexo, fechaNac, dni);
            veterinarios.add(veterinario);
        }else {
            Util.mostrarMensajeError("Debes rellenar todos los campos para continuar");
        }
        actualizarVeterinarios(dlmVeterinarios);
    }

    /**
     * Da de alta un nuevo animal y lo plasma en el JList de animales en la lista.
     * @param dlmAnimales Representa elmodelo del JList de animales
     * @param nombre Representa el nombre del animal
     * @param especie Representa la especie del animal
     * @param macho Representa el sexo del animal
     * @param strPeso Representa el peso del animal
     * @param duenyo Representa el dueño del animal
     */
    void altaAnimal(DefaultListModel<Animal> dlmAnimales, String nombre, String especie, Boolean macho, String strPeso,
                    String duenyo){
        String sexo = "macho";
        if(!macho){
            sexo = "hembra";
        }
        try{
            Float peso = Float.valueOf(strPeso);
            if(!nombre.equalsIgnoreCase("") && !especie.equalsIgnoreCase("") &&
                    !strPeso.equalsIgnoreCase("") && !duenyo.equalsIgnoreCase("")){
                Animal animal = new Animal(nombre, especie, sexo, peso, duenyo);
                animales.add(animal);
            }else{
                Util.mostrarMensajeError("Debes rellenar todos los campos para continuar");
            }
        }catch(NumberFormatException e){
            Util.mostrarMensajeError("Tipo de dato introducido incorrecto.\n" +
                    "Compruebe el campo \"Peso\"");
        }
        actualizarAnimales(dlmAnimales);
    }

    /**
     * Da de alta un nuevo medicamento y lo plasma en el JList de medicamentos en la lista.
     * @param dlmMedicamentos Representa el modelo del JList de medicamentos
     * @param nombre Representa el nombre del medicamento
     * @param fabricante Representa el nombre del fabricante del medicamento
     * @param strDosis Representa la dosis en miligramos del medicamento
     * @param strPrecio Representa el precio en euros (€) del medicamento
     * @param compuesto Representa el compuesto principal del medicamento
     */
    void altaMedicamento(DefaultListModel<Medicamento> dlmMedicamentos, String nombre, String fabricante,
                         String strDosis, String strPrecio, String compuesto){
        try{
            Float dosis = Float.valueOf(strDosis);
            Float precio = Float.valueOf(strPrecio);
            if(!nombre.equalsIgnoreCase("") && !fabricante.equalsIgnoreCase("") &&
                    !strDosis.equalsIgnoreCase("") && !strPrecio.equalsIgnoreCase("") &&
                    !compuesto.equalsIgnoreCase("")){
                Medicamento medicamento = new Medicamento(nombre, fabricante, dosis, precio, compuesto);
                medicamentos.add(medicamento);
            }else {
                Util.mostrarMensajeError("Debes rellenar todos los campos para continuar");
            }
        }catch(NumberFormatException e){
            Util.mostrarMensajeError("Tipo de dato introducido incorrecto.\n" +
                    "Compruebe los campos \"Dosis\" y \"Precio\"");
        }
        actualizarMedicamentos(dlmMedicamentos);
    }

    /**
     * Modifica el veterinario seleccionado de la lista dándole los valores actuales de los campos.
     * @param dlmVeterinarios Representa el modelo del JList de veterinarios
     * @param lista Representa la lista que muestra todos los veterinarios
     * @param nombre Representa el nombre del veterinario
     * @param apellido Representa el apellido del veterinario
     * @param masculino Representa el sexo del veterinario
     * @param fechaNac Representa la fecha de nacimiento del veterinario
     * @param dni Representa el dni del veterinario
     */
    void modificarVeterinario(DefaultListModel<Veterinario> dlmVeterinarios, JList lista, String nombre, String apellido,
                              Boolean masculino, LocalDate fechaNac, String dni){
        String sexo = "masculino";
        if(!masculino){
            sexo = "femenino";
        }
        if(!nombre.equalsIgnoreCase("") && !apellido.equalsIgnoreCase("") && fechaNac != null &&
                !dni.equalsIgnoreCase("")){
            if(!lista.isSelectionEmpty()){
                dlmVeterinarios.getElementAt(lista.getSelectedIndex()).setNombre(nombre);
                dlmVeterinarios.getElementAt(lista.getSelectedIndex()).setApellido(apellido);
                dlmVeterinarios.getElementAt(lista.getSelectedIndex()).setSexo(sexo);
                dlmVeterinarios.getElementAt(lista.getSelectedIndex()).setFechaNacimiento(fechaNac);
                dlmVeterinarios.getElementAt(lista.getSelectedIndex()).setDni(dni);
            }else{
                Util.mostrarMensajeError("Debes seleccionar el objeto que deseas modificar");
            }
        }else {
            Util.mostrarMensajeError("Debes rellenar todos los campos para continuar");
        }
        actualizarVeterinarios(dlmVeterinarios);
    }

    /**
     * Modifica el animal seleccionado de la lista dándole los valores actuales de los campos.
     * @param dlmAnimales Representa elmodelo del JList de animales
     * @param lista Representa la lista que muestra todos los animales
     * @param nombre Representa el nombre del animal
     * @param especie Representa la especie del animal
     * @param macho Representa el sexo del animal
     * @param strPeso Representa el peso del animal
     * @param duenyo Representa el dueño del animal
     */
    void modificarAnimal(DefaultListModel<Animal> dlmAnimales, JList lista, String nombre, String especie, Boolean macho,
                         String strPeso, String duenyo){
        String sexo = "macho";
        if(!macho){
            sexo = "hembra";
        }
        try{
            Float peso = Float.valueOf(strPeso);
            if(!nombre.equalsIgnoreCase("") && !especie.equalsIgnoreCase("") &&
                    !strPeso.equalsIgnoreCase("") && !duenyo.equalsIgnoreCase("")){
                if (!lista.isSelectionEmpty()){
                    dlmAnimales.getElementAt(lista.getSelectedIndex()).setNombre(nombre);
                    dlmAnimales.getElementAt(lista.getSelectedIndex()).setEspecie(especie);
                    dlmAnimales.getElementAt(lista.getSelectedIndex()).setSexo(sexo);
                    dlmAnimales.getElementAt(lista.getSelectedIndex()).setPeso(peso);
                    dlmAnimales.getElementAt(lista.getSelectedIndex()).setDuenyo(duenyo);
                }else {
                    Util.mostrarMensajeError("Debes seleccionar el objeto que deseas modificar");
                }
            }else{
                Util.mostrarMensajeError("Debes rellenar todos los campos para continuar");
            }
        }catch(NumberFormatException e){
            Util.mostrarMensajeError("Tipo de dato introducido incorrecto.\n" +
                    "Compruebe el campo \"Peso\"");
        }
        actualizarAnimales(dlmAnimales);
    }

    /**
     * Modifica el medicamento seleccionado de la lista dándole los valores actuales de los campos.
     * @param dlmMedicamentos Representa el modelo del JList de medicamentos
     * @param lista Representa la lista que muestra todos los medicamentos
     * @param nombre Representa el nombre del medicamento
     * @param fabricante Representa el nombre del fabricante del medicamento
     * @param strDosis Representa la dosis en miligramos del medicamento
     * @param strPrecio Representa el precio en euros (€) del medicamento
     * @param compuesto Representa el compuesto principal del medicamento
     */
    void modificarMedicamento(DefaultListModel<Medicamento> dlmMedicamentos, JList lista, String nombre, String fabricante,
                              String strDosis, String strPrecio, String compuesto){
        try{
            Float dosis = Float.valueOf(strDosis);
            Float precio = Float.valueOf(strPrecio);
            if(!nombre.equalsIgnoreCase("") && !fabricante.equalsIgnoreCase("") &&
                    !strDosis.equalsIgnoreCase("") && !strPrecio.equalsIgnoreCase("") &&
                    !compuesto.equalsIgnoreCase("")){
                if(!lista.isSelectionEmpty()){
                    dlmMedicamentos.getElementAt(lista.getSelectedIndex()).setNombre(nombre);
                    dlmMedicamentos.getElementAt(lista.getSelectedIndex()).setFabricante(fabricante);
                    dlmMedicamentos.getElementAt(lista.getSelectedIndex()).setDosis(dosis);
                    dlmMedicamentos.getElementAt(lista.getSelectedIndex()).setPrecio(precio);
                    dlmMedicamentos.getElementAt(lista.getSelectedIndex()).setCompuesto(compuesto);
                }else{
                    Util.mostrarMensajeError("Debes seleccionar el objeto que deseas modificar");
                }
            }else {
                Util.mostrarMensajeError("Debes rellenar todos los campos para continuar");
            }
        }catch(NumberFormatException e){
            Util.mostrarMensajeError("Tipo de dato introducido incorrecto.\n" +
                    "Compruebe los campos \"Dosis\" y \"Precio\"");
        }
        actualizarMedicamentos(dlmMedicamentos);
    }

    /**
     * Elimina de la lista al veterinario seleccionado.
     * @param dlmVeterinarios Representa el modelo del JList de veterinarios
     * @param lista Representa la lista que muestra todos los veterinarios
     */
    void eliminarVeterinario(DefaultListModel<Veterinario> dlmVeterinarios, JList lista){
        if (!lista.isSelectionEmpty()){
            veterinarios.remove(lista.getSelectedValue());
        }else {
            Util.mostrarMensajeError("Debes seleccionar el objeto que deseas eliminar");
        }
        actualizarVeterinarios(dlmVeterinarios);
    }

    /**
     * Elimina de la lista al animal seleccionado.
     * @param dlmAnimales Representa elmodelo del JList de animales
     * @param lista Representa la lista que muestra todos los animales
     */
    void eliminarAnimal(DefaultListModel<Animal> dlmAnimales, JList lista){
        if (!lista.isSelectionEmpty()){
            animales.remove(lista.getSelectedValue());
        }else {
            Util.mostrarMensajeError("Debes seleccionar el objeto que deseas eliminar");
        }
        actualizarAnimales(dlmAnimales);
    }

    /**
     * Elimina de la lista el medicamento seleccionado.
     * @param dlmMedicamentos Representa el modelo del JList de medicamentos
     * @param lista Representa la lista que muestra todos los medicamentos
     */
    void eliminarMedicamento(DefaultListModel<Medicamento> dlmMedicamentos, JList lista){
        if (!lista.isSelectionEmpty()){
            medicamentos.remove(lista.getSelectedValue());
        }else {
            Util.mostrarMensajeError("Debes seleccionar el objeto que deseas eliminar");
        }
        actualizarMedicamentos(dlmMedicamentos);
    }

    /**
     * Refresca la lista de veterinarios.
     * @param dlmVeterinarios Representa el modelo del JList que muestra los veterinarios.
     */
    private void actualizarVeterinarios(DefaultListModel<Veterinario> dlmVeterinarios){
        dlmVeterinarios.clear();
        for (Veterinario veterinario : veterinarios) {
            dlmVeterinarios.addElement(veterinario);
        }
    }

    /**
     * Despliega un diálogo para añadir o eliminar animales al cuidado del veterinario seleccionado
     * @param dlmVeterinarios Representa el modelo del JList de veterinarios
     * @param listVeterinarios Representa la lista que muestra todos los veterinarios
     */
    void anyadirAnimal(DefaultListModel<Veterinario> dlmVeterinarios, JList listVeterinarios){
        if(!listVeterinarios.isSelectionEmpty()){
            AnyadirAnimalDialog dialog = new AnyadirAnimalDialog(dlmVeterinarios.get(listVeterinarios.getSelectedIndex()),
                    animales);
            dialog.mostrarDialogo();
        }else {
            Util.mostrarMensajeError("Debes seleccionar un veterinario para proceder");
        }
    }

    /**
     * Despliega un diálogo para añadir o eliminar medicamentos recetados al animal seleccionado
     * @param dlmAnimales Representa elmodelo del JList de animales
     * @param listAnimales Representa la lista que muestra todos los animales
     */
    void anyadirMedicamento(DefaultListModel<Animal> dlmAnimales, JList listAnimales){
        if (!listAnimales.isSelectionEmpty()){
            AnyadirMedicamentoDialog dialog = new AnyadirMedicamentoDialog(dlmAnimales.get(listAnimales.getSelectedIndex()), medicamentos);
            dialog.mostrarDialogo();
        }else{
            Util.mostrarMensajeError("Debes selecciona un animal para proceder");
        }
    }

    /**
     * Refresca la lista de animales.
     * @param dlmAnimales Representa el modelo del JList que muestra los animales.
     */
    private void actualizarAnimales(DefaultListModel<Animal> dlmAnimales){
        dlmAnimales.clear();
        for (Animal animal : animales) {
            dlmAnimales.addElement(animal);
        }
    }

    /**
     * Refresca la lista de medicamentos.
     * @param dlmMedicamentos Representa el modelo del JList que muestra los medicamentos.
     */
    private void actualizarMedicamentos(DefaultListModel<Medicamento> dlmMedicamentos){
        dlmMedicamentos.clear();
        for (Medicamento medicamento : medicamentos) {
            dlmMedicamentos.addElement(medicamento);
        }
    }

    /**
     * Crea un archivo con el nombre y la ubicación especificadas por el usuario que contendrá todos los datos que haya
     * actualmente en la aplicación.
     */
    void guardarArchivo(){
        JFileChooser selector = new JFileChooser();
        int opcion = selector.showSaveDialog(null);
        if(opcion == JFileChooser.APPROVE_OPTION){
            try {
                conf.setProperty("ultimo_archivo", selector.getSelectedFile().getPath());
                FileWriter fw = new FileWriter("properties.conf");
                conf.store(fw, "");
                fw.close();
                ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(selector.getSelectedFile().getPath()));
                oos.writeObject(veterinarios);
                oos.writeObject(animales);
                oos.writeObject(medicamentos);
                oos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
                Util.mostrarMensajeError("Error de escritura de archivo.");
            }
        }
    }

    /**
     * Carga los datos de veterinarios, animales y medicamentos desde el fichero seleccionado.
     */
    void cargarArchivo(DefaultListModel<Veterinario> dlmVeterinarios, DefaultListModel<Animal> dlmAnimales,
                       DefaultListModel<Medicamento> dlmMedicamentos){
        JFileChooser selector = new JFileChooser();
        int opcion = selector.showOpenDialog(null);
        if (opcion == JFileChooser.APPROVE_OPTION){
            try{
                conf.setProperty("ultimo_archivo", selector.getSelectedFile().getPath());
                FileWriter fw = new FileWriter("properties.conf");
                conf.store(fw, "");
                fw.close();
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(selector.getSelectedFile().getPath()));
                veterinarios = (HashSet<Veterinario>) ois.readObject();
                animales = (HashSet<Animal>) ois.readObject();
                medicamentos = (HashSet<Medicamento>) ois.readObject();
                ois.close();
                actualizarVeterinarios(dlmVeterinarios);
                actualizarAnimales(dlmAnimales);
                actualizarMedicamentos(dlmMedicamentos);
            }catch (FileNotFoundException e){
                e.printStackTrace();
                Util.mostrarMensajeError("Error. El archivo especificado no existe.");
            }catch (IOException | ClassNotFoundException e){
                e.printStackTrace();
                Util.mostrarMensajeError("Error de lectura de archivo.");
            }
        }
    }

    /**
     * Determina basándose en el archivo de configuración, si debe o no de guardar automáticamente el archivo con
     * el que se está trabajando, en caso afirmativo, determina la ruta de guardado por defecto especificada en el
     * archivo de configuración.
     */
    void guardarDefault() {
        try {
            FileReader fr = new FileReader("properties.conf");
            conf.load(fr);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(conf.getProperty("ruta_defecto")));
            fr.close();
            oos.writeObject(veterinarios);
            oos.writeObject(animales);
            oos.writeObject(medicamentos);
            oos.close();
            conf.setProperty("ultimo_archivo", conf.getProperty("ruta_defecto"));
            FileWriter fw = new FileWriter("properties.conf");
            conf.store(fw, "");
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            Util.mostrarMensajeError("Error de escritura de archivo.");
        }
    }

    /**
     * Carga el último archivo generado por el programa, leyendo la ruta desde el archivo de configuración. En caso
     * de que no haya una ruta guardada, o no se encuentre el archivo en la ruta especificada, se generará un nuevo
     * archivo.
     */
    void cargarUltimoArchivo(DefaultListModel<Veterinario> dlmVeterinarios, DefaultListModel<Animal> dlmAnimales,
                             DefaultListModel<Medicamento> dlmMedicamentos) {
        try{
            FileReader fr = new FileReader("properties.conf");
            conf.load(fr);
            if(!conf.getProperty("ultimo_archivo").equalsIgnoreCase("")){
                ObjectInputStream ois = new ObjectInputStream(new FileInputStream(conf.getProperty("ultimo_archivo")));
                fr.close();
                veterinarios = (HashSet<Veterinario>) ois.readObject();
                animales = (HashSet<Animal>) ois.readObject();
                medicamentos = (HashSet<Medicamento>) ois.readObject();
                ois.close();
                actualizarVeterinarios(dlmVeterinarios);
                actualizarAnimales(dlmAnimales);
                actualizarMedicamentos(dlmMedicamentos);
            }
            fr.close();
        }catch (FileNotFoundException e){
            Util.mostrarMensajeError("Error. No se encuentra el último archivo, se cargará un archivo nuevo.");
        }catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
            Util.mostrarMensajeError("Error de lectura de archivo. Se cargará un archivo nuevo.");
        }
    }

    void menuOpciones(){
        MenuOpcionesDialog dialog = new MenuOpcionesDialog();
        dialog.mostrarDialogo();
    }
}

package com.apalacios.practica.v2.gui;

import com.apalacios.practica.v2.base.Animal;
import com.apalacios.practica.v2.base.Medicamento;
import com.apalacios.practica.v2.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;

public class AnyadirMedicamentoDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList<Medicamento> listMedicacionActual;
    private DefaultListModel<Medicamento> dlmMedicacionActual;
    private JList<Medicamento> listMedicacionDisponible;
    private DefaultListModel<Medicamento> dlmMedicacionDisponible;
    private JButton btnRetirar;
    private JButton btnAnyadir;

    private Animal animal;

    public AnyadirMedicamentoDialog(Animal animal, HashSet<Medicamento> medicamentos) {
        this.animal = animal;

        this.dlmMedicacionActual = new DefaultListModel<>();
        listMedicacionActual.setModel(dlmMedicacionActual);
        this.dlmMedicacionDisponible = new DefaultListModel<>();
        listMedicacionDisponible.setModel(dlmMedicacionDisponible);

        for (Medicamento medicamento : medicamentos) {
            dlmMedicacionDisponible.addElement(medicamento);
        }

        for (Medicamento medicamento : animal.getMedicamentosRecetados()) {
            dlmMedicacionActual.addElement(medicamento);
        }

        setSize(new Dimension(400, 300));
        setResizable(false);
        setTitle("Añadir medicación al animal");
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        btnAnyadir.addActionListener(e -> anyadirMedicacion());

        btnRetirar.addActionListener(e -> retirarMedicacion());

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
    }

    private void anyadirMedicacion(){
        if(!listMedicacionDisponible.isSelectionEmpty()){
            dlmMedicacionActual.addElement(dlmMedicacionDisponible.get(listMedicacionDisponible.getSelectedIndex()));
        }else{
            Util.mostrarMensajeError("Debes seleccionar un medicamento para recetar");
        }
    }

    private void retirarMedicacion(){
        if(!listMedicacionActual.isSelectionEmpty()){
            animal.getMedicamentosRecetados().remove(dlmMedicacionActual.get(listMedicacionActual.getSelectedIndex()));
            dlmMedicacionActual.clear();
            for (Medicamento medicamento : animal.getMedicamentosRecetados()) {
                dlmMedicacionActual.addElement(medicamento);
            }
        }else{
            Util.mostrarMensajeError("Debes seleccionar un medicamento para retirar");
        }
    }

    private void onOK() {
        animal.getMedicamentosRecetados().clear();
        for(int i = 0; i < dlmMedicacionActual.size(); i++){
            animal.getMedicamentosRecetados().add(dlmMedicacionActual.get(i));
        }
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    void mostrarDialogo(){
        setVisible(true);
    }
}

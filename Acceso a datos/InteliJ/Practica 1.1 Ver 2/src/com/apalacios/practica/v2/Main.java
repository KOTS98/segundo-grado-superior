package com.apalacios.practica.v2;

/**
 * Clase princpipal del programa
 *
 * @author José Adrián Palacios Romo
 */
public class Main {

    /**
     * Método principal del programa, se encarga de crear el modelo, la vista y el controlador, cumpliendo con el
     * patrón de diseño MVC.
     *
     * @param args
     */
    public static void main(String[] args) {
        LoginDialog dialog = new LoginDialog();
        dialog.mostrarDialogo();
    }
}

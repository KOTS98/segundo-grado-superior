package base;

import java.time.LocalDate;

/**
 * Clase que representa a un videojuego
 * @author Jose Adrian Palacios Romo
 */
public class Videojuego {
    private String nombre;
    private String genero;
    private LocalDate fechaLanzamiento;
    private float precio;

    /**
     * Constructor de la clase, instancia todos los parametros del objeto
     *
     * @param nombre Representa el nombre del videojuego
     * @param genero Representa el genero del videojuego
     * @param fechaLanzamiento Representa la fecha de lanzamiento del videojuego
     * @param precio Representa el precio del videojuego
     */
    public Videojuego(String nombre, String genero, LocalDate fechaLanzamiento, float precio){
        this.nombre = nombre;
        this.genero = genero;
        this.fechaLanzamiento = fechaLanzamiento;
        this.precio = precio;
    }

    /**
     * Devuelve el nombre del videojuego
     *
     * @return String que representa el nombre del videojuego
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Devuelve el genero del videojuego
     *
     * @return String que representa el genero del videojuego
     */
    public String getGenero() {
        return genero;
    }

    /**
     * Devuelve la fecha de lanzamiento del videojuego
     *
     * @return LocalDate que representa la fecha de lanzamiento del videojuego
     */
    public LocalDate getFechaLanzamiento() {
        return fechaLanzamiento;
    }

    /**
     * Devuelve el precio del videojuego
     *
     * @return float que representa el precio del videojuego
     */
    public float getPrecio() {
        return precio;
    }
}

package com.apalacios.videojuegos;

import util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * Dialogo que permite ver y manipular los parametros de conexion con la base de datos
 *
 * @author Jose Adrian Palacios Romo
 */
public class DialogConfiguracion extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtConfPass;
    private JTextField txtConfUser;
    private JTextField txtConfTabla;
    private JTextField txtConfBBDD;
    private JTextField txtConfPuerto;
    private JTextField txtConfServidor;

    private Properties conf;

    /**
     * Constructor de la clase, carga las preferencias del archivo de preferencias
     */
    DialogConfiguracion() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        conf = new Properties();

        cargarPreferencias();

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        setTitle("Configuración");
        setLocationRelativeTo(null);
        setSize(new Dimension(400, 300));
        setResizable(false);
        pack();
        setVisible(true);
    }

    /**
     * Lee el archivo de configuracion "Properties.conf" y actualiza los campos basandose en los parametros del archivo
     */
    private void cargarPreferencias(){
        try {
            FileReader fr = new FileReader("Properties.conf");
            conf.load(fr);

            txtConfServidor.setText(conf.getProperty("host"));
            txtConfPuerto.setText(conf.getProperty("puerto"));
            txtConfBBDD.setText(conf.getProperty("nombre_BBDD"));
            txtConfTabla.setText(conf.getProperty("tabla_BBDD"));
            txtConfUser.setText(conf.getProperty("user"));
            txtConfPass.setText(conf.getProperty("pass"));

            fr.close();

        //Controla la posibilidad de que el archivo de configuracion no exista
        } catch (FileNotFoundException e) {
            Util.mostrarMensajeError("Archivo de configuración no encontrado." +
                    "\nReiniciar la aplicación creará uno nuevo.");

        //Controla posibles errores de lectura del archvio
        } catch (IOException e) {
            Util.mostrarMensajeError("Error al leer el archivo de configuración.");
        }
    }

    /**
     * Modifica el archivo de configuracion basandose en los valores de los campos
     */
    private void guardarPreferencias(){
        try{
            FileWriter fw = new FileWriter("Properties.conf");

            conf.setProperty("host", txtConfServidor.getText());
            conf.setProperty("puerto", txtConfPuerto.getText());
            conf.setProperty("nombre_BBDD", txtConfBBDD.getText());
            conf.setProperty("tabla_BBDD", txtConfTabla.getText());
            conf.setProperty("user", txtConfUser.getText());
            conf.setProperty("pass", txtConfPass.getText());
            conf.store(fw, "");

            fw.close();

        //Controla posibles errores de escritura del archivo
        } catch (IOException e){
            Util.mostrarMensajeError("Error al escribir el archivo de configuración.");
        }
    }

    /**
     * Guarda los cambios al archivo de configuracion y los actualiza en el programa
     */
    private void onOK() {
        guardarPreferencias();
        ConexionBBDD.obtenerConfiguracion();
        dispose();
    }

    /**
     * Cancela los cambios
     */
    private void onCancel() {
        dispose();
    }
}

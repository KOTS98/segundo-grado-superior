package com.apalacios.videojuegos;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Representa la interfaz grafica del programa
 *
 * @author Jose Adrian Palacios Romo
 */
public class Vista {
    private JFrame frame;
    private JPanel panelPrincipal;
    JTextField txtNombre;
    JTextField txtGenero;
    JTextField txtPrecio;
    DatePicker dtpFechaLanzamiento;
    JButton btnAnyadir;
    JButton btnModificar;
    JButton btnEliminar;
    JButton btnBuscar;
    JTextField txtBusqueda;
    JComboBox<String> cbBusqueda;
    JTable tblTabla;

    //Menu
    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JMenuItem itemConf;
    JMenuItem itemCrearTabla;

    //Tabla
    DefaultTableModel dtmTabla;

    /**
     * Constructor de la clase
     */
    Vista(){
        frame = new JFrame("Práctica 2.1 - Adrián Palacios");
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(700, 400));
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        crearMenu();
        dtmTabla = new DefaultTableModel();
        tblTabla.setModel(dtmTabla);
        tblTabla.setDefaultEditor(Object.class, null);

        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Genera la barra de menu superior con sus diferentes elementos y les anyade a estos su correspondiente
     * ActionComand
     */
    private void crearMenu(){
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");
        itemConf = new JMenuItem("Configuración");
        itemConf.setActionCommand("configuracion");
        itemCrearTabla = new JMenuItem("Crear tabla");
        itemCrearTabla.setActionCommand("crearTabla");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenu menuBBDD = new JMenu("Base de datos");
        menuBBDD.add(itemConf);
        menuBBDD.add(itemCrearTabla);

        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);
        barraMenu.add(menuBBDD);

        frame.setJMenuBar(barraMenu);
    }
}

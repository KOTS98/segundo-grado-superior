package com.apalacios.videojuegos;

import util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Dialogo que permite crear una nueva tabla para la base de datos
 *
 * @author Jose Adrian Palacios Romo
 */
public class CrearTablaDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtNombreTabla;

    /**
     * Constructor de la clase
     */
    CrearTablaDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        setTitle("Nueva tabla");
        setLocationRelativeTo(null);
        setSize(new Dimension(230, 120));
        setResizable(false);
        pack();
        setVisible(true);
    }

    /**
     * Genera y ejecuta una sentencia de SQL con la que crea una tabla (si no existe ya una bajo el mismo nombre) con
     * el nombre especificado por el usuario
     *
     * @param nombreTabla Representa el nombre deseado para la tabla
     */
    private void crearTabla(String nombreTabla){
        try {
            String consulta = "CREATE TABLE IF NOT EXISTS " + nombreTabla + "(" +
                    "id INTEGER auto_increment primary key," +
                    "nombre varchar(50)," +
                    "genero varchar(20)," +
                    "fecha_lanzamiento date," +
                    "precio float" +
                    ");";
            PreparedStatement sentencia;
            sentencia = Modelo.conexion.prepareStatement(consulta);
            sentencia.executeUpdate();
            Util.mostrarMensajeInfo("Tabla \"" + nombreTabla + "\" creada con éxito.");

        //Controla posibles excepciones de SQL
        } catch (SQLException e) {
            Util.mostrarMensajeError("Error al crear la tabla \"" + nombreTabla + "\" en la\n base de datos.");
            e.printStackTrace();
        }
    }

    /**
     * Formatea la cadena introducida eliminando los espacios y cambiando a mayusculas las iniciales de las palabras
     * que la componen a excepcion de la primera.
     * Ejemplo:
     *
     * Cadena introducida - tabla de prueba
     * Cadena resultante  - tablaDePrueba
     *
     * @param cadena Representa la cadena que se desea formatear
     * @return String que representa la cadena formateada
     */
    private String formatearCadena(String cadena){
        String[] caracteres = cadena.split("");
        StringBuilder nombreFormateado = new StringBuilder();
        for (int i = 0; i < caracteres.length; i++){
            if (caracteres[i].equalsIgnoreCase(" ")){
                if((i + 1) < caracteres.length){
                    caracteres[i + 1] = caracteres[i + 1].toUpperCase();
                }
            }
        }
        for (String caracter : caracteres) {
            if (!caracter.equalsIgnoreCase(" ")) {
                nombreFormateado.append(caracter);
            }
        }
        return nombreFormateado.toString();
    }

    /**
     * Determina si el nombre introducido para la tabla tiene al menos 3 caracteres, en caso afirmativo, crea la tabla
     */
    private void onOK() {
        if(formatearCadena(txtNombreTabla.getText()).length() < 3) {
            Util.mostrarMensajeError("El nombre de la tabla debe contener al menos 3 carácteres.");
        }else{
            crearTabla(formatearCadena(txtNombreTabla.getText()));
            dispose();
        }
    }

    /**
     * Cancela la creacion de la tabla
     */
    private void onCancel() {
        dispose();
    }
}

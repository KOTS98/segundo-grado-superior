package com.apalacios.videojuegos;

import base.Videojuego;

import java.sql.*;

/**
 * Se encarga de realizar las principales funciones de la aplicacion
 *
 * @author Jose Adrian Palacios Romo
 */
class Modelo {
    static Connection conexion;

    /**
     * Estabelce una conexion con la base de datos basandose en los parametros de configuracion obtenidos del archivo
     * de configuracion
     *
     * @throws SQLException Excepcion de SQL
     */
    void conectarBBDD() throws SQLException {
        conexion = DriverManager.getConnection("jdbc:mysql://" + ConexionBBDD.host + ":" + ConexionBBDD.puerto
                + "/" + ConexionBBDD.nombre_BBDD, ConexionBBDD.user, ConexionBBDD.pass);
    }

    /**
     * Corta la conexion con la base de datos
     *
     * @throws SQLException Excepcion de SQL
     */
    void desconectarBBDD() throws SQLException {
        conexion.close();
        conexion = null;
    }

    /**
     * Solicita todos los datos de la tabla seleccionada en el archivo de configuracion y los devuelve en formato
     * ResultSet
     *
     * @return ResultSet que representa la respuesta de la consulta a la base de datos
     * @throws SQLException Excepcion de SQL
     */
    ResultSet obtenerDatos() throws SQLException {
        String consulta = "SELECT * FROM " + ConexionBBDD.tabla_BBDD;
        PreparedStatement sentencia;
        sentencia = conexion.prepareStatement(consulta);
        return sentencia.executeQuery();
    }

    /**
     * Determina si hay o no una conexion abierta con la base de datos
     *
     * @return boolean que representa si hay o no conexion con la base de datos (true = hay conexion, false de lo
     * contrario)
     */
    boolean comprobarConexion(){
        return conexion != null;
    }

    /**
     * Genera y ejecuta una sentencia de SQL con la que introduce un nuevo registro basado en los parametros del objeto
     * recibido
     *
     * @param videojuego Representa el videojuego que se desea insertar en la base de datos
     * @throws SQLException Excepcion de SQL
     */
    void insertarVideojuego(Videojuego videojuego) throws SQLException {
        PreparedStatement sentencia;
        String consulta = "INSERT INTO " + ConexionBBDD.tabla_BBDD + "(nombre, genero, fecha_lanzamiento, precio) " +
                "VALUES (?, ?, ?, ?)";
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, videojuego.getNombre());
        sentencia.setString(2, videojuego.getGenero());
        sentencia.setDate(3, Date.valueOf(videojuego.getFechaLanzamiento()));
        sentencia.setFloat(4, videojuego.getPrecio());
        sentencia.executeUpdate();
        sentencia.close();
    }

    /**
     * Genera y ejecuta una sentencia de SQL con la que modifica el registro con el id recibido, atualizando sus
     * valores a los del objeto recibido
     *
     * @param id Representa el id del registro a modificar
     * @param videojuego Representa el objeto del que se obtendran los datos para modificar el registro
     * @throws SQLException Excepcion de SQL
     */
    void modificarVideojuego(int id, Videojuego videojuego) throws SQLException {
        String consulta = "UPDATE " + ConexionBBDD.tabla_BBDD + " SET nombre=?, genero=?, fecha_lanzamiento=?, " +
                "precio=? WHERE id=?";
        PreparedStatement sentencia;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, videojuego.getNombre());
        sentencia.setString(2, videojuego.getGenero());
        sentencia.setDate(3, Date.valueOf(videojuego.getFechaLanzamiento()));
        sentencia.setFloat(4, videojuego.getPrecio());
        sentencia.setInt(5, id);
        sentencia.executeUpdate();
        sentencia.close();
    }

    /**
     * Genera y ejecuta una sentencia de SQL con la que elimina el registro con el id recibido
     *
     * @param id Representa el id del registro a eliminar
     * @throws SQLException Excepcion de SQL
     */
    void eliminarVideojuego(int id) throws SQLException {
        String consulta = "DELETE FROM " + ConexionBBDD.tabla_BBDD + " WHERE id =?";
        PreparedStatement sentencia;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, id);
        sentencia.executeUpdate();
        sentencia.close();
    }

    /**
     * Genera y ejecuta una consulta de SQL con la que devuelve la lista de registros cuyo nombre o genero (basandose
     * en el parametro recibido) coincidan con la busqueda realizada
     *
     * @param busqueda Representa el dato que se desea buscar
     * @param parametro Representa el valor con el que se esperan coincidencias
     * @return ResultSet que representa el resultado de la consulta de SQL
     * @throws SQLException Excepcion de SQL
     */
    ResultSet buscarVideojuegos(String busqueda, String parametro) throws SQLException {
        String consulta;
        if (parametro.equalsIgnoreCase("Nombre")){
            consulta = "SELECT * FROM " + ConexionBBDD.tabla_BBDD + " WHERE nombre = \"" + busqueda + "\"";
        }else{
            consulta = "SELECT * FROM " + ConexionBBDD.tabla_BBDD + " WHERE genero = \"" + busqueda + "\"";
        }

        PreparedStatement sentencia;
        sentencia = conexion.prepareStatement(consulta);
        return sentencia.executeQuery();
    }
}

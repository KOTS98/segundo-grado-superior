package com.apalacios.videojuegos;

/**
 * Clase principal del programa
 *
 * @author Jose Adrian Palacios Romo
 */
public class Main {

    /**
     * Crea un controlador con un modelo y una vista, y carga los datos de configuracion
     *
     * @param args Args
     */
    public static void main(String[] args) {
        new Controlador(new Modelo(), new Vista());
        ConexionBBDD.obtenerConfiguracion();
    }
}

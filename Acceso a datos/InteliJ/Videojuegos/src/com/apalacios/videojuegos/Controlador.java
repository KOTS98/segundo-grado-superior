package com.apalacios.videojuegos;

import base.Videojuego;
import util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Actua como puente entre el modelo y la vista, tambien gestiona los escuchadores de evento o (Listeners)
 *
 * @author Jose Adrian Palacios Romo
 */
public class Controlador implements ActionListener, TableModelListener, ListSelectionListener {
    private Modelo modelo;
    private Vista vista;
    private enum tipoEstado {conectado, desconectado}

    private tipoEstado estado;

    /**
     * Constructor de la clase, instancia la vista y el modelo y llama a los metodos para anyadir los Listeners
     *
     * @param modelo Representa al modelo (Se encarga de realizar todas las funciones de la aplicacion)
     * @param vista Representa la interfaz grafica de la aplicacion
     */
    Controlador(Modelo modelo, Vista vista){
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        addActionListener(this);
        addTableModelListener(this);
        addListSelectionListener(this);
    }

    /**
     * Formatea la cabecera de la tabla
     */
    private void iniciarTabla(){
        String[] headers = {"Id", "Nombre", "Género", "Fecha Lanzamiento", "Precio"};
        vista.dtmTabla.setColumnIdentifiers(headers);
    }

    /**
     * Vacia el contenido de la tabla y genera una fila nueva para cada dato recibido de resultSet
     *
     * @param resultSet Representa la espuesta obtenida de una consulta de SQL
     */
    private void cargarFilas(ResultSet resultSet){
        try{
            Object[] fila = new Object[5];
            vista.dtmTabla.setRowCount(0);
            while (resultSet.next()){
                fila[0] = resultSet.getObject(1);
                fila[1] = resultSet.getObject(2);
                fila[2] = resultSet.getObject(3);
                fila[3] = resultSet.getObject(4);
                fila[4] = resultSet.getObject(5);

                vista.dtmTabla.addRow(fila);
            }

        //Controla un posible fallo con la sentencia de SQL
        }catch(SQLException ex){
            Util.mostrarMensajeError("Error al cargar los datos desde la base de datos.");
        }
    }

    /**
     * Se encarga de dotar a todos los botones de la interfaz (vista) de ActionListeners
     *
     * @param listener Representa el escuchador de evento
     */
    private void addActionListener(ActionListener listener){
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemConf.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.btnAnyadir.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnBuscar.addActionListener(listener);
    }

    /**
     * Se encarga de dotar al DefaultTableModel de la tabla de un TableModelListener
     *
     * @param listener Representa el escuchador de evento
     */
    private void addTableModelListener(TableModelListener listener){
        vista.dtmTabla.addTableModelListener(listener);
    }

    /**
     * Se encarga de dotar al modelo de la tabla de un ListSelectionListener
     *
     * @param listener Representa el escuchador de evento
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.tblTabla.getSelectionModel().addListSelectionListener(listener);
    }

    /**
     * Determina cuando y que boton ha sido pulsado y actua en consecuencia
     *
     * @param e Evento de accion unico para cada elemento interactuable de la interfaz
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){

            //Botón "Añadir"
            case "anyadir":
                try {
                    if(modelo.comprobarConexion() && comporbarFormatoPrecio()){
                        if(comprobarCampos() && comporbarFormatoPrecio()){
                            modelo.insertarVideojuego(new Videojuego(vista.txtNombre.getText(), vista.txtGenero.getText(),
                                    vista.dtpFechaLanzamiento.getDate(), Float.parseFloat(vista.txtPrecio.getText())));
                            cargarFilas(modelo.obtenerDatos());
                        }else {
                            Util.mostrarMensajeInfo("Todos los campos deben de estar cubiertos antes de\n" +
                                    "añadir un registro.");
                        }
                    }
                } catch (SQLException ex) {
                    Util.mostrarMensajeError("Error al insertar registro en la base de datos.");
                }
                break;

            //Botón "Modificar"
            case "modificar":
                try {
                    if(modelo.comprobarConexion()){
                        if(comprobarCampos() && comporbarFormatoPrecio() && comprobarSeleccionTabla()){
                            modelo.modificarVideojuego((Integer) vista.dtmTabla.getValueAt(vista.tblTabla.getSelectedRow(), 0),
                                    new Videojuego(vista.txtNombre.getText(), vista.txtGenero.getText(),
                                            vista.dtpFechaLanzamiento.getDate(), Float.parseFloat(vista.txtPrecio.getText())));
                            cargarFilas(modelo.obtenerDatos());
                        }else {
                            Util.mostrarMensajeInfo("Todos los campos deben de estar cubiertos antes de\n" +
                                    "añadir un registro.");
                        }
                    }
                } catch (SQLException ex) {
                    Util.mostrarMensajeError("Error al modificar registro en la base de datos.");
                }
                break;

            //Botón "Eliminar"
            case "eliminar":
                try {
                    if(modelo.comprobarConexion() && comprobarSeleccionTabla()){
                        modelo.eliminarVideojuego((Integer) vista.dtmTabla.getValueAt(vista.tblTabla.getSelectedRow(), 0));
                        vista.dtmTabla.removeRow(vista.tblTabla.getSelectedRow());
                        limpiarCampos();
                    }
                } catch (SQLException ex) {
                    Util.mostrarMensajeError("Error el eliminar registro en la base de datos.");
                }
                break;

            //Botón "Buscar"
            case "buscar":
                vista.tblTabla.getSelectionModel().clearSelection();
                limpiarCampos();
                try {
                    if(modelo.comprobarConexion()){
                        if(vista.txtBusqueda.getText().isEmpty()){
                            cargarFilas(modelo.obtenerDatos());
                        }else{
                            cargarFilas(modelo.buscarVideojuegos(vista.txtBusqueda.getText(),
                                    (String) Objects.requireNonNull(vista.cbBusqueda.getSelectedItem())));
                        }
                    }
                } catch (SQLException ex) {
                    Util.mostrarMensajeError("Error al buscar en la base de datos.");
                }
                break;

            //Item de menú "Salir"
            case "salir":
                try {
                    if(modelo.comprobarConexion()){
                        salir();
                    }
                } catch (SQLException ex) {
                    Util.mostrarMensajeError("Error de conexión con la base de datos.");
                }
                break;

            //Item de menú "Conectar"
            case "conectar":
                try {
                    if(estado == tipoEstado.desconectado) {
                        modelo.conectarBBDD();
                        estado = tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());
                        vista.itemConectar.setText("Desconectar");
                    }else{
                        modelo.desconectarBBDD();
                        vista.itemConectar.setText("Conectar");
                        estado = tipoEstado.desconectado;
                        limpiarCampos();
                        limpiarTabla();
                    }
                } catch (SQLException ex) {
                    Util.mostrarMensajeError("Error de conexión con la base de datos." +
                            "\nCompruebe que la configuración es correcta.");
                }
                break;

            //Item de menú "Configuración"
            case "configuracion":
                new DialogConfiguracion();
                break;

            //Item de menú "Crear tabla"
            case "crearTabla":
                if(modelo.comprobarConexion()){
                    new CrearTablaDialog();
                }else{
                    Util.mostrarMensajeInfo("Es necesario estar conectado a la base de datos\n" +
                            "para crear una tabla.");
                }
                break;
        }
    }

    /**
     * Determina si hay algun campo sin rellenar
     *
     * @return boolean que representa el estado de los campos (true = si todos estan rellenos, false de lo contrario)
     */
    private boolean comprobarCampos(){
        return !vista.txtNombre.getText().equalsIgnoreCase("")
                && !vista.txtGenero.getText().equalsIgnoreCase("")
                && !vista.dtpFechaLanzamiento.getText().equalsIgnoreCase("")
                && !vista.txtPrecio.getText().equalsIgnoreCase("");
    }

    /**
     * Determina si el dato insertado en el campo de Precio es o no un numero (entero o flotante)
     *
     * @return boolean que representa el resultado de la comprobacion (true = si es un numero, false de lo contrario)
     */
    private boolean comporbarFormatoPrecio(){
        try{
            Float.valueOf(vista.txtPrecio.getText());
            return true;
        }catch (NumberFormatException e){
            Util.mostrarMensajeInfo("El campo \"Precio\" solo puede contener números enteros\n" +
                    "o decimales.");
            return false;
        }
    }

    /**
     * Reinicia todos los campos de la interfaz
     */
    private void limpiarCampos(){
        vista.txtNombre.setText("");
        vista.txtGenero.setText("");
        vista.dtpFechaLanzamiento.clear();
        vista.txtPrecio.setText("");
    }

    /**
     * Determina si hay alguna fila de la tabla seleccionada
     *
     * @return boolean que representa el estado de la comprobacion (true = hay algo seleccionado, false de lo contrario)
     */
    private boolean comprobarSeleccionTabla(){
        return !vista.tblTabla.getSelectionModel().isSelectionEmpty();
    }

    /**
     * Establece en 0 el numero de filas de la tabla
     */
    private void limpiarTabla(){
        vista.dtmTabla.setRowCount(0);
    }

    /**
     * Cierra la conexion con la base de datos, limpia los campos y la tabla, y cierra la aplicacion
     *
     * @throws SQLException Excepcion de SQL
     */
    private void salir() throws SQLException {
        modelo.desconectarBBDD();
        limpiarCampos();
        limpiarTabla();
        System.exit(0);
    }

    /**
     * Determina cuando ha la tabla y la actualiza
     *
     * @param e Escuchador de evento de la tabla
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
            try {
                cargarFilas(modelo.obtenerDatos());
            } catch (SQLException ex) {
                Util.mostrarMensajeError("Error al actualizar la tabla. Compruebe la compatiblidad de la base" +
                        "de datos con el programa.");
            }
        }
    }

    /**
     * Determina cuando ha cambiado la seleccion en la tabla y actualiza los campos con los contenidos de la fila
     * seleccionada
     *
     * @param e Escuchador de evento de la lista de la tabla
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        try {
            vista.txtNombre.setText((String) vista.dtmTabla.getValueAt(vista.tblTabla.getSelectedRow(), 1));
            vista.txtGenero.setText((String) vista.dtmTabla.getValueAt(vista.tblTabla.getSelectedRow(), 2));
            Date fecha_lanzamiento = (Date) vista.dtmTabla.getValueAt(vista.tblTabla.getSelectedRow(), 3);
            vista.dtpFechaLanzamiento.setDate(fecha_lanzamiento.toLocalDate());
            vista.txtPrecio.setText(String.valueOf(vista.dtmTabla.getValueAt(vista.tblTabla.getSelectedRow(), 4)));
        }catch (ArrayIndexOutOfBoundsException ignored){}
    }
}
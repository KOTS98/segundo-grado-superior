package com.apalacios.videojuegos;

import util.Util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Clase que contiene una serie de variables estaticas que representan los parametros para la conexion con la
 * base de datos
 *
 * @author Jose Adrian Palacios Romo
 */
class ConexionBBDD {
    static String host = "";
    static String puerto = "";
    static String nombre_BBDD = "";
    static String tabla_BBDD = "";
    static String user = "";
    static String pass = "";

    private static Properties conf = new Properties();

    /**
     * Lee el archivo, obtiene los parametros de configuracion y los carga a las variables estaticas
     */
    static void obtenerConfiguracion(){
        try {
            FileReader fr = new FileReader("Properties.conf");
            conf.load(fr);

            host = conf.getProperty("host");
            puerto = conf.getProperty("puerto");
            nombre_BBDD = conf.getProperty("nombre_BBDD");
            tabla_BBDD = conf.getProperty("tabla_BBDD");
            user = conf.getProperty("user");
            pass = conf.getProperty("pass");

            fr.close();

        //Controla la posibilidad de que el archivo de configuracion no exista
        } catch (FileNotFoundException e) {
            Util.mostrarMensajeError("Archivo de configuración no encontrado." +
                    "\nReiniciar la aplicación creará uno nuevo.");

        //Controla la posibilidad de un fallo en la lectura del archivo
        } catch (IOException e) {
            Util.mostrarMensajeError("Error al leer el archivo de configuración.");
        }
    }
}

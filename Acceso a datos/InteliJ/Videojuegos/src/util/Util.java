package util;

import javax.swing.*;

/**
 * Clase que contiene metodos que pueden ser necesitados desde cualquier otra clase.
 *
 * @author Jose Adrian Palacios Romo
 */
public class Util {

    /**
     * Muestra el mensaje de error deseado en una ventana emergente.
     *
     * @param mensaje Representa el texto deseado para el mensaje de error.
     */
    public static void mostrarMensajeError(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Muestra el mensaje de informacion deseado en una ventana emergente.
     *
     * @param mensaje Representa el texto deseado para el mensaje de informacion.
     */
    public static void mostrarMensajeInfo(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje, "Información", JOptionPane.INFORMATION_MESSAGE);
    }
}

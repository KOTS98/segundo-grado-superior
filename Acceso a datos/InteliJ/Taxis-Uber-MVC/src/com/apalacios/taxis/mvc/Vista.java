package com.apalacios.taxis.mvc;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JTabbedPane tabbedPane1;
    private JPanel panelPrincipal;
    private JFrame frame;
    JTextField txtMarca;
    JTextField txtModelo;
    JTextField txtMatricula;
    JSlider sliderPlazas;
    JTable tableTaxis;
    JButton btnInsertar;
    JButton btnEliminar;
    DefaultTableModel dtmTaxis;
    JMenuItem itemSalir;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        crearMenu();
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        inicializarTabla();
    }

    private  void inicializarTabla(){
        dtmTaxis = new DefaultTableModel();
        tableTaxis.setModel(dtmTaxis);
        Object[] cabeceras = {"id", "marca", "modelo", "matricula", "n_plazas"};
        dtmTaxis.setColumnIdentifiers(cabeceras);
    }

    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("itemSalir");
        menu.add(itemSalir);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }
}

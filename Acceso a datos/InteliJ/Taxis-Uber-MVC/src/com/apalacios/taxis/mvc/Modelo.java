package com.apalacios.taxis.mvc;

import java.sql.*;

public class Modelo {
    private Connection conexion;

    public void connectar(){
        try{
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/compania_taxis",
                    "root", "");
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error de SQL");
        }
    }

    public void desconectar(){
        try{
            conexion.close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            System.out.println("Error de SQL");
        }
    }

    public void insertarTaxi(String marca, String modelo, String matricula, int nPalzas){
        String consulta = "INSERT INTO taxis (marca, modelo, matricula, n_plazas) VALUES(?, ?, ?, ?)";
        try {
            PreparedStatement stmt = conexion.prepareStatement(consulta);
            stmt.setString(1, marca);
            stmt.setString(2, modelo);
            stmt.setString(3, matricula);
            stmt.setInt(4, nPalzas);

            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error de SQL");
        }
    }

    public void eliminarTaxi(int id){
        String consulta = "DELETE FROM taxis WHERE id = ?";
        try {
            PreparedStatement stmt = conexion.prepareStatement(consulta);
            stmt.setInt(1, id);

            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error de SQL");
        }
    }

    public void modificarTaxi(int idMod, String marca, String modelo, String matricula, int nPalzas){
        String consulta = "UPDATE FROM taxis SET marca = ?, modelo = ?, matricula = ?, nPlazas = ? WHERE idMod = ?";
        try {
            PreparedStatement stmt = conexion.prepareStatement(consulta);
            stmt.setString(1, marca);
            stmt.setString(2, modelo);
            stmt.setString(3, matricula);
            stmt.setInt(4, nPalzas);
            stmt.setInt(5, idMod);

            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error de SQL");
        }

    }

    public ResultSet obtenerTaxisResultSet(){
        String consulta = "SELECT * FROM taxis";
        ResultSet resultado = null;
        try{
            PreparedStatement stmt = conexion.prepareStatement(consulta);
            resultado = stmt.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error de SQL");
        }
        return resultado;
    }
}

package com.apalacios.taxis.mvc;

import com.apalacios.taxis.mvc.Modelo;
import com.apalacios.taxis.mvc.Vista;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Controlador implements ActionListener {
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;

        addActionListener(this);
        modelo.connectar();
        listarTaxis();
    }

    private void addActionListener(ActionListener listener){
        vista.btnEliminar.addActionListener(listener);
        vista.btnInsertar.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch(comando){
            case "Insertar":{
                String marca = vista.txtMarca.getText();
                String modeloTaxi = vista.txtModelo.getText();
                String matricula = vista.txtMatricula.getText();
                int nPlazas = vista.sliderPlazas.getValue();
                modelo.insertarTaxi(marca, modeloTaxi, matricula, nPlazas);
                listarTaxis();
            }
            break;
            case "Modificar":{
               // modelo.modificarTaxi();
            }
            case "Eliminar":{
               // modelo.eliminarTaxi();
            }
            break;
            case "Listar":{
               listarTaxis();
            }
            case "itemSalir":{
                System.exit(0);
            }
            break;
        }
    }

    private void listarTaxis() {
        ResultSet resultado = modelo.obtenerTaxisResultSet();

        //Elimino los registros anteriores de la tabla
        vista.dtmTaxis.setRowCount(0);
        try{
            while(resultado.next()){

                //Obtengo los valores de las columnas del conjunto de registros(ResultSet)
                int id = resultado.getInt(1);
                String marca = resultado.getString(2);
                String modelo = resultado.getString(3);
                String matricula = resultado.getString(4);
                int nPlazas = resultado.getInt(5);

                //Añado al dtm un Array con los valores de una fila
                vista.dtmTaxis.addRow(new Object[]{id, marca, modelo, matricula, nPlazas});
            }
        }catch (SQLException e){
            e.printStackTrace();
            System.out.println("ERROR DE SQL");
        }
    }
}

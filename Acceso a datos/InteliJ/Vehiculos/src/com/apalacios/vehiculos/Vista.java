package com.apalacios.vehiculos;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class Vista {
    private JTextField txtMatricula;
    private JTextField txtMarca;
    private JTextField txtModelo;
    private JButton btnAnyadir;
    private JButton btnEliminar;
    private JButton btnBuscar;
    private DateTimePicker dtpFechaMatriculacion;
    private JTextField txtBusqueda;
    private JPanel panelPrincipal;
    private JTable tblTabla;

    JLabel lblAction;
    DefaultTableModel dtmTabla;
    JMenuItem itemConectar;
    JMenuItem itemSalir;
    JFrame frame;

    public Vista(){
        JFrame frame = new JFrame("Vehiculos BBDD");
        frame.setContentPane(panelPrincipal);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        dtmTabla = new DefaultTableModel();
        tblTabla.setModel(dtmTabla);
        crearMenu();

        frame.pack();
        frame.setVisible(true);
    }

    private void crearMenu(){
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }
}

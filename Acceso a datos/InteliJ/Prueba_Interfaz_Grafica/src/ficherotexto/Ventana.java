package ficherotexto;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Ventana {
    private JTextField txtEntrada;
    private JPanel panel1;
    private JRadioButton rbtnGuardar;
    private JRadioButton rbtnCargar;
    private JButton btnEjecutar;
    private JTextArea txtSalida;

    public Ventana() {
        File fichero = new File("fichero.txt");
        btnEjecutar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(rbtnGuardar.isSelected()){
                    FileWriter fw;
                    {
                        try {
                            BufferedWriter bw = new BufferedWriter(new FileWriter("fichero.txt", true));
                            bw.write(txtEntrada.getText().toString());
                            bw.newLine();
                            bw.close();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    }
                }
                else{
                    try {
                        String contenido;
                        BufferedReader br = new BufferedReader(new FileReader("fichero.txt"));
                        while((contenido = br.readLine())!= null) {
                            txtSalida.append(contenido+"\n");
                        }
                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    }catch (IOException ex){
                        ex.printStackTrace();
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(new Ventana().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

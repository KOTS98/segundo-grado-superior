package com.elenajif.vehiculosHibernateElena.gui;

import com.elenajif.vehiculosHibernateElena.Coche;
import com.elenajif.vehiculosHibernateElena.Propietario;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {
    SessionFactory sessionFactory;

    public void conectar() {
        Configuration configuracion = new Configuration();

        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Coche.class);
        configuracion.addAnnotatedClass(Propietario.class);

        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    public void desconectar() {

        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    public ArrayList<Coche> getCoches() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Coche");
        ArrayList<Coche> listaCoches = (ArrayList<Coche>) query.getResultList();
        sesion.close();
        return listaCoches;
    }

    public ArrayList<Propietario> getPropietarios() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Propietario ");
        ArrayList<Propietario> listaPropietarios = (ArrayList<Propietario>) query.getResultList();
        sesion.close();
        return listaPropietarios;
    }

    public ArrayList<Coche> getCochesPropietario(Propietario propietarioSelec) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Coche WHERE Propietario=:prop");
        query.setParameter("prop", propietarioSelec);
        ArrayList<Coche> listaCoches = (ArrayList<Coche>) query.getResultList();
        sesion.close();
        return listaCoches;
    }

    public void altaCoche(Coche nuevoCoche) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCoche);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public void modificarCoche(Coche cocheSelec) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(cocheSelec);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public void eliminarCoche(Coche cocheSelec) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.delete(cocheSelec);
        sesion.getTransaction().commit();

        sesion.close();
    }
}

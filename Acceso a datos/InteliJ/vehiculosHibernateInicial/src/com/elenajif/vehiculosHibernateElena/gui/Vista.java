package com.elenajif.vehiculosHibernateElena.gui;

import com.elenajif.vehiculosHibernateElena.Coche;
import com.elenajif.vehiculosHibernateElena.Propietario;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;

public class Vista extends JFrame{
    private JPanel panelPrincipal;

    JTextField txtID;
    JTextField txtMatricula;
    JTextField txtMarca;
    JTextField txtModelo;
    DateTimePicker txtFechaMatricula;
    JTextField txtPropietario;

    JButton btnListarPropietarios;
    JButton btnListarVehiculos;
    JButton btnAdd;
    JButton btnMod;
    JButton btnDel;

    JList<Coche> listVehiculos;
    JList<Propietario> listPropietarios;
    JList<Coche> listCochesPropietario;

    DefaultListModel<Coche> dlm;
    DefaultListModel<Propietario> dlmPropietarios;
    DefaultListModel<Coche> dlmCochesPropietario;

    JMenuItem conexionItem;
    JMenuItem salirItem;

    public Vista() {
        setContentPane(panelPrincipal);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Vehículos - Adrián Palacios");
        setResizable(false);
        setLocationRelativeTo(null);
        pack();
        setVisible(true);

        crearMenu();
        crearModelos();
    }

    private void crearModelos() {
        dlm = new DefaultListModel<>();
        listVehiculos.setModel(dlm);
        dlmPropietarios = new DefaultListModel<>();
        listPropietarios.setModel(dlmPropietarios);
        dlmCochesPropietario = new DefaultListModel<>();
        listCochesPropietario.setModel(dlmCochesPropietario);
    }

    private void crearMenu() {
        JMenuBar barraMenu = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barraMenu.add(menu);
        setJMenuBar(barraMenu);
    }
}

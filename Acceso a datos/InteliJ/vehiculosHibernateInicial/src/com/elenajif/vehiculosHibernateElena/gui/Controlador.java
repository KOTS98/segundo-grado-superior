package com.elenajif.vehiculosHibernateElena.gui;

import com.elenajif.vehiculosHibernateElena.Coche;
import com.elenajif.vehiculosHibernateElena.Propietario;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.ArrayList;

public class Controlador implements ActionListener, ListSelectionListener {
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    private void addActionListeners(ActionListener listener){
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.btnAdd.addActionListener(listener);
        vista.btnDel.addActionListener(listener);
        vista.btnMod.addActionListener(listener);
        vista.btnListarVehiculos.addActionListener(listener);
        vista.btnListarPropietarios
                .addActionListener(listener);
    }

    private void addListSelectionListener(ListSelectionListener listener){
        vista.listVehiculos.addListSelectionListener(listener);
        vista.listPropietarios.addListSelectionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;

            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "AltaCoche":
                modelo.altaCoche(new Coche(vista.txtMatricula.getText(), vista.txtMarca.getText(),
                        vista.txtModelo.getText(),
                        Timestamp.valueOf(vista.txtFechaMatricula.getDateTimePermissive())));
                break;

            case "ModificarCoche":
                modelo.modificarCoche(new Coche(vista.txtMatricula.getText(), vista.txtMarca.getText(),
                        vista.txtModelo.getText(),
                        Timestamp.valueOf(vista.txtFechaMatricula.getDateTimePermissive()),
                        vista.listPropietarios.getSelectedValue()));
                break;

            case "EliminarCoche":
                modelo.eliminarCoche(vista.listVehiculos.getSelectedValue());
                break;

            case "ListarCoches":
                listarCoches(modelo.getCoches());
                break;

            case "ListarPropietarios":
                listarPropietarios(modelo.getPropietarios());
                break;
        }
        listarCoches(modelo.getCoches());
    }

    private void listarPropietarios(ArrayList<Propietario> propietarios) {
        vista.dlmPropietarios.clear();
        for (Propietario propietario : propietarios) {
            vista.dlmPropietarios.addElement(propietario);
        }
    }

    public void listarCoches(ArrayList<Coche> coches) {
        vista.dlm.clear();
        for (Coche coche : coches) {
            vista.dlm.addElement(coche);
        }
    }

    public void listarCochesPropietario(ArrayList<Coche> cochesPropietario) {
        vista.dlmCochesPropietario.clear();
        for (Coche coche : cochesPropietario) {
            vista.dlmCochesPropietario.addElement(coche);
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}

package com.apalacios.radiobuttons;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RadioGroup grupo = findViewById(R.id.rbtngColors);
        grupo.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        TextView etiqueta = findViewById(R.id.lblSelection);
        switch(checkedId){
            case R.id.rbtnNegro:
                etiqueta.setText("Color negro");
                break;
            case R.id.rbtnAzul:
                etiqueta.setText("Color azul");
                break;
            case R.id.rbtnRosa:
                etiqueta.setText("Color rosa");
                break;
        }
    }
}

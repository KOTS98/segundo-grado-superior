package com.apalacios.calculadora2activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

public class ListaCompra extends AppCompatActivity {
    Button btnAnyadirCompra;
    Button btnQuitarCompra;
    ListView listaCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_compra);
        btnAnyadirCompra = findViewById(R.id.btnAnyadirCompra);
        btnQuitarCompra = findViewById(R.id.btnQuitarCompra);
        listaCompra = findViewById(R.id.listaCompra);
    }
}

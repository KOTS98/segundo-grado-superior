package com.apalacios.calculadora2activitys;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Calculadora extends AppCompatActivity implements View.OnClickListener{

    Button btn0;
    Button btn1;
    Button btn2;
    Button btn3;
    Button btn4;
    Button btn5;
    Button btn6;
    Button btn7;
    Button btn8;
    Button btn9;
    Button btnSum;
    Button btnRes;
    Button btnMul;
    Button btnDiv;
    Button btnIgual;
    Button btnCE;
    Button btnDel;
    TextView txtResultado;
    Boolean operando = false;
    String num1 = "", num2 = "";
    int nOperacion = 0; //0 = nada, 1 = suma, 2 = resta, 3 = mult, 4 = div.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);
        btn0 = findViewById(R.id.btn0);
        btn0.setOnClickListener(this);
        btn1 = findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        btn2 = findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
        btn3 = findViewById(R.id.btn3);
        btn3.setOnClickListener(this);
        btn4 = findViewById(R.id.btn4);
        btn4.setOnClickListener(this);
        btn5 = findViewById(R.id.btn5);
        btn5.setOnClickListener(this);
        btn6 = findViewById(R.id.btn6);
        btn6.setOnClickListener(this);
        btn7 = findViewById(R.id.btn7);
        btn7.setOnClickListener(this);
        btn8 = findViewById(R.id.btn8);
        btn8.setOnClickListener(this);
        btn9 = findViewById(R.id.btn9);
        btn9.setOnClickListener(this);
        btnSum = findViewById(R.id.btnSum);
        btnSum.setOnClickListener(this);
        btnRes = findViewById(R.id.btnRes);
        btnRes.setOnClickListener(this);
        btnMul = findViewById(R.id.btnMul);
        btnMul.setOnClickListener(this);
        btnDiv = findViewById(R.id.btnDiv);
        btnDiv.setOnClickListener(this);
        btnIgual = findViewById(R.id.btnIgual);
        btnIgual.setOnClickListener(this);
        btnCE = findViewById(R.id.btnCE);
        btnCE.setOnClickListener(this);
        btnDel = findViewById(R.id.btnDel);
        btnDel.setOnClickListener(this);
        txtResultado = findViewById(R.id.txtResultado);
    }

    @Override
    public void onClick(View v) {
        String contenido = txtResultado.getText().toString();
        switch (v.getId()){
            case R.id.btn0:
                if(contenido.equalsIgnoreCase("0")){
                    num1 += "0";
                }else{
                    if (operando == false){
                        num1 += "0";
                    }else{
                        num2 += "0";
                    }
                    txtResultado.append("0");
                }
                break;
            case R.id.btn1:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("1");
                    num1 += "1";
                }else{
                    if (operando == false){
                        num1 += "1";
                    }else{
                        num2 += "1";
                    }
                    txtResultado.append("1");
                }
                break;
            case R.id.btn2:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("2");
                    num1 += "2";
                }else{
                    if (operando == false){
                        num1 += "2";
                    }else{
                        num2 += "2";
                    }
                    txtResultado.append("2");
                }
                break;
            case R.id.btn3:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("3");
                    num1 += "3";
                }else{
                    if (operando == false){
                        num1 += "3";
                    }else{
                        num2 += "3";
                    }
                    txtResultado.append("3");
                }
                break;
            case R.id.btn4:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("4");
                    num1 += "4";
                }else{
                    if (operando == false){
                        num1 += "4";
                    }else{
                        num2 += "4";
                    }
                    txtResultado.append("4");
                }
                break;
            case R.id.btn5:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("5");
                    num1 += "5";
                }else{
                    if (operando == false){
                        num1 += "5";
                    }else{
                        num2 += "5";
                    }
                    txtResultado.append("5");
                }
                break;
            case R.id.btn6:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("6");
                    num1 += "6";
                }else{
                    if (operando == false){
                        num1 += "6";
                    }else{
                        num2 += "6";
                    }
                    txtResultado.append("6");
                }
                break;
            case R.id.btn7:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("7");
                    num1 += "7";
                }else{
                    if (operando == false){
                        num1 += "7";
                    }else{
                        num2 += "7";
                    }
                    txtResultado.append("7");
                }
                break;
            case R.id.btn8:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("8");
                    num1 += "8";
                }else{
                    if (operando == false){
                        num1 += "8";
                    }else{
                        num2 += "8";
                    }
                    txtResultado.append("8");
                }
                break;
            case R.id.btn9:
                if(contenido.equalsIgnoreCase("0")){
                    txtResultado.setText("9");
                    num1 += "9";
                }else{
                    if (operando == false){
                        num1 += "9";
                    }else{
                        num2 += "9";
                    }
                    txtResultado.append("9");
                }
                break;
            case R.id.btnSum:
                if(operando == false){
                    txtResultado.append("+");
                    operando = true;

                    nOperacion = 1;
                }
                break;
            case R.id.btnRes:
                if(operando == false){
                    txtResultado.append("-");
                    operando = true;
                    nOperacion = 2;
                }
                break;
            case R.id.btnMul:
                if(operando == false){
                    txtResultado.append("x");
                    operando = true;
                    nOperacion = 3;
                }
                break;
            case R.id.btnDiv:
                if(operando == false){
                    txtResultado.append("/");
                    operando = true;
                    nOperacion = 4;
                }
                break;
            case R.id.btnIgual:
                int num1I = 0;
                if (num1 != ""){
                    num1I = Integer.parseInt(num1);
                }
                int num2I = 0;
                if(num2 != ""){
                    num2I = Integer.parseInt(num2);
                }
                int resultadoI;
                String resultado;
                switch(nOperacion){
                    case 1:
                        resultadoI = num1I + num2I;
                        resultado = String.valueOf(resultadoI);
                        break;
                    case 2:
                        resultadoI = num1I - num2I;
                        resultado = String.valueOf(resultadoI);
                        break;
                    case 3:
                        resultadoI = num1I * num2I;
                        resultado = String.valueOf(resultadoI);
                        break;
                    case 4:
                        if(num1I == 0 || num2I <= 0){
                            resultado = "Syntax error";
                        }else{
                            resultadoI = num1I / num2I;
                            resultado = String.valueOf(resultadoI);
                        }
                        break;
                    default:
                        resultado = "";
                        operando = false;
                        break;
                }
                txtResultado.setText(resultado);
                break;
            case R.id.btnCE:
                txtResultado.setText("0");
                operando = false;
                num1 = "";
                num2 = "";
                break;
            case R.id.btnDel:
                break;
        }
    }
}

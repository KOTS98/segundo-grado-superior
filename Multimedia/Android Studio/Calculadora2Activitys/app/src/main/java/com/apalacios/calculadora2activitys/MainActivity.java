package com.apalacios.calculadora2activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    Button btnCalculadora;
    Button btnListaCompra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnCalculadora = findViewById(R.id.btnCalculadora);
        btnCalculadora.setOnClickListener(this);
        btnListaCompra = findViewById(R.id.btnCompra);
        btnListaCompra.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnCalculadora:
                Intent calculadora = new Intent(this, Calculadora.class);
                startActivity(calculadora);
                break;
            case R.id.btnCompra:
                Intent compra = new Intent(this, ListaCompra.class);
                startActivity(compra);
                break;
        }
    }
}

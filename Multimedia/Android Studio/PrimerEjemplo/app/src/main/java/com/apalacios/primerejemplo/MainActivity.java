package com.apalacios.primerejemplo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button botonCopy;
    Button botonCalc;
    EditText textoRellenar;
    TextView textoCopiar;
    EditText numeroCalcular;
    TextView numeroResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botonCopy = findViewById(R.id.btnCopiar);
        botonCopy.setOnClickListener(this);
        textoRellenar=findViewById(R.id.txtRellenar);
        textoCopiar=findViewById(R.id.lblCopiado);

        botonCalc = findViewById(R.id.btnCalcular);
        botonCalc.setOnClickListener(this);
        numeroCalcular = findViewById(R.id.txtNumCalc);
        numeroResultado = findViewById(R.id.lblNumResultado);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnCopiar:
                String texto = textoRellenar.getText().toString();
                textoCopiar.setText(texto);
                break;
            case R.id.btnCalcular:
                for(int i =Integer.parseInt(numeroCalcular.getText().toString()); i>0; i--){
                    Integer.parseInt(numeroResultado.getText().toString());
                }
                break;
        }
    }
}

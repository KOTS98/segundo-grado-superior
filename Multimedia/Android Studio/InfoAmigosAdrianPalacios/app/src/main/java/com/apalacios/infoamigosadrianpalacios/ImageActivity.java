package com.apalacios.infoamigosadrianpalacios;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class ImageActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView iv1, iv2, iv3, iv4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        iv1 = findViewById(R.id.iv_seleccion1);
        iv2 = findViewById(R.id.iv_seleccion2);
        iv3 = findViewById(R.id.iv_seleccion3);
        iv4 = findViewById(R.id.iv_seleccion4);

        iv1.setImageDrawable(getResources().getDrawable(R.drawable.icono));
        iv2.setImageDrawable(getResources().getDrawable(R.drawable.icono));
        iv3.setImageDrawable(getResources().getDrawable(R.drawable.icono));
        iv4.setImageDrawable(getResources().getDrawable(R.drawable.icono));

        iv1.setOnClickListener(this);
        iv2.setOnClickListener(this);
        iv3.setOnClickListener(this);
        iv4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        Intent intent = getIntent();
        int seleccion = 1;
        switch (v.getId()){
            case R.id.iv_seleccion1:
                seleccion = 1;
                break;
            case R.id.iv_seleccion2:
                seleccion = 2;
                break;
            case R.id.iv_seleccion3:
                seleccion = 3;
                break;
            case R.id.iv_seleccion4:
                seleccion = 4;
                break;
        }
        intent.putExtra("SELECCION", seleccion);
        this.setResult(RESULT_OK, intent);
        finish();
    }
}

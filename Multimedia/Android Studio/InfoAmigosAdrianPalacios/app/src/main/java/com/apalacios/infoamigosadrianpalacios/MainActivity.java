package com.apalacios.infoamigosadrianpalacios;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.apalacios.infoamigosadrianpalacios.db.Database;
import com.apalacios.infoamigosadrianpalacios.util.Util;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    private AmigoAdapter adaptador;
    private ArrayList <Amigo> listaAmigos;
    private Amigo amigoSeleccionado;
    private Database db;
    private ListView lvLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new Database(this);
        listaAmigos = db.getAmigos();
        lvLista = findViewById(R.id.lvLista);
        adaptador = new AmigoAdapter(this, listaAmigos);
        lvLista.setAdapter(adaptador);
        lvLista.setOnItemClickListener(this);

        Button btnAnyadir = findViewById(R.id.btnAnyadir);
        btnAnyadir.setOnClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView seleccion = findViewById(R.id.lblSeleccion);
        amigoSeleccionado = listaAmigos.get(position);
        seleccion.setText(amigoSeleccionado.getNombre());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        listaAmigos = db.getAmigos();
        adaptador = new AmigoAdapter(this, listaAmigos);
        lvLista.setAdapter(adaptador);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.strPreferencias:
                break;
            case R.id.strAbout:
                Toast.makeText(this, "Aplicación desarrollada por KOTS98", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnAnyadir:
                Intent intent = new Intent(this, AddActivity.class);
                startActivity(intent);
                break;
        }
    }
}

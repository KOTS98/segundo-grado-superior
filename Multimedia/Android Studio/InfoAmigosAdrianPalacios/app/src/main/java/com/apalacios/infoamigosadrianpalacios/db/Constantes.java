package com.apalacios.infoamigosadrianpalacios.db;

public class Constantes {
    public static final String BASE_DATOS="amigos.db";
    public static final String TABLA_AMIGOS="amigos";
    public static final String NOMBRE="nombre";
    public static final String EMAIL="email";
    public static final String TLF="telefono";
    public static final String DEUDAS="deudas";
    public static final String FOTO="foto";
}

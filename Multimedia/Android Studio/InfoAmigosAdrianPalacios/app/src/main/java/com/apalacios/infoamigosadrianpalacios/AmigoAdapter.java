package com.apalacios.infoamigosadrianpalacios;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AmigoAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<Amigo> listaAmigos;
    private LayoutInflater inflater;

    public AmigoAdapter(Activity context, ArrayList<Amigo> listaAmigos){
        this.context = context;
        this.listaAmigos = listaAmigos;
        inflater = LayoutInflater.from(context);
    }

    static class ViewHolder{
        ImageView foto;
        TextView txtNombre;
        TextView txtTelefono;
        TextView txtEmail;
    }

    @Override
    public int getCount() {
        return listaAmigos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaAmigos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.fila, null);
            holder = new ViewHolder();
            holder.foto = convertView.findViewById(R.id.ivFoto);
            holder.txtNombre = convertView.findViewById(R.id.txtNombre);
            holder.txtTelefono = convertView.findViewById(R.id.txtTelefono);
            holder.txtEmail = convertView.findViewById(R.id.txtEmail);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        Amigo amigo = listaAmigos.get(position);
        holder.foto.setImageBitmap(amigo.getFoto());
        holder.txtNombre.setText(amigo.getNombre());
        holder.txtTelefono.setText(amigo.getTelefono());
        holder.txtEmail.setText(amigo.getEmail());
        return convertView;
    }
}

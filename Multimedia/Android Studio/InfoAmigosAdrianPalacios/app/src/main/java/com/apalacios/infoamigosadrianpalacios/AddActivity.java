package com.apalacios.infoamigosadrianpalacios;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.apalacios.infoamigosadrianpalacios.db.Database;

public class AddActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView foto;
    Database db;
    private int RESULTADO_CARGA_IMAGEN = 42;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        db = new Database(this);
        foto = findViewById(R.id.iv_seleccion);
        foto.setImageDrawable(getResources().getDrawable(R.drawable.icono));
        foto.setOnClickListener(this);
        Button btnInsertar = findViewById(R.id.btnAceptar);
        btnInsertar.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    if(requestCode == RESULTADO_CARGA_IMAGEN && resultCode == RESULT_OK && data != null){

    }
        int res = data.getExtras().getInt("SELECTION");
        switch(res){
            case 1:
                foto.setImageDrawable(getResources().getDrawable(R.drawable.icono));
                break;
            case 2:
                foto.setImageDrawable(getResources().getDrawable(R.drawable.icono));
                break;
            case 3:
                foto.setImageDrawable(getResources().getDrawable(R.drawable.icono));
                break;
            case 4:
                foto.setImageDrawable(getResources().getDrawable(R.drawable.icono));
                break;
        }
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.iv_seleccion:
                Intent  intent = new Intent(this, ImageActivity.class);
                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN);
                break;
            case R.id.btnAceptar:
                Amigo amigo = new Amigo();
                EditText txt = findViewById(R.id.txtNombre);
                amigo.setNombre(txt.getText().toString());
                txt = findViewById(R.id.txtEmail);
                amigo.setEmail(txt.getText().toString());
                txt = findViewById(R.id.txtTelefono);
                amigo.setTelefono(txt.getText().toString());
                txt = findViewById(R.id.txtDeuda);
                amigo.setDeuda(Float.valueOf(txt.getText().toString()));
                amigo.setFoto(((BitmapDrawable) foto.getDrawable()).getBitmap());
                db.nuevoAmigo(amigo);
                break;
        }
    }
}

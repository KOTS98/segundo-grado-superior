package com.apalacios.infoamigosadrianpalacios;

import android.graphics.Bitmap;

public class Amigo {
    private long id;
    private String nombre;
    private String email;
    private String telefono;
    private float deuda;
    private Bitmap foto;

    public Amigo(){

    }

    public Amigo(String nombre){
        this.nombre = nombre;
    }

    public long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefono() {
        return telefono;
    }

    public float getDeuda() {
        return deuda;
    }

    public Bitmap getFoto() {
        return foto;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDeuda(float deuda) {
        this.deuda = deuda;
    }

    public void setFoto(Bitmap foto) {
        this.foto = foto;
    }
}

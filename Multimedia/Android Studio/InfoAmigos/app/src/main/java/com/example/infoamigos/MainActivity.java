package com.example.infoamigos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.infoamigos.db.Database;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
View.OnClickListener, AdapterView.OnItemLongClickListener{

    private AmigoAdapter adaptador;
    private ArrayList<Amigo> listaAmigos;
    private Amigo amigoSeleccionado;
    private Database db;
    private ListView lvLista;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        db = new Database(this);

        listaAmigos = new ArrayList<Amigo>();

        if(prefs.getBoolean("check_box_preference_1", false) == false){
            listaAmigos = db.getAmigos();
        }else{
            listaAmigos = db.getAmigosD();
        }
         lvLista=findViewById(R.id.lv_lista);
        adaptador=new AmigoAdapter(this,listaAmigos);
        lvLista.setAdapter(adaptador);
        lvLista.setOnItemClickListener(this);
        lvLista.setOnItemLongClickListener(this);
        registerForContextMenu(lvLista);

        Button botonanyadir=findViewById(R.id.btn_anyadir);
        botonanyadir.setOnClickListener(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        listaAmigos=db.getAmigos();
        adaptador=new AmigoAdapter(this, listaAmigos);
        lvLista.setAdapter(adaptador);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.list_menu, menu);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        TextView seleccion= findViewById(R.id.lbl_seleccion);
        amigoSeleccionado=listaAmigos.get(position);
        seleccion.setText(amigoSeleccionado.getNombre());

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_preferencias:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;

            case R.id.menu_acercade:
                Toast.makeText(this, "Aplicación desarrollada por 2DAM", Toast.LENGTH_LONG).show();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_detalles:
                break;
            case R.id.menu_borrar:
                db.eliminarAmigo(amigoSeleccionado);
                onRestart();
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_anyadir:
                Intent intent=new Intent(this,AddActivity.class);
                startActivity(intent);
                break;



        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        amigoSeleccionado = listaAmigos.get(position);
        return false;
    }
}

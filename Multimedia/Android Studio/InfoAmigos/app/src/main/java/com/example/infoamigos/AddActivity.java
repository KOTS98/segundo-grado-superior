package com.example.infoamigos;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.infoamigos.db.Database;
import com.example.infoamigos.util.Util;

public class AddActivity extends AppCompatActivity implements View.OnClickListener{
ImageView foto;
Database db;
private int RESULTADO_CARGA_IMAGEN = 42;
private int RESULTADO_CARGA_IMAGEN_GALERIA = 43;
private int SOLICITUD_PERMISO_ALMACENAMIENTO_EXTERNO = 1;
private SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        db=new Database(this);
        foto=findViewById(R.id.iv_seleccion);
        foto.setImageDrawable(getResources().getDrawable(R.drawable.av1));
        foto.setOnClickListener(this);
        Button btninsertar=findViewById(R.id.btn_aceptar);
        btninsertar.setOnClickListener(this);
    }



//TODO onActivityResult()


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    if((requestCode==RESULTADO_CARGA_IMAGEN) && (resultCode==RESULT_OK) && (data!=null)){
        int res=data.getExtras().getInt("SELECCION");
            switch (res) {
                case 1:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.av1));
                    break;
                case 2:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv2));
                    break;
                case 3:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv3));
                    break;
                case 4:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv4));
                    break;
            }
        }else if(requestCode == RESULTADO_CARGA_IMAGEN_GALERIA && resultCode == RESULT_OK &&
            data!= null){
        foto.setImageBitmap(Util.reduceBitmap(this, data.getDataString(), 1024,
                1024));

    }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_seleccion:
                if(!prefs.getBoolean("checkboxfoto", false)){
                    Intent intent=new Intent(this, ImageActivity.class);
                    startActivityForResult(intent, RESULTADO_CARGA_IMAGEN);
                }else{
                    if(ContextCompat.checkSelfPermission(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED){
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, RESULTADO_CARGA_IMAGEN_GALERIA);
                    }else{
                        Util.solicitarPermiso(Manifest.permission.READ_EXTERNAL_STORAGE,
                                "Sin el permiso almacenamiento externo no puedo" +
                                        "acceder a la galería de imágenes",
                                SOLICITUD_PERMISO_ALMACENAMIENTO_EXTERNO, this);
                    }
                }
                break;
            case R.id.btn_aceptar:
                Amigo amigo=new Amigo();
                EditText txt=findViewById(R.id.txt_nombre);
                amigo.setNombre(txt.getText().toString());

                txt=findViewById(R.id.txt_email);
                amigo.setEmail(txt.getText().toString());

                txt=findViewById(R.id.txt_telefono);
                amigo.setTlf(txt.getText().toString());

                txt=findViewById(R.id.txt_deuda);
                amigo.setDeuda(Float.valueOf(txt.getText().toString()));

                amigo.setFoto(((BitmapDrawable)foto.getDrawable()).getBitmap());
                db.nuevoAmigo(amigo);

                break;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        if (requestCode == SOLICITUD_PERMISO_ALMACENAMIENTO_EXTERNO) {
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN_GALERIA);
            }else{
                Toast.makeText(this,"Pues tú te lo pierdes", Toast.LENGTH_LONG).show();
            }
        }
    }
}

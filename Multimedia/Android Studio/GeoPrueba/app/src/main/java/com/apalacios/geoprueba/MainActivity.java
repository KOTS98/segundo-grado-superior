package com.apalacios.geoprueba;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apalacios.geoprueba.util.Util;

import java.io.File;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {
    Button boton;
    Button btnCamara;
    TextView latitud;
    TextView longitud;
    LocationManager gestor;
    int SOLICITUD_PERMISO_LOCALIZACION = 3;
    int RESULTADO_FOTO = 4;
    int SOLICITUD_PERMISO_EXTERNO = 5;
    Uri uri;
    Location localizacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gestor = (LocationManager) getSystemService(LOCATION_SERVICE);
        boton = findViewById(R.id.btnMapa);
        boton.setOnClickListener(this);
        btnCamara = findViewById(R.id.btnCamara);
        btnCamara.setOnClickListener(this);
        latitud = findViewById(R.id.txtLatitud);
        longitud = findViewById(R.id.txtLongitud);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == SOLICITUD_PERMISO_LOCALIZACION){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if(gestor.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                    obtenerLocalizacion();
                }
            }else{
                Toast.makeText(this, "Pues tú te lo pierdes", Toast.LENGTH_LONG).show();
            }
        }else if(requestCode == SOLICITUD_PERMISO_EXTERNO){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                ImageView iv = findViewById(R.id.ivFoto);
                iv.setImageURI(uri);
            }
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        activarProveedor();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            gestor.removeUpdates(this);
        }
    }

    private void activarProveedor() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            gestor.requestLocationUpdates(LocationManager.GPS_PROVIDER,1000*20, 5,
                    this);
        }else{
            Util.solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, "Sin este" +
                    "permiso no hay GPS", SOLICITUD_PERMISO_LOCALIZACION, this);
        }
    }

    public void obtenerLocalizacion(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if(gestor.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                localizacion = gestor.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(localizacion == null){
                    latitud.setText(String.valueOf(0));
                    longitud.setText(String.valueOf(0));
                }else{
                    latitud.setText(String.valueOf(localizacion.getLatitude()));
                    longitud.setText(String.valueOf(localizacion.getLongitude()));
                }
            }
        }else{
            Util.solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, "Sin este" +
                    "permiso no hay GPS", SOLICITUD_PERMISO_LOCALIZACION, this);
        }
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnMapa){
            obtenerLocalizacion();
            uri = Uri.parse("geo:"+localizacion.getLatitude()+","+localizacion.getLongitude()+"?z="+
                    20);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }else if(v.getId() == R.id.btnCamara){
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            uri = Uri.fromFile(new File(Environment.getExternalStorageDirectory() +
                    File.separator+"img_"+(System.currentTimeMillis()/1000)+".jpg"));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            System.out.println(uri.toString());
            startActivityForResult(intent, RESULTADO_FOTO);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULTADO_FOTO && resultCode == Activity.RESULT_OK) {
            //lo que quiera hacer con la foto
            ImageView iv = findViewById(R.id.ivFoto);
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                iv.setImageURI(uri);
            }else{
                Util.solicitarPermiso(Manifest.permission.READ_EXTERNAL_STORAGE,
                        "Necesito leer la foto", SOLICITUD_PERMISO_EXTERNO, this);
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        obtenerLocalizacion();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}

package com.apalacios.util_idades.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

public class Util {
    public static byte[] getBytes(Bitmap bitmap){
        ByteArrayOutputStream bos= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,0,bos);
        return bos.toByteArray();
    }

    public static Bitmap getBitmap(byte[] bytes){
        return BitmapFactory.decodeByteArray(bytes,0,bytes.length);

    }

    public static void solicitarPermiso(final String PERMISO, String justificacion,
                                        final int REQUEST_CODE, final Activity ACTIVIDAD){
        if(ActivityCompat.shouldShowRequestPermissionRationale(ACTIVIDAD, PERMISO)){
            new AlertDialog.Builder(ACTIVIDAD).setTitle("Solicitud de permiso")
                    .setMessage(justificacion).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(ACTIVIDAD,
                                    new String[]{PERMISO}, REQUEST_CODE);
                        }
                    }).show();
        }else{
            ActivityCompat.requestPermissions(ACTIVIDAD, new String[]{PERMISO}, REQUEST_CODE);
        }
    }

    public static Bitmap reduceBitmap(Context contexto, String uri, int maxAncho, int maxAlto){
        try {
            final BitmapFactory.Options OPTIONS = new BitmapFactory.Options();
            OPTIONS.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(contexto.getContentResolver()
                    .openInputStream(Uri.parse(uri)), null, OPTIONS);
            OPTIONS.inSampleSize = (int) Math.max(Math.ceil(OPTIONS.outWidth / maxAncho),
                    Math.ceil(OPTIONS.outHeight / maxAlto));
            OPTIONS.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(contexto.getContentResolver()
                    .openInputStream(Uri.parse(uri)), null, OPTIONS);
        } catch (FileNotFoundException e) {
            Toast.makeText(contexto, "Recurso no encontrado", Toast.LENGTH_LONG);
            e.printStackTrace();
            return null;
        }
    }
}

package com.apalacios.util_idades.listacompra;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;

import com.apalacios.util_idades.R;

/**
 * Activity que permite al usuario seleccionar una serie de imagenes predefinidas
 *
 * @author José Adrián Palacios Romo
 */
public class ImageActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView iv1, iv2, iv3, iv4, iv5, iv6, iv7, iv8, iv9;

    /**
     * Crea las imagenes clicables y les asocia su componente gráfico y un listener
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("prefTemaOscuro", false)){
            setTheme(R.style.AppTheme);
        }else{
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_image);

        iv1 = findViewById(R.id.iv_seleccion1);
        iv2 = findViewById(R.id.iv_seleccion2);
        iv3 = findViewById(R.id.iv_seleccion3);
        iv4 = findViewById(R.id.iv_seleccion4);
        iv5 = findViewById(R.id.iv_seleccion5);
        iv6 = findViewById(R.id.iv_seleccion6);
        iv7 = findViewById(R.id.iv_seleccion7);
        iv8 = findViewById(R.id.iv_seleccion8);
        iv9 = findViewById(R.id.iv_seleccion9);

        iv1.setImageDrawable(getResources().getDrawable(R.drawable.iv1_fruta));
        iv2.setImageDrawable(getResources().getDrawable(R.drawable.iv2_verdura));
        iv3.setImageDrawable(getResources().getDrawable(R.drawable.iv3_carne));
        iv4.setImageDrawable(getResources().getDrawable(R.drawable.iv4_pescado));
        iv5.setImageDrawable(getResources().getDrawable(R.drawable.iv5_pan));
        iv6.setImageDrawable(getResources().getDrawable(R.drawable.iv6_chocolate));
        iv7.setImageDrawable(getResources().getDrawable(R.drawable.iv7_refresco));
        iv8.setImageDrawable(getResources().getDrawable(R.drawable.iv8_pasta));
        iv9.setImageDrawable(getResources().getDrawable(R.drawable.iv9_leche));

        iv1.setOnClickListener(this);
        iv2.setOnClickListener(this);
        iv3.setOnClickListener(this);
        iv4.setOnClickListener(this);
        iv5.setOnClickListener(this);
        iv6.setOnClickListener(this);
        iv7.setOnClickListener(this);
        iv8.setOnClickListener(this);
        iv9.setOnClickListener(this);
    }

    /**
     * Determina cuándo el usuario ha pulsado sobre una imagen y reacciona en consecuencia
     *
     * @param v Representa el componente gráfico sobre el que se ha pulsado
     */
    @Override
    public void onClick(View v) {
        Intent intent = getIntent();
        int seleccion = 1;
        switch (v.getId()){
            case R.id.iv_seleccion1:
                seleccion = 1;
                break;
            case R.id.iv_seleccion2:
                seleccion = 2;
                break;
            case R.id.iv_seleccion3:
                seleccion = 3;
                break;
            case R.id.iv_seleccion4:
                seleccion = 4;
                break;
            case R.id.iv_seleccion5:
                seleccion = 5;
                break;
            case R.id.iv_seleccion6:
                seleccion = 6;
                break;
            case R.id.iv_seleccion7:
                seleccion = 7;
                break;
            case R.id.iv_seleccion8:
                seleccion = 8;
                break;
            case R.id.iv_seleccion9:
                seleccion = 9;
                break;
        }
        intent.putExtra("SELECCION", seleccion);
        this.setResult(RESULT_OK, intent);
        finish();
    }
}

package com.apalacios.util_idades.listacompra;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.apalacios.util_idades.R;
import com.apalacios.util_idades.listacompra.db.Database;
import com.apalacios.util_idades.util.Util;

import java.util.Objects;

/**
 * Activity que permite dotar a un producto de un nombre, un tipo y una imagen que puede ser
 * seleccionada de una serie de imagenes predefinidas o cargada desde la galería de imágenes
 * del dispositivo
 *
 * @author José Adrián Palacios Romo
 */
public class AddActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView foto;
    Database db;
    private int RESULTADO_CARGA_IMAGEN = 1;
    private int RESULTADO_CARGA_IMAGEN_GALERIA = 2;
    private int SOLICITUD_PERMISO_ALMACENAMIENTO_EXTERNO = 3;

    /**
     * Se ejecuta al inicar la Activity, crea una nueva instancia de la base de datos, los botones,
     * y una previsualización de la imagen seleccionada
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("prefTemaOscuro", false)){
            setTheme(R.style.AppTheme);
        }else{
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_add);
        db = new Database(this);
        foto = findViewById(R.id.iv_fotoSelect);
        foto.setImageDrawable(getResources().getDrawable(R.drawable.iv_default_bolsa));
        Button btnAnyadirProducto = findViewById(R.id.btnAnyadirProducto);
        btnAnyadirProducto.setOnClickListener(this);
        Button btnCancelar = findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);
        Button btnCargarImgDefault = findViewById(R.id.btnCargarImgDefault);
        btnCargarImgDefault.setOnClickListener(this);
        Button btnCargarDesdeGaleria = findViewById(R.id.btnCargarImgGaleria);
        btnCargarDesdeGaleria.setOnClickListener(this);
    }

    /**
     * Se ejectuta trás volver de la Activity que permite cargar una imágen predefinida, o trás
     * seleccionar una imagen de la galería del telefono, y actualiza la previsualización de la
     * imagen a la de la imagen seleccionada
     *
     * @param requestCode Representa el código resultante de la carga de una imagen (1 = imagen
     *                    predefinida, 2 = imagen de la galería)
     * @param resultCode Representa el resultado de la Activity de carga de la imagen, se busca
     *                   obtener el valor "RESULT_OK"
     * @param data Representan los datos enviados a la Activity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if((requestCode == RESULTADO_CARGA_IMAGEN) && (resultCode == RESULT_OK) && (data != null)){
            int seleccion = Objects.requireNonNull(data.getExtras()).getInt("SELECCION");
            switch (seleccion) {
                case 1:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv1_fruta));
                    break;
                case 2:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv2_verdura));
                    break;
                case 3:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv3_carne));
                    break;
                case 4:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv4_pescado));
                    break;
                case 5:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv5_pan));
                    break;
                case 6:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv6_chocolate));
                    break;
                case 7:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv7_refresco));
                    break;
                case 8:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv8_pasta));
                    break;
                case 9:
                    foto.setImageDrawable(getResources().getDrawable(R.drawable.iv9_leche));
                    break;
            }
        }else if(requestCode == RESULTADO_CARGA_IMAGEN_GALERIA && resultCode == RESULT_OK &&
                data!= null){
            foto.setImageBitmap(Util.reduceBitmap(this, data.getDataString(), 1024,
                    1024));
        }
    }

    /**
     * Determina cuándo el usuario a pulsado un botón y actúa en consecuencia
     *
     * @param v Representa el objeto de la vista que ha sido pulsado
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCargarImgDefault:{
                Intent intent = new Intent(this, ImageActivity.class);
                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN);
                break;}
            case R.id.btnCargarImgGaleria:{
                if(ContextCompat.checkSelfPermission(this, Manifest.permission.
                        READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.
                            EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, RESULTADO_CARGA_IMAGEN_GALERIA);

                }else{
                    Util.solicitarPermiso(Manifest.permission.READ_EXTERNAL_STORAGE,
                            "Permiso necesario para cargar imágenes desde la glaería",
                            SOLICITUD_PERMISO_ALMACENAMIENTO_EXTERNO, this);
                }
                break;}
            case R.id.btnAnyadirProducto:
                Producto producto = new Producto();
                EditText txt = findViewById(R.id.txtNombre);
                producto.setNombre(txt.getText().toString());
                txt = findViewById(R.id.txtTipo);
                producto.setTipo(txt.getText().toString());
                producto.setFoto(((BitmapDrawable)foto.getDrawable()).getBitmap());
                db.nuevoProducto(producto);
                finish();
                break;

            case R.id.btnCancelar:
                finish();
                break;
        }
    }

    /**
     * Se activa al recibir el resultado de la solicitud de permisos, en caso de no recibir la
     * aprobación del permiso, se muestra un mensaje explicando la necesidad del mismo
     *
     * @param requestCode Representa el código de solicitud del permiso (3 = Permiso de galería)
     * @param permissions Representa el vector que contiene la lista de permisos
     * @param grantResults Representa el resultado de la solicitud del permiso
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        if (requestCode == SOLICITUD_PERMISO_ALMACENAMIENTO_EXTERNO) {
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, RESULTADO_CARGA_IMAGEN_GALERIA);
            }else{
                Toast.makeText(this,"El permiso es necesario para poder cargar imágenes"
                       + "desde la galería", Toast.LENGTH_LONG).show();
            }
        }
    }
}

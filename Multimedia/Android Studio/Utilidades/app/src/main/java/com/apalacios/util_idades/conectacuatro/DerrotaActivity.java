package com.apalacios.util_idades.conectacuatro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apalacios.util_idades.MainActivity;
import com.apalacios.util_idades.R;

/**
 * Activity que se muestra al perder la partida de Conecta Cuatro, ofrece dos botones para poder
 * iniciar una nueva partida o volver al menú principal de la aplicación
 *
 * @author José Adrián Palacios Romo
 */
public class DerrotaActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Se ejecuta al iniciar la Activity, crea los botones, y los asocia a su correspondiente
     * componente gráfico, así mismo, les asigna un listener
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_derrota);

        Button btnNuevoJuegoD = findViewById(R.id.btnJugarNuevoD);
        btnNuevoJuegoD.setOnClickListener(this);
        Button btnMenuPrincipalD = findViewById(R.id.btnMenuPrincipalD);
        btnMenuPrincipalD.setOnClickListener(this);
    }

    /**
     * Determina cuándo el usuario a pulsado un boton y reacciona en consecuencia iniciando la
     * Activity deseada
     *
     * @param v Representa el objeto pulsado de la vista
     */
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnJugarNuevoD){
            startActivity(new Intent(this, JuegoActivity.class));
        }else if(v.getId() == R.id.btnMenuPrincipalD){
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}

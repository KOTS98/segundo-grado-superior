package com.apalacios.util_idades.parquimetros;

import java.util.ArrayList;

public class Constantes {
    public final static String URL = "https://www.zaragoza.es/sede/servicio/urbanismo-infraestructuras/" +
            "equipamiento/parquimetro.json?srsname=wgs84&start=0&rows=50&distance=500&point=" +
            "-0.8774209607549572%2C41.65599799116259";

    public static ArrayList<Parquimetro> listaParquimetros;
}

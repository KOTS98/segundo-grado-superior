package com.apalacios.util_idades.parquimetros;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apalacios.util_idades.R;

import java.util.ArrayList;

public class ParquimetroAdapter extends BaseAdapter {
    private ArrayList<Parquimetro> listaParquimetros;
    private LayoutInflater inflater;

    public ParquimetroAdapter(Context contexto, ArrayList<Parquimetro> listaParquimetros) {
        this.listaParquimetros = listaParquimetros;
        this.inflater = LayoutInflater.from(contexto);
    }

    static class ViewHolder{
        TextView txtIDParquimetro;
        TextView txtTipoParquimetro;
        TextView txtDireccionParquimetro;
    }

    @Override
    public int getCount() {
        return listaParquimetros.size();
    }

    @Override
    public Object getItem(int position) {
        return listaParquimetros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.fila_parquimetros,null);
            holder = new ViewHolder();
            holder.txtIDParquimetro = convertView.findViewById(R.id.txtIDParquimetro);
            holder.txtTipoParquimetro = convertView.findViewById(R.id.txtTipoParquimetro);
            holder.txtDireccionParquimetro = convertView.findViewById(R.id.txtDireccionParquimetro);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Parquimetro parquimetro = listaParquimetros.get(position);
        holder.txtIDParquimetro.setText(String.valueOf(parquimetro.getId()));
        holder.txtTipoParquimetro.setText(parquimetro.getTipo());
        holder.txtDireccionParquimetro.setText(parquimetro.getDireccion());
        return convertView;
    }
}

package com.apalacios.util_idades.parquimetros;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.apalacios.util_idades.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ParquimetrosActivity extends AppCompatActivity implements View.OnClickListener{
    private ParquimetroAdapter adaptador;
    private ArrayList<Parquimetro> listaParquimetros;
    private ListView lvLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("prefTemaOscuro", false)){
            setTheme(R.style.AppTheme);
        }else{
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_parquimetros);
        listaParquimetros = new ArrayList<>();
        lvLista = findViewById(R.id.lvParquimetros);
        adaptador = new ParquimetroAdapter(this, listaParquimetros);

        Button btnVerEnMapa = findViewById(R.id.btnVerEnMapa);
        btnVerEnMapa.setOnClickListener(this);

        onRestart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        adaptador = new ParquimetroAdapter(this, listaParquimetros);
        lvLista.setAdapter(adaptador);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Descarga descarga = new Descarga();
        descarga.execute(Constantes.URL);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnVerEnMapa){
            startActivity(new Intent(this, MapaActivity.class));
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class Descarga extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String cadena;
            JSONObject json;
            JSONArray jsonArray;
            try{
                URL url = new URL(Constantes.URL);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String linea;
                while((linea=br.readLine())!=null) {
                    sb.append(linea).append("\n");
                }
                conexion.disconnect();
                br.close();
                cadena = sb.toString();

                json = new JSONObject(cadena);
                jsonArray = json.getJSONArray("result");

                //Parquimetro
                int id;
                String title;
                String descripcion;
                String lastUpdated;
                Geometria geometry;
                Coordenadas coordinates;
                String icon;

                Parquimetro parquimetro;
                for (int i = 0; i <jsonArray.length(); i++) {
                    id = jsonArray.getJSONObject(i).getInt("id");

                    title = jsonArray.getJSONObject(i).getString("title");

                    descripcion = jsonArray.getJSONObject(i).getString("description");

                    lastUpdated = jsonArray.getJSONObject(i).getString("lastUpdated");

                    coordinates = new Coordenadas(jsonArray.getJSONObject(i).getJSONObject("geometry")
                            .getJSONArray("coordinates").getString(0),
                            jsonArray.getJSONObject(i).getJSONObject("geometry")
                                    .getJSONArray("coordinates").getString(1));

                    geometry = new Geometria(jsonArray.getJSONObject(i).getJSONObject("geometry")
                            .getString("type"), coordinates);

                    icon = jsonArray.getJSONObject(i).getString("icon");

                    parquimetro = new Parquimetro(id, title, descripcion, lastUpdated, geometry, icon);
                    listaParquimetros.add(parquimetro);
                }
                Constantes.listaParquimetros = listaParquimetros;

            }catch(IOException | JSONException ioe){
                ioe.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            lvLista.setAdapter(adaptador);
        }
    }
}

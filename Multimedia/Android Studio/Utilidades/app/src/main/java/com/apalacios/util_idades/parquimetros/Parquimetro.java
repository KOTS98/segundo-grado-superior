package com.apalacios.util_idades.parquimetros;

public class Parquimetro {
    private int id;
    private String tipo;
    private String direccion;
    private String ultimaActualizacion;
    private Geometria geometria;
    private String icono;

    public Parquimetro(int id, String tipo, String direccion, String ultimaActualizacion, Geometria geometria, String icono) {
        this.id = id;
        this.tipo = tipo;
        this.direccion = direccion;
        this.ultimaActualizacion = ultimaActualizacion;
        this.geometria = geometria;
        this.icono = icono;
    }

    public int getId() {
        return id;
    }

    public String getTipo() {
        return tipo;
    }

    public String getDireccion() {
        return direccion;
    }

    public Geometria getGeometria() {
        return geometria;
    }

    @Override
    public String toString() {
        return "Id: " + id + ", Tipo: " + tipo + ", Dirección: " + direccion;
    }
}

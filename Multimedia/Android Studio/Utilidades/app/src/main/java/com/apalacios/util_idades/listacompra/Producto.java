package com.apalacios.util_idades.listacompra;

import android.graphics.Bitmap;

public class Producto {
    private long id;
    private String nombre;
    private String tipo;
    private Bitmap foto;

    public Producto(){
    }

    public long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public Bitmap getFoto() {
        return foto;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setFoto(Bitmap foto) {
        this.foto = foto;
    }
}

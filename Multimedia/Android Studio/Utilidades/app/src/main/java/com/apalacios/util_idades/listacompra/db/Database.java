package com.apalacios.util_idades.listacompra.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.apalacios.util_idades.listacompra.Producto;
import com.apalacios.util_idades.util.Util;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;

/**
 * Esta clase se encarga de gestionar las operaciones a realizar con la base de datos de la
 * aplicación
 *
 * @author José Adrián Palacios Romo
 */
public class Database extends SQLiteOpenHelper {

    /**
     * Representa la versión de la base de datos que será utilizada
     */
    private static final int VERSION = 1;

    /**
     * Constructor de la clase
     *
     * @param contexto Representa el contexto en el que se ejecuta la base de datos
     */
    public Database(Context contexto){
        super(contexto,Constantes.BASE_DATOS, null, VERSION);
    }

    /**
     * Se ejecuta al cargar la base de datos, comprueba si existe la tabla especificada en la clase
     * Constantes, en caso negativo, la crea
     *
     * @param db Representa el objeto de base de datos con el que se trabajará
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Constantes.TABLA_PRODUCTOS + "(" + _ID +
                " INTEGER PRIMARY KEY AUTOINCREMENT, " + Constantes.NOMBRE + " TEXT," +
                Constantes.TIPO + " TEXT," + Constantes.FOTO + " BLOB)");
    }

    /**
     * Se ejecuta cuándo se modifica la versión de la base de datos. Elimina la tabla actual y
     * crea una nueva
     *
     * @param db Representa la base de datos
     * @param oldVersion Representa la versión anterior de la base de datos
     * @param newVersion Representa la nueva versión de la base de datos
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Constantes.TABLA_PRODUCTOS);
        onCreate(db);
    }

    /**
     * Recibe un objeto de tipo producto y lo añade a la base de datos
     *
     * @param producto Representa el objeto que desea añadirse a la base de datos
     */
    public void nuevoProducto(Producto producto){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Constantes.NOMBRE, producto.getNombre());
        values.put(Constantes.TIPO, producto.getTipo());
        values.put(Constantes.FOTO, Util.getBytes(producto.getFoto()));
        db.insertOrThrow(Constantes.TABLA_PRODUCTOS,null, values);
        db.close();
    }

    /**
     * Elimina el producto especificado de la base de datos
     *
     * @param producto Representa el producto que se desea eliminar de la base de datos
     */
    public void eliminarProducto(Producto producto){
        SQLiteDatabase db = getWritableDatabase();
        String[] argumentos = new String[]{String.valueOf((producto.getId()))};
        db.delete(Constantes.TABLA_PRODUCTOS,"_id=?", argumentos);
        db.close();
    }

    /**
     * Elimina todas las entradas de la tabla en la base de datos
     *
     * @param listaProductos Representa la lista de productos que serán eliminados de la base de
     *                       datos
     */
    public void eliminarTodo(ArrayList<Producto> listaProductos){
        SQLiteDatabase db = getWritableDatabase();
        for (Producto producto : listaProductos) {
            String[] argumentos = new String[]{String.valueOf((producto.getId()))};
            db.delete(Constantes.TABLA_PRODUCTOS,"_id=?", argumentos);
        }
        db.close();
    }

    /**
     * Devuelve la lista de productos de la base de datos ordenados por su ID
     *
     * @return La lista de productos ordenados por ID
     */
    public ArrayList<Producto> getProductosID(){
        final String[] SELECT = {_ID, Constantes.NOMBRE, Constantes.TIPO, Constantes.FOTO};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Constantes.TABLA_PRODUCTOS,SELECT,null, null,
                null, null, _ID);
        ArrayList<Producto> listaProductos = new ArrayList<>();
        Producto producto;
        while(cursor.moveToNext()){
            producto = new Producto();
            producto.setId(cursor.getLong(0));
            producto.setNombre(cursor.getString(1));
            producto.setTipo(cursor.getString(2));
            producto.setFoto(Util.getBitmap(cursor.getBlob(3)));
            listaProductos.add(producto);
        }
        cursor.close();
        db.close();
        return listaProductos;
    }

    /**
     * Devuelve la lista de productos de la base de datos ordenados por su nombre
     *
     * @return La lista de productos ordenados por nombre
     */
    public ArrayList<Producto> getProductosNombre(){
        final String[] SELECT = {_ID, Constantes.NOMBRE, Constantes.TIPO, Constantes.FOTO};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(Constantes.TABLA_PRODUCTOS,SELECT,null, null,
                null, null, Constantes.NOMBRE);
        ArrayList<Producto> listaProductos = new ArrayList<>();
        Producto producto;
        while(cursor.moveToNext()){
            producto = new Producto();
            producto.setId(cursor.getLong(0));
            producto.setNombre(cursor.getString(1));
            producto.setTipo(cursor.getString(2));
            producto.setFoto(Util.getBitmap(cursor.getBlob(3)));
            listaProductos.add(producto);
        }
        cursor.close();
        db.close();
        return listaProductos;
    }
}

package com.apalacios.util_idades.listacompra;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.apalacios.util_idades.R;

import java.util.ArrayList;

public class ProductoAdapter extends BaseAdapter {
    private ArrayList<Producto> listaProductos;
    private LayoutInflater inflater;

    ProductoAdapter(Context context, ArrayList<Producto> listaProductos) {
        this.listaProductos = listaProductos;
        inflater = LayoutInflater.from(context);
    }

    static class ViewHolder{
        TextView txt_nombre;
        TextView txt_tipo;
        ImageView foto;
    }

    @Override
    public int getCount() {
        return listaProductos.size();
    }

    @Override
    public Object getItem(int position) {
        return listaProductos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.fila_lista_compra,null);
            holder = new ViewHolder();
            holder.txt_nombre = convertView.findViewById(R.id.txt_nombre);
            holder.txt_tipo = convertView.findViewById(R.id.txt_tipo);
            holder.foto = convertView.findViewById(R.id.iv_fotoSelect);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Producto producto = listaProductos.get(position);
        holder.txt_nombre.setText(producto.getNombre());
        holder.txt_tipo.setText(producto.getTipo());
        holder.foto.setImageBitmap(producto.getFoto());
        return convertView;
    }
}
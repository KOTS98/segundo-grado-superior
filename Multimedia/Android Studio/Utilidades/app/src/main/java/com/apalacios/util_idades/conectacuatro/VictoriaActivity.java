package com.apalacios.util_idades.conectacuatro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.apalacios.util_idades.MainActivity;
import com.apalacios.util_idades.R;

/**
 * Activity que se muestra al ganar la partida de Conecta Cuatro, ofrece dos botones para poder
 * iniciar una nueva partida o volver al menú principal de la aplicación
 *
 * @author José Adrián Palacios Romo
 */
public class VictoriaActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * Se ejecuta al iniciar la Activity, crea los botones, y los asocia a su correspondiente
     * componente gráfico, así mismo, les asigna un listener
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_victoria);

        Button btnNuevoJuego = findViewById(R.id.btnJugarNuevo);
        btnNuevoJuego.setOnClickListener(this);
        Button btnMenuPrincipal = findViewById(R.id.btnMenuPrincipal);
        btnMenuPrincipal.setOnClickListener(this);
    }

    /**
     * Determina cuándo el usuario a pulsado un boton y reacciona en consecuencia iniciando la
     * Activity deseada
     *
     * @param v Representa el objeto pulsado de la vista
     */
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnJugarNuevo){
            startActivity(new Intent(this, JuegoActivity.class));
        }else if(v.getId() == R.id.btnMenuPrincipal){
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}

package com.apalacios.util_idades.listacompra.db;

/**
 * Contiene una serie de constantes que representan los parámetros deseados para la base de datos
 *
 * @author José Adrián Palacios Romo
 */
public class Constantes {

    /**
     * Representa la base de datos con la que se interactuará
     */
    public static final String BASE_DATOS = "productos.db";

    /**
     * Representa la tabla con la que se interactuará
     */
    public static final String TABLA_PRODUCTOS = "productos";

    /**
     * Representan los valores de nombre, tipo y foto, del objeto que se añadirá a la base de datos
     */
    public static final String NOMBRE = "nombre";
    public static final String TIPO = "tipo";
    public static final String FOTO = "foto";
}

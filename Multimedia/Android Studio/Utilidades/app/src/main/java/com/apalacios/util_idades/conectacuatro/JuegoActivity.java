package com.apalacios.util_idades.conectacuatro;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.apalacios.util_idades.R;

import java.util.Random;

import pl.droidsonroids.gif.GifImageView;

/**
 * Activity principal del juego de Conecta Cuatro. Se trata del juego original de Conecta Cuatro,
 * pero con una serie de modificaciones. El juego cuénta con temática del juego "Undertale", y posee
 * mecánicas nuevas, cómo que el enemigo pueda hacer "trampas" eliminando una columna completa de
 * fichas
 *
 * @author José Adrián Palacios Romo
 */
public class JuegoActivity extends AppCompatActivity implements View.OnClickListener {

    private int[][] tablero = new int[7][6]; //R.drawable.vacio = vacio, R.drawable.corazon = corazon, R.drawable.hueso = hueso

    private ImageView iv_casilla_0_0;
    private ImageView iv_casilla_1_0;
    private ImageView iv_casilla_2_0;
    private ImageView iv_casilla_3_0;
    private ImageView iv_casilla_4_0;
    private ImageView iv_casilla_5_0;
    private ImageView iv_casilla_6_0;

    private ImageView iv_casilla_0_1;
    private ImageView iv_casilla_1_1;
    private ImageView iv_casilla_2_1;
    private ImageView iv_casilla_3_1;
    private ImageView iv_casilla_4_1;
    private ImageView iv_casilla_5_1;
    private ImageView iv_casilla_6_1;

    private ImageView iv_casilla_0_2;
    private ImageView iv_casilla_1_2;
    private ImageView iv_casilla_2_2;
    private ImageView iv_casilla_3_2;
    private ImageView iv_casilla_4_2;
    private ImageView iv_casilla_5_2;
    private ImageView iv_casilla_6_2;

    private ImageView iv_casilla_0_3;
    private ImageView iv_casilla_1_3;
    private ImageView iv_casilla_2_3;
    private ImageView iv_casilla_3_3;
    private ImageView iv_casilla_4_3;
    private ImageView iv_casilla_5_3;
    private ImageView iv_casilla_6_3;

    private ImageView iv_casilla_0_4;
    private ImageView iv_casilla_1_4;
    private ImageView iv_casilla_2_4;
    private ImageView iv_casilla_3_4;
    private ImageView iv_casilla_4_4;
    private ImageView iv_casilla_5_4;
    private ImageView iv_casilla_6_4;

    private ImageView iv_casilla_0_5;
    private ImageView iv_casilla_1_5;
    private ImageView iv_casilla_2_5;
    private ImageView iv_casilla_3_5;
    private ImageView iv_casilla_4_5;
    private ImageView iv_casilla_5_5;
    private ImageView iv_casilla_6_5;

    private boolean turnoJugador;

    private GifImageView sans;

    private MediaPlayer charla;

    private static MediaPlayer playerMusica;

    private ImageView cabezaVertical;

    private ImageView dialogo;

    /**
     * Se encarga de establecer el componente gráfico de cada elemento del juego, crear los botones,
     * y establecerles un listener. También inicia la música del juego
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);

        iv_casilla_0_0 = findViewById(R.id.iv_casilla_0_0);
        iv_casilla_1_0 = findViewById(R.id.iv_casilla_1_0);
        iv_casilla_2_0 = findViewById(R.id.iv_casilla_2_0);
        iv_casilla_3_0 = findViewById(R.id.iv_casilla_3_0);
        iv_casilla_4_0 = findViewById(R.id.iv_casilla_4_0);
        iv_casilla_5_0 = findViewById(R.id.iv_casilla_5_0);
        iv_casilla_6_0 = findViewById(R.id.iv_casilla_6_0);

        iv_casilla_0_1 = findViewById(R.id.iv_casilla_0_1);
        iv_casilla_1_1 = findViewById(R.id.iv_casilla_1_1);
        iv_casilla_2_1 = findViewById(R.id.iv_casilla_2_1);
        iv_casilla_3_1 = findViewById(R.id.iv_casilla_3_1);
        iv_casilla_4_1 = findViewById(R.id.iv_casilla_4_1);
        iv_casilla_5_1 = findViewById(R.id.iv_casilla_5_1);
        iv_casilla_6_1 = findViewById(R.id.iv_casilla_6_1);

        iv_casilla_0_2 = findViewById(R.id.iv_casilla_0_2);
        iv_casilla_1_2 = findViewById(R.id.iv_casilla_1_2);
        iv_casilla_2_2 = findViewById(R.id.iv_casilla_2_2);
        iv_casilla_3_2 = findViewById(R.id.iv_casilla_3_2);
        iv_casilla_4_2 = findViewById(R.id.iv_casilla_4_2);
        iv_casilla_5_2 = findViewById(R.id.iv_casilla_5_2);
        iv_casilla_6_2 = findViewById(R.id.iv_casilla_6_2);

        iv_casilla_0_3 = findViewById(R.id.iv_casilla_0_3);
        iv_casilla_1_3 = findViewById(R.id.iv_casilla_1_3);
        iv_casilla_2_3 = findViewById(R.id.iv_casilla_2_3);
        iv_casilla_3_3 = findViewById(R.id.iv_casilla_3_3);
        iv_casilla_4_3 = findViewById(R.id.iv_casilla_4_3);
        iv_casilla_5_3 = findViewById(R.id.iv_casilla_5_3);
        iv_casilla_6_3 = findViewById(R.id.iv_casilla_6_3);

        iv_casilla_0_4 = findViewById(R.id.iv_casilla_0_4);
        iv_casilla_1_4 = findViewById(R.id.iv_casilla_1_4);
        iv_casilla_2_4 = findViewById(R.id.iv_casilla_2_4);
        iv_casilla_3_4 = findViewById(R.id.iv_casilla_3_4);
        iv_casilla_4_4 = findViewById(R.id.iv_casilla_4_4);
        iv_casilla_5_4 = findViewById(R.id.iv_casilla_5_4);
        iv_casilla_6_4 = findViewById(R.id.iv_casilla_6_4);

        iv_casilla_0_5 = findViewById(R.id.iv_casilla_0_5);
        iv_casilla_1_5 = findViewById(R.id.iv_casilla_1_5);
        iv_casilla_2_5 = findViewById(R.id.iv_casilla_2_5);
        iv_casilla_3_5 = findViewById(R.id.iv_casilla_3_5);
        iv_casilla_4_5 = findViewById(R.id.iv_casilla_4_5);
        iv_casilla_5_5 = findViewById(R.id.iv_casilla_5_5);
        iv_casilla_6_5 = findViewById(R.id.iv_casilla_6_5);

        Button btnColumna0 = findViewById(R.id.btnColumna0);
        Button btnColumna1 = findViewById(R.id.btnColumna1);
        Button btnColumna2 = findViewById(R.id.btnColumna2);
        Button btnColumna3 = findViewById(R.id.btnColumna3);
        Button btnColumna4 = findViewById(R.id.btnColumna4);
        Button btnColumna5 = findViewById(R.id.btnColumna5);
        Button btnColumna6 = findViewById(R.id.btnColumna6);

        btnColumna0.setOnClickListener(this);
        btnColumna1.setOnClickListener(this);
        btnColumna2.setOnClickListener(this);
        btnColumna3.setOnClickListener(this);
        btnColumna4.setOnClickListener(this);
        btnColumna5.setOnClickListener(this);
        btnColumna6.setOnClickListener(this);

        sans = findViewById(R.id.gIVSansJuego);

        cabezaVertical = findViewById(R.id.cabeza_vertical);
        turnoJugador = true;

        playerMusica = MediaPlayer.create(this, R.raw.chara_theme);
        playerMusica.setVolume((float) 0.5, (float) 0.5);
        playerMusica.setLooping(true);
        playerMusica.start();

        inicializarTablero();

        dialogo = findViewById(R.id.ivDialogo);

        ObjectAnimator animation = ObjectAnimator.ofFloat(dialogo,
                "translationY", 500);
        animation.setDuration(500);
        animation.start();
    }

    /**
     * Se encarga de pausar la música del juego al cambiar de aplicación o bloquear el dispositivo
     */
    @Override
    protected void onPause() {
        super.onPause();
        playerMusica.pause();
    }

    /**
     * Se encarga de continuar la ejecución de la música del juego al volver desde otra aplicación
     * o desbloquear el dispositivo
     */
    @Override
    protected void onResume() {
        super.onResume();
        playerMusica.start();
    }

    /**
     * Detiene la música del juego al detenerse la aplicación
     */
    @Override
    protected void onStop() {
        super.onStop();
        playerMusica.pause();
    }

    /**
     * Determina cuándo el usuario a pulsado un botón y actua en consecuencia, colocando una ficha
     * en la posición seleccionada y cediendo el turno al contrincante
     * @param v
     */
    @Override
    public void onClick(View v) {
        if (turnoJugador){
            switch (v.getId()){
                case R.id.btnColumna0:{
                    turnoJugador = !colocarFicha(0, R.drawable.corazon);
                    comprobarVictoria();
                    turnoSans();
                }
                break;

                case R.id.btnColumna1:{
                    turnoJugador = !colocarFicha(1, R.drawable.corazon);
                    comprobarVictoria();
                    turnoSans();
                }
                break;

                case R.id.btnColumna2:{
                    turnoJugador = !colocarFicha(2, R.drawable.corazon);
                    comprobarVictoria();
                    turnoSans();
                }
                break;

                case R.id.btnColumna3:{
                    turnoJugador = !colocarFicha(3, R.drawable.corazon);
                    comprobarVictoria();
                    turnoSans();
                }
                break;

                case R.id.btnColumna4:{
                    turnoJugador = !colocarFicha(4, R.drawable.corazon);
                    comprobarVictoria();
                    turnoSans();
                }
                break;

                case R.id.btnColumna5:{
                    turnoJugador = !colocarFicha(5, R.drawable.corazon);
                    comprobarVictoria();
                    turnoSans();
                }
                break;

                case R.id.btnColumna6:{
                    turnoJugador = !colocarFicha(6, R.drawable.corazon);
                    comprobarVictoria();
                    turnoSans();
                }
                break;
            }
        }
    }

    /**
     * Mediante azar, determina si el contrincante colocará una ficha de marea convencional, o
     * utilizará su habilidad especial, en caso de utilizar la habilidad, al terminar, reiniciará el
     * diálogo y terminará la ejecución del sonido de su voz.
     */
    private void turnoSans(){
        Random random = new Random();
        int numRandom = random.nextInt((5 - 1) + 1) + 1;
        if (numRandom < 5){
            mejorMovimientoSans();

        }else if (numRandom == 5) {
            dialogoSans();
            especialVerticalSans();
            new CountDownTimer(2500, 1000) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    charla.release();
                    dialogo.setImageResource(R.drawable.dialogo_vacio);
                }
            }.start();
        }
    }

    /**
     * Selecciona un diálogo aleatorio que se mostrará en pantalla pretendiendo ser dicho por
     * el contrincante y ejecuta un MediaPlayer con el sonido de su voz.
     */
    private void dialogoSans(){
        Random random = new Random();
        int numRandom = random.nextInt((7 - 1) + 1) + 1;
        switch(numRandom){
            case 1:
                dialogo.setImageResource(R.drawable.dialogo1);
                break;
            case 2:
                dialogo.setImageResource(R.drawable.dialogo2);
                break;
            case 3:
                dialogo.setImageResource(R.drawable.dialogo3);
                break;
            case 4:
                dialogo.setImageResource(R.drawable.dialogo4);
                break;
            case 5:
                dialogo.setImageResource(R.drawable.dialogo5);
                break;
            case 6:
                dialogo.setImageResource(R.drawable.dialogo6);
                break;
            case 7:
                dialogo.setImageResource(R.drawable.dialogo7);
                break;
        }
        charla = MediaPlayer.create(this, R.raw.sans_noise);
        charla.setVolume(1, 1);
        charla.start();
    }

    /**
     * Cuando el contrincate va a colocar una ficha, espera un tiempo aleatorio de entre 1 y 3
     * segundos, dando la sensación de que está "pensando" su siguiente movimiento.
     * Despues, cambia la imagen del contrincate, a la imagen correspondiente de la secuencia de
     * ataque, espera 0.6 segundos, coloca su ficha y vuelve a establecer su imagen normal
     *
     * @param columna Representa la columna donde se colocará la ficha
     */
    private void ataqueSans(int columna){
        Random random = new Random();
        int numRandom = random.nextInt((3000 - 1000) + 1) + 1000;

        new CountDownTimer(numRandom, 1000) {
            public void onTick(long millisUntilFinished) {}

            public void onFinish() {
                sans.setImageResource(R.drawable.sans_ataque);

                new CountDownTimer(600, 1000) {
                    public void onTick(long millisUntilFinished) {}

                    public void onFinish() {
                        colocarFicha(columna, R.drawable.hueso);
                        turnoJugador = true;
                        sans.setImageResource(R.drawable.sans_normal);
                    }
                }.start();
            }
        }.start();
        comprobarVictoria();
    }

    /**
     * Determina con cierto margen de error, el "mejor" movimiento posible del contrincante,
     * primero determina si puede realizar algún movimiento que le conceda la victoria, después
     * determina si puede bloquear el avance del usuario para evitar que gane en el siguiente turno,
     * después comprueba si tiene alguna cadena ya empezada para continuarla, y como último recurso,
     * selecciona una posición al azar para colocar la ficha
     */
    private void mejorMovimientoSans(){
        boolean fichaPuesta = false;

        int combo;

        //Verticales - Ganar
        for (int i = 0; i <= 6; i++){
            combo = 0;
            for (int j = 5; j >= 0; j--){
                if(tablero[i][j] == R.drawable.hueso){
                    combo++;
                }else if(tablero[i][j] == R.drawable.corazon){
                    combo = 0;
                }
                if(combo == 3 && j < 5){
                    if(tablero[i][j + 1] == R.drawable.vacio && !fichaPuesta){
                        ataqueSans(i);
                        fichaPuesta = true;
                    }
                }
            }
        }

        //Horizontales - Ganar
        if (!fichaPuesta){
            //De izquierda a derecha
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 0; j <= 6; j++){
                    if(tablero[j][i] == R.drawable.hueso){
                        combo++;
                    }else if (tablero[j][i] == R.drawable.corazon){
                        combo = 0;
                    }
                    if(combo == 3 && j < 6) {
                        if(tablero[j + 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j + 1);
                            fichaPuesta = true;
                        }
                    }
                }
            }

            //De derecha a izquierda
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 6; j >= 0; j--){
                    if(tablero[j][i] == R.drawable.hueso){
                        combo++;
                    }else if (tablero[j][i] == R.drawable.corazon){
                        combo = 0;
                    }
                    if(combo == 3 && j > 0) {
                        if(tablero[j - 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j - 1);
                            fichaPuesta = true;
                        }
                    }
                }
            }
        }

        //Verticales - Defensa
        if(!fichaPuesta){
            for (int i = 0; i <= 6; i++){
                combo = 0;
                for (int j = 5; j >= 0; j--){
                    if(tablero[i][j] == R.drawable.corazon){
                        combo++;
                    }else if(tablero[i][j] == R.drawable.hueso){
                        combo = 0;
                    }
                    if(combo == 3 && j < 5){
                        if(tablero[i][j + 1] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(i);
                            fichaPuesta = true;
                        }
                    }
                }
            }
        }

        //Horizcntales - Defensa
        if (!fichaPuesta){
            //De izquierda a derecha
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 0; j <= 6; j++){
                    if(tablero[j][i] == R.drawable.corazon){
                        combo++;
                    }else if (tablero[j][i] == R.drawable.hueso){
                        combo = 0;
                    }
                    if(combo == 3 && j < 6) {
                        if(tablero[j + 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j + 1);
                            fichaPuesta = true;
                        }
                    }
                }
            }

            //De derecha a izquierda
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 6; j >= 0; j--){
                    if(tablero[j][i] == R.drawable.corazon){
                        combo++;
                    }else if (tablero[j][i] == R.drawable.hueso){
                        combo = 0;
                    }
                    if(combo == 3 && j > 0) {
                        if(tablero[j - 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j - 1);
                            fichaPuesta = true;
                        }
                    }
                }
            }
        }

        //Verticales - Avance
        if(!fichaPuesta){
            for (int i = 0; i <= 6; i++){
                combo = 0;
                for (int j = 5; j >= 0; j--){
                    if(tablero[i][j] == R.drawable.hueso){
                        combo++;
                    }else if(tablero[i][j] == R.drawable.corazon){
                        combo = 0;
                    }
                    if(combo == 1 && j < 5){
                        if(tablero[i][j + 1] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(i);
                            fichaPuesta = true;
                        }
                    }
                }
            }
        }

        //Horizontales - Avance
        if (!fichaPuesta){
            //De izquierda a derecha
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 0; j <= 6; j++){
                    if(tablero[j][i] == R.drawable.hueso){
                        combo++;
                    }else if (tablero[j][i] == R.drawable.corazon){
                        combo = 0;
                    }
                    if(combo == 1 && j < 6) {
                        if(tablero[j + 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j + 1);
                            fichaPuesta = true;
                        }
                    }
                    if(combo == 2 && j < 6) {
                        if(tablero[j + 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j + 1);
                            fichaPuesta = true;
                        }
                    }
                }
            }

            //De derecha a izquierda
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 6; j >= 0; j--){
                    if(tablero[j][i] == R.drawable.hueso){
                        combo++;
                    }else if (tablero[j][i] == R.drawable.corazon){
                        combo = 0;
                    }
                    if(combo == 1 && j > 0) {
                        if(tablero[j - 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j - 1);
                            fichaPuesta = true;
                        }
                    }
                    if(combo == 2 && j > 0) {
                        if(tablero[j - 1][i] == R.drawable.vacio && !fichaPuesta){
                            ataqueSans(j - 1);
                            fichaPuesta = true;
                        }
                    }
                }
            }
        }

        //Aleatorio
        if(!fichaPuesta){
            Random random = new Random();
            int numRandom = random.nextInt((7 - 1) + 1);
            ataqueSans(numRandom);
        }
    }

    /**
     * Selecciona al azar una columna para lanzar su habilidad especial, lo que consiste en eliminar
     * una columna entera de fichas.
     * Primero, genera la cabeza que apunta a la columna a eliminar, después la desplaza a la
     * posición de la columna, elimina una por una, de arriba a abajo todas las fichas de la columna,
     * vuelve a su posición inicial, y desaparece.
     * Después, el contricante lanza una ficha a la misma columna donde se ha lanzado la habilidad
     * especial.
     */
    private void especialVerticalSans(){
        Random random = new Random();
        int numRandom = random.nextInt((7 - 1) + 1);

        //Movemos laser y disparamos
        cabezaVertical.setImageResource(R.drawable.cabeza_laser);
        moverCabezaVertical(numRandom, true);
        sans.setImageResource(R.drawable.sans_ataque);
        new CountDownTimer(1800, 300) {
            int contador = 0;
            public void onTick(long millisUntilFinished) {
                tablero[numRandom][contador] = R.drawable.vacio;
                actualizarTablero();
                contador++;
            }

            public void onFinish() {
                sans.setImageResource(R.drawable.sans_normal);
                moverCabezaVertical(numRandom, false);
                new CountDownTimer(1000 , 1000) {
                    public void onTick(long millisUntilFinished) {}
                    public void onFinish() {
                        cabezaVertical.setImageResource(R.drawable.vacio);
                        ataqueSans(numRandom);
                    }
                }.start();
            }
        }.start();

    }

    /**
     * Establece imagenes vacias en todas las casillas del tablero
     */
    private void inicializarTablero(){
        for (int i = 0; i <= 6; i++){
            for (int j = 0; j <= 5; j++){
                tablero[i][j] = R.drawable.vacio;
            }
        }
    }

    /**
     * Coloca una ficha del tipo seleccionado en la columna seleccionada, mandando la ficha a la
     * posición más baja disponible en la columna
     *
     * @param columna Representa la columna deseada para colocar la ficha
     * @param tipoFicha Representa el tipo de ficha deseada (corazón o hueso)
     * @return Devuelve true, si se ha colocado la ficha
     */
    private boolean colocarFicha(int columna, int tipoFicha){
        boolean colocada = false;
        for (int i = 5; i >= 0; i--){
            if(tablero[columna][i] == R.drawable.vacio && !colocada){
                if (tipoFicha == R.drawable.corazon){
                    tablero[columna][i] = R.drawable.corazon;
                }else{
                    tablero[columna][i] = R.drawable.hueso;
                }
                colocada = true;
            }
        }
        actualizarTablero();
        comprobarVictoria();
        return colocada;
    }

    /**
     * Actualiza el componente gráfico de todas las casillas del tablero
     */
    private void actualizarTablero(){
        //Columna 1
        iv_casilla_0_0.setImageResource(tablero[0][0]);
        iv_casilla_0_1.setImageResource(tablero[0][1]);
        iv_casilla_0_2.setImageResource(tablero[0][2]);
        iv_casilla_0_3.setImageResource(tablero[0][3]);
        iv_casilla_0_4.setImageResource(tablero[0][4]);
        iv_casilla_0_5.setImageResource(tablero[0][5]);

        //Columna 2
        iv_casilla_1_0.setImageResource(tablero[1][0]);
        iv_casilla_1_1.setImageResource(tablero[1][1]);
        iv_casilla_1_2.setImageResource(tablero[1][2]);
        iv_casilla_1_3.setImageResource(tablero[1][3]);
        iv_casilla_1_4.setImageResource(tablero[1][4]);
        iv_casilla_1_5.setImageResource(tablero[1][5]);

        //Columna 3
        iv_casilla_2_0.setImageResource(tablero[2][0]);
        iv_casilla_2_1.setImageResource(tablero[2][1]);
        iv_casilla_2_2.setImageResource(tablero[2][2]);
        iv_casilla_2_3.setImageResource(tablero[2][3]);
        iv_casilla_2_4.setImageResource(tablero[2][4]);
        iv_casilla_2_5.setImageResource(tablero[2][5]);

        //Columna 4
        iv_casilla_3_0.setImageResource(tablero[3][0]);
        iv_casilla_3_1.setImageResource(tablero[3][1]);
        iv_casilla_3_2.setImageResource(tablero[3][2]);
        iv_casilla_3_3.setImageResource(tablero[3][3]);
        iv_casilla_3_4.setImageResource(tablero[3][4]);
        iv_casilla_3_5.setImageResource(tablero[3][5]);

        //Columna 5
        iv_casilla_4_0.setImageResource(tablero[4][0]);
        iv_casilla_4_1.setImageResource(tablero[4][1]);
        iv_casilla_4_2.setImageResource(tablero[4][2]);
        iv_casilla_4_3.setImageResource(tablero[4][3]);
        iv_casilla_4_4.setImageResource(tablero[4][4]);
        iv_casilla_4_5.setImageResource(tablero[4][5]);

        //Columna 6
        iv_casilla_5_0.setImageResource(tablero[5][0]);
        iv_casilla_5_1.setImageResource(tablero[5][1]);
        iv_casilla_5_2.setImageResource(tablero[5][2]);
        iv_casilla_5_3.setImageResource(tablero[5][3]);
        iv_casilla_5_4.setImageResource(tablero[5][4]);
        iv_casilla_5_5.setImageResource(tablero[5][5]);

        //Columna 7
        iv_casilla_6_0.setImageResource(tablero[6][0]);
        iv_casilla_6_1.setImageResource(tablero[6][1]);
        iv_casilla_6_2.setImageResource(tablero[6][2]);
        iv_casilla_6_3.setImageResource(tablero[6][3]);
        iv_casilla_6_4.setImageResource(tablero[6][4]);
        iv_casilla_6_5.setImageResource(tablero[6][5]);
    }

    /**
     * Realiza el desplazamiento de la cabeza que se muestra durante la habilidad especial del
     * contrincante, a la posición del tablero deseada y en la dirección deseada, para ello
     * convierte la posición de la columna especificada, a su distancia en pixeles desde la
     * izquierda de la pantalla
     *
     * @param posicionTablero Representa la columna donde desea moverse la cabeza
     * @param direccion Representa la direccion en la que se moverá la cabeza (true = derecha,
     *                  false = izquierda)
     */
    private void moverCabezaVertical(int posicionTablero, boolean direccion){
        float distancia = 0;
        switch (posicionTablero){
            case 0:
                distancia = 0;
                break;
            case 1:
                distancia = 120;
                break;
            case 2:
                distancia = 220;
                break;
            case 3:
                distancia = 370;
                break;
            case 4:
                distancia = 500;
                break;
            case 5:
                distancia = 630;
                break;
            case 6:
                distancia = 750;
                break;
        }

        if(direccion){ //true = de izquierda a derecha, false = derecha a izquierda
            ObjectAnimator animation = ObjectAnimator.ofFloat(cabezaVertical,
                    "translationX", distancia);
            animation.setDuration(500);
            animation.start();
        }else{
            ObjectAnimator animation = ObjectAnimator.ofFloat(cabezaVertical,
                    "translationX", -distancia);
            animation.setDuration(500);
            animation.start();
        }
    }

    /**
     * Determina si el usuario o el contricante han conectado con éxito 4 fichas de sus tipos
     * correspondientes horizontal o verticalmente. En caso afirmativo termina la partida
     */
    private void comprobarVictoria(){
        int combo;
        int[] casillas = new int[8];

        boolean finDelJuego = false;

        //Victoria jugador vertical
        for (int i = 0; i <= 6; i++){
            combo = 0;
            for (int j = 5; j >= 0; j--){
                if(tablero[i][j] == R.drawable.corazon){
                    casillas[combo] = i;
                    casillas[combo + 1] = j;
                    combo += 2;
                }else if(tablero[i][j] == R.drawable.hueso){
                    combo = 0;
                }
                if(combo == 8){
                    finDelJuego = true;
                    finDeLaPartida(true, casillas);
                }
            }
        }

        //Victoria jugador horizontal
        if(!finDelJuego){
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 0; j <= 6; j++){
                    if(tablero[j][i] == R.drawable.corazon){
                        casillas[combo] = j;
                        casillas[combo + 1] = i;
                        combo += 2;
                    }else if (tablero[j][i] == R.drawable.hueso){
                        combo = 0;
                    }
                    if(combo == 8) {
                        finDelJuego = true;
                        finDeLaPartida(true, casillas);
                    }
                }
            }
        }

        //Victoria sans vertical
        for (int i = 0; i <= 6; i++){
            combo = 0;
            for (int j = 5; j >= 0; j--){
                if(tablero[i][j] == R.drawable.hueso){
                    casillas[combo] = i;
                    casillas[combo + 1] = j;
                    combo += 2;
                }else if(tablero[i][j] == R.drawable.corazon){
                    combo = 0;
                }
                if(combo == 8){
                    finDelJuego = true;
                    finDeLaPartida(false, casillas);
                }
            }
        }

        //Victoria sans horizontal
        if(!finDelJuego){
            for(int i = 5; i >= 0; i--){
                combo = 0;
                for(int j = 0; j <= 6; j++){
                    if(tablero[j][i] == R.drawable.hueso){
                        casillas[combo] = j;
                        casillas[combo + 1] = i;
                        combo += 2;
                    }else if (tablero[j][i] == R.drawable.corazon){
                        combo = 0;
                    }
                    if(combo == 8) {
                        finDeLaPartida(false, casillas);
                    }
                }
            }
        }
    }

    /**
     * Elimina todas las fichas del tablero a excepción de las fichas que activaron la condición de
     * victoria, detiene la música, y muestra un diálogo del contricante que varia dependiendo
     * de si se ha ganado o perdido la partida.
     * Después, dirige al usuario a la pantalla de fin de partida
     *
     * @param victoria
     * @param casillas
     */
    private void finDeLaPartida(boolean victoria, int[] casillas){ //true = victoria jugador, false = victoria sans
        for (int i = 0; i <= 6; i++){
            for (int j = 0; j <= 5; j++){
                if ((i != casillas[0] || j != casillas[1]) && (i != casillas[2] || j != casillas[3])
                        && (i != casillas[4] || j != casillas[5]) && (i != casillas[6] || j != casillas[7])) {
                            tablero[i][j] = R.drawable.vacio;
                        }
                actualizarTablero();
            }
        }
        playerMusica.stop();
        charla = MediaPlayer.create(this, R.raw.sans_noise);
        charla.setVolume(1, 1);
        charla.start();
        if(victoria){
            dialogo.setImageResource(R.drawable.dialogo_victoria);
            new CountDownTimer(3000, 1000) {
                public void onTick(long millisUntilFinished) {}
                public void onFinish() {
                    cargarActivityVictoria(true);
                }
            }.start();
        }else{
            dialogo.setImageResource(R.drawable.dialogo_derrota);
            new CountDownTimer(3000, 1000) {
                public void onTick(long millisUntilFinished) {}
                public void onFinish() {
                    cargarActivityVictoria(false);
                }
            }.start();
        }
    }

    /**
     * Dirige al usuario a una u otra pantalla de fin de partida dependiendo de si se ha ganado
     * o perdido, dato que se obtiene através de un booleano
     *
     * @param victoria Representa el estado de la partida (true = victoria, false = derrota)
     */
    private void cargarActivityVictoria(boolean victoria){
        if(victoria){
            startActivity(new Intent(this, VictoriaActivity.class));
        }else{
            startActivity(new Intent(this, DerrotaActivity.class));
        }
    }
}

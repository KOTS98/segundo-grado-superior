package com.apalacios.util_idades;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.apalacios.util_idades.calculadora.CalculadoraActivity;
import com.apalacios.util_idades.conectacuatro.ConectaCuatroActivity;
import com.apalacios.util_idades.listacompra.ListaCompraActivity;
import com.apalacios.util_idades.parquimetros.ParquimetrosActivity;

/**
 * Actividad principal de la aplicación, posee un menú superior con diferentes opciones y una serie
 * de botones destinados a cargar las diferentes funciones de la aplicación.
 *
 * @author José Adrián Palacios Romo
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    /**
     * Método que se ejecuta al iniciar la Activity, crea los botones y les añade un listener.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("prefTemaOscuro", false)){
            setTheme(R.style.AppTheme);
        }else{
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_main);
        ImageButton iBtnCalculadora = findViewById(R.id.iBtnCalculadora);
        iBtnCalculadora.setOnClickListener(this);
        ImageButton iBtnListaCompra = findViewById(R.id.iBtnListaCompra);
        iBtnListaCompra.setOnClickListener(this);
        ImageButton iBtnConectaCuatro = findViewById(R.id.iBtnConectaCuatro);
        iBtnConectaCuatro.setOnClickListener(this);
        ImageButton iBtnParquimetros = findViewById(R.id.iBtnParquimetros);
        iBtnParquimetros.setOnClickListener(this);
    }

    /**
     * Se ejecuta al crearse el menú de opciones y lo infla.
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    /**
     * Método que detecta cuándo el usuario toca alguno de los botones, y responde en consecuencia
     * cargando la Activity correspondiente.
     * @param v Representa la vista principal
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iBtnCalculadora:
                startActivity(new Intent(this, CalculadoraActivity.class));
                break;
            case R.id.iBtnListaCompra:
                startActivity(new Intent(this, ListaCompraActivity.class));
                break;
            case R.id.iBtnParquimetros:
                startActivity(new Intent(this, ParquimetrosActivity.class));
                break;
            case R.id.iBtnConectaCuatro:
                startActivity(new Intent(this, ConectaCuatroActivity.class));
                break;
        }
    }

    /**
     * Método que se ejecuta al crear el menú contextual
     *
     * @param menu Representa el menu contextual
     * @param v Representa la vista principal
     * @param menuInfo Representa el menú de información
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    /**
     * Detecta cuándo se pulsa un botón del menú, y realiza la acción consecuente
     *
     * @param item Representa el objeto del menú
     * @return Devuelve el objeto seleccionado por el usuario
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_preferencias:
                startActivity(new Intent(this, PreferencesActivity.class));
                return true;

            case R.id.menu_acercade:
                Toast.makeText(this, "Aplicación desarrollada por KOTS98",
                        Toast.LENGTH_LONG).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

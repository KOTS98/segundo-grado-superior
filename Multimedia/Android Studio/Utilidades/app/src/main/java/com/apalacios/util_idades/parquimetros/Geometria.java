package com.apalacios.util_idades.parquimetros;

public class Geometria {
    private String tipo;
    private Coordenadas coordenadas;

    public Geometria(String tipo, Coordenadas coordenadas) {
        this.tipo = tipo;
        this.coordenadas = coordenadas;
    }

    public Coordenadas getCoordenadas() {
        return coordenadas;
    }
}

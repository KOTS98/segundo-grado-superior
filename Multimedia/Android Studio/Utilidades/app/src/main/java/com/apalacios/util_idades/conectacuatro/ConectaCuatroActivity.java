package com.apalacios.util_idades.conectacuatro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.apalacios.util_idades.R;

import pl.droidsonroids.gif.GifImageView;

/**
 * Activity que actúa como lanzador del juego Conecta Cuatro.
 *
 * @author José Adrián Palacios Romo
 */
public class ConectaCuatroActivity extends AppCompatActivity implements View.OnClickListener {

    private GifImageView sans;

    /**
     * Se ejecuta al iniciar la Activity, se encarga de crear el botón para iniciar partida y dotarle
     * de su respectivo componente gráfico, así como establecerle un listener
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager
                .LayoutParams.FLAG_FULLSCREEN);
        this.setContentView(R.layout.activity_conecta_cuatro);

        sans = findViewById(R.id.gIVInicio);
        Button nuevaPartida = findViewById(R.id.btnNuevaPartida);
        nuevaPartida.setOnClickListener(this);
    }

    /**
     * Detecta cuándo el usuario ha pulsado el botón de nueva partida y carga la Activity consecuente
     * @param v Representa el objeto pulsado en la vista
     */
    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnNuevaPartida){
            sans.setImageResource(R.drawable.sans_ataque);

            new CountDownTimer(600, 1000) {
                public void onTick(long millisUntilFinished) {}

                public void onFinish() {
                    iniciarJuego();
                }
            }.start();
        }
    }

    /**
     * Inicia la activity del juego
     */
    private void iniciarJuego(){
        startActivity(new Intent(this, JuegoActivity.class));
    }
}

package com.apalacios.util_idades;

import android.os.Bundle;
import android.preference.PreferenceActivity;

@SuppressWarnings("ALL")
/**
 * Actividad destinada a seleccionar las preferencias de configuración
 *
 * @author José Adrián Palacios Romo
 */
public class PreferencesActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferencias);
    }
}

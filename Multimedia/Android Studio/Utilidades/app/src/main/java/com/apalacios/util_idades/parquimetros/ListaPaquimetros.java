package com.apalacios.util_idades.parquimetros;

import java.util.ArrayList;

public class ListaPaquimetros {
    private int conteoTotal;
    private int inicio;
    private int filas;
    private ArrayList<Parquimetro> parquimetros;

    public ListaPaquimetros(int conteoTotal, int inicio, int filas, ArrayList<Parquimetro> parquimetros) {
        this.conteoTotal = conteoTotal;
        this.inicio = inicio;
        this.filas = filas;
        this.parquimetros = parquimetros;
    }

    public ArrayList<Parquimetro> getParquimetros() {
        return parquimetros;
    }
}

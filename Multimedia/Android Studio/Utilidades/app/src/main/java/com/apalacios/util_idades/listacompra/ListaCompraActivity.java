package com.apalacios.util_idades.listacompra;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.apalacios.util_idades.R;
import com.apalacios.util_idades.listacompra.db.Database;

import java.util.ArrayList;

public class ListaCompraActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemLongClickListener {

    private ProductoAdapter adaptador;
    private ArrayList<Producto> listaProductos;
    private Producto productoSeleccionado;
    private Database db;
    private ListView lvLista;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("prefTemaOscuro", false)){
            setTheme(R.style.AppTheme);
        }else{
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_lista_compra);
        Button btnAnyadir = findViewById(R.id.btnAnyadir);
        btnAnyadir.setOnClickListener(this);
        db = new Database(this);
        listaProductos = new ArrayList<>();
        lvLista = findViewById(R.id.listListaProductos);
        adaptador = new ProductoAdapter(this, listaProductos);
        lvLista.setAdapter(adaptador);
        lvLista.setOnItemLongClickListener(this);
        registerForContextMenu(lvLista);
        onRestart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (!prefs.getBoolean("OrdenarPorNombre", false)){
            listaProductos = db.getProductosID();
        }else{
            listaProductos = db.getProductosNombre();
        }
        adaptador = new ProductoAdapter(this, listaProductos);
        lvLista.setAdapter(adaptador);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnAnyadir){
            startActivity(new Intent(this, AddActivity.class));
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.list_menu, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.list_delete, menu);
        return true;
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        db.eliminarProducto(productoSeleccionado);
        onRestart();
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        productoSeleccionado = listaProductos.get(position);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        db.eliminarTodo(listaProductos);
        onRestart();
        return false;
    }
}

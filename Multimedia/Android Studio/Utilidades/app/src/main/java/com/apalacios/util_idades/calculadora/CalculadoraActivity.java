package com.apalacios.util_idades.calculadora;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.apalacios.util_idades.R;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Activity que permite al usuario realizar operaciones básicas de calculadora
 * Posee dos componentes graficos de tipo TextView para visualizar la información en pantalla
 *
 * @author José Adrián Palacios Romo
 */
public class CalculadoraActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView lblDisplay;
    private TextView lblOperacionPrevia;
    private String operandoActual = "";
    private boolean operacionTerminada;
    private boolean operandoEscrito;
    private boolean decimalNum1;
    private boolean decimalNum2;
    private float num1 = 0;
    private float num2 = 0;
    private float memoria = 0;

    /**
     * Se encarga de crear todos los botones y especificar a que componente gráfico están asociados.
     * También inicializa las variables para que el usuario pueda comenzar a usar la calculadora
     * desde cero
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if(!prefs.getBoolean("prefTemaOscuro", false)){
            setTheme(R.style.AppTheme);
        }else{
            setTheme(R.style.DarkTheme);
        }
        setContentView(R.layout.activity_calculadora);

        lblDisplay = findViewById(R.id.lblDisplay);
        lblOperacionPrevia = findViewById(R.id.lblOperacionPrevia);
        operandoEscrito = false;
        operacionTerminada = false;
        HashSet<Button> botones = new HashSet<>();
        Button btn0 = findViewById(R.id.btn0);
        botones.add(btn0);
        Button btn1 = findViewById(R.id.btn1);
        botones.add(btn1);
        Button btn2 = findViewById(R.id.btn2);
        botones.add(btn2);
        Button btn3 = findViewById(R.id.btn3);
        botones.add(btn3);
        Button btn4 = findViewById(R.id.btn4);
        botones.add(btn4);
        Button btn5 = findViewById(R.id.btn5);
        botones.add(btn5);
        Button btn6 = findViewById(R.id.btn6);
        botones.add(btn6);
        Button btn7 = findViewById(R.id.btn7);
        botones.add(btn7);
        Button btn8 = findViewById(R.id.btn8);
        botones.add(btn8);
        Button btn9 = findViewById(R.id.btn9);
        botones.add(btn9);
        Button btnSum = findViewById(R.id.btnSum);
        botones.add(btnSum);
        Button btnRes = findViewById(R.id.btnRes);
        botones.add(btnRes);
        Button btnMult = findViewById(R.id.btnMult);
        botones.add(btnMult);
        Button btnDiv = findViewById(R.id.btnDiv);
        botones.add(btnDiv);
        Button btnComa = findViewById(R.id.btnComa);
        botones.add(btnComa);
        Button btnC = findViewById(R.id.btnC);
        botones.add(btnC);
        Button btnCE = findViewById(R.id.btnCE);
        botones.add(btnCE);
        Button btnRetroceso = findViewById(R.id.btnRetroceso);
        botones.add(btnRetroceso);
        Button btnIgual = findViewById(R.id.btnIgual);
        botones.add(btnIgual);
        Button btnMC = findViewById(R.id.btnMC);
        botones.add(btnMC);
        Button btnMR = findViewById(R.id.btnMR);
        botones.add(btnMR);
        Button btnMSum = findViewById(R.id.btnMSum);
        botones.add(btnMSum);
        Button btnMRes = findViewById(R.id.btnMRes);
        botones.add(btnMRes);
        for (Button boton : botones) {
            boton.setOnClickListener(this);
        }
    }

    /**
     * Detecta cuándo el usuario ha pulsado un botón y responde en consecuencia añadiendo al display
     * el número seleccionado por el usuario, también limpia el display si detecta un error de
     * sintaxis o que el valor en pantalla es el resultado de la operación anterior
     *
     * @param v Representa la vista principal
     */
    @Override
    public void onClick(View v) {
        if(lblDisplay.getText().toString().equalsIgnoreCase("Syntax error")){
            limpiar();
        }
        if(operacionTerminada){
            lblOperacionPrevia.setText(lblDisplay.getText().toString());
            if(v.getId() == R.id.btnSum || v.getId() == R.id.btnRes ||
                    v.getId() == R.id.btnMult || v.getId() == R.id.btnDiv) {
                obtenerResultadoAnterior();
            }else if(v.getId() == R.id.btnMSum) {
                sumarMemoria();
            }else if(v.getId() == R.id.btnMRes){
                restarMemoria();
            }else{
                lblOperacionPrevia.setText(lblDisplay.getText().toString());
                operacionTerminada = false;
                limpiar();
            }
        }
        switch (v.getId()){
            case R.id.btn0:
                escribirNumero("0");
                break;

            case R.id.btn1:
                escribirNumero("1");
                break;

            case R.id.btn2:
                escribirNumero("2");
                break;

            case R.id.btn3:
                escribirNumero("3");
                break;

            case R.id.btn4:
                escribirNumero("4");
                break;

            case R.id.btn5:
                escribirNumero("5");
                break;

            case R.id.btn6:
                escribirNumero("6");
                break;

            case R.id.btn7:
                escribirNumero("7");
                break;

            case R.id.btn8:
                escribirNumero("8");
                break;

            case R.id.btn9:
                escribirNumero("9");
                break;

            case R.id.btnSum:
                escribirOperando("+");
                break;

            case R.id.btnRes:
                escribirOperando("-");
                break;

            case R.id.btnMult:
                escribirOperando("x");
                break;

            case R.id.btnDiv:
                escribirOperando("÷");
                break;

            case R.id.btnComa:
                escribirComa();
                break;

            case R.id.btnC:
                limpiar();
                break;

            case R.id.btnCE:
                lblOperacionPrevia.setText("");
                limpiar();
                break;

            case R.id.btnRetroceso:
                eliminarUltimo();
                break;

            case R.id.btnIgual:
                obtenerResultado();
                break;

            case R.id.btnMC:
                memoria = 0;
                break;

            case R.id.btnMR:
                escribirContenidoMemoria();
                break;
        }
    }

    /**
     * Escribe en pantalla el número seleccionado por el usuario, después, analiza la información
     * en pantalla para determinar los diferentes números con los que se va a operar y el tipo de
     * operando deseado. También se encarga de diferenciar cuándo el símbolo de menos ( - ) se
     * utiliza para hacer negativo a un número, o como operando
     * @param numero
     */
    private void escribirNumero(String numero){
        if (!operandoEscrito){
            if(lblDisplay.getText().toString().equalsIgnoreCase("0")){
                lblDisplay.setText(numero);
                num1 = Float.valueOf(numero);
            }else{
                lblDisplay.append(numero);
                num1 = Float.valueOf(lblDisplay.getText().toString());
            }
        }else{
            lblDisplay.append(numero);
            boolean operandoEncontrado = false;
            String[] caracteres = lblDisplay.getText().toString().split("");
            ArrayList<String> caracteresNum2 = new ArrayList<>();
            for (int i = 0; i < caracteres.length; i++){
                if (caracteres[i].equalsIgnoreCase("+") ||
                        caracteres[i].equalsIgnoreCase("x") ||
                        caracteres[i].equalsIgnoreCase("÷")){
                    operandoEncontrado = true;
                }
                if (caracteres[i].equalsIgnoreCase("-") && i > 1){
                    operandoEncontrado = true;
                }
                if (operandoEncontrado){
                    caracteresNum2.add(caracteres[i]);
                }
            }
            caracteresNum2.remove(0);
            StringBuilder strNum2 = new StringBuilder();
            for (String caracter : caracteresNum2) {
                strNum2.append(caracter);
            }
            num2 = Float.valueOf(strNum2.toString());
        }
    }

    /**
     * Escribe en pantalla el operando seleccionado por el usuario en caso de que no haya ya un operando
     * escrito. También se encarga de discernir si en caso de que el operando seleccionado sea el
     * simbolo de menos ( - ), se trata de un número negativo o un operando
     * @param operando
     */
    private void escribirOperando(String operando){
        if(!operandoEscrito){
            if (operando.equalsIgnoreCase("-")){
                if(num1 == 0){
                    lblDisplay.setText("-");
                }else{
                    comprobarComa();
                    lblDisplay.append("-");
                    operandoActual = operando;
                    operandoEscrito = true;
                }
            }else{
                if (!lblDisplay.getText().toString().equalsIgnoreCase("-")){
                    comprobarComa();
                    lblDisplay.append(operando);
                    operandoActual = operando;
                    operandoEscrito = true;
                }
            }
        }else{
            if(num2 == 0){
                lblDisplay.append("-");
            }
        }
    }

    /**
     * Escribe una coma (o en este caso un punto) que dota al número actual de parte decimal.
     * Para ello primero determina si se trata del primer o segundo número de la operación y
     * si este número posee o no una parte decimal, evitando así errores de sintaxis
     */
    private void escribirComa(){
        if(!operandoEscrito){
            if(!decimalNum1){
                decimalNum1 = true;
                lblDisplay.append(".");
            }
        }else{
            if(!decimalNum2){
                decimalNum2 = true;
                if(num2 != 0){
                    lblDisplay.append(".");
                }
            }
        }
    }

    /**
     * Comprueba que la coma que representa la parte decimal de un número no este escrita junto
     * al operando, evitando errores de sintaxis
     */
    private void comprobarComa(){
        String[] caracteres = lblDisplay.getText().toString().split("");
        if (caracteres[caracteres.length - 1].equalsIgnoreCase(".")){
            StringBuilder strDisplay = new StringBuilder();
            for (int i = 0; i < caracteres.length - 1; i++){
                strDisplay.append(caracteres[i]);
            }
            lblDisplay.setText(strDisplay.toString());
        }
    }

    /**
     * Reinicia todas las variables y establece en "0" el contenido del display, permitiendo iniciar
     * una nueva operación
     */
    private void limpiar(){
        operandoEscrito = false;
        operandoActual = "";
        decimalNum1 = false;
        decimalNum2 = false;
        num1 = 0;
        num2 = 0;
        operacionTerminada = false;
        lblDisplay.setText("0");
    }

    /**
     * Elimina el último caracter introducido por el usuario, y actualiza las variables afectadas
     * en consecuencia
     */
    private void eliminarUltimo(){
        String[] caracteres = lblDisplay.getText().toString().split("");
        switch(caracteres[caracteres.length - 1]){
            case "+":
            case "-":
            case "x":
            case "÷":
                operandoActual = "";
                operandoEscrito = false;
                break;
            case ".":
                if (!operandoEscrito){
                    decimalNum1 = false;
                }else{
                    decimalNum2 = false;
                }
                break;
            default:
                if (!operandoEscrito){
                    String[] caracteresNum1 = String.valueOf(num1).split("");
                    StringBuilder strNum1 = new StringBuilder();
                    for (int i = 0; i < caracteresNum1.length - 1; i++){
                        strNum1.append(caracteresNum1[i]);
                    }
                    num1 = Float.valueOf(strNum1.toString());
                }else{
                    String[] caracteresNum2 = String.valueOf(num2).split("");
                    StringBuilder strNum2 = new StringBuilder();
                    for (int i = 0; i < caracteresNum2.length - 1; i++){
                        strNum2.append(caracteresNum2[i]);
                    }
                    num1 = Float.valueOf(strNum2.toString());
                }
                break;
        }
        StringBuilder strDisplay = new StringBuilder();
        for (int i = 0; i < caracteres.length - 1; i++){
            strDisplay.append(caracteres[i]);
        }
        lblDisplay.setText(strDisplay.toString());
        if (lblDisplay.getText().toString().equalsIgnoreCase("")){
            lblDisplay.setText("0");
        }
    }

    /**
     * Analiza el contenido del display principal y realiza la operación matemática correspondiente.
     * Actualiza en el display principal el resultado de dicha operación.
     * Controla diferentes errores como dividir un número entre 0, o dividir 0 entre 0.
     */
    private void obtenerResultado(){
        if (operandoEscrito){
            String resultado = "";
            switch (operandoActual){
                case "+":
                    resultado = String.valueOf(num1 + num2);
                    break;
                case "-":
                    resultado = String.valueOf(num1 - num2);
                    break;
                case "x":
                    resultado = String.valueOf(num1 * num2);
                    break;
                case "÷":
                    if(num1 == 0 && num2 ==0){
                        resultado = "Syntax error";
                    }else if(num1 != 0 && num2 == 0){
                        resultado = "Syntax error";
                    }else{
                        resultado = String.valueOf(num1 / num2);
                    }
                    break;
            }
            if(!resultado.equalsIgnoreCase("Syntax error")){
                float flResultado = Float.valueOf(resultado);
                if (flResultado %1 == 0){
                    int intResultado = (int) flResultado;
                    resultado = String.valueOf(intResultado);
                }
            }
            lblDisplay.setText(resultado);
            operacionTerminada = true;
        }
    }

    /**
     * Carga en el display principal el resultado de la última operación realizada por el usuario y
     * actualiza las variables para iniciar una nueva operación, en la que el primer número es dicho
     * resultado
     */
    private void obtenerResultadoAnterior(){
        if(!lblOperacionPrevia.getText().toString().equalsIgnoreCase("0")){
            num1 = Float.valueOf(lblDisplay.getText().toString());
            if (lblDisplay.getText().toString().contains(".")){
                decimalNum1 = true;
            }
            num2 = 0;
            operacionTerminada = false;
            operandoActual = "";
            operandoEscrito = false;
            decimalNum2 = false;
            if (num1 %1 == 0){
                int intNum1 = (int) num1;
                lblDisplay.setText(String.valueOf(intNum1));
            }else{
                lblDisplay.setText(String.valueOf(num1));
            }
        }
    }

    /**
     * Concatena al display principal el contenido de la memoria
     */
    private void escribirContenidoMemoria(){
        if (!operandoEscrito){
            num1 = memoria;
            if (num1 %1 == 0){
                int intNum1 = (int) num1;
                lblDisplay.setText(String.valueOf(intNum1));
            }else{
                lblDisplay.setText(String.valueOf(num1));
            }
            if (String.valueOf(memoria).contains(".")){
                decimalNum1 = true;
            }
        }else{
            int longitudDisplay = lblDisplay.getText().toString().length();
            int longitudOperacion = String.valueOf(num1).length() + 1;
            if (longitudDisplay == longitudOperacion){
                num2 = memoria;
                if (num2 %1 == 0){
                    int intNum2 = (int) num2;
                    lblDisplay.setText(String.valueOf(intNum2));
                }else{
                    lblDisplay.setText(String.valueOf(num2));
                }
                if (String.valueOf(memoria).contains(".")){
                    decimalNum2 = true;
                }
            }
        }
    }

    /**
     * Suma el resultado de la operación actual al valor contenido en la memoria
     */
    private void sumarMemoria(){
        if(operacionTerminada){
            memoria += Float.valueOf(lblDisplay.getText().toString());
        }
    }

    /**
     * Resta el resultado de la operación actual al valor contenido en la memoria
     */
    private void restarMemoria(){
        if(operacionTerminada){
            memoria -= Float.valueOf(lblDisplay.getText().toString());
        }
    }
}

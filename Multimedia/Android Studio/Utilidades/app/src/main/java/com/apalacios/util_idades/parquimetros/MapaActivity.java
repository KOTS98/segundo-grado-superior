package com.apalacios.util_idades.parquimetros;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.apalacios.util_idades.R;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import java.util.ArrayList;

public class MapaActivity extends AppCompatActivity {
    private MapView mapa;
    private ArrayList<Parquimetro> parquimetros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1Ijoia290czk4IiwiYSI6ImNrMzQ0dWlsYTB5cWYzY28yY2drNjVieTUifQ.JWAYRC1SrNQmc839nUA2xw");
        setContentView(R.layout.activity_mapa);
        parquimetros = Constantes.listaParquimetros;

        mapa = findViewById(R.id.mapView);
        mapa.onCreate(savedInstanceState);
        pintarPuntos();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapa.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapa.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapa.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapa.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapa.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapa.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapa.onSaveInstanceState(outState);
    }

    private void pintarPuntos(){
        mapa.getMapAsync(new OnMapReadyCallback() {
            double latitud;
            double longitud;

            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                for(Parquimetro parquimetro : parquimetros){
                    latitud = Double.valueOf(parquimetro.getGeometria().getCoordenadas().getLatitud());
                    longitud = Double.valueOf(parquimetro.getGeometria().getCoordenadas().getLongitud());

                    mapboxMap.addMarker(new MarkerOptions()
                            .position(new LatLng(longitud, latitud))
                            .title("Id: " + parquimetro.getId())
                            .snippet(parquimetro.getDireccion()));
                }
                CameraPosition posicion = new CameraPosition.Builder()
                        .target(new LatLng(longitud, latitud))
                        .zoom(17)
                        .tilt(30)
                        .build();
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(posicion), 7000);
            }
        });
    }
}

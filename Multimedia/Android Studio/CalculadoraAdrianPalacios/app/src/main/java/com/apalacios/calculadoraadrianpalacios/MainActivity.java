package com.apalacios.calculadoraadrianpalacios;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener{

    int operacion = 1; //(1- Sumar, 2- Restar, 3- Multiplicar, 4- Dividir)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RadioGroup grupo = findViewById(R.id.rbtnOperaciones);
        grupo.setOnCheckedChangeListener(this);
        Button btnCalcular = findViewById(R.id.btnCalcular);
        final TextView numero1 = findViewById(R.id.txtNumero1);
        final TextView numero2 = findViewById(R.id.txtNumero2);
        final TextView resultado = findViewById(R.id.txtResultado);
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float n1 = Integer.parseInt(numero1.getText().toString());
                float n2 = Integer.parseInt(numero2.getText().toString());
                switch(operacion){
                    case 1:
                        resultado.setText(Float.toString((n1 + n2)));
                        break;
                    case 2:
                        resultado.setText(Float.toString((n1 - n2)));
                        break;
                    case 3:
                        resultado.setText(Float.toString((n1 * n2)));
                        break;
                    case 4:
                        resultado.setText(Float.toString((n1 / n2)));
                        break;
                }
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch(checkedId){
            case R.id.rbtnSumar:
                operacion = 1;
                break;
            case R.id.rbtnRestar:
                operacion = 2;
                break;
            case R.id.rbtnMul:
                operacion = 3;
                break;
            case R.id.rbtnDiv:
               operacion = 4;
                break;
        }
    }
}

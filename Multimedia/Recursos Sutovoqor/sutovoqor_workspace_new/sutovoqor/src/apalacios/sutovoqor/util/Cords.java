package apalacios.sutovoqor.util;

public class Cords {
	private int cordX;
	private int cordY;
	
	public Cords(int cordX, int cordY) {
		this.cordX = cordX;
		this.cordY = cordY;
	}

	public int getCordX() {
		return cordX;
	}

	public void setCordX(int cordX) {
		this.cordX = cordX;
	}

	public int getCordY() {
		return cordY;
	}

	public void setCordY(int cordY) {
		this.cordY = cordY;
	}
	
	public void clear() {
		cordX = 0;
		cordY = 0;
	}
}

package apalacios.sutovoqor.util;

import com.badlogic.gdx.scenes.scene2d.Stage;

import apalacios.sutovoqor.characters.BaseActor;

public class Cursor extends BaseActor{
	private int position;
	
	public Cursor(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		setName("cursor");
		position = 1;
		
		setAnimation(loadAnimationFromSheet("assets/hud/cursor.png", 2, 1, 0.3f, true));
	}
	
	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public void act(float dt) {
		super.act(dt);
		switch (position) {
		case 1:
			centerAtPosition(220, 350);
			break;
		case 2:
			centerAtPosition(220, 280);
			break;
		case 3:
			centerAtPosition(220, 210);
			break;
		}
		
	}
}

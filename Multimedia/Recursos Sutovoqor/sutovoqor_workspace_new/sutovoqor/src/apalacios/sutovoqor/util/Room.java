package apalacios.sutovoqor.util;


import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.Stage;

import apalacios.sutovoqor.characters.Barrel;
import apalacios.sutovoqor.characters.BaseActor;
import apalacios.sutovoqor.characters.Chest;
import apalacios.sutovoqor.characters.Coin;
import apalacios.sutovoqor.characters.Hearth;
import apalacios.sutovoqor.characters.Item;
import apalacios.sutovoqor.characters.Key;
import apalacios.sutovoqor.characters.Rock;
import apalacios.sutovoqor.characters.Seller;
import apalacios.sutovoqor.characters.Trapdoor;
import apalacios.sutovoqor.enmies.Bat;
import apalacios.sutovoqor.enmies.BigSlime;
import apalacios.sutovoqor.enmies.Cuthulu;
import apalacios.sutovoqor.enmies.Enemy;
import apalacios.sutovoqor.enmies.Jimor;
import apalacios.sutovoqor.enmies.Orc;
import apalacios.sutovoqor.screens.LevelScreen;

public class Room {
	private Stage stage;
	private boolean enabled;
	private boolean visited;
	private BaseActor[][] components;
	private int level;
	private int room;
	public enum RoomType{
		NORMAL,
		SHOP,
		BOSS;
	}
	public RoomType roomType;
	
	public Room(Stage stage, int level, int room, RoomType roomType) {
		this.stage = stage;
		this.level = level;
		this.room = room;
		this.roomType = roomType;
		enabled = false;
		visited = false;
		components = new BaseActor[16][12];
	}
	
	public int getRoomNumber() {
		return room;
	}
	
	public void setVisited(boolean visited) {
		this.visited = visited;
	}
	
	public boolean isVisited() {
		return visited;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		if (enabled) {
			generateElements();
		}
	}
	
	public int[][] getComponents() {
		int[][] compMatrix = new int[16][12];
		for (int i = 0; i < 16; i++) {
			for (int j = 0; j < 12; j++) {
				try {
					switch (components[i][j].getName()) {
					case "rock":
					case "barrel":
					case "chest":
						compMatrix[i][j] = 99; // Objeto inamovible
						break;
					
					default:
						compMatrix[i][j] = 0; // Camino libre
						break;		
					}
				}catch(NullPointerException ex) {
					// Controla que el elemento no tenga nombre
					compMatrix[i][j] = 0; // Camino libre
				}
			}
		}
		return compMatrix;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	private void generateElements() {
		
		if (level == 1) {
			switch(room) {
			case 3:
				
				// Rocas esquina superior izquierda
				components[1][9] = new Rock(1 * 50, 9 * 50, stage);
				components[2][9] = new Rock(2 * 50, 9 * 50, stage);
				components[3][9] = new Rock(3 * 50, 9 * 50, stage);
				components[4][9] = new Rock(4 * 50, 9 * 50, stage);
				components[4][10] = new Rock(4 * 50, 10 * 50, stage);
				
				// Monedas esquina superior izquierda
				components[1][10] = new Coin(1 * 50, 10 * 50, stage);
				components[2][10] = new Coin(2 * 50, 10 * 50, stage);
				components[3][10] = new Coin(3 * 50, 10 * 50, stage);
					
				// Barriles esquina superior derecha
				components[13][10] = new Barrel(13 * 50, 10 * 50, stage, false);
				components[14][10] = new Barrel(14 * 50, 10 * 50, stage, true);
				
				// Rocas esquina inferior derecha
				components[11][1] = new Rock(11 * 50, 1 * 50, stage);
				components[11][2] = new Rock(11 * 50, 2 * 50, stage);
				components[12][2] = new Rock(12 * 50, 2 * 50, stage);
				components[13][2] = new Rock(13 * 50, 2 * 50, stage);
				components[14][2] = new Rock(14 * 50, 2 * 50, stage);
				
				// Monedas esquina inferior derecha
				components[12][1] = new Coin(12 * 50, 1 * 50, stage);
				components[13][1] = new Coin(13 * 50, 1 * 50, stage);
				components[14][1] = new Coin(14 * 50, 1 * 50, stage);
							
				// Barriles esquina inferior izquierda
				components[1][1] = new Barrel(1 * 50, 1 * 50, stage, true);
				components[2][1] = new Barrel(2 * 50, 1 * 50, stage, false);
				
				// Piedras centro
				components[6][7] = new Rock(6 * 50, 7 * 50, stage);
				components[7][7] = new Rock(7 * 50, 7 * 50, stage);
				components[8][7] = new Rock(8 * 50, 7 * 50, stage);
				components[9][7] = new Rock(9 * 50, 7 * 50, stage);
				components[6][5] = new Rock(6 * 50, 5 * 50, stage);
				components[6][6] = new Rock(6 * 50, 6 * 50, stage);
				components[9][5] = new Rock(9 * 50, 5 * 50, stage);
				components[9][6] = new Rock(9 * 50, 6 * 50, stage);
				components[6][4] = new Rock(6 * 50, 4 * 50, stage);
				components[7][4] = new Rock(7 * 50, 4 * 50, stage);
				components[8][4] = new Rock(8 * 50, 4 * 50, stage);
				components[9][4] = new Rock(9 * 50, 4 * 50, stage);
				
				// Cofre centro
				components[7][6] = new Chest(7 * 50, 6 * 50, stage);
				
				// Enemigos
				components[15][11] = new Orc(1 * 50, 8 * 50, stage);
				components[13][11] = new Orc(13 * 50, 3 * 50, stage);
				
				break;
				
			case 1:
				
				// Esquina superior izquierda
				components[3][9] = new Rock(3 * 50, 9 * 50, stage);
				components[4][9] = new Rock(4 * 50, 9 * 50, stage);
				components[2][8] = new Rock(2 * 50, 8 * 50, stage);
				components[2][7] = new Rock(2 * 50, 7 * 50, stage);
				components[2][9] = new Barrel(2 * 50, 9 * 50, stage, true);
				
				// Esquina superior derecha
				components[11][9] = new Rock(11 * 50, 9 * 50, stage);
				components[12][9] = new Rock(12 * 50, 9 * 50, stage);
				components[13][8] = new Rock(13 * 50, 8 * 50, stage);
				components[13][7] = new Rock(13 * 50, 7 * 50, stage);
				components[13][9] = new Barrel(13 * 50, 9 * 50, stage, false);
				
				// Esquina inferior izquierda
				components[3][2] = new Rock(3 * 50, 2 * 50, stage);
				components[4][2] = new Rock(4 * 50, 2 * 50, stage);
				components[2][4] = new Rock(2 * 50, 4 * 50, stage);
				components[2][3] = new Rock(2 * 50, 3 * 50, stage);
				components[2][2] = new Barrel(2 * 50, 2 * 50, stage, false);
				
				// Esquina inferior derecha
				components[11][2] = new Rock(11 * 50, 2 * 50, stage);
				components[12][2] = new Rock(12 * 50, 2 * 50, stage);
				components[13][4] = new Rock(13 * 50, 4 * 50, stage);
				components[13][3] = new Rock(13 * 50, 3 * 50, stage);
				components[13][2] = new Barrel(13 * 50, 2 * 50, stage, false);
				
				// Centro
				components[7][7] = new Rock(7 * 50, 7 * 50, stage);
				components[8][7] = new Rock(8 * 50, 7 * 50, stage);
				components[7][4] = new Rock(7 * 50, 4 * 50, stage);
				components[8][4] = new Rock(8 * 50, 4 * 50, stage);
				components[6][5] = new Rock(6 * 50, 5 * 50, stage);
				components[6][6] = new Rock(6 * 50, 6 * 50, stage);
				components[9][5] = new Rock(9 * 50, 5 * 50, stage);
				components[9][6] = new Rock(9 * 50, 6 * 50, stage);
				components[7][5] = new Coin(7 * 50, 5 * 50, stage);
				components[7][6] = new Coin(7 * 50, 6 * 50, stage);
				components[8][5] = new Coin(8 * 50, 5 * 50, stage);
				components[8][6] = new Coin(8 * 50, 6 * 50, stage);
				
				// Enemigos
				components[15][11] = new Orc(3 * 50, 8 * 50, stage);
				components[14][11] = new Orc(12 * 50, 8 * 50, stage);
				components[13][11] = new Orc(3 * 50, 3 * 50, stage);
				components[12][11] = new Orc(12 * 50, 3 * 50, stage);
				break;
				
			case 4:
				
				// Esquina superior izquierda
				components[1][10] = new Barrel(1 * 50, 10 * 50, stage, false);
				components[2][10] = new Barrel(2 * 50, 10 * 50, stage, false);
				
				// Esquina inferior derecha
				components[13][1] = new Barrel(13 * 50, 1 * 50, stage, false);
				components[14][1] = new Barrel(14 * 50, 1 * 50, stage, false);
				
				// Centro
				components[7][5] = new Barrel(7 * 50, 5 * 50, stage, false);
				components[8][5] = new Barrel(8 * 50, 5 * 50, stage, true);
				components[7][6] = new Barrel(7 * 50, 6 * 50, stage, true);
				components[8][6] = new Barrel(8 * 50, 6 * 50, stage, false);
				
				// Enemigos
				components[7][7] = new BigSlime(7 * 50, 7 * 50, stage);
				components[8][4] = new BigSlime(8 * 50, 4 * 50, stage);
				
				break;
				
			case 2:
				
				// Muro izquieda
				components[3][8] = new Rock(3 * 50, 8 * 50, stage);
				components[3][7] = new Rock(3 * 50, 7 * 50, stage);
				components[3][6] = new Rock(3 * 50, 6 * 50, stage);
				components[3][5] = new Rock(3 * 50, 5 * 50, stage);
				components[3][4] = new Rock(3 * 50, 4 * 50, stage);
				components[3][3] = new Rock(3 * 50, 3 * 50, stage);
				components[3][2] = new Rock(3 * 50, 2 * 50, stage);
				components[3][1] = new Rock(3 * 50, 1 * 50, stage);
				
				// Muro arriba
				components[4][8] = new Rock(4 * 50, 8 * 50, stage);
				components[5][8] = new Rock(5 * 50, 8 * 50, stage);
				components[6][8] = new Rock(6 * 50, 8 * 50, stage);
				components[7][8] = new Rock(7 * 50, 8 * 50, stage);
				components[8][8] = new Rock(8 * 50, 8 * 50, stage);
				components[9][8] = new Rock(9 * 50, 8 * 50, stage);
				
				// Muro derecha
				components[12][10] = new Rock(12 * 50, 10 * 50, stage);
				components[12][9] = new Rock(12 * 50, 9 * 50, stage);
				components[12][8] = new Rock(12 * 50, 8 * 50, stage);
				components[12][7] = new Rock(12 * 50, 7 * 50, stage);
				components[12][6] = new Rock(12 * 50, 6 * 50, stage);
				components[12][5] = new Rock(12 * 50, 5 * 50, stage);
				components[12][4] = new Rock(12 * 50, 4 * 50, stage);
				components[12][3] = new Rock(12 * 50, 3 * 50, stage);
							
				// Muro abajo
				components[6][3] = new Rock(6 * 50, 3 * 50, stage);
				components[7][3] = new Rock(7 * 50, 3 * 50, stage);
				components[8][3] = new Rock(8 * 50, 3 * 50, stage);
				components[9][3] = new Rock(9 * 50, 3 * 50, stage);
				components[10][3] = new Rock(10 * 50, 3 * 50, stage);
				components[11][3] = new Rock(11 * 50, 3 * 50, stage);
				
				// Enemigos
				components[7][5] = new BigSlime(7 * 50, 5 * 50, stage);
				components[7][6] = new BigSlime(7 * 50, 6 * 50, stage);
				break;
				
			case 5:
				// Esquina inferior izquierda
				components[1][1] = new Barrel(1 * 50, 1 * 50, stage, true);
				components[2][1] = new Barrel(2 * 50, 1 * 50, stage, false);
				components[1][2] = new Barrel(1 * 50, 2 * 50, stage, false);
				
				// Esquina superior derecha
				components[15][11] = new Barrel(15 * 50, 11 * 50, stage, true);
				components[14][11] = new Barrel(14 * 50, 11 * 50, stage, false);
				components[15][10] = new Barrel(15 * 50, 10 * 50, stage, false);
				
				// Esquina inferior derecha
				components[14][1] = new Key((14 * 50) + 25, 1 * 50, stage, false);
				components[14][2] = new Rock(14 * 50, 2 * 50, stage);
				components[13][1] = new Rock(13 * 50, 1 * 50, stage);
				components[12][1] = new Rock(12 * 50, 1 * 50, stage);
				components[13][2] = new Rock(13 * 50, 2 * 50, stage);
				components[14][3] = new Rock(14 * 50, 3 * 50, stage);
				components[11][1] = new Rock(11 * 50, 1 * 50, stage);
				components[12][2] = new Rock(12 * 50, 2 * 50, stage);
				components[13][3] = new Rock(13 * 50, 3 * 50, stage);
				components[14][4] = new Rock(14 * 50, 4 * 50, stage);
				components[10][1] = new Rock(10 * 50, 1 * 50, stage);
				components[11][2] = new Rock(11 * 50, 2 * 50, stage);
				components[12][3] = new Rock(12 * 50, 3 * 50, stage);
				
				// Esquina superior izquierda
				components[1][10] = new Hearth(1 * 50, (10 * 50) + 10, stage, false);
				components[2][10] = new Rock(2 * 50, 10 * 50, stage);
				components[1][9] = new Rock(1 * 50, 9 * 50, stage);
				components[1][8] = new Rock(1 * 50, 8 * 50, stage);
				components[2][9] = new Rock(2 * 50, 9 * 50, stage);
				components[3][10] = new Rock(3 * 50, 10 * 50, stage);
				components[1][7] = new Rock(1 * 50, 7 * 50, stage);
				components[2][8] = new Rock(2 * 50, 8 * 50, stage);
				components[3][9] = new Rock(3 * 50, 9 * 50, stage);
				components[4][10] = new Rock(4 * 50, 10 * 50, stage);
				components[2][8] = new Rock(2 * 50, 8 * 50, stage);
				components[4][9] = new Rock(4 * 50, 9 * 50, stage);
				components[5][10] = new Rock(5 * 50, 10 * 50, stage);
				
				// Enemigos 
				components[2][2] = new Orc(2 * 50, 2 * 50, stage);
				components[14][10] = new Orc(14 * 50, 10 * 50, stage);
				
				break;
				
			case 6: // Tienda
				components[7][7] = new Seller(370, 400, stage);
				components[5][6] = new Item((5 * 50) + 20, 6 * 50, LevelScreen.itemList.get(0), stage);
				LevelScreen.itemList.remove(0);
				components[7][6] = new Item((7 * 50) + 20, 6 * 50, LevelScreen.itemList.get(1), stage);
				LevelScreen.itemList.remove(1);
				
				if (new Random().nextInt(100) + 1 > 50) {
					components[9][6] = new Key((9 * 50) + 20, 6 * 50, stage, true);
				} else {
					components[9][6] = new Hearth((9 * 50) + 20, 6 * 50, stage, true);
				}
			break;
			case 7:// Sala boss
				// Puerta salida
				components[5][5] = new Trapdoor(375, 275, stage);
				
				// Boss
				components[6][5] = new Jimor(6 * 50, 5 * 50, stage);
				break;
			}
		} else if (level == 2) {
			switch (room) {
			case 1:
				// Esquina superior izquierda
				components[1][10] = new Rock(1 * 50, 10 * 50, stage);
				components[2][10] = new Rock(2 * 50, 10 * 50, stage);
				components[1][9] = new Rock(1 * 50, 9 * 50, stage);
				components[2][9] = new Rock(2 * 50, 9 * 50, stage);
				components[1][8] = new Rock(1 * 50, 8 * 50, stage);
				components[3][10] = new Rock(3 * 50, 10 * 50, stage);
				
				// Esquina superior derecha
				components[14][10] = new Rock(14 * 50, 10 * 50, stage);
				components[13][10] = new Rock(13 * 50, 10 * 50, stage);
				components[14][9] = new Rock(14 * 50, 9 * 50, stage);
				components[13][9] = new Rock(13 * 50, 9 * 50, stage);
				components[12][10] = new Rock(12 * 50, 10 * 50, stage);
				components[14][8] = new Rock(14 * 50, 8 * 50, stage);
				
				// Esquina inferior izquierda
				components[1][1] = new Rock(1 * 50, 1 * 50, stage);
				components[2][1] = new Rock(2 * 50, 1 * 50, stage);
				components[1][2] = new Rock(1 * 50, 2 * 50, stage);
				components[2][2] = new Rock(2 * 50, 2 * 50, stage);
				components[1][3] = new Rock(1 * 50, 3 * 50, stage);
				components[3][1] = new Rock(3 * 50, 1 * 50, stage);
				
				// Esquina inferior derecha
				components[14][1] = new Rock(14 * 50, 1 * 50, stage);
				components[13][1] = new Rock(13 * 50, 1 * 50, stage);
				components[14][2] = new Rock(14 * 50, 2 * 50, stage);
				components[13][2] = new Rock(13 * 50, 2 * 50, stage);
				components[14][3] = new Rock(14 * 50, 3 * 50, stage);
				components[12][1] = new Rock(12 * 50, 1 * 50, stage);
				
				// Centro
				components[7][7] = new Rock(7 * 50, 7 * 50, stage);
				components[8][7] = new Rock(8 * 50, 7 * 50, stage);
				components[7][4] = new Rock(7 * 50, 4 * 50, stage);
				components[8][4] = new Rock(8 * 50, 4 * 50, stage);
				components[6][5] = new Rock(6 * 50, 5 * 50, stage);
				components[6][6] = new Rock(6 * 50, 6 * 50, stage);
				components[9][5] = new Rock(9 * 50, 5 * 50, stage);
				components[9][6] = new Rock(9 * 50, 6 * 50, stage);
				
				// Enemigos
				components[15][11] = new Bat(7 * 50, 5 * 50, stage);
				components[14][11] = new Bat(8 * 50, 6 * 50, stage);
				components[6][7] = new Orc(6 * 50, 7 * 50, stage);
				components[9][4] = new Orc(9 * 50, 4 * 50, stage);				
				break;
			case 2:
				
				// Esquina superior izquierda
				components[7][7] = new Rock(7 * 50, 7 * 50, stage);
				components[6][7] = new Rock(6 * 50, 7 * 50, stage);
				components[7][8] = new Rock(7 * 50, 8 * 50, stage);
				components[6][8] = new Barrel(6 * 50, 8 * 50, stage, false);
				
				// Esquina superior derecha
				components[9][6] = new Rock(9 * 50, 6 * 50, stage);
				components[10][6] = new Rock(10 * 50, 6 * 50, stage);
				components[9][7] = new Rock(9 * 50, 7 * 50, stage);
				components[10][7] = new Barrel(10 * 50, 7 * 50, stage, false);
				
				// Esquina inferior izquierda
				components[6][5] = new Rock(6 * 50, 5 * 50, stage);
				components[5][5] = new Rock(5 * 50, 5 * 50, stage);
				components[6][4] = new Rock(6 * 50, 4 * 50, stage);
				components[5][4] = new Barrel(5 * 50, 4 * 50, stage, false);
				
				// Esquina inferior derecha
				components[8][4] = new Rock(8 * 50, 4 * 50, stage);
				components[9][4] = new Rock(9 * 50, 4 * 50, stage);
				components[8][3] = new Rock(8 * 50, 3 * 50, stage);
				components[9][3] = new Barrel(9 * 50, 3 * 50, stage, false);
				
				// Centro - Enemigos
				components[15][8] = new Bat(7 * 50, 5 * 50, stage);
				components[15][9] = new Bat(8 * 50, 5 * 50, stage);
				components[15][10] = new Bat(7 * 50, 6 * 50, stage);
				components[15][11] = new Bat(8 * 50, 6 * 50, stage);
				break;
				
			case 3:
				// Esquina superior izquierda
				components[1][8] = new Rock(1 * 50, 8 * 50, stage);
				components[2][8] = new Rock(2 * 50, 8 * 50, stage);
				components[3][8] = new Rock(3 * 50, 8 * 50, stage);
				components[4][8] = new Rock(4 * 50, 8 * 50, stage);
				components[5][8] = new Rock(5 * 50, 8 * 50, stage);
				components[6][8] = new Rock(6 * 50, 8 * 50, stage);
				components[6][9] = new Rock(6 * 50, 9 * 50, stage);
				components[6][10] = new Rock(6 * 50, 10 * 50, stage);
				components[1][9] = new Coin(1 * 50, 9 * 50, stage);
				components[2][10] = new Coin(2 * 50, 10 * 50, stage);
				components[3][9] = new Key((3 * 50) + 20, 9 * 50, stage, false);
				components[4][10] = new Coin(4 * 50, 10 * 50, stage);
				components[5][9] = new Coin(5 * 50, 9 * 50, stage);
				
				// Esquina inferior derecha
				components[10][3] = new Rock(10 * 50, 3 * 50, stage);
				components[11][3] = new Rock(11 * 50, 3 * 50, stage);
				components[12][3] = new Rock(12 * 50, 3 * 50, stage);
				components[13][3] = new Rock(13 * 50, 3 * 50, stage);
				components[14][3] = new Rock(14 * 50, 3 * 50, stage);
				components[10][2] = new Rock(10 * 50, 2 * 50, stage);
				components[10][1] = new Rock(10 * 50, 1 * 50, stage);
				components[12][2] = new Chest(12 * 50, 2 * 50, stage);
				
				// Centro
				components[7][5] = new Barrel(7 * 50, 5 * 50, stage, true);
				components[8][5] = new Rock(8 * 50, 5 * 50, stage);
				components[7][6] = new Rock(7 * 50, 6 * 50, stage);
				components[8][6] = new Barrel(8 * 50, 6 * 50, stage, false);
				
				// Centro Arriba derecha
				components[10][7] = new Rock(10 * 50, 7 * 50, stage);
				components[10][8] = new Rock(10 * 50, 8 * 50, stage);
				components[11][7] = new Rock(11 * 50, 7 * 50, stage);
				components[11][8] = new Orc(11 * 50, 8 * 50, stage);
				
				// Centro Abajo izquierda
				components[5][4] = new Rock(5 * 50, 4 * 50, stage);
				components[4][4] = new Rock(4 * 50, 4 * 50, stage);
				components[5][3] = new Rock(5 * 50, 3 * 50, stage);
				components[4][3] = new Orc(4 * 50, 3 * 50, stage);			
				break;
				
			case 4:
				
				// Esquina superior izquierda
				components[1][8] = new Rock(1 * 50, 8 * 50, stage);
				components[2][8] = new Rock(2 * 50, 8 * 50, stage);
				components[3][9] = new Rock(3 * 50, 9 * 50, stage);
				components[3][10] = new Rock(3 * 50, 10 * 50, stage);
				components[2][9] = new Coin(2 * 50, 9 * 50, stage);
				
				// Esquina superior derecha
				components[12][10] = new Rock(12 * 50, 10 * 50, stage);
				components[12][9] = new Rock(12 * 50, 9 * 50, stage);
				components[13][8] = new Rock(13 * 50, 8 * 50, stage);
				components[14][8] = new Rock(14 * 50, 8 * 50, stage);
				components[13][9] = new Key((13 * 50) + 20, (9 * 50) + 20, stage, false);
				
				// Esquina inferior izquierda
				components[1][3] = new Rock(1 * 50, 3 * 50, stage);
				components[2][3] = new Rock(2 * 50, 3 * 50, stage);
				components[3][2] = new Rock(3 * 50, 2 * 50, stage);
				components[3][1] = new Rock(3 * 50, 1 * 50, stage);
				components[2][2] = new Hearth(2 * 50, 2 * 50, stage, false);
				
				// Esquina inferior derecha
				components[14][3] = new Rock(14 * 50, 3 * 50, stage);
				components[13][3] = new Rock(13 * 50, 3 * 50, stage);
				components[12][2] = new Rock(12 * 50, 2 * 50, stage);
				components[12][1] = new Rock(12 * 50, 1 * 50, stage);
				components[13][2] = new Coin(13 * 50, 2 * 50, stage);
				
				// Centro
				components[7][6] = new Barrel(7 * 50, 6 * 50, stage, true);
				components[8][6] = new Orc(8 * 50, 6 * 50, stage);
				components[7][5] = new Orc(7 * 50, 5 * 50, stage);
				components[8][5] = new Orc(8 * 50, 5 * 50, stage);
				
				components[6][7] = new Barrel(6 * 50, 7 * 50, stage, false);
				components[7][7] = new Barrel(7 * 50, 7 * 50, stage, false);
				components[8][7] = new Barrel(8 * 50, 7 * 50, stage, false);
				components[7][8] = new Barrel(7 * 50, 8 * 50, stage, false);
				components[8][8] = new Barrel(8 * 50, 8 * 50, stage, false);
				components[9][7] = new Barrel(9 * 50, 7 * 50, stage, false);
				
				components[6][4] = new Barrel(6 * 50, 4 * 50, stage, false);
				components[7][4] = new Barrel(7 * 50, 4 * 50, stage, false);
				components[8][4] = new Barrel(8 * 50, 4 * 50, stage, false);
				components[7][3] = new Barrel(7 * 50, 3 * 50, stage, false);
				components[8][3] = new Barrel(8 * 50, 3 * 50, stage, false);
				components[9][4] = new Barrel(9 * 50, 4 * 50, stage, false);
				
				components[6][6] = new Barrel(6 * 50, 6 * 50, stage, false);
				components[6][5] = new Barrel(6 * 50, 5 * 50, stage, false);
				components[5][6] = new Barrel(5 * 50, 6 * 50, stage, false);
				components[5][5] = new Barrel(5 * 50, 5 * 50, stage, false);
				
				components[9][6] = new Barrel(9 * 50, 6 * 50, stage, false);
				components[9][5] = new Barrel(9 * 50, 5 * 50, stage, false);
				components[10][6] = new Barrel(10 * 50, 6 * 50, stage, false);
				components[10][5] = new Barrel(10 * 50, 5 * 50, stage, false);
				break;
				
			case 5:
				// Esquina superior izquierda
				components[1][10] = new Rock(1 * 50, 10 * 50, stage);
				components[2][9] = new Rock(2 * 50, 9 * 50, stage);
				components[3][8] = new Rock(3 * 50, 8 * 50, stage);
				
				// Esquina superior derecha
				components[14][10] = new Rock(14 * 50, 10 * 50, stage);
				components[13][9] = new Rock(13 * 50, 9 * 50, stage);
				components[12][8] = new Rock(12 * 50, 8 * 50, stage);
				
				// Esquina inferior izquierda
				components[1][1] = new Rock(1 * 50, 1 * 50, stage);
				components[2][2] = new Rock(2 * 50, 2 * 50, stage);
				components[3][3] = new Rock(3 * 50, 3 * 50, stage);
				
				// Esquina inferior derecha
				components[12][3] = new Rock(12 * 50, 3 * 50, stage);
				components[13][2] = new Rock(13 * 50, 2 * 50, stage);
				components[14][1] = new Rock(14 * 50, 1 * 50, stage);
				
				// Centro
				components[7][6] = new Orc(7 * 50, 6 * 50, stage);
				components[8][6] = new Orc(8 * 50, 6 * 50, stage);
				components[7][5] = new Orc(7 * 50, 5 * 50, stage);
				components[8][5] = new Orc(8 * 50, 5 * 50, stage);
				
				components[7][8] = new Rock(7 * 50, 8 * 50, stage);
				components[8][8] = new Rock(8 * 50, 8 * 50, stage);
				
				components[7][3] = new Rock(7 * 50, 3 * 50, stage);
				components[8][3] = new Rock(8 * 50, 3 * 50, stage);
				
				components[5][6] = new Rock(5 * 50, 6 * 50, stage);
				components[5][5] = new Rock(5 * 50, 5 * 50, stage);
				
				components[10][6] = new Rock(10 * 50, 6 * 50, stage);
				components[10][5] = new Rock(10 * 50, 5 * 50, stage);
				break;
			case 6:
				components[7][7] = new Seller(370, 400, stage);
				components[5][6] = new Item((5 * 50) + 20, 6 * 50, LevelScreen.itemList.get(0), stage);
				LevelScreen.itemList.remove(0);
				components[7][6] = new Item((7 * 50) + 20, 6 * 50, LevelScreen.itemList.get(1), stage);
				LevelScreen.itemList.remove(1);
				
				
				if (new Random().nextInt(100) + 1 > 50) {
					components[9][6] = new Key((9 * 50) + 20, 6 * 50, stage, true);
				} else {
					components[9][6] = new Hearth((9 * 50) + 20, 6 * 50, stage, true);
				}
				break;
			case 7:
				// Puerta salida
				components[5][5] = new Trapdoor(375, 275, stage);
				
				// Boss
				components[6][5] = new Cuthulu(6 * 50, 5 * 50, stage);
				break;
			}
		}
	}
	
	public void drawComponents(Stage stage) {
		for (int i = 0; i < 16; i++) {
			for (int j = 0; j < 12; j++) {
				if (!(components[i][j] == null)) {
					if (components[i][j].getName().equalsIgnoreCase("enemy")) {
						Enemy enemy = (Enemy) components[i][j];
						if (enemy.isAlive()) {
							stage.addActor(components[i][j]);
						}
					} else if (components[i][j].getName().equalsIgnoreCase("rock")) {
						Rock rock = (Rock) components[i][j];
						if (!rock.isDestroyed()) {
							stage.addActor(components[i][j]);
						}
					}else {
						stage.addActor(components[i][j]);
					}
				}
			}
		}
	}
}

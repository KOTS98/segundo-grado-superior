package apalacios.sutovoqor.screens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg.Sound;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import apalacios.sutovoqor.characters.Barrel;
import apalacios.sutovoqor.characters.BaseActor;
import apalacios.sutovoqor.characters.Chest;
import apalacios.sutovoqor.characters.Collectable;
import apalacios.sutovoqor.characters.Door;
import apalacios.sutovoqor.characters.Hearth;
import apalacios.sutovoqor.characters.Item;
import apalacios.sutovoqor.characters.Key;
import apalacios.sutovoqor.characters.Mobile;
import apalacios.sutovoqor.characters.Player;
import apalacios.sutovoqor.characters.Player.Weapon;
import apalacios.sutovoqor.characters.Proyectile;
import apalacios.sutovoqor.characters.Rock;
import apalacios.sutovoqor.characters.Solid;
import apalacios.sutovoqor.characters.Tooth;
import apalacios.sutovoqor.characters.Trapdoor;
import apalacios.sutovoqor.characters.VisualEffect;
import apalacios.sutovoqor.enmies.BabySlime;
import apalacios.sutovoqor.enmies.Bat;
import apalacios.sutovoqor.enmies.BigSlime;
import apalacios.sutovoqor.enmies.Cuthulu;
import apalacios.sutovoqor.enmies.Enemy;
import apalacios.sutovoqor.enmies.Jimor;
import apalacios.sutovoqor.enmies.Orc;
import apalacios.sutovoqor.game.BaseGame;
import apalacios.sutovoqor.game.SutovoqorGame;
import apalacios.sutovoqor.util.Cords;
import apalacios.sutovoqor.util.Room;
import apalacios.sutovoqor.util.Score;
import apalacios.sutovoqor.util.Room.RoomType;

/**
 * Representa la pantalla principal de juego
 * 
 * @author KOTS98
 */
public class LevelScreen extends BaseScreen {
	private Viewport viewport;
    private Camera camera;
	
	private Player player;
	private Room[][] level1;
	private Room[][] level2;
	private Room[][] level3;
	private Cords currentRoom;
	private int currentLevel;
	private BaseActor background;
	
	// Generaci�n procedural
	private Cords lastRoom;
	private Cords temp;
	
	// Pantalla de cambio de sala
	private BaseActor blackScreen;
	
	// Sonidos
	private Sound coinSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/moneda.ogg"));
	private Sound keySound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/llave.ogg"));
	private Sound hearthSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/corazon.ogg"));
	private Sound explosionSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/explosion.ogg"));
	private Sound openDoorsSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/abrir_puertas.ogg"));
	
	// M�sica
	private Music level1Music;
	private Music level2Music;
	private Music level3Music;
	private Music boss1Music;
	private Music boss2Music;
	private Music boss3Music;
	private Music deathMusic;
	
	// Puntuaci�n
	Label scoreLabel;
	long scoreStart;
	
	// Objetos
	public static ArrayList<Integer> itemList;
	
	// Items recogidos
	public boolean shieldOn;
	private BaseActor shield;
	
	/**
	 * Establece el fondo para la pantalla y las dimensiones de la misma
	 */
	@Override
	public void initialize() {
		camera = new PerspectiveCamera();
        viewport = new FitViewport(800, 600, camera);
		
		// Objetos
		itemList = new ArrayList<>();
		randomizeItems();
		
		// Crear las salas
		currentLevel = 1;
		setupRooms();
		createRooms(level3, 3);
		createRooms(level2, 2);
		createRooms(level1, 1);
		
		detectShop(level1);
		detectShop(level2);
		detectShop(level3);
		
		// Dar l�mite al mundo
		BaseActor.setWorldBounds(background);
		
		// Crear jugador
		player = new Player(350, 300, mainStage);
		player.setLife(6);
		player.setDamage(20);
		player.setShotSpeed(500);
		player.setEvadeRate(5);
		player.setCoins(0);
		player.setKeys(0);
		
		// Hud
		loadHudElements();
		
		// Configurar reproducci�n de m�sica
		level1Music = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/music/nivel1.mp3"));
		level2Music = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/music/nivel2.mp3"));
		level2Music.setVolume(0.5f);
		level3Music = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/music/nivel3.mp3"));
		level3Music.setVolume(0.8f);
		boss1Music = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/music/boss1.mp3"));
		boss1Music.setVolume(0.5f);
		boss2Music = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/music/boss2.mp3"));
		boss2Music.setVolume(0.5f);
		boss3Music = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/music/boss3.mp3"));
		deathMusic = (Music) Gdx.audio.newMusic(Gdx.files.internal("assets/music/muerte.mp3"));
		
		level1Music.setLooping(true);
		level2Music.setLooping(true);
		level3Music.setLooping(true);
		boss1Music.setLooping(true);
		boss2Music.setLooping(true);
		boss3Music.setLooping(true);
		deathMusic.setLooping(true);
		
		level1Music.setVolume(0.2f);
		level1Music.play();
		
		// Puntuaci�n
		scoreStart = System.currentTimeMillis();
		
		// Escudo
		shieldOn = false;
		shield = new BaseActor(player.getX(), player.getY(), mainStage);
		shield.loadTexture("assets/characters/items/escudo_personal_mk1.png");
		shield.setSize(70, 70);
		shield.remove();
	}
	
	/**
	 * Inicializa la matriz de habitaciones
	 */
	private void setupRooms() {
		level1 = new Room[5][5];
		level2 = new Room[5][5];
		level3 = new Room[5][5];
		for (int i = 0; i < 5; i++)
			for (int j = 0;  j < 5; j++) {
				level1[i][j] = new Room(mainStage, 1, 0, RoomType.NORMAL);
				level2[i][j] = new Room(mainStage, 2, 0, RoomType.NORMAL);
				level3[i][j] = new Room(mainStage, 3, 0, RoomType.NORMAL);
			}
	}
	
	public void createRooms(Room[][] level, int levelNumber) {
		currentRoom = new Cords(2, 2);
		level[2][2].setEnabled(true); // Habilitar la habitaci�n de inicio
		level[2][2].setVisited(true);
		
		int roomNumber = 0;
		try {
			temp = new Cords(2, 2);
			for (int i = 0; i < 3; i++) {
				roomNumber ++;
				lastRoom = generateRandomRoom(temp, level, levelNumber, roomNumber); // roomNumber
				temp = lastRoom;
			}
			
			// Generar sala del boss
			generateRandomRoom(temp, level, levelNumber, 7);
				
			temp = new Cords(2, 2);
			for (int i = 0; i < 2; i++) {
				roomNumber ++;
				lastRoom = generateRandomRoom(temp, level, levelNumber, roomNumber); // roomNumber
				temp = lastRoom;
			}
				
			// Generar la tienda
			generateRandomRoom(temp, level, levelNumber, 6); // 6
							
		}catch(NullPointerException ex) {
			// Controla la posibilidad de que no se puedan crear m�s salas debido
			// a la limitaci�n de espacio
		}
			
		// Eliminar elementos
		for (BaseActor actor : BaseActor.getList(mainStage, BaseActor.class.getName())) {
			actor.remove();
		}
		
		// Fondo de pantalla
		background = new BaseActor(0, 0, mainStage);
		background.loadTexture("assets/backgrounds/mazmorra_1_inicio.png");
		background.setSize(800, 600);
		background.setName("background");
		
		generateDoors(new Cords(2, 2), level);
	}
	
	public Cords generateRandomRoom(Cords startingRoom, Room[][] level, int levelNumber, int roomNumber) {
		Random random = new Random();
		int repeticiones = 0;
		do {
			repeticiones++;
			switch(random.nextInt(4) + 1) { // 1 Arriba, 2 abajo, 3 izquierda, 4 derecha
			case 1:
				if (startingRoom.getCordY() - 1 >= 0) {
					if(!level[startingRoom.getCordY() - 1][startingRoom.getCordX()].isEnabled()) {
						if (roomNumber == 7) {
							level[startingRoom.getCordY() - 1][startingRoom.getCordX()]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.BOSS);
						} else {
							level[startingRoom.getCordY() - 1][startingRoom.getCordX()]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.NORMAL);
						}
						level[startingRoom.getCordY() - 1][startingRoom.getCordX()].setEnabled(true);
						return new Cords(startingRoom.getCordX(), startingRoom.getCordY() - 1);
					}
				}
				break;
			case 2:
				if (startingRoom.getCordY() + 1 <= 4) {
					if (!level[startingRoom.getCordY() + 1][startingRoom.getCordX()].isEnabled()) {
						if (roomNumber == 7) {
							level[startingRoom.getCordY() + 1][startingRoom.getCordX()]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.BOSS);
						} else {
							level[startingRoom.getCordY() + 1][startingRoom.getCordX()]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.NORMAL);
						}
						level[startingRoom.getCordY() + 1][startingRoom.getCordX()].setEnabled(true);			
						return new Cords(startingRoom.getCordX(), startingRoom.getCordY() + 1);
					}
				}
				break;
			case 3:
				if (startingRoom.getCordX() - 1 >= 0) {
					if (!level[startingRoom.getCordY()][startingRoom.getCordX() - 1].isEnabled()) {
						if (roomNumber == 7) {
							level[startingRoom.getCordY()][startingRoom.getCordX() - 1]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.BOSS);
						} else {
							level[startingRoom.getCordY()][startingRoom.getCordX() - 1]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.NORMAL);
						}
						level[startingRoom.getCordY()][startingRoom.getCordX() - 1].setEnabled(true);
						return new Cords(startingRoom.getCordX() - 1, startingRoom.getCordY());
					}
				}
				break;
			case 4:
				if (startingRoom.getCordX() + 1 <= 4) {
					if (!level[startingRoom.getCordY()][startingRoom.getCordX() + 1].isEnabled()) {
						if (roomNumber == 7) {
							level[startingRoom.getCordY()][startingRoom.getCordX() + 1]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.BOSS);
						} else {
							level[startingRoom.getCordY()][startingRoom.getCordX() + 1]
									= new Room(mainStage, levelNumber, roomNumber, RoomType.NORMAL);
						}
						level[startingRoom.getCordY()][startingRoom.getCordX() + 1].setEnabled(true);
						return new Cords(startingRoom.getCordX() + 1, startingRoom.getCordY());
					}
				}
				break;
			}
		}while (repeticiones < 100);
		return null;
	}
	
	private void generateDoors(Cords room, Room[][] level) {
		
		// Comprobar sala de arriba
		if (room.getCordY() - 1 >= 0) {
			if (level[room.getCordY() - 1][room.getCordX()].isEnabled()) {
				if (currentLevel == 3) {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.UP);
					} else {
						if (level[room.getCordY() - 1][room.getCordX()].roomType == RoomType.NORMAL ||
								level[room.getCordY() - 1][room.getCordX()].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.DARK, Door.Orientation.UP);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.UP);
						}
					}
				} else {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.UP);
					} else {
						if (level[room.getCordY() - 1][room.getCordX()].roomType == RoomType.NORMAL ||
								level[room.getCordY() - 1][room.getCordX()].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.NORMAL, Door.Orientation.UP);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.UP);
						}
					}
				}
			}
		}
		
		// Comprobar sala de abajo
		if (room.getCordY() + 1 <= 4) {
			if (level[room.getCordY() + 1][room.getCordX()].isEnabled()) {
				if (currentLevel == 3) {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.DOWN);
					} else {
						if (level[room.getCordY() + 1][room.getCordX()].roomType == RoomType.NORMAL ||
								level[room.getCordY() + 1][room.getCordX()].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.DARK, Door.Orientation.DOWN);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.DOWN);
						}
					}
				} else {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.DOWN);
					} else {
						if (level[room.getCordY() + 1][room.getCordX()].roomType == RoomType.NORMAL ||
								level[room.getCordY() + 1][room.getCordX()].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.NORMAL, Door.Orientation.DOWN);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.DOWN);
						}
					}
				}
			}
		}
		
		// Comprobar sala de la izquierda
		if (room.getCordX() - 1 >= 0) {
			if (level[room.getCordY()][room.getCordX() - 1].isEnabled()) {
				if (currentLevel == 3) {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.LEFT);
					} else {
						if (level[room.getCordY()][room.getCordX() - 1].roomType == RoomType.NORMAL ||
								level[room.getCordY()][room.getCordX() - 1].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.DARK, Door.Orientation.LEFT);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.LEFT);
						}
					}
				} else {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.LEFT);
					} else {
						if (level[room.getCordY()][room.getCordX() - 1].roomType == RoomType.NORMAL ||
								level[room.getCordY()][room.getCordX() - 1].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.NORMAL, Door.Orientation.LEFT);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.LEFT);
						}
					}
				}
			}
		}
		
		// Comprobar sala de la derecha
		if (room.getCordX() + 1 <= 4) {
			if (level[room.getCordY()][room.getCordX() + 1].isEnabled()) {
				if (currentLevel == 3) {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.RIGHT);
					} else {
						if (level[room.getCordY()][room.getCordX() + 1].roomType == RoomType.NORMAL ||
								level[room.getCordY()][room.getCordX() + 1].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.DARK, Door.Orientation.RIGHT);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_2, Door.Orientation.RIGHT);
						}
					}
				} else {
					if (level[room.getCordY()][room.getCordX()].roomType == RoomType.BOSS) {
						new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.RIGHT);
					} else {
						if (level[room.getCordY()][room.getCordX() + 1].roomType == RoomType.NORMAL ||
								level[room.getCordY()][room.getCordX() + 1].roomType == RoomType.SHOP) {
							new Door(0, 0, mainStage, Door.DoorType.NORMAL, Door.Orientation.RIGHT);
						} else {
							new Door(0, 0, mainStage, Door.DoorType.BOSS_1, Door.Orientation.RIGHT);
						}
					}
				}
			}
		}
	}
	
	private void detectShop(Room[][] level) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (level[i][j].getRoomNumber() == 6) {
					level[i][j].roomType = RoomType.SHOP;
				}
			}
		}
	}
	
	private void loadHudElements() {
		uiStage.clear();
		BaseActor hud = new BaseActor(0, 0, uiStage);
		hud.loadTexture("assets/hud/base_hud.png");
		hud.setSize(800, 600);
		hud.setName("hud");
		
		Room[][] level = null;
		switch (currentLevel) {
		case 1:
			level = level1;
			break;
		case 2:
			level = level2;
			break;
		case 3:
			level = level3;
			break;
		}
		
		BaseActor[][] minimap = new BaseActor[5][5];
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				minimap[i][j] = new BaseActor(660 + (i * 25), 560 - (j * 25), uiStage);
				minimap[i][j].setName("minimap");
				if (currentRoom.getCordX() == i && currentRoom.getCordY() == j) {
					minimap[i][j].loadTexture("assets/hud/sala_jugador.png");
				} else if (level[j][i].isVisited() && level[j][i].roomType == RoomType.NORMAL){
					minimap[i][j].loadTexture("assets/hud/sala_vacia.png");
				} else if (level[j][i].isVisited() && level[j][i].roomType == RoomType.SHOP) {
					minimap[i][j].loadTexture("assets/hud/sala_vacia_tienda.png");
				} else if (level[j][i].isEnabled() && !level[j][i].isVisited()) {
					minimap[i][j].loadTexture("assets/hud/sala_sin_visitar.png");
				}
				minimap[i][j].setSize(25, 25);
			}
		}
		
		BaseActor[] healthBar = new BaseActor[20];
		for (int i = 0; i < player.getLife(); i++) {
			healthBar[i] = new BaseActor(10 + (i * 15), 570, uiStage);
			if (i % 2 == 0) {
				healthBar[i].loadTexture("assets/hud/corazon_izq.png");
			}else {
				healthBar[i].loadTexture("assets/hud/corazon_der.png");
			}
			healthBar[i].setSize(15, 25);
		}
		
		BaseActor coinHud = new BaseActor(10, 3, uiStage);
		coinHud.loadTexture("assets/hud/moneda.png");
		coinHud.setSize(30, 30);
		coinHud.setName("hud");
		
		Label coinLabel = new Label("x ", BaseGame.labelStyle);
		coinLabel.setPosition(40, 5);
		uiStage.addActor(coinLabel);
		
		coinLabel.setText("x " + player.getCoins());
		
		BaseActor keyHud = new BaseActor(110, 7, uiStage);
		keyHud.loadTexture("assets/hud/llave.png");
		keyHud.setName("hud");
		
		Label keyLabel = new Label("x ", BaseGame.labelStyle);
		keyLabel.setPosition(130, 5);
		uiStage.addActor(keyLabel);
		keyLabel.setText("x " + player.getKeys());
		
		scoreLabel = new Label("000000", BaseGame.labelStyle);
		scoreLabel.setPosition(370, 570);
		scoreLabel.setColor(255, 255, 255, 0.5f);
		uiStage.addActor(scoreLabel);
		String strScore = String.valueOf(player.getScore());
		int scoreLenght = strScore.length();
		
		String scoreText = "";
		for (int i = 0; i < 6 - scoreLenght; i++) {
			scoreText += "0";
		}
		scoreText += strScore;
		
		scoreLabel.setText(scoreText);
	}
	
	private void changeRoom(Door.Orientation orientation, Room[][] level) {
		float tempLife = player.getLife();
		float tempDamage = player.getDamage();
		float tempMoveSpeed = player.getSpeed();
		float tempShotSpeed = player.getShotSpeed();
		float tempEvadeRate = player.getEvadeRate();
		int tempCoins = player.getCoins();
		int tempKeys = player.getKeys();
		int tempScore = player.getScore();
		Weapon tempWeapon = player.weapon;
		boolean tempShield = player.shield;
		boolean tempBigProyectiles = player.biggerProyectiles;
		// A�adir m�s atributos arriba
		
		// Oscurecer
		blackScreen = new BaseActor(0, 0, effectStage);
		blackScreen.clearActions();
		blackScreen.loadTexture("assets/hud/black.png");
		blackScreen.setSize(800, 600);
		blackScreen.addAction(Actions.fadeOut(1));
		
		// Limpiar elementos
		mainStage.dispose();
		uiStage.dispose();
		effectStage.dispose();
		mainStage = new Stage();
		uiStage = new Stage();
		effectStage = new Stage();
		
		
		
		try {
			switch(orientation) {
			case UP:
				currentRoom.setCordY(currentRoom.getCordY() - 1);
				break;
			case DOWN:
				currentRoom.setCordY(currentRoom.getCordY() + 1);
				break;
			case LEFT:
				currentRoom.setCordX(currentRoom.getCordX() - 1);
				break;
			case RIGHT:
				currentRoom.setCordX(currentRoom.getCordX() + 1);
				break;
			}
		}catch(NullPointerException ex) {
			// Controla la posibilidad de que no haya oritentacion
			currentRoom.setCordX(2);
			currentRoom.setCordY(2);
		}
		
		// Fondo
		background = new BaseActor(0, 0, mainStage);
		background.setName("background");
	
		switch (currentLevel) {
		case 1:
			if (currentRoom.getCordX() == 2 && currentRoom.getCordY() == 2) {
				background.loadTexture("assets/backgrounds/mazmorra_1_inicio.png");
			} else if (level1[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.SHOP) {
				background.loadTexture("assets/backgrounds/mazmorra_1_tienda.png");
			} else {
				background.loadTexture("assets/backgrounds/mazmorra_1.png");
			}
			break;
		case 2:
			if (level2[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.SHOP) {
				background.loadTexture("assets/backgrounds/mazmorra_2_tienda.png");
			} else {
				background.loadTexture("assets/backgrounds/mazmorra_2.png");	
			}		
			break;
		case 3:
			if (level3[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.SHOP) {
				background.loadTexture("assets/backgrounds/mazmorra_3_tienda.png");
			} else {
				background.loadTexture("assets/backgrounds/mazmorra_3.png");	
			}
			break;
		}
		
		background.setSize(800, 600);
		
		// Puertas
		generateDoors(new Cords(currentRoom.getCordX(), currentRoom.getCordY()), level);
		
		// Elementos de la sala
		if (currentRoom.getCordY() != 2 || currentRoom.getCordX() != 2)
			level[currentRoom.getCordY()][currentRoom.getCordX()].drawComponents(mainStage);
		
		// Posicionar al personaje
		player = new Player(0, 0, mainStage);
		player.setLife(tempLife);
		player.setDamage(tempDamage);
		player.setSpeed(tempMoveSpeed);
		player.setShotSpeed(tempShotSpeed);
		player.setEvadeRate(tempEvadeRate);
		player.setCoins(tempCoins);
		player.setKeys(tempKeys);
		player.setScore(tempScore);
		player.weapon = tempWeapon;
		player.shield = tempShield;
		player.biggerProyectiles = tempBigProyectiles;
		
		try {
		switch(orientation) {
			case UP:
				player.setPosition(400 - player.getWidth() / 2, 100);
				player.addAction(Actions.moveTo(400 - player.getWidth() / 2, 100, 0.5f));
				break;
			case DOWN:
				player.setPosition(400 - player.getWidth() / 2, 490);
				player.addAction(Actions.moveTo(400 - player.getWidth() / 2, 490, 0.5f));
				break;
			case LEFT:
				player.setPosition(690, 300 - player.getHeight() / 2);
				player.addAction(Actions.moveTo(690, 300 - player.getHeight() / 2, 0.5f));
				break;
			case RIGHT:
				player.setPosition(60, 300 - player.getHeight() / 2);
				player.addAction(Actions.moveTo(60, 300 - player.getHeight() / 2, 0.5f));
				break;
			}
		}catch(NullPointerException ex) {
			// Controla que la  orientaci�n sea null
			player.setPosition(350, 300);
		}
		player.setSpeed(0);
		
		// Marcar como visitada la habitaci�n
		level[currentRoom.getCordY()][currentRoom.getCordX()].setVisited(true);
		
		// Cargar el hud
		loadHudElements();
		
		// Actualizar puertas
		boolean aliveEnemies = false;
		for (BaseActor enemyActor : BaseActor.getList(mainStage, Enemy.class.getName())) {
			Enemy enemy = (Enemy) enemyActor;
			if (enemy.isAlive()) {
				aliveEnemies = true;
			}
		}
		
		if (aliveEnemies) {
			for (BaseActor doorActor : BaseActor.getList(mainStage, Door.class.getName())) {
				Door door = (Door) doorActor;
				door.closeDoor();
			}
			
			switch (currentLevel) {
			case 1:
				if (level[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.BOSS) {
					level1Music.stop();
					boss1Music.play();
				}
				break;
			case 2:
				if (level[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.BOSS) {
					level2Music.stop();
					boss2Music.play();
				}
				break;
			case 3:
				if (level[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.BOSS) {
					level3Music.stop();
					boss3Music.play();
				}
				break;
			}
		}
		
		// Items
		if (player.shield) {
			if (shieldOn) {
				mainStage.addActor(shield);
			}
		}
	}
	
	private void randomizeItems() {
		itemList.add(1);
		itemList.add(2);
		itemList.add(3);
		itemList.add(4);
		itemList.add(5);
		itemList.add(6);
		itemList.add(7);
		itemList.add(8);
		Collections.shuffle(itemList);
	}
	
	private void setShieldOn(boolean state) {
		shieldOn = state;
		if (state) {
			mainStage.addActor(shield);
			shield.clearActions();
			shield.addAction(Actions.fadeOut(0.1f));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
		} else {
			shield.clearActions();
			shield.addAction(Actions.fadeOut(0.1f));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.fadeIn(0.1f)));
			shield.addAction(Actions.after(Actions.fadeOut(0.1f)));
			shield.addAction(Actions.after(Actions.removeActor()));
		}
	}
	
	@Override
	public void update(float dt) {
		
		// Ajustar posici�n del escudo
		shield.centerAtActor(player);
		
		long scoreCurrent = System.currentTimeMillis();
		if (scoreStart - scoreCurrent < -1000) {
			scoreStart = scoreCurrent;
			if (player.getScore() - 1 >= 0) {
				player.setScore(player.getScore() - 1);
			}
			String strScore = String.valueOf(player.getScore());
			int scoreLenght = strScore.length();
			
			String scoreText = "";
			for (int i = 0; i < 6 - scoreLenght; i++) {
				scoreText += "0";
			}
			scoreText += strScore;
			
			scoreLabel.setText(scoreText);
			
			scoreLabel.remove();
			uiStage.addActor(scoreLabel);
		}
		
		for (BaseActor toothActor : BaseActor.getList(mainStage, Tooth.class.getName())) {
			if (toothActor.overlaps(player)) {
				if (player.shield) {
					if (shieldOn) {
						setShieldOn(false);
						player.lastHit = System.currentTimeMillis();
					} else {
						player.takeDamage();
						loadHudElements();
						toothActor.remove();
					}
				} else {
					player.takeDamage();
					loadHudElements();
					toothActor.remove();
				}
			}
		}
		
		
		for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
			if (solidActor.getName().equalsIgnoreCase("chest")) {
				Chest chest = (Chest) solidActor;
				
				// Determinar si el jugador colisiona con el cofre
				if (player.overlaps(chest) && !chest.isOpened()) {
					if (player.getKeys() >= 1) {
						
						// Abrir el cofre
						player.loseKey();
						chest.open();
						loadHudElements();
					}
				}
			} else if (solidActor.getName().equalsIgnoreCase("rock")) {
				Rock rock = (Rock) solidActor;
				if (player.overlaps(rock) && !rock.isDestroyed()) {
					player.preventOverlap(rock);
				}
			}
			
			// Evitar colisi�n del jugador con objetos s�lidos
			player.preventOverlap(solidActor);
			
			for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
				
				// Evitar colisi�n de objetos m�viles con objetos s�lidos
				if (mobileActor instanceof Bat) {
				} else {
					mobileActor.preventOverlap(solidActor);
				}
			}
			
			for (BaseActor collectableActor : BaseActor.getList(mainStage, Collectable.class.getName())) {
				
				// Evitar colisi�n de objetos obtenibles con objetos s�lidos
				collectableActor.preventOverlap(solidActor);
			}
		}
		
		for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
			
			// Evitar que el elemento m�vil salga de los l�mites del mundo
			mobileActor.boundToWorld();
			
			if (mobileActor.getName().equalsIgnoreCase("enemy")) {
				
				// Evitar que el jugador colisione con enemigos
				Enemy enemy = (Enemy) mobileActor;
				if (enemy.isAlive()) {
					
					// Perseguir al jugador
					if (enemy instanceof Orc) {
						Orc orc = (Orc) enemy;
						orc.chasePlayer(player);
					} else if (enemy instanceof Bat) {
						Bat bat = (Bat) enemy;
						bat.escapePlayer(player);
					} else if (enemy instanceof Jimor) {
						Jimor jimor = (Jimor) enemy;
						jimor.chasePlayer(player);
					} else if (enemy instanceof Cuthulu) {
						Cuthulu cuthulu = (Cuthulu) enemy;
						cuthulu.chasePlayer(player);
					}
					
					// En caso de  que el jugador toque a un enemigo, el jugador recibira da�o
					if (player.overlaps(enemy)) {
						if (player.shield) {
							if (shieldOn) {
								setShieldOn(false);
								player.lastHit = System.currentTimeMillis();
							} else {
								player.takeDamage();
								loadHudElements();
								player.preventOverlap(mobileActor);
							}
						} else {
							player.takeDamage();
							loadHudElements();
							player.preventOverlap(mobileActor);
						}
					}
				}
			} else {
				
				// Evitar colisi�n de objetos m�viles con el jugador
				mobileActor.preventOverlap(player);
			}
		}
		
		for (BaseActor mobileActor : BaseActor.getList(mainStage, Mobile.class.getName())) {
			
			// Evitar colisi�n de objetos m�viles con otros objetos m�viles
			for (BaseActor mobileActor2 : BaseActor.getList(mainStage, Mobile.class.getName())) {
				if (mobileActor != mobileActor2) {
					if (mobileActor instanceof Bat || mobileActor2 instanceof Bat ||
							mobileActor instanceof Cuthulu || mobileActor2 instanceof Cuthulu) {
					} else {
						mobileActor.preventOverlap(mobileActor2);
					}
				}
			}
		}
		
		for (BaseActor collectableActor : BaseActor.getList(mainStage, Collectable.class.getName())) {
			Collectable collectable = (Collectable) collectableActor;
			
			// Detectar colisi�n de objetos obtenibles con el jugador
			if (player.overlaps(collectable)) {
				switch (collectable.getName()) {
				case "coin":
					coinSound.play();
					player.setScore(player.getScore() + 50);
					collectable.collect(player);
					collectable.remove();
					loadHudElements();
					break;
				case "key":
					Key key = (Key) collectable;
					if (key.isOnShop()) {
						if (player.getCoins() >= 3) {
							player.setCoins(player.getCoins() - 3);
							keySound.play();
							player.setScore(player.getScore() + 100);
							collectable.collect(player);
							collectable.remove();
							loadHudElements();
						}
					} else {
						keySound.play();
						player.setScore(player.getScore() + 100);
						collectable.collect(player);
						collectable.remove();
						loadHudElements();
					}
					break;
				case "hearth":
					Hearth hearth = (Hearth) collectable;
					if (hearth.isOnShop()) {
						if (player.getCoins() >= 3) {
							player.setCoins(player.getCoins() - 3);
							hearthSound.play();
							player.setScore(player.getScore() + 100);
							collectable.collect(player);
							collectable.remove();
							loadHudElements();
						}
					} else {
						hearthSound.play();
						player.setScore(player.getScore() + 100);
						collectable.collect(player);
						collectable.remove();
						loadHudElements();
					}
					break;
				case "item":
					if (player.getCoins() >= 10) {
						player.setCoins(player.getCoins() - 10);
						loadHudElements();
						player.setScore(player.getScore() + 200);
						Item item = (Item) collectable;
						collectable.collect(player);
						if (item.getId() == 5) {
							setShieldOn(true);
						}
						collectable.remove();
					}
					break;
				}
			}
			
			// Evitar colisi�n de objetos obtenibles con otros objetos obtenibles
			for (BaseActor collectableActor2 : BaseActor.getList(mainStage, Collectable.class.getName())) {
				if (collectableActor != collectableActor2) {
					collectableActor.preventOverlap(collectableActor2);
				}
			}
		}
		
		for (BaseActor doorActor : BaseActor.getList(mainStage, Door.class.getName())) {
			Door door = (Door) doorActor;
			
			// Detectar colisi�n del jugador con las puertas
			if (player.overlaps(door) && door.isOpen()) {
				
				// Cambiar de habitaci�n
				switch (currentLevel) {
				case 1:
					changeRoom(door.getOrientation(), level1);
					break;
				case 2:
					changeRoom(door.getOrientation(), level2);
					break;
				case 3:
					changeRoom(door.getOrientation(), level3);
					break;
				}
			}
		}
		
		for (BaseActor proyectileActor : BaseActor.getList(mainStage, Proyectile.class.getName())) {
			
			// Evitar colisi�n de cuchillos con objetos s�lidos
			for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
				if (proyectileActor.overlaps(solidActor)) {
					proyectileActor.remove();
				}
			}
			
			// Detectar colisi�n de cuchillos con enemigos
			for (BaseActor enemyActor : BaseActor.getList(mainStage, Enemy.class.getName())) {
				Enemy enemy = (Enemy) enemyActor;
				if (proyectileActor.overlaps(enemy) && enemy.isAlive()) {
					enemy.recibeDamage(player.getDamage());
					proyectileActor.remove();
					if (!enemy.isAlive()) {
						if (enemy instanceof BabySlime) {
							player.setScore(player.getScore() + 115);
						} else if (enemy instanceof BigSlime) {
							player.setScore(player.getScore() + 220);
						} else if (enemy instanceof Orc) {
							player.setScore(player.getScore() + 300);
						} else if (enemy instanceof Bat) {
							player.setScore(player.getScore() + 235);
						} else if (enemy instanceof Jimor) {
							player.setScore(player.getScore() + 5000);
						}
					}
				}
			}
			
			// Detectar colisi�n de cuchillos con barriles
			for (BaseActor barrelActor : BaseActor.getList(mainStage, Barrel.class.getName())) {
				Barrel barrel = (Barrel) barrelActor;
				
				if (proyectileActor.overlaps(barrelActor) && !barrel.isDestroyed()) {
					proyectileActor.remove();
					barrel.reciveDamage(player);
					
					// Determinar si el barril debe explotar
					if (barrel.isDestroyed() && barrel.isExplosive()) {
						
						// Efecto visual de la explosi�n
						VisualEffect vf = new VisualEffect(0, 0, "assets/characters/barrels/Explosion.png",
								4, 4, 0.03f, mainStage);
						explosionSound.play();
						vf.setBoundaryPolygon(8, true);
						vf.centerAtActor(barrel);
						
						// Detectar si la explosi�n afecta al jugador
						if (vf.overlaps(player)) {
							if (player.shield) {
								if (shieldOn) {
									setShieldOn(false);
									player.lastHit = System.currentTimeMillis();
								} else {
									player.takeDamage();
									loadHudElements();
									player.preventOverlap(vf);
								}
							} else {
								player.takeDamage();
								loadHudElements();
								player.preventOverlap(vf);
							}
						}
						
						// Detectar si la explosi�n afecta a enemigos
						for (BaseActor enemyActor : BaseActor.getList(mainStage, Enemy.class.getName())) {
							Enemy enemy = (Enemy) enemyActor;
							if (vf.overlaps(enemy)) {
								enemy.recibeDamage(100);
								enemy.preventOverlap(vf);
							}
						}
						
						// Detectar si la explosi�n afecta a las rocas
						for (BaseActor rockActor : BaseActor.getList(mainStage, Rock.class.getName())) {
							Rock rock = (Rock) rockActor;
							if (vf.overlaps(rockActor))
								rock.destroy(player);
						}
						
						// Detectar si la explosi�n afecta a otros barriles
						for (BaseActor barrelActor2 : BaseActor.getList(mainStage, Barrel.class.getName())) {
							Barrel barrel2 = (Barrel) barrelActor2;
							if (vf.overlaps(barrel2)) {
								barrel2.setHits(3);
								barrel2.reciveDamage(player);
							}
						}
					}
				}
			}
		}
					
		// Abrir y cerrar puertas
		boolean aliveEnemies = false;
		boolean doorsClosed = false;
					
		for (BaseActor enemyActor : BaseActor.getList(mainStage, Enemy.class.getName())) {
			Enemy enemy = (Enemy) enemyActor;
			if (enemy.isAlive()) {
				aliveEnemies = true;
			}
		}
					
		for (BaseActor doorActor : BaseActor.getList(mainStage, Door.class.getName())) {
			Door door = (Door) doorActor;
			if (!door.isOpen()) {
				doorsClosed = true;
				door.closeDoor();
			}
		}
		
		if (aliveEnemies) {
			for (BaseActor trapDoorActor : BaseActor.getList(mainStage, Trapdoor.class.getName())) {
				Trapdoor trapdoor = (Trapdoor) trapDoorActor;
				trapdoor.setOpen(false);
			}
		}
					
		if (!aliveEnemies && doorsClosed) {
			Room[][] level = null;
			switch (currentLevel) {
			case 1:
				level = level1;
				break;
			case 2:
				level = level2;
				break;
			case 3:
				level = level3;
				break;
			}
			if (level[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.NORMAL ||
					level[currentRoom.getCordY()][currentRoom.getCordX()].roomType == RoomType.SHOP) {
				for (BaseActor doorActor : BaseActor.getList(mainStage, Door.class.getName())) {
					Door door = (Door) doorActor;
					door.openDoor();
					openDoorsSound.play();
					player.setScore(player.getScore() + 1000);
					if (player.shield) {
						if (!shieldOn) {
							setShieldOn(true);
						} else {
							mainStage.addActor(shield);
						}
					}
				}
			} else {
				for (BaseActor trapDoorActor : BaseActor.getList(mainStage, Trapdoor.class.getName())) {
					Trapdoor trapdoor = (Trapdoor) trapDoorActor;
					trapdoor.setOpen(true);
					
					if (currentLevel == 1) {
						boss1Music.stop();
						level1Music.play();
					} else if (currentLevel == 2) {
						boss2Music.stop();
						level2Music.play();
					}
				}
			}
		}
		
		for (BaseActor trapDoorActor : BaseActor.getList(mainStage, Trapdoor.class.getName())) {
			if (player.overlaps(trapDoorActor) && ((Trapdoor) trapDoorActor).isOpen()) {
				if (currentLevel == 1) {
					currentLevel = 2;
					level1Music.stop();
					boss1Music.stop();
					changeRoom(null, level2);
					level2Music.play();
				} else if (currentLevel == 2) {
					level2Music.stop();
					boss2Music.stop();
					
					Score.score = player.getScore();
					SutovoqorGame.setActiveScreen(new VictoryScreen());
				}
			}
		}
		
		// Condici�n de fin de partida
		if (player.getLife() <= 0) {
			level1Music.stop();
			boss1Music.stop();
			level2Music.stop();
			boss2Music.stop();
		
			Score.score = player.getScore();
			SutovoqorGame.setActiveScreen(new DeathScreen());
		}
	}

	@Override
	public void resize(int arg0, int arg1) {
		viewport.update(arg0, arg1);
        camera.viewportWidth = arg0;
        camera.viewportHeight = arg1;
	}
}
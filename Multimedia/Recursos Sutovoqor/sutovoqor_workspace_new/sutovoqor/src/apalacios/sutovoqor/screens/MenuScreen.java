package apalacios.sutovoqor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import apalacios.sutovoqor.characters.BaseActor;
import apalacios.sutovoqor.game.SutovoqorGame;
import apalacios.sutovoqor.util.Cursor;

public class MenuScreen extends BaseScreen {
	private Music titleMusic;
	private Viewport viewport;
    private Camera camera;
    Cursor cursor;

	@Override
	public void initialize() {
		camera = new PerspectiveCamera();
        viewport = new FitViewport(800, 600, camera);
		
		titleMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/music/pantalla_titulo.mp3"));
		titleMusic.setLooping(true);
		titleMusic.play();
		
		BaseActor background = new BaseActor(0, 0, mainStage);
		background.loadTexture("assets/backgrounds/pantalla_inicio.jpg");
		background.setSize(800, 600);
		
		BaseActor title = new BaseActor(0, 0, mainStage);
		title.loadTexture("assets/text/titulo.png");
		title.centerAtPosition(400, 500);
		title.setScale(1.3f);
		
		BaseActor elements = new BaseActor(0, 0, mainStage);
		elements.loadTexture("assets/text/menu_inicio.png");
		elements.centerAtPosition(420, 280);
		elements.setScale(0.8f);
		
		cursor = new Cursor(220, 350, mainStage);
	}

	@Override
	public void update(float dt) {
		if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
			switch (cursor.getPosition()) {
			case 1:
				titleMusic.stop();
				SutovoqorGame.setActiveScreen(new LevelScreen());
				break;
			case 2:
				break;
			case 3:
				Gdx.app.exit();
				break;
			}
		}
		if (Gdx.input.isKeyJustPressed(Keys.W) ||
				Gdx.input.isKeyJustPressed(Keys.UP)) {
			if (cursor.getPosition() - 1 >= 1) {
				cursor.setPosition(cursor.getPosition() - 1);
			} else {
				cursor.setPosition(3);
			}
		} else if (Gdx.input.isKeyJustPressed(Keys.S) ||
				Gdx.input.isKeyJustPressed(Keys.DOWN)) {
			if (cursor.getPosition() + 1 <= 3) {
				cursor.setPosition(cursor.getPosition() + 1);
			} else {
				cursor.setPosition(1);
			}
		}
	}

	@Override
	public void resize(int arg0, int arg1) {
		viewport.update(arg0, arg1);
        camera.viewportWidth = arg0;
        camera.viewportHeight = arg1;
	}
}

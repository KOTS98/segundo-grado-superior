package apalacios.sutovoqor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import apalacios.sutovoqor.characters.BaseActor;
import apalacios.sutovoqor.game.BaseGame;
import apalacios.sutovoqor.game.SutovoqorGame;
import apalacios.sutovoqor.util.Score;

public class DeathScreen extends BaseScreen {
	private Music deathMusic;
	private Viewport viewport;
    private Camera camera;

	@Override
	public void initialize() {
		camera = new PerspectiveCamera();
        viewport = new FitViewport(800, 600, camera);
		
		deathMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/music/muerte.mp3"));
		deathMusic.setLooping(true);
		deathMusic.play();
		
		BaseActor title = new BaseActor(0, 0, mainStage);
		title.loadTexture("assets/text/fin_partida.png");
		title.centerAtPosition(400, 500);
		title.setScale(1.3f);
		
		BaseActor puntuacion = new BaseActor(0, 0, mainStage);
		puntuacion.loadTexture("assets/text/puntuacion.png");
		puntuacion.centerAtPosition(230, 350);
		puntuacion.setScale(0.5f);
		
		Label scoreLabel = new Label("", BaseGame.labelStyle);
		scoreLabel.setScale(3);
		scoreLabel.setPosition(350, 345);
		uiStage.addActor(scoreLabel);
		scoreLabel.setText("" + Score.score);
		
		BaseActor continuar = new BaseActor(0, 0, mainStage);
		continuar.loadTexture("assets/text/enter_continuar.png");
		continuar.centerAtPosition(400, 150);
		continuar.setScale(0.6f);
	}

	@Override
	public void update(float dt) {
		if (Gdx.input.isKeyJustPressed(Keys.ENTER)) {
			deathMusic.stop();
			SutovoqorGame.setActiveScreen(new MenuScreen());
		}


	}

	@Override
	public void resize(int arg0, int arg1) {
		viewport.update(arg0, arg1);
        camera.viewportWidth = arg0;
        camera.viewportHeight = arg1;
	}
}

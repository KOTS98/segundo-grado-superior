package apalacios.sutovoqor.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class BaseScreen implements Screen {
	protected static Stage mainStage;
	protected Stage uiStage;
	protected Stage effectStage;
	
	public BaseScreen() {
		mainStage = new Stage();
		uiStage = new Stage();
		effectStage = new Stage();
		
		initialize();
	}
	
	public static Stage getMainStage() {
		return mainStage;
	}
	
	public abstract void initialize();
	
	public abstract void update(float dt);
	
	public void render(float dt) {
		effectStage.act(dt);
		uiStage.act(dt);
		mainStage.act(dt);
		
		update(dt);
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		mainStage.draw();
		uiStage.draw();
		effectStage.draw();
	}
	
	// M�todos necesarios para la interfaz de Screen
	
	public void pause() {}
	
	public void resume() {}
	
	public void dispose() {}
	
	public void show() {}
	
	public void hide() {}
}

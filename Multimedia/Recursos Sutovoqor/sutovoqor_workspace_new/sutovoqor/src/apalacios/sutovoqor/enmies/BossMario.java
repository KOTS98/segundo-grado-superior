package apalacios.sutovoqor.enmies;

import com.badlogic.gdx.scenes.scene2d.Stage;

import apalacios.sutovoqor.characters.BaseActor;

public class BossMario extends BaseActor {
	
	// Registra el momento en el que se actualizo por �ltima vez la cuenta atras
	private long cuentaAtrasInicio = System.currentTimeMillis();
	
	// Representa los segundos restantes en la cuenta atr�s
	private int segundosRestantes = 10;
	
	// Representa el estado actual de la cuenta atr�s (false = terminado)
	private boolean cuentaAtrasTerminada = false;
	
	/**
	 * Constructor del objeto
	 * 
	 * @param posX Coordenada en pixeles sobre el eje horizontal
	 * @param posY Coordenada en pixeles sobre el eje vertical
	 * @param stage Stage donde se crear� el objeto
	 */
	public BossMario(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		
		// Cargar la animaci�n
		// Dar poligono de colisi�n
		// Ajustes de velocidad y aceleraci�n
		// Otros ajutes del objeto
	}
	
	/**
	 * Controla las acciones del objeto, se ejecuta con cada iteraci�n del bucle de
	 * juego principal
	 */
	@Override
	public void act(float dt) {
		super.act(dt);
		
		// Mover el objeto
		// Cambiar animaciones
		// Actualizar otros par�metros
		
		
		// Ejecutar cuenta atr�s
		
		if (segundosRestantes > 0) { // Determinar si la cuenta atr�s ha terminado o no
			
			// Representa el momento en el que se intenta reducir un segundo
			long cuentaAtrasActual = System.currentTimeMillis();
			
			if ((cuentaAtrasInicio - cuentaAtrasActual) < -1000) { // Determinar si ha pasado al menos un
				// segundo desde la �ltima ejecuci�n
				
				// Sobreescribe el momento en el que se actualizo por �ltima vez la cuenta atr�s
				cuentaAtrasInicio = cuentaAtrasActual;
				
				// Resta un segundo a la cuenta atr�s
				segundosRestantes -= 1;
			}
		}
		
		if (segundosRestantes <= 0 && !cuentaAtrasTerminada) { // Determina si la cuenta atr�s esta a 0, y
			//si a�n no se ha terminado
			
			// Desactiva la cuenta atr�s
			cuentaAtrasTerminada = true;
			
			// Crear el nuevo objeto
			// Eliminar el objeto actual	
		}
	}
}

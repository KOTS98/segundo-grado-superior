package apalacios.sutovoqor.enmies;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import apalacios.sutovoqor.characters.Player;
import apalacios.sutovoqor.characters.Tooth;

public class Jimor extends Enemy {
	private Random random;
	
	public boolean isJumping;
	private boolean isFalling;
	private boolean isAttacking;
	
	private float angleToPlayer;
	
	private boolean alive;
	
	private Animation attack;
	private Animation idle;
	private Animation jump;
	private Animation fall;
	
	private float life;
	private float damage;
	
	private long actStartTime = System.currentTimeMillis();
	private long jumpStartTime = System.currentTimeMillis();
	private long attackStartTime = System.currentTimeMillis();
	
	private Sound spitSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/jimor_escupir.ogg"));
	private Sound jumpSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/jimor_salto.ogg"));
	private Sound fallSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/jimor_cae.ogg"));
	
	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}
	
	@Override
	public float getDamage() {
		return damage;
	}
	
	public boolean isAlive() {
		return alive;
	}

	public Jimor(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		setName("enemy");
		
		// Carga la animación del enemigo
		attack = loadTexture("assets/characters/enmies/jimor_attack.png");
		idle = loadTexture("assets/characters/enmies/jimor_idle.png");
		jump = loadTexture("assets/characters/enmies/jimor_jump.png");
		fall = loadTexture("assets/characters/enmies/jimor_fall.png");

		
		setAnimation(idle);
		setAnimationPaused(true);
		setScale(2.5f);
		
		// Crea el polígono para colisiones
		setBoundaryPolygon(8, false);
		
		// Ajusta la velocidad y aceleración
		setAcceleration(300);
		setMaxSpeed(150);
		setDeceleration(300);
		
		// Otros ajustes
		random = new Random();
		isJumping = false;
		isAttacking = false;
		setLife(1500);
		alive = true;
		angleToPlayer = 0;
	}
	
	public void setDirection(int direction) {
	}
	
	public void act(float dt) {
		super.act(dt);
        
        long actTime = System.currentTimeMillis();
        if ((actStartTime - actTime) < -1500) {
        	actStartTime = actTime;
        	
        	if ((random.nextInt(10) + 1) < 8) {
        		setAnimation(jump);
        		jumpSound.play();
        		setAnimationPaused(false);
            	
            	Action jimorUp = Actions.moveBy(0, 70, 0.4f);
    			Action JimorDown = Actions.moveBy(0, -70, 0.4f);
    			addAction(Actions.after(jimorUp));
    			addAction(Actions.after(JimorDown));
    			removeAction(jimorUp);
    			removeAction(JimorDown);
    			isJumping = true;
    			jumpStartTime = System.currentTimeMillis();
        	} else {
        		setAnimation(attack);
        		spitSound.play();
        		isAttacking = true;
        		attackStartTime = System.currentTimeMillis();
        		Tooth tooth1 = new Tooth(getX(), getY(), getStage());
        		Tooth tooth2 = new Tooth(getX(), getY(), getStage());
        		Tooth tooth3 = new Tooth(getX(), getY(), getStage());

        		tooth1.setMotionAngle(angleToPlayer + 20);
        		tooth2.setMotionAngle(angleToPlayer);
        		tooth3.setMotionAngle(angleToPlayer - 20);
        	}
        }
        
        // Cesar ataque
        if (isAttacking) {
        	long attackCurrentTime = System.currentTimeMillis();
        	if ((attackStartTime - attackCurrentTime) < -500) {
        		attackStartTime = attackCurrentTime;
        		isAttacking = false;
        		setAnimation(idle);
        	}
        }
        
        // Caer
        if (isJumping && !isFalling) {
        	long jumpCurrentTime = System.currentTimeMillis();
        	if ((jumpStartTime - jumpCurrentTime) < -400) {
        		isFalling = true;
        		setAnimation(fall);
        	}
        }
        
        // Dejar de saltar
        if (isJumping) {
        	long jumpCurrentTime = System.currentTimeMillis();
        	if ((jumpStartTime - jumpCurrentTime) < -800) {
        		jumpStartTime = jumpCurrentTime;
        		isJumping = false;
        		isFalling = false;
        		setAnimation(idle);
        		fallSound.play();
        	}
        }
		
		
		applyPhisics(dt);
		boundToWorld();
	}

	@Override
	public void recibeDamage(float damage) {
		life -= damage;
		if (life <= damage) {
			alive = false;
			clearActions();
			addAction(Actions.fadeOut(0.2f));
			addAction(Actions.after(Actions.removeActor()));
		}
	}
	
	public void chasePlayer(Player player) {
		Vector2 playerPosition = new Vector2(player.getX(), player.getY() );
        Vector2 enemyPosition = new Vector2(this.getX(), this.getY() );
        Vector2 hitVector = enemyPosition.sub(playerPosition );
        setMotionAngle(hitVector.angle() - 180);
        angleToPlayer = hitVector.angle() - 180;
        
        if (isJumping) {
        	setSpeed(400);
        }
	}
}

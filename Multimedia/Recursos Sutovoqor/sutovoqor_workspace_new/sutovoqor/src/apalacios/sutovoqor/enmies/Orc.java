package apalacios.sutovoqor.enmies;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;

import apalacios.sutovoqor.characters.Player;

public class Orc extends Enemy {
	
	private boolean alive;
	
	private Animation north;
	private Animation south;
	private Animation east;
	private Animation west;
	
	private float life;
	private float damage;
	
	private Sound screamSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/grito_orco.ogg"));
	
	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}
	
	@Override
	public float getDamage() {
		return damage;
	}
	
	public boolean isAlive() {
		return alive;
	}

	public Orc(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		setName("enemy");
		
		// Carga la animación del jugador
		Texture texture = new Texture(Gdx.files.internal("assets/characters/enmies/orco.png"), true);
		int rows = 4;
		int cols = 3;
		int frameHeight = texture.getHeight() / rows;
		int frameWidth = texture.getWidth() / cols;
		float frameDuration = 0.3f;
		
		TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight);
		
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[0][i]);
		south = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[1][i]);
		west = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[2][i]);
		east = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[3][i]);
		north = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		setAnimation(south);
		
		// Crea el polígono para colisiones
		setBoundaryPolygon(8, false);
		
		// Ajusta la velocidad y aceleración
		setAcceleration(280);
		setMaxSpeed(130);
		setDeceleration(280);
		
		// Otros ajustes
		setLife(75);
		alive = true;
	}
	
	public void setDirection(int direction) {
	}
	
	public void act(float dt) {
		super.act(dt);
		
		// Establecer animación
		float angle = getMotionAngle();
		if (angle >= 45 && angle <= 135) {
			setAnimation(north);
		} else if (angle > 135 && angle < 225) {
			setAnimation(west);
		} else if (angle >= 225 && angle <= 315) {
			setAnimation(south);
		} else {
			setAnimation(east);
		}
		
		applyPhisics(dt);
		boundToWorld();
		
		if (new Random().nextInt(200) + 1 == 1) {
			screamSound.play();
		}
	}

	@Override
	public void recibeDamage(float damage) {
		life -= damage;
		if (life <= damage) {
			alive = false;
			clearActions();
			addAction(Actions.fadeOut(0.2f));
			addAction(Actions.after(Actions.removeActor()));
		}
	}
	
	public void chasePlayer(Player player) {
		setSpeed(200);
		Vector2 playerPosition = new Vector2(player.getX(), player.getY() );
        Vector2 enemyPosition = new Vector2(this.getX(), this.getY() );
        Vector2 hitVector = enemyPosition.sub(playerPosition );
        setMotionAngle(hitVector.angle() - 180);
	}
}

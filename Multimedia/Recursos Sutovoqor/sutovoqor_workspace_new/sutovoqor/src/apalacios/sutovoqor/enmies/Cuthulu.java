package apalacios.sutovoqor.enmies;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import apalacios.sutovoqor.characters.Player;

public class Cuthulu extends Enemy {
	
	public boolean isJumping;
	private boolean isTransformating;
	private boolean isAttacking;
	private boolean lowHealth;
	
	private float angleToPlayer;
	
	private boolean alive;
	
	private Animation attack;
	private Animation normal;
	
	private float life;
	private float damage;
	
	private long actStartTime = System.currentTimeMillis();
	private long transformStartTime = System.currentTimeMillis();
	private long attackStartTime = System.currentTimeMillis();
	
	private Sound screamSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/cuthulu_scream.ogg"));
	private Sound spitSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/jimor_cae.ogg"));
	
	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}
	
	@Override
	public float getDamage() {
		return damage;
	}
	
	public boolean isAlive() {
		return alive;
	}

	public Cuthulu(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		setName("enemy");
		
		// Carga la animación del enemigo
		attack = loadAnimationFromSheet("assets/characters/enmies/cuthulu_ataque.png", 1, 3, 0.15f, true);
		normal = loadAnimationFromSheet("assets/characters/enmies/cuthulu_normal.png", 1, 3, 0.15f, true);

		setAnimation(normal);
		
		// Crea el polígono para colisiones
		setBoundaryPolygon(8, false);
		
		// Ajusta la velocidad y aceleración
		setAcceleration(300);
		setMaxSpeed(1000);
		setDeceleration(300);
		
		// Otros ajustes
		isJumping = false;
		isTransformating = false;
		isAttacking = false;
		setLife(1800);
		alive = true;
		angleToPlayer = 0;
		lowHealth = false;
	}
	
	public void setDirection(int direction) {
	}
	
	public void act(float dt) {
		super.act(dt);
		
		if (isTransformating && !lowHealth) {
			screamSound.play();
		}
        
        long actTime = System.currentTimeMillis();
        if ((actStartTime - actTime) < -(new Random().nextInt(1500) + 3000) && !lowHealth) {
        	actStartTime = actTime;
        	if (!isTransformating) {
            	new Demon_eye(getX(), getY(), getStage());
            	spitSound.play();
        	}
        }
        
        if ((actStartTime - actTime) < -(new Random().nextInt(1000) + 1500) && lowHealth) {
        	if (lowHealth && !isTransformating && !isAttacking) {
        		isAttacking = true;
        		attackStartTime = System.currentTimeMillis();
        	}
        	if (isAttacking) {
	        	long attackCurrentTime = System.currentTimeMillis();
	        	if ((attackStartTime - attackCurrentTime) < -2500) {
	        		screamSound.play();
	        		attackStartTime = attackCurrentTime;
	        		isAttacking = false;
	        	}
        	}
        }
        
        if (life < 900 && lowHealth == false && !isTransformating) {
        	screamSound.play();
        	lowHealth = true;       	
        	isTransformating = true;       	
        	transformStartTime = System.currentTimeMillis();
        	clearActions();
        	addAction(Actions.rotateBy(360, 0.5f));
        	addAction(Actions.after(Actions.rotateBy(540, 0.5f)));
        	addAction(Actions.after(Actions.rotateBy(900, 0.5f)));
        	addAction(Actions.after(Actions.rotateBy(540, 0.5f)));
        	addAction(Actions.rotateBy(360, 0.5f));
        }
        
        long transformCurrentTime = System.currentTimeMillis();
        if ((transformStartTime - transformCurrentTime) < -1250 && isTransformating) {
        	setAnimation(attack);   	
        }
        if ((transformStartTime - transformCurrentTime) < -2500 && isTransformating) {
        	isTransformating = false;       	
        }
       
		applyPhisics(dt);
		boundToWorld();
	}

	@Override
	public void recibeDamage(float damage) {
		life -= damage;
		if (life <= damage) {
			alive = false;
			clearActions();
			addAction(Actions.fadeOut(0.2f));
			addAction(Actions.after(Actions.removeActor()));
		}
	}
	
	public void chasePlayer(Player player) {
		Vector2 playerPosition = new Vector2(player.getX(), player.getY());
        Vector2 enemyPosition = new Vector2(this.getX(), this.getY());
        Vector2 hitVector = enemyPosition.sub(playerPosition);
        angleToPlayer = hitVector.angle() - 180;
        
        if (!isTransformating && !lowHealth) {
        	setRotation(angleToPlayer + 90);
        	setMotionAngle(angleToPlayer);
        	setSpeed(50);
        }
        if (!isTransformating && lowHealth) {
        	setRotation(angleToPlayer + 90);
        	if (!isAttacking) {
        		setMotionAngle(angleToPlayer);
        		setSpeed(50);
        	} else {
        		setSpeed(800);
        		
        	}
        }
	}
}

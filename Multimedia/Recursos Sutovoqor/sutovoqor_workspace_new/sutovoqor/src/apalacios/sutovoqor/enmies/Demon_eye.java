package apalacios.sutovoqor.enmies;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Demon_eye extends Enemy {
	
	private boolean alive;
	
	private Animation move;
	
	private float life;
	private float damage;
	
	private int direction;
	
	private long startTime = System.currentTimeMillis();
	
	private Sound death0Sound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/slime_muerte0.ogg"));
	private Sound death1Sound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/slime_muerte1.ogg"));
	private Sound death2Sound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/slime_muerte2.ogg"));

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}
	
	@Override
	public float getDamage() {
		return damage;
	}
	
	public boolean isAlive() {
		return alive;
	}

	public Demon_eye(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		setName("enemy");
		
		direction = 2;
		
		// Carga la animación del ojo
		move = loadAnimationFromSheet("assets/characters/enmies/demon_eye.png", 1, 2, 0.15f, true);
		
		setAnimation(move);
		
		// Crea el polígono para colisiones
		setBoundaryPolygon(8, false);
		
		// Ajusta la velocidad y aceleración
		setAcceleration(500);
		setMaxSpeed(145);
		setDeceleration(500);
		
		// Otros ajustes
		setLife(40);
		alive = true;
	}
	
	public void setDirection(int direction) {
		this.direction = direction;
	}
	
	public void act(float dt) {
		super.act(dt);
		
		long currentTime = System.currentTimeMillis();
		if (startTime - currentTime < -(new Random().nextInt(1000) + 1000)) {
			startTime = currentTime;
			direction = new Random().nextInt(360) + 1;
		}
		
		// Movimiento
		setMotionAngle(direction);
		setRotation(direction - 180);
		accelerateAtAngle(direction);
		
		applyPhisics(dt);
		boundToWorld();
	}

	@Override
	public void recibeDamage(float damage) {
		life -= damage;
		if (life <= damage) {
			alive = false;
			clearActions();
			switch (new Random().nextInt(3) + 1) {
			case 1:
				death0Sound.play();
				break;
			case 2:
				death1Sound.play();
				break;
			case 3:
				death2Sound.play();
				break;
			}
			
			addAction(Actions.fadeOut(0.2f));
			addAction(Actions.after(Actions.removeActor()));
		}
	}
}

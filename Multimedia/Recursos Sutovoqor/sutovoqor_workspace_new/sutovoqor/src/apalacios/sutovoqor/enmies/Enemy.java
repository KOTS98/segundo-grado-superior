package apalacios.sutovoqor.enmies;

import com.badlogic.gdx.scenes.scene2d.Stage;

import apalacios.sutovoqor.characters.Mobile;

public abstract class Enemy extends Mobile {
	
	private boolean alive;
	
	public Enemy(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		alive = true;
	}

	public abstract void recibeDamage(float damage);
	
	public abstract float getDamage();
	
	public void setAlive(boolean alive) {
		this.alive = alive;
	}
	
	public boolean isAlive() {
		return alive;
	}
}

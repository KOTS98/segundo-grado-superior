package apalacios.sutovoqor.enmies;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;

public class BigSlime extends Enemy {
	
	private boolean alive;
	
	private Animation north;
	private Animation south;
	private Animation east;
	private Animation west;
	
	private float life;
	private float damage;
	
	private int direction;
	
	private long startTime = System.currentTimeMillis();
	
	private Sound death0Sound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/slime_muerte0.ogg"));
	private Sound death1Sound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/slime_muerte1.ogg"));
	private Sound death2Sound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/slime_muerte2.ogg"));

	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getDamage() {
		return damage;
	}

	public BigSlime(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		setName("enemy");
		
		direction = 2;
		
		// Carga la animación del jugador
		Texture texture = new Texture(Gdx.files.internal("assets/characters/enmies/slime.png"), true);
		int rows = 4;
		int cols = 3;
		int frameHeight = texture.getHeight() / rows;
		int frameWidth = texture.getWidth() / cols;
		float frameDuration = 0.4f;
		
		TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight);
		
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[0][i]);
		south = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[1][i]);
		west = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[2][i]);
		east = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[3][i]);
		north = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		setAnimation(south);
		
		// Crea el polígono para colisiones
		setBoundaryPolygon(8, false);
		
		// Ajusta la velocidad y aceleración
		setAcceleration(100);
		setMaxSpeed(50);
		setDeceleration(100);
		setScale(2);
		
		// Otras propiedades
		setLife(100);
		alive = true;
	}
	
	public boolean isAlive() {
		return alive;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public void act(float dt) {
		super.act(dt);
		
		long currentTime = System.currentTimeMillis();
		if (startTime - currentTime < -(new Random().nextInt(1000) + 1000)) {
			startTime = currentTime;
			direction = new Random().nextInt(4) + 1;
		}
		
		// Movimiento hacia arriba
		if (direction == 1) {
			setAnimation(north);
			accelerateAtAngle(90);
		}
				
		// Movimiento hacia abajo
		if (direction == 2) {
			setAnimation(south);
			accelerateAtAngle(270);
		}
				
		// Movimiento hacia la izquierda
		if (direction == 3) {
			setAnimation(west);
			accelerateAtAngle(180);
		}
				
		// Movimiento hacia la derecha
		if (direction == 4) {
			setAnimation(east);
			accelerateAtAngle(0);
		}
		
		applyPhisics(dt);
		boundToWorld();
	}
	
	@Override
	public void recibeDamage(float damage) {
		life -= damage;
		if (life <= damage) {
			alive = false;
			clearActions();
			switch (new Random().nextInt(3) + 1) {
			case 1:
				death0Sound.play();
				break;
			case 2:
				death1Sound.play();
				break;
			case 3:
				death2Sound.play();
				break;
			}
			
			addAction(Actions.fadeOut(0.2f));
			BabySlime babySlime;
			for (int i = 0; i < 4; i++) {
				babySlime = new BabySlime(getX(), getY(), getStage());
				babySlime.setDirection(i + 1);
			}
			addAction(Actions.after(Actions.removeActor()));
		}
	}
}

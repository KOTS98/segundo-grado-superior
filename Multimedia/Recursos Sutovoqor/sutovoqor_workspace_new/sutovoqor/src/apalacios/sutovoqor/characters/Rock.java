package apalacios.sutovoqor.characters;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

/**
 * Representa a una roca
 * 
 * @author KOTS98
 */
public class Rock extends Solid {
	boolean destroyed;
	
	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param posX Representa su posici�n en el eje horizontal
	 * @param posY Representa su posici�n en el eje vertical
	 * @param stage Represente el stage al que ser� a�adido
	 */
	public Rock(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		
		destroyed = false;
		
		// Establece la textura del item
		loadRandomTexture();
		setSize(50, 50);
		
		// Crea el pol�gono para colisiones
		setBoundaryPolygon(8, false);
		
		this.setName("rock");
	}
	
	public boolean isDestroyed() {
		return destroyed;
	}
	
	private void loadRandomTexture() {
		Random random = new Random();
		switch(random.nextInt(5) + 1) {
		case 1:
			loadTexture("assets/characters/rocks/roca1.png");
			break;
		case 2:
			loadTexture("assets/characters/rocks/roca2.png");
			break;
		case 3:
			loadTexture("assets/characters/rocks/roca3.png");
			break;
		case 4:
			loadTexture("assets/characters/rocks/roca4.png");
			break;
		case 5:
			loadTexture("assets/characters/rocks/roca5.png");
			break;
		}
	}
	
	public void destroy(Player player) {
		player.setScore(player.getScore() + 85);
		destroyed = true;
		addAction(Actions.fadeOut(0.05f));
		addAction(Actions.after(Actions.removeActor()));
	}
}

package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Bathlet extends Proyectile {

	public Bathlet(float posX, float posY, Stage stage, boolean big) {
		super(posX, posY, stage);
		setName("tooth");
		
		loadTexture("assets/characters/items/bathlet.png");
		if (big) {
			setSize(75, 75);
		}
		
		addAction(Actions.delay(1));
		addAction(Actions.after(Actions.fadeOut(0.5f)));
		addAction(Actions.after(Actions.removeActor()));
		
		setSpeed(600);
		setMaxSpeed(600);
		setDeceleration(0);
		setBoundaryPolygon(8, false);
		
		Action spin = Actions.rotateBy(500, 1);
		this.addAction(Actions.forever(spin));
	}
	
	public void act(float dt) {
		super.act(dt);
		applyPhisics(dt);
	}
}

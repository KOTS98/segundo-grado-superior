package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Tooth extends BaseActor {

	public Tooth(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		setName("tooth");
		
		loadTexture("assets/characters/enmies/diente.png");
		
		addAction(Actions.delay(1));
		addAction(Actions.after(Actions.fadeOut(0.5f)));
		addAction(Actions.after(Actions.removeActor()));
		
		setSpeed(600);
		setMaxSpeed(600);
		setDeceleration(0);
		setBoundaryPolygon(8, false);
		
		Action spin = Actions.rotateBy(500, 1);
		this.addAction(Actions.forever(spin));
	}
	
	public void act(float dt) {
		super.act(dt);
		applyPhisics(dt);
	}
}

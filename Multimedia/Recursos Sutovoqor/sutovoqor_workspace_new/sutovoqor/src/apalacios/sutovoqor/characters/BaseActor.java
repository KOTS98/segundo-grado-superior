package apalacios.sutovoqor.characters;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Intersector.MinimumTranslationVector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

/**
 * De esta clase heredar�n todos los elementos gr�ficos del juego, tales como el personaje
 * principal, enemigos y paredes
 * 
 * @author KOTS98
 */
public class BaseActor extends Actor {
	// Animaci�n
	private Animation<TextureRegion> animation;
	private float elapsedTime;
	private boolean animationPaused;
	
	// Velocidad y aceleraci�n
	private Vector2 velocityVec;
	private Vector2 accelerationVec;
	private float acceleration;
	private float maxSpeed;
	private float deceleration;
	
	// Colisiones
	private Polygon boundaryPolygon;
	private static Rectangle worldBounds;
	
	/**
	 * Constructor de la calse, inicializar los par�metros de la misma
	 * 
	 * @param posX Representa su posici�n respecto a la pantalla en el eje horizontal
	 * @param posY Representa su posici�n respecto a la pantalla en el eje vertical
	 * @param stage El actor (independientemente de lo que sea), sera a�adido al stage,
	 * que se encarga de ordenar todos los elementos jer�rquicamente
	 */
	public BaseActor(float posX, float posY, Stage stage) {
		super();
		
		setPosition(posX, posY);
		stage.addActor(this);
		animation = null;
		elapsedTime = 0;
		animationPaused = false;
		
		velocityVec = new Vector2(0, 0);
		accelerationVec = new Vector2(0, 0);
		acceleration = 0;
		maxSpeed = 1000;
		deceleration = 0;
	}
	
	/**
	 * Devuelve la lista de actores que corresponden a la clase especificada
	 * 
	 * @param stage Representa en el stage donde se han a�adido los actores
	 * @param className Representa el nombre de la clase de la que se desean obtener actores
	 * @return ArrayList<BaseActor> con los actores de la clase especificada
	 */
	public static ArrayList<BaseActor> getList(Stage stage, String className) {
		ArrayList<BaseActor> list = new ArrayList<BaseActor>();
		
		Class<?> theClass = null;
		try {
			theClass = Class.forName(className);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		for (Actor actor : stage.getActors()) {
			if (theClass.isInstance(actor))
				list.add((BaseActor) actor);
		}
		return list;
	}
	
	public static int count(Stage stage, String className) {
		return getList(stage, className).size();
	}
	
	/**
	 * Establece la animaci�n deseada. Es necesario establecer una animaci�n al actor antes
	 * de manipular otros par�metros como el tama�o, rotaci�n, etc...
	 * 
	 * @param animation Representa la animaci�n deseada para el actor
	 */
	public void setAnimation(Animation<TextureRegion> animation) {
		this.animation = animation;
		TextureRegion tr = animation.getKeyFrame(0);
		float width = tr.getRegionWidth();
		float height = tr.getRegionHeight();
		setSize(width, height);
		setOrigin(width / 2, height / 2);
		
		// Generar un pol�gono para colisiones bas�ndose en las dimensiones del actor
		if (boundaryPolygon == null)
			setBoundaryRectangle();
	}
	
	/**
	 * Pausa o reanuda la animaci�n
	 * 
	 * @param animationPaused [true = animaci�n pausada, false = animaci�n en ejecuci�n]
	 */
	public void setAnimationPaused(boolean animationPaused) {
		this.animationPaused = animationPaused;
	}
	
	/**
	 * Actualiza autom�ticamente la imagen que debe ser mostrada por la animaci�n basandose en el
	 * tiempo de ejecuci�n actual
	 */
	public void act(float dt) {
		super.act(dt);
		
		// Determina si la animaci�n esta pausada antes de ajustar el frame
		if(!animationPaused)
			elapsedTime += dt;
	}
	
	/**
	 * Determina la imagen exacta a dibujar por la animaci�n y la dibuja
	 */
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
		// Aplicar color sobre el sprite
		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a);
		
		if (animation != null && isVisible())
			batch.draw(animation.getKeyFrame(elapsedTime), getX(), getY(), getOriginX(),
					getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
	}
	
	/**
	 * Crea una animaci�n basada en una serie de archivos de imagen separados y establece la
	 * animaci�n del actor en caso de que no se haya establecido previamente
	 * 
	 * @param fileNames Representa los nombres de todos los archivos que componen la imagen
	 * @param frameDuration Representa el tiempo que se mantiene en pantalla cada cuadro de la
	 * animaci�n
	 * @param loop Representa si la animaci�n se ejecutar� una sola vez, o se ejecutar� en bucle
	 * [false = se ejecuta una �nica vez, true = se ejecuta en bucle]
	 * @return Devuelve la animaci�n generada
	 */
	public Animation<TextureRegion> loadAnimationFromFiles(String[] fileNames, float frameDuration,
			boolean loop) {
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		
		for (int i = 0; i < fileNames.length; i++) {
			String fileName = fileNames[i];
			Texture texture = new Texture(Gdx.files.internal(fileName));
			texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			textureArray.add(new TextureRegion(texture));
		}
		
		Animation<TextureRegion> anim = new Animation<TextureRegion> (frameDuration, textureArray);
		
		if (animation == null)
			setAnimation(anim);
		
		return anim;
	}
	
	/**
	 * Crea una animaci�n basada en una "spritesheet", es decir, un solo archivo que contiene
	 * los diferentes frames de la animaci�n ordenados en filas y columnas
	 * 
	 * @param fileNames Representa el nombre del archivo que contiene los diferentes cuadros de la
	 * imagen
	 * @param rows Representa la cantidad de filas que componen el archivo
	 * @param cols Representa la cantidad de columnas que componen el archivo
	 * @param frameDuration Representa el tiempo que se mantiene en pantalla cada cuadro de la
	 * animaci�n
	 * @param loop Representa si la animaci�n se ejecutar� una sola vez, o se ejecutar� en bucle
	 * [false = se ejecuta una �nica vez, true = se ejecuta en bucle]
	 * @return Devuelve la animaci�n generada
	 */
	public Animation<TextureRegion> loadAnimationFromSheet(String fileName, int rows, int cols,
			float frameDuration, boolean loop) {
		Texture texture = new Texture(Gdx.files.internal(fileName), true);
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		int frameWidth = texture.getWidth() / cols;
		int frameHeight = texture.getHeight() / rows;
		
		TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight);
		
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				textureArray.add(temp[i][j]);
		
		Animation<TextureRegion> anim = new Animation<TextureRegion>(frameDuration, textureArray);
		
		if (loop)
			anim.setPlayMode(Animation.PlayMode.LOOP);
		else
			anim.setPlayMode(Animation.PlayMode.NORMAL);
		
		if (animation == null)
			setAnimation(anim);
		
		return anim;
	}
	
	/**
	 * En caso de que el actor no posea una animaci�n, si no una im�gen est�tica, este m�todo
	 * establece dicha imagen como la "animacion" del actor
	 * 
	 * @param fileName Representa el nombre del archivo que contiene la imagen deseada para el
	 * actor
	 * @return Devuelve la "animacion" (en este caso la im�gen est�tica) del actor
	 */
	public Animation<TextureRegion> loadTexture(String fileName) {
		String[] fileNames = new String[1];
		fileNames[0] = fileName;
		return loadAnimationFromFiles(fileNames, 1, true);
	}
	
	/*
	 * Devuelve, en caso de que la animaci�n del actor no se ejecute en bucle, true si esta se sigue
	 * ejecutando, o false en caso contrario
	 */
	public boolean isAnimationFinished() {
		return animation.isAnimationFinished(elapsedTime);
	}
	
	/**
	 * Establece la velocidad de movimiento del actor
	 * 
	 * @param speed Representa la velocidad deseada en pixeles por segundo
	 */
	public void setSpeed(float speed) {
		// En caso de que la longitud sea 0, se asume que el angulo de movimiento es 0
		if (velocityVec.len() == 0)
			velocityVec.set(speed, 0);
		else
			velocityVec.setLength(speed);
	}
	
	/**
	 * Devuelve la velocidad actual del actor en pixeles por segundo
	 * 
	 * @return float que representa la velocidad del actor
	 */
	public float getSpeed() {
		return velocityVec.len();
	}
	
	/**
	 * Establece el �ngulo en el que se mover� el jugador (en grados)
	 * 
	 * @param angle Representa el �ngulo en el que se mover� el actor
	 */
	public void setMotionAngle(float angle) {
		velocityVec.setAngle(angle);
	}
	
	/**
	 * Devuelve el �ngulo al que est� apuntando el actor
	 * 
	 * @return float que representa el �ngulo al que apunta el actor
	 */
	public float getMotionAngle() {
		return velocityVec.angle();
	}
	
	/**
	 * Determina si el actor est� en movimiento o no, bas�ndose en su velocidad
	 * actual
	 * 
	 * @return boolean que representa si el actor est� o no en movimiento
	 * [true = esta en movimiento, false = esta quieto]
	 */
	public boolean isMoving() {
		return (getSpeed() > 0);
	}
	
	/**
	 * Establece la aceleraci�n deseada para el actor
	 * 
	 * @param acceleration Representa la aceleraci�n
	 */
	public void setAcceleration(float acceleration) {
		this.acceleration = acceleration;
	}
	
	/**
	 * Acelera el actor en el �ngulo deseado
	 * 
	 * @param angle Representa el �ngulo en el que se desea acelerar
	 */
	public void accelerateAtAngle(float angle) {
		accelerationVec.add(new Vector2(acceleration, 0).setAngle(angle));
	}
	
	/**
	 * Acelera el actor en la direcci�n a la que est� apuntando actualmente
	 * 
	 */
	public void accelerateForward() {
		accelerateAtAngle(getRotation());
	}
	
	/**
	 * Establece la velocidad m�xima a la que puede desplazarse el actor
	 * 
	 * @param maxSpeed Representa la velocidad m�xima
	 */
	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	/**
	 * Establece el factor de deceleraci�n del actor
	 * 
	 * @param deceleration Representa la deceleraci�n
	 */
	public void setDeceleration(float deceleration) {
		this.deceleration = deceleration;
	}
	
	/**
	 * Realiza todas las funciones relacionadas con la velocidad y la aceleraci�n,
	 * lo que incluye:
	 * - Ajustar el vector de velocidad bas�ndose en el vector de aceleraci�n
	 * - Si el objeto no esta acelerando, aplicar la deceleraci�n a la velocidad actual
	 * - Asegurarse de que la velocidad no supera el valor de velocidad m�xima
	 * - Ajustar la posici�n del actor bas�ndose en el vector de velocidad
	 * - Reiniciar el vector de aceleraci�n
	 * 
	 * @param dt Representa el "delta time" (Tiempo de ejecuci�n del �ltimo frame)
	 */
	public void applyPhisics(float dt) {
		// Aplicar aceleraci�n
		velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);
		
		float speed = getSpeed();
		
		// Reducir velocidad (decelerar) mientras no se este acelerando
		if (accelerationVec.len() == 0)
			speed -= deceleration * dt;
		
		// Matener la velocidad dentro de los m�rgenes
		speed = MathUtils.clamp(speed, 0, maxSpeed);
		
		// Actualizar velocidad
		setSpeed(speed);
		
		// Aplicar velocidad
		moveBy(velocityVec.x * dt, velocityVec.y * dt);
		
		// Reiniciar aceleraci�n
		accelerationVec.set(0, 0);
	}
	
	/**
	 * Genera un rect�ngulo de colisiones bas�ndose en el ancho y el alto del actor
	 */
	public void setBoundaryRectangle() {
		float width = getWidth();
		float height = getHeight();
		float[] vertices = {0,0, width,0, width,height, 0,height};
		boundaryPolygon = new Polygon(vertices);
	}
	
	/**
	 * Genera un rect�ngulo de colisiones aproximado a una elipse, Ocupando todo el espacio
	 * posible dentro de un rect�ngulo del mismo ancho y alto (Deben ser previamente establecidos).
	 * La definici�n del pol�gono (n�mero de lados) depender� del valor recibido
	 * 
	 * @param numSides Representa la cantidad de caras deseada para el pol�gono
	 * @param extra Se utiliza para indicar si deseas que el poligono de colisi�n sea un 50% m�s grande
	 */
	public void setBoundaryPolygon(int numSides, boolean extra) {
		float width = 0;
		float height = 0;
		
		if (extra) {
			width = getWidth() + (getWidth() / 8);
			height = getHeight() + (getHeight() / 8);
		} else {
			width = getWidth();
			height = getHeight();
		}
		
		float[] vertices = new float[2 * numSides];
		for (int i = 0;  i < numSides; i++) {
			float angle = i * 6.28f / numSides;
			
			// Coordenada X
			vertices[2 * i] = width / 2 * MathUtils.cos(angle) + width / 2;
			
			// Coordenada Y
			vertices[2 * i + 1] = height / 2 * MathUtils.sin(angle) + height / 2;
		}
		boundaryPolygon = new Polygon(vertices);
	}
	
	/**
	 * Devuelve el pol�gono de colisi�n del actor, ajust�ndolo a los actuales parametros de
	 * posici�n, origen, rotaci�n y escala
	 * 
	 * @return Pol�gono de colisi�n del actor
	 */
	public Polygon getBoundaryPolygon() {
		boundaryPolygon.setPosition(getX(), getY());
		boundaryPolygon.setOrigin(getOriginX(), getOriginY());
		boundaryPolygon.setRotation(getRotation());
		boundaryPolygon.setScale(getScaleX(), getScaleY());
		return boundaryPolygon;
	}
	
	/**
	 * Comprueba si el pol�gono de este actor solapa con el pol�gono del actor especificado
	 * 
	 * @param otherActor Representa al segundo actor con el que se desea comprobar la colisi�n
	 * @return boolean que determina si colisionan o no [true = colisionan, false = no colisionan]
	 */
	public boolean overlaps(BaseActor otherActor) {
		Polygon polygon1 = this.getBoundaryPolygon();
		Polygon polygon2 = otherActor.getBoundaryPolygon();
		
		// Comprobaci�n inicial para mejorar el rendimiento
		if (!polygon1.getBoundingRectangle().overlaps(polygon2.getBoundingRectangle()))
			return false;
		
		return Intersector.overlapConvexPolygons(polygon1, polygon2);
	}
	
	/**
	 * Comprueba si el pol�gono de este actor solapa con el pol�gono del actor especificado y
	 * en caso afirmativo, desplaza al actor a la ultima posici�n conocida en la que no solapaba
	 * con el actor especificado
	 * 
	 * @param otherActor Representa al segundo actor con el que se desa comprobar la colisi�n
	 * @return mtv.normal en caso de una colisi�n, o null si no la hay
	 */
	public Vector2 preventOverlap(BaseActor otherActor) {
		Polygon polygon1 = this.getBoundaryPolygon();
		Polygon polygon2 = otherActor.getBoundaryPolygon();
		
		// Comprobaci�n inicial para mejorar el rendimiento
		if (!polygon1.getBoundingRectangle().overlaps(polygon2.getBoundingRectangle()))
			return null;
		
		MinimumTranslationVector mtv = new MinimumTranslationVector();
		boolean polygonOverlap = Intersector.overlapConvexPolygons(polygon1, polygon2, mtv);
		
		if(!polygonOverlap)
			return null;
		
		this.moveBy(mtv.normal.x * mtv.depth, mtv.normal.y * mtv.depth);
		return mtv.normal;
	}
	
	/**
	 * Centra al actor en las coordenadas especificadas
	 * 
	 * @param posX Representa la coordenada X (eje horizontal)
	 * @param posY Representa la coordenada Y (eje vertical)
	 */
	public void centerAtPosition(float posX, float posY) {
		setPosition(posX - getWidth() / 2, posY - getHeight() / 2);
	}
	
	/**
	 * Centra el actor en la posici�n de otro actor especificado
	 * 
	 * @param otherActor Representa al actor con el que se desea centrar
	 */
	public void centerAtActor(BaseActor otherActor) {
		centerAtPosition(otherActor.getX() + otherActor.getWidth() / 2, otherActor.getY()
				+ otherActor.getHeight() / 2);
	}
	
	/**
	 * Establece la opacidad (canal alfa) del actor
	 * 
	 * @param opacity Representa el valor de opacidad (0 - 255)
	 */
	public void setOpacity(float opacity) {
		this.getColor().a = opacity;
	}
	
	/**
	 * Establece los l�mites del mundo (Las barreras que delimitan hasta donde puede desplazarse
	 * el actor) mediante dos valores n�mericos
	 * 
	 * @param width Representa el ancho deseado para la barrera del mundo
	 * @param height Representa el alto deseado para la barrera del mundo
	 */
	public static void setWorldBounds(float width, float height) {
		worldBounds = new Rectangle(0, 0, width, height);
	}
	
	/**
	 * Establece los l�mites del mundo (Las barreras que delimitan hasta donde puede desplazarse
	 * el actor) bas�ndose en la anchura y altura del actor especificado
	 * 
	 * @param actor Representa al actor del que se obtendr� el ancho y el alto
	 */
	public static void setWorldBounds(BaseActor actor) {
		setWorldBounds(actor.getWidth(), actor.getHeight());
	}
	
	/**
	 * Comprueba que el actor se encuentra dentro de los l�mites del mundo, y lo devuelve a su
	 * posici�n anterior en caso de exceder dichos l�mites
	 */
	public void boundToWorld() {
		// Comprobar l�mite izquierdo
		if (getX() < 20)
			setX(20);
		// Comprobar l�mite derecho
		if (getX() + getWidth() + 20 > worldBounds.width)
			setX(worldBounds.width - getWidth() - 20);
		// Comprobar l�mite inferior
		if (getY() < 50)
			setY(50);
		// Comprobar l�mite superior
		if (getY() + getHeight() + 20 > worldBounds.height)
			setY(worldBounds.height - getHeight() - 20);
	}
	
	/**
	 * Mantiene el campo de visi�n de la camara dentro de los l�mites del mundo
	 */
	public void alignCamera() {
		Camera camera = this.getStage().getCamera();
		
		// Centrar la c�mara en el actor
		camera.position.set(this.getX() + this.getOriginX(), this.getY() + this.getOriginY(), 0);
		
		// Atar c�mara al actor
		camera.position.x = MathUtils.clamp(camera.position.x, camera.viewportWidth / 2,
				worldBounds.width - camera.viewportWidth / 2);
		camera.position.y = MathUtils.clamp(camera.position.y, camera.viewportHeight / 2,
				worldBounds.height - camera.viewportHeight / 2);
		camera.update();
	}
}
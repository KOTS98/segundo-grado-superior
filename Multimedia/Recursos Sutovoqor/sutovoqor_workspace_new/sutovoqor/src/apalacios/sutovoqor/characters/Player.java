package apalacios.sutovoqor.characters;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Representa al personaje principal del juego, qui�n ser� controlado por el jugador
 * 
 * @author KOTS98
 */
public class Player extends BaseActor {
	
	private Animation north;
	private Animation south;
	private Animation east;
	private Animation west;
	
	// Atributos de juego
	float life;
	float damage;
	float moveSpeed;
	float shotSpeed;
	float evadeRate;
	int keys;
	int coins;
	int score;
	
	private long lastKnife;
	public long lastHit;
	public boolean shield;
	
	public enum Weapon{
		KNIFE,
		BATHLET,
		RIFLE;
	}
	
	public Weapon weapon;
	public boolean biggerProyectiles;
	
	// Sonido de esquivar
	
	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param posX Representa su posici�n en el eje horizontal
	 * @param posY Representa su posici�n en el eje vertical
	 * @param stage Stage donde ser� a�adido
	 */
	public Player(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		
		setName("player");
		setLife(6);
		weapon = Weapon.KNIFE;
		biggerProyectiles = false;
		score = 0;
		
		// Carga la animaci�n del jugador
		Texture texture = new Texture(Gdx.files.internal("assets/characters/player/noroq_walk.png"), true);
		int rows = 4;
		int cols = 3;
		int frameHeight = texture.getHeight() / rows;
		int frameWidth = texture.getWidth() / cols;
		float frameDuration = 0.2f;
		
		TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight);
		
		Array<TextureRegion> textureArray = new Array<TextureRegion>();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[0][i]);
		south = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[1][i]);
		west = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[2][i]);
		east = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		textureArray.clear();
		for (int i = 0; i < cols; i++)
			textureArray.add(temp[3][i]);
		north = new Animation(frameDuration, textureArray, Animation.PlayMode.LOOP_PINGPONG);
		
		setAnimation(south);
		
		// Crea el pol�gono para colisiones
		setBoundaryPolygon(8, false);
		
		// Ajusta la velocidad y aceleraci�n
		setAcceleration(1000);
		setMaxSpeed(290);
		setDeceleration(1000);
		
		// Ajusta el tiempo para el �ltimo cuchillo lanzado
		lastKnife = System.currentTimeMillis();
		lastHit = lastKnife;
		
		// Items
		shield = false;
	}
	
	public float getLife() {
		return life;
	}

	public void setLife(float life) {
		this.life = life;
	}

	public float getDamage() {
		return damage;
	}

	public void setDamage(float damage) {
		this.damage = damage;
	}

	public float getMoveSpeed() {
		return moveSpeed;
	}

	public void setMoveSpeed(float moveSpeed) {
		this.moveSpeed = moveSpeed;
	}

	public float getShotSpeed() {
		return shotSpeed;
	}

	public void setShotSpeed(float shotSpeed) {
		this.shotSpeed = shotSpeed;
	}

	public float getEvadeRate() {
		return evadeRate;
	}

	public void setEvadeRate(float evadeRate) {
		this.evadeRate = evadeRate;
	}

	public int getKeys() {
		return keys;
	}

	public void setKeys(int keys) {
		this.keys = keys;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}
	
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public void takeDamage() {
		long currentTime = System.currentTimeMillis();
		if (lastHit - currentTime < -1500) {
			if (score - 250 >= 0) {
				score -= 250;
			}
			
			Random rand = new Random();
			int rNumber = rand.nextInt(100) + 1;
			if (rNumber >= evadeRate) {
				lastHit = currentTime;
				this.life -= 1;
				clearActions();
				addAction(Actions.fadeOut(0.1f));
				addAction(Actions.after(Actions.fadeIn(0.1f)));
				addAction(Actions.after(Actions.fadeOut(0.1f)));
				addAction(Actions.after(Actions.fadeIn(0.1f)));
				addAction(Actions.after(Actions.fadeOut(0.1f)));
				addAction(Actions.after(Actions.fadeIn(0.1f)));
				addAction(Actions.after(Actions.fadeOut(0.1f)));
				addAction(Actions.after(Actions.fadeIn(0.1f)));
				addAction(Actions.after(Actions.fadeOut(0.1f)));
				addAction(Actions.after(Actions.fadeIn(0.1f)));
				addAction(Actions.after(Actions.fadeOut(0.1f)));
				addAction(Actions.after(Actions.fadeIn(0.1f)));
				addAction(Actions.after(Actions.fadeOut(0.1f)));
				addAction(Actions.after(Actions.fadeIn(0.1f)));
			} else {
				lastHit = currentTime;
				clearActions();
				addAction(Actions.rotateBy(360, 0.3f));
				if (Gdx.input.isKeyPressed(Keys.W)) {
					addAction(Actions.moveBy(0, -100, 0.3f));
				}
				if (Gdx.input.isKeyPressed(Keys.S)) {
					addAction(Actions.moveBy(0, 100, 0.3f));
				}
				if (Gdx.input.isKeyPressed(Keys.A)) {
					addAction(Actions.moveBy(100, 0, 0.3f));
				}
				if (Gdx.input.isKeyPressed(Keys.D)) {
					addAction(Actions.moveBy(-100, 0, 0.3f));
				}
			}
		}
	}

	public void act(float dt) {
		super.act(dt);
		
		// Pausar la animaci�n del jugador si esta quieto
		if (getSpeed() == 0)
			setAnimationPaused(true);
		else {
			// Reanudar la animaci�n
			setAnimationPaused(false);
		}
		
		// Movimiento hacia arriba
		if (Gdx.input.isKeyPressed(Keys.W)) {
			setAnimation(north);
			accelerateAtAngle(90);
		}
		
		// Movimiento hacia abajo
		if (Gdx.input.isKeyPressed(Keys.S)) {
			setAnimation(south);
			accelerateAtAngle(270);
		}
		
		// Movimiento hacia la izquierda
		if (Gdx.input.isKeyPressed(Keys.A)) {
			setAnimation(west);
			accelerateAtAngle(180);
		}
		
		// Movimiento hacia la derecha
		if (Gdx.input.isKeyPressed(Keys.D)) {
			setAnimation(east);
			accelerateAtAngle(0);
		}
		
		// Disparar hacia arriba
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			setAnimation(north);
			shoot(90);
		}
		
		// Disparar hacia abajo
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			setAnimation(south);
			shoot(270);
		}
				
		// Disparar hacia la izquierda
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			setAnimation(west);
			shoot(180);
		}
				
		// Disparar hacia la derecha
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			setAnimation(east);
			shoot(0);
		}
		
		applyPhisics(dt);
		
		// Comprobar que el jugador no sale del mundo
		boundToWorld();
	}
	
	public void shoot(float angle) {
		if (getStage() == null)
			return;
		
		long currentTime = System.currentTimeMillis();
		if (lastKnife - currentTime < - getShotSpeed()) {
			lastKnife = currentTime;
			Proyectile proyectile = null;
			switch(weapon) {
			case KNIFE:
				proyectile = new Knife(getX() + 20, getY(), getStage(), biggerProyectiles);
				break;
			case BATHLET:
				proyectile = new Bathlet(getX() + 20, getY(), getStage(), biggerProyectiles);
				break;
			case RIFLE:
				proyectile = new Laser(getX() + 20, getY(), getStage(), biggerProyectiles);
				break;
			}
				
			switch(String.valueOf(angle)) {
			case "0.0":
				proyectile.setPosition(getX() + 20, getY());
				break;
			case "90.0":
				proyectile.setPosition(getX(), getY() + 25);
				break;
			case "180.0":
				proyectile.setPosition(getX() - 20, getY());
				break;
			case "270.0":
				proyectile.setPosition(getX(), getY() - 20);
				break;
			}
			proyectile.setRotation(angle);
			proyectile.setMotionAngle(angle);
		}
	}
	
	public void acquireCoin() {
		coins += 1;
	}
	
	public void acquireKey() {
		keys += 1;
	}
	
	public void acquireHearth() {
		if (life + 2 <= 20) {
			life += 2;
		}
	}
	
	public void loseCoins(int coins) {
		this.coins = coins;
	}
	
	public void loseKey() {
		keys -= 1;
	}
	
	public void loseLife(int life) {
		this.life -= life;
	}
}

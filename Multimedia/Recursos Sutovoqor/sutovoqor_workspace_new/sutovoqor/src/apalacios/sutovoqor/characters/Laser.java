package apalacios.sutovoqor.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.audio.Ogg.Sound;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Laser extends Proyectile {
	
	private Sound pewSound = (Sound) Gdx.audio.newSound(Gdx.files.internal("assets/sound/laser.ogg"));

	public Laser(float posX, float posY, Stage stage, boolean big) {
		super(posX, posY, stage);
		setName("knife");
		
		loadTexture("assets/characters/items/laser.png");
		if (big) {
			setSize(70, 15);
		}
		
		addAction(Actions.delay(1));
		addAction(Actions.after(Actions.fadeOut(0)));
		addAction(Actions.after(Actions.removeActor()));
		
		setSpeed(800);
		setMaxSpeed(800);
		setDeceleration(0);
		setBoundaryPolygon(8, false);
		
		pewSound.play(0.5f);
	}
	
	public void act(float dt) {
		super.act(dt);
		applyPhisics(dt);
	}
}

package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class VisualEffect extends BaseActor {
	public VisualEffect(float cordX, float cordY, String texture, int rows, int cols, float time, Stage stage) {
		super(cordX , cordY,stage);
		loadAnimationFromSheet(texture, rows, cols, time, false);
		setName("visualEffect");
	}
	
	public void act(float dt) {
		super.act(dt);
		if (isAnimationFinished())
			remove();
	}
}

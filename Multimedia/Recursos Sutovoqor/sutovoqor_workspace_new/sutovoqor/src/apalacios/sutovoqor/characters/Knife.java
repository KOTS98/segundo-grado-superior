package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Knife extends Proyectile {

	public Knife(float posX, float posY, Stage stage, boolean big) {
		super(posX, posY, stage);
		setName("knife");
		
		loadTexture("assets/characters/items/cuchillo.png");
		
		if (big) {
			setSize(70, 30);
		}
		
		addAction(Actions.delay(1));
		addAction(Actions.after(Actions.fadeOut(0.5f)));
		addAction(Actions.after(Actions.removeActor()));
		
		setSpeed(400);
		setMaxSpeed(400);
		setDeceleration(0);
		setBoundaryPolygon(8, false);
	}
	
	public void act(float dt) {
		super.act(dt);
		applyPhisics(dt);
	}
}

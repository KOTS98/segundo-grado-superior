package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Coin extends Collectable {
private boolean collected;
	
	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param posX Representa su posici�n en el eje horizontal
	 * @param posY Representa su posici�n en el eje vertical
	 * @param stage Represente el stage al que ser� a�adido
	 */
	public Coin(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		
		// Establece la textura del item
		loadTexture("assets/characters/items/moneda.png");
		
		// Crea el pol�gono para colisiones
		setBoundaryPolygon(8, false);
		
		// Otras propiedades
		collected = false;
		
		this.setName("coin");
	}
	
	/**
	 * Determina si el objeto ha sido recogido o no
	 * 
	 * @return Devuelve true si ha sido recogido, false en caso contrario
	 */
	public boolean isCollected() {
		return collected;
	}
	
	/**
	 * Elimina el item al ser recogido
	 */
	@Override
	public void collect(Player player) {
		if (!collected) {
			collected = true;
			clearActions();
			addAction(Actions.fadeOut(0.05f));
			addAction(Actions.after(Actions.removeActor()));
			player.acquireCoin();
		}
	}
}

package apalacios.sutovoqor.characters;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Barrel extends Mobile {
	private boolean explosive;
	private boolean destroyed;
	private int hits;

	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param posX Representa su posici�n en el eje horizontal
	 * @param posY Representa su posici�n en el eje vertical
	 * @param stage Represente el stage al que ser� a�adido
	 */
	public Barrel(float posX, float posY, Stage stage, boolean explosive) {
		super(posX, posY, stage);
		this.explosive = explosive;
		this.destroyed = false;
		this.hits = 0;
		this.setName("barrel");
		
		// Establece la textura del item
		if (explosive)
			loadTexture("assets/characters/barrels/barril_explosivo.png");
		else {
			loadTexture("assets/characters/barrels/barril.png");
		}
		
		setSize(50, 50);
		
		// Crea el pol�gono para colisiones
		setBoundaryPolygon(8, false);
	}
	
	public boolean isExplosive() {
		return explosive;
	}
	
	public boolean isDestroyed() {
		return destroyed;
	}
	
	public void setHits(int hits) {
		this.hits = hits;
	}
	
	public void reciveDamage(Player player) {
		hits += 1;
		
		setSize(getWidth() + 10, getHeight() - 5);
		setPosition(getX() -5, getY());
		setBoundaryPolygon(8, false);
		
		if (hits >= 3) {
			player.setScore(player.getScore() + 100);
			destroyed = true;
			setAnimation(loadTexture("assets/characters/barrels/restos_barril.png"));
			setPosition(getX() + 20, getY());
			addAction(Actions.fadeOut(1.5f));
			addAction(Actions.after(Actions.removeActor()));
			if (!explosive) {
				switch (new Random().nextInt(10) + 1) {
				case 1:
				case 2:
					new Coin(getX(), getY(), getStage());
					break;
				case 3:
					new Hearth(getX(), getY(), getStage(), false);
					break;
				}
			}
		}
	}
}

package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Trapdoor extends BaseActor {
	private boolean open;

	public Trapdoor(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		
		setName("door");
		open = false;
		setAnimation(loadTexture("assets/characters/doors/trampilla_cerrada.png"));
		setBoundaryPolygon(8, false);
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
		
		if (open) {
			setAnimation(loadTexture("assets/characters/doors/trampilla.png"));
		} else {
			setAnimation(loadTexture("assets/characters/doors/trampilla_cerrada.png"));
		}
	}
}

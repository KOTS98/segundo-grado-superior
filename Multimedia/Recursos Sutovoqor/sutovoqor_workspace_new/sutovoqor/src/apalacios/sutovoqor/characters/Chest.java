package apalacios.sutovoqor.characters;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Chest extends Solid {
	private boolean opened;
	
	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param posX Representa su posici�n en el eje horizontal
	 * @param posY Representa su posici�n en el eje vertical
	 * @param stage Represente el stage al que ser� a�adido
	 */
	public Chest(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		
		opened = false;

		// Establece la textura del cofre
		loadTexture("assets/characters/chest/cofre_cerrado.png");
		
		// Crea el rect�ngulo para colisiones
		setBoundaryRectangle();
		
		this.setName("chest");
		this.setSize(50, 45);
	}
	
	public boolean isOpened() {
		return opened;
	}
	
	public void open() {
		if (!opened) {
			opened = true;
			setAnimation(loadTexture("assets/characters/chest/cofre_abierto.png"));
			setSize(50, 45);
			
			generateRandomItem();
		}
	}
	
	private void generateRandomItem() {
		switch (new Random().nextInt(10) + 1) {
		case 1:
			for (int i = 0; i < 5; i++) {
				Coin coin = new Coin(getX(), getY(), getStage());
				coin.preventOverlap(this);
			}
			break;
			
		case 2:
			Hearth hearth = new Hearth(getX(), getY(), getStage(), false);
			hearth.preventOverlap(this);
			break;
			
		case 3:
			Key key = new Key(getX(), getY(), getStage(), false);
			key.preventOverlap(this);
			break;
			
		default:
			Coin coin = new Coin(getX(), getY(), getStage());
			coin.preventOverlap(this);
			break;
		}
	}
}

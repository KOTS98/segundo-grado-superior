package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class Collectable extends BaseActor {

	public Collectable(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
	}
	
	public abstract void collect(Player player);
}

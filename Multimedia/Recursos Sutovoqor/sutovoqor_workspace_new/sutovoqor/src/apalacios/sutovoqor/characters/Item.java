package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import apalacios.sutovoqor.characters.Player.Weapon;

/**
 * Representa un objeto o "power up" dentro del juego
 * 
 * @author KOTS98
 */
public class Item extends Collectable {
	private long itemStartTime;
	private boolean collected;
	private int id;
	
	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param posX Representa su posici�n en el eje horizontal
	 * @param posY Representa su posici�n en el eje vertical
	 * @param stage Represente el stage al que ser� a�adido
	 */
	public Item(float posX, float posY, int id, Stage stage) {
		super(posX, posY, stage);
		this.id = id;
		setName("item");
		
		// Establece la textura del item
		loadTextureById();
		loadTexture("assets/characters/items/escudo_personal_mk1.png");
		
		// Crea el pol�gono para colisiones
		setBoundaryPolygon(8, false);
		
		// Otras propiedades
		collected = false;
		itemStartTime = System.currentTimeMillis();
	}
	
	public int getId() {
		return id;
	}
	
	/**
	 * Determina si el objeto ha sido recogido o no
	 * 
	 * @return Devuelve true si ha sido recogido, false en caso contrario
	 */
	public boolean isCollected() {
		return collected;
	}
	
	public void act(float dt) {
		super.act(dt);
		
		long currentTime = System.currentTimeMillis();
		if (itemStartTime - currentTime < -2000) {
			itemStartTime = currentTime;
						
			Action itemUp = Actions.moveBy(0, 10, 1);
			Action itemDown = Actions.moveBy(0, -10, 1);
			addAction(Actions.after(itemUp));
			addAction(Actions.after(itemDown));
			removeAction(itemUp);
			removeAction(itemDown);
		}
	}
	
	private void loadTextureById() {
		switch (id) {
		case 1:
			loadTexture("assets/characters/items/bathlet.png");
			break;
		case 2:
			loadTexture("assets/characters/items/brazo_protesico_borg.png");
			break;
		case 3:
			loadTexture("assets/characters/items/conversor_de_materia_energia.png");
			break;
		case 4:
			loadTexture("assets/characters/items/discriminador_de_fase.png");
			break;
		case 5:
			loadTexture("assets/characters/items/escudo_personal_mk1.png");
			break;
		case 6:
			loadTexture("assets/characters/items/generador_de_campo_estatico.png");
			break;
		case 7:
			loadTexture("assets/characters/items/rifle_disrruptor.png");
			break;
		case 8:
			loadTexture("assets/characters/items/tarta_de_sangre.png");
			break;
		}
	}
	
	/**
	 * Elimina el item al ser recogido
	 */
	@Override
	public void collect(Player player) {
		collected = true;
		VisualEffect vf = new VisualEffect(0, 0, "assets/characters/items/coger_objeto.png",
				6, 5, 0.03f, getStage());
		vf.centerAtActor(this);
		vf.setOpacity(0.25f);
		clearActions();
		switch (id) {
		case 1:
			player.setDamage(player.getDamage() + 10);
			player.weapon = Weapon.BATHLET;
			break;
		case 2:
			player.setShotSpeed(player.getShotSpeed() -200);
			break;
		case 4:
			player.setEvadeRate(player.getEvadeRate() + 20);
			break;
		case 5 :
			player.shield = true;
			break;
		case 7:
			player.setDamage(player.getDamage() / 2);
			player.setShotSpeed(player.getShotSpeed() / 2);
			player.weapon = Weapon.RIFLE;
			break;
		case 8:
			player.biggerProyectiles = true;
			player.setDamage(player.getDamage() + 5);
			break;
		}
		
		addAction(Actions.fadeOut(0));
		addAction(Actions.removeActor());
	}
}

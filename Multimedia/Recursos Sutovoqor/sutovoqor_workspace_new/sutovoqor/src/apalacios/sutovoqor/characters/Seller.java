package apalacios.sutovoqor.characters;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Seller extends Solid {

	public Seller(float posX, float posY, Stage stage) {
		super(posX, posY, stage);
		loadTexture("assets/characters/enmies/vendedor.png"); // Cambiar imagen
		setBoundaryPolygon(8, false);
		setSize(50, 50);
		setName("Seller");
	}
}

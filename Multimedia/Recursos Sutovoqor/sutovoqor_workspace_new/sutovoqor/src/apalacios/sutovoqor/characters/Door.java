package apalacios.sutovoqor.characters;

import java.util.Random;

import com.badlogic.gdx.scenes.scene2d.Stage;

public class Door extends BaseActor {
	private boolean randomTexture1;
	private boolean open;
	public enum DoorType {
		NORMAL, DARK, BOSS_1, BOSS_2
	}
	public enum Orientation {
		UP, DOWN, LEFT, RIGHT
	}
	private Orientation orientation;
	private DoorType doorType;
	
	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param posX Representa su posici�n en el eje horizontal
	 * @param posY Representa su posici�n en el eje vertical
	 * @param stage Represente el stage al que ser� a�adido
	 */
	public Door(float posX, float posY, Stage stage, DoorType doorType, Orientation orientation) {
		super(posX, posY, stage);
		
		setName("door");
		
		this.doorType = doorType;
		this.randomTexture1 = false;
		this.orientation = orientation;
		this.open = true;
		
		switch (doorType) {
		case NORMAL:
			loadRandomTexture();
			break;
		case DARK:
			loadTexture("assets/characters/doors/puerta_3.png");
			break;
		case BOSS_1:
			loadTexture("assets/characters/doors/puerta_boss_1.png");
			break;
		case BOSS_2:
			loadTexture("assets/characters/doors/puerta_boss_2.png");
			break;
		}
		
		// Establece su posici�n y orientaci�n
		setPositionAndOrientation();
		
		// Crea el pol�gono para colisiones
		setBoundaryPolygon(8, false);
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public Orientation getOrientation() {
		return orientation;
	}
	
	private void loadRandomTexture() {
		Random random = new Random();
		switch(random.nextInt(2) + 1) {
		case 1:
			loadTexture("assets/characters/doors/puerta_1.png");
			randomTexture1 = true;
			break;
		case 2:
			loadTexture("assets/characters/doors/puerta_2.png");
		}
	}
	
	private void setPositionAndOrientation() {
		switch (orientation) {
		case UP:
			setRotation(0);
			setSize(getWidth() + 10, getHeight());
			setPosition(400 - getWidth() / 2, 545);
			break;
		case DOWN:
			setRotation(180);
			setSize(getWidth() + 10, getHeight());
			setPosition(400 - getWidth() / 2, 0);
			break;
		case LEFT:
			setRotation(90);
			setSize(getWidth() + 10, getHeight() -10);
			setPosition(-25, 300 - getHeight() / 2);
			break;
		case RIGHT:
			setRotation(270);
			setSize(getWidth() + 10, getHeight() -10);
			setPosition(742, 300 - getHeight() / 2);
			break;
		}
	}
	
	public void closeDoor() {
		open = false;
		switch (doorType) {
		case NORMAL:
			if (randomTexture1) {
				setAnimation(loadTexture("assets/characters/doors/puerta_1_cerrada.png"));
			} else {
				setAnimation(loadTexture("assets/characters/doors/puerta_2_cerrada.png"));
			}
			break;
		case DARK:
			setAnimation(loadTexture("assets/characters/doors/puerta_3_cerrada.png"));
			break;
		case BOSS_1:
			setAnimation(loadTexture("assets/characters/doors/puerta_boss_1_cerrada.png"));
			break;
		case BOSS_2:
			setAnimation(loadTexture("assets/characters/doors/puerta_boss_2_cerrada.png"));
			break;
		}
		setPositionAndOrientation();
	}
	
	public void openDoor() {
		open = true;
		switch (doorType) {
		case NORMAL:
			if (randomTexture1) {
				setAnimation(loadTexture("assets/characters/doors/puerta_1.png"));
			} else {
				setAnimation(loadTexture("assets/characters/doors/puerta_2.png"));
			}
			break;
		case DARK:
			setAnimation(loadTexture("assets/characters/doors/puerta_3.png"));
			break;
		case BOSS_1:
			setAnimation(loadTexture("assets/characters/doors/puerta_boss_1.png"));
			break;
		case BOSS_2:
			setAnimation(loadTexture("assets/characters/doors/puerta_boss_2.png"));
			break;
		}
		setPositionAndOrientation();
	}
}

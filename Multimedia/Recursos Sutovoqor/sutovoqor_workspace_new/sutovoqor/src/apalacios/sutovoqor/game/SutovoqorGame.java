package apalacios.sutovoqor.game;

import apalacios.sutovoqor.screens.MenuScreen;

public class SutovoqorGame extends BaseGame {
	
	@Override
	public void create() {
		super.create();
		setActiveScreen(new MenuScreen());
	}
}

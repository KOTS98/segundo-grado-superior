package apalacios.sutovoqor.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

import apalacios.sutovoqor.screens.BaseScreen;

public abstract class BaseGame extends Game {
	
	private static BaseGame game;
	public static LabelStyle labelStyle;
	
	
	public BaseGame() {
		game = this;
	}
	
	/**
	 * Establece la pantalla especificada como pantalla actual
	 * 
	 * @param screen Representa la pantalla que se desea establecer
	 */
	public static void setActiveScreen(BaseScreen screen) {
		game.setScreen(screen);
	}
	
	public void create() {
		labelStyle = new LabelStyle();
		
		FreeTypeFontGenerator fontGenerator
		= new FreeTypeFontGenerator(Gdx.files.internal("assets/text/astonished.ttf"));
		
		FreeTypeFontParameter fontParameters = new FreeTypeFontParameter();
		fontParameters.size = 25;
		fontParameters.color = Color.WHITE;
		fontParameters.borderWidth = 2;
		fontParameters.borderColor = Color.BLACK;
		fontParameters.borderStraight = true;
		fontParameters.minFilter = TextureFilter.Linear;
		fontParameters.magFilter = TextureFilter.Linear;
		
		BitmapFont customFont = fontGenerator.generateFont(fontParameters);
		labelStyle.font = customFont;
	}
}

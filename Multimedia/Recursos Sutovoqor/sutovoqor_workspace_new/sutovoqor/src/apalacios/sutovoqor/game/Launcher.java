package apalacios.sutovoqor.game;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

/**
 * Lanzador del juego, se encarga de iniciar el juego
 * 
 * @author KOTS98
 */
public class Launcher {
	
	/**
	 * M�todo principal del juego, ejecuta el juego en una ventana
	 * 
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main (String[] args) {
		LwjglApplication launcher = new LwjglApplication(new SutovoqorGame(), "Sutovoqor", 800, 600);
	}
}

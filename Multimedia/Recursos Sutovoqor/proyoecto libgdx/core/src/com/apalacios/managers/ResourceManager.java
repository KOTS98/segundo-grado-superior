package com.apalacios.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class ResourceManager {
	
	private static AssetManager assets = new AssetManager();
	
	public static void loadAllResources() {
		assets.load("characters/player/noroq.pack", TextureAtlas.class);
	}
	
	public static boolean update() {
		return assets.update();
	}
	
	public static TextureAtlas getAtlas(String path) {
		return assets.get(path, TextureAtlas.class);
	}
	
	public static Music getMusic(String path) {
		return assets.get(path, Music.class);
	}
	
	public static Sound getSound(String path) {
		return assets.get(path, Sound.class);
	}
	
	public static void dispose() {
		assets.dispose();
	}
}

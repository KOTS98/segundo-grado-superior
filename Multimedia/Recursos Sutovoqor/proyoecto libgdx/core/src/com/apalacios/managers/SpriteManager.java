package com.apalacios.managers;

import com.apalacios.characters.Enemy;
import com.apalacios.characters.Player;
import com.apalacios.characters.Player.Walking;
import com.apalacios.characters.Wall;
import com.apalacios.sutovoqor.Sutovoqor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;

public class SpriteManager {
	private static Sutovoqor game;
	
	// Batch, c�mara y ShapeRenderer
	private Batch mainBatch;
	private Batch hudBatch;
	private OrthographicCamera camera;
	private ShapeRenderer shapeRenderer;
	
	// Elementos gr�ficos
	public Player player;
	public Array<Enemy> enemies;
	public Array<Wall> walls;
	
	// Elementos del HUD
	
	// Elementos sonoros
	public Music music;
	
	public SpriteManager(Sutovoqor game) {
		this.game = game;
		
		mainBatch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 1024, 600); // Modificar param�tricamente
		
		enemies = new Array<>();
		walls = new Array<>();
		
		loadCurrentLevel();
	}
	
	public Array<Enemy> getEnemies() {
		return enemies;
	}

	public Array<Wall> getWalls() {
		return walls;
	}

	public void loadCurrentLevel() {
		player = new Player(this);
		player.setPosition(10, 100);
		
		// A�adir enemigos
		
		// A�adir paredes
		
		// A�adir m�sica
	}
	
	public void update(float dt) {
		player.update(dt);
		
		for (Enemy enemy : enemies) {
			enemy.update(dt);
		}
		
		camera.position.x = player.getPositionX();
		camera.position.y = player.getPositionY();
		camera.update();
	}
	
	public void draw() {
		mainBatch.setProjectionMatrix(camera.combined);
		mainBatch.begin();
		
		player.render(mainBatch);
		for (Enemy enemy : enemies) {
			enemy.render(mainBatch);
		}
		for (Wall wall : walls) {
			wall.render(mainBatch);
		}
		
		mainBatch.end();
	}
	
	public void handleInput() {
		
		// Mover arriba
		if (Gdx.input.isKeyPressed(Keys.W)) {
			player.walk(Walking.UP);
		}
		
		// Mover abajo
		if (Gdx.input.isKeyPressed(Keys.S)) {
			player.walk(Walking.DOWN);
		}
		
		// Mover izquierda
		if (Gdx.input.isKeyPressed(Keys.A)) {
			player.walk(Walking.LEFT);
		}
		
		// Mover derecha
		if (Gdx.input.isKeyPressed(Keys.D)) {
			player.walk(Walking.RIGHT);
		}
	}
}

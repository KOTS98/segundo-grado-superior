package com.apalacios.characters;

import com.apalacios.managers.SpriteManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class Character {
	protected SpriteManager sm;
	
	private Vector2 position;
	private Vector2 velocity;
	private float width;
	private float height;
	
	protected TextureRegion currentFrame;
	protected float stateTime;
	
	private boolean alive;
	
	private Rectangle rectangle;
	
	public Character(SpriteManager sm) {
		this.sm = sm;
		
		this.position = new Vector2(0, 0);
		this.velocity = new Vector2(0, 0);
		
		this.alive = true;
	}
	
	
	
	public Vector2 getPosition() {
		return position;
	}

	public float getPositionX() {
		return position.x;
	}
	
	public float getPositionY() {
		return position.y;
	}

	public void setPosition(int posX, int posY) {
		this.position = new Vector2(posX, posY);
	}

	public float getVelocityX() {
		return velocity.x;
	}
	
	public float getVelocityY() {
		return velocity.y;
	}

	public void setVelocity(float x, float y) {
		this.velocity = new Vector2(x, y);
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public void setAlive(boolean alive) {
		this.alive = alive;
	}

	public Rectangle getRectangle() {
		return rectangle;
	}
	
	public void setRectangle(Rectangle rectangle) {
		this.rectangle = rectangle;
	}


	public void render(Batch batch) {
		batch.draw(currentFrame, position.x, position.y, width, height);
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public abstract void update(float dt);
	
	public abstract void checkCollisions(SpriteManager sm);
}

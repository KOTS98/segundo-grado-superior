package com.apalacios.characters;

import com.apalacios.managers.SpriteManager;

public abstract class Enemy extends Character{
	
	public float WALKING_SPEED;
	// Aqui van los atributos como fuerza, da�o, vida, etc...

	public Enemy(SpriteManager sm) {
		super(sm);
	}
	
	public abstract void damage(float damage);
}

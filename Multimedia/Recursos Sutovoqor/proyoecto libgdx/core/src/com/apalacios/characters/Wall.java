package com.apalacios.characters;

import com.apalacios.managers.ResourceManager;
import com.apalacios.managers.SpriteManager;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Wall {
	
	private Vector2 position;
	private SpriteManager sm;
	private Rectangle rectangle;
	private TextureRegion currentFrame;
	private int width;
	private int height;
	public enum Location {
		UP, DOWN, LEFT, RIGHT
	}
	private Location location;
	
	public Wall(SpriteManager sm, int posX, int posY, int width, int height, Location location) {
		this.sm = sm;
		position = new Vector2(posX, posY);
		
		TextureAtlas atlas = ResourceManager.getAtlas(""); // Introducir ruta del bloque o plataforma
		if(MathUtils.randomBoolean() == false) {
			currentFrame = atlas.findRegion(""); // Nombre del primer bloque
		} else {
			currentFrame = atlas.findRegion(""); // Nombre del segundo bloque
		}
		
		rectangle = new Rectangle(posX, posY, width, height);
		this.width = width;
		this.height = height;
		this.location = location;
	}
	
	public Location getLocation() {
		return location;
	}

	public void render(Batch spriteBatch) {
		spriteBatch.draw(currentFrame, this.position.x, this.position.y, this.width, this.height);
	}

	public Rectangle getRectangle() {
		return rectangle;
	}
}

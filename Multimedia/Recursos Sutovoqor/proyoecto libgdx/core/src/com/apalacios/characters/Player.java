package com.apalacios.characters;

import com.apalacios.characters.Wall.Location;
import com.apalacios.managers.ResourceManager;
import com.apalacios.managers.SpriteManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

public class Player extends Character {
	
	private boolean isWalking;
	
	public enum Walking {
		IDLE, UP, DOWN, LEFT, RIGHT
	}
	public enum Facing {
		UP, DOWN, LEFT, RIGHT
	}
	private Walking walking;
	private Facing facing;
	
	private ShapeRenderer shapeRenderer;
	
	private static float WALKING_SPEED = 300.0f;
	
	private Animation<TextureRegion> walkUpAnimation;
	private Animation<TextureRegion> walkDownAnimation;
	private Animation<TextureRegion> walkLeftAnimation;
	private Animation<TextureRegion> walkRightAnimation;
	private TextureRegion idle;

	public Player(SpriteManager sm) {
		super(sm);
		
		isWalking = false;
		
		setPosition(0, 0);
		setVelocity(0, 0);
		walking = Walking.IDLE;
		facing = Facing.DOWN;
		
		TextureAtlas atlas = ResourceManager.getAtlas("characters/player/noroq.pack");
		shapeRenderer = new ShapeRenderer();
		
		idle = atlas.findRegion("down", 1);
		
		walkUpAnimation = new Animation<TextureRegion>(0.15f, atlas.findRegions("up"));
		walkDownAnimation = new Animation<TextureRegion>(0.15f, atlas.findRegions("down"));
		walkLeftAnimation = new Animation<TextureRegion>(0.15f, atlas.findRegions("left"));
		walkRightAnimation = new Animation<TextureRegion>(0.15f, atlas.findRegions("right"));
		
		this.setRectangle(new Rectangle(getPositionX(), getPositionY(), getWidth(), getHeight()));
	}

	public Walking getWalking() {
		return walking;
	}

	public Facing getFacing() {
		return facing;
	}
	
	public void walk(Walking direction) {
		walking = direction;
		isWalking = true;
	}
	
	public void  render(Batch spriteBatch) {
		stateTime += Gdx.graphics.getDeltaTime();
		
		switch (walking) {
			case IDLE:
				currentFrame = idle;
				spriteBatch.draw(currentFrame, getPositionX(), getPositionY(), getWidth(), getHeight());
			break;
			
			case UP:
				currentFrame = walkUpAnimation.getKeyFrame(stateTime, true);
				
			break;
		
			case DOWN:
				currentFrame = walkDownAnimation.getKeyFrame(stateTime, true);
			break;
		
			case LEFT:
				currentFrame = walkLeftAnimation.getKeyFrame(stateTime, true);
				spriteBatch.draw(currentFrame, getPositionX(), getPositionY(), getWidth(), getHeight());
			break;
		
			case RIGHT:
				currentFrame = walkRightAnimation.getKeyFrame(stateTime, true);
				spriteBatch.draw(currentFrame, getPositionX() + getWidth(), getPositionY(), -getWidth(), getHeight());
			break;
		}
		spriteBatch.end();
		shapeRenderer.setProjectionMatrix(spriteBatch.getProjectionMatrix());
		shapeRenderer.end();
		spriteBatch.begin();
	}

	@Override
	public void update(float dt) {
		this.checkCollisions(sm);
		this.getPosition().add(getVelocityX() * dt, this.getVelocityX() * dt);
		
		// Caminar
		if (isWalking) {
			switch (walking) {
			case UP:
				
				break;
				
			case DOWN:
				
				break;
				
			case LEFT:
				setVelocity(getVelocityX() - WALKING_SPEED / 5 * dt, getVelocityY());
				break;
			
			case RIGHT:
				setVelocity(getVelocityX() + WALKING_SPEED / 5 * dt, getVelocityY());
				break;
			}
		}
	}

	@Override
	public void checkCollisions(SpriteManager sm) {
		for (Wall wall : sm.getWalls()) {
			if(getRectangle().overlaps(wall.getRectangle())) {
				
				if(wall.getLocation() == Location.UP) { // Pared arriba
					if(this.walking == Walking.UP) {
						this.setVelocity(getVelocityX(), 0);
					} else if(this.walking == Walking.DOWN) {
						this.setVelocity(getVelocityX(), WALKING_SPEED / 5);
					}
					
				} else if (wall.getLocation() == Location.DOWN) { // Pared abajo
					if(this.walking == Walking.DOWN) {
						this.setVelocity(getVelocityX(), 0);
					} else if(this.walking == Walking.UP) {
						this.setVelocity(getVelocityX(), WALKING_SPEED / 5);
					}
				}
				
				if(wall.getLocation() == Location.LEFT) { // Pared izquierda
					if(this.walking == Walking.LEFT) {
						this.setVelocity(0, getVelocityY());
					} else if(this.walking == Walking.RIGHT) {
						this.setVelocity(WALKING_SPEED / 5, getVelocityY());
					}
					
				} else if(wall.getLocation() == Location.RIGHT) { // Pared derecha
					if(this.walking == Walking.RIGHT) {
						this.setVelocity(0, getVelocityY());
					} else if(this.walking == Walking.LEFT) {
						this.setVelocity(WALKING_SPEED / 5, getVelocityY());
					}
				}
			}
		}
	}
}

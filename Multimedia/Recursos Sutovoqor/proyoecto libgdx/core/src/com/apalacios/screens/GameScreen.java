package com.apalacios.screens;

import com.apalacios.managers.ResourceManager;
import com.apalacios.managers.SpriteManager;
import com.apalacios.sutovoqor.Sutovoqor;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class GameScreen implements Screen {
	final Sutovoqor game;
	private ResourceManager rm;
	private SpriteManager sm;
	
	public GameScreen(Sutovoqor game) {
		this.game = game;
		this.rm = new ResourceManager();
		ResourceManager.loadAllResources();
		while (!ResourceManager.update()) {}
		sm = new SpriteManager(game);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		sm.update(delta);
		
		sm.draw();
		sm.handleInput();
	}
	
	@Override
	public void show() {}
	@Override
	public void resize(int width, int height) {}
	@Override
	public void pause() {}
	@Override
	public void resume() {}
	@Override
	public void hide() {}
	@Override
	public void dispose() {}
}

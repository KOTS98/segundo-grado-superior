﻿using System;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class PersonajePrincipal
{
    public int CalcularPuntosTotales(int bajasEnemigas, int bajasAliadas, int segundosRestantes) {
        int puntos = 0;

        puntos += bajasEnemigas * 100;
        puntos -= bajasAliadas * 50;
        puntos += segundosRestantes * 10;

        if (segundosRestantes <= 0)
            puntos = 0;
        
        return puntos;
    }

    public string FormatearNombreUsuario(string nombreOriginal, int limiteCaracteres) {

        if (nombreOriginal.Length > limiteCaracteres)
        {
            string nombreFormateado = "";

            for (int i = 0; i < limiteCaracteres; i++)
                nombreFormateado += nombreOriginal[i];

            return nombreFormateado;
        }
        else
            return nombreOriginal;
    }

    public bool NivelCompletado(int monedasRecolectadas, int enemigosRestantes, bool llaveEncontrada) {
        return monedasRecolectadas >= 10 && enemigosRestantes == 0 && llaveEncontrada;
    }
}

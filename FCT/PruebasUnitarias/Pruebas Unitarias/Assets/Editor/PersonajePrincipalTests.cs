﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PersonajePrincipalTests
    {
        [Test]
        public void CalcularPuntosTotalesTodoPositivoTest()
        {

            // Asignación
            var pp = new PersonajePrincipal();   
            var bajasEnemigas = 10;
            var bajasAliadas = 3;
            var segundosRestantes = 5;
            var puntosEsperados = (bajasEnemigas * 100) - (bajasAliadas * 50) + (segundosRestantes * 10); // 900 puntos

            // Actuación
            var puntos = pp.CalcularPuntosTotales(bajasEnemigas, bajasAliadas, segundosRestantes);

            // Afirmación
            Assert.That(puntos, Is.EqualTo(puntosEsperados));
        }

        [Test]
        public void CalcularPuntosTotalesTodoNegativoTest()
        {

            // Asignación
            var pp = new PersonajePrincipal();   
            var bajasEnemigas = -10;
            var bajasAliadas = -3;
            var segundosRestantes = -5;
            var puntosEsperados = 0;

            // Actuación
            var puntos = pp.CalcularPuntosTotales(bajasEnemigas, bajasAliadas, segundosRestantes);

            // Afirmación
            Assert.That(puntos, Is.EqualTo(puntosEsperados));
        }

        [Test]
        public void FormatearNombreUsuarioMenorQueLimiteTest()
        {

            // Asignación
            var pp = new PersonajePrincipal();
            var nombreOriginal = "AdrianPalacios";
            var limiteCaracteres = 20;
            var nombreEsperado = "AdrianPalacios";

            // Actuación
            var nombreUsuario = pp.FormatearNombreUsuario(nombreOriginal, limiteCaracteres);

            // Afirmación
            Assert.That(nombreUsuario, Is.EqualTo(nombreEsperado));
        }

        [Test]
        public void FormatearNombreUsuarioMayorQueLimiteTest()
        {

            // Asignación
            var pp = new PersonajePrincipal();
            var nombreOriginal = "AdrianPalacios";
            var limiteCaracteres = 6;
            var nombreEsperado = "Adrian";

            // Actuación
            var nombreUsuario = pp.FormatearNombreUsuario(nombreOriginal, limiteCaracteres);

            // Afirmación
            Assert.That(nombreUsuario, Is.EqualTo(nombreEsperado));
        }

        [Test]
        public void NivelCompletadoExitoTest()
        {

            // Asignación
            var pp = new PersonajePrincipal();
            var monedasRecolectadas = 15;
            var enemigosRestantes = 0;
            var llaveEncontrada = true;
            var resultadoEsperado = true;

            // Actuación
            var resultado = pp.NivelCompletado(monedasRecolectadas, enemigosRestantes, llaveEncontrada);

            // Afirmación
            Assert.That(resultado, Is.EqualTo(resultadoEsperado));
        }

        [Test]
        public void NivelCompletadoFracasoTest()
        {

            // Asignación
            var pp = new PersonajePrincipal();
            var monedasRecolectadas = 5;
            var enemigosRestantes = 3;
            var llaveEncontrada = false;
            var resultadoEsperado = false;

            // Actuación
            var resultado = pp.NivelCompletado(monedasRecolectadas, enemigosRestantes, llaveEncontrada);

            // Afirmación
            Assert.That(resultado, Is.EqualTo(resultadoEsperado));
        }
    }
}

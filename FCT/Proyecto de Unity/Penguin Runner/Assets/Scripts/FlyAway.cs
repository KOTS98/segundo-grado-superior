﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyAway : MonoBehaviour
{
    private Vector3 moveDirection;
    private bool move = false;

    void Update()
    {
        if (move)
            transform.Translate(moveDirection * Time.deltaTime * 100);
    }

    public void SetMoveVector(Vector3 direction) {
        moveDirection = direction;
        move = true;
    }
}

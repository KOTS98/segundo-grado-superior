﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomiaMotor : MonoBehaviour
{   
    private Vector3 position = new Vector3();
    private int enemyMovingX = 1; // 0 izquierda, 1 centro, 2 derecha
    private float timeSinceLastMoveX = 0;
    private bool move = false;

    void Start() {
        position = transform.position;
    }

    void Update()
    {   
        if (move)
            position.z -= Time.deltaTime * 10;

        if (timeSinceLastMoveX < 1) {
                    timeSinceLastMoveX += Time.deltaTime;
                } else {
                    enemyMovingX = Random.Range(0, 3);
                    timeSinceLastMoveX = 0;
                }

                switch (enemyMovingX) {
                    case 0:
                        if (transform.position.x > -3.3f) {
                            position.x = transform.position.x - 4 * Time.deltaTime;
                        }
                    break;

                    case 1:
                        if (transform.position.x > 0) {
                            position.x = transform.position.x - 4 * Time.deltaTime;
                        }
                        if (transform.position.x < 0) {
                            position.x = transform.position.x + 4 * Time.deltaTime;
                        }
                    break;

                    case 2:
                        if (transform.position.x < 3.6f) {
                            position.x = transform.position.x + 4 * Time.deltaTime;
                        }
                    break;
                }

        transform.position = position;
    }

    public void EnableMoving() {
        move = true;
        transform.localScale = new Vector3(50, 50, 50);
    }
}

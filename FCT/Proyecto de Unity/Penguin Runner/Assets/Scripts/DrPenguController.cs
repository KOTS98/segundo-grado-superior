﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrPenguController : MonoBehaviour
{
    public GameObject tileManager;
    [Range(10, 30)]
    public float timeBetweenSpawns;
    private float timeSinceLastSpawn = 0;
    private float timeAlive = 0;
    private bool enemyAlive = false;

    private Transform playerTransform;
    private Transform enemyTransform;
    private Vector3 enemyPosition;

    private int enemyMovingX = 1; // 0 -> Izquierda, 1 -> Centro, 2 -> Derecha
    private float timeSinceLastMoveX = 0;

    // Mover abajo al aparecer
    private bool hasArrived = false;
    private GameObject enemyObject;

    // Animación de subir y bajar
    private GameObject puntuacionObject;

    private float topHeight = 1f;
    private float bottomHeight = 0;
    private float currentHeight = 0;
    private bool goingUp = true;

    // Laser -- Limites de rotación (Y): Izquierda -> 200, Centro -> 180, Derecha -> 160
    private GameObject laser;
    private ParticleSystemRenderer laserPSR;
    private AudioSource laserSource;

    private float timeSinceLastAttack = 0;
    private float attackDuration = 0;
    private bool attackStarted = false;
    private bool attackSelected = false;
    private bool attackLeftToRight = false;
    private bool attackRightToLeft = false;
    private bool doubleAttack = false;
    private bool doubleAttackInv = false;
    private float moveZExit = 0;
    private float moveYExit = 0;
    private bool exitSoundPlayed = false;

    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        puntuacionObject = GameObject.FindGameObjectWithTag("Puntuacion");
    }

    void Update()
    {
        if (!enemyAlive) {
            if (timeSinceLastSpawn < timeBetweenSpawns) {
                timeSinceLastSpawn += Time.deltaTime;
            } else {
                tileManager.GetComponent<TileManager>().SpawnEnemy();
                if (tileManager.GetComponent<TileManager>().EnemyApeared()) {
                    enemyTransform = GameObject.FindGameObjectWithTag("Enemy").transform;
                    enemyObject = GameObject.FindGameObjectWithTag("Enemy");
                    enemyAlive = true;
                    enemyPosition = enemyTransform.position;
                    timeSinceLastSpawn = 0;

                    // Laser
                    laser = GameObject.FindGameObjectWithTag("Laser");
                    laserPSR = laser.GetComponent<ParticleSystemRenderer>();
                    laserPSR.maxParticleSize = 0;

                    enemyObject.SetActive(false);
                }
            }
        } else {
            if (!hasArrived) {
                if (enemyTransform.position.z - playerTransform.position.z <= 10) {
                    puntuacionObject.SetActive(false);
                    enemyObject.SetActive(true);
                    enemyPosition.z = playerTransform.position.z + 10;
                    enemyPosition.y -= Time.deltaTime * 3;
                    enemyTransform.position = enemyPosition;
                    if (enemyPosition.y <= 0) {
                        hasArrived = true;
                    }
                }
            }

            if (hasArrived) {
                timeAlive += Time.deltaTime;

                if (timeAlive <= 15) {
                    if (timeSinceLastAttack >= 0.7f) {
                        if (attackSelected) {

                            // Ejecutar ataque
                            if (attackLeftToRight) {
                                if (!attackStarted) {
                                    attackStarted = true;
                                    laser.transform.rotation = Quaternion.Euler(20, 210, 0);
                                    laserPSR.maxParticleSize = 300;
                                    PlayLaserSound();
                                } else {
                                    laser.transform.Rotate(0, -(Time.deltaTime * 30), 0);
                                }
                                if (attackDuration >= 1.25f) {
                                    RestartLaserPosition();
                                }
                            }

                            if (attackRightToLeft) {
                                if (!attackStarted) {
                                    attackStarted = true;
                                    laser.transform.rotation = Quaternion.Euler(20, 150, 0);
                                    laserPSR.maxParticleSize = 300;
                                    PlayLaserSound();
                                } else {
                                    laser.transform.Rotate(0, (Time.deltaTime * 30), 0);
                                }
                                if (attackDuration >= 1.25f) {
                                    RestartLaserPosition();
                                }
                            }

                            if (doubleAttack) {
                                if (!attackStarted) {
                                    attackStarted = true;
                                    laser.transform.rotation = Quaternion.Euler(20, 210, 0);
                                    laserPSR.maxParticleSize = 300;
                                    PlayLaserSound();
                                } else {
                                    if (attackDuration <= 1.25f) {
                                        laser.transform.Rotate(0, -(Time.deltaTime * 30), 0);
                                    } else {
                                        laser.transform.Rotate(0, (Time.deltaTime * 30), 0);
                                    }
                                }
                                if (attackDuration >= 2.5f) {
                                    RestartLaserPosition();
                                }
                            }

                            if (doubleAttackInv) {
                                if (!attackStarted) {
                                    attackStarted = true;
                                    laser.transform.rotation = Quaternion.Euler(20, 150, 0);
                                    laserPSR.maxParticleSize = 300;
                                    PlayLaserSound();
                                } else {
                                    if (attackDuration <= 1.25f) {
                                        laser.transform.Rotate(0, (Time.deltaTime * 30), 0);
                                    } else {
                                        laser.transform.Rotate(0, -(Time.deltaTime * 30), 0);
                                    }
                                }
                                if (attackDuration >= 2.5f) {
                                    RestartLaserPosition();
                                }
                            }
                            attackDuration += Time.deltaTime;
                        } else {

                            // Seleccionar ataque
                            switch (Random.Range(0, 4)) {
                                case 0:
                                    attackLeftToRight = true;
                                break;

                                case 1:
                                    attackRightToLeft = true;
                                break;

                                case 2:
                                    doubleAttack = true;
                                break;

                                case 3:
                                    doubleAttackInv = true;
                                break;
                            }
                            attackSelected = true;
                        }
                    } else {
                        timeSinceLastAttack += Time.deltaTime;

                        if (timeSinceLastMoveX < 0.2f) {
                            timeSinceLastMoveX += Time.deltaTime;
                        } else {
                            enemyMovingX = Random.Range(0, 3);
                            timeSinceLastMoveX = 0;
                    }

                    switch (enemyMovingX) {
                        case 0:
                            if (enemyTransform.position.x > -1f) {
                                enemyPosition.x = enemyTransform.position.x - 0.03f;
                            }
                        break;

                        case 1:
                            if (enemyTransform.position.x > 0) {
                                enemyPosition.x = enemyTransform.position.x - 0.03f;
                            }
                            if (enemyTransform.position.x < 0) {
                                enemyPosition.x = enemyTransform.position.x + 0.03f;
                            }
                        break;

                        case 2:
                            if (enemyTransform.position.x < 1f) {
                                enemyPosition.x = enemyTransform.position.x + 0.03f;
                            }
                        break;
                    }    
                    }

                    currentHeight = enemyTransform.position.y;
            
                    if (goingUp) {
                        enemyPosition.y = Vector3.Lerp(new Vector3(0, currentHeight, 0), new Vector3(0, topHeight, 0), Time.deltaTime / 2).y;
                    } else {
                        enemyPosition.y = Vector3.Lerp(new Vector3(0, currentHeight, 0), new Vector3(0, bottomHeight, 0), Time.deltaTime / 2).y;
                    }

                    if (enemyTransform.position.y >= topHeight -0.1f)
                        goingUp = false;
                    if (enemyTransform.position.y <= bottomHeight + 0.1f)
                        goingUp = true;

                    enemyPosition.z = playerTransform.position.z + 10;
                } else {
                    tileManager.GetComponent<TileManager>().NormalTerrain();
                    if (timeAlive >= 19) {
                        tileManager.GetComponent<TileManager>().DespawnEnemy();
                        timeAlive = 0;
                        hasArrived = false;
                        moveZExit = 0;
                        moveYExit = 0;
                        enemyAlive = false;
                        exitSoundPlayed = false;
                        puntuacionObject.SetActive(true);
                    }
                    if (timeAlive >= 17) {
                        moveYExit += Time.deltaTime;
                        enemyTransform.Rotate(0, 3, 0);
                        enemyPosition.y = moveYExit * 5;
                        enemyPosition.z = playerTransform.position.z + 10 + (moveZExit * 8);
                        if (!exitSoundPlayed) {
                            laserSource = FindObjectOfType<AudioManager>().Play("DrPenguSalida");
                            exitSoundPlayed = true;
                        }
                    } else {
                        RestartLaserPosition();
                        moveZExit += Time.deltaTime;
                        enemyPosition.z = playerTransform.position.z + 10 + (moveZExit * 8);
                    }
                }
                
                enemyTransform.position = enemyPosition;
            }
        }
    }

    private void RestartLaserPosition() {
        laser.transform.rotation = Quaternion.Euler(20, 0, 0);
        laserPSR.maxParticleSize = 0;
        attackDuration = 0;
        timeSinceLastAttack = 0;
        attackSelected = false;
        attackLeftToRight = false;
        attackRightToLeft = false;
        doubleAttack = false;
        doubleAttackInv = false;
        attackStarted = false;
        StopLaserSound();
    }

    private void PlayLaserSound() {
        laserSource = FindObjectOfType<AudioManager>().Play("Laser");
    }

    private void StopLaserSound() {
        laserSource.Stop();
    }
}

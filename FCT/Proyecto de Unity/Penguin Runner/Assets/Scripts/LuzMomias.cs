﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuzMomias : MonoBehaviour
{
    private bool oscurecer = false;
    private bool aclarar = false;
    private Light lt;
    private Color startColor;
    public float speedMultiplier;

    void Start() {
        lt = GetComponent<Light>();
        startColor = lt.color;
    }

    void Update()
    {   
        if (oscurecer) {
            lt.color -= (Color.white / 2.0f) * Time.deltaTime;
        }

        if (aclarar) {
            lt.color = Color.Lerp(lt.color, startColor, Time.deltaTime * speedMultiplier * 3);
        }
    }

    public void ObscurecerLuz() {
        oscurecer = true;
        aclarar = false;
    }

    public void AclararLuz() {
        oscurecer = false;
        aclarar = true;
    }
}

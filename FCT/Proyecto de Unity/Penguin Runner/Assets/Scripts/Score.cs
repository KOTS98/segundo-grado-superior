﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private float score = 0.0f;

    private int difficultyLevel = 1;
    private int maxDifficultyLevel = 20;
    private int scoreToNextLevel = 10;

    public Text scoreText;
    public DeathMenu deathMenu;

    // Monedas
    private int nCoins = 0;

private bool isDead = false;

    void Update()
    {
        if (isDead)
            return;

        if (score >= scoreToNextLevel)
            LevelUp ();

        score += Time.deltaTime * difficultyLevel;
        scoreText.text = ((int)score).ToString();

    }

    void LevelUp()
    {
        if (difficultyLevel == maxDifficultyLevel)
            return;

        scoreToNextLevel *= 2;
        difficultyLevel++;

        GetComponent<PlayerMotor>().SetSpeed(difficultyLevel);
    }

    public int GetLevel() {
        return difficultyLevel;
    }

    public void CollectCoins(int coinsCollected) {
        nCoins += coinsCollected;
        score += (coinsCollected * difficultyLevel);
    }

    public void OnDeath()
    {
        isDead = true;

        if  (PlayerPrefs.GetFloat("Record") < score)
            PlayerPrefs.SetFloat("Record", score);

        deathMenu.ToggleEndMenu (score);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomiasController : MonoBehaviour
{   
    public GameObject tileManager;

    [Range(10, 30)]
    public float timeBetweenSpawns;
    private float timeSinceLastSpawn = 0;
    private bool enemyAlive = false;

    private Transform enemyTransform;
    private Vector3 enemyPosition;

    void Update()
    {
        if (!enemyAlive) {
            if (timeSinceLastSpawn < timeBetweenSpawns) {
                timeSinceLastSpawn += Time.deltaTime;
            } else {
                tileManager.GetComponent<TileManager>().SpawnEnemy();
                if (tileManager.GetComponent<TileManager>().EnemyApeared()) {
                    enemyAlive = true;
                    timeSinceLastSpawn = 0;
                }
            }
        } else {
            if (enemyAlive) {
                if (tileManager.GetComponent<TileManager>().LastMomiaPassed()) {
                    tileManager.GetComponent<TileManager>().DespawnEnemy();
                    enemyAlive = false;
                }
            }
        }
    }
}

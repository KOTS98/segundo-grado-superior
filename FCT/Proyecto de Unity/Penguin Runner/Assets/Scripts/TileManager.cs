﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour
{
    // Arrays de Tiles
    public GameObject[] tilesNormales;
    public GameObject[] tilesCambioDeAltura;
    public GameObject[] tilesSpawnEnemigo;

    // Ajustes de generación de terreno
    private int tilesEnPantalla; // Mínimo 5, Maximo 15, Recomendado 10
    [Range(5, 100)]
    public int frecuenciaCambioAltura; // Mínimo 5, Maximo 100, Recomendado 50 (Más alto = menor posibilidad)

    public int nivel; // 1 -> Montaña, 2 -> Desierto, 3 -> Ciudad, 4 -> Espacio

    // Generación procedural
    private float puntoSpawnTiles = -6.3f;
    private float longitudTile = 12.6f;
    private float zonaSegura = 15.0f;

    private int indiceUltimoTile = 0;
    private List<GameObject> tilesActivos;

    // Cambio de altura
    private float alturaActual = 0.0f;
    private float probCambioAltura = 0.0f;
    private bool alturaCambiando = false;

    // Jugador
    private Transform transformJugador;

    // Enemigo
    public GameObject enemigo;
    private GameObject enemigoActual;

    private bool enemigoConVida = false;
    private bool enemigoSpawneado = false;

    // Momias
    private float puntoInicioMomias = 0;
    private float puntoFinMomias = 0;
    private List<GameObject> momias;
    private bool momiasEnMovimiento = false;
    private AudioSource audioSourceMomias;
    private bool desiertoClaro = true;

    // Coche
    private int tilesSinDesaparecer = 0;

    // Música
    private AudioSource musicSource;
    private bool musicaIniciada = false;

    private void Start()
    {   
        tilesEnPantalla = PlayerPrefs.GetInt("Renderizado");
        tilesActivos = new List<GameObject>();
        transformJugador = GameObject.FindGameObjectWithTag("Player").transform;
        momias = new List<GameObject>();
        // En caso de ser el nivel 2, obscurece la luz que acompaña al jugador. Esta luz se utiliza durante la zona de las momias
        if (nivel == 2)
            GameObject.FindGameObjectWithTag("LuzP").GetComponent<LuzMomias>().ObscurecerLuz();

        // Los tres primeros tiles son siempre tiles vacíos (ínidice 0)
        for (int i = 0; i < tilesEnPantalla; i++)
        {
            if (i < 2)
                GenerarTile(0);
            else
                GenerarTile();
        }
    }

    private void Update()
    {

        // Inicializa la música
        if (!musicaIniciada)
        {
            FindObjectOfType<AudioManager>().Play("Musica");
            musicaIniciada = true;
        }

        // Añade y elimina tiles conforme avanza el jugador
        if (transformJugador.position.z - zonaSegura - 12.6f > (puntoSpawnTiles - tilesEnPantalla * longitudTile))
        {
            GenerarTile();

            if (nivel == 2)
                ComprobarPosicionRespectoAMomias();

            MantenerUltimosDiezTiles();
        }

        probCambioAltura += Time.deltaTime;
    }

    public int GetDistanciaRenderizado() {
        return tilesEnPantalla;
    }

    public bool EnemyApeared()
    {
        return enemigoSpawneado;
    }

    public bool LastMomiaPassed()
    {
        return transformJugador.position.z > enemigoActual.transform.position.z + 3;
    }

    public bool CarBehind()
    {
        return transformJugador.position.z > enemigoActual.transform.position.z + 10;
    }

    public GameObject GetCurrentEnemy()
    {
        return enemigoActual;
    }

    public void SpawnEnemy()
    {
        enemigoConVida = true;
    }

    public void NormalTerrain()
    {
        enemigoConVida = false;
        enemigoSpawneado = false;
    }

    private void ComprobarPosicionRespectoAMomias()
    {

        // En caso de que las momias ya hayan sido generadas, comprueba si el jugador ha entrado al área donde se encuentran
        if (transformJugador.position.z - zonaSegura - 12.6f >= puntoInicioMomias - longitudTile * 3 &&
            enemigoSpawneado && enemigoConVida && !momiasEnMovimiento && desiertoClaro) {
            ObscurecerDesierto();
        }
        if (transformJugador.position.z - zonaSegura - 12.6f >= puntoInicioMomias &&
            enemigoSpawneado && enemigoConVida && !momiasEnMovimiento) {
            HabilitarMovimientoMomias();
            audioSourceMomias = FindObjectOfType<AudioManager>().Play("Momias");
        }
        if (momias.Count == 0 && !desiertoClaro) {
            AclararDesierto();
        }
    }

    private void MantenerUltimosDiezTiles()
    {

        // En caso de que este sea el nivel 3 y que se haya generado el coche, mantiene cargados 10 tiles
        // por detrás del jugador y los elimina al desaparecer el coche
        if (enemigoSpawneado && nivel == 3)
        {
            tilesSinDesaparecer += 1;

            if (tilesSinDesaparecer == 10)
            {
                EliminarUltimoTile();
                tilesSinDesaparecer -= 1;
            }
        }
        else
        {
            if (tilesSinDesaparecer > 0)
            {
                for (int i = 0; i < tilesSinDesaparecer; i++)
                    EliminarUltimoTile();

                tilesSinDesaparecer = 0;
            }
            else
                EliminarUltimoTile();
        }
    }

    private void GenerarTile(int indiceTile = -1)
    {
        CambiarAlturaDelTerreno();

        GameObject tile = InstanciarTile(indiceTile);

        PosicionarTile(tile);

        tilesActivos.Add(tile);
        puntoSpawnTiles += longitudTile;

        // En caso de estar en el nivel 2, genera terreno convencional si se ha traspasado la zona de las momias
        if (enemigoConVida && nivel == 2 && puntoSpawnTiles >= puntoFinMomias)
            NormalTerrain();
    }

    private void CambiarAlturaDelTerreno()
    {
        if (frecuenciaCambioAltura != 100 && !enemigoConVida)
        {
            if (Random.Range(1, frecuenciaCambioAltura) <= probCambioAltura)
            {
                probCambioAltura = 0.0f;
                alturaCambiando = true;

                if (alturaActual == -2.5f || alturaActual == 2.5f)
                    alturaActual = 0.0f;
                else
                {
                    if (Random.Range(1, 100) <= 50)
                        alturaActual = 2.5f;
                    else
                        alturaActual = -2.5f;
                }
            }
        }
    }

    private GameObject InstanciarTile(int indiceTile)
    {
        GameObject tile;

        if (indiceTile == -1)
        {
            if (enemigoConVida)
            {
                tile = Instantiate(tilesSpawnEnemigo[IndiceAleatorioTileEnemigo()]) as GameObject;
                if (!enemigoSpawneado)
                    GenerarEnemigo();
            }
            else
            {
                if (alturaCambiando)
                {
                    tile = Instantiate(tilesCambioDeAltura[IndiceAleatorioTileAltura()]) as GameObject;
                    alturaCambiando = false;
                }
                else
                    tile = Instantiate(tilesNormales[IndiceAleatorioTileNormal()]) as GameObject;
            }
        }
        else
            tile = Instantiate(tilesNormales[indiceTile]) as GameObject;

        return tile;
    }

    private void PosicionarTile(GameObject tile)
    {
        tile.transform.SetParent(transform);
        tile.transform.position = new Vector3(0, alturaActual, puntoSpawnTiles);
    }

    private void EliminarUltimoTile()
    {
        Destroy(tilesActivos[0]);
        tilesActivos.RemoveAt(0);
    }

    private int IndiceAleatorioTileNormal()
    {
        if (tilesNormales.Length <= 1)
            return 0;

        int indiceAleatorio = indiceUltimoTile;
        while (indiceAleatorio == indiceUltimoTile)
            indiceAleatorio = Random.Range(1, tilesNormales.Length);

        indiceUltimoTile = indiceAleatorio;
        return indiceAleatorio;
    }

    private int IndiceAleatorioTileEnemigo()
    {
        if (tilesSpawnEnemigo.Length <= 1)
            return 0;

        int indiceAleatorio = indiceUltimoTile;
        while (indiceAleatorio == indiceUltimoTile)
            indiceAleatorio = Random.Range(0, tilesSpawnEnemigo.Length);

        indiceUltimoTile = indiceAleatorio;
        return indiceAleatorio;
    }

    private int IndiceAleatorioTileAltura()
    {
        if (tilesCambioDeAltura.Length <= 1)
            return 0;

        int indiceAleatorio = indiceUltimoTile;
        while (indiceAleatorio == indiceUltimoTile)
            indiceAleatorio = Random.Range(0, tilesCambioDeAltura.Length);

        indiceUltimoTile = indiceAleatorio;
        return indiceAleatorio;
    }

    public void DespawnEnemy()
    {
        if (nivel == 2)
        {
            for (int i = 0; i < 30; i++)
            {
                Destroy(momias[0]);
                momias.RemoveAt(0);
            }
            puntoInicioMomias = 0;
            puntoFinMomias = 0;
            momiasEnMovimiento = false;

            audioSourceMomias.Stop();
        }
        else
            Destroy(enemigoActual);
    }

    private void HabilitarMovimientoMomias()
    {
        momiasEnMovimiento = true;
        for (int i = 0; i < 30; i++)
            momias[i].GetComponent<MomiaMotor>().EnableMoving();
    }

    private void ObscurecerDesierto() {
        GameObject.FindGameObjectWithTag("Sol").GetComponent<LuzMomias>().ObscurecerLuz();
        GameObject.FindGameObjectWithTag("Ambiente").GetComponent<LuzMomias>().ObscurecerLuz();
        GameObject.FindGameObjectWithTag("LuzP").GetComponent<LuzMomias>().AclararLuz();
        desiertoClaro = false;
    }

    private void AclararDesierto() {
        GameObject.FindGameObjectWithTag("Sol").GetComponent<LuzMomias>().AclararLuz();
        GameObject.FindGameObjectWithTag("Ambiente").GetComponent<LuzMomias>().AclararLuz();
        GameObject.FindGameObjectWithTag("LuzP").GetComponent<LuzMomias>().ObscurecerLuz();
        desiertoClaro = true;
    }

    private void GenerarEnemigo()
    {
        switch (nivel)
        {
            case 1:
                enemigoActual = Instantiate(enemigo) as GameObject;
                enemigoActual.transform.SetParent(transform);
                enemigoActual.transform.position = new Vector3(0, alturaActual, puntoSpawnTiles + longitudTile);
                break;

            case 2:
                for (int i = 0; i < 30; i++)
                {
                    enemigoActual = Instantiate(enemigo) as GameObject;
                    enemigoActual.transform.SetParent(transform);
                    enemigoActual.transform.position = new Vector3(Random.Range(-3.3f, 3.6f), alturaActual,
                        puntoSpawnTiles + (i * longitudTile) + (longitudTile * 2));
                    momias.Add(enemigoActual);

                    puntoInicioMomias = puntoSpawnTiles - 25.2f;
                    puntoFinMomias = puntoSpawnTiles + (longitudTile * 25);
                }
                break;

            case 3:
                enemigoActual = Instantiate(enemigo) as GameObject;
                enemigoActual.transform.SetParent(transform);
                if (Random.Range(0, 2) == 0)
                    enemigoActual.transform.position = new Vector3(-1.8f, alturaActual, puntoSpawnTiles + (longitudTile * 2));
                else
                    enemigoActual.transform.position = new Vector3(1.8f, alturaActual, puntoSpawnTiles + (longitudTile * 2));
                break;

            case 4:
                enemigoActual = Instantiate(enemigo) as GameObject;
                enemigoActual.transform.SetParent(transform);
                enemigoActual.transform.position = new Vector3(0, alturaActual + 10, puntoSpawnTiles - 20f);
                break;
        }
        enemigoSpawneado = true;
    }
}

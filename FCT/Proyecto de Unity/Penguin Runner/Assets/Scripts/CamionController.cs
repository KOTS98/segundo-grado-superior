﻿using System.Collections;
using UnityEngine;

public class CamionController : MonoBehaviour
{
    private TileManager tileManager;
    public GameObject enemyBarrel;
    private GameObject currentBarrel;

    [Range(10, 30)]
    public float tiempoSpawn;
    private float tiempoDesdeUltimoSpawn = 0;
    private float timeAlive = 0;
    private float timeStoping = 0;
    private bool enemyAlive = false;
    private bool enemyStoping = false;
    private bool terrenoNormal = false;

    private Transform playerTransform;
    private Transform enemyTransform;
    private Vector3 enemyPosition;

    private int enemyMovingX = 1; // 0 -> Izquierda, 1 -> Centro, 2 -> Derecha
    private float velocityDecrease = 0;
    private float timeSinceLastMoveX = 0;
    private float timeSinceLastBarrel = 0;

    private int distanciaRender;

    void Start() {
        tileManager = FindObjectOfType<TileManager>().GetComponent<TileManager>();
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        distanciaRender = GetComponent<TileManager>().GetDistanciaRenderizado();
    }

    void Update() {
        if (enemyAlive) {
            if (enemyTransform.position.z - playerTransform.position.z <= 15) {
                timeAlive += Time.deltaTime;

                MovimientoLateralCamion();

                enemyPosition.z = playerTransform.position.z + 15;

                enemyTransform.position = enemyPosition;

                if (timeSinceLastBarrel < Random.Range(0.5f, 3.0f))
                    timeSinceLastBarrel += Time.deltaTime;
                else
                    LanzarBarrilCamion();

            } else {
                enemyPosition.y = enemyTransform.position.y;
            }

            if (timeAlive > 17 - (distanciaRender / 2) && !terrenoNormal)
                tileManager.NormalTerrain();

            if (timeAlive > 15) {
                enemyAlive = false;
                enemyStoping = true;
                timeSinceLastBarrel = 0;
                tiempoDesdeUltimoSpawn = 0;
                timeAlive = 0;
            }
        }
        else {
            if (tiempoDesdeUltimoSpawn < tiempoSpawn)
                tiempoDesdeUltimoSpawn += Time.deltaTime;
            else
                GenerarCamion();

            if (enemyStoping) {
                FrenarCamion();
                timeStoping += Time.deltaTime;

                if (timeStoping >= 5)
                    EliminarCamion();
            }
        }
    }

    private void GenerarCamion() {
        tileManager.SpawnEnemy();

        if (tileManager.EnemyApeared()) {
            enemyTransform = GameObject.FindGameObjectWithTag("Enemy").transform;
            enemyPosition.z = enemyTransform.position.z;
            enemyAlive = true;
        }
    }

    private void EliminarCamion() {
        enemyStoping = false;
        velocityDecrease = 0;
        tiempoDesdeUltimoSpawn = 0;
        timeAlive = 0;
        timeStoping = 0;
        enemyMovingX = 1;
        //enemyPosition = new Vector3();
        tileManager.DespawnEnemy();
    }

    private void MovimientoLateralCamion() {
        if (timeSinceLastMoveX < 1)
            timeSinceLastMoveX += Time.deltaTime;
        else {
            enemyMovingX = Random.Range(0, 3);
            timeSinceLastMoveX = 0;
        }

        switch (enemyMovingX) {
            case 0:
                if (enemyTransform.position.x > -1.8f)
                    enemyPosition.x = enemyTransform.position.x - Time.deltaTime * 2;
                break;

            case 1:
                if (enemyTransform.position.x > 0)
                    enemyPosition.x = enemyTransform.position.x - Time.deltaTime * 2;

                if (enemyTransform.position.x < 0)
                    enemyPosition.x = enemyTransform.position.x + Time.deltaTime * 2;
                break;

            case 2:
                if (enemyTransform.position.x < 1.8f)
                    enemyPosition.x = enemyTransform.position.x + Time.deltaTime * 2;
                break;
        }
    }

    private void FrenarCamion() {
        velocityDecrease += Time.deltaTime;
        float value = playerTransform.position.z + 15;
        enemyPosition.z = (playerTransform.position.z + 15) -
            (velocityDecrease * GameObject.FindGameObjectWithTag("Player").GetComponent<Score>().GetLevel() * 2);
        enemyTransform.position = enemyPosition;
    }

    private void LanzarBarrilCamion() {
        timeSinceLastBarrel = 0;

        currentBarrel = Instantiate(enemyBarrel) as GameObject;
        currentBarrel.transform.SetParent(enemyTransform);
        Vector3 movimientoBarril = new Vector3(enemyPosition.x, enemyPosition.y + 0.5f, enemyPosition.z - 2.49f);

        switch (enemyMovingX) {
            case 0:
                movimientoBarril.x -= 0.85f;
                break;

            case 1:
                movimientoBarril.x += 0.86f;
                break;

            case 2:
                movimientoBarril.x += 1.65f;
                break;
        }

        currentBarrel.transform.position = movimientoBarril;
    }
}

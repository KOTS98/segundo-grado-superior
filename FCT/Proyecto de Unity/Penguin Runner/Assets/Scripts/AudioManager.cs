﻿using System;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public Sound[] sonidos;

    void Awake() {

        foreach (Sound s in sonidos) {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.audioMixer;
        }
    }

    public AudioSource Play(string nombre) {
        Sound s = Array.Find(sonidos, sound => sound.name == nombre);
        if (s == null) {
            Debug.LogWarning("Sonido:  " + nombre + " no encontrado.");
            return null;
        }
        s.source.Play();
        return s.source;
    }
}

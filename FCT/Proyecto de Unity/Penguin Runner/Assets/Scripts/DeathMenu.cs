﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathMenu : MonoBehaviour
{
    public Text scoreText;
    public Image backgroundImg;
    public Text finPartidaText;
    public Text puntuacionText;
    public Image finalScoreImg;
    public Button jugarButton;
    public Button menuButton;
    private bool isShowned = false;

    private float transition = 0.0f;
  
    void Start()
    {
        gameObject.SetActive (false);
    }

    void Update()
    {
        if (!isShowned)
            return;

        transition += Time.deltaTime / 4;
        backgroundImg.color = Color.Lerp(new Color(0,0,0,0), Color.black, transition);
        finPartidaText.color = Color.Lerp(new Color(255,255,255,0), Color.white, transition);
        puntuacionText.color = Color.Lerp(new Color(255,255,255,0), Color.white, transition);
        finalScoreImg.color = Color.Lerp(new Color(255,255,255,0), Color.white, transition);
        jugarButton.GetComponent<Image>().color = Color.Lerp(new Color(255,255,255,0), Color.white, transition);
        menuButton.GetComponent<Image>().color = Color.Lerp(new Color(255,255,255,0), Color.white, transition);
    }

    public void ToggleEndMenu(float score)
    {
        gameObject.SetActive (true);
        scoreText.text = ((int)score).ToString ();
        isShowned = true;
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ToMenu() {
        SceneManager.LoadScene("Menu");
    }
}

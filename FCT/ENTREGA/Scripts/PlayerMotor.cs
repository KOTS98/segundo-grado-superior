﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMotor : MonoBehaviour
{
    // Físicas
    private float velocidadH = 3.0f;
    private float velocidadV = 0.0f;
    private float gravedad = 8.0f;

    private CharacterController controller;
    private Vector3 VectorMovimiento;

    // Salto
    private bool saltando = false;
    private float tiempoSalto = 0.0f;

    // Power Ups
    private bool monedasX3 = false;
    private bool invencibilidad = false;
    private bool velocidadReducida = false;
    public GameObject[] particulasPowerUps;

    private float tiempoMonedasX3 = 0.0f;
    private float tiempoInvencibilidad = 0.0f;
    private float tiempoVelocidadReducida = 0.0f;

    // Impedir movimiento los primeros 3 segundos de partida
    private float duracionAnimacionInicio = 3.0f;
    private float tiempoInicio;
    private bool juegoIniciado = false;

    private bool muerto = false;

    // Control de la animación
    private Animator animator;
    private GameObject puntuacion;

    // Sonido
    public GameObject audioManager;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
        animator.Play("Streching");
        tiempoInicio = Time.time;
        puntuacion = GameObject.FindGameObjectWithTag("Puntuacion");
    }

    void Update()
    {

        // Evitar movimiento si el jugador está muerto
        if (muerto)
        {
            VectorMovimiento.y -= gravedad * Time.deltaTime;
            VectorMovimiento.z = 0.0f;
            controller.Move((VectorMovimiento * velocidadH) * Time.deltaTime);
            return;
        }

        // Evitar movimiento del jugador durante la animación de inicio
        if (Time.time - tiempoInicio < duracionAnimacionInicio)
        {
            return;
        }
        else if (!juegoIniciado)
        {
            juegoIniciado = true;
            animator.Play("Running");
        }

        // Reiniciar vector de movimiento
        VectorMovimiento = Vector3.zero;

        CalcularGravedad();

        CalcularMovimiento();

        ControlarTiempoPowerUps();
    }

    public bool Alive()
    {
        return !muerto;
    }

    public void SetSpeed(float modifier)
    {
        velocidadH += (modifier / 20);
    }

    public void Saltar() {
        if (saltando == false) {
            saltando = true;
            animator.Play("Jump");
            audioManager.GetComponent<AudioManager>().Play("Salto");
        }
    }

    private void CalcularGravedad()
    {
        if (controller.isGrounded)
        {
            velocidadV = -0.5f;

            // Recuperarse después de saltar
            if (saltando && tiempoSalto > 0.1f)
            {
                tiempoSalto = 0.0f;
                saltando = false;
                audioManager.GetComponent<AudioManager>().Play("Caer");
                animator.Play("Running");
            }
        }
        else
            velocidadV -= gravedad * Time.deltaTime;
    }

    private void CalcularMovimiento()
    {

        // X - Izquierda - Derecha
        //VectorMovimiento.x = CrossPlatformInputManager.GetAxis("Horizontal") * velocidadH;
        VectorMovimiento.x = Input.GetAxisRaw("Horizontal") * velocidadH;

        if (saltando)
        {
            VectorMovimiento.x /= 4;
            tiempoSalto += Time.deltaTime;
        }

        // Y - Arriba - Abajo
        VectorMovimiento.y = velocidadV;

        if (saltando == true)
            VectorMovimiento.y += 4.5f;

        if ((Input.GetKeyDown(KeyCode.Space) /*|| CrossPlatformInputManager.GetButtonDown("Salto")*/) && saltando == false)
        {
            saltando = true;
            animator.Play("Jump");
            audioManager.GetComponent<AudioManager>().Play("Salto");
        }

        // Z - Alante - Atrás
        VectorMovimiento.z = velocidadH;
    }

    private void ControlarTiempoPowerUps()
    {

        // Velocidad reducida
        if (velocidadReducida)
        {
            controller.Move(((VectorMovimiento * velocidadH) * Time.deltaTime) / 1.5f);
            tiempoVelocidadReducida += Time.deltaTime;

            if (tiempoVelocidadReducida >= 5)
            {
                velocidadReducida = false;
                animator.SetFloat("animSpeed", 1.0f);
                particulasPowerUps[2].SetActive(false);
            }
        }
        else
            controller.Move((VectorMovimiento * velocidadH) * Time.deltaTime);

        // Monedas x3
        if (monedasX3)
        {
            tiempoMonedasX3 += Time.deltaTime;
            if (tiempoMonedasX3 >= 10)
            {
                monedasX3 = false;
                particulasPowerUps[0].SetActive(false);
            }
        }

        // Invencibilidad
        if (invencibilidad)
        {
            tiempoInvencibilidad += Time.deltaTime;
            if (tiempoInvencibilidad >= 7)
            {
                invencibilidad = false;
                particulasPowerUps[1].SetActive(false);
            }
        }
    }

    // Detectar colisiones
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {

        // Ignorar colisiones al inicio de la partida
        if (Time.time - tiempoInicio < duracionAnimacionInicio + 2)
            return;

        ComprobarColisionObstaculo(hit);

        ComprobarColisionLaser(hit);

        ComprobarColisionCoche(hit);

        ComprobarColisionMuro(hit);

        ComprobarColisionMoneda(hit);

        ComprobarColisionPowerUp(hit);
    }

    private void ComprobarColisionObstaculo(ControllerColliderHit hit)
    {

        // Determina si el obstáculo se encuentra delante del jugador
        if (hit.point.z > transform.position.z + (controller.radius * 15) && hit.gameObject.CompareTag("Obstaculo"))
        {
            if (invencibilidad)
            {
                Physics.IgnoreCollision(hit.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
                Vector3 direction = (this.transform.position - hit.gameObject.transform.position).normalized;
                hit.gameObject.GetComponent<FlyAway>().SetMoveVector(direction);
                audioManager.GetComponent<AudioManager>().Play("SalirVolando");
            }
            else
                Death();
        }
    }

    private void ComprobarColisionLaser(ControllerColliderHit hit)
    {
        if (hit.gameObject.CompareTag("Laser"))
        {
            if (invencibilidad)
                Physics.IgnoreCollision(hit.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
            else
                Death();
        }
    }

    private void ComprobarColisionCoche(ControllerColliderHit hit)
    {
        if (hit.gameObject.CompareTag("Coche"))
        {
            if (muerto)
                Physics.IgnoreCollision(hit.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
            else
                Death();
        }
    }

    private void ComprobarColisionMuro(ControllerColliderHit hit)
    {

        // Comprueba que el muro se encuentra delante del jugador
        if (hit.point.z > transform.position.z + (controller.radius * 15) && hit.gameObject.tag == "Muro")
            Death();
    }

    private void ComprobarColisionMoneda(ControllerColliderHit hit)
    {
        if (hit.gameObject.tag == "Moneda")
        {
            audioManager.GetComponent<AudioManager>().Play("Moneda");
            Destroy(hit.gameObject);

            if (monedasX3)
                GetComponent<Score>().CollectCoins(3);
            else
                GetComponent<Score>().CollectCoins(1);
        }
    }

    private void ComprobarColisionPowerUp(ControllerColliderHit hit)
    {

        // Monedas x3
        if (hit.gameObject.tag == "PowerUp1")
        {
            audioManager.GetComponent<AudioManager>().Play("PowerUp");
            Destroy(hit.gameObject);
            removePowerUps();
            monedasX3 = true;
            particulasPowerUps[0].SetActive(true);
        }

        // Invencibilidad
        if (hit.gameObject.tag == "PowerUp2")
        {
            audioManager.GetComponent<AudioManager>().Play("PowerUp");
            Destroy(hit.gameObject);
            removePowerUps();
            invencibilidad = true;
            particulasPowerUps[1].SetActive(true);
        }

        // Velocidad reducida
        if (hit.gameObject.tag == "PowerUp3")
        {
            audioManager.GetComponent<AudioManager>().Play("PowerUp");
            Destroy(hit.gameObject);
            removePowerUps();
            velocidadReducida = true;
            animator.SetFloat("animSpeed", 0.5f);
            particulasPowerUps[2].SetActive(true);
        }
    }

    private void removePowerUps()
    {
        monedasX3 = false;
        invencibilidad = false;
        velocidadReducida = false;

        tiempoMonedasX3 = 0.0f;
        tiempoInvencibilidad = 0.0f;
        tiempoVelocidadReducida = 0.0f;

        particulasPowerUps[0].SetActive(false);
        particulasPowerUps[1].SetActive(false);
        particulasPowerUps[2].SetActive(false);
    }

    public void Death()
    {
        if (!muerto)
            audioManager.GetComponent<AudioManager>().Play("Muerte");
        if (puntuacion != null)
            puntuacion.SetActive(true);
        if (!muerto)
            //GameObject.FindGameObjectWithTag("Controles").SetActive(false);
        removePowerUps();
        animator.Play("Triping");
        muerto = true;
        GetComponent<Score>().OnDeath();
    }
}

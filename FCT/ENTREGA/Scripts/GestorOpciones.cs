﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class GestorOpciones : MonoBehaviour {

    public AudioMixer audioMixer;
    private float volumenMusica;
    private float volumenEfectos;
    private int distanciaRenderizado;

    // Sliders
    public Slider sliderMusica;
    public Slider sliderEfectos;
    public Slider sliderRenderizado;

    void Start() {
        audioMixer.SetFloat("volumenMusica", Mathf.Log10(PlayerPrefs.GetFloat("VolMusica")) * 20);
        audioMixer.SetFloat("volumenEfectos", Mathf.Log10(PlayerPrefs.GetFloat("VolEfectos")) * 20);
        sliderMusica.value = PlayerPrefs.GetFloat("VolMusica");
        sliderEfectos.value = PlayerPrefs.GetFloat("VolEfectos");
        sliderRenderizado.value = PlayerPrefs.GetInt("Renderizado");
    }

    public void SetVolumenMusica(float volumen) {
        volumenMusica = volumen;
        PlayerPrefs.SetFloat("VolMusica", volumen);
    }

    public void SetVolumenEfectos(float volumen) {
        volumenEfectos = volumen;
        PlayerPrefs.SetFloat("VolEfectos", volumen);
    }

    public void SetDistanciaRenderizado(float distancia) {
        distanciaRenderizado = (int) distancia;
        PlayerPrefs.SetInt("Renderizado", (int) distancia);
    }

    void Update() {
        audioMixer.SetFloat("volumenMusica", Mathf.Log10(volumenMusica) * 20);
        audioMixer.SetFloat("volumenEfectos", Mathf.Log10(volumenEfectos) * 20);
    }
}

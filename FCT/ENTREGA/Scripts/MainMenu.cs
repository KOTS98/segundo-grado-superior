﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MainMenu : MonoBehaviour
{
public Text recordText;

    public static AudioSource menuMusicSource;
    public AudioMixer audioMixer;
    public GameObject menuInicio;
    public GameObject menuOpciones;
    public GameObject menuNivel;

    void Start()
    {   
        menuMusicSource = FindObjectOfType<AudioManager>().Play("Menu");
    }

    public void ToLevelSelect()
    {   
        menuInicio.SetActive(false);
        menuNivel.SetActive(true);
    }

    public void ToLvl1() {
        SceneManager.LoadScene("LevelSnow");
    }

    public void ToLvl2() {
        SceneManager.LoadScene("LevelDessert");
    }

    public void ToLvl3() {
        SceneManager.LoadScene("LevelStreet");
    }

    public void ToLvl4() {
        SceneManager.LoadScene("LevelSpace");
    }

    public void ToOpciones() {
        menuInicio.SetActive(false);
        menuOpciones.SetActive(true);
    }

    public void Salir() {
        Application.Quit();
    }

    public void ToMainMenu() {
        menuOpciones.SetActive(false);
        menuNivel.SetActive(false);
        menuInicio.SetActive(true);
    }
}

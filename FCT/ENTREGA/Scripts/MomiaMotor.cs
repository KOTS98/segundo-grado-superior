﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomiaMotor : MonoBehaviour
{   
    private Vector3 position = new Vector3();
    private int enemyMovingX = 1; // 0 izquierda, 1 centro, 2 derecha
    private float timeSinceLastMoveX = 0;
    private bool move = false;

    void Start() {
        position.x = transform.position.x;
        position.y = transform.position.y;
        position.z = transform.position.z;
    }

    void Update()
    {   
        if (move)
            position.z -= 0.1f;

        if (timeSinceLastMoveX < 1) {
                    timeSinceLastMoveX += Time.deltaTime;
                } else {
                    enemyMovingX = Random.Range(0, 3);
                    timeSinceLastMoveX = 0;
                }

                switch (enemyMovingX) {
                    case 0:
                        if (transform.position.x > -3.3f) {
                            position.x = transform.position.x - 0.02f;
                        }
                    break;

                    case 1:
                        if (transform.position.x > 0) {
                            position.x = transform.position.x - 0.02f;
                        }
                        if (transform.position.x < 0) {
                            position.x = transform.position.x + 0.02f;
                        }
                    break;

                    case 2:
                        if (transform.position.x < 3.6f) {
                            position.x = transform.position.x + 0.02f;
                        }
                    break;
                }

        transform.position = position;
    }

    public void EnableMoving() {
        move = true;
    }
}

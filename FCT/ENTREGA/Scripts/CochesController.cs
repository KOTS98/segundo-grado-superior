﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CochesController : MonoBehaviour // Movimiento lateral coche: Izq -> -1.8, Der -> 1.8
{   
    public GameObject tileManager;
    public GameObject camara;

    [Range(10, 30)]
    public float timeBetweenSpawns;
    private float timeSinceLastSpawn = 0;
    private float timeAlive = 0;
    private bool enemyAlive = false;
    private bool playerAhead = false;

    // Movimiento del coche
    private int enemyMovingX = 1; // 0 -> Izquierda, 1 -> Centro, 2 -> Derecha
    private float timeSinceLastMoveX = 0;
    private Vector3 enemyPosition = new Vector3(0, 0, 0);

    private float timeSinceAcceleration = 0;
    private bool accelerating = false;
    private bool decelerating = false;
    private Vector3 decelerationStartPoint;

    private float accelerationTime = 0;
    private float decelerationTime = 0;

    void Update()
    {
        if (!enemyAlive) {
            if (timeSinceLastSpawn < timeBetweenSpawns) {
                timeSinceLastSpawn += Time.deltaTime;
            } else {
                tileManager.GetComponent<TileManager>().SpawnEnemy();
                if (tileManager.GetComponent<TileManager>().EnemyApeared()) {
                    enemyAlive = true;
                    timeSinceLastSpawn = 0;
                }
            }
        } else {
            if (tileManager.GetComponent<TileManager>().CarBehind() && !playerAhead) {
                camara.GetComponent<CameraMotor>().ChangeToFightView();
                playerAhead = true;
            }

            if (playerAhead) {
                timeAlive += Time.deltaTime;

                if (timeSinceLastMoveX < 1) {
                    timeSinceLastMoveX += Time.deltaTime;
                } else {
                    enemyMovingX = Random.Range(0, 3);
                    timeSinceLastMoveX = 0;
                }

                if (!accelerating && !decelerating) {
                    if (timeSinceAcceleration < Random.Range(3, 11)){
                        timeSinceAcceleration += Time.deltaTime;
                    } else {
                        accelerating = true;
                    }

                    switch (enemyMovingX) {
                        case 0:
                            if (tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x > -1.8f) {
                                enemyPosition.x = tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x - 0.02f;
                            }
                        break;

                        case 1:
                            if (tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x > 0) {
                                enemyPosition.x = tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x - 0.02f;
                            }
                            if (tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x < 0) {
                                enemyPosition.x = tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x + 0.02f;
                            }
                        break;

                        case 2:
                            if (tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x < 1.8f) {
                                enemyPosition.x = tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position.x + 0.02f;
                            }
                        break;
                    }

                    enemyPosition.z = transform.position.z - 15;
                } else {
                    if (accelerating) {
                        Vector3 playerZ = new Vector3(enemyPosition.x, 0, transform.position.z + 2);

                        enemyPosition = Vector3.Lerp(enemyPosition, playerZ, Time.deltaTime * 5);
                        accelerationTime += Time.deltaTime;

                        if (GetComponent<PlayerMotor>().Alive()) {
                            if (accelerationTime >= 0.3f) {
                                if (transform.position.x > enemyPosition.x - 1.8f && transform.position.x < enemyPosition.x + 1.8f) {
                                    GetComponent<PlayerMotor>().Death();
                                }
                            }
                        }
                        
                        if (accelerationTime >= 1) {
                            accelerationTime = 0;
                            accelerating = false;
                            decelerating = true;
                            decelerationStartPoint = enemyPosition;
                        }
                    }
                
                    if (decelerating) {
                        enemyPosition.z = transform.position.z - 1f - (decelerationTime * 5);
                        decelerationTime += Time.deltaTime;
                        
                        if (decelerationTime >= 3) {
                            decelerationTime = 0;
                            timeSinceAcceleration = 0;
                            decelerating = false;
                            if (timeAlive > 15) {
                                timeAlive = 0;
                                accelerationTime = 0;
                                decelerationTime = 0;
                                accelerating = false;
                                decelerating = false;
                                enemyAlive = false;
                                playerAhead = false;
                                timeSinceAcceleration = 0;
                                enemyMovingX = 1;
                                timeSinceLastMoveX = 0;
                                timeSinceLastSpawn = 0;
                                tileManager.GetComponent<TileManager>().DespawnEnemy();
                                camara.GetComponent<CameraMotor>().ChangeToNormalView();
                                tileManager.GetComponent<TileManager>().NormalTerrain();
                            }
                        }
                    }
                }

                tileManager.GetComponent<TileManager>().GetCurrentEnemy().transform.position = enemyPosition;
            }
        }
    }
}

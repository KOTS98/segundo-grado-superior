﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour {
    // PowerUps
    public GameObject powerUp1;
    public GameObject powerUp2;
    public GameObject powerUp3;

    private Transform transformTile;
    private float longitudTile  = 12.6f;

    void Start(){
        transformTile = GetComponent<Transform> ();
        GameObject GOPowerUP;

        int nAleatorio = Random.Range(1, 100);
        if (nAleatorio <= 33)
            GOPowerUP = Instantiate(powerUp1) as GameObject;
        else if (nAleatorio <= 66)
            GOPowerUP = Instantiate(powerUp2) as GameObject;
        else
            GOPowerUP = Instantiate(powerUp3) as GameObject;

        GOPowerUP.transform.SetParent(transform);
        GOPowerUP.transform.position = new Vector3(transformTile.position.x, transformTile.position.y, transformTile.position.z + (longitudTile / 2));
    }
}

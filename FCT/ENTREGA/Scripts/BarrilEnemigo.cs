﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrilEnemigo : MonoBehaviour {

    private Vector3 posicion;
    private float tiempoVivo = 0;

    void Start() {
        posicion = transform.position;
    }

    void Update() {
        transform.Rotate(0, 300 * Time.deltaTime, 0);
        posicion.z -= 0.025f;

        if (posicion.y > 0.5f)
            posicion.y -= 0.05f;

        transform.position = posicion;
        tiempoVivo += Time.deltaTime;

        if (tiempoVivo > 3)
            Destroy(transform.parent.gameObject);
    }
}

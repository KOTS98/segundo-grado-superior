﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour {
    private Transform transformJugador;
    private Vector3 distanciaAlJugador;
    private Vector3 vectorMovimiento;

    // Animación de la cámara al inicio de la partida
    private float tiempoTransicion = 0;
    private float duracionAnimacion = 3;
    private Vector3 desfaseAnimacionInicio = new Vector3(0, 5, 5);

    // Ajustes para cambiar entre vista normal y vista de pelea
    private bool vistaPelea = false;
    private bool transicionPelea = false;
    private bool transicionNormal = false;

    private Vector3 desfasePosicionPelea = new Vector3(0, 0, 15);

    void Start() {
        transformJugador = GameObject.FindGameObjectWithTag("Player").transform;
        distanciaAlJugador = transform.position - transformJugador.position;
    }

    void Update() {
        ReiniciarVectorMovimiento();

        if (transicionPelea)
            TransicionVistaPelea();

        if (transicionNormal)
            TransicionVistaNormal();

        if (!transicionPelea && !transicionNormal) {
            if (tiempoTransicion > 1)
                SeguirJugador();
            else
                AnimacionInicioPartida();
        }
    }

    public void ChangeToFightView() {
        tiempoTransicion = 0;
        vistaPelea = true;
        transicionPelea = true;
    }

    public void ChangeToNormalView() {
        tiempoTransicion = 0;
        vistaPelea = false;
        transicionNormal = true;
    }

    private void ReiniciarVectorMovimiento() {
        vectorMovimiento = transformJugador.position + distanciaAlJugador;

        // X (Debe mantenerse en el centro del camino)
        vectorMovimiento.x = 0;

        // Y (Debe limitar el móvimiento vertical)
        vectorMovimiento.y = Mathf.Clamp(vectorMovimiento.y, 0, 15);
    }

    private void TransicionVistaPelea() {
        if (tiempoTransicion > 0.1f) {
            transicionPelea = false;
            transform.position = vectorMovimiento + desfasePosicionPelea;
            tiempoTransicion = 2;
        } else {
            transform.position = Vector3.Lerp(transform.position, desfasePosicionPelea + vectorMovimiento, tiempoTransicion);
            tiempoTransicion += Time.deltaTime * (1 / duracionAnimacion);
            transform.LookAt(transformJugador.position + Vector3.up * 2.5f);
        }
        transformJugador.position = new Vector3(0, 0, transformJugador.position.z);
    }

    private void TransicionVistaNormal() {
        if (tiempoTransicion > 0.1f) {
            transicionNormal = false;
            transform.position = vectorMovimiento;
            tiempoTransicion = 2;
        } else {
            transform.position = Vector3.Lerp(vectorMovimiento + desfasePosicionPelea, vectorMovimiento, tiempoTransicion * 10);
            tiempoTransicion += Time.deltaTime * (1 / duracionAnimacion);
            transform.LookAt(transformJugador.position + Vector3.up * 2.5f);
        }
        transformJugador.position = new Vector3(0, 0, transformJugador.position.z);
    }

    private void SeguirJugador() {
        if (vistaPelea)
            transform.position = vectorMovimiento + desfasePosicionPelea;
        else
            transform.position = vectorMovimiento;
    }

    private void AnimacionInicioPartida() {
        transform.position = Vector3.Lerp(vectorMovimiento + desfaseAnimacionInicio, vectorMovimiento, tiempoTransicion);
        tiempoTransicion += Time.deltaTime * (1 / duracionAnimacion);
        transform.LookAt(transformJugador.position + Vector3.up * 2.5f);
    }
}

package apalacios.pfg.util;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class Config {

    // Música
    public static float musicVolume = 0.8f;
    public static float soundVolume = 1;
    public static boolean fullScreen = true;
    public static int screenWidth = 1920;
    public static int screenHeight = 1080;

    public static void loadConfig() {
        Properties conf = new Properties();
        try {
            conf.load(new FileReader("src/apalacios/pfg/properties.conf"));
            musicVolume = Float.parseFloat(conf.getProperty("music_volume"));
            soundVolume = Float.parseFloat(conf.getProperty("sound_volume"));
            fullScreen = Boolean.parseBoolean(conf.getProperty("full_screen"));
            screenWidth = Integer.parseInt(conf.getProperty("screen_width"));
            screenHeight = Integer.parseInt(conf.getProperty("screen_height"));
        } catch(Exception ex) {
            saveConfig();
        }
    }

    public static void saveConfig() {
        Properties conf = new Properties();
        conf.setProperty("music_volume", String.valueOf(musicVolume));
        conf.setProperty("sound_volume", String.valueOf(soundVolume));
        conf.setProperty("full_screen", String.valueOf(fullScreen));
        conf.setProperty("screen_width", String.valueOf(screenWidth));
        conf.setProperty("screen_height", String.valueOf(screenHeight));
        try {
            conf.store(new FileWriter("src/apalacios/pfg/properties.conf"), "");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

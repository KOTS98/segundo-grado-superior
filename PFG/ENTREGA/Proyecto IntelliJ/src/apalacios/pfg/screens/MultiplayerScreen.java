package apalacios.pfg.screens;

import apalacios.pfg.characters.BaseActor;
import apalacios.pfg.characters.Player;
import apalacios.pfg.characters.Solid;
import apalacios.pfg.controllers.ControllerProfile;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.ArrayList;

public class MultiplayerScreen extends BaseGamepadScreen {

    // Perfiles
    private ArrayList<Profile> profiles;
    private ArrayList<Integer> profilesInUse;

    // Estado selección
    private enum Menu {
        PROFILE, COLOR, SKIN, ROUNDS, START
    }

    private Menu currentMenuP1;
    private int selectedOptionP1;
    private int maxMenuOptionP1;

    private Menu currentMenuP2;
    private int selectedOptionP2;
    private int maxMenuOptionP2;

    private Menu currentMenuP3;
    private int selectedOptionP3;
    private int maxMenuOptionP3;

    private Menu currentMenuP4;
    private int selectedOptionP4;
    private int maxMenuOptionP4;

    // Flechas de selección perfil
    private Label selectArrowLeftLabelP1;
    private Label selectArrowRightLabelP1;

    private Label selectArrowLeftLabelP2;
    private Label selectArrowRightLabelP2;

    private Label selectArrowLeftLabelP3;
    private Label selectArrowRightLabelP3;

    private Label selectArrowLeftLabelP4;
    private Label selectArrowRightLabelP4;

    // Seleccionar perfil
    private Label selectedProfileLabelP1;
    private int selectedProfileIdP1;

    private Label selectedProfileLabelP2;
    private int selectedProfileIdP2;

    private Label selectedProfileLabelP3;
    private int selectedProfileIdP3;

    private Label selectedProfileLabelP4;
    private int selectedProfileIdP4;

    // Pulsa un botón
    private Label pressBtnP2;
    private Label pressBtnP3;
    private Label pressBtnP4;

    // Seleccionar color
    private BaseActor bodyP1;
    private BaseActor wingP1;
    private BaseActor colorSquareP1;
    private int selectedColorP1;

    private BaseActor bodyP2;
    private BaseActor wingP2;
    private BaseActor colorSquareP2;
    private int selectedColorP2;

    private BaseActor bodyP3;
    private BaseActor wingP3;
    private BaseActor colorSquareP3;
    private int selectedColorP3;

    private BaseActor bodyP4;
    private BaseActor wingP4;
    private BaseActor colorSquareP4;
    private int selectedColorP4;

    // Seleccionar skin
    private BaseActor skinP1;
    private int selectedSkinP1;

    private BaseActor skinP2;
    private int selectedSkinP2;

    private BaseActor skinP3;
    private int selectedSkinP3;

    private BaseActor skinP4;
    private int selectedSkinP4;

    // Seleccionar rondas
    private BaseActor blackScreen;
    private Label seleccionaRondasLabel;
    private Label fiveLabel;
    private Label tenLabel;
    private Label fifteenLabel;

    // Animación jugadores
    private Player p1;
    private Player p2;
    private Player p3;
    private Player p4;

    // Controles
    private Controller controllerP1;
    private Controller controllerP2;
    private Controller controllerP3;
    private Controller controllerP4;

    private BaseActor loadingScreen;
    private boolean loading;

    // Sonido
    private Sound navigateSound;
    private Sound acceptSound;

    // Iniciar partida
    private boolean readyP1;
    private boolean readyP2;
    private boolean readyP3;
    private boolean readyP4;
    private boolean gameStarting;
    private long gameStartTime;

    @Override
    public void initialize() {

        // Perfiles
        profiles = Util.loadProfiles();

        // Estado selección
        currentMenuP1 = Menu.PROFILE;
        selectedOptionP1 = 1;
        maxMenuOptionP1 = profiles.size();

        currentMenuP2 = Menu.PROFILE;
        selectedOptionP2 = 1;
        maxMenuOptionP2 = profiles.size();

        currentMenuP3 = Menu.PROFILE;
        selectedOptionP3 = 1;
        maxMenuOptionP3 = profiles.size();

        currentMenuP4 = Menu.PROFILE;
        selectedOptionP4 = 1;
        maxMenuOptionP4 = profiles.size();

        // Fondo
        BaseActor background = new BaseActor(0, 0, mainStage);
        background.loadTexture("src/apalacios/pfg/assets/background/one_player_background.png");

        // Seleccionar perfil
        selectedProfileIdP1 = 0;
        selectedProfileIdP2 = 0;
        selectedProfileIdP3 = 0;
        selectedProfileIdP4 = 0;

        selectedProfileLabelP1 = new Label(profiles.get(0).getName(), BaseGame.styleNormal);
        selectedProfileLabelP1.setPosition(240 - (selectedProfileLabelP1.getWidth() / 2), 800);
        uiStage.addActor(selectedProfileLabelP1);

        selectedProfileLabelP2 = new Label(profiles.get(0).getName(), BaseGame.styleNormal);
        selectedProfileLabelP2.setPosition(720 - (selectedProfileLabelP2.getWidth() / 2), 800);
        uiStage.addActor(selectedProfileLabelP2);
        selectedProfileLabelP2.addAction(Actions.fadeOut(0));
        pressBtnP2 = new Label("J2 PULSA UN BOTON", BaseGame.styleNormal);
        pressBtnP2.setPosition(720 - (pressBtnP2.getWidth() / 2), 800);
        uiStage.addActor(pressBtnP2);

        selectedProfileLabelP3 = new Label(profiles.get(0).getName(), BaseGame.styleNormal);
        selectedProfileLabelP3.setPosition(1200 - (selectedProfileLabelP3.getWidth() / 2), 800);
        uiStage.addActor(selectedProfileLabelP3);
        selectedProfileLabelP3.addAction(Actions.fadeOut(0));
        pressBtnP3 = new Label("J3 PULSA UN BOTON", BaseGame.styleNormal);
        pressBtnP3.setPosition(1200 - (pressBtnP3.getWidth() / 2), 800);
        uiStage.addActor(pressBtnP3);

        selectedProfileLabelP4 = new Label(profiles.get(0).getName(), BaseGame.styleNormal);
        selectedProfileLabelP4.setPosition(1680 - (selectedProfileLabelP4.getWidth() / 2), 800);
        uiStage.addActor(selectedProfileLabelP4);
        selectedProfileLabelP4.addAction(Actions.fadeOut(0));
        pressBtnP4 = new Label("J4 PULSA UN BOTON", BaseGame.styleNormal);
        pressBtnP4.setPosition(1680 - (pressBtnP4.getWidth() / 2), 800);
        uiStage.addActor(pressBtnP4);

        // Seleccionar color
        selectedColorP1 = 1;
        colorSquareP1 = new BaseActor(-50, 0, uiStage);
        colorSquareP1.loadTexture("src/apalacios/pfg/assets/colors/color_1.png");
        colorSquareP1.setPosition(240 - (colorSquareP1.getWidth() / 2), 570);

        selectedColorP2 = 1;
        colorSquareP2 = new BaseActor(-50, 0, uiStage);
        colorSquareP2.loadTexture("src/apalacios/pfg/assets/colors/color_1.png");
        colorSquareP2.setPosition(720 - (colorSquareP2.getWidth() / 2), 570);
        colorSquareP2.addAction(Actions.fadeOut(0));

        selectedColorP3 = 1;
        colorSquareP3 = new BaseActor(-50, 0, uiStage);
        colorSquareP3.loadTexture("src/apalacios/pfg/assets/colors/color_1.png");
        colorSquareP3.setPosition(1200 - (colorSquareP3.getWidth() / 2), 570);
        colorSquareP3.addAction(Actions.fadeOut(0));

        selectedColorP4 = 1;
        colorSquareP4 = new BaseActor(-50, 0, uiStage);
        colorSquareP4.loadTexture("src/apalacios/pfg/assets/colors/color_1.png");
        colorSquareP4.setPosition(1680 - (colorSquareP4.getWidth() / 2), 570);
        colorSquareP4.addAction(Actions.fadeOut(0));

        bodyP1 = new BaseActor(-50, 0, uiStage);
        bodyP1.loadTexture("src/apalacios/pfg/assets/characters/pj1/pj1_stand_right.png");
        bodyP1.setPosition(240 - (bodyP1.getWidth() / 2), 365);
        bodyP1.setScale(1.5f);

        bodyP2 = new BaseActor(-50, 0, uiStage);
        bodyP2.loadTexture("src/apalacios/pfg/assets/characters/pj1/pj1_stand_right.png");
        bodyP2.setPosition(720 - (bodyP2.getWidth() / 2), 365);
        bodyP2.setScale(1.5f);
        bodyP2.addAction(Actions.fadeOut(0));

        bodyP3 = new BaseActor(-50, 0, uiStage);
        bodyP3.loadTexture("src/apalacios/pfg/assets/characters/pj1/pj1_stand_right.png");
        bodyP3.setPosition(1200 - (bodyP3.getWidth() / 2), 365);
        bodyP3.setScale(1.5f);
        bodyP3.addAction(Actions.fadeOut(0));

        bodyP4 = new BaseActor(-50, 0, uiStage);
        bodyP4.loadTexture("src/apalacios/pfg/assets/characters/pj1/pj1_stand_right.png");
        bodyP4.setPosition(1680 - (bodyP4.getWidth() / 2), 365);
        bodyP4.setScale(1.5f);
        bodyP4.addAction(Actions.fadeOut(0));

        wingP1 = new BaseActor(-50, 0, uiStage);
        wingP1.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_right.png");
        wingP1.setPosition(240 - (wingP1.getWidth() / 2), 365);
        wingP1.setScale(1.5f);

        wingP2 = new BaseActor(-50, 0, uiStage);
        wingP2.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_right.png");
        wingP2.setPosition(720 - (wingP2.getWidth() / 2), 365);
        wingP2.setScale(1.5f);
        wingP2.addAction(Actions.fadeOut(0));

        wingP3 = new BaseActor(-50, 0, uiStage);
        wingP3.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_right.png");
        wingP3.setPosition(1200 - (wingP3.getWidth() / 2), 365);
        wingP3.setScale(1.5f);
        wingP3.addAction(Actions.fadeOut(0));

        wingP4 = new BaseActor(-50, 0, uiStage);
        wingP4.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_right.png");
        wingP4.setPosition(1680 - (wingP4.getWidth() / 2), 365);
        wingP4.setScale(1.5f);
        wingP4.addAction(Actions.fadeOut(0));

        // Seleccionar skin
        selectedSkinP1 = 0;
        skinP1 = new BaseActor(-50, 0, uiStage);
        skinP1.loadTexture("src/apalacios/pfg/assets/characters/skins/sk0/right.png");
        skinP1.setPosition(240 - (skinP1.getWidth() / 2), 365);
        skinP1.setScale(1.5f);

        selectedSkinP2 = 0;
        skinP2 = new BaseActor(-50, 0, uiStage);
        skinP2.loadTexture("src/apalacios/pfg/assets/characters/skins/sk0/right.png");
        skinP2.setPosition(720 - (skinP2.getWidth() / 2), 365);
        skinP2.setScale(1.5f);
        skinP2.addAction(Actions.fadeOut(0));

        selectedSkinP3 = 0;
        skinP3 = new BaseActor(-50, 0, uiStage);
        skinP3.loadTexture("src/apalacios/pfg/assets/characters/skins/sk0/right.png");
        skinP3.setPosition(1200 - (skinP3.getWidth() / 2), 365);
        skinP3.setScale(1.5f);
        skinP3.addAction(Actions.fadeOut(0));

        selectedSkinP4 = 0;
        skinP4 = new BaseActor(-50, 0, uiStage);
        skinP4.loadTexture("src/apalacios/pfg/assets/characters/skins/sk0/right.png");
        skinP4.setPosition(1680 - (skinP4.getWidth() / 2), 365);
        skinP4.setScale(1.5f);
        skinP4.addAction(Actions.fadeOut(0));

        // Seleccionar rondas
        blackScreen = new BaseActor(0, 0, uiStage);
        blackScreen.loadTexture("src/apalacios/pfg/assets/background/blackScreen.png");
        blackScreen.addAction(Actions.fadeOut(0));

        seleccionaRondasLabel = new Label("NUMERO DE RONDAS", BaseGame.styleNormal);
        seleccionaRondasLabel.setPosition(960 - (seleccionaRondasLabel.getWidth() / 2), 700);
        uiStage.addActor(seleccionaRondasLabel);
        seleccionaRondasLabel.addAction(Actions.fadeOut(0));

        fiveLabel = new Label("5", BaseGame.styleNormal);
        fiveLabel.setPosition(960 - (fiveLabel.getWidth() / 2) - 100, 550);
        uiStage.addActor(fiveLabel);
        fiveLabel.addAction(Actions.fadeOut(0));

        tenLabel = new Label("10", BaseGame.styleNormal);
        tenLabel.setPosition(960 - (tenLabel.getWidth() / 2), 550);
        uiStage.addActor(tenLabel);
        tenLabel.addAction(Actions.fadeOut(0));

        fifteenLabel = new Label("15", BaseGame.styleNormal);
        fifteenLabel.setPosition(960 - (fifteenLabel.getWidth() / 2) + 100, 550);
        uiStage.addActor(fifteenLabel);
        fifteenLabel.addAction(Actions.fadeOut(0));

        // Flechas de selección
        selectArrowLeftLabelP1 = new Label("<", BaseGame.styleNormal);
        selectArrowLeftLabelP1.setPosition(20, 800);
        uiStage.addActor(selectArrowLeftLabelP1);
        selectArrowLeftLabelP1.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (-10, 0, 0.5f), Actions.moveBy(10, 0, 0.5f))));

        selectArrowRightLabelP1 = new Label(">", BaseGame.styleNormal);
        selectArrowRightLabelP1.setPosition(440, 800);
        uiStage.addActor(selectArrowRightLabelP1);
        selectArrowRightLabelP1.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (10, 0, 0.5f), Actions.moveBy(-10, 0, 0.5f))));

        selectArrowLeftLabelP2 = new Label("<", BaseGame.styleNormal);
        selectArrowLeftLabelP2.setPosition(500, 800);
        uiStage.addActor(selectArrowLeftLabelP2);
        selectArrowLeftLabelP2.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (-10, 0, 0.5f), Actions.moveBy(10, 0, 0.5f))));
        selectArrowLeftLabelP2.addAction(Actions.fadeOut(0));

        selectArrowRightLabelP2 = new Label(">", BaseGame.styleNormal);
        selectArrowRightLabelP2.setPosition(920, 800);
        uiStage.addActor(selectArrowRightLabelP2);
        selectArrowRightLabelP2.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (10, 0, 0.5f), Actions.moveBy(-10, 0, 0.5f))));
        selectArrowRightLabelP2.addAction(Actions.fadeOut(0));

        selectArrowLeftLabelP3 = new Label("<", BaseGame.styleNormal);
        selectArrowLeftLabelP3.setPosition(980, 800);
        uiStage.addActor(selectArrowLeftLabelP3);
        selectArrowLeftLabelP3.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (-10, 0, 0.5f), Actions.moveBy(10, 0, 0.5f))));
        selectArrowLeftLabelP3.addAction(Actions.fadeOut(0));

        selectArrowRightLabelP3 = new Label(">", BaseGame.styleNormal);
        selectArrowRightLabelP3.setPosition(1420, 800);
        uiStage.addActor(selectArrowRightLabelP3);
        selectArrowRightLabelP3.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (10, 0, 0.5f), Actions.moveBy(-10, 0, 0.5f))));
        selectArrowRightLabelP3.addAction(Actions.fadeOut(0));

        selectArrowLeftLabelP4 = new Label("<", BaseGame.styleNormal);
        selectArrowLeftLabelP4.setPosition(1480, 800);
        uiStage.addActor(selectArrowLeftLabelP4);
        selectArrowLeftLabelP4.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (-10, 0, 0.5f), Actions.moveBy(10, 0, 0.5f))));
        selectArrowLeftLabelP4.addAction(Actions.fadeOut(0));

        selectArrowRightLabelP4 = new Label(">", BaseGame.styleNormal);
        selectArrowRightLabelP4.setPosition(1880, 800);
        uiStage.addActor(selectArrowRightLabelP4);
        selectArrowRightLabelP4.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (10, 0, 0.5f), Actions.moveBy(-10, 0, 0.5f))));
        selectArrowRightLabelP4.addAction(Actions.fadeOut(0));

        // Jugadores listos
        readyP1 = false;
        readyP2 = false;
        readyP3 = false;
        readyP4 = false;

        // Suelo
        new Solid(0, 0, 1920, 100, mainStage);

        // Ventana carga
        loading = false;

        // Sonido
        navigateSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_navigate.ogg"));
        acceptSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_accept.ogg"));

        new Thread(() -> StartMenuScreen.titleMusic.setOnCompletionListener(music -> {
            if (StartMenuScreen.currentSong == 13)
                StartMenuScreen.currentSong = 1;
            else
                StartMenuScreen.currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (StartMenuScreen.currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            StartMenuScreen.titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            StartMenuScreen.titleMusic.setLooping(false);
            StartMenuScreen.titleMusic.play();
            StartMenuScreen.titleMusic.setVolume(Config.musicVolume);
        })).start();
    }

    private void showPlayer(Controller controller) {
        if (controllerP1 == null) {
            controllerP1 = controller;
        } else if (controllerP2 == null && controller != controllerP1) {
            controllerP2 = controller;
            pressBtnP2.addAction(Actions.fadeOut(0));
            selectedProfileLabelP2.addAction(Actions.fadeIn(0));
            colorSquareP2.addAction(Actions.fadeIn(0));
            bodyP2.addAction(Actions.fadeIn(0));
            wingP2.addAction(Actions.fadeIn(0));
            skinP2.addAction(Actions.fadeIn(0));
            selectArrowLeftLabelP2.addAction(Actions.fadeIn(0));
            selectArrowRightLabelP2.addAction(Actions.fadeIn(0));
        } else if(controllerP3 == null&&controller !=controllerP1 &&controller !=controllerP2) {
            controllerP3 = controller;
            pressBtnP3.addAction(Actions.fadeOut(0));
            selectedProfileLabelP3.addAction(Actions.fadeIn(0));
            colorSquareP3.addAction(Actions.fadeIn(0));
            bodyP3.addAction(Actions.fadeIn(0));
            wingP3.addAction(Actions.fadeIn(0));
            skinP3.addAction(Actions.fadeIn(0));
            selectArrowLeftLabelP3.addAction(Actions.fadeIn(0));
            selectArrowRightLabelP3.addAction(Actions.fadeIn(0));
        } else if (controllerP4 == null && controller != controllerP1 &&
                controller != controllerP2 && controller != controllerP3) {
            controllerP4 = controller;
            pressBtnP4.addAction(Actions.fadeOut(0));
            selectedProfileLabelP4.addAction(Actions.fadeIn(0));
            colorSquareP4.addAction(Actions.fadeIn(0));
            bodyP4.addAction(Actions.fadeIn(0));
            wingP4.addAction(Actions.fadeIn(0));
            skinP4.addAction(Actions.fadeIn(0));
            selectArrowLeftLabelP4.addAction(Actions.fadeIn(0));
            selectArrowRightLabelP4.addAction(Actions.fadeIn(0));
        }
    }

    @Override
    public boolean povMoved(Controller controller, int i, PovDirection povDirection) {
        showPlayer(controller);

        int nPlayer = 0;
        if (controller == controllerP1)
            nPlayer = 1;
        else if (controller == controllerP2)
            nPlayer = 2;
        else if (controller == controllerP3)
            nPlayer = 3;
        else if (controller == controllerP4)
            nPlayer = 4;

        if (playersReady()) {
            if (nPlayer == 1) {
                if (povDirection == PovDirection.east)
                    updateMenuSelection(1, currentMenuP1 != Menu.ROUNDS, 1);
                else if (povDirection == PovDirection.west)
                    updateMenuSelection(-1, currentMenuP1 != Menu.ROUNDS, 1);
            }
        } else {
            if (povDirection == PovDirection.east)
                updateMenuSelection(1, currentMenuP1 != Menu.ROUNDS, nPlayer);
            else if (povDirection == PovDirection.west)
                updateMenuSelection(-1, currentMenuP1 != Menu.ROUNDS, nPlayer);
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int i, float v) {
        showPlayer(controller);

        int nPlayer = 0;
        if (controller == controllerP1)
            nPlayer = 1;
        else if (controller == controllerP2)
            nPlayer = 2;
        else if (controller == controllerP3)
            nPlayer = 3;
        else if (controller == controllerP4)
            nPlayer = 4;

        if (playersReady()) {
            if (nPlayer == 1) {
                if (i == 3 && (v == 1 || v == -1))
                    updateMenuSelection((int) v, currentMenuP1 != Menu.ROUNDS, 1);
            }
        } else {
            if (i == 3 && (v == 1 || v == -1))
                updateMenuSelection((int) v, currentMenuP1 != Menu.ROUNDS, nPlayer);
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        showPlayer(controller);

        int nPlayer = 0;
        if (controller == controllerP1)
            nPlayer = 1;
        else if (controller == controllerP2)
            nPlayer = 2;
        else if (controller == controllerP3)
            nPlayer = 3;
        else if (controller == controllerP4)
            nPlayer = 4;

        if (playersReady()) {
            if (nPlayer == 1) {
                if (i == 0)
                    optionSelected(1);
                else if (i == 1)
                    goBack(1);
            }
        } else {
            if (i == 0)
                optionSelected(nPlayer);
            else if (i == 1)
                goBack(nPlayer);
        }
        return false;
    }

    private void updateMenuSelection(int value, boolean sound, int nPlayer) {
        switch (nPlayer) {
            case 1:
                if (sound && currentMenuP1 != Menu.START)
                    navigateSound.play(Config.soundVolume);

                if (value == 1) {
                    if (selectedOptionP1 + 1 <= maxMenuOptionP1)
                        selectedOptionP1++;
                    else
                        selectedOptionP1 = 1;
                } else if (value == -1){
                    if (selectedOptionP1 - 1 >= 1)
                        selectedOptionP1--;
                    else
                        selectedOptionP1 = maxMenuOptionP1;
                }

                switch (currentMenuP1) {
                    case PROFILE:
                        selectedProfileIdP1 = selectedOptionP1 - 1;
                        selectedProfileLabelP1.remove();
                        selectedProfileLabelP1 = new Label(profiles.get(selectedOptionP1 - 1).getName(), BaseGame.styleNormal);
                        selectedProfileLabelP1.setPosition(240 - (selectedProfileLabelP1.getWidth() / 2), 800);
                        uiStage.addActor(selectedProfileLabelP1);
                        break;

                    case COLOR:
                        selectedColorP1 = selectedOptionP1;
                        colorSquareP1.setAnimation(colorSquareP1.loadTexture("src/apalacios/pfg/assets/colors/color_" +
                                selectedColorP1 + ".png"));
                        bodyP1.setAnimation(bodyP1.loadTexture("src/apalacios/pfg/assets/characters/pj" +
                                selectedColorP1 + "/pj" + selectedColorP1 + "_stand_right.png"));
                        wingP1.setAnimation(wingP1.loadTexture("src/apalacios/pfg/assets/characters/pj" + selectedColorP1 +
                                "/aleta_" + selectedColorP1 + "_right.png"));
                        break;

                    case SKIN:
                        selectedSkinP1 = selectedOptionP1 - 1;
                        skinP1.setAnimation(skinP1.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" +
                                selectedSkinP1 + "/right.png"));
                        break;

                    case ROUNDS:
                        navigateSound.play(Config.soundVolume);
                        fiveLabel.setColor(Color.WHITE);
                        tenLabel.setColor(Color.WHITE);
                        fifteenLabel.setColor(Color.WHITE);

                        switch (selectedOptionP1) {
                            case 1:
                                GameScore.totalRounds = 5;
                                fiveLabel.setColor(Color.RED);
                                break;
                            case 2:
                                GameScore.totalRounds = 10;
                                tenLabel.setColor(Color.RED);
                                break;
                            case 3:
                                GameScore.totalRounds = 15;
                                fifteenLabel.setColor(Color.RED);
                                break;
                        }
                        break;
                }

                break;
            case 2:
                if (sound && currentMenuP2 != Menu.START)
                    navigateSound.play(Config.soundVolume);

                if (value == 1) {
                    if (selectedOptionP2 + 1 <= maxMenuOptionP2)
                        selectedOptionP2++;
                    else
                        selectedOptionP2 = 1;
                } else if (value == -1){
                    if (selectedOptionP2 - 1 >= 1)
                        selectedOptionP2--;
                    else
                        selectedOptionP2 = maxMenuOptionP2;
                }

                switch (currentMenuP2) {
                    case PROFILE:
                        selectedProfileIdP2 = selectedOptionP2 - 1;
                        selectedProfileLabelP2.remove();
                        selectedProfileLabelP2 = new Label(profiles.get(selectedOptionP2 - 1).getName(), BaseGame.styleNormal);
                        selectedProfileLabelP2.setPosition(720 - (selectedProfileLabelP2.getWidth() / 2), 800);
                        uiStage.addActor(selectedProfileLabelP2);
                        break;

                    case COLOR:
                        selectedColorP2 = selectedOptionP2;
                        colorSquareP2.setAnimation(colorSquareP2.loadTexture("src/apalacios/pfg/assets/colors/color_" +
                                selectedColorP2 + ".png"));
                        bodyP2.setAnimation(bodyP2.loadTexture("src/apalacios/pfg/assets/characters/pj" +
                                selectedColorP2 + "/pj" + selectedColorP2 + "_stand_right.png"));
                        wingP2.setAnimation(wingP2.loadTexture("src/apalacios/pfg/assets/characters/pj" + selectedColorP2 +
                                "/aleta_" + selectedColorP2 + "_right.png"));
                        break;

                    case SKIN:
                        selectedSkinP2 = selectedOptionP2 - 1;
                        skinP2.setAnimation(skinP2.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" +
                                selectedSkinP2 + "/right.png"));
                        break;

                    case ROUNDS:
                        navigateSound.play(Config.soundVolume);
                        fiveLabel.setColor(Color.WHITE);
                        tenLabel.setColor(Color.WHITE);
                        fifteenLabel.setColor(Color.WHITE);

                        switch (selectedOptionP2) {
                            case 1:
                                GameScore.totalRounds = 5;
                                fiveLabel.setColor(Color.RED);
                                break;
                            case 2:
                                GameScore.totalRounds = 10;
                                tenLabel.setColor(Color.RED);
                                break;
                            case 3:
                                GameScore.totalRounds = 15;
                                fifteenLabel.setColor(Color.RED);
                                break;
                        }
                        break;
                }
                break;
            case 3:
                if (sound && currentMenuP3 != Menu.START)
                    navigateSound.play(Config.soundVolume);

                if (value == 1) {
                    if (selectedOptionP3 + 1 <= maxMenuOptionP3)
                        selectedOptionP3++;
                    else
                        selectedOptionP3 = 1;
                } else if (value == -1){
                    if (selectedOptionP3 - 1 >= 1)
                        selectedOptionP3--;
                    else
                        selectedOptionP3 = maxMenuOptionP3;
                }

                switch (currentMenuP3) {
                    case PROFILE:
                        selectedProfileIdP3 = selectedOptionP3 - 1;
                        selectedProfileLabelP3.remove();
                        selectedProfileLabelP3 = new Label(profiles.get(selectedOptionP3 - 1).getName(), BaseGame.styleNormal);
                        selectedProfileLabelP3.setPosition(1200 - (selectedProfileLabelP3.getWidth() / 2), 800);
                        uiStage.addActor(selectedProfileLabelP3);
                        break;

                    case COLOR:
                        selectedColorP3 = selectedOptionP3;
                        colorSquareP3.setAnimation(colorSquareP3.loadTexture("src/apalacios/pfg/assets/colors/color_" +
                                selectedColorP3 + ".png"));
                        bodyP3.setAnimation(bodyP3.loadTexture("src/apalacios/pfg/assets/characters/pj" +
                                selectedColorP3 + "/pj" + selectedColorP3 + "_stand_right.png"));
                        wingP3.setAnimation(wingP3.loadTexture("src/apalacios/pfg/assets/characters/pj" + selectedColorP3 +
                                "/aleta_" + selectedColorP3 + "_right.png"));
                        break;

                    case SKIN:
                        selectedSkinP3 = selectedOptionP3 - 1;
                        skinP3.setAnimation(skinP3.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" +
                                selectedSkinP3 + "/right.png"));
                        break;

                    case ROUNDS:
                        navigateSound.play(Config.soundVolume);
                        fiveLabel.setColor(Color.WHITE);
                        tenLabel.setColor(Color.WHITE);
                        fifteenLabel.setColor(Color.WHITE);

                        switch (selectedOptionP3) {
                            case 1:
                                GameScore.totalRounds = 5;
                                fiveLabel.setColor(Color.RED);
                                break;
                            case 2:
                                GameScore.totalRounds = 10;
                                tenLabel.setColor(Color.RED);
                                break;
                            case 3:
                                GameScore.totalRounds = 15;
                                fifteenLabel.setColor(Color.RED);
                                break;
                        }
                        break;
                }
                break;
            case 4:
                if (sound && currentMenuP4 != Menu.START)
                    navigateSound.play(Config.soundVolume);

                if (value == 1) {
                    if (selectedOptionP4 + 1 <= maxMenuOptionP4)
                        selectedOptionP4++;
                    else
                        selectedOptionP4 = 1;
                } else if (value == -1){
                    if (selectedOptionP4 - 1 >= 1)
                        selectedOptionP4--;
                    else
                        selectedOptionP4 = maxMenuOptionP4;
                }

                switch (currentMenuP4) {
                    case PROFILE:
                        selectedProfileIdP4 = selectedOptionP4 - 1;
                        selectedProfileLabelP4.remove();
                        selectedProfileLabelP4 = new Label(profiles.get(selectedOptionP4 - 1).getName(), BaseGame.styleNormal);
                        selectedProfileLabelP4.setPosition(240 - (selectedProfileLabelP4.getWidth() / 2), 800);
                        uiStage.addActor(selectedProfileLabelP4);
                        break;

                    case COLOR:
                        selectedColorP4 = selectedOptionP4;
                        colorSquareP4.setAnimation(colorSquareP4.loadTexture("src/apalacios/pfg/assets/colors/color_" +
                                selectedColorP4 + ".png"));
                        bodyP4.setAnimation(bodyP4.loadTexture("src/apalacios/pfg/assets/characters/pj" +
                                selectedColorP4 + "/pj" + selectedColorP4 + "_stand_right.png"));
                        wingP4.setAnimation(wingP4.loadTexture("src/apalacios/pfg/assets/characters/pj" + selectedColorP4 +
                                "/aleta_" + selectedColorP4 + "_right.png"));
                        break;

                    case SKIN:
                        selectedSkinP4 = selectedOptionP4 - 1;
                        skinP4.setAnimation(skinP4.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" +
                                selectedSkinP4 + "/right.png"));
                        break;

                    case ROUNDS:
                        navigateSound.play(Config.soundVolume);
                        fiveLabel.setColor(Color.WHITE);
                        tenLabel.setColor(Color.WHITE);
                        fifteenLabel.setColor(Color.WHITE);

                        switch (selectedOptionP4) {
                            case 1:
                                GameScore.totalRounds = 5;
                                fiveLabel.setColor(Color.RED);
                                break;
                            case 2:
                                GameScore.totalRounds = 10;
                                tenLabel.setColor(Color.RED);
                                break;
                            case 3:
                                GameScore.totalRounds = 15;
                                fifteenLabel.setColor(Color.RED);
                                break;
                        }
                        break;
                }
                break;
        }
    }

    private void optionSelected(int nPlayer) {
        switch (nPlayer) {
            case 1:
                if (currentMenuP1 != Menu.START)
                    acceptSound.play(Config.soundVolume);

                switch (currentMenuP1) {
                    case PROFILE:
                        if (checkProfiles(1, selectedProfileIdP1)) {
                            currentMenuP1 = Menu.COLOR;
                            selectedOptionP1 = 1;
                            maxMenuOptionP1 = 8;
                            selectArrowLeftLabelP1.setPosition(selectArrowLeftLabelP1.getX() + 150,
                                    570 + (colorSquareP1.getHeight() / 2) - (selectArrowLeftLabelP1.getHeight() / 2));
                            selectArrowRightLabelP1.setPosition(selectArrowRightLabelP1.getX() - 150,
                                    570 + (colorSquareP1.getHeight() / 2) - (selectArrowRightLabelP1.getHeight() / 2));
                        }
                        break;

                    case COLOR:
                        currentMenuP1 = Menu.SKIN;
                        selectedOptionP1 = 1;
                        maxMenuOptionP1 = 15;
                        selectArrowLeftLabelP1.setPosition(selectArrowLeftLabelP1.getX(),
                                370 + (bodyP1.getHeight() / 2) - (selectArrowLeftLabelP1.getHeight() / 2));
                        selectArrowRightLabelP1.setPosition(selectArrowRightLabelP1.getX(),
                                370 + (bodyP1.getHeight() / 2) - (selectArrowRightLabelP1.getHeight() / 2));
                        break;

                    case SKIN:
                        readyP1 = true;
                        if (playersReady())
                            displayRoundSelection();
                        break;

                    case ROUNDS:
                        currentMenuP1 = Menu.START;
                        selectArrowLeftLabelP1.remove();
                        selectArrowRightLabelP1.remove();

                        bodyP1.remove();
                        wingP1.remove();
                        skinP1.remove();
                        p1 = new Player(240 - (bodyP1.getWidth() / 2), 365, mainStage, selectedColorP1, selectedSkinP1, false);
                        p1.setController(controllerP1);
                        for (ControllerProfile cp : profiles.get(selectedProfileIdP1).getControllerProfiles()) {
                            if (cp.getControllerType().equals(controllerP1.getName()))
                                p1.setControllerProfile(cp);
                        }
                        if (p1.getControllerProfile() == null)
                            p1.setControllerProfile(createDefaultControllerProfile());
                        p1.setPlayerName(profiles.get(selectedProfileIdP1).getName(), uiStage);
                        p1.setScale(1.5f);
                        p1.setMaxHorizontalSpeed(500);

                        bodyP2.remove();
                        wingP2.remove();
                        skinP2.remove();
                        p2 = new Player(720 - (bodyP2.getWidth() / 2), 365, mainStage, selectedColorP2, selectedSkinP2, false);
                        p2.setController(controllerP2);
                        for (ControllerProfile cp : profiles.get(selectedProfileIdP2).getControllerProfiles()) {
                            if (cp.getControllerType().equals(controllerP2.getName()))
                                p2.setControllerProfile(cp);
                        }
                        if (p2.getControllerProfile() == null)
                            p2.setControllerProfile(createDefaultControllerProfile());
                        p2.setPlayerName(profiles.get(selectedProfileIdP2).getName(), uiStage);
                        p2.setScale(1.5f);
                        p2.setMaxHorizontalSpeed(500);

                        if (controllerP3 != null) {
                            bodyP3.remove();
                            wingP3.remove();
                            skinP3.remove();
                            p3 = new Player(1200 - (bodyP3.getWidth() / 2), 365, mainStage, selectedColorP3, selectedSkinP3, false);
                            p3.setController(controllerP3);
                            for (ControllerProfile cp : profiles.get(selectedProfileIdP3).getControllerProfiles()) {
                                if (cp.getControllerType().equals(controllerP3.getName()))
                                    p3.setControllerProfile(cp);
                            }
                            if (p3.getControllerProfile() == null)
                                p3.setControllerProfile(createDefaultControllerProfile());
                            p3.setPlayerName(profiles.get(selectedProfileIdP3).getName(), uiStage);
                            p3.setScale(1.5f);
                            p3.setMaxHorizontalSpeed(500);
                        }

                        if (controllerP4 != null) {
                            bodyP4.remove();
                            wingP4.remove();
                            skinP4.remove();
                            p4 = new Player(1680 - (bodyP4.getWidth() / 2), 365, mainStage, selectedColorP4, selectedSkinP4, false);
                            p4.setController(controllerP4);
                            for (ControllerProfile cp : profiles.get(selectedProfileIdP4).getControllerProfiles()) {
                                if (cp.getControllerType().equals(controllerP4.getName()))
                                    p4.setControllerProfile(cp);
                            }
                            if (p4.getControllerProfile() == null)
                                p4.setControllerProfile(createDefaultControllerProfile());
                            p4.setPlayerName(profiles.get(selectedProfileIdP4).getName(), uiStage);
                            p4.setScale(1.5f);
                            p4.setMaxHorizontalSpeed(500);
                        }

                        // Animaciones
                        blackScreen.addAction(Actions.fadeOut(0.2f));
                        seleccionaRondasLabel.addAction(Actions.fadeOut(0.2f));
                        fiveLabel.addAction(Actions.fadeOut(0.2f));
                        tenLabel.addAction(Actions.fadeOut(0.2f));
                        fifteenLabel.addAction(Actions.fadeOut(0.2f));

                        loadingScreen = new BaseActor(1920, 1080, uiStage);
                        loadingScreen.addAction(Actions.fadeOut(0));

                        p1.moveRight();
                        p2.moveRight();
                        if (controllerP3 != null)
                            p3.moveRight();
                        if (controllerP4 != null)
                            p4.moveRight();

                        if (GameScore.totalRounds == 0)
                            GameScore.totalRounds = 10;
                        GameScore.currentRound = 1;
                        GameScore.lastLevel = 0;

                        if (controllerP3 == null) {
                            GameScore.nPlayers = 2;
                        } else {
                            if (controllerP4 == null) {
                                GameScore.nPlayers = 3;
                            } else {
                                GameScore.nPlayers = 4;
                            }
                        }

                        GameScore.scoreP1 = 0;
                        GameScore.scoreP2 = 0;
                        GameScore.scoreP3 = 0;
                        GameScore.scoreP4 = 0;

                        gameStartTime = System.currentTimeMillis();
                        gameStarting = true;
                        break;
                }
                break;

            case 2:
                if (currentMenuP2 != Menu.START)
                    acceptSound.play(Config.soundVolume);

                switch (currentMenuP2) {
                    case PROFILE:
                        if (checkProfiles(2, selectedProfileIdP2)) {
                            currentMenuP2 = Menu.COLOR;
                            selectedOptionP2 = 1;
                            maxMenuOptionP2 = 8;
                            selectArrowLeftLabelP2.setPosition(selectArrowLeftLabelP2.getX() + 150,
                                    570 + (colorSquareP2.getHeight() / 2) - (selectArrowLeftLabelP2.getHeight() / 2));
                            selectArrowRightLabelP2.setPosition(selectArrowRightLabelP2.getX() - 150,
                                    570 + (colorSquareP2.getHeight() / 2) - (selectArrowRightLabelP2.getHeight() / 2));
                        }
                        break;

                    case COLOR:
                        currentMenuP2 = Menu.SKIN;
                        selectedOptionP2 = 1;
                        maxMenuOptionP2 = 15;
                        selectArrowLeftLabelP2.setPosition(selectArrowLeftLabelP2.getX(),
                                370 + (bodyP2.getHeight() / 2) - (selectArrowLeftLabelP2.getHeight() / 2));
                        selectArrowRightLabelP2.setPosition(selectArrowRightLabelP2.getX(),
                                370 + (bodyP2.getHeight() / 2) - (selectArrowRightLabelP2.getHeight() / 2));
                        break;

                    case SKIN:
                        readyP2 = true;
                        if (playersReady())
                            displayRoundSelection();
                        break;
                }
                break;

            case 3:
                if (currentMenuP3 != Menu.START)
                    acceptSound.play(Config.soundVolume);

                switch (currentMenuP3) {
                    case PROFILE:
                        if (checkProfiles(3, selectedProfileIdP3)) {

                            currentMenuP3 = Menu.COLOR;
                            selectedOptionP3 = 1;
                            maxMenuOptionP3 = 8;
                            selectArrowLeftLabelP3.setPosition(selectArrowLeftLabelP3.getX() + 150,
                                    570 + (colorSquareP3.getHeight() / 2) - (selectArrowLeftLabelP3.getHeight() / 2));
                            selectArrowRightLabelP3.setPosition(selectArrowRightLabelP3.getX() - 150,
                                    570 + (colorSquareP3.getHeight() / 2) - (selectArrowRightLabelP3.getHeight() / 2));
                        }
                        break;

                    case COLOR:
                        currentMenuP3 = Menu.SKIN;
                        selectedOptionP3 = 1;
                        maxMenuOptionP3 = 15;
                        selectArrowLeftLabelP3.setPosition(selectArrowLeftLabelP3.getX(),
                                370 + (bodyP3.getHeight() / 2) - (selectArrowLeftLabelP3.getHeight() / 2));
                        selectArrowRightLabelP3.setPosition(selectArrowRightLabelP3.getX(),
                                370 + (bodyP3.getHeight() / 2) - (selectArrowRightLabelP3.getHeight() / 2));
                        break;

                    case SKIN:
                        readyP3 = true;
                        if (playersReady())
                            displayRoundSelection();
                        break;
                }
                break;

            case 4:
                if (currentMenuP4 != Menu.START)
                    acceptSound.play(Config.soundVolume);

                switch (currentMenuP4) {
                    case PROFILE:
                        if (checkProfiles(4, selectedProfileIdP4)) {
                            currentMenuP4 = Menu.COLOR;
                            selectedOptionP4 = 1;
                            maxMenuOptionP4 = 8;
                            selectArrowLeftLabelP4.setPosition(selectArrowLeftLabelP4.getX() + 150,
                                    570 + (colorSquareP4.getHeight() / 2) - (selectArrowLeftLabelP4.getHeight() / 2));
                            selectArrowRightLabelP4.setPosition(selectArrowRightLabelP4.getX() - 150,
                                    570 + (colorSquareP4.getHeight() / 2) - (selectArrowRightLabelP4.getHeight() / 2));
                        }
                        break;

                    case COLOR:
                        currentMenuP4 = Menu.SKIN;
                        selectedOptionP4 = 1;
                        maxMenuOptionP4 = 15;
                        selectArrowLeftLabelP4.setPosition(selectArrowLeftLabelP4.getX(),
                                370 + (bodyP4.getHeight() / 2) - (selectArrowLeftLabelP4.getHeight() / 2));
                        selectArrowRightLabelP4.setPosition(selectArrowRightLabelP4.getX(),
                                370 + (bodyP4.getHeight() / 2) - (selectArrowRightLabelP4.getHeight() / 2));
                        break;

                    case SKIN:
                        readyP4 = true;
                        if (playersReady())
                            displayRoundSelection();
                        break;
                }
                break;
        }
    }

    private ControllerProfile createDefaultControllerProfile() {
        ControllerProfile cp = new ControllerProfile();
        cp.setJoystickX(1);
        cp.setJoystickY(0);
        cp.setButtonJump(1);
        cp.setButtonShoot(2);
        cp.setButtonGrab(0);
        cp.setButtonThrow(3);
        cp.setButtonStart(7);
        return cp;
    }

    private boolean checkProfiles(int nPlayer, int profileId) {
        boolean repeated = false;

        if (profilesInUse == null)
            profilesInUse = new ArrayList<>();

        for (int p : profilesInUse) {
            if (p == profileId) {
                repeated = true;
                break;
            }
        }

        if (repeated) {
            switch (nPlayer) {
                case 1:
                    selectedProfileLabelP1.clearActions();
                    selectedProfileLabelP1.addAction(Actions.color(Color.RED, 0.5f));
                    selectedProfileLabelP1.addAction(Actions.after(Actions.color(Color.WHITE, 0.5f)));
                    break;
                case 2:
                    selectedProfileLabelP2.clearActions();
                    selectedProfileLabelP2.addAction(Actions.color(Color.RED, 0.5f));
                    selectedProfileLabelP2.addAction(Actions.after(Actions.color(Color.WHITE, 0.5f)));
                    break;
                case 3:
                    selectedProfileLabelP3.clearActions();
                    selectedProfileLabelP3.addAction(Actions.color(Color.RED, 0.5f));
                    selectedProfileLabelP3.addAction(Actions.after(Actions.color(Color.WHITE, 0.5f)));
                    break;
                case 4:
                    selectedProfileLabelP4.clearActions();
                    selectedProfileLabelP4.addAction(Actions.color(Color.RED, 0.5f));
                    selectedProfileLabelP4.addAction(Actions.after(Actions.color(Color.WHITE, 0.5f)));
                    break;
            }
            return false;
        } else {
            profilesInUse.add(profileId);
            return true;
        }
    }

    private void displayRoundSelection() {
        currentMenuP1 = Menu.ROUNDS;
        selectedOptionP1 = 2;
        maxMenuOptionP1 = 3;
        selectArrowLeftLabelP1.setPosition(fiveLabel.getX() - 30, fiveLabel.getY());
        selectArrowRightLabelP1.setPosition(fifteenLabel.getX() + fifteenLabel.getWidth() +
                30, fiveLabel.getY());

        blackScreen.addAction(Actions.fadeIn(0.2f));
        seleccionaRondasLabel.addAction(Actions.fadeIn(0.2f));
        fiveLabel.addAction(Actions.fadeIn(0.2f));
        tenLabel.addAction(Actions.fadeIn(0.2f));
        fifteenLabel.addAction(Actions.fadeIn(0.2f));

        tenLabel.setColor(Color.RED);

        selectArrowLeftLabelP2.addAction(Actions.fadeOut(0));
        selectArrowRightLabelP2.addAction(Actions.fadeOut(0));
        if (controllerP3 != null) {
            selectArrowLeftLabelP3.addAction(Actions.fadeOut(0));
            selectArrowRightLabelP3.addAction(Actions.fadeOut(0));
        }
        if (controllerP4 != null) {
            selectArrowLeftLabelP4.addAction(Actions.fadeOut(0));
            selectArrowRightLabelP4.addAction(Actions.fadeOut(0));
        }
    }

    private boolean playersReady() {
        if (readyP1 && readyP2) {
            if (controllerP3 != null) {
                if (readyP3) {
                    if (controllerP4 != null)
                        return readyP4;
                    else
                        return true;
                } else
                    return false;
            } else
                return true;
        } else
            return false;
    }

    private void goBack(int nPlayer) {
        switch (nPlayer) {
            case 1:
                switch (currentMenuP1) {
                    case PROFILE:
                        Util.lastScreen = LastScreen.ONEPLAYER;
                        PFGGame.setActiveScreen(new StartMenuScreen());
                        break;

                    case COLOR:
                        profilesInUse.remove(new Integer(selectedProfileIdP1));
                        currentMenuP1 = Menu.PROFILE;
                        selectedOptionP1 = selectedProfileIdP1 + 1;
                        maxMenuOptionP1 = profiles.size();
                        selectArrowLeftLabelP1.setPosition(20, 800);
                        selectArrowRightLabelP1.setPosition(440, 800);
                        break;

                    case SKIN:
                        readyP1 = false;
                        currentMenuP1 = Menu.COLOR;
                        selectedOptionP1 = selectedColorP1;
                        maxMenuOptionP1 = 8;
                        selectArrowLeftLabelP1.setPosition(selectArrowLeftLabelP1.getX(),
                                570 + (colorSquareP1.getHeight() / 2) - (selectArrowLeftLabelP1.getHeight() / 2));
                        selectArrowRightLabelP1.setPosition(selectArrowRightLabelP1.getX(),
                                570 + (colorSquareP1.getHeight() / 2) - (selectArrowRightLabelP1.getHeight() / 2));
                        break;

                    case ROUNDS:
                        readyP1 = false;
                        currentMenuP1 = Menu.SKIN;
                        selectedOptionP1 = selectedSkinP1;
                        maxMenuOptionP1 = 15;
                        selectArrowLeftLabelP1.setPosition(170, 370 + (colorSquareP1.getHeight() / 2) -
                                (selectArrowLeftLabelP1.getHeight() / 2));
                        selectArrowRightLabelP1.setPosition(290, 370 + (colorSquareP1.getHeight() / 2) -
                                (selectArrowRightLabelP1.getHeight() / 2));

                        blackScreen.addAction(Actions.fadeOut(0.2f));
                        seleccionaRondasLabel.addAction(Actions.fadeOut(0.2f));
                        fiveLabel.addAction(Actions.fadeOut(0.2f));
                        tenLabel.addAction(Actions.fadeOut(0.2f));
                        fifteenLabel.addAction(Actions.fadeOut(0.2f));

                        fiveLabel.setColor(Color.WHITE);
                        tenLabel.setColor(Color.WHITE);
                        fifteenLabel.setColor(Color.WHITE);

                        selectArrowLeftLabelP2.addAction(Actions.fadeIn(0));
                        selectArrowRightLabelP2.addAction(Actions.fadeIn(0));
                        if (controllerP3 != null) {
                            selectArrowLeftLabelP3.addAction(Actions.fadeIn(0));
                            selectArrowRightLabelP3.addAction(Actions.fadeIn(0));
                        }
                        if (controllerP4 != null) {
                            selectArrowLeftLabelP4.addAction(Actions.fadeIn(0));
                            selectArrowRightLabelP4.addAction(Actions.fadeIn(0));
                        }
                        break;
                }
                break;

            case 2:
                switch (currentMenuP2) {
                    case PROFILE:
                        Util.lastScreen = LastScreen.ONEPLAYER;
                        PFGGame.setActiveScreen(new StartMenuScreen());
                        break;

                    case COLOR:
                        profilesInUse.remove(new Integer(selectedProfileIdP2));
                        currentMenuP2 = Menu.PROFILE;
                        selectedOptionP2 = selectedProfileIdP2 + 2;
                        maxMenuOptionP2 = profiles.size();
                        selectArrowLeftLabelP2.setPosition(500, 800);
                        selectArrowRightLabelP2.setPosition(920, 800);
                        break;

                    case SKIN:
                        readyP2 = false;
                        currentMenuP2 = Menu.COLOR;
                        selectedOptionP2 = selectedColorP2;
                        maxMenuOptionP2 = 8;
                        selectArrowLeftLabelP2.setPosition(selectArrowLeftLabelP2.getX(),
                                570 + (colorSquareP2.getHeight() / 2) - (selectArrowLeftLabelP2.getHeight() / 2));
                        selectArrowRightLabelP2.setPosition(selectArrowRightLabelP2.getX(),
                                570 + (colorSquareP2.getHeight() / 2) - (selectArrowRightLabelP2.getHeight() / 2));
                        break;
                }
                break;

            case 3:
                switch (currentMenuP3) {
                    case PROFILE:
                        Util.lastScreen = LastScreen.ONEPLAYER;
                        PFGGame.setActiveScreen(new StartMenuScreen());
                        break;

                    case COLOR:
                        profilesInUse.remove(new Integer(selectedProfileIdP3));
                        currentMenuP3 = Menu.PROFILE;
                        selectedOptionP3 = selectedProfileIdP3 + 1;
                        maxMenuOptionP3 = profiles.size();
                        selectArrowLeftLabelP3.setPosition(980, 800);
                        selectArrowRightLabelP3.setPosition(1420, 800);
                        break;

                    case SKIN:
                        readyP3 = false;
                        currentMenuP3 = Menu.COLOR;
                        selectedOptionP3 = selectedColorP3;
                        maxMenuOptionP3 = 8;
                        selectArrowLeftLabelP3.setPosition(selectArrowLeftLabelP3.getX(),
                                570 + (colorSquareP3.getHeight() / 2) - (selectArrowLeftLabelP3.getHeight() / 2));
                        selectArrowRightLabelP3.setPosition(selectArrowRightLabelP3.getX(),
                                570 + (colorSquareP3.getHeight() / 2) - (selectArrowRightLabelP3.getHeight() / 2));
                        break;
                }
                break;

            case 4:
                switch (currentMenuP4) {
                    case PROFILE:
                        Util.lastScreen = LastScreen.ONEPLAYER;
                        PFGGame.setActiveScreen(new StartMenuScreen());
                        break;

                    case COLOR:
                        profilesInUse.remove(new Integer(selectedProfileIdP4));
                        currentMenuP4 = Menu.PROFILE;
                        selectedOptionP4 = selectedProfileIdP4 + 1;
                        maxMenuOptionP4 = profiles.size();
                        selectArrowLeftLabelP4.setPosition(1480, 800);
                        selectArrowRightLabelP4.setPosition(1880, 800);
                        break;

                    case SKIN:
                        readyP4 = false;
                        currentMenuP4 = Menu.COLOR;
                        selectedOptionP4 = selectedColorP1;
                        maxMenuOptionP4 = 8;
                        selectArrowLeftLabelP4.setPosition(selectArrowLeftLabelP4.getX(),
                                570 + (colorSquareP4.getHeight() / 2) - (selectArrowLeftLabelP4.getHeight() / 2));
                        selectArrowRightLabelP4.setPosition(selectArrowRightLabelP4.getX(),
                                570 + (colorSquareP4.getHeight() / 2) - (selectArrowRightLabelP4.getHeight() / 2));
                        break;
                }
                break;
        }
    }

    @Override
    public void update(float dt) {
        if (gameStarting){

            // COLISION JUGADORES SUELO
            for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
                for (BaseActor mobileActor : BaseActor.getList(mainStage, Player.class.getName())) {
                    Player playerActor = (Player) mobileActor;
                    if (playerActor.isAlive()) {
                        if (playerActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) &&
                                    playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setPosition(playerActor.getX(),
                                        solidActor.getY() + solidActor.getHeight() + 2);
                            }
                        }

                        if (playerActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) ||
                                    playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setGravityEnabled(false);
                                playerActor.setOnGround(true);
                            }
                        } else {
                            playerActor.preventOverlap(solidActor);
                        }
                    }
                }
            }

            long gameCurrentTime = System.currentTimeMillis();
            if (gameStartTime - gameCurrentTime < -3000 && !loading) {
                loading = true;
                loadingScreen.setPosition(0, 0);
                loadingScreen.setAnimation(
                        loadingScreen.loadTexture("src/apalacios/pfg/assets/background/loadingScreen.png"));
                loadingScreen.addAction(Actions.fadeIn(1f));
            }

            if (gameStartTime - gameCurrentTime < -5000) {
                if (controllerP3 == null) {
                    PFGGame.setActiveScreen(new LevelScreen(p1, p2, null, null, 1)); // nLevel -> Random
                } else {
                    if (controllerP4 == null) {
                        PFGGame.setActiveScreen(new LevelScreen(p1, p2, p3, null, 1)); // nLevel -> Random
                    } else {
                        PFGGame.setActiveScreen(new LevelScreen(p1, p2, p3, p4, 1)); // nLevel -> Random
                    }
                }
            }
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
        mainStage.getViewport().update(arg0, arg1, true);
    }
}

package apalacios.pfg.screens;

import apalacios.pfg.characters.BaseActor;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.LastScreen;
import apalacios.pfg.util.Profile;
import apalacios.pfg.util.Util;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.StringBuilder;

import java.util.ArrayList;

public class ProfilesScreen extends BaseGamepadScreen {
    private long startInteractionTime;
    private BaseActor television;
    private int selectedOption;
    private int maxMenuOption;
    private Controller currentController;

    private enum Menu {
        MAIN, NEW_PROFILE, EDIT_PROFILE_SELECT, EDIT_PROFILE, EDIT_DELETE_PROFILE, EXPORT_PROFILE
    }
    private Menu currentMenu;

    // Perfiles
    public static ArrayList<Profile> profiles;

    // Menú principal
    private Label nuevoPerfilLabel;
    private Label editarPerfilLabel;
    private Label importarPerfilLabel;
    private Label exportarPerfilLabel;

    // Menú crear perfil
    private Label nombreLabel;
    private String typedNameText;
    private Label typedNameLabel;
    private BaseActor blackScreen;

    // Menú seleccionar perfil
    private Label seleccionaPerfilLabel;
    private Label selectedProfileLabel;
    private Label selectArrowLeftLabel;
    private Label selectArrowRightLabel;
    private int selectedProfileId;

    // Menú editar perfil
    private Label profileNameLabel;
    private Label addMandoLabel;
    private Label probarPerfilLabel;
    private Label borrarPerfilLabel;

    // Menú eliminar perfil
    private BaseActor deleteProfileBlackScreen;
    private Label eliminarPerfilLabel;
    private Label siEliminarPerfilLabel;
    private Label noEliminarPerfilLabel;
    private boolean deleteProfileSelection;

    // Sonido
    private Sound navigateSound;
    private Sound acceptSound;

    @Override
    public void initialize() {
        startInteractionTime = System.currentTimeMillis();

        profiles = Util.loadProfiles();

        BaseActor background = new BaseActor(0, 0, mainStage);
        background.loadTexture("src/apalacios/pfg/assets/background/perfiles_fondo.png");
        television = new BaseActor(0, 0, mainStage);
        television.loadTexture("src/apalacios/pfg/assets/background/tv_nuevo_perfil.png");

        // Sonidos
        navigateSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_navigate.ogg"));
        acceptSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_accept.ogg"));

        // Menu principal
        nuevoPerfilLabel = new Label("NUEVO PERFIL", BaseGame.styleNormal);
        nuevoPerfilLabel.setPosition(1230, -50);
        uiStage.addActor(nuevoPerfilLabel);

        editarPerfilLabel = new Label("EDITAR PERFIL", BaseGame.styleNormal);
        editarPerfilLabel.setPosition(1225, -50);
        uiStage.addActor(editarPerfilLabel);

        importarPerfilLabel = new Label("IMPORTAR PERFIL", BaseGame.styleNormal);
        importarPerfilLabel.setPosition(1205, -50);
        uiStage.addActor(importarPerfilLabel);

        exportarPerfilLabel = new Label("EXPORTAR PERFIL", BaseGame.styleNormal);
        exportarPerfilLabel.setPosition(1200, -50);
        uiStage.addActor(exportarPerfilLabel);

        // Menú nuevo perfil
        blackScreen = new BaseActor(0, 0, uiStage);
        blackScreen.loadTexture("src/apalacios/pfg/assets/background/blackScreen.png");
        blackScreen.addAction(Actions.fadeOut(0));

        nombreLabel = new Label("NOMBRE DEL PERFIL", BaseGame.styleNormal);
        nombreLabel.setPosition(960 - (nombreLabel.getWidth() / 2), -50);
        uiStage.addActor(nombreLabel);

        typedNameText = "";

        typedNameLabel = new Label("", BaseGame.styleNormal);
        typedNameLabel.setPosition(972, 620); // X - 12.5 por letra
        uiStage.addActor(typedNameLabel);

        // Menú seleccionar perfil
        selectedProfileId = 0;

        seleccionaPerfilLabel = new Label("SELECCIONAR PERFIL", BaseGame.styleNormal);
        seleccionaPerfilLabel.setPosition(960 - (seleccionaPerfilLabel.getWidth() / 2), -50);
        uiStage.addActor(seleccionaPerfilLabel);

        selectedProfileLabel = new Label("", BaseGame.styleNormal);
        selectedProfileLabel.setPosition(972, -50);
        uiStage.addActor(selectedProfileLabel);

        selectArrowLeftLabel = new Label("<", BaseGame.styleNormal);
        selectArrowLeftLabel.setPosition(740, -50);
        uiStage.addActor(selectArrowLeftLabel);
        selectArrowLeftLabel.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (-10, 0, 0.5f), Actions.moveBy(10, 0, 0.5f))));

        selectArrowRightLabel = new Label(">", BaseGame.styleNormal);
        selectArrowRightLabel.setPosition(1160, -50);
        uiStage.addActor(selectArrowRightLabel);
        selectArrowRightLabel.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (10, 0, 0.5f), Actions.moveBy(-10, 0, 0.5f))));

        // Menú editar perfil
        profileNameLabel = new Label("", BaseGame.styleNormal);
        profileNameLabel.setPosition(1350, -50);
        uiStage.addActor(profileNameLabel);

        addMandoLabel = new Label("NUEVO MANDO", BaseGame.styleNormal);
        addMandoLabel.setPosition(1230, -50);
        uiStage.addActor(addMandoLabel);

        probarPerfilLabel = new Label("PROBAR PERFIL", BaseGame.styleNormal);
        probarPerfilLabel.setPosition(1220, -50);
        uiStage.addActor(probarPerfilLabel);

        borrarPerfilLabel = new Label("BORRAR PERFIL", BaseGame.styleNormal);
        borrarPerfilLabel.setPosition(1220, -50);
        uiStage.addActor(borrarPerfilLabel);

        // Menú eliminar perfil
        deleteProfileBlackScreen = new BaseActor(0, 0, uiStage);
        deleteProfileBlackScreen.loadTexture("src/apalacios/pfg/assets/background/blackScreen.png");
        deleteProfileBlackScreen.addAction(Actions.fadeOut(0));

        eliminarPerfilLabel = new Label ("ELIMINAR ESTE PERFIL?", BaseGame.styleNormal);
        eliminarPerfilLabel.setPosition(720, -50);
        uiStage.addActor(eliminarPerfilLabel);

        siEliminarPerfilLabel = new Label("SI", BaseGame.styleNormal);
        siEliminarPerfilLabel.setPosition(800, -50);
        uiStage.addActor(siEliminarPerfilLabel);

        noEliminarPerfilLabel = new Label("NO", BaseGame.styleNormal);
        noEliminarPerfilLabel.setPosition(1000, -50);
        uiStage.addActor(noEliminarPerfilLabel);
        noEliminarPerfilLabel.setColor(Color.RED);

        deleteProfileSelection = false;

        animateMainMenu(true);

        selectedOption = 0;
        maxMenuOption = 4;
        currentMenu = Menu.MAIN;
        updateMenuSelection(1, false);

        // Música
        new Thread(() -> StartMenuScreen.titleMusic.setOnCompletionListener(music -> {
            if (StartMenuScreen.currentSong == 13)
                StartMenuScreen.currentSong = 1;
            else
                StartMenuScreen.currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (StartMenuScreen.currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            StartMenuScreen.titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            StartMenuScreen.titleMusic.setLooping(false);
            StartMenuScreen.titleMusic.play();
            StartMenuScreen.titleMusic.setVolume(Config.musicVolume);
        })).start();
    }

    private void animateMainMenu(boolean intro) {
        if (intro) {
            startInteractionTime = System.currentTimeMillis();
            nuevoPerfilLabel.addAction(Actions.moveTo(1230, 600, 0.2f));
            editarPerfilLabel.addAction(Actions.moveTo(1225, 520, 0.3f));
            importarPerfilLabel.addAction(Actions.moveTo(1205, 440, 0.4f));
            exportarPerfilLabel.addAction(Actions.moveTo(1200, 360, 0.5f));
        } else {
            startInteractionTime = System.currentTimeMillis();
            nuevoPerfilLabel.addAction(Actions.moveTo(1930, 600, 0.5f));
            editarPerfilLabel.addAction(Actions.moveTo(1930, 520, 0.4f));
            importarPerfilLabel.addAction(Actions.moveTo(1930, 440, 0.3f));
            exportarPerfilLabel.addAction(Actions.moveTo(1930, 360, 0.2f));

            // Reposicionar abajo
            nuevoPerfilLabel.addAction(Actions.after(Actions.moveTo(1230, -50)));
            editarPerfilLabel.addAction(Actions.after(Actions.moveTo(1225, -50)));
            importarPerfilLabel.addAction(Actions.after(Actions.moveTo(1205, -50)));
            exportarPerfilLabel.addAction(Actions.after(Actions.moveTo(1200, -50)));
        }
    }

    private void animateNewProfileMenu(boolean intro) {
        if (intro) {
            startInteractionTime = System.currentTimeMillis();
            blackScreen.addAction(Actions.fadeIn(0.2f));
            nombreLabel.addAction(Actions.moveTo(780, 700, 0.2f));
        } else {
            startInteractionTime = System.currentTimeMillis();
            blackScreen.addAction(Actions.fadeOut(0.2f));
            nombreLabel.addAction(Actions.moveTo(1930, 700, 0.2f));
            typedNameLabel.setText("");
            typedNameLabel.setPosition(972, 620);

            // Reposicionar abajo
            nombreLabel.addAction(Actions.after(Actions.moveTo(780, -50)));
            typedNameLabel.addAction(Actions.after(Actions.moveTo(972, 620)));
        }
    }

    private void animateProfileSelectMenu(boolean intro) {
        if (intro) {
            startInteractionTime = System.currentTimeMillis();
            blackScreen.addAction(Actions.fadeIn(0.2f));
            selectedProfileLabel.remove();
            selectedProfileLabel = new Label(profiles.get(0).getName(), BaseGame.styleNormal);
            selectedProfileLabel.setPosition(960 - (selectedProfileLabel.getWidth() / 2), -50);
            uiStage.addActor(selectedProfileLabel);
            seleccionaPerfilLabel.addAction(Actions.moveTo(960 - (seleccionaPerfilLabel.getWidth() / 2), 700, 0.2f));
            selectedProfileLabel.addAction(Actions.moveTo(960 -
                    (selectedProfileLabel.getWidth() / 2), 620, 0.2f));
            selectArrowLeftLabel.addAction(Actions.moveTo(910 - (seleccionaPerfilLabel.getWidth() / 2), 620, 0.2f));
            selectArrowRightLabel.addAction(Actions.moveTo(1010 + (seleccionaPerfilLabel.getWidth() / 2), 620, 0.2f));
        } else {
            startInteractionTime = System.currentTimeMillis();
            blackScreen.addAction(Actions.fadeOut(0.2f));
            seleccionaPerfilLabel.addAction(Actions.moveTo(1930, 700, 0.2f));
            selectedProfileLabel.setText("");
            selectedProfileLabel.setPosition(1930, 620);
            selectArrowLeftLabel.addAction(Actions.moveTo(1930, 610, 0.2f));
            selectArrowRightLabel.addAction(Actions.moveTo(1930, 610, 0.2f));

            // Reposicionar abajo
            seleccionaPerfilLabel.addAction(Actions.after(Actions.moveTo(767.5f, -50)));
            selectedProfileLabel.addAction(Actions.after(Actions.moveTo(972, -50)));
            selectArrowLeftLabel.addAction(Actions.after(Actions.moveTo(740, -50)));
            selectArrowRightLabel.addAction(Actions.after(Actions.moveTo(1160, -50)));
        }
    }

    private void animateEditProfileMenu(boolean intro) {
        if (intro) {
            startInteractionTime = System.currentTimeMillis();
            profileNameLabel.remove();
            profileNameLabel = new Label(profiles.get(selectedProfileId).getName(), BaseGame.styleNormal);
            profileNameLabel.setText(profiles.get(selectedProfileId).getName());
            profileNameLabel.setPosition(1350 - (profileNameLabel.getWidth() / 2), -50);
            uiStage.addActor(profileNameLabel);
            profileNameLabel.addAction(Actions.moveTo(1350 - (profileNameLabel.getWidth() / 2), 700, 0.5f));
            addMandoLabel.addAction(Actions.moveTo(1350 - (addMandoLabel.getWidth() / 2), 520, 0.2f));
            probarPerfilLabel.addAction(Actions.moveTo(1350 - (probarPerfilLabel.getWidth() / 2), 440, 0.3f));
            borrarPerfilLabel.addAction(Actions.moveTo(1350 - (borrarPerfilLabel.getWidth() / 2), 360, 0.4f));
        } else {
            startInteractionTime = System.currentTimeMillis();
            profileNameLabel.addAction(Actions.moveTo(1930, 700, 0.5f));
            addMandoLabel.addAction(Actions.moveTo(1930, 520, 0.4f));
            probarPerfilLabel.addAction(Actions.moveTo(1930, 440, 0.3f));
            borrarPerfilLabel.addAction(Actions.moveTo(1930, 360, 0.2f));

            // Reposicionar abajo
            profileNameLabel.addAction(Actions.after(Actions.moveTo(profileNameLabel.getX(), -50)));
            addMandoLabel.addAction(Actions.after(Actions.moveTo(addMandoLabel.getX(), -50)));
            probarPerfilLabel.addAction(Actions.after(Actions.moveTo(probarPerfilLabel.getX(), -50)));
            borrarPerfilLabel.addAction(Actions.after(Actions.moveTo(borrarPerfilLabel.getX(), -50)));
        }
    }

    private void animateDeleteProfileMenu(boolean intro) {
        if (intro) {
            startInteractionTime = System.currentTimeMillis();
            deleteProfileBlackScreen.addAction(Actions.fadeIn(0.2f));
            eliminarPerfilLabel.addAction(Actions.moveTo(720, 700, 0.2f));
            siEliminarPerfilLabel.addAction(Actions.moveTo(800, 620, 0.3f));
            noEliminarPerfilLabel.addAction(Actions.moveTo(1000, 620, 0.3f));
        } else {
            startInteractionTime = System.currentTimeMillis();
            deleteProfileBlackScreen.addAction(Actions.fadeOut(0.2f));
            eliminarPerfilLabel.addAction(Actions.moveTo(1930, 700, 0.3f));
            siEliminarPerfilLabel.addAction(Actions.moveTo(1930, 620, 0.2f));
            noEliminarPerfilLabel.addAction(Actions.moveTo(1930, 620, 0.2f));

            // Reposicionar abajo
            eliminarPerfilLabel.addAction(Actions.after(Actions.moveTo(720, -50)));
            siEliminarPerfilLabel.addAction(Actions.after(Actions.moveTo(800, -50)));
            noEliminarPerfilLabel.addAction(Actions.after(Actions.moveTo(1000, -50)));
        }
    }

    @Override
    public boolean povMoved(Controller controller, int i, PovDirection povDirection) {
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (currentMenu != Menu.NEW_PROFILE && currentMenu != Menu.EDIT_PROFILE_SELECT &&
                    currentMenu != Menu.EDIT_DELETE_PROFILE && currentMenu != Menu.EXPORT_PROFILE) {
                if (povDirection == PovDirection.north)
                    updateMenuSelection(-1, true);
                else if (povDirection == PovDirection.south)
                    updateMenuSelection(1, true);
            } else if (currentMenu == Menu.EDIT_PROFILE_SELECT || currentMenu == Menu.EXPORT_PROFILE) {
                if (povDirection == PovDirection.east)
                    updateSelectedProfile(1);
                else if (povDirection == PovDirection.west)
                    updateSelectedProfile(-1);
            } else if (currentMenu == Menu.EDIT_DELETE_PROFILE) {
                if (povDirection == PovDirection.east || povDirection == PovDirection.west)
                    updateMenuSelection(0, true);
            }
        }
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int i, float v) {
        // i Representa la palanca del mando y el eje en el que se ha movido
        // Palanca derecha: Eje vertical -> 0 Eje horizontal -> 1
        // Palanca izquierda: Ejeve vertical -> 2 Eje horizontal -> 3
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (currentMenu != Menu.NEW_PROFILE && currentMenu != Menu.EDIT_PROFILE_SELECT &&
                    currentMenu != Menu.EDIT_DELETE_PROFILE && currentMenu != Menu.EXPORT_PROFILE) {
                if (i == 2 && (v == 1 || v == -1))
                    updateMenuSelection((int) v, true);
            } else if (currentMenu == Menu.EDIT_PROFILE_SELECT || currentMenu == Menu.EXPORT_PROFILE) {
                if (i == 3 && (v == 1 || v == -1))
                    updateSelectedProfile((int) v);
            } else if (currentMenu == Menu.EDIT_DELETE_PROFILE) {
                if (i == 3 && (v == 1 || v == -1))
                    updateMenuSelection(0, true);
            }
        }
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        currentController = controller;
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (i == 0)
                optionSelected();
            else if (i == 1)
                goBack();
        }
        return false;
    }

    private void optionSelected() {
        switch (currentMenu) {
            case MAIN:
                switch (selectedOption) {
                    case 1:
                        acceptSound.play(Config.soundVolume);
                        currentMenu = Menu.NEW_PROFILE;
                        selectedOption = 0;
                        maxMenuOption = 0;
                        animateNewProfileMenu(true);
                        break;

                    case 2:
                        if (profiles.size() > 0) {
                            acceptSound.play(Config.soundVolume);
                            currentMenu = Menu.EDIT_PROFILE_SELECT;
                            selectedOption = 0;
                            maxMenuOption = 0;
                            animateProfileSelectMenu(true);
                        }
                        break;

                    case 3:
                        acceptSound.play(Config.soundVolume);
                        Util.importProfile();
                        break;

                    case 4:
                        if (profiles.size() > 0) {
                            acceptSound.play(Config.soundVolume);
                            currentMenu = Menu.EXPORT_PROFILE;
                            selectedOption = 0;
                            maxMenuOption = 0;
                            animateProfileSelectMenu(true);
                        }
                        break;
                }
                break;

            case NEW_PROFILE:
                if (typedNameLabel.getText().length < 4) {
                    Label errorMsg = new Label("USA AL MENOS CUATRO CARACTERES!", BaseGame.styleNormal);
                    errorMsg.setColor(Color.RED);
                    errorMsg.setPosition(630, 780);
                    uiStage.addActor(errorMsg);
                    errorMsg.addAction(Actions.delay(2.5f));
                    errorMsg.addAction(Actions.after(Actions.fadeOut(0.5f)));
                    errorMsg.addAction(Actions.after(Actions.removeActor()));
                } else {
                    acceptSound.play(Config.soundVolume);
                    profiles.add(new Profile(typedNameLabel.getText().toString()));
                    Util.saveProfiles(profiles);
                    selectedProfileId = profiles.size() - 1;
                    currentMenu = Menu.EDIT_PROFILE;
                    selectedOption = 1;
                    maxMenuOption = 3;

                    animateNewProfileMenu(false);
                    animateMainMenu(false);
                    animateEditProfileMenu(true);
                    updateMenuSelection(0, false);
                }
                break;

            case EDIT_PROFILE_SELECT:
                acceptSound.play(Config.soundVolume);
                currentMenu = Menu.EDIT_PROFILE;
                selectedOption = 1;
                maxMenuOption = 3;
                animateProfileSelectMenu(false);
                animateMainMenu(false);
                animateEditProfileMenu(true);
                updateMenuSelection(0, false);
                break;

            case EDIT_PROFILE:
                acceptSound.play(Config.soundVolume);
                switch (selectedOption) {
                    case 1:
                        PFGGame.setActiveScreen(new AddControllerScreen(profiles.get(selectedProfileId)));
                        break;

                    case 2:
                        PFGGame.setActiveScreen(new TestControllerScreen(profiles.get(selectedProfileId), currentController));
                        break;

                    case 3:
                        currentMenu = Menu.EDIT_DELETE_PROFILE;
                        selectedOption = 0;
                        maxMenuOption = 0;
                        animateDeleteProfileMenu(true);
                        break;
                }
                break;

            case EDIT_DELETE_PROFILE:
                if (deleteProfileSelection) {
                    acceptSound.play(Config.soundVolume);
                    profiles.remove(selectedProfileId);
                    Util.saveProfiles(profiles);
                    currentMenu = Menu.MAIN;
                    selectedOption = 2;
                    maxMenuOption = 4;
                    if (profiles.size() > 0)
                        selectedProfileId = 0;

                    animateDeleteProfileMenu(false);
                    animateEditProfileMenu(false);
                    animateMainMenu(true);
                    television.setAnimation(television.loadTexture("src/apalacios/pfg/assets/background/" +
                            "tv_editar_perfil.png"));
                } else {
                    goBack();
                }
                break;

            case EXPORT_PROFILE:
                acceptSound.play(Config.soundVolume);
                Util.exportProfile(profiles.get(selectedProfileId));
                animateProfileSelectMenu(false);
                currentMenu = Menu.MAIN;
                selectedOption = 4;
                maxMenuOption = 4;
                break;
        }
    }

    private void goBack() {
        switch (currentMenu) {
            case MAIN:
                Util.saveProfiles(profiles);
                Util.lastScreen = LastScreen.PROFILES;
                PFGGame.setActiveScreen(new StartMenuScreen());
                break;

            case NEW_PROFILE:
                currentMenu = Menu.MAIN;
                selectedOption = 1;
                maxMenuOption = 4;
                typedNameText = "";

                animateNewProfileMenu(false);
                break;

            case EDIT_PROFILE:
                currentMenu = Menu.MAIN;
                selectedOption = 2;
                maxMenuOption = 4;

                animateEditProfileMenu(false);
                animateMainMenu(true);
                updateMenuSelection(0, false);
                break;

            case EDIT_DELETE_PROFILE:
                currentMenu = Menu.EDIT_PROFILE;
                selectedOption = 3;
                maxMenuOption = 3;

                animateDeleteProfileMenu(false);
                break;

            case EDIT_PROFILE_SELECT:
                currentMenu = Menu.MAIN;
                selectedOption = 2;
                maxMenuOption = 4;

                animateProfileSelectMenu(false);
                break;
        }
    }

    private void updateMenuSelection(int value, boolean sound) {
        if (sound)
            navigateSound.play(Config.soundVolume);
        if (value == 1) {
            if (selectedOption + 1 <= maxMenuOption)
                selectedOption++;
            else
                selectedOption = 1;
        } else if (value == -1) {
            if (selectedOption - 1 >= 1)
                selectedOption--;
            else
                selectedOption = maxMenuOption;
        }

        switch (currentMenu) {
            case MAIN:
                nuevoPerfilLabel.setColor(Color.WHITE);
                if (profiles.size() == 0)
                    editarPerfilLabel.setColor(Color.GRAY);
                else
                    editarPerfilLabel.setColor(Color.WHITE);
                importarPerfilLabel.setColor(Color.WHITE);
                exportarPerfilLabel.setColor(Color.WHITE);
                switch (selectedOption) {
                    case 1:
                        nuevoPerfilLabel.setColor(Color.RED);
                        television.setAnimation(television.loadTexture
                                ("src/apalacios/pfg/assets/background/tv_nuevo_perfil.png"));
                        break;

                    case 2:
                        if (profiles.size() == 0) {
                            updateMenuSelection(value, true);
                            return;
                        }
                        editarPerfilLabel.setColor(Color.RED);
                        television.setAnimation(television.loadTexture
                                ("src/apalacios/pfg/assets/background/tv_editar_perfil.png"));
                        break;

                    case 3:
                        importarPerfilLabel.setColor(Color.RED);
                        television.setAnimation(television.loadTexture
                                ("src/apalacios/pfg/assets/background/tv_importar_perfil.png"));
                        break;

                    case 4:
                        exportarPerfilLabel.setColor(Color.RED);
                        television.setAnimation(television.loadTexture
                                ("src/apalacios/pfg/assets/background/tv_exportar_perfil.png"));
                        break;
                }
                break;

            case EDIT_PROFILE:
                addMandoLabel.setColor(Color.WHITE);
                probarPerfilLabel.setColor(Color.WHITE);
                borrarPerfilLabel.setColor(Color.WHITE);
                switch (selectedOption) {
                    case 1:
                        addMandoLabel.setColor(Color.RED);
                        television.setAnimation(television.loadTexture
                                ("src/apalacios/pfg/assets/background/tv_add_mando.png"));
                        break;

                    case 2:
                        probarPerfilLabel.setColor(Color.RED);
                        television.setAnimation(television.loadTexture
                                ("src/apalacios/pfg/assets/background/tv_probar_perfil.png"));
                        break;

                    case 3:
                        borrarPerfilLabel.setColor(Color.RED);
                        television.setAnimation(television.loadTexture
                                ("src/apalacios/pfg/assets/background/tv_borrar_perfil.png"));
                        break;
                }
                break;

            case EDIT_DELETE_PROFILE:
                siEliminarPerfilLabel.setColor(Color.WHITE);
                noEliminarPerfilLabel.setColor(Color.WHITE);
                deleteProfileSelection = !deleteProfileSelection;
                if (deleteProfileSelection)
                    siEliminarPerfilLabel.setColor(Color.RED);
                else
                    noEliminarPerfilLabel.setColor(Color.RED);
                break;
        }
    }

    private void updateSelectedProfile(int value) {
        navigateSound.play(Config.soundVolume);
        if (value == 1) {
            if (selectedProfileId + 1 <= profiles.size() - 1)
                selectedProfileId++;
            else
                selectedProfileId = 0;
        } else if (value == -1) {
            if (selectedProfileId - 1 >= 0)
                selectedProfileId--;
            else
                selectedProfileId = profiles.size() - 1;
        }
        selectedProfileLabel.remove();
        selectedProfileLabel = new Label(profiles.get(selectedProfileId).getName(), BaseGame.styleNormal);
        selectedProfileLabel.setPosition(960 - (selectedProfileLabel.getWidth() / 2), 620);
        uiStage.addActor(selectedProfileLabel);
    }

    private void addCharacter(String character) {
        if (typedNameText.length() < 20) {
            typedNameText += character;
            typedNameLabel.remove();
            typedNameLabel = new Label(typedNameText, BaseGame.styleNormal);
            typedNameLabel.setPosition(960 - (typedNameLabel.getWidth() / 2), 620);
            uiStage.addActor(typedNameLabel);
        }
    }

    private void deleteCharacter(String text) {
        String[] chars = text.split("");
        StringBuilder finalText = new StringBuilder();

        for (int i = 0; i < text.length() - 1; i++)
            finalText.append(chars[i]);

        typedNameText = finalText.toString();
        typedNameLabel.remove();
        typedNameLabel = new Label(typedNameText, BaseGame.styleNormal);
        typedNameLabel.setPosition(960 - (typedNameLabel.getWidth() / 2), 620);
        uiStage.addActor(typedNameLabel);
    }

    @Override
    public void update(float dt) {
        long currentInteractionTime = System.currentTimeMillis();
        if (startInteractionTime - currentInteractionTime < -500) {
            if (currentMenu != Menu.NEW_PROFILE && currentMenu != Menu.EDIT_PROFILE_SELECT &&
                    currentMenu != Menu.EDIT_DELETE_PROFILE && currentMenu != Menu.EXPORT_PROFILE) {
                if (Gdx.input.isKeyJustPressed(Keys.UP))
                    updateMenuSelection(-1, true);
                else if (Gdx.input.isKeyJustPressed(Keys.DOWN))
                    updateMenuSelection(1, true);
            } else if (currentMenu == Menu.EDIT_PROFILE_SELECT || currentMenu == Menu.EXPORT_PROFILE) {
                if (Gdx.input.isKeyJustPressed(Keys.LEFT))
                    updateSelectedProfile(-1);
                else if (Gdx.input.isKeyJustPressed(Keys.RIGHT))
                    updateSelectedProfile(1);
            } else if (currentMenu == Menu.EDIT_DELETE_PROFILE) {
                if (Gdx.input.isKeyJustPressed(Keys.LEFT) || Gdx.input.isKeyJustPressed(Keys.RIGHT))
                    updateMenuSelection(0, true);
            }

            if (Gdx.input.isKeyJustPressed(Keys.ENTER))
                optionSelected();
            else if (Gdx.input.isKeyJustPressed(Keys.ESCAPE))
                goBack();

            if (currentMenu == Menu.NEW_PROFILE) {
                if (Gdx.input.isKeyJustPressed(Keys.A))
                    addCharacter("A");
                else if (Gdx.input.isKeyJustPressed(Keys.B))
                    addCharacter("B");
                else if (Gdx.input.isKeyJustPressed(Keys.C))
                    addCharacter("C");
                else if (Gdx.input.isKeyJustPressed(Keys.D))
                    addCharacter("D");
                else if (Gdx.input.isKeyJustPressed(Keys.E))
                    addCharacter("E");
                else if (Gdx.input.isKeyJustPressed(Keys.F))
                    addCharacter("F");
                else if (Gdx.input.isKeyJustPressed(Keys.G))
                    addCharacter("G");
                else if (Gdx.input.isKeyJustPressed(Keys.H))
                    addCharacter("H");
                else if (Gdx.input.isKeyJustPressed(Keys.I))
                    addCharacter("I");
                else if (Gdx.input.isKeyJustPressed(Keys.J))
                    addCharacter("J");
                else if (Gdx.input.isKeyJustPressed(Keys.K))
                    addCharacter("K");
                else if (Gdx.input.isKeyJustPressed(Keys.L))
                    addCharacter("L");
                else if (Gdx.input.isKeyJustPressed(Keys.M))
                    addCharacter("M");
                else if (Gdx.input.isKeyJustPressed(Keys.N))
                    addCharacter("N");
                else if (Gdx.input.isKeyJustPressed(Keys.O))
                    addCharacter("O");
                else if (Gdx.input.isKeyJustPressed(Keys.P))
                    addCharacter("P");
                else if (Gdx.input.isKeyJustPressed(Keys.Q))
                    addCharacter("Q");
                else if (Gdx.input.isKeyJustPressed(Keys.R))
                    addCharacter("R");
                else if (Gdx.input.isKeyJustPressed(Keys.S))
                    addCharacter("S");
                else if (Gdx.input.isKeyJustPressed(Keys.T))
                    addCharacter("T");
                else if (Gdx.input.isKeyJustPressed(Keys.U))
                    addCharacter("U");
                else if (Gdx.input.isKeyJustPressed(Keys.V))
                    addCharacter("V");
                else if (Gdx.input.isKeyJustPressed(Keys.W))
                    addCharacter("W");
                else if (Gdx.input.isKeyJustPressed(Keys.X))
                    addCharacter("X");
                else if (Gdx.input.isKeyJustPressed(Keys.Y))
                    addCharacter("Y");
                else if (Gdx.input.isKeyJustPressed(Keys.Z))
                    addCharacter("Z");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_0))
                    addCharacter("0");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_1))
                    addCharacter("1");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_2))
                    addCharacter("2");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_3))
                    addCharacter("3");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_4))
                    addCharacter("4");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_5))
                    addCharacter("5");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_6))
                    addCharacter("6");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_7))
                    addCharacter("7");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_8))
                    addCharacter("8");
                else if (Gdx.input.isKeyJustPressed(Keys.NUM_9))
                    addCharacter("9");
                else if (Gdx.input.isKeyJustPressed(Keys.BACKSPACE))
                    deleteCharacter(typedNameText);
            }
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
        mainStage.getViewport().update(arg0, arg1, true);
    }
}

package apalacios.pfg.weapons;

import apalacios.pfg.characters.Crate;
import apalacios.pfg.characters.Weapon;
import apalacios.pfg.util.Direction;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class CrateWeapon extends Weapon {

    public CrateWeapon(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 0, -1);
    }

    @Override
    public void drop() {
        Crate crate = new Crate(getX(), getY(), 36, 36, getStage());
        if (direction == Direction.RIGHT)
            crate.setPosition(getX() + 60, getY() + 50);
        else
            crate.setPosition(getX() - 20, getY() + 50);
        crate.drop(direction);
    }

    // No usados en la caja
    @Override
    public void shoot() {}
    @Override
    public void stopShooting() {}
    @Override
    public void reload() {}
}
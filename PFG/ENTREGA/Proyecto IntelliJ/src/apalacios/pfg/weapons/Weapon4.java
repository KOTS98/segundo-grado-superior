package apalacios.pfg.weapons;

import apalacios.pfg.characters.Proyectile;
import apalacios.pfg.characters.Weapon;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.Direction;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.Random;

public class Weapon4 extends Weapon {
    private double lastShot;
    private boolean shooting;
    private Sound sound1;
    private Sound sound2;
    private Sound sound3;
    private Sound sound4;

    public Weapon4(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 4, -1);
        shootSpeed = 200;
        nMunition = 1;
        shooting = false;
        lastShot = System.currentTimeMillis();

        sound1 = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/trumpet01.ogg"));
        sound2 = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/trumpet02.ogg"));
        sound3 = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/trumpet03.ogg"));
        sound4 = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/trumpet04.ogg"));
    }

    @Override
    public void shoot() {
        double currentShot = System.currentTimeMillis();
        if (lastShot - currentShot < -shootSpeed) {
            lastShot = currentShot;
            shooting = true;
            randomSound();

            if (direction == Direction.RIGHT)
                new Proyectile(getX() + (getWidth() / 2) + 40, getY() +
                        new Random().nextInt(150) + 1, getStage(), 300,
                        direction, false, 4, 250);
            else
                new Proyectile(getX() + (getWidth() / 2) - 40, getY() +
                        new Random().nextInt(150) + 1, getStage(), 300,
                        direction, false, 4, 250);
        }
    }

    @Override
    public void drop() {
        super.drop();
        sound1.stop();
        sound2.stop();
        sound3.stop();
        sound4.stop();
    }

    private void randomSound() {
        sound1.stop();
        sound2.stop();
        sound3.stop();
        sound4.stop();
        switch (new Random().nextInt(4) + 1) {
            case 1:
                sound1.play(Config.soundVolume / 4f);
                break;
            case 2:
                sound2.play(Config.soundVolume / 4f);
                break;
            case 3:
                sound3.play(Config.soundVolume / 4f);
                break;
            case 4:
                sound4.play(Config.soundVolume / 4f);
                break;
        }
    }

    @Override
    public void stopShooting() {
        shooting = false;
        sound1.stop();
        sound2.stop();
        sound3.stop();
        sound4.stop();
    }

    @Override
    public void reload() {

    }

    @Override
    public void act(float dt) {
        super.act(dt);
        if (shooting)
            shoot();
    }
}

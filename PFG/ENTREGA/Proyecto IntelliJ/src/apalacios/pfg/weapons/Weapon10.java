package apalacios.pfg.weapons;

import apalacios.pfg.characters.Weapon;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Weapon10 extends Weapon {


    public Weapon10(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 10, -1);
    }

    @Override
    public void shoot() {

    }

    @Override
    public void stopShooting() {

    }

    @Override
    public void reload() {

    }
}

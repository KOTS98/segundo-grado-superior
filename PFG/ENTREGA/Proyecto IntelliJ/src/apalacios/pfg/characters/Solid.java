package apalacios.pfg.characters;

import apalacios.pfg.characters.BaseActor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Solid extends BaseActor {
    private float posX;
    private float posY;
    private float width;
    private float height;

    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX  Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY  Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     */
    public Solid(float posX, float posY, float width, float height, Stage stage) {
        super(posX, posY, stage);

        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;

        this.setSize(width, height);
        setBoundaryRectangle();
    }

    public float getPosX() {
        return posX;
    }

    public float getPosY() {
        return posY;
    }

    @Override
    public float getWidth() {
        return width;
    }

    @Override
    public float getHeight() {
        return height;
    }
}

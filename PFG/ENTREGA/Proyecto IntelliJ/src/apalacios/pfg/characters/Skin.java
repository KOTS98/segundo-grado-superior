package apalacios.pfg.characters;

import apalacios.pfg.util.Direction;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Skin extends BaseActor {
    private boolean isCarry;
    private Direction direction;

    private Animation leftTexture;
    private Animation rightTexture;
    private Animation leftTextureAlt;
    private Animation rightTextureAlt;

    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX  Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY  Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     */
    public Skin(float posX, float posY, Stage stage, int nAnimation) {
        super(posX, posY, stage);

        isCarry = false;
        direction = Direction.RIGHT;

        leftTexture = loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nAnimation + "/left.png");
        rightTexture = loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nAnimation + "/right.png");
        leftTextureAlt = loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nAnimation + "/left_alt.png");
        rightTextureAlt = loadTexture("src/apalacios/pfg/assets/characters/skins/sk" + nAnimation + "/right_alt.png");
        setAnimation(rightTexture);

        setBoundaryRectangle();
    }

    void setCarry(boolean carry) {
        isCarry = carry;
        if (carry) {
            if (direction == Direction.LEFT) {
                setAnimation(leftTextureAlt);
            } else {
                setAnimation(rightTextureAlt);
            }
        } else {
            if (direction == Direction.LEFT) {
                setAnimation(leftTexture);
            } else {
                setAnimation(rightTexture);
            }
        }
    }

    void setDirection(Direction direction) {
        this.direction = direction;
        if (isCarry) {
            if (direction == Direction.LEFT) {
                setAnimation(leftTextureAlt);
            } else {
                setAnimation(rightTextureAlt);
            }
        } else {
            if (direction == Direction.LEFT) {
                setAnimation(leftTexture);
            } else {
                setAnimation(rightTexture);
            }
        }
    }
}
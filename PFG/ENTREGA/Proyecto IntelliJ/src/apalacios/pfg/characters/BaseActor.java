package apalacios.pfg.characters;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Intersector.MinimumTranslationVector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;

public class BaseActor extends Actor {

    // Animación
    private Animation<TextureRegion> animation;
    private float elapsedTime;
    private boolean animationPaused;

    // Velocidad y aceleración
    Vector2 velocityVec;
    Vector2 accelerationVec;
    private float acceleration;
    private float maxSpeed;
    private float deceleration;

    // Colisiones
    private Polygon boundaryPolygon;
    private static Rectangle worldBounds;

    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     * que se encarga de ordenar todos los elementos jerárquicamente
     */
    public BaseActor(float posX, float posY, Stage stage) {
        super();

        setPosition(posX, posY);
        stage.addActor(this);
        animation = null;
        elapsedTime = 0;
        animationPaused = false;

        velocityVec = new Vector2(0, 0);
        accelerationVec = new Vector2(0, 0);
        acceleration = 0;
        maxSpeed = 1000;
        deceleration = 0;
    }

    /**
     * Devuelve la lista de actores que corresponden a la clase especificada
     *
     * @param stage Representa en el stage donde se han añadido los actores
     * @param className Representa el nombre de la clase de la que se desean obtener actores
     * @return ArrayList<BaseActor> con los actores de la clase especificada
     */
    public static ArrayList<BaseActor> getList(Stage stage, String className) {
        ArrayList<BaseActor> list = new ArrayList<>();

        Class<?> theClass = null;
        try {
            theClass = Class.forName(className);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        for (Actor actor : stage.getActors()) {
            assert theClass != null;
            if (theClass.isInstance(actor))
                list.add((BaseActor) actor);
        }
        return list;
    }

    public static int count(Stage stage, String className) {
        return getList(stage, className).size();
    }

    /**
     * Establece la animación deseada. Es necesario establecer una animación al actor antes
     * de manipular otros parámetros como el tamaño, rotación, etc...
     *
     * @param animation Representa la animación deseada para el actor
     */
    public void setAnimation(Animation<TextureRegion> animation) {
        this.animation = animation;
        TextureRegion tr = animation.getKeyFrame(0);
        float width = tr.getRegionWidth();
        float height = tr.getRegionHeight();
        setSize(width, height);
        setOrigin(width / 2, height / 2);

        // Generar un polígono para colisiones basándose en las dimensiones del actor
        if (boundaryPolygon == null)
            setBoundaryRectangle();
    }

    /**
     * Pausa o reanuda la animación
     *
     * @param animationPaused [true = animación pausada, false = animación en ejecución]
     */
    public void setAnimationPaused(boolean animationPaused) {
        this.animationPaused = animationPaused;
    }

    /**
     * Actualiza automáticamente la imagen que debe ser mostrada por la animación basandose en el
     * tiempo de ejecución actual
     */
    public void act(float dt) {
        super.act(dt);

        // Determina si la animación esta pausada antes de ajustar el frame
        if(!animationPaused)
            elapsedTime += dt;
    }

    /**
     * Determina la imagen exacta a dibujar por la animación y la dibuja
     */
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        // Aplicar color sobre el sprite
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a);

        if (animation != null && isVisible())
            batch.draw(animation.getKeyFrame(elapsedTime), getX(), getY(), getOriginX(),
                    getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }

    /**
     * Crea una animación basada en una serie de archivos de imagen separados y establece la
     * animación del actor en caso de que no se haya establecido previamente
     *
     * @param fileNames Representa los nombres de todos los archivos que componen la imagen
     * @param frameDuration Representa el tiempo que se mantiene en pantalla cada cuadro de la
     * animación
     * @param loop Representa si la animación se ejecutará una sola vez, o se ejecutará en bucle
     * [false = se ejecuta una única vez, true = se ejecuta en bucle]
     * @return Devuelve la animación generada
     */
    public Animation<TextureRegion> loadAnimationFromFiles(String[] fileNames, float frameDuration,
                                                           boolean loop) {
        Array<TextureRegion> textureArray = new Array<>();

        for (String fileName : fileNames) {
            Texture texture = new Texture(Gdx.files.internal(fileName));
            texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
            textureArray.add(new TextureRegion(texture));
        }

        Animation<TextureRegion> anim = new Animation<>(frameDuration, textureArray);

        if (animation == null)
            setAnimation(anim);

        return anim;
    }

    /**
     * Crea una animación basada en una "spritesheet", es decir, un solo archivo que contiene
     * los diferentes frames de la animación ordenados en filas y columnas
     *
     * imagen
     * @param rows Representa la cantidad de filas que componen el archivo
     * @param cols Representa la cantidad de columnas que componen el archivo
     * @param frameDuration Representa el tiempo que se mantiene en pantalla cada cuadro de la
     * animación
     * @param loop Representa si la animación se ejecutará una sola vez, o se ejecutará en bucle
     * [false = se ejecuta una única vez, true = se ejecuta en bucle]
     * @return Devuelve la animación generada
     */
    public Animation<TextureRegion> loadAnimationFromSheet(String fileName, int rows, int cols,
                                                           float frameDuration, boolean loop) {
        Texture texture = new Texture(Gdx.files.internal(fileName), true);
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        int frameWidth = texture.getWidth() / cols;
        int frameHeight = texture.getHeight() / rows;

        TextureRegion[][] temp = TextureRegion.split(texture, frameWidth, frameHeight);

        Array<TextureRegion> textureArray = new Array<>();

        for (int i = 0; i < rows; i++)
            for (int j = 0; j < cols; j++)
                textureArray.add(temp[i][j]);

        Animation<TextureRegion> anim = new Animation<>(frameDuration, textureArray);

        if (loop)
            anim.setPlayMode(Animation.PlayMode.LOOP);
        else
            anim.setPlayMode(Animation.PlayMode.NORMAL);

        if (animation == null)
            setAnimation(anim);

        return anim;
    }

    /**
     * En caso de que el actor no posea una animación, si no una imágen estática, este método
     * establece dicha imagen como la "animacion" del actor
     *
     * @param fileName Representa el nombre del archivo que contiene la imagen deseada para el
     * actor
     * @return Devuelve la "animacion" (en este caso la imágen estática) del actor
     */
    public Animation<TextureRegion> loadTexture(String fileName) {
        String[] fileNames = new String[1];
        fileNames[0] = fileName;
        return loadAnimationFromFiles(fileNames, 1, true);
    }

    /*
     * Devuelve, en caso de que la animación del actor no se ejecute en bucle, true si esta se sigue
     * ejecutando, o false en caso contrario
     */
    public boolean isAnimationFinished() {
        return animation.isAnimationFinished(elapsedTime);
    }

    /**
     * Establece la velocidad de movimiento del actor
     *
     * @param speed Representa la velocidad deseada en pixeles por segundo
     */
    public void setSpeed(float speed) {
        // En caso de que la longitud sea 0, se asume que el angulo de movimiento es 0
        if (velocityVec.len() == 0)
            velocityVec.set(speed, 0);
        else
            velocityVec.setLength(speed);
    }

    /**
     * Devuelve la velocidad actual del actor en pixeles por segundo
     *
     * @return float que representa la velocidad del actor
     */
    public float getSpeed() {
        return velocityVec.len();
    }

    /**
     * Establece el ángulo en el que se moverá el jugador (en grados)
     *
     * @param angle Representa el ángulo en el que se moverá el actor
     */
    public void setMotionAngle(float angle) {
        velocityVec.setAngle(angle);
    }

    /**
     * Devuelve el ángulo al que está apuntando el actor
     *
     * @return float que representa el ángulo al que apunta el actor
     */
    public float getMotionAngle() {
        return velocityVec.angle();
    }

    /**
     * Determina si el actor está en movimiento o no, basándose en su velocidad
     * actual
     *
     * @return boolean que representa si el actor está o no en movimiento
     * [true = esta en movimiento, false = esta quieto]
     */
    public boolean isMoving() {
        return (getSpeed() > 0);
    }

    /**
     * Establece la aceleración deseada para el actor
     *
     * @param acceleration Representa la aceleración
     */
    public void setAcceleration(float acceleration) {
        this.acceleration = acceleration;
    }

    /**
     * Acelera el actor en el ángulo deseado
     *
     * @param angle Representa el ángulo en el que se desea acelerar
     */
    public void accelerateAtAngle(float angle) {
        accelerationVec.add(new Vector2(acceleration, 0).setAngle(angle));
    }

    /**
     * Acelera el actor en la dirección a la que esté apuntando actualmente
     *
     */
    public void accelerateForward() {
        accelerateAtAngle(getRotation());
    }

    /**
     * Establece la velocidad máxima a la que puede desplazarse el actor
     *
     * @param maxSpeed Representa la velocidad máxima
     */
    public void setMaxSpeed(float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    /**
     * Establece el factor de deceleración del actor
     *
     * @param deceleration Representa la deceleración
     */
    public void setDeceleration(float deceleration) {
        this.deceleration = deceleration;
    }

    /**
     * Realiza todas las funciones relacionadas con la velocidad y la aceleración,
     * lo que incluye:
     * - Ajustar el vector de velocidad basándose en el vector de aceleración
     * - Si el objeto no esta acelerando, aplicar la deceleración a la velocidad actual
     * - Asegurarse de que la velocidad no supera el valor de velocidad máxima
     * - Ajustar la posición del actor basándose en el vector de velocidad
     * - Reiniciar el vector de aceleración
     *
     * @param dt Representa el "delta time" (Tiempo de ejecución del último frame)
     */
    public void applyPhisics(float dt) {
        // Aplicar aceleración
        velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);

        float speed = getSpeed();

        // Reducir velocidad (decelerar) mientras no se este acelerando
        if (accelerationVec.len() == 0)
            speed -= deceleration * dt;

        // Matener la velocidad dentro de los márgenes
        speed = MathUtils.clamp(speed, 0, maxSpeed);

        // Actualizar velocidad
        setSpeed(speed);

        // Aplicar velocidad
        moveBy(velocityVec.x * dt, velocityVec.y * dt);

        // Reiniciar aceleración
        accelerationVec.set(0, 0);
    }

    /**
     * Genera un rectángulo de colisiones basándose en el ancho y el alto del actor
     */
    public void setBoundaryRectangle() {
        float width = getWidth();
        float height = getHeight();
        float[] vertices = {0,0, width,0, width,height, 0,height};
        boundaryPolygon = new Polygon(vertices);
    }

    /**
     * Genera un rectángulo de colisiones aproximado a una elipse, Ocupando todo el espacio
     * posible dentro de un rectángulo del mismo ancho y alto (Deben ser previamente establecidos).
     * La definición del polígono (número de lados) dependerá del valor recibido
     *
     * @param numSides Representa la cantidad de caras deseada para el polígono
     * @param extra Se utiliza para indicar si deseas que el poligono de colisión sea un 50% más grande
     */
    public void setBoundaryPolygon(int numSides, boolean extra) {
        float width = 0;
        float height = 0;

        if (extra) {
            width = getWidth() + (getWidth() / 8);
            height = getHeight() + (getHeight() / 8);
        } else {
            width = getWidth();
            height = getHeight();
        }

        float[] vertices = new float[2 * numSides];
        for (int i = 0;  i < numSides; i++) {
            float angle = i * 6.28f / numSides;

            // Coordenada X
            vertices[2 * i] = width / 2 * MathUtils.cos(angle) + width / 2;

            // Coordenada Y
            vertices[2 * i + 1] = height / 2 * MathUtils.sin(angle) + height / 2;
        }
        boundaryPolygon = new Polygon(vertices);
    }

    /**
     * Devuelve el polígono de colisión del actor, ajustándolo a los actuales parametros de
     * posición, origen, rotación y escala
     *
     * @return Polígono de colisión del actor
     */
    public Polygon getBoundaryPolygon() {
        boundaryPolygon.setPosition(getX(), getY());
        boundaryPolygon.setOrigin(getOriginX(), getOriginY());
        boundaryPolygon.setRotation(getRotation());
        boundaryPolygon.setScale(getScaleX(), getScaleY());
        return boundaryPolygon;
    }

    /**
     * Comprueba si el polígono de este actor solapa con el polígono del actor especificado
     *
     * @param otherActor Representa al segundo actor con el que se desea comprobar la colisión
     * @return boolean que determina si colisionan o no [true = colisionan, false = no colisionan]
     */
    public boolean overlaps(BaseActor otherActor) {
        Polygon polygon1 = this.getBoundaryPolygon();
        Polygon polygon2 = otherActor.getBoundaryPolygon();

        // Comprobación inicial para mejorar el rendimiento
        if (!polygon1.getBoundingRectangle().overlaps(polygon2.getBoundingRectangle()))
            return false;

        return Intersector.overlapConvexPolygons(polygon1, polygon2);
    }

    /**
     * Comprueba si el polígono de este actor solapa con el polígono del actor especificado y
     * en caso afirmativo, desplaza al actor a la ultima posición conocida en la que no solapaba
     * con el actor especificado
     *
     * @param otherActor Representa al segundo actor con el que se desa comprobar la colisión
     * @return mtv.normal en caso de una colisión, o null si no la hay
     */
    public Vector2 preventOverlap(BaseActor otherActor) {
        Polygon polygon1 = this.getBoundaryPolygon();
        Polygon polygon2 = otherActor.getBoundaryPolygon();

        // Comprobación inicial para mejorar el rendimiento
        if (!polygon1.getBoundingRectangle().overlaps(polygon2.getBoundingRectangle()))
            return null;

        MinimumTranslationVector mtv = new MinimumTranslationVector();
        boolean polygonOverlap = Intersector.overlapConvexPolygons(polygon1, polygon2, mtv);

        if(!polygonOverlap)
            return null;

        this.moveBy(mtv.normal.x * mtv.depth, mtv.normal.y * mtv.depth);
        return mtv.normal;
    }

    /**
     * Centra al actor en las coordenadas especificadas
     *
     * @param posX Representa la coordenada X (eje horizontal)
     * @param posY Representa la coordenada Y (eje vertical)
     */
    public void centerAtPosition(float posX, float posY) {
        setPosition(posX - getWidth() / 2, posY - getHeight() / 2);
    }

    /**
     * Centra el actor en la posición de otro actor especificado
     *
     * @param otherActor Representa al actor con el que se desea centrar
     */
    public void centerAtActor(BaseActor otherActor) {
        centerAtPosition(otherActor.getX() + otherActor.getWidth() / 2, otherActor.getY()
                + otherActor.getHeight() / 2);
    }

    /**
     * Establece la opacidad (canal alfa) del actor
     *
     * @param opacity Representa el valor de opacidad (0 - 255)
     */
    public void setOpacity(float opacity) {
        this.getColor().a = opacity;
    }

    /**
     * Establece los límites del mundo (Las barreras que delimitan hasta donde puede desplazarse
     * el actor) mediante dos valores númericos
     *
     * @param width Representa el ancho deseado para la barrera del mundo
     * @param height Representa el alto deseado para la barrera del mundo
     */
    public static void setWorldBounds(float width, float height) {
        worldBounds = new Rectangle(0, 0, width, height);
    }

    /**
     * Establece los límites del mundo (Las barreras que delimitan hasta donde puede desplazarse
     * el actor) basándose en la anchura y altura del actor especificado
     *
     * @param actor Representa al actor del que se obtendrá el ancho y el alto
     */
    public static void setWorldBounds(BaseActor actor) {
        setWorldBounds(actor.getWidth(), actor.getHeight());
    }

    /**
     * Comprueba que el actor se encuentra dentro de los límites del mundo, y lo devuelve a su
     * posición anterior en caso de exceder dichos límites
     */
    public void boundToWorld() {
        // Comprobar límite izquierdo
        if (getX() < 20)
            setX(20);
        // Comprobar límite derecho
        if (getX() + getWidth() + 20 > worldBounds.width)
            setX(worldBounds.width - getWidth() - 20);
        // Comprobar límite inferior
        if (getY() < 50)
            setY(50);
        // Comprobar límite superior
        if (getY() + getHeight() + 20 > worldBounds.height)
            setY(worldBounds.height - getHeight() - 20);
    }

    /**
     * Mantiene el campo de visión de la camara dentro de los límites del mundo
     */
    public void alignCamera() {
        Camera camera = this.getStage().getCamera();

        // Centrar la cámara en el actor
        camera.position.set(this.getX() + this.getOriginX(), this.getY() + this.getOriginY(), 0);

        // Atar cámara al actor
        camera.position.x = MathUtils.clamp(camera.position.x, camera.viewportWidth / 2,
                worldBounds.width - camera.viewportWidth / 2);
        camera.position.y = MathUtils.clamp(camera.position.y, camera.viewportHeight / 2,
                worldBounds.height - camera.viewportHeight / 2);
        camera.update();
    }
}

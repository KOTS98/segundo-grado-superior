package analizadorDePerfiles;

import apalacios.pfg.controllers.ControllerProfile;
import apalacios.pfg.util.Profile;
import apalacios.pfg.util.Util;

import java.util.ArrayList;

public class AnalisisPerfiles {
    public static void main(String[] args) {
        System.out.println("[INFO] Leyendo perfiles...");
        ArrayList<Profile> perfiles = Util.loadProfiles();

        System.out.println("\n============= CONTENIDO DEL FICHERO =============");
        for (Profile pr : perfiles) {
            System.out.println("> " + pr.getName() + " <\nControles registrados:\n");
            for (ControllerProfile cp : pr.getControllerProfiles()) {
                System.out.println("\t> " + cp.getControllerType() + " <");
                System.out.println("\tJoystick X:\t" + cp.getJoystickX() + "\tJoystick Y:\t" + cp.getJoystickY());
                System.out.println("\tSaltar:\t\t" + cp.getButtonJump() + "\tDisparar:\t" + cp.getButtonShoot());
                System.out.println("\tCoger:\t\t" + cp.getButtonGrab() + "\tLanzar:\t\t" + cp.getButtonThrow());
                System.out.println("\tStart:\t\t" + cp.getButtonStart());
                System.out.println("");
            }
            System.out.println("");
        }
        System.out.println("================ FIN DEL FICHERO ================");
    }
}

package apalacios.pfg.weapons;

import apalacios.pfg.characters.Proyectile;
import apalacios.pfg.characters.Weapon;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.Direction;
import apalacios.pfg.util.Util;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class Weapon2 extends Weapon {
    private double lastShot;

    public Weapon2(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 2, -1);

        if (nMunition == -1)
            nMunition = 9;

        shootSpeed = 250;
        lastShot = System.currentTimeMillis();
        shootSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/pistola.ogg"));
    }

    @Override
    public void shoot() {
        double currentShot = System.currentTimeMillis();
        if (lastShot - currentShot < -shootSpeed) {
            lastShot = currentShot;
            if (nMunition > 0) {
                shootSound.play(Config.soundVolume);
                if (direction == Direction.RIGHT)
                    new Proyectile(getX() + (getWidth() /2) + 40, getY() + 50, getStage(), 2000, direction,
                            false, 2, 200);
                else
                    new Proyectile(getX() + (getWidth() /2) - 40, getY() + 50, getStage(), 2000, direction,
                            false, 2, 200);
                nMunition--;
            } else {
                noAmmoSound.play(Config.soundVolume);
            }
        }
    }

    @Override
    public void stopShooting() {

    }

    @Override
    public void reload() {

    }
}

package apalacios.pfg.weapons;

import apalacios.pfg.characters.Proyectile;
import apalacios.pfg.characters.Weapon;
import apalacios.pfg.util.Config;
import apalacios.pfg.util.Direction;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.Random;

public class Weapon3 extends Weapon {
    private double lastShot;

    public Weapon3(float posX, float posY, Stage stage) {
        super(posX, posY, stage, 3, -1);
        if (nMunition == -1)
            nMunition = 5;

        shootSpeed = 250;
        lastShot = System.currentTimeMillis();
        shootSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/laser.ogg"));
    }

    @Override
    public void shoot() {
        double currentShot = System.currentTimeMillis();
        if (lastShot - currentShot < -shootSpeed) {
            lastShot = currentShot;
            if (nMunition > 0) {
                shootSound.play(Config.soundVolume);
                if (direction == Direction.RIGHT)
                    new Proyectile(getX() + (getWidth() /2) + 40, getY() + 50, getStage(), 1000, direction,
                            false, 3, 500);
                else
                    new Proyectile(getX() + (getWidth() /2) - 40, getY() + 50, getStage(), 1000, direction,
                            false, 3, 500);
                nMunition--;
            } else {
                noAmmoSound.play(Config.soundVolume);
            }
        }
    }

    @Override
    public void stopShooting() {

    }

    @Override
    public void reload() {

    }
}

package apalacios.pfg.screens;

import apalacios.pfg.characters.*;
import apalacios.pfg.controllers.ControllerProfile;
import apalacios.pfg.game.BaseGame;
import apalacios.pfg.game.PFGGame;
import apalacios.pfg.util.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.Random;

public class OnePlayerScreen extends BaseGamepadScreen {
    private Viewport viewPort;
    private Camera camera;

    // Perfiles
    private ArrayList<Profile> profiles;

    // Estado selección
    private enum Menu {
        PROFILE, COLOR, SKIN, ROUNDS, START
    }
    private Menu currentMenu;
    private int selectedOption;
    private int maxMenuOption;

    // Flechas de selección perfil
    private Label selectArrowLeftLabel;
    private Label selectArrowRightLabel;

    // Seleccionar perfil
    private Label selectedProfileLabel;
    private int selectedProfileId;

    // Seleccionar color
    private BaseActor player;
    private BaseActor wing;
    private BaseActor colorSquare;
    private int selectedColor;
    private ArrayList<Integer> colorsInUse;

    // Seleccionar skin
    private BaseActor skin;
    private int selectedSkin;
    private ArrayList<Integer> skinsInUse;

    // Seleccionar rondas
    private BaseActor blackScreen;
    private Label seleccionaRondasLabel;
    private Label fiveLabel;
    private Label tenLabel;
    private Label fifteenLabel;

    // Animación jugadores
    private Player p1;
    private Player cpu1;
    private Player cpu2;
    private Player cpu3;
    private BaseActor bodyCPU1;
    private BaseActor bodyCPU2;
    private BaseActor bodyCPU3;

    private BaseActor loadingScreen;
    private boolean loading;

    // Sonido
    private Sound navigateSound;
    private Sound acceptSound;

    // Iniciar partida
    private boolean gameStarting;
    private long gameStartTime;

    @Override
    public void initialize() {
        camera = new PerspectiveCamera();
        viewPort = new FitViewport(1920, 1080, camera);

        // Perfiles
        profiles = Util.loadProfiles();

        // Estado selección
        currentMenu = Menu.PROFILE;
        selectedOption = 1;
        maxMenuOption = profiles.size();

        // Fondo
        BaseActor background = new BaseActor(0, 0, mainStage);
        background.loadTexture("src/apalacios/pfg/assets/background/one_player_background.png");

        // Seleccionar perfil
        selectedProfileId = 0;

        selectedProfileLabel = new Label(profiles.get(0).getName(), BaseGame.styleNormal);
        selectedProfileLabel.setPosition(240 - (selectedProfileLabel.getWidth() / 2), 800);
        uiStage.addActor(selectedProfileLabel);

        Label cpu1Label = new Label("CPU 1", BaseGame.styleNormal);
        cpu1Label.setPosition(720 - (cpu1Label.getWidth() / 2), 800);
        uiStage.addActor(cpu1Label);

        Label cpu2Label = new Label("CPU 2", BaseGame.styleNormal);
        cpu2Label.setPosition(1200 - (cpu2Label.getWidth() / 2), 800);
        uiStage.addActor(cpu2Label);

        Label cpu3Label = new Label("CPU 3", BaseGame.styleNormal);
        cpu3Label.setPosition(1680 - (cpu3Label.getWidth() / 2), 800);
        uiStage.addActor(cpu3Label);

        // Seleccionar color
        selectedColor = 1;
        colorSquare = new BaseActor(-50, 0, uiStage);
        colorSquare.loadTexture("src/apalacios/pfg/assets/colors/color_1.png");
        colorSquare.setPosition(240 - (colorSquare.getWidth() / 2), 570);

        player = new BaseActor(-50, 0, uiStage);
        player.loadTexture("src/apalacios/pfg/assets/characters/pj1/pj1_stand_right.png");
        player.setPosition(240 - (player.getWidth() / 2), 365);
        player.setScale(1.5f);

        wing = new BaseActor(-50, 0, uiStage);
        wing.loadTexture("src/apalacios/pfg/assets/characters/pj1/aleta_1_right.png");
        wing.setPosition(240 - (wing.getWidth() / 2), 365);
        wing.setScale(1.5f);

        BaseActor colorSquareCPU1 = new BaseActor(-50, 0, uiStage);
        colorSquareCPU1.loadTexture("src/apalacios/pfg/assets/colors/color_cpu.png");
        colorSquareCPU1.setPosition(720 - (colorSquareCPU1.getWidth() / 2), 570);

        bodyCPU1 = new BaseActor(-50, 0, uiStage);
        bodyCPU1.loadTexture("src/apalacios/pfg/assets/characters/cpu/body.png");
        bodyCPU1.setPosition(720 - (bodyCPU1.getWidth() / 2), 365);
        bodyCPU1.setScale(1.5f);

        BaseActor colorSquareCPU2 = new BaseActor(-50, 0, uiStage);
        colorSquareCPU2.loadTexture("src/apalacios/pfg/assets/colors/color_cpu.png");
        colorSquareCPU2.setPosition(1200 - (colorSquareCPU2.getWidth() / 2), 570);

        bodyCPU2 = new BaseActor(-50, 0, uiStage);
        bodyCPU2.loadTexture("src/apalacios/pfg/assets/characters/cpu/body.png");
        bodyCPU2.setPosition(1200 - (bodyCPU2.getWidth() / 2), 365);
        bodyCPU2.setScale(1.5f);

        BaseActor colorSquareCPU3 = new BaseActor(-50, 0, uiStage);
        colorSquareCPU3.loadTexture("src/apalacios/pfg/assets/colors/color_cpu.png");
        colorSquareCPU3.setPosition(1680 - (colorSquareCPU3.getWidth() / 2), 570);

        bodyCPU3 = new BaseActor(-50, 0, uiStage);
        bodyCPU3.loadTexture("src/apalacios/pfg/assets/characters/cpu/body.png");
        bodyCPU3.setPosition(1680 - (bodyCPU3.getWidth() / 2), 365);
        bodyCPU3.setScale(1.5f);

        // Seleccionar skin
        selectedSkin = 0;
        skin = new BaseActor(-50, 0, uiStage);
        skin.loadTexture("src/apalacios/pfg/assets/characters/skins/sk0/right.png");
        skin.setPosition(240 - (skin.getWidth() / 2), 365);
        skin.setScale(1.5f);

        // Seleccionar rondas
        blackScreen = new BaseActor(0, 0, uiStage);
        blackScreen.loadTexture("src/apalacios/pfg/assets/background/blackScreen.png");
        blackScreen.addAction(Actions.fadeOut(0));

        seleccionaRondasLabel = new Label("NUMERO DE RONDAS", BaseGame.styleNormal);
        seleccionaRondasLabel.setPosition(960 - (seleccionaRondasLabel.getWidth() / 2), 700);
        uiStage.addActor(seleccionaRondasLabel);
        seleccionaRondasLabel.addAction(Actions.fadeOut(0));

        fiveLabel = new Label("5", BaseGame.styleNormal);
        fiveLabel.setPosition(960 - (fiveLabel.getWidth() / 2) - 100, 550);
        uiStage.addActor(fiveLabel);
        fiveLabel.addAction(Actions.fadeOut(0));

        tenLabel = new Label("10", BaseGame.styleNormal);
        tenLabel.setPosition(960 - (tenLabel.getWidth() / 2), 550);
        uiStage.addActor(tenLabel);
        tenLabel.addAction(Actions.fadeOut(0));

        fifteenLabel = new Label("15", BaseGame.styleNormal);
        fifteenLabel.setPosition(960 - (fifteenLabel.getWidth() / 2) + 100, 550);
        uiStage.addActor(fifteenLabel);
        fifteenLabel.addAction(Actions.fadeOut(0));

        // Flechas de selección
        selectArrowLeftLabel = new Label("<", BaseGame.styleNormal);
        selectArrowLeftLabel.setPosition(20, 800);
        uiStage.addActor(selectArrowLeftLabel);
        selectArrowLeftLabel.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (-10, 0, 0.5f), Actions.moveBy(10, 0, 0.5f))));

        selectArrowRightLabel = new Label(">", BaseGame.styleNormal);
        selectArrowRightLabel.setPosition(440, 800);
        uiStage.addActor(selectArrowRightLabel);
        selectArrowRightLabel.addAction(Actions.forever(Actions.sequence(Actions.moveBy
                (10, 0, 0.5f), Actions.moveBy(-10, 0, 0.5f))));

        // Suelo
        new Solid(0, 0, 1920, 100, mainStage);

        // Ventana carga
        loading = false;

        // Sonido
        navigateSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_navigate.ogg"));
        acceptSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/menu_accept.ogg"));

        new Thread(() -> StartMenuScreen.titleMusic.setOnCompletionListener(music -> {
            if (StartMenuScreen.currentSong == 13)
                StartMenuScreen.currentSong = 1;
            else
                StartMenuScreen.currentSong++;

            String songName = "04 Shell Shock Shake";
            switch (StartMenuScreen.currentSong) {
                case 1:
                    songName = "01 Introjuice";
                    break;
                case 2:
                    songName = "02 Failien Funk";
                    break;
                case 3:
                    songName = "03 Stroll 'n Roll";
                    break;
                case 4:
                    songName = "04 Shell Shock Shake";
                    break;
                case 5:
                    songName = "05 I'm a Fighter";
                    break;
                case 6:
                    songName = "06 Going Down Tune";
                    break;
                case 7:
                    songName = "07 Cloud Crash";
                    break;
                case 8:
                    songName = "08 Filaments and Voids";
                    break;
                case 9:
                    songName = "09 Bonus Rage";
                    break;
                case 10:
                    songName = "10 It's not My Ship";
                    break;
                case 11:
                    songName = "11 Perihelium";
                    break;
                case 12:
                    songName = "12 Shingle Tingle";
                    break;
                case 13:
                    songName = "13 Just a Minuet";
                    break;
            }

            StartMenuScreen.titleMusic = Gdx.audio.newMusic(Gdx.files.internal
                    ("src/apalacios/pfg/assets/music/" + songName + ".mp3"));
            StartMenuScreen.titleMusic.setLooping(false);
            StartMenuScreen.titleMusic.play();
            StartMenuScreen.titleMusic.setVolume(Config.musicVolume);
        })).start();
    }

    @Override
    public boolean povMoved(Controller controller, int i, PovDirection povDirection) {
        if (povDirection == PovDirection.east)
            updateMenuSelection(1, currentMenu != Menu.ROUNDS);
        else if (povDirection == PovDirection.west)
            updateMenuSelection(-1, currentMenu != Menu.ROUNDS);
        return false;
    }

    @Override
    public boolean axisMoved(Controller controller, int i, float v) {
        if (i == 3 && (v == 1 || v == -1))
            updateMenuSelection((int) v, currentMenu != Menu.ROUNDS);
        return false;
    }

    @Override
    public boolean buttonUp(Controller controller, int i) {
        if (i == 0)
            optionSelected(controller);
        else if (i == 1)
            goBack();
        return false;
    }

    private void updateMenuSelection(int value, boolean sound) {
        if (sound && currentMenu != Menu.START)
            navigateSound.play(Config.soundVolume);

        if (value == 1) {
            if (selectedOption + 1 <= maxMenuOption)
                selectedOption++;
            else
                selectedOption = 1;
        } else if (value == -1){
            if (selectedOption - 1 >= 1)
                selectedOption--;
            else
                selectedOption = maxMenuOption;
        }

        switch (currentMenu) {
            case PROFILE:
                selectedProfileId = selectedOption - 1;
                selectedProfileLabel.remove();
                selectedProfileLabel = new Label(profiles.get(selectedOption - 1).getName(), BaseGame.styleNormal);
                selectedProfileLabel.setPosition(240 - (selectedProfileLabel.getWidth() / 2), 800);
                uiStage.addActor(selectedProfileLabel);
                break;

            case COLOR:
                selectedColor = selectedOption;
                colorSquare.setAnimation(colorSquare.loadTexture("src/apalacios/pfg/assets/colors/color_" +
                        selectedColor + ".png"));
                player.setAnimation(player.loadTexture("src/apalacios/pfg/assets/characters/pj" +
                        selectedColor + "/pj" + selectedColor + "_stand_right.png"));
                wing.setAnimation(wing.loadTexture("src/apalacios/pfg/assets/characters/pj" + selectedColor +
                        "/aleta_" + selectedColor + "_right.png"));
                break;

            case SKIN:
                selectedSkin = selectedOption - 1;
                skin.setAnimation(skin.loadTexture("src/apalacios/pfg/assets/characters/skins/sk" +
                        selectedSkin + "/right.png"));
                break;

            case ROUNDS:
                navigateSound.play(Config.soundVolume);
                fiveLabel.setColor(Color.WHITE);
                tenLabel.setColor(Color.WHITE);
                fifteenLabel.setColor(Color.WHITE);

                switch (selectedOption) {
                    case 1:
                        GameScore.totalRounds = 5;
                        fiveLabel.setColor(Color.RED);
                        break;
                    case 2:
                        GameScore.totalRounds = 10;
                        tenLabel.setColor(Color.RED);
                        break;
                    case 3:
                        GameScore.totalRounds = 15;
                        fifteenLabel.setColor(Color.RED);
                        break;
                }
                break;
        }
    }

    private void optionSelected(Controller controller) {
        if (currentMenu != Menu.START)
            acceptSound.play(Config.soundVolume);

        switch (currentMenu) {
            case PROFILE:
                currentMenu = Menu.COLOR;
                selectedOption = 1;
                maxMenuOption = 8;
                selectArrowLeftLabel.setPosition(selectArrowLeftLabel.getX() + 150,
                        570 + (colorSquare.getHeight() / 2) - (selectArrowLeftLabel.getHeight() / 2));
                selectArrowRightLabel.setPosition(selectArrowRightLabel.getX() - 150,
                        570 + (colorSquare.getHeight() / 2) - (selectArrowRightLabel.getHeight() / 2));
                break;

            case COLOR:
                currentMenu = Menu.SKIN;
                selectedOption = 1;
                maxMenuOption = 15;
                selectArrowLeftLabel.setPosition(selectArrowLeftLabel.getX(),
                        370 + (player.getHeight() / 2) - (selectArrowLeftLabel.getHeight() / 2));
                selectArrowRightLabel.setPosition(selectArrowRightLabel.getX(),
                        370 + (player.getHeight() / 2) - (selectArrowRightLabel.getHeight() / 2));
                break;

            case SKIN:
                currentMenu = Menu.ROUNDS;
                selectedOption = 2;
                maxMenuOption = 3;
                selectArrowLeftLabel.setPosition(fiveLabel.getX() - 30, fiveLabel.getY());
                selectArrowRightLabel.setPosition(fifteenLabel.getX() + fifteenLabel.getWidth() +
                        30, fiveLabel.getY());

                blackScreen.addAction(Actions.fadeIn(0.2f));
                seleccionaRondasLabel.addAction(Actions.fadeIn(0.2f));
                fiveLabel.addAction(Actions.fadeIn(0.2f));
                tenLabel.addAction(Actions.fadeIn(0.2f));
                fifteenLabel.addAction(Actions.fadeIn(0.2f));

                tenLabel.setColor(Color.RED);
                break;

            case ROUNDS:
                currentMenu = Menu.START;
                selectArrowLeftLabel.remove();
                selectArrowRightLabel.remove();

                player.remove();
                wing.remove();
                skin.remove();

                colorsInUse = new ArrayList<>();
                skinsInUse = new ArrayList<>();

                p1 = new Player(240 - (player.getWidth() / 2), 365, mainStage, selectedColor, selectedSkin, false);
                p1.setController(controller);
                for (ControllerProfile cp : profiles.get(selectedProfileId).getControllerProfiles()) {
                    if (cp.getControllerType().equals(controller.getName()))
                        p1.setControllerProfile(cp);
                }
                if (p1.getControllerProfile() == null)
                    p1.setControllerProfile(createDefaultControllerProfile());
                p1.setPlayerName(profiles.get(selectedProfileId).getName(), uiStage);
                p1.setScale(1.5f);
                p1.setMaxHorizontalSpeed(500);

                colorsInUse.add(selectedColor);
                skinsInUse.add(selectedSkin);

                bodyCPU1.remove();
                cpu1 = new Player(720 - (player.getWidth() / 2), 365, mainStage,
                        randomColor(), randomSkin(), true);
                cpu1.setPlayerName("CPU 1", uiStage);
                cpu1.setScale(1.5f);
                cpu1.setMaxHorizontalSpeed(500);

                bodyCPU2.remove();
                cpu2 = new Player(1200 - (player.getWidth() / 2), 365, mainStage,
                        randomColor(), randomSkin(), true);
                cpu2.setPlayerName("CPU 2", uiStage);
                cpu2.setScale(1.5f);
                cpu2.setMaxHorizontalSpeed(500);

                bodyCPU3.remove();
                cpu3 = new Player(1680 - (player.getWidth() / 2), 365, mainStage,
                        randomColor(), randomSkin(), true);
                cpu3.setPlayerName("CPU 3", uiStage);
                cpu3.setScale(1.5f);
                cpu3.setMaxHorizontalSpeed(500);

                // Animaciones
                blackScreen.addAction(Actions.fadeOut(0.2f));
                seleccionaRondasLabel.addAction(Actions.fadeOut(0.2f));
                fiveLabel.addAction(Actions.fadeOut(0.2f));
                tenLabel.addAction(Actions.fadeOut(0.2f));
                fifteenLabel.addAction(Actions.fadeOut(0.2f));

                loadingScreen = new BaseActor(1920, 1080, uiStage);
                loadingScreen.addAction(Actions.fadeOut(0));

                p1.moveRight();
                cpu1.moveRight();
                cpu2.moveRight();
                cpu3.moveRight();

                if (GameScore.totalRounds == 0)
                    GameScore.totalRounds = 10;
                GameScore.currentRound = 1;
                GameScore.lastLevel = 0;
                GameScore.nPlayers = 4;

                GameScore.scoreP1 = 0;
                GameScore.scoreP2 = 0;
                GameScore.scoreP3 = 0;
                GameScore.scoreP4 = 0;

                gameStartTime = System.currentTimeMillis();
                gameStarting = true;
                break;
        }
    }

    private int randomColor() {
        boolean uniqueColor;
        int rand;

        do {
            uniqueColor = true;
            rand = new Random().nextInt(8) + 1;

            for (int c : colorsInUse) {
                if (c == rand) {
                    uniqueColor = false;
                    break;
                }
            }
        } while (!uniqueColor);

        colorsInUse.add(rand);

        return rand;
    }

    private int randomSkin() {
        boolean uniqueSkin;
        int rand;

        do {
            uniqueSkin = true;
            rand = new Random().nextInt(14) + 1;

            for (int c : skinsInUse) {
                if (c == rand) {
                    uniqueSkin = false;
                    break;
                }
            }
        } while (!uniqueSkin);

        skinsInUse.add(rand);

        return rand;
    }

    private ControllerProfile createDefaultControllerProfile() {
        ControllerProfile cp = new ControllerProfile();
        cp.setJoystickX(1);
        cp.setJoystickY(0);
        cp.setButtonJump(1);
        cp.setButtonShoot(2);
        cp.setButtonGrab(0);
        cp.setButtonThrow(3);
        cp.setButtonStart(7);
        return cp;
    }

    private void goBack() {
        switch (currentMenu) {
            case PROFILE:
                Util.lastScreen = LastScreen.ONEPLAYER;
                PFGGame.setActiveScreen(new StartMenuScreen());
                break;

            case COLOR:
                currentMenu = Menu.PROFILE;
                selectedOption = selectedProfileId + 1;
                maxMenuOption = profiles.size();
                selectArrowLeftLabel.setPosition(20, 800);
                selectArrowRightLabel.setPosition(440, 800);
                break;

            case SKIN:
                currentMenu = Menu.COLOR;
                selectedOption = selectedColor;
                maxMenuOption = 8;
                selectArrowLeftLabel.setPosition(selectArrowLeftLabel.getX(),
                        570 + (colorSquare.getHeight() / 2) - (selectArrowLeftLabel.getHeight() / 2));
                selectArrowRightLabel.setPosition(selectArrowRightLabel.getX(),
                        570 + (colorSquare.getHeight() / 2) - (selectArrowRightLabel.getHeight() / 2));
                break;

            case ROUNDS:
                currentMenu = Menu.SKIN;
                selectedOption = selectedSkin;
                maxMenuOption = 15;
                selectArrowLeftLabel.setPosition(170, 370 + (colorSquare.getHeight() / 2) -
                        (selectArrowLeftLabel.getHeight() / 2));
                selectArrowRightLabel.setPosition(290, 370 + (colorSquare.getHeight() / 2) -
                        (selectArrowRightLabel.getHeight() / 2));

                blackScreen.addAction(Actions.fadeOut(0.2f));
                seleccionaRondasLabel.addAction(Actions.fadeOut(0.2f));
                fiveLabel.addAction(Actions.fadeOut(0.2f));
                tenLabel.addAction(Actions.fadeOut(0.2f));
                fifteenLabel.addAction(Actions.fadeOut(0.2f));

                fiveLabel.setColor(Color.WHITE);
                tenLabel.setColor(Color.WHITE);
                fifteenLabel.setColor(Color.WHITE);
                break;
        }
    }

    @Override
    public void update(float dt) {
        if (gameStarting){

            // COLISION JUGADORES SUELO
            for (BaseActor solidActor : BaseActor.getList(mainStage, Solid.class.getName())) {
                for (BaseActor mobileActor : BaseActor.getList(mainStage, Player.class.getName())) {
                    Player playerActor = (Player) mobileActor;
                    if (playerActor.isAlive()) {
                        if (playerActor.getY() <= solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) &&
                                    playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setPosition(playerActor.getX(),
                                        solidActor.getY() + solidActor.getHeight() + 2);
                            }
                        }

                        if (playerActor.getY() > solidActor.getY() + solidActor.getHeight()) {
                            if (playerActor.getFeetSensorL().overlaps(solidActor) ||
                                    playerActor.getFeetSensorR().overlaps(solidActor)) {
                                playerActor.setGravityEnabled(false);
                                playerActor.setOnGround(true);
                            }
                        } else {
                            playerActor.preventOverlap(solidActor);
                        }
                    }
                }
            }

            long gameCurrentTime = System.currentTimeMillis();
            if (gameStartTime - gameCurrentTime < -3000 && !loading) {
                loading = true;
                loadingScreen.setPosition(0, 0);
                loadingScreen.setAnimation(
                        loadingScreen.loadTexture("src/apalacios/pfg/assets/background/loadingScreen.png"));
                loadingScreen.addAction(Actions.fadeIn(1f));
            }

            if (gameStartTime - gameCurrentTime < -5000) {
                PFGGame.setActiveScreen(new LevelScreen(p1, cpu1, cpu2, cpu3, 1)); // nLevel -> Random
            }
        }
    }

    @Override
    public void resize(int arg0, int arg1) {
        viewPort.update(arg0, arg1);
        camera.viewportWidth = arg0;
        camera.viewportHeight = arg1;
    }
}

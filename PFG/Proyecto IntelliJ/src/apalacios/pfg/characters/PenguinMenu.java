package apalacios.pfg.characters;

import apalacios.pfg.util.Direction;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.Random;

public class PenguinMenu extends BaseActor {

    private Animation rightStand;
    private Animation leftStand;
    private Animation rightWalk;
    private Animation leftWalk;
    private Animation rightKick;
    private Animation leftKick;

    private BaseActor wing;
    private Skin skin;
    private int nTexture;

    private boolean isWalking;
    private boolean isKicking;
    private boolean isFalling;

    private Direction direction;

    // Contadores de tiempo
    private long actStartTime = System.currentTimeMillis();
    private long kickStartTime = System.currentTimeMillis();

    // Físicas
    private float walkAcceleration;
    private float walkDeceleration;
    private float maxHorizontalSpeed;
    private float gravity;
    private float maxVerticalSpeed;

    public BaseActor collisionBox;

    /**
     * Constructor de la calse, inicializar los parámetros de la misma
     *
     * @param posX  Representa su posición respecto a la pantalla en el eje horizontal
     * @param posY  Representa su posición respecto a la pantalla en el eje vertical
     * @param stage El actor (independientemente de lo que sea), sera añadido al stage,
     */
    public PenguinMenu(float posX, float posY, Stage stage) {
        super(posX, posY, stage);

        wing = new BaseActor(getX(), getY(), getStage());
        loadRandomTexture();

        collisionBox = new BaseActor(getX(), getY(), getStage());
        collisionBox.loadTexture("src/apalacios/pfg/assets/characters/other/pj_colision_box.png");

        isWalking = false;
        isKicking = false;
        isFalling = false;
        direction = Direction.LEFT;

        maxHorizontalSpeed = 300;
        walkAcceleration = 600;
        walkDeceleration = 600;
        gravity = 1000;
        maxVerticalSpeed = 1000;

        skin = new Skin(getX(), getY(), getStage(), new Random().nextInt(14) + 1);

        setBoundaryRectangle();
    }

    public Skin getSkin() {
        return skin;
    }

    private void loadRandomTexture() {
        // Animaciones
        nTexture = new Random().nextInt(8) + 1;
        rightStand = loadTexture("src/apalacios/pfg/assets/characters/pj" + nTexture + "/pj" + nTexture
                + "_stand_right.png");
        leftStand = loadTexture("src/apalacios/pfg/assets/characters/pj" + nTexture + "/pj" + nTexture
                + "_stand_left.png");
        rightWalk = loadAnimationFromSheet("src/apalacios/pfg/assets/characters/pj" + nTexture + "/pj"
                        + nTexture + "_walk_right.png",
                1, 8, 0.1f, true);
        leftWalk = loadAnimationFromSheet("src/apalacios/pfg/assets/characters/pj" + nTexture + "/pj"
                        + nTexture + "_walk_left.png",
                1, 8, 0.1f, true);
        rightKick = loadTexture("src/apalacios/pfg/assets/characters/pj" + nTexture + "/pj" + nTexture
                + "_kick_right.png");
        leftKick = loadTexture("src/apalacios/pfg/assets/characters/pj" + nTexture + "/pj" + nTexture
                + "_kick_left.png");
        wing.setAnimation(loadTexture("src/apalacios/pfg/assets/characters/pj" + nTexture + "/aleta_"
                + nTexture + "_left.png"));
        setAnimation(leftStand);
    }

    @Override
    public void act(float dt) {
        super.act(dt);

        long currentTime = System.currentTimeMillis();
        if (isKicking) {
            if (kickStartTime - currentTime < -500) {
                kickStartTime = currentTime;
                isKicking = false;
                if (direction == Direction.RIGHT) {
                    setAnimation(rightStand);
                } else {
                    setAnimation(leftStand);
                }
            }
        }

        if (!isKicking) {
            if (actStartTime - currentTime < -(new Random().nextInt(1000) + 2000)) {
                actStartTime = currentTime;
                if ((new Random().nextInt(100) + 1) <= 66) {
                    if (new Random().nextInt(100) + 1 <= 50) {
                        setAnimation(rightWalk);
                        wing.setAnimation(loadTexture("src/apalacios/pfg/assets/characters/pj" + nTexture + "/aleta_"
                                + nTexture + "_right.png"));
                        direction = Direction.RIGHT;
                        isWalking = true;
                    } else {
                        setAnimation(leftWalk);
                        wing.setAnimation(loadTexture("src/apalacios/pfg/assets/characters/pj" + nTexture + "/aleta_"
                                + nTexture + "_left.png"));
                        direction = Direction.LEFT;
                        isWalking = true;
                    }
                } else {
                    if (direction == Direction.RIGHT) {
                        setAnimation(rightStand);
                    } else {
                        setAnimation(leftStand);
                    }
                    isWalking = false;
                }
            }
        }

        // Aceleración y deceleración
        if (isWalking) {
            if (direction == Direction.RIGHT) {
                accelerationVec.add(walkAcceleration, 0);
            } else{
                accelerationVec.add(-walkAcceleration, 0);
            }
        } else {
            float decelerationAmount = walkDeceleration * dt;
            float walkSpeed = Math.abs(velocityVec.x);

            walkSpeed -= decelerationAmount;
            if (walkSpeed < 0)
                walkSpeed = 0;

            if (direction == Direction.RIGHT) {
                velocityVec.x = walkSpeed;
            } else {
                velocityVec.x = -walkSpeed;
            }
        }

        velocityVec.x = MathUtils.clamp(velocityVec.x, -maxHorizontalSpeed, maxHorizontalSpeed);
        velocityVec.y = MathUtils.clamp(velocityVec.y, -maxVerticalSpeed, maxVerticalSpeed);

        // Ajustar caja de colisión
        if (direction == Direction.RIGHT) {
            collisionBox.centerAtPosition(getX() + 35, getY() + 42);
        } else {
            collisionBox.centerAtPosition(getX() + 30, getY() + 42);
        }

        // Mantener aleta sobre el pingüino
        wing.setPosition(getX(), getY());

        // Simular gravedad
        accelerationVec.add(0, -gravity);
        velocityVec.add(accelerationVec.x * dt, accelerationVec.y * dt);

        moveBy(Math.round(velocityVec.x * dt), Math.round(velocityVec.y * dt));
        accelerationVec.set(0, 0);

        boundToWorld();

        skin.setDirection(direction);
        skin.centerAtActor(this);
    }

    public void kick(PenguinMenu pm) {
        if (!isKicking && !isFalling) {
            isKicking = true;
            isWalking = false;

            kickStartTime = System.currentTimeMillis();
            if (direction == Direction.RIGHT && getX() <= pm.getX()) {
                setAnimation(rightKick);
                pm.accelerationVec.set(walkAcceleration * 100, walkAcceleration * 100);
                pm.isKicking = true;
            } else if (direction == Direction.LEFT && getX() >= pm.getX()){
                setAnimation(leftKick);
                pm.accelerationVec.set(-walkAcceleration * 100, walkAcceleration * 100);
                pm.isKicking = true;
            }
        }
    }
}

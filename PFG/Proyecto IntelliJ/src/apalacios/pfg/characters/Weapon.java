package apalacios.pfg.characters;

import apalacios.pfg.util.Direction;
import apalacios.pfg.weapons.Weapon9;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Stage;

public abstract class Weapon extends BaseActor {

    // Munición
    public int nMunition;
    public BaseActor munitionType;

    // Otras propiedades
    private int nWeapon;
    public int shootSpeed; // Tiempo entre disparos en milisegundos (1000 = una bala por segundo)

    // Visual
    protected Direction direction;
    private Animation right;
    private Animation left;
    public Sound shootSound;
    public static Sound noAmmoSound = Gdx.audio.newSound(Gdx.files.internal("src/apalacios/pfg/assets/sound/sinMunicion.ogg"));;

    public Weapon(float posX, float posY, Stage stage, int nWeapon, int nMunition) {
        super(posX, posY, stage);

        this.nWeapon = nWeapon;
        this.nMunition = nMunition;

        direction = Direction.RIGHT;
        right = loadTexture("src/apalacios/pfg/assets/weapons/ar" + nWeapon + "/right.png");
        left = loadTexture("src/apalacios/pfg/assets/weapons/ar" + nWeapon + "/left.png");
        setAnimation(right);
    }

    public int getnWeapon() {
        return nWeapon;
    }

    public abstract void shoot();

    public abstract void stopShooting();

    public abstract void reload();

    public void setnMunition(int nMunition) {
        this.nMunition = nMunition;
    }

    void setDirection(Direction direction) {
        this.direction = direction;
        if (direction == Direction.RIGHT)
            setAnimation(right);
        else
            setAnimation(left);
    }

    public void drop() {
        WeaponItem weaponItem = new WeaponItem(getX(), getY(), getStage(), nWeapon, nMunition);
        if (direction == Direction.RIGHT)
            weaponItem.setPosition(getX() + 60, getY() + 50);
        else
            weaponItem.setPosition(getX() - 20, getY() + 50);
        weaponItem.drop(direction);
    }
}

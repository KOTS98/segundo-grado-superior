package apalacios.pfg.game;

import apalacios.pfg.screens.BaseScreen;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class BaseGame extends Game {

    private static BaseGame game;
    public static LabelStyle styleNormal;
    public static LabelStyle styleBig;
    public static Skin uiSkin;

    public BaseGame() {
        game = this;
    }

    public static void setActiveScreen(BaseScreen screen) {
        game.setScreen(screen);
    }

    @Override
    public void create() {
        styleNormal = new LabelStyle();
        styleBig = new LabelStyle();

        uiSkin = new Skin(Gdx.files.internal("src/apalacios/pfg/assets/uiskin/pixthulhu-ui.json"));

        FreeTypeFontGenerator fontGenerator
                = new FreeTypeFontGenerator(Gdx.files.internal("src/apalacios/pfg/assets/fonts/slkscrb.ttf"));

        FreeTypeFontParameter fontParameters = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontParameters.size = 25;
        fontParameters.color = Color.WHITE;
        fontParameters.borderWidth = 2;
        fontParameters.borderColor = Color.BLACK;
        fontParameters.borderStraight = true;
        fontParameters.minFilter = Texture.TextureFilter.Linear;
        fontParameters.magFilter = Texture.TextureFilter.Linear;

        BitmapFont fontNormal = fontGenerator.generateFont(fontParameters);
        styleNormal.font = fontNormal;

        fontParameters.size = 50;
        fontNormal = fontGenerator.generateFont(fontParameters);
        styleBig.font = fontNormal;
    }
}

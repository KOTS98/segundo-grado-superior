package base;

/**
 * Representa las estadísticas de consumo de un producto
 * 
 * @author Adrián Palacios
 */
public class ProductoStats {
	private String nombre;
	private float precio;
	private int ventas;

	/**
	 * Constructor de la clase, inicializa sus parámetros
	 * 
	 * @param nombre Representa el nombre del producto
	 * @param precio Representa el precio o valor de venta del producto
	 * @param ventas Representa la cantidad de unidades vendidas del producto
	 */
	public ProductoStats(String nombre, float precio, int ventas) {
		this.nombre = nombre;
		this.precio = precio;
		this.ventas = ventas;
	}

	/**
	 * Devuelve el nombre del producto
	 * 
	 * @return String que representa el nombre del producto
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Establece el nombre del producto al recibido como parámetro
	 * 
	 * @param nombre Representa el nombre deseado para el producto
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Devuelve el precio del producto
	 * 
	 * @return float que representa el precio del producto
	 */
	public float getPrecio() {
		return precio;
	}

	/**
	 * Establece el precio del producto al recibido como parámetro
	 * 
	 * @param precio Representa el precio del producto
	 */
	public void setPrecio(float precio) {
		this.precio = precio;
	}

	/**
	 * Devuelve la cantidad de unidades vendidas del producto
	 * 
	 * @return int que representa la cantidad de unidades vendidas del prducto
	 */
	public int getVentas() {
		return ventas;
	}

	/**
	 * Establece la cantidad de unidades vendidas del producto a la recibida como
	 * parámetro
	 * 
	 * @param ventas Representa la catnidad de unidades vendidas deseada para el
	 *               producto
	 */
	public void setVentas(int ventas) {
		this.ventas = ventas;
	}
}

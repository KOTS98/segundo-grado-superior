package com.apalacios.seccion31.app;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.text.View;

import base.Oferta;

/**
 * Clase encargada del proceso de renderizaci�n de objetos de tipo "Oferta" en
 * las listas de ofertas
 * 
 * @author Adri�n Palacios
 *
 */
@SuppressWarnings("serial")
class OfertaRenderer extends JPanel implements ListCellRenderer<Object> {

	/**
	 * Genera el componente de lista renderizada con los elementos de la clase
	 * oferta especificados
	 * 
	 * @param list         Representa la lista donde se cargar�n los objetos
	 * @param valores      Representa el objeto individual en la lista
	 * @param index        Representa el �dice del objeto
	 * @param isSelected   Determina si el objeto especificado est� o no
	 *                     seleccionado
	 * @param cellHasFocus Determina si la lista tiene el "foco" (si recibira o no
	 *                     las entradas del teclado / rat�n)
	 * @return Component que representa la lista de ofertas ya renderizada
	 */
	@Override
	public Component getListCellRendererComponent(JList<?> list, Object valores, int index, boolean isSelected,
			boolean cellHasFocus) {

		// Eliminar todos los elementos
		this.removeAll();
		// Establecer el Layout, y crear el panel
		this.setLayout(new BorderLayout());
		JPanel informacion = new JPanel();
		informacion.setBackground(Constantes.PANTALLA_CLR);
		informacion.setLayout(new BoxLayout(informacion, BoxLayout.Y_AXIS));

		// Comprobar si hay un elemento seleccionado
		if (isSelected) {
			// Establecer color de fondo para el elemento seleccionado
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
			informacion.setBackground(list.getBackground());
			informacion.setForeground(list.getForeground());

		} else {
			// Establecer color de fondo para el elemento no seleccionado
			setBackground(list.getBackground());
			setForeground(list.getForeground());
			informacion.setBackground(list.getBackground());
			informacion.setForeground(list.getForeground());
		}

		// Crear el prototipo de oferta y dotarlo de sus distintivos gr�ficos
		Oferta oferta = (Oferta) valores;
		ImageIcon icon = new ImageIcon(View.class.getResource(oferta.getImagen()));
		JLabel imagen = new JLabel(icon);
		JLabel titulo = new JLabel(oferta.getTitulo());
		titulo.setFont(Constantes.BTN_FNT);
		titulo.setForeground(Constantes.TEXTO_CLR);

		// A�adir la imagen al panel principal
		this.add(imagen, BorderLayout.WEST);

		// A�adir el resto de campos al panel de informaci�n
		informacion.add(titulo);

		// A�adir el panel de informaci�n al panel principal y establecer un borde
		this.add(informacion, BorderLayout.CENTER);
		this.setBorder(Constantes.STNDR_BRD);
		return this;
	}
}

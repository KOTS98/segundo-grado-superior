package com.apalacios.seccion31.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Objeto de interfaz personalizado. Se comporta como un elemento JButton
 * convencional, con la diferencia de que al pulsarlo, existe un 20% de
 * posibilidades de que se despliegue un di�logo indicando que no es necesario
 * pagar la cuenta
 * 
 * @author Adri�n Palacios
 */
@SuppressWarnings("serial")
public class BtnPagar extends JButton {

	/**
	 * Constructor de la clase
	 * 
	 * @param nombre Representa el nombre deseado para el bot�n (El nombre que se
	 *               mostrar� en la interfaz gr�fica)
	 */
	public BtnPagar(String nombre) {
		setText(nombre);
		addActionListener(new ActionListener() {

			/**
			 * Se ejecuta cu�ndo al pulsar el bot�n, genera un n�mero aleatorio de entre 1 y
			 * 100. Si el n�mero est� comprendido entre 1 y 20, indicar� al usuario que no
			 * deber� pagar la cuenta
			 */
			@Override
			public void actionPerformed(ActionEvent e) {
				Random r = new Random();
				int intAleatorio = r.nextInt(100) + 1;
				if (intAleatorio < 20) {
					JDialog descuento = new JDialog();
					descuento.setTitle("�FELICICADES!");

					JPanel panelDescuento = new JPanel();
					panelDescuento.setLayout(new BorderLayout());
					panelDescuento.setBackground(Constantes.PANTALLA_CLR);

					JLabel titulo = new JLabel("�FELICIDADES!");
					titulo.setForeground(Constantes.TEXTO_CLR);
					titulo.setFont(Constantes.BTN_FNT);
					panelDescuento.add(titulo, BorderLayout.NORTH);

					JLabel mensaje = new JLabel("HOY NO PAGAS LA CUENTA");
					mensaje.setForeground(Constantes.TEXTO_CLR);
					mensaje.setFont(Constantes.BTN_FNT);
					panelDescuento.add(mensaje, BorderLayout.CENTER);

					descuento.setModal(true);
					descuento.setContentPane(panelDescuento);
					setSize(new Dimension(300, 300));
					descuento.setResizable(false);
					descuento.setLocationRelativeTo(null);
					descuento.pack();
					descuento.setVisible(true);
				}
			}
		});
	}
}

package com.apalacios.seccion31.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.View;

import base.Oferta;

@SuppressWarnings("serial")
public class OfertasAdminDialog extends JFrame implements ActionListener, ListSelectionListener {
	// private Properties conf = new Properties();
	private JDialog dialog;
	private OfertaRenderer or;
	private JList<Oferta> listaOfertas;
	private DefaultListModel<Oferta> dlmOfertas;
	private JTextField txtNombreOferta;
	private JTextArea txtDescOferta;
	private JButton btnImagen1;
	private JButton btnImagen2;
	private JButton btnImagen3;
	private int btnSeleccionado = 1;
	private JButton btnAddOferta;
	private JButton btnModificarOferta;
	private JButton btnEliminarOferta;
	private Border UNSELECT_BRD = BorderFactory.createLineBorder(Constantes.PANTALLA_CLR, 5);

	public OfertasAdminDialog(DefaultListModel<Oferta> dlmOfertas) {
		or = new OfertaRenderer();
		this.dlmOfertas = dlmOfertas;
		setBackground(Constantes.FONDO_CLR);
		dialog = new JDialog(this, "Vista de administrador", true);
		init();
		addActionListeners(this);
		addListListener(this);
		// cargarPreferencias();
		dialog.setSize(new Dimension(1200, 750));
		dialog.setLocationRelativeTo(null);
		dialog.setResizable(false);
		dialog.setVisible(true);
	}

	private void init() {
		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new BorderLayout());
		panelPrincipal.setBackground(Constantes.PANTALLA_CLR);

		// Panel Izquierdo
		JPanel panelIzquierdo = new JPanel();
		panelIzquierdo.setLayout(new GridBagLayout());
		panelIzquierdo.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pIConstraints = new GridBagConstraints();

		// Titulo lista ofertas
		JLabel lblListaOfertas = new JLabel("Lista de ofertas");
		lblListaOfertas.setSize(new Dimension(50, 540));
		lblListaOfertas.setFont(Constantes.BTN_FNT);
		lblListaOfertas.setForeground(Constantes.TEXTO_CLR);
		pIConstraints.gridx = 0;
		pIConstraints.gridy = 0;
		panelIzquierdo.add(lblListaOfertas, pIConstraints);

		// Lista de ofertas
		listaOfertas = new JList<Oferta>();
		listaOfertas.setModel(dlmOfertas);
		JScrollPane scroll = new JScrollPane(listaOfertas, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setPreferredSize(new Dimension(500, 670));
		listaOfertas.setBackground(Constantes.CONTENEDOR_CLR);
		listaOfertas.setBorder(Constantes.STNDR_BRD);
		listaOfertas.setCellRenderer(or);
		pIConstraints.gridy = 1;

		panelIzquierdo.add(scroll, pIConstraints);

		panelPrincipal.add(panelIzquierdo, BorderLayout.WEST);

		// Panel derecho
		JPanel panelDerecho = new JPanel();
		panelDerecho.setLayout(new GridBagLayout());
		panelDerecho.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pDConstraints = new GridBagConstraints();

		// Datos de la oferta
		// Etiqueta nombre oferta
		JLabel lblNombreOferta = new JLabel("Nombre");
		lblNombreOferta.setFont(new Font("Impact", 0, 20));
		lblNombreOferta.setForeground(Constantes.TEXTO_CLR);
		pDConstraints.gridx = 0;
		pDConstraints.gridy = 0;
		pDConstraints.insets = new Insets(-350, 0, 0, 500);
		panelDerecho.add(lblNombreOferta, pDConstraints);

		// Campo de texto para el nombre de la oferta
		txtNombreOferta = new JTextField();
		txtNombreOferta.setForeground(Constantes.TEXTO_CLR);
		txtNombreOferta.setFont(new Font("Impact", 0, 15));
		txtNombreOferta.setPreferredSize(new Dimension(400, 30));
		txtNombreOferta.setBorder(Constantes.STNDR_BRD);
		pDConstraints.gridy = 1;
		pDConstraints.insets = new Insets(-290, 0, 0, 170);
		panelDerecho.add(txtNombreOferta, pDConstraints);

		// Etiqeuta descripcion oferta
		JLabel lblDescOferta = new JLabel("Descripci�n");
		lblDescOferta.setFont(new Font("Impact", 0, 20));
		lblDescOferta.setForeground(Constantes.TEXTO_CLR);
		pDConstraints.gridy = 2;
		pDConstraints.insets = new Insets(-180, 0, 0, 470);
		panelDerecho.add(lblDescOferta, pDConstraints);

		// Campo de texto para la descripcion de la oferta
		txtDescOferta = new JTextArea();
		txtDescOferta.setForeground(Constantes.TEXTO_CLR);
		txtDescOferta.setFont(new Font("Impact", 0, 15));
		txtDescOferta.setPreferredSize(new Dimension(400, 250));
		txtDescOferta.setBorder(Constantes.STNDR_BRD);
		txtDescOferta.setLineWrap(true);
		txtDescOferta.setWrapStyleWord(true);
		pDConstraints.gridy = 3;
		pDConstraints.insets = new Insets(-75, 0, 0, 170);
		panelDerecho.add(txtDescOferta, pDConstraints);

		// Etiqueta de seleccion de imagen de oferta
		JLabel lblSelectImagen = new JLabel("Imagen");
		lblSelectImagen.setFont(new Font("Impact", 0, 20));
		lblSelectImagen.setForeground(Constantes.TEXTO_CLR);
		pDConstraints.gridy = 4;
		pDConstraints.insets = new Insets(0, 0, -80, 505);
		panelDerecho.add(lblSelectImagen, pDConstraints);

		// Panel de seleccion de imagen
		JPanel panelSelectImagen = new JPanel();
		panelSelectImagen.setLayout(new GridBagLayout());
		panelSelectImagen.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pSIConstraints = new GridBagConstraints();
		pDConstraints.gridy = 5;
		pDConstraints.insets = new Insets(0, 0, -220, 175);

		// Imagen 1
		btnImagen1 = new JButton(new ImageIcon(View.class.getResource("/imagenes/emblema_tactico.png")));
		btnImagen1.setBorder(Constantes.SELECT_BRD);
		btnImagen1.setBackground(Constantes.PANTALLA_CLR);
		btnImagen1.setActionCommand("btnImagen1");
		pSIConstraints.gridx = 0;
		pSIConstraints.gridy = 0;
		pSIConstraints.insets = new Insets(0, 0, 0, 35);
		panelSelectImagen.add(btnImagen1, pSIConstraints);

		// Imagen 2
		btnImagen2 = new JButton(new ImageIcon(View.class.getResource("/imagenes/emblema_ingenieria.png")));
		btnImagen2.setBorder(UNSELECT_BRD);
		btnImagen2.setBackground(Constantes.PANTALLA_CLR);
		btnImagen2.setActionCommand("btnImagen2");
		pSIConstraints.gridx = 1;
		panelSelectImagen.add(btnImagen2, pSIConstraints);

		// Imagen 3
		btnImagen3 = new JButton(new ImageIcon(View.class.getResource("/imagenes/emblema_ciencias.png")));
		btnImagen3.setBorder(UNSELECT_BRD);
		btnImagen3.setBackground(Constantes.PANTALLA_CLR);
		btnImagen3.setActionCommand("btnImagen3");
		pSIConstraints.gridx = 2;
		pSIConstraints.insets = new Insets(0, 0, 0, 0);
		panelSelectImagen.add(btnImagen3, pSIConstraints);

		panelDerecho.add(panelSelectImagen, pDConstraints);

		// Panel para los botones, a�adir, modificar y eliminar
		JPanel panelFunciones = new JPanel();
		panelFunciones.setLayout(new GridBagLayout());
		panelFunciones.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pFConstraints = new GridBagConstraints();
		pDConstraints.gridy = 6;
		pDConstraints.insets = new Insets(0, 0, -440, 170);

		// Boton a�adir oferta
		btnAddOferta = new JButton(" A�adir ");
		btnAddOferta.setFont(Constantes.BTN_FNT);
		btnAddOferta.setForeground(Constantes.TEXTO_CLR);
		btnAddOferta.setBorder(Constantes.STNDR_BRD);
		btnAddOferta.setBackground(Constantes.CONTENEDOR_CLR);
		btnAddOferta.setActionCommand("btnAddOferta");
		pFConstraints.gridx = 0;
		pFConstraints.gridy = 0;
		pFConstraints.insets = new Insets(0, 0, 0, 35);
		panelFunciones.add(btnAddOferta, pFConstraints);

		// Boton modificar oferta
		btnModificarOferta = new JButton(" Modificar ");
		btnModificarOferta.setFont(Constantes.BTN_FNT);
		btnModificarOferta.setForeground(Constantes.TEXTO_CLR);
		btnModificarOferta.setBorder(Constantes.STNDR_BRD);
		btnModificarOferta.setBackground(Constantes.CONTENEDOR_CLR);
		btnModificarOferta.setActionCommand("btnModificarOferta");
		pFConstraints.gridx = 1;
		panelFunciones.add(btnModificarOferta, pFConstraints);

		// Boton eliminar oferta
		btnEliminarOferta = new JButton(" Eliminar ");
		btnEliminarOferta.setFont(Constantes.BTN_FNT);
		btnEliminarOferta.setForeground(Constantes.TEXTO_CLR);
		btnEliminarOferta.setBorder(Constantes.STNDR_BRD);
		btnEliminarOferta.setBackground(Constantes.CONTENEDOR_CLR);
		btnEliminarOferta.setActionCommand("btnEliminarOferta");
		pFConstraints.gridx = 2;
		pFConstraints.insets = new Insets(0, 0, 0, 0);
		panelFunciones.add(btnEliminarOferta, pFConstraints);

		panelDerecho.add(panelFunciones, pDConstraints);

		panelPrincipal.add(panelDerecho, BorderLayout.EAST);

		dialog.add(panelPrincipal);
	}

	private void addActionListeners(ActionListener listener) {
		btnAddOferta.addActionListener(listener);
		btnModificarOferta.addActionListener(listener);
		btnEliminarOferta.addActionListener(listener);
		btnImagen1.addActionListener(listener);
		btnImagen2.addActionListener(listener);
		btnImagen3.addActionListener(listener);
	}

	private void addListListener(ListSelectionListener listener) {
		listaOfertas.addListSelectionListener(listener);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "btnImagen1":
			btnImagen1.setBorder(Constantes.SELECT_BRD);
			btnImagen2.setBorder(UNSELECT_BRD);
			btnImagen3.setBorder(UNSELECT_BRD);
			btnSeleccionado = 1;
			break;
		case "btnImagen2":
			btnImagen2.setBorder(Constantes.SELECT_BRD);
			btnImagen1.setBorder(UNSELECT_BRD);
			btnImagen3.setBorder(UNSELECT_BRD);
			btnSeleccionado = 2;
			break;
		case "btnImagen3":
			btnImagen3.setBorder(Constantes.SELECT_BRD);
			btnImagen1.setBorder(UNSELECT_BRD);
			btnImagen2.setBorder(UNSELECT_BRD);
			btnSeleccionado = 3;
			break;
		case "btnAddOferta":
			if (!txtNombreOferta.getText().equalsIgnoreCase("") && !txtDescOferta.getText().equalsIgnoreCase("")) {
				String imagenOferta = "";
				switch (btnSeleccionado) {
				case 1:
					imagenOferta = "/imagenes/emblema_tactico.png";
					break;
				case 2:
					imagenOferta = "/imagenes/emblema_ingenieria.png";
					break;
				case 3:
					imagenOferta = "/imagenes/emblema_ciencias.png";
					break;
				}
				Oferta oferta = new Oferta(imagenOferta, txtNombreOferta.getText(), txtDescOferta.getText());
				dlmOfertas.addElement(oferta);
				inicializarCampos();
			} else {
				JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos para proceder", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
			break;
		case "btnModificarOferta":
			if (!listaOfertas.isSelectionEmpty()) {
				if (!txtNombreOferta.getText().equalsIgnoreCase("") && !txtDescOferta.getText().equalsIgnoreCase("")) {
					String imagenOferta = "";
					switch (btnSeleccionado) {
					case 1:
						imagenOferta = "/imagenes/emblema_tactico.png";
						break;
					case 2:
						imagenOferta = "/imagenes/emblema_ingenieria.png";
						break;
					case 3:
						imagenOferta = "/imagenes/emblema_ciencias.png";
						break;
					}
					dlmOfertas.get(listaOfertas.getSelectedIndex()).setTitulo(txtNombreOferta.getText());
					dlmOfertas.get(listaOfertas.getSelectedIndex()).setDescripcion(txtDescOferta.getText());
					dlmOfertas.get(listaOfertas.getSelectedIndex()).setImagen(imagenOferta);
					inicializarCampos();
				} else {
					JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos para proceder", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {
				JOptionPane.showMessageDialog(null, "Debes seleccionar un elemento para modificar", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
			break;
		case "btnEliminarOferta":
			if (!listaOfertas.isSelectionEmpty()) {
				dlmOfertas.remove(listaOfertas.getSelectedIndex());
				inicializarCampos();
			} else {
				JOptionPane.showMessageDialog(null, "Debes seleccionar un elemento para eliminar", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
			break;
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource().equals(listaOfertas)) {
			if (!listaOfertas.isSelectionEmpty()) {
				txtNombreOferta.setText(dlmOfertas.get(listaOfertas.getSelectedIndex()).getTitulo());
				txtDescOferta.setText(dlmOfertas.get(listaOfertas.getSelectedIndex()).getDescripcion());
				switch (dlmOfertas.get(listaOfertas.getSelectedIndex()).getImagen()) {
				case "/imagenes/emblema_tactico.png":
					btnImagen1.setBorder(Constantes.SELECT_BRD);
					btnImagen2.setBorder(UNSELECT_BRD);
					btnImagen3.setBorder(UNSELECT_BRD);
					btnSeleccionado = 1;
					break;
				case "/imagenes/emblema_ingenieria.png":
					btnImagen2.setBorder(Constantes.SELECT_BRD);
					btnImagen1.setBorder(UNSELECT_BRD);
					btnImagen3.setBorder(UNSELECT_BRD);
					btnSeleccionado = 2;
					break;
				case "/imagenes/emblema_ciencias.png":
					btnImagen3.setBorder(Constantes.SELECT_BRD);
					btnImagen1.setBorder(UNSELECT_BRD);
					btnImagen2.setBorder(UNSELECT_BRD);
					btnSeleccionado = 3;
					break;
				}
			}
		}
	}

	private void inicializarCampos() {
		txtNombreOferta.setText("");
		txtDescOferta.setText("");
		btnImagen1.setBorder(Constantes.SELECT_BRD);
		btnImagen2.setBorder(UNSELECT_BRD);
		btnImagen3.setBorder(UNSELECT_BRD);
		btnSeleccionado = 1;
	}
}

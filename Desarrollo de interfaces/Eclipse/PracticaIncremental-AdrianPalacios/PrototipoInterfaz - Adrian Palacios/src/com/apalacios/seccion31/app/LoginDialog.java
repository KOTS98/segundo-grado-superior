package com.apalacios.seccion31.app;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import base.Oferta;

@SuppressWarnings("serial")
public class LoginDialog extends JFrame implements ActionListener {
	private Properties conf = new Properties();
	private JDialog dialog;
	private String usuario;
	private String pass;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JTextField txtUsuario;
	private JTextField txtPass;
	private DefaultListModel<Oferta> dlmOfertas;

	public LoginDialog(DefaultListModel<Oferta> dlmOfertas) {
		this.dlmOfertas = dlmOfertas;
		setBackground(Constantes.FONDO_CLR);
		dialog = new JDialog(this, "Acceso de administrador", true);
		init();
		addActionListeners();
		obtenerCredenciales();
		dialog.setSize(320, 250);
		dialog.setLocationRelativeTo(null);
		dialog.setResizable(false);
		dialog.setVisible(true);
	}

	private void init() {
		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());
		panelPrincipal.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints c = new GridBagConstraints();

		// Titulo
		JLabel lblTitulo = new JLabel("Iniciar sesi�n");
		lblTitulo.setFont(new Font("Impact", 0, 25));
		lblTitulo.setForeground(Constantes.TEXTO_CLR);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.insets = new Insets(-50, 0, 0, 0);
		panelPrincipal.add(lblTitulo, c);

		// Etiqueta usuario
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Impact", 0, 20));
		lblUsuario.setForeground(Constantes.TEXTO_CLR);
		c.gridy = 1;
		c.insets = new Insets(10, 0, 0, 0);
		c.anchor = GridBagConstraints.WEST;
		panelPrincipal.add(lblUsuario, c);

		// Texto usuario
		txtUsuario = new JTextField();
		txtUsuario.setFont(new Font("Impact", 0, 15));
		txtUsuario.setPreferredSize(new Dimension(300, 20));
		c.gridy = 2;
		panelPrincipal.add(txtUsuario, c);

		// Etiqueta contrase�a
		JLabel lblPass = new JLabel("Contrase�a");
		lblPass.setFont(new Font("Impact", 0, 20));
		lblPass.setForeground(Constantes.TEXTO_CLR);
		c.gridy = 3;
		panelPrincipal.add(lblPass, c);

		// Texto contrase�a
		txtPass = new JPasswordField();
		txtPass.setFont(new Font("Impact", 0, 15));
		txtPass.setPreferredSize(new Dimension(300, 20));
		c.gridy = 4;
		panelPrincipal.add(txtPass, c);

		// Boton aceptar
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setForeground(Constantes.TEXTO_CLR);
		btnAceptar.setFont(new Font("Impact", 0, 15));
		btnAceptar.setBackground(Constantes.CONTENEDOR_CLR);
		c.gridy = 5;
		c.gridwidth = 1;
		c.insets = new Insets(0, 0, -50, 0);
		panelPrincipal.add(btnAceptar, c);

		// Boton cancelar
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setForeground(Constantes.TEXTO_CLR);
		btnCancelar.setFont(new Font("Impact", 0, 15));
		btnCancelar.setBackground(Constantes.CONTENEDOR_CLR);
		c.gridx = 1;
		c.insets = new Insets(0, 130, -50, 0);
		panelPrincipal.add(btnCancelar, c);

		dialog.add(panelPrincipal);
	}

	private void obtenerCredenciales() {
		FileReader fr;
		try {
			fr = new FileReader("properties.conf");
			conf.load(fr);
			usuario = conf.getProperty("usuario");
			pass = conf.getProperty("pass");
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean comprobarCredenciales() {
		if (usuario.equalsIgnoreCase(txtUsuario.getText()) && pass.equalsIgnoreCase(txtPass.getText())) {
			return true;
		} else {
			return false;
		}
	}

	private void addActionListeners() {
		btnAceptar.addActionListener(this);
		btnCancelar.addActionListener(this);
		btnAceptar.setActionCommand("btnAceptar");
		btnCancelar.setActionCommand("btnCancelar");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equalsIgnoreCase("btnAceptar")) {
			if (comprobarCredenciales()) {
				dispose();
				@SuppressWarnings("unused")
				OfertasAdminDialog oad = new OfertasAdminDialog(dlmOfertas);
			} else {
				JOptionPane.showMessageDialog(null, "Credenciales incorrectas", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} else {
			dispose();
		}
	}
}

package com.apalacios.seccion31.app;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public class MenuOpcionesDialog extends JFrame {
	Properties conf = new Properties();
	JDialog dialog;
	JRadioButton rBtnTemaClaro;
	JRadioButton rBtnTemaOscuro;
	JButton btnGuardarPref;

	public MenuOpcionesDialog() {
		setBackground(Constantes.FONDO_CLR);
		dialog = new JDialog(this, "Opciones", true);
		init();
		cargarPreferencias();
		dialog.setSize(400, 300);
		dialog.setLocationRelativeTo(null);
		dialog.setResizable(false);
		dialog.setVisible(true);
	}

	private void init() {
		JPanel panelPrincipal = new JPanel();
		panelPrincipal.setLayout(new GridBagLayout());
		panelPrincipal.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints c = new GridBagConstraints();

		rBtnTemaClaro = new JRadioButton("Claro");
		rBtnTemaClaro.setFont(Constantes.BTN_FNT);
		rBtnTemaClaro.setForeground(Constantes.TEXTO_CLR);
		rBtnTemaClaro.setBackground(Constantes.PANTALLA_CLR);
		rBtnTemaOscuro = new JRadioButton("Oscuro");
		rBtnTemaOscuro.setFont(Constantes.BTN_FNT);
		rBtnTemaOscuro.setForeground(Constantes.TEXTO_CLR);
		rBtnTemaOscuro.setBackground(Constantes.PANTALLA_CLR);
		ButtonGroup grupo = new ButtonGroup();
		grupo.add(rBtnTemaClaro);
		grupo.add(rBtnTemaOscuro);

		// Titulo
		JLabel lblTitulo = new JLabel("Selecciona un tema:");
		lblTitulo.setFont(Constantes.BTN_FNT);
		lblTitulo.setForeground(Constantes.TEXTO_CLR);
		c.insets = new Insets(0, 0, 50, 0);
		panelPrincipal.add(lblTitulo, c);

		// RoundButton 1 - Tema claro
		c.gridx = 0;
		c.gridy = 1;
		c.insets = new Insets(0, 0, 0, 0);
		c.anchor = GridBagConstraints.WEST;
		panelPrincipal.add(rBtnTemaClaro, c);

		// RoundButton 2 - Tema oscuro
		c.gridy = 2;
		panelPrincipal.add(rBtnTemaOscuro, c);
		dialog.add(panelPrincipal);

		// Bot�n guardar preferencias
		btnGuardarPref = new JButton(" Guardar preferencias ");
		btnGuardarPref.setFont(Constantes.BTN_FNT);
		btnGuardarPref.setBorder(Constantes.STNDR_BRD);
		btnGuardarPref.setForeground(Constantes.TEXTO_CLR);
		btnGuardarPref.setBackground(Constantes.CONTENEDOR_CLR);
		c.gridx = 0;
		c.gridy = 3;
		c.insets = new Insets(40, 0, 0, 0);
		c.anchor = GridBagConstraints.CENTER;
		panelPrincipal.add(btnGuardarPref, c);

		btnGuardarPref.addActionListener(e -> {
			guardarPreferencias();
		});
	}

	private void cargarPreferencias() {
		FileReader fr;
		try {
			fr = new FileReader("properties.conf");
			conf.load(fr);
			if (conf.getProperty("tema").equalsIgnoreCase("claro")) {
				rBtnTemaClaro.setSelected(true);
				rBtnTemaOscuro.setSelected(false);
			} else {
				rBtnTemaClaro.setSelected(false);
				rBtnTemaOscuro.setSelected(true);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void guardarPreferencias() {
		try {
			FileWriter fw = new FileWriter("properties.conf");
			if (rBtnTemaClaro.isSelected()) {
				conf.setProperty("tema", "claro");
			} else {
				conf.setProperty("tema", "oscuro");
			}
			conf.store(fw, "");
		} catch (IOException e) {
			e.printStackTrace();
		}
		dispose();
	}
}

package com.apalacios.seccion31.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BtnPagar extends JButton {
	public BtnPagar(String nombre) {
		setText(nombre);
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Random r = new Random();
				int intAleatorio = r.nextInt(100) + 1;
				if (intAleatorio < 20) {
					JDialog descuento = new JDialog();
					descuento.setTitle("ˇFELICICADES!");

					JPanel panelDescuento = new JPanel();
					panelDescuento.setLayout(new BorderLayout());
					panelDescuento.setBackground(Constantes.PANTALLA_CLR);

					JLabel titulo = new JLabel("ˇFELICIDADES!");
					titulo.setForeground(Constantes.TEXTO_CLR);
					titulo.setFont(Constantes.BTN_FNT);
					panelDescuento.add(titulo, BorderLayout.NORTH);

					JLabel mensaje = new JLabel("HOY NO PAGAS LA CUENTA");
					mensaje.setForeground(Constantes.TEXTO_CLR);
					mensaje.setFont(Constantes.BTN_FNT);
					panelDescuento.add(mensaje, BorderLayout.CENTER);

					descuento.setModal(true);
					descuento.setContentPane(panelDescuento);
					setSize(new Dimension(300, 300));
					descuento.setResizable(false);
					descuento.setLocationRelativeTo(null);
					descuento.pack();
					descuento.setVisible(true);
				}
			}
		});
	}
}

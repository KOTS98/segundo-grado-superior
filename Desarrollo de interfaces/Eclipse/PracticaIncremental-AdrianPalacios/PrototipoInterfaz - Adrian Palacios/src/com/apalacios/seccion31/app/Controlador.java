package com.apalacios.seccion31.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import base.Oferta;
import base.Producto;

public class Controlador implements ActionListener, ListSelectionListener, WindowListener {
	private Vista vista;

	public Controlador(Vista vista) {
		this.vista = vista;
		anyadirListeners();
	}

	private void anyadirListeners() {
		// Vista principal, cargar datos al abrir, guardarlos al cerrar
		vista.addWindowListener(this);

		// Imagenes en las que se puede clicar para obtener una descripci�n del producto
		// seleccionado
		vista.btnImgProducto1.addActionListener(this);
		vista.btnImgProducto2.addActionListener(this);
		vista.btnImgProducto3.addActionListener(this);
		vista.btnImgProducto4.addActionListener(this);
		vista.btnImgProducto5.addActionListener(this);

		// Botones para a�adir el producto seleccionado al carrito de la compra
		vista.btnAdd1.addActionListener(this);
		vista.btnAdd2.addActionListener(this);
		vista.btnAdd3.addActionListener(this);
		vista.btnAdd4.addActionListener(this);
		vista.btnAdd5.addActionListener(this);

		// Boton para eliminar un producto del carrito
		vista.eliminarProducto.addActionListener(this);

		// Boton para hacer el check-out (pagar)
		vista.pagar.addActionListener(this);

		// Lista de ofertas
		vista.listaOfertas.addListSelectionListener(this);
		vista.btnOfertasAdmin.addActionListener(this);

		// Boton que llama al men� de opciones
		vista.btnMenuOpciones.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {

		// Click en las imagenes, ofrece una descripci�n del producto seleccionado
		case "producto1":
			vista.descripcion.setText(Constantes.DESC_P1);
			break;

		case "producto2":
			vista.descripcion.setText(Constantes.DESC_P2);
			break;

		case "producto3":
			vista.descripcion.setText(Constantes.DESC_P3);
			break;

		case "producto4":
			vista.descripcion.setText(Constantes.DESC_P4);
			break;

		case "producto5":
			vista.descripcion.setText(Constantes.DESC_P5);
			break;

		// Botones para a�adir productos al carrito de la compra
		case "addProducto1": {
			addProducto(vista.producto1);
		}
			break;
		case "addProducto2": {
			addProducto(vista.producto2);
		}
			break;
		case "addProducto3": {
			addProducto(vista.producto3);
		}
			break;
		case "addProducto4": {
			addProducto(vista.producto4);
		}
			break;
		case "addProducto5": {
			addProducto(vista.producto5);
		}
			break;
		case "btnOfertasAdmin":
			@SuppressWarnings("unused")
			LoginDialog login = new LoginDialog(vista.dlmOfertas);
			break;
		case "eliminarProductoSelec":
			eliminarUnidad();
			break;
		case "pagar":
			eliminarTodos();
			break;
		case "opciones":
			@SuppressWarnings("unused")
			MenuOpcionesDialog dialog = new MenuOpcionesDialog();
			break;
		}
		vista.calcularPrecioTotal();
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource().equals(vista.listaOfertas)) {
			vista.txtTituloOferta.setText(vista.dlmOfertas.get(vista.listaOfertas.getSelectedIndex()).getTitulo());
			vista.txtDescOferta.setText(vista.dlmOfertas.get(vista.listaOfertas.getSelectedIndex()).getDescripcion());
		}
	}

	public void addProducto(Producto producto) {
		// Sumar uno al conteo de unidades (cantidad) del producto
		producto.agregarUnidad();
		// Sumar el precio unitario del producto al precio total
		vista.precioTotal += producto.getPrecio();
		// Comprobar si el producto cuenta con una �nica unidad.
		if (producto.getCantidad() == 1) {
			// A�adir el producto al DefaultListModel
			vista.dlmCarritoCompra.addElement(producto);
		}
		vista.pagar.setEnabled(true);
	}

	public void eliminarUnidad() {
		// Comprobar que hay un elemento seleccionado
		if (vista.carritoCompra.getSelectedValue() != null) {
			// Comprobar si el n�mero de unidades del producto (cantidad), es mayor que 0
			if (vista.dlmCarritoCompra.get(vista.carritoCompra.getSelectedIndex()).getCantidad() > 0) {
				// Restar una unidad del producto y restar al precio total el precio unitario
				// del producto
				vista.dlmCarritoCompra.get(vista.carritoCompra.getSelectedIndex()).eliminarUnidad();
				vista.precioTotal -= vista.dlmCarritoCompra.get(vista.carritoCompra.getSelectedIndex()).getPrecio();
			}
			actualizarCarrito();
		}
		// Comprobar el precioTotal para determinar si hay alg�n producto en el carrito
		// de la compra.
		if (vista.precioTotal == 0.0) {
			vista.pagar.setEnabled(false);
		}
	}

	public void eliminarTodos() {
		// Recorrer el DefaultListModel que contiene los productos
		for (int i = 0; i < vista.dlmCarritoCompra.size(); i++) {
			// Establecer en 0 el n�mero de unidades (cantidad) de cada producto
			vista.dlmCarritoCompra.get(i).setCantidad(0);
		}
		// Eliminar todos los elementos del DefaultListModel
		vista.dlmCarritoCompra.removeAllElements();
		// Actualizar el precio total
		vista.precioTotal = 0;
		vista.pagar.setEnabled(false);
	}

	public void actualizarCarrito() {
		// Recorrer el DefaultListModel que contiene los productos
		for (int i = 0; i < vista.dlmCarritoCompra.size(); i++) {
			// Comprobar si el n�mero de unidades del producto (cantidad), es igual a 0
			if (vista.dlmCarritoCompra.get(i).getCantidad() == 0) {
				// Eliminar el producto de la lista
				vista.dlmCarritoCompra.remove(i);
			}
		}
		// Repintar el precio y la lista de productos
		vista.precioTotalTxt.repaint();
		vista.carritoCompra.repaint();
	}

	@Override
	public void windowOpened(WindowEvent arg0) {
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(new FileInputStream("datos.bin"));
			@SuppressWarnings("unchecked")
			DefaultListModel<Oferta> ofertasArchivo = (DefaultListModel<Oferta>) ois.readObject();
			ois.close();
			for (int i = 0; i < ofertasArchivo.getSize(); i++) {
				vista.dlmOfertas.addElement(ofertasArchivo.get(i));
			}
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,
					"No se ha encontrado el archivo de datos, se generar� uno nuevo al cerrar la aplicaci�n", "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void windowClosing(WindowEvent arg0) {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("datos.bin"));
			oos.writeObject(vista.dlmOfertas);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
	}
}

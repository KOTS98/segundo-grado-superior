package com.apalacios.seccion31.app;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.text.View;

import base.Oferta;

@SuppressWarnings("serial")
public class OfertaRenderer extends JPanel implements ListCellRenderer<Object> {
	@Override
	public Component getListCellRendererComponent(JList<?> list, Object valores, int index, boolean isSelected,
			boolean cellHasFocus) {
		// Eliminar todos los elementos
		this.removeAll();
		// Establecer el Layout, y crear el panel
		this.setLayout(new BorderLayout());
		JPanel informacion = new JPanel();
		informacion.setBackground(Constantes.PANTALLA_CLR);
		informacion.setLayout(new BoxLayout(informacion, BoxLayout.Y_AXIS));

		// Comprobar si hay un elemento seleccionado
		if (isSelected) {
			// Establecer color de fondo para el elemento seleccionado
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
			informacion.setBackground(list.getBackground());
			informacion.setForeground(list.getForeground());

		} else {
			// Establecer color de fondo para el elemento no seleccionado
			setBackground(list.getBackground());
			setForeground(list.getForeground());
			informacion.setBackground(list.getBackground());
			informacion.setForeground(list.getForeground());
		}

		// Crear el prototipo de oferta y dotarlo de sus distintivos gr�ficos
		Oferta oferta = (Oferta) valores;
		ImageIcon icon = new ImageIcon(View.class.getResource(oferta.getImagen()));
		JLabel imagen = new JLabel(icon);
		JLabel titulo = new JLabel(oferta.getTitulo());
		titulo.setFont(Constantes.BTN_FNT);
		titulo.setForeground(Constantes.TEXTO_CLR);

		// A�adir la imagen al panel principal
		this.add(imagen, BorderLayout.WEST);

		// A�adir el resto de campos al panel de informaci�n
		informacion.add(titulo);

		// A�adir el panel de informaci�n al panel principal y establecer un borde
		this.add(informacion, BorderLayout.CENTER);
		this.setBorder(Constantes.STNDR_BRD);
		return this;
	}
}

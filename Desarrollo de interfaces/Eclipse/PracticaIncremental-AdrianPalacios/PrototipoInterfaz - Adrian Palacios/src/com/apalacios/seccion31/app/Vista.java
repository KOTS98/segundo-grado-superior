package com.apalacios.seccion31.app;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.View;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import base.Oferta;
import base.Producto;

@SuppressWarnings("serial")
public class Vista extends JFrame {

	// Estructura de la vista
	Container contenedor;
	JTabbedPane pestanyas;
	JPanel panelProductos;
	JPanel panelOfertas;
	JPanel panelCarrito;
	JPanel panelStats;
	JLabel lblLoginTitulo;
	JTextField txtLoginUsuario;
	JTextField txtLoginPass;

	// Barra de menu
	JMenuItem btnMenuOpciones;

	// Productos
	// Botones con imagen para seleccionar producto.
	JButton btnImgProducto1;
	JButton btnImgProducto2;
	JButton btnImgProducto3;
	JButton btnImgProducto4;
	JButton btnImgProducto5;

	// Botones para a�adir producto al carrito
	JButton btnAdd1;
	JButton btnAdd2;
	JButton btnAdd3;
	JButton btnAdd4;
	JButton btnAdd5;

	// Campo de texto explicativo del producto
	JTextArea descripcion;

	// Ofertas - Vista usuario
	// Lista de ofertas
	JList<Oferta> listaOfertas;
	DefaultListModel<Oferta> dlmOfertas;
	OfertaRenderer or;

	// Carrito
	// Renderer para ajustar los productos a la lista
	JList<Producto> carritoCompra;
	DefaultListModel<Producto> dlmCarritoCompra;
	ProductoRenderer pr;
	ArrayList<Producto> listaProductos;
	JTextArea txtTituloOferta;
	JTextArea txtDescOferta;
	JButton btnOfertasAdmin;

	// Calculo de precio total
	JTextArea precioTotalTxt;
	float precioTotal;

	// Boton eliminar producto
	JButton eliminarProducto;

	// Boton de pagar
	BtnPagar pagar;

	// Logo flota estelar
	JButton logoFE;

	// Productos
	Producto producto1 = new Producto("Atardecer samariano", "/imagenes/atardecer_samariano.png", 8);
	Producto producto2 = new Producto("Brandy sauriano", "/imagenes/brandy_sauriano.png", 6);
	Producto producto3 = new Producto("Cerveza romulana", "/imagenes/cerveza_romulana.png", (float) 2.5);
	Producto producto4 = new Producto("Kanar", "/imagenes/kanar.png", 2);
	Producto producto5 = new Producto("T� Plomeek", "/imagenes/te_plomeek.png", 3);

	public Vista() {
		init();
	}

	private void init() {
		contenedor = new Container();
		contenedor = getContentPane();
		getContentPane().setBackground(Constantes.FONDO_CLR);
		pestanyas = new JTabbedPane();
		pestanyas.setBackground(Constantes.PANTALLA_CLR);
		pestanyas.setForeground(Constantes.TEXTO_CLR);
		pestanyas.setFont(new Font("Impact", 0, 30));
		pr = new ProductoRenderer();
		dlmCarritoCompra = new DefaultListModel<>();
		listaProductos = new ArrayList<>();
		or = new OfertaRenderer();
		dlmOfertas = new DefaultListModel<>();
		precioTotal = 0;
		panelProductos();
		panelOfertas();
		panelCarrito();
		panelEstadisticas();
		getContentPane().add(pestanyas);
		crearMenuOpciones();
	}

	public void panelProductos() {
		panelProductos = new JPanel();
		panelProductos.setLayout(new BorderLayout());
		panelProductos.setBackground(Constantes.PANTALLA_CLR);

		// Panel superior
		JPanel panelSuperior = new JPanel();
		panelSuperior.setLayout(new GridBagLayout());
		panelSuperior.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pSConstraints = new GridBagConstraints();

		// Nombre producto 1
		JLabel lblNombreP1 = new JLabel("Atardecer Samariano");
		lblNombreP1.setFont(new Font("Impact", 0, 18));
		lblNombreP1.setForeground(Constantes.TEXTO_CLR);
		pSConstraints.insets = Constantes.LBL_SPACE;
		pSConstraints.gridx = 0;
		pSConstraints.gridy = 0;
		panelSuperior.add(lblNombreP1, pSConstraints);

		// Nombre producto 2
		JLabel lblNombreP2 = new JLabel("Brandy Sauriano");
		lblNombreP2.setFont(new Font("Impact", 0, 15));
		lblNombreP2.setForeground(Constantes.TEXTO_CLR);
		pSConstraints.gridx = 1;
		panelSuperior.add(lblNombreP2, pSConstraints);

		// Nombre producto 3
		JLabel lblNombreP3 = new JLabel("Cerveza Romulana");
		lblNombreP3.setFont(new Font("Impact", 0, 15));
		lblNombreP3.setForeground(Constantes.TEXTO_CLR);
		pSConstraints.gridx = 2;
		panelSuperior.add(lblNombreP3, pSConstraints);

		// Nombre producto 4
		JLabel lblNombreP4 = new JLabel("Kanar");
		lblNombreP4.setFont(new Font("Impact", 0, 15));
		lblNombreP4.setForeground(Constantes.TEXTO_CLR);
		pSConstraints.gridx = 3;
		panelSuperior.add(lblNombreP4, pSConstraints);

		// Nombre producto 5
		JLabel lblNombreP5 = new JLabel("T� Plomeek");
		lblNombreP5.setFont(new Font("Impact", 0, 15));
		lblNombreP5.setForeground(Constantes.TEXTO_CLR);
		pSConstraints.gridx = 4;
		panelSuperior.add(lblNombreP5, pSConstraints);

		// Imagen 1
		btnImgProducto1 = new JButton(new ImageIcon(View.class.getResource("/imagenes/atardecer_samariano.png")));
		btnImgProducto1.setActionCommand("producto1");
		btnImgProducto1.setPreferredSize(Constantes.IMG_SIZE);
		btnImgProducto1.setBorder(Constantes.STNDR_BRD);
		pSConstraints.insets = Constantes.IMG_SPACE;
		pSConstraints.gridx = 0;
		pSConstraints.gridy = 1;
		panelSuperior.add(btnImgProducto1, pSConstraints);

		// Imagen 2
		btnImgProducto2 = new JButton(new ImageIcon(View.class.getResource("/imagenes/brandy_sauriano.png")));
		btnImgProducto2.setActionCommand("producto2");
		btnImgProducto2.setPreferredSize(Constantes.IMG_SIZE);
		btnImgProducto2.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 1;
		panelSuperior.add(btnImgProducto2, pSConstraints);

		// Imagen 3
		btnImgProducto3 = new JButton(new ImageIcon(View.class.getResource("/imagenes/cerveza_romulana.png")));
		btnImgProducto3.setActionCommand("producto3");
		btnImgProducto3.setPreferredSize(Constantes.IMG_SIZE);
		btnImgProducto3.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 2;
		panelSuperior.add(btnImgProducto3, pSConstraints);

		// Imagen 4
		btnImgProducto4 = new JButton(new ImageIcon(View.class.getResource("/imagenes/kanar.png")));
		btnImgProducto4.setActionCommand("producto4");
		btnImgProducto4.setPreferredSize(Constantes.IMG_SIZE);
		btnImgProducto4.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 3;
		panelSuperior.add(btnImgProducto4, pSConstraints);

		// Imagen 5
		btnImgProducto5 = new JButton(new ImageIcon(View.class.getResource("/imagenes/te_plomeek.png")));
		btnImgProducto5.setActionCommand("producto5");
		btnImgProducto5.setPreferredSize(Constantes.IMG_SIZE);
		btnImgProducto5.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 4;
		panelSuperior.add(btnImgProducto5, pSConstraints);

		// Boton 1
		btnAdd1 = new JButton("A�adir");
		btnAdd1.setPreferredSize(Constantes.BTN_SIZE);
		btnAdd1.setActionCommand("addProducto1");
		btnAdd1.setFont(Constantes.BTN_FNT);
		btnAdd1.setForeground(Constantes.TEXTO_CLR);
		btnAdd1.setBackground(Constantes.CONTENEDOR_CLR);
		btnAdd1.setBorder(Constantes.STNDR_BRD);
		pSConstraints.insets = Constantes.BTN_SPACE;
		pSConstraints.gridx = 0;
		pSConstraints.gridy = 2;
		panelSuperior.add(btnAdd1, pSConstraints);

		// Boton 2
		btnAdd2 = new JButton("A�adir");
		btnAdd2.setPreferredSize(Constantes.BTN_SIZE);
		btnAdd2.setActionCommand("addProducto2");
		btnAdd2.setFont(Constantes.BTN_FNT);
		btnAdd2.setForeground(Constantes.TEXTO_CLR);
		btnAdd2.setBackground(Constantes.CONTENEDOR_CLR);
		btnAdd2.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 1;
		panelSuperior.add(btnAdd2, pSConstraints);

		// Boton 3
		btnAdd3 = new JButton("A�adir");
		btnAdd3.setPreferredSize(Constantes.BTN_SIZE);
		btnAdd3.setActionCommand("addProducto3");
		btnAdd3.setFont(Constantes.BTN_FNT);
		btnAdd3.setForeground(Constantes.TEXTO_CLR);
		btnAdd3.setBackground(Constantes.CONTENEDOR_CLR);
		btnAdd3.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 2;
		panelSuperior.add(btnAdd3, pSConstraints);

		// Boton 4
		btnAdd4 = new JButton("A�adir");
		btnAdd4.setPreferredSize(Constantes.BTN_SIZE);
		btnAdd4.setActionCommand("addProducto4");
		btnAdd4.setFont(Constantes.BTN_FNT);
		btnAdd4.setForeground(Constantes.TEXTO_CLR);
		btnAdd4.setBackground(Constantes.CONTENEDOR_CLR);
		btnAdd4.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 3;
		panelSuperior.add(btnAdd4, pSConstraints);

		// Boton 5
		btnAdd5 = new JButton("A�adir");
		btnAdd5.setPreferredSize(Constantes.BTN_SIZE);
		btnAdd5.setActionCommand("addProducto5");
		btnAdd5.setFont(Constantes.BTN_FNT);
		btnAdd5.setForeground(Constantes.TEXTO_CLR);
		btnAdd5.setBackground(Constantes.CONTENEDOR_CLR);
		btnAdd5.setBorder(Constantes.STNDR_BRD);
		pSConstraints.gridx = 4;
		panelSuperior.add(btnAdd5, pSConstraints);

		// Panel inferior
		JPanel panelInferior = new JPanel();
		panelInferior.setLayout(new GridBagLayout());
		panelInferior.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pIConstraints = new GridBagConstraints();

		// Etiqueta informacion
		JLabel txtInformacion = new JLabel("Informaci�n");
		txtInformacion.setFont(new Font("Impact", 0, 40));
		txtInformacion.setBackground(Constantes.CONTENEDOR_CLR);
		txtInformacion.setForeground(Constantes.TEXTO_CLR);
		pIConstraints.gridx = 0;
		pIConstraints.gridy = 0;
		pIConstraints.insets = new Insets(0, 0, 10, 800);
		panelInferior.add(txtInformacion, pIConstraints);

		// Campo de texto
		descripcion = new JTextArea(Constantes.DESC_P1);
		descripcion.setPreferredSize(new Dimension(1000, 120));
		descripcion.setBorder(Constantes.STNDR_BRD);
		descripcion.setFont(new Font("Impact", 0, 25));
		descripcion.setBackground(Constantes.CONTENEDOR_CLR);
		descripcion.setForeground(Constantes.TEXTO_CLR);
		descripcion.setEditable(false);
		descripcion.setLineWrap(true);
		descripcion.setWrapStyleWord(true);
		pIConstraints.gridy = 1;
		pIConstraints.insets = new Insets(0, 0, 0, 0);
		panelInferior.add(descripcion, pIConstraints);

		// A�adir a panel principal
		panelProductos.add(panelSuperior, BorderLayout.NORTH);
		panelProductos.add(panelInferior, BorderLayout.SOUTH);
		pestanyas.addTab("PRODUCTOS", panelProductos);
	}

	public void panelOfertas() {
		panelOfertas = new JPanel();
		panelOfertas.setLayout(new BorderLayout());
		panelOfertas.setBackground(Constantes.PANTALLA_CLR);

		// Panel izquierdo
		JPanel panelIzquierdo = new JPanel();
		panelIzquierdo.setLayout(new BorderLayout());

		// Lista de ofertas
		listaOfertas = new JList<Oferta>();
		JScrollPane scroll = new JScrollPane(listaOfertas, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setPreferredSize(new Dimension(500, 540));
		listaOfertas.setBackground(Constantes.CONTENEDOR_CLR);
		listaOfertas.setBorder(Constantes.STNDR_BRD);
		listaOfertas.setModel(dlmOfertas);
		listaOfertas.setCellRenderer(or);
		panelIzquierdo.add(scroll, BorderLayout.CENTER);

		// Boton modificar ofertas (admin)
		btnOfertasAdmin = new JButton("Modificar ofertas");
		btnOfertasAdmin.setFont(Constantes.BTN_FNT);
		btnOfertasAdmin.setForeground(Constantes.TEXTO_CLR);
		btnOfertasAdmin.setBackground(Constantes.CONTENEDOR_CLR);
		btnOfertasAdmin.setBorder(Constantes.STNDR_BRD);
		btnOfertasAdmin.setActionCommand("btnOfertasAdmin");
		panelIzquierdo.add(btnOfertasAdmin, BorderLayout.SOUTH);

		panelOfertas.add(panelIzquierdo, BorderLayout.WEST);

		// Panel derecho
		JPanel panelDerecho = new JPanel();
		panelDerecho.setLayout(new GridBagLayout());
		panelDerecho.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pDConstraints = new GridBagConstraints();

		// Titulo oferta
		txtTituloOferta = new JTextArea();
		txtTituloOferta.setBackground(Constantes.CONTENEDOR_CLR);
		txtTituloOferta.setForeground(Constantes.TEXTO_CLR);
		txtTituloOferta.setFont(Constantes.BTN_FNT);
		txtTituloOferta.setBorder(Constantes.STNDR_BRD);
		txtTituloOferta.setPreferredSize(new Dimension(400, 50));
		txtTituloOferta.setEditable(false);
		pDConstraints.gridx = 0;
		pDConstraints.gridy = 0;
		pDConstraints.insets = new Insets(-200, 0, 0, 140);
		panelDerecho.add(txtTituloOferta, pDConstraints);

		// Descripci�n oferta
		txtDescOferta = new JTextArea();
		txtDescOferta.setBackground(Constantes.CONTENEDOR_CLR);
		txtDescOferta.setLineWrap(true);
		txtDescOferta.setWrapStyleWord(true);
		txtDescOferta.setForeground(Constantes.TEXTO_CLR);
		txtDescOferta.setFont(Constantes.BTN_FNT);
		txtDescOferta.setBorder(Constantes.STNDR_BRD);
		txtDescOferta.setPreferredSize(new Dimension(400, 300));
		txtDescOferta.setEditable(false);
		pDConstraints.gridy = 1;
		pDConstraints.insets = new Insets(0, 0, 0, 140);
		panelDerecho.add(txtDescOferta, pDConstraints);

		panelOfertas.add(panelDerecho, BorderLayout.EAST);
		pestanyas.addTab("OFERTAS", panelOfertas);

	}

	public void panelCarrito() {
		panelCarrito = new JPanel();
		panelCarrito.setLayout(new BorderLayout());
		JPanel panelIzquierdo = new JPanel();
		panelIzquierdo.setLayout(new BorderLayout());

		// Lista carrito de la compra
		carritoCompra = new JList<>();
		JScrollPane scroll = new JScrollPane(carritoCompra, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setPreferredSize(new Dimension(500, 540));
		carritoCompra.setBackground(Constantes.CONTENEDOR_CLR);
		carritoCompra.setBorder(Constantes.STNDR_BRD);
		carritoCompra.setModel(dlmCarritoCompra);
		carritoCompra.setCellRenderer(pr);
		panelIzquierdo.add(scroll, BorderLayout.CENTER);
		panelCarrito.add(panelIzquierdo, BorderLayout.WEST);

		// Precio total
		precioTotalTxt = new JTextArea(" Total a pagar: " + String.valueOf(precioTotal) + "0 �");
		precioTotalTxt.setEditable(false);
		precioTotalTxt.setFont(Constantes.BTN_FNT);
		precioTotalTxt.setForeground(Constantes.TEXTO_CLR);
		precioTotalTxt.setBackground(Constantes.CONTENEDOR_CLR);
		precioTotalTxt.setPreferredSize(new Dimension(500, 50));
		precioTotalTxt.setBorder(Constantes.STNDR_BRD);
		panelIzquierdo.add(precioTotalTxt, BorderLayout.SOUTH);

		JPanel panelDerecho = new JPanel();
		panelDerecho.setLayout(new GridBagLayout());
		panelDerecho.setBackground(Constantes.PANTALLA_CLR);
		GridBagConstraints pCConstraints = new GridBagConstraints();
		panelCarrito.setBackground(Constantes.PANTALLA_CLR);

		// Logo flota estelar
		logoFE = new JButton(new ImageIcon(View.class.getResource("/imagenes/logo_flota_estelar.png")));
		logoFE.setBorder(null);
		logoFE.setBackground(null);
		logoFE.setMaximumSize(new Dimension(400, 400));
		logoFE.setMinimumSize(new Dimension(400, 400));
		logoFE.setPreferredSize(new Dimension(400, 400));
		pCConstraints.gridx = 0;
		pCConstraints.gridy = 0;
		pCConstraints.gridwidth = 2;
		pCConstraints.insets = new Insets(50, 0, 0, 150);
		panelDerecho.add(logoFE, pCConstraints);

		// Boton eliminar producto
		eliminarProducto = new JButton(" Eliminar del carrito ");
		eliminarProducto.setActionCommand("eliminarProductoSelec");
		eliminarProducto.setFont(Constantes.BTN_FNT);
		eliminarProducto.setForeground(Constantes.TEXTO_CLR);
		eliminarProducto.setBackground(Constantes.CONTENEDOR_CLR);
		eliminarProducto.setBorder(Constantes.STNDR_BRD);
		pCConstraints.gridy = 1;
		pCConstraints.gridwidth = 1;
		pCConstraints.insets = new Insets(100, 0, 0, 0);
		panelDerecho.add(eliminarProducto, pCConstraints);

		// Boton pagar
		pagar = new BtnPagar(" PAGAR ");
		pagar.setActionCommand("pagar");
		pagar.setFont(Constantes.BTN_FNT);
		pagar.setForeground(Constantes.TEXTO_CLR);
		pagar.setBackground(Constantes.CONTENEDOR_CLR);
		pagar.setBorder(Constantes.STNDR_BRD);
		pagar.setEnabled(false);
		pCConstraints.gridx = 1;
		pCConstraints.insets = new Insets(100, 0, 0, 150);
		panelDerecho.add(pagar, pCConstraints);

		panelCarrito.add(panelDerecho, BorderLayout.EAST);
		pestanyas.addTab("CARRITO", panelCarrito);
	}

	public void panelEstadisticas() {
		panelStats = new JPanel();
		panelStats.setLayout(new BorderLayout());
		panelStats.setBackground(Constantes.PANTALLA_CLR);

		// Gr�fico de barras
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.setValue(2.9, "Consumo", "Atardecer Samariano");
		dataset.setValue(2.0, "Consumo", "Brandy Sauriano");
		dataset.setValue(4.1, "Consumo", "Cerveza Romulana");
		dataset.setValue(1.7, "Consumo", "Kanar");
		dataset.setValue(1.2, "Consumo", "T� Plomeek");
		JFreeChart chart = ChartFactory.createBarChart("Consumo de productos", "Consumo de productos", "Consumo",
				dataset, PlotOrientation.VERTICAL, true, true, false);
		ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setBackground(Constantes.CONTENEDOR_CLR);
		chartPanel.setFont(Constantes.BTN_FNT);
		chartPanel.setForeground(Constantes.TEXTO_CLR);
		panelStats.add(chartPanel, BorderLayout.CENTER);

		pestanyas.addTab("ESTAD�STICAS", panelStats);
	}

	public void crearMenuOpciones() {
		JMenuBar barraMenu = new JMenuBar();
		btnMenuOpciones = new JMenuItem("Opciones");
		btnMenuOpciones.setFont(new Font("Impact", 0, 15));
		btnMenuOpciones.setActionCommand("opciones");
		barraMenu.add(btnMenuOpciones);
		this.setJMenuBar(barraMenu);
	}

	public void calcularPrecioTotal() {
		precioTotalTxt.setText(" Total a pagar: " + String.valueOf(precioTotal) + "0 �");
	}
}

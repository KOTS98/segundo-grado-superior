package base;

public class Producto {
	private String nombre;
	private String imagen;
	private float precio;
	private int cantidad;

	public Producto(String nombre, String imagen, float precio) {
		this.nombre = nombre;
		this.imagen = imagen;
		this.precio = precio;
		this.cantidad = 0;
	}

	public String getNombre() {
		return nombre;
	}

	public String getImagen() {
		return imagen;
	}

	public float getPrecio() {
		return precio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public void agregarUnidad() {
		this.cantidad += 1;
	}

	public void eliminarUnidad() {
		this.cantidad -= 1;
	}
}

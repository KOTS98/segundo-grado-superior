package base;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Oferta implements Serializable {
	private String imagen;
	private String titulo;
	private String descripcion;

	public Oferta(String imagen, String titulo, String descripcion) {
		this.imagen = imagen;
		this.titulo = titulo;
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}

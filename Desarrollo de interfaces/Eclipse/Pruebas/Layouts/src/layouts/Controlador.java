package layouts;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Controlador implements ActionListener, MouseListener {

	private Vista vista;

	public Controlador(Vista vista) {
		this.vista = vista;
		anyadirListeners();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String key = vista.combobox.getSelectedItem().toString();
		CardLayout layout = (CardLayout) vista.panelCarta.getLayout();
		layout.show(vista.panelCarta, key);
	}

	public void anyadirListeners() {
		vista.combobox.addActionListener(this);
		vista.txt.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		vista.txt.setText("click");

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		vista.txt.setBackground(Color.BLUE);

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		vista.txt.setBackground(Color.WHITE);

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		vista.txt.setText("Bot�n pulsado");

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		vista.txt.setText("Bot�n soltado");

	}
}

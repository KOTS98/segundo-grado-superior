package layouts;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

public class Vista extends JFrame {
	public JComboBox combobox;
	public JPanel panelCarta;
	public JTextArea txt;

	public Vista() {
		init();
	}

	private void init() {
		setTitle("Layouts");
		setSize(new Dimension(500, 400));
		setDefaultCloseOperation(3);
		setLocationRelativeTo(null);

		getContentPane().setLayout(new BorderLayout());
		this.combobox = new JComboBox();
		ArrayList<String> opciones = new ArrayList<>();

		opciones.add("BorderLayout");
		opciones.add("FlowLayout");
		opciones.add("GridLayout");
		opciones.add("GridBagLayout");
		opciones.add("TabbedPane");
		opciones.add("SplitPane");
		opciones.add("ScrollPane");

		for (String opcion : opciones) {
			combobox.addItem(opcion);
		}

		getContentPane().add(this.combobox, BorderLayout.NORTH);
		this.panelCarta = new JPanel();
		this.panelCarta.setLayout(new CardLayout());
		getContentPane().add(this.panelCarta, BorderLayout.CENTER);
		this.panelCarta.add(bl(), "BorderLayout");
		this.panelCarta.add(fl(), "FlowLayout");
		this.panelCarta.add(gl(), "GridLayout");
		this.panelCarta.add(gbl(), "GridBagLayout");
		this.panelCarta.add(tp(), "TabbedPane");
		this.panelCarta.add(sp(), "SplitPane");
		this.panelCarta.add(scp(), "ScrollPane");
	}

	private JPanel bl() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JButton boton = new JButton("Norte");
		panel.add(boton, BorderLayout.NORTH);
		JButton boton2 = new JButton("Este");
		panel.add(boton2, BorderLayout.EAST);
		return panel;
	}

	private JPanel fl() {
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		JLabel etiqueta;
		for (int i = 0; i < 17; i++) {
			etiqueta = new JLabel("Soy una etiqueta n�" + i + " ");
			panel.add(etiqueta);
		}
		return panel;
	}

	private JPanel gl() {
		JPanel panel = new JPanel();
		GridLayout layout = new GridLayout(4, 5);
		layout.setHgap(5);
		layout.setVgap(25);
		panel.setLayout(layout);
		JButton boton;
		for (int i = 0; i < 17; i++) {
			boton = new JButton("" + i);
			panel.add(boton);
		}
		return panel;
	}

	private JPanel gbl() {
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		JTextArea txt = new JTextArea("0");
		txt.setPreferredSize(new Dimension(100, 100));
		c.gridx = 0;
		c.gridy = 1;
		panel.add(txt, c);
		JButton boton = new JButton("Bot�n");
		c.gridx = 0;
		c.gridy = 0;
		c.fill = GridBagConstraints.HORIZONTAL;
		panel.add(boton, c);
		JButton boton1 = new JButton("Si a todo");
		boton1.setPreferredSize(new Dimension(100, 100));
		c.gridx = 1;
		c.gridy = 1;
		panel.add(boton1, c);
		return panel;
	}

	private JPanel tp() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JTabbedPane pestanas = new JTabbedPane(JTabbedPane.BOTTOM);
		panel.add(pestanas, BorderLayout.CENTER);
		pestanas.addTab("BorderLayout", bl());
		pestanas.addTab("Flow Layout", fl());
		pestanas.addTab("GridBag Layout", gbl());
		return panel;
	}

	private JPanel sp() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		split.setLeftComponent(fl());
		split.setRightComponent(bl());
		panel.add(split, BorderLayout.CENTER);
		return panel;
	}

	private JPanel scp() {
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		txt = new JTextArea("Texto por defecto.");
		JScrollPane scroll = new JScrollPane(txt);
		panel.add(scroll, BorderLayout.CENTER);
		return panel;
	}
}

package main;

public class Graficos {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Vista vista = new Vista();
		vista.setVisible(true);
		Controlador controlador = new Controlador(vista);
	}
}
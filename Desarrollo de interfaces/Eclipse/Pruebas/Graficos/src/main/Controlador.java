package main;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener {
	private Vista vista;

	public Controlador(Vista vista) {
		this.vista = vista;
		anyadirListeners();
	}

	private void anyadirListeners() {
		vista.comboBox.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String key = vista.comboBox.getSelectedItem().toString();
		CardLayout layout = (CardLayout) vista.panel.getLayout();
		layout.show(vista.panel, key);

	}
}

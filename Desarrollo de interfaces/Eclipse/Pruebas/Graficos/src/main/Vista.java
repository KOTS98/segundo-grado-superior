package main;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

@SuppressWarnings("serial")
public class Vista extends JFrame {
	@SuppressWarnings("rawtypes")
	public JComboBox comboBox;
	public JPanel panel;

	public Vista() {
		init();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void init() {
		setDefaultCloseOperation(3);
		setLocationRelativeTo(null);

		Container contenedor = getContentPane();
		contenedor.setLayout(new BorderLayout());

		comboBox = new JComboBox();
		this.comboBox.setModel(new DefaultComboBoxModel(new String[] { "Gr�fico de quesitos", "Gr�fico de barras" }));
		contenedor.add(comboBox, BorderLayout.NORTH);

		panel = new JPanel();
		panel.setLayout(new CardLayout());
		contenedor.add(panel, BorderLayout.CENTER);

		panel.add(graficoQuesito(), "Gr�fico de quesitos");
		panel.add(graficoBarras(), "Gr�fico de barras");
		pack();
	}

	private JPanel graficoQuesito() {
		JPanel panel = new JPanel();
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Linux", 2.9);
		dataset.setValue("Mac", 2.0);
		dataset.setValue("Windows", 5.1);
		dataset.setValue("Android", 1.7);
		JFreeChart chart = ChartFactory.createPieChart3D("Sistemas Operativos", dataset, true, true, false);
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.ANTICLOCKWISE);
		plot.setForegroundAlpha(0.5f);

		ChartPanel chartPanel = new ChartPanel(chart);
		panel.add(chartPanel);

		return panel;
	}

	private JPanel graficoBarras() {
		JPanel panel = new JPanel();
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.setValue(2.9, "Uso", "Linux");
		dataset.setValue(2.0, "Uso", "Mac");
		dataset.setValue(5.1, "Uso", "Windows");
		dataset.setValue(1.7, "Uso", "Android");

		JFreeChart chart = ChartFactory.createBarChart("Sistemas Operativos", "Sistemas operativos", "Uso", dataset,
				PlotOrientation.VERTICAL, true, true, false);

		ChartPanel chartPanel = new ChartPanel(chart);
		panel.add(chartPanel);

		return panel;
	}
}

package view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

public class View {

	private JFrame ventana;
	private Container contenedor;
	private JTree arbol;
	private DefaultMutableTreeNode principal;
	private DefaultMutableTreeNode sub1;
	private DefaultMutableTreeNode sub2;
	private JMenuBar barraMenu;

	public View() {
		ventana = new JFrame();
		ventana.setTitle("T�tulo de la ventana");
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setSize(450, 500);
		ventana.setLocationRelativeTo(null);
		ventana.setVisible(true);
		contenedor = ventana.getContentPane();
		contenedor.setLayout(new BorderLayout());
		principal = new DefaultMutableTreeNode("Colecciones");
		sub1 = new DefaultMutableTreeNode("Peliculas");
		principal.add(sub1);
		sub2 = new DefaultMutableTreeNode("Pel�cula1");
		sub1.add(sub2);

		sub2 = new DefaultMutableTreeNode("Pel�cula2");
		sub1.add(sub2);

		sub2 = new DefaultMutableTreeNode("Pel�cula3");
		sub1.add(sub2);

		sub2 = new DefaultMutableTreeNode("Pel�cula4");
		sub1.add(sub2);

		sub1 = new DefaultMutableTreeNode("Libros");
		principal.add(sub1);

		sub2 = new DefaultMutableTreeNode("Libro");
		sub1.add(sub2);

		sub2 = new DefaultMutableTreeNode("Libro2");
		sub1.add(sub2);

		sub2 = new DefaultMutableTreeNode("Libro3");
		sub1.add(sub2);

		arbol = new JTree(principal);
		contenedor.add(arbol, BorderLayout.EAST);

		JMenu archivo = new JMenu("Archivo");
		JMenu editar = new JMenu("Editar");
		JMenuItem nuevo = new JMenuItem("Nuevo");
		JMenuItem abrir = new JMenuItem("Abrir");
		JMenuItem ver = new JMenuItem("Ver");
		JMenuItem copiar = new JMenuItem("Copiar");
		JMenuItem pegar = new JMenuItem("Pegar");
		JMenuItem borrar = new JMenuItem("Borrar", KeyEvent.VK_R);
		archivo.add(nuevo);
		archivo.add(abrir);

		JMenu guardarComo = new JMenu("Guardar como");
		JMenuItem doc = new JMenuItem(".doc");
		guardarComo.add(doc);
		JMenuItem pdf = new JMenuItem(".pdf");
		guardarComo.add(pdf);
		JMenuItem txt = new JMenuItem(".txt");
		guardarComo.add(txt);
		archivo.add(guardarComo);

		archivo.add(ver);
		editar.add(copiar);
		editar.add(pegar);
		editar.add(borrar);
		barraMenu = new JMenuBar();
		barraMenu.add(archivo);
		barraMenu.add(editar);
		ventana.setJMenuBar(barraMenu);
	}
}

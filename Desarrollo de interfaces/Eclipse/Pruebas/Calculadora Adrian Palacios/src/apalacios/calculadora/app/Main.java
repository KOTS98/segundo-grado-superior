package apalacios.calculadora.app;

import apalacios.calculadora.view.Controlador;
import apalacios.calculadora.view.View;

public class Main {

	public static void main(String[] args) {
		Controlador controlador = new Controlador(new View());
		controlador.generarListeners();
	}
}

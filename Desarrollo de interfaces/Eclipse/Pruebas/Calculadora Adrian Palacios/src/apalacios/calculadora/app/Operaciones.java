package apalacios.calculadora.app;

public class Operaciones {
	
	public static String sumar(int num1, int num2) {
		return String.valueOf(num1 + num2);
	}
	
	public static String restar(int num1, int num2) {
		return String.valueOf(num1 - num2);
	}
	
	public static String multiplicar(int num1, int num2) {
		return String.valueOf(num1 * num2);
	}
	
	public static String dividir(int num1, int num2) {
		return String.valueOf(num1 / num2);
	}
	
	public static String elevarCuadrado(int num) {
		return String.valueOf(Math.pow(num, 2));
	}
	
	public static String raizCuadrada(int num) {
		return String.valueOf(Math.sqrt(num));
	}
	
}

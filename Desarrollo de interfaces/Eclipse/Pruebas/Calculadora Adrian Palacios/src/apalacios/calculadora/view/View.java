package apalacios.calculadora.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class View {
	private JFrame ventana;
	private Container contenedor;
	private JPanel panel;
	private GridBagConstraints c;
	protected ArrayList<JButton> listaBotones;

	public View() {
		// Creación de la ventana.
		ventana = new JFrame();
		ventana.setTitle("Calculadora");
		ventana.setDefaultCloseOperation(3);
		ventana.setSize(new Dimension(320, 515));
		ventana.setMinimumSize(new Dimension(320, 415));
		ventana.setLocationRelativeTo(null);
		contenedor = ventana.getContentPane();
		contenedor.setLayout(new BorderLayout());

		// Creación del layout
		panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		c = new GridBagConstraints();
		contenedor.add(panel, BorderLayout.NORTH);

		// Creación del menú.
		JMenu ver = new JMenu("Ver");
		JMenu edicion = new JMenu("Edición");
		JMenu ayuda = new JMenu("Ayuda");
		JMenuBar barraMenu = new JMenuBar();
		barraMenu.add(ver);
		barraMenu.add(edicion);
		barraMenu.add(ayuda);
		ventana.setJMenuBar(barraMenu);

		// Display
		JTextArea txtDisplay = new JTextArea();

		// Botones numéricos.
		JButton btn0 = new JButton("0");
		JButton btn1 = new JButton("1");
		JButton btn2 = new JButton("2");
		JButton btn3 = new JButton("3");
		JButton btn4 = new JButton("4");
		JButton btn5 = new JButton("5");
		JButton btn6 = new JButton("6");
		JButton btn7 = new JButton("7");
		JButton btn8 = new JButton("8");
		JButton btn9 = new JButton("9");

		// Botones de función.
		JButton btnSum = new JButton("+");
		JButton btnRes = new JButton("-");
		JButton btnMul = new JButton("*");
		JButton btnDiv = new JButton("÷");
		JButton btnMasMenos = new JButton("±");
		JButton btnRaiz = new JButton("√");
		JButton btnPorciento = new JButton("%");
		JButton btnSobreX = new JButton("1/x");
		JButton btnIgual = new JButton("=");

		// Botones de memoria.
		JButton btnMC = new JButton("MC");
		JButton btnMR = new JButton("MR");
		JButton btnMS = new JButton("MS");
		JButton btnMSum = new JButton("M+");
		JButton btnMRes = new JButton("M-");

		// Otros botones.
		JButton btnC = new JButton("C");
		JButton btnCE = new JButton("CE");
		JButton btnRetroceso = new JButton("<-");
		JButton btnComa = new JButton(",");

		// Establecer actionCommand.

		// Lista de botones.
		listaBotones = new ArrayList<>();
		listaBotones.add(btn0);
		listaBotones.add(btn1);
		listaBotones.add(btn2);
		listaBotones.add(btn3);
		listaBotones.add(btn4);
		listaBotones.add(btn5);
		listaBotones.add(btn6);
		listaBotones.add(btn7);
		listaBotones.add(btn8);
		listaBotones.add(btn9);
		listaBotones.add(btnSum);
		listaBotones.add(btnRes);
		listaBotones.add(btnMul);
		listaBotones.add(btnDiv);
		listaBotones.add(btnMasMenos);
		listaBotones.add(btnRaiz);
		listaBotones.add(btnPorciento);
		listaBotones.add(btnSobreX);
		listaBotones.add(btnIgual);
		listaBotones.add(btnMC);
		listaBotones.add(btnMR);
		listaBotones.add(btnMS);
		listaBotones.add(btnMSum);
		listaBotones.add(btnMRes);
		listaBotones.add(btnC);
		listaBotones.add(btnCE);
		listaBotones.add(btnRetroceso);
		listaBotones.add(btnComa);

		// Action command
		for (JButton boton : listaBotones) {
			boton.setActionCommand(boton.getText());
		}

		// Posicionar display
		txtDisplay.setPreferredSize(new Dimension(270, 80));
		txtDisplay.setFont(new Font("Terminal", 0, 30));
		txtDisplay.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		txtDisplay.setEditable(false);
		txtDisplay.setText("0");
		Border border = BorderFactory.createLineBorder(Color.BLACK);
		txtDisplay
				.setBorder(BorderFactory.createCompoundBorder(border, BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		c.gridy = 0;
		c.gridwidth = 5;
		c.fill = GridBagConstraints.HORIZONTAL;
		panel.add(txtDisplay, c);
		c.gridwidth = 1;

		// Posicionar botones
		Dimension dimBtnNormal = new Dimension(50, 50);

		btnMC.setPreferredSize(dimBtnNormal);
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnMC, c);

		btnMR.setPreferredSize(dimBtnNormal);
		c.gridx = 1;
		c.gridy = 2;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnMR, c);

		btnMS.setPreferredSize(dimBtnNormal);
		c.gridx = 2;
		c.gridy = 2;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnMS, c);

		btnMSum.setPreferredSize(dimBtnNormal);
		c.gridx = 3;
		c.gridy = 2;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnMSum, c);

		btnMRes.setPreferredSize(dimBtnNormal);
		c.gridx = 4;
		c.gridy = 2;
		c.insets = new Insets(10, 0, 0, 0);
		panel.add(btnMRes, c);

		btnRetroceso.setPreferredSize(dimBtnNormal);
		c.gridx = 0;
		c.gridy = 3;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnRetroceso, c);

		btnCE.setPreferredSize(dimBtnNormal);
		c.gridx = 1;
		c.gridy = 3;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnCE, c);

		btnC.setPreferredSize(dimBtnNormal);
		c.gridx = 2;
		c.gridy = 3;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnC, c);

		btnMasMenos.setPreferredSize(dimBtnNormal);
		c.gridx = 3;
		c.gridy = 3;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnMasMenos, c);

		btnRaiz.setPreferredSize(dimBtnNormal);
		c.gridx = 4;
		c.gridy = 3;
		c.insets = new Insets(10, 0, 0, 0);
		panel.add(btnRaiz, c);

		btn7.setPreferredSize(dimBtnNormal);
		c.gridx = 0;
		c.gridy = 4;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn7, c);

		btn8.setPreferredSize(dimBtnNormal);
		c.gridx = 1;
		c.gridy = 4;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn8, c);

		btn9.setPreferredSize(dimBtnNormal);
		c.gridx = 2;
		c.gridy = 4;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn9, c);

		btnDiv.setPreferredSize(dimBtnNormal);
		c.gridx = 3;
		c.gridy = 4;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnDiv, c);

		btnPorciento.setPreferredSize(dimBtnNormal);
		c.gridx = 4;
		c.gridy = 4;
		c.insets = new Insets(10, 0, 0, 0);
		panel.add(btnPorciento, c);

		btn4.setPreferredSize(dimBtnNormal);
		c.gridx = 0;
		c.gridy = 5;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn4, c);

		btn5.setPreferredSize(dimBtnNormal);
		c.gridx = 1;
		c.gridy = 5;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn5, c);

		btn6.setPreferredSize(dimBtnNormal);
		c.gridx = 2;
		c.gridy = 5;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn6, c);

		btnMul.setPreferredSize(dimBtnNormal);
		c.gridx = 3;
		c.gridy = 5;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnMul, c);

		btnSobreX.setPreferredSize(dimBtnNormal);
		c.gridx = 4;
		c.gridy = 5;
		c.insets = new Insets(10, 0, 0, 0);
		panel.add(btnSobreX, c);

		btn1.setPreferredSize(dimBtnNormal);
		c.gridx = 0;
		c.gridy = 6;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn1, c);

		btn2.setPreferredSize(dimBtnNormal);
		c.gridx = 1;
		c.gridy = 6;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn2, c);

		btn3.setPreferredSize(dimBtnNormal);
		c.gridx = 2;
		c.gridy = 6;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn3, c);

		btnRes.setPreferredSize(dimBtnNormal);
		c.gridx = 3;
		c.gridy = 6;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnRes, c);

		btnIgual.setPreferredSize(new Dimension(50, 110));
		c.gridx = 4;
		c.gridy = 6;
		c.gridheight = 2;
		c.insets = new Insets(10, 0, 0, 0);
		panel.add(btnIgual, c);
		c.gridheight = 1;

		btn0.setPreferredSize(new Dimension(110, 50));
		c.gridx = 0;
		c.gridy = 7;
		c.gridwidth = 2;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btn0, c);
		c.gridwidth = 1;

		btnComa.setPreferredSize(dimBtnNormal);
		c.gridx = 2;
		c.gridy = 7;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnComa, c);

		btnSum.setPreferredSize(dimBtnNormal);
		c.gridx = 3;
		c.gridy = 7;
		c.insets = new Insets(10, 0, 0, 10);
		panel.add(btnSum, c);

		// Hacer visibles los elementos.
		ventana.setVisible(true);
	}
}

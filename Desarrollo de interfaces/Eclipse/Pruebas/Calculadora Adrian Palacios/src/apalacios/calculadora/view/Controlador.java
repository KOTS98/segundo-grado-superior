package apalacios.calculadora.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Controlador implements ActionListener {
	private View vista;

	public Controlador(View vista) {
		this.vista = vista;
	}

	public void generarListeners() {
		for (JButton boton : vista.listaBotones) {
			boton.addActionListener(this);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "0":
			System.out.println("hey");
			break;
		case "1":
			System.out.println("hey1");
			break;
		case "2":
			System.out.println("hey2");
			break;
		case "btn3":

			break;
		case "btn4":

			break;
		case "btn5":

			break;
		case "btn6":

			break;
		case "btn7":

			break;
		case "btn8":

			break;
		case "btn9":

			break;
		}
	}
}

package elements;

public class Cliente {
	private int id;
	private String nombre;
	private String consola;

	public Cliente (int id, String nombre, String consola) {
		this.id = id;
		this.consola = consola;
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getConsola() {
		return consola;
	}

	public void setConsola(String consola) {
		this.consola = consola;
	}
}
package elements;

public class Consolas {
	private int id;
	private String nombre;
	private int ventas;
	
	public Consolas(int id, String nombre, int ventas) {
		this.id = id;
		this.nombre = nombre;
		this.ventas = ventas;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getVentas() {
		return ventas;
	}

	public void setVentas(int ventas) {
		this.ventas = ventas;
	}
}

package main;

import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Vista extends JFrame {
	
	ArrayList<JButton> botones;
	
	public Vista() {
		init();
		this.setVisible(true);
		this.pack();
	}
	
	private void init() {
		botones = new ArrayList<>();
		
		JButton primerInforme = new JButton("Primer informe");
		primerInforme.setActionCommand("PrimerInforme");
		botones.add(primerInforme);
		
		JButton segundoInforme = new JButton("Segundo informe");
		segundoInforme.setActionCommand("SegundoInforme");
		botones.add(segundoInforme);
		
		JButton tercerInforme = new JButton("Tercer informe");
		tercerInforme.setActionCommand("TercerInforme");
		botones.add(tercerInforme);
		
		JButton cuartoInforme = new JButton("Cuarto informe");
		cuartoInforme.setActionCommand("CuartoInforme");
		botones.add(cuartoInforme);
		
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(primerInforme);
		this.getContentPane().add(segundoInforme);
		this.getContentPane().add(tercerInforme);
		this.getContentPane().add(cuartoInforme);
	}
}

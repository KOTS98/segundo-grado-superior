package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
	
	public Connection conectar() {
		Connection conexion = null;
		String url = "jdbc:mysql://localhost:3306/pokimon?user=root&password=";
		try {
			conexion = DriverManager.getConnection(url);
		} catch(SQLException ex) {
			System.out.println("Error de SQL");
		}
		return conexion;
	}
}

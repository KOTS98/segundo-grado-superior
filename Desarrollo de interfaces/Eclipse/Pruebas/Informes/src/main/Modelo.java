package main;

import java.util.ArrayList;

import elements.Cliente;
import elements.Consolas;
import elements.Producto;

public class Modelo {
	ArrayList<Producto> productos;
	ArrayList<Cliente> clientes;
	ArrayList<Consolas> consolas;
	
	public Modelo() {
		this.productos = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			productos.add(new Producto(i, "Nombre" + i, "Descripci�n" + i, i * 2));
		}
		
		this.clientes = new ArrayList<Cliente>();
		
		for(int i = 0; i <= 20; i++) {
			clientes.add(new Cliente(i, "Nombre" + i, "Switch"));
		}

		for(int i = 21; i <= 42; i++) {
			clientes.add(new Cliente(i, "Nombre" + i, "PS4"));
		}

		for(int i = 43; i <= 75; i++) {
			clientes.add(new Cliente(i, "Nombre" + i, "Xbox One"));
		}
		
		for(int i = 21; i <= 36; i++) {
			clientes.add(new Cliente(i, "Nombre" + i, "Tel�fono"));
		}
		
		this.consolas = new ArrayList<Consolas>();
		consolas.add(new Consolas(1, "Switch", 42));
		consolas.add(new Consolas(2, "PS4", 15));
		consolas.add(new Consolas(3, "Xbox One", 30));
		consolas.add(new Consolas(4, "Tel�fono", 63));
	}
}
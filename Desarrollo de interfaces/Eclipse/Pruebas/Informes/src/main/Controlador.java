package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.HashMap;

import javax.swing.JButton;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class Controlador implements ActionListener{
	
	Vista vista;
	Modelo modelo;
	Connection conexion;
	
	public Controlador(Vista vista, Modelo modelo) {
		this.vista = vista;
		this.modelo = modelo;
		this.conexion = new Database().conectar();
		anyadirListeners();
	}
	
	private void anyadirListeners() {
		for (JButton boton : vista.botones) {
			boton.addActionListener(this);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		case "PrimerInforme":
			pintarPrimerInforme();
			break;
			
		case "SegundoInforme":
			pintarSegundoInforme();
			break;
			
		case "TercerInforme":
			pintarTercerInforme();
			break;
		case "CuartoInforme":
			pintarCuartoInforme();
		}
	}
	
	private void pintarPrimerInforme() {
		//JasperPrint (Encargado de renderizar/pintar el informe)
		JasperPrint jasperPrint;
		
		try {
			//JasperFillManager (Encargado de rellenar los informes)
			//.fillReport (Rellenar el informe)
			jasperPrint = JasperFillManager.fillReport("Cherry_Table_based.jasper", null, conexion);
			
			//JPRdfExporter (Encargado de exportar a PDF)
			JRPdfExporter exportador = new JRPdfExporter();
			
			//De donde recibe los datos a exportar
			exportador.setExporterInput(new SimpleExporterInput(jasperPrint));
			
			//Donde genera el archivo
			exportador.setExporterOutput(new SimpleOutputStreamExporterOutput("Seccion31 - Informe.pdf"));
			
			//Configuración para exportar a PDF
			SimplePdfExporterConfiguration configuracion = new SimplePdfExporterConfiguration();
			
			//Se establece la configuración al exportador
			exportador.setConfiguration(configuracion);
			
			//Exporta el archivo
			exportador.exportReport();
			
			//Visualizar el informe (el booleano que recibe indica si se debe cerrar la aplicación al cerrar
			//el informe)
			JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
			jasperViewer.setVisible(true);
			
			
		} catch (JRException e) {
			System.out.println("Error al generar el informe");
			e.printStackTrace();
		}
	}

	private void pintarSegundoInforme() {
		//JasperFillManager (Encargado de rellenar los informes)
		//.fillReport (Rellenar el informe)
		JasperPrint jasperPrint;
		try {
			HashMap<String, Object> parametros = new HashMap<>();
			parametros.put("titulo", "Informe de productos");
			parametros.put("autor", "Adrián Palacios");
			jasperPrint = JasperFillManager.fillReport("InformeSencilloProductos.jasper", parametros,
					new JRBeanCollectionDataSource(modelo.productos));
			//Visualizar el informe (el booleano que recibe indica si se debe cerrar la aplicación al cerrar
			//el informe)
			JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
			jasperViewer.setVisible(true);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}
	
	private void pintarTercerInforme() {
		//JasperFillManager (Encargado de rellenar los informes)
		//.fillReport (Rellenar el informe)
		JasperPrint jasperPrint;
		try {
			jasperPrint = JasperFillManager.fillReport("Ventas2.jasper", null,
					new JRBeanCollectionDataSource(modelo.consolas));
			//Visualizar el informe (el booleano que recibe indica si se debe cerrar la aplicación al cerrar
			//el informe)
			JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
			jasperViewer.setVisible(true);
		} catch (JRException e) {
			e.printStackTrace();
		}
	}
	
	private void pintarCuartoInforme() {
		//JasperPrint (Encargado de renderizar/pintar el informe)
				JasperPrint jasperPrint;

				try {
					//JasperFillManager (Encargado de rellenar los informes)
					//.fillReport (Rellenar el informe)
					jasperPrint = JasperFillManager.fillReport("Motivadores2.jasper", null, conexion);
					
					//Visualizar el informe (el booleano que recibe indica si se debe cerrar la aplicación al cerrar
					//el informe)
					JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
					jasperViewer.setVisible(true);
					
				} catch (JRException e) {
					System.out.println("Error al generar el informe");
					e.printStackTrace();
				}
	}
}

package view;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class View {

	private JFrame ventana;
	private Container contenedor;
	private JLabel etiqueta;
	private JLabel etiqueta2;
	private JLabel etiqueta3;
	private JButton boton;
	private JCheckBox check;

	public View() {
		ventana = new JFrame();
		ventana.setTitle("T�tulo de la ventana");
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setSize(450, 500);
		ventana.setLocationRelativeTo(null);
		ventana.setVisible(true);
		contenedor = ventana.getContentPane();
		contenedor.setLayout(new BorderLayout());
		etiqueta = new JLabel("Contenido de la etiqueta");
		contenedor.add(etiqueta, BorderLayout.EAST);
		etiqueta2 = new JLabel("Contenido de la etiqueta 2");
		contenedor.add(etiqueta2, BorderLayout.SOUTH);
		etiqueta3 = new JLabel("Contenido de la etiqueta 3");
		contenedor.add(etiqueta3, BorderLayout.WEST);
		boton = new JButton("Texto del boton");
		contenedor.add(boton, BorderLayout.NORTH);
		check = new JCheckBox("Texto del checkbox");
		contenedor.add(check, BorderLayout.CENTER);
	}
}

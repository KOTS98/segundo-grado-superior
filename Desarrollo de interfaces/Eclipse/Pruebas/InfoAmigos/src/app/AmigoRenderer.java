package app;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import elements.Amigo;

@SuppressWarnings({ "serial", "rawtypes" })
public class AmigoRenderer extends JPanel implements ListCellRenderer {

	@Override
	public Component getListCellRendererComponent(JList list, Object valores, int index, boolean isSelected,
			boolean cellHasFocus) {
		this.removeAll();
		this.setLayout(new BorderLayout());
		JPanel informacion = new JPanel();
		informacion.setLayout(new BoxLayout(informacion, BoxLayout.Y_AXIS));

		if (isSelected) {
			setBackground(list.getSelectionBackground());
			setForeground(list.getSelectionForeground());
			informacion.setBackground(list.getSelectionBackground());
			informacion.setForeground(list.getSelectionForeground());

		} else {
			setBackground(list.getBackground());
			setForeground(list.getForeground());
			informacion.setBackground(list.getBackground());
			informacion.setForeground(list.getForeground());
		}

		Amigo amigo = (Amigo) valores;
		ImageIcon icon = amigo.getFoto();
		JLabel icono = new JLabel(icon);
		JLabel nombre = new JLabel(amigo.getNombre());
		JLabel direccion = new JLabel(amigo.getDireccion());
		this.add(icono, BorderLayout.WEST);
		informacion.add(nombre);
		informacion.add(direccion);
		this.add(informacion, BorderLayout.CENTER);
		return this;
	}

}
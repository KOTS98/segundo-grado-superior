package app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener {
	private Vista vista = new Vista();

	public Controlador(Vista vista) {
		this.vista = vista;
		anyadirListeners();
	}

	public void anyadirListeners() {
		vista.btnAnyadir.addActionListener(this);
		vista.btnEliminar.addActionListener(this);
		vista.btnConfirmar.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "anyadir":
			vista.ventanaAnyadir();
			break;
		case "eliminar":
			vista.eliminarAmigo();
			break;
		case "confirmar":
			vista.anyadirAmigo();
			vista.cerrarVentana();
			break;
		}
	}
}

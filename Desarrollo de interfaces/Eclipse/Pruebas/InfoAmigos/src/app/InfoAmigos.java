package app;

public class InfoAmigos {
	public static void main(String[] args) {
		Vista vista = new Vista();
		Controlador controlador = new Controlador(vista);
		vista.setLocationRelativeTo(null);
		vista.pack();
		vista.setVisible(true);
	}
}

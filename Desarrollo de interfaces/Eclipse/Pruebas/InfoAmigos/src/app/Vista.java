package app;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import elements.Amigo;

public class Vista extends JFrame {
	JList amigos;
	JLabel texto;
	JButton btnEliminar, btnAnyadir;
	JButton btnConfirmar;
	Container contenedor;
	private DefaultListModel<Amigo> listModel;
	private JTextField txtNombre;
	private JTextField txtDireccion;
	JDialog ventana;
	ImageIcon defecto;

	public Vista() {
		init();
	}

	private void init() {
		contenedor = getContentPane();
		contenedor.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		texto = new JLabel("Lista de amigos                                    ");
		contenedor.add(texto, c);
		defecto = new ImageIcon(Vista.class.getResource("/javax/swing/plaf/metal/icons/ocean/error.png"));
		Amigo amigo1 = new Amigo("Juan", "Juan@gmail.com", defecto);
		Amigo amigo2 = new Amigo("Antonia", "Antonia@gmail.com", defecto);
		this.listModel = new DefaultListModel();
		listModel.addElement(amigo1);
		listModel.addElement(amigo2);
		amigos = new JList(listModel);
		AmigoRenderer cellRenderer = new AmigoRenderer();
		amigos.setCellRenderer(cellRenderer);
		JScrollPane scroll = new JScrollPane(amigos, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.BOTH;
		contenedor.add(scroll, c);

		btnAnyadir = new JButton("A�adir");
		btnAnyadir.setActionCommand("anyadir");
		c.gridy = 2;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.SOUTHEAST;
		contenedor.add(btnAnyadir, c);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.setActionCommand("eliminar");
		c.gridx = 1;
		contenedor.add(btnEliminar, c);

		txtNombre = new JTextField(40);
		txtDireccion = new JTextField(40);
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setActionCommand("confirmar");
	}

	public void eliminarAmigo() {
		if (amigos.getSelectedIndex() == -1) {

		} else {
			int index = amigos.getSelectedIndex();
			this.listModel.remove(index);
			int size = listModel.getSize();
			if (size == 0) {
				btnEliminar.setEnabled(false);
			} else {
				if (index == listModel.getSize()) {
					index--;
				}
				amigos.setSelectedIndex(index);
				amigos.ensureIndexIsVisible(index);
			}
		}
	}

	public void ventanaAnyadir() {
		ventana = new JDialog(this, "A�adir amigo", true);
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		JLabel nombre = new JLabel("Nombre");
		panel.add(nombre, c);
		c.gridwidth = GridBagConstraints.REMAINDER;
		panel.add(txtNombre, c);
		c.gridwidth = GridBagConstraints.RELATIVE;
		JLabel direccion = new JLabel("Direcci�n");
		panel.add(direccion, c);
		c.gridwidth = GridBagConstraints.REMAINDER;
		panel.add(txtDireccion, c);
		panel.add(btnConfirmar, c);
		ventana.getContentPane().add(panel);
		ventana.pack();
		ventana.setVisible(true);
	}

	public void cerrarVentana() {
		this.ventana.dispose();
	}

	public void anyadirAmigo() {
		String nombre = txtNombre.getText();
		String direccion = txtDireccion.getText();
		ImageIcon icono = defecto;
		Amigo amigo = new Amigo(nombre, direccion, icono);
		listModel.addElement(amigo);
		btnEliminar.setEnabled(true);
	}
}

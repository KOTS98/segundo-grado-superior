package elements;

import javax.swing.ImageIcon;

public class Amigo {
	private String nombre;
	private String direccion;
	private ImageIcon foto;

	public Amigo() {
	};

	public Amigo(String nombre, String direccion, ImageIcon foto) {
		this.nombre = nombre;
		this.direccion = direccion;
		this.foto = foto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public ImageIcon getFoto() {
		return foto;
	}

	public void setFoto(ImageIcon foto) {
		this.foto = foto;
	}
}
package tablero;

import java.awt.GridLayout;

import javax.swing.JFrame;
public class Vista extends JFrame{
	int filas = 8;
	int columnas = 10;
	
	public Vista() {
		init();
		this.pack();
		this.setVisible(true);
	}
	
	public void init() {
		boolean b;
		this.getContentPane().setLayout(new GridLayout(filas, columnas));
		for(int i = 0; i < filas; i++) {
			for(int j = 0; j < columnas; j++) {
				b = false;
				if(Math.random() < 0.2) {
					b = true;
				}
				this.getContentPane().add(new Casilla(i, j, b));
			}
		}
	}
}

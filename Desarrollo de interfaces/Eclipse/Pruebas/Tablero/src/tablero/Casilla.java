package tablero;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Casilla extends JButton implements ActionListener{
	int x;
	int y;
	boolean bomba;
	
	public Casilla(int x, int y, boolean bomba) {
		this.x = x;
		this.y = y;
		this.bomba = bomba;
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(bomba) {
			this.setText("B");
		}
	}
}

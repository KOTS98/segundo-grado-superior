package com.apalacios.seccion31.app;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.border.Border;

/**
 * Clase que contiene todas las constantes del programa y se encarga de cargar
 * el archivo de preferencias "properties.conf"
 * 
 * @author Adri�n Palacios
 *
 */
class Constantes {
	Properties conf = new Properties();

	// Descripciones de productos
	static String DESC_P1 = " La puesta de sol samariana es una bebida que inicialmente es clara, "
			+ "pero cuando golpeas el borde del vaso, aparece un remolino de luminiscencia naranja "
			+ "y dorada en el l�quido, hasta que toda la bebida se convierte en un dorado opaco no " + "luminiscente.";
	static String DESC_P2 = " Una bebida popular, esta potente bebida se sirve en una botella de "
			+ "cuello torcido. Fue uno de los favoritos entre los equipos de la Flota Estelar en "
			+ "la d�cada de 2260.";
	static String DESC_P3 = " La cerveza romulana es una bebida alcoh�lica altamente intoxicante de "
			+ "origen romulano con un color azul caracter�stico (que var�a desde un azul cielo p�lido "
			+ "hasta un azul oscuro de medianoche). El doctor Leonard McCoy brome� diciendo que lo usa "
			+ "con \"fines medicinales\".";
	static String DESC_P4 = " Una bebida espesa y marr�n, de naturaleza alcoh�lica y de origen cardassiano. "
			+ "Se trata de una bebida de sabor adquirido.";
	static String DESC_P5 = " Una bebida de origen Vulcano, caliente, ins�pida y no alcoholica, pero muy"
			+ "rica en nutrientes. Excelente para recuperarte en una ma�ana de resaca.";

	// Dimensiones
	static Dimension IMG_SIZE = new Dimension(150, 150);
	static Dimension BTN_SIZE = new Dimension(150, 30);

	// Insets (Margenes)
	static Insets IMG_SPACE = new Insets(20, 20, 0, 40);
	static Insets BTN_SPACE = new Insets(30, 20, 0, 40);
	static Insets LBL_SPACE = new Insets(80, 20, 0, 40);

	// Fuentes
	static Font BTN_FNT = new Font("Impact", 0, 30);

	// Colores
	static Color TEXTO_CLR;
	static Color FONDO_CLR;
	static Color PANTALLA_CLR;
	static Color CONTENEDOR_CLR;

	// Bordes
	static Border STNDR_BRD = BorderFactory.createLineBorder(Color.BLACK);
	static Border SELECT_BRD = BorderFactory.createLineBorder(Color.YELLOW, 5);

	/**
	 * Constructor de la clase, ejecuta los metodos para crear el archivo de
	 * configuraci�n y para cargar los datos desde ese mismo archivo
	 * 
	 * @throws IOException Excepci�n de lectura / escritura de datos
	 */
	Constantes() throws IOException {
		crearPreferencias();
		cargarPreferencias();
	}

	/**
	 * Determina si existe o no un archivo de configuraci�n en la ruta del programa,
	 * en caso de no encontrarlo, crea uno e inicializa sus par�metros
	 */
	private void crearPreferencias() {
		if (!new File("properties.conf").exists()) {
			try {
				conf.setProperty("tema", "claro");
				conf.setProperty("usuario", "admin");
				conf.setProperty("pass", "admin");
				FileWriter fw;
				fw = new FileWriter("properties.conf");
				conf.store(fw, "");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Lee desde el archivo de configuraci�n el tema (paleta de colores) que debe
	 * utilizarse
	 * 
	 * @throws IOException Excepci�n de lectura / escritura de datos
	 */
	private void cargarPreferencias() throws IOException {
		FileReader fr = new FileReader("properties.conf");
		conf.load(fr);
		if (conf.getProperty("tema").equalsIgnoreCase("claro")) {
			cargarTemaClaro();
		} else {
			cargarTemaOscuro();
		}
	}

	/**
	 * Establece los colores claros para el programa
	 */
	private void cargarTemaClaro() {
		TEXTO_CLR = Color.BLACK;
		FONDO_CLR = Color.WHITE;
		PANTALLA_CLR = Color.LIGHT_GRAY;
		CONTENEDOR_CLR = Color.GRAY;
	}

	/**
	 * Establece los colores oscuros para el programa
	 */
	private void cargarTemaOscuro() {
		TEXTO_CLR = Color.WHITE;
		FONDO_CLR = Color.BLACK;
		PANTALLA_CLR = Color.DARK_GRAY;
		CONTENEDOR_CLR = Color.GRAY;
	}
}

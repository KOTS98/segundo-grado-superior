package com.apalacios.seccion31.app;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import base.Oferta;
import base.Producto;
import base.ProductoStats;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Clase que se encarga de recibir las pulsaciones de botones y actuar en
 * consecuencia
 * 
 * @author Adri�n Palacios
 */
public class Controlador implements ActionListener, ListSelectionListener, WindowListener {
	private Vista vista;
	private JasperPrint informeRecibo;
	private JasperPrint informeOfertas;
	private JasperPrint informeEstadisticas;

	/**
	 * Constructor de la clase, instancia la vista (interfaz gr�fica) y llama al
	 * m�todo que a�ade los escuchadores de evento
	 * 
	 * @param vista Representa la interfaz gr�fica del programa
	 */
	Controlador(Vista vista) {
		this.vista = vista;
		anyadirListeners();
	}

	/**
	 * A�ade los escuchadores de evento (listeners) a los botones del interfaz,
	 * permitiendo as� reaccionar a las pulsaciones de dichos botones
	 */
	private void anyadirListeners() {

		// Vista principal, cargar datos al abrir, guardarlos al cerrar
		vista.addWindowListener(this);

		// Imagenes en las que se puede clicar para obtener una descripci�n del producto
		// seleccionado
		vista.btnImgProducto1.addActionListener(this);
		vista.btnImgProducto2.addActionListener(this);
		vista.btnImgProducto3.addActionListener(this);
		vista.btnImgProducto4.addActionListener(this);
		vista.btnImgProducto5.addActionListener(this);

		// Botones para a�adir el producto seleccionado al carrito de la compra
		vista.btnAdd1.addActionListener(this);
		vista.btnAdd2.addActionListener(this);
		vista.btnAdd3.addActionListener(this);
		vista.btnAdd4.addActionListener(this);
		vista.btnAdd5.addActionListener(this);

		// Boton para eliminar un producto del carrito
		vista.eliminarProducto.addActionListener(this);

		// Boton para hacer el check-out (pagar)
		vista.pagar.addActionListener(this);

		// Boton para generar el recibo
		vista.recibo.addActionListener(this);

		// Boton para generar el pdf del recibo
		vista.reciboPDF.addActionListener(this);

		// Lista de ofertas
		vista.listaOfertas.addListSelectionListener(this);
		vista.btnOfertasAdmin.addActionListener(this);
		vista.btnInformeOfertas.addActionListener(this);
		vista.btnOfertasPDF.addActionListener(this);

		// Estadisticas
		vista.btnInformeStats.addActionListener(this);
		vista.btnExportarStats.addActionListener(this);

		// Boton que llama al men� de opciones
		vista.btnMenuOpciones.addActionListener(this);
	}

	/**
	 * Determina cu�ndo se pulsa un bot�n y que bot�n ha sido pulsado, desp�es act�a
	 * en consecuencia
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {

		// Click en las imagenes, ofrece una descripci�n del producto seleccionado
		case "producto1":
			vista.descripcion.setText(Constantes.DESC_P1);
			break;

		case "producto2":
			vista.descripcion.setText(Constantes.DESC_P2);
			break;

		case "producto3":
			vista.descripcion.setText(Constantes.DESC_P3);
			break;

		case "producto4":
			vista.descripcion.setText(Constantes.DESC_P4);
			break;

		case "producto5":
			vista.descripcion.setText(Constantes.DESC_P5);
			break;

		// Botones para a�adir productos al carrito de la compra
		case "addProducto1":
			addProducto(vista.producto1);
			break;
		case "addProducto2":
			addProducto(vista.producto2);
			break;
		case "addProducto3":
			addProducto(vista.producto3);
			break;
		case "addProducto4":
			addProducto(vista.producto4);
			break;
		case "addProducto5":
			addProducto(vista.producto5);
			break;
		case "btnOfertasAdmin":
			@SuppressWarnings("unused")
			LoginDialog login = new LoginDialog(vista.dlmOfertas);
			break;
		case "eliminarProductoSelec":
			eliminarUnidad();
			break;
		case "pagar":
			eliminarTodos();
			break;
		case "recibo":
			generarRecibo();
			break;
		case "reciboPDF":
			exportarInformePDF(informeRecibo);
			break;
		case "opciones":
			@SuppressWarnings("unused")
			MenuOpcionesDialog dialog = new MenuOpcionesDialog();
			break;
		case "btnInformeOfertas":
			generarInformeOfertas();
			break;
		case "btnOfertasPDF":
			exportarInformePDF(informeOfertas);
			break;
		case "btnInformeStats":
			generarInformeStats();
			break;
		case "btnExportarStats":
			exportarInformePDF(informeEstadisticas);
			break;
		}
		vista.calcularPrecioTotal();
	}

	/**
	 * Detecta cambios en el elemento seleccionado en la lista de ofertas y
	 * actualiza los campos circundantes para mostrar los datos de la oferta
	 * seleccionada
	 */
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource().equals(vista.listaOfertas)) {
			vista.txtTituloOferta.setText(vista.dlmOfertas.get(vista.listaOfertas.getSelectedIndex()).getTitulo());
			vista.txtDescOferta.setText(vista.dlmOfertas.get(vista.listaOfertas.getSelectedIndex()).getDescripcion());
		}
	}

	/**
	 * A�ade una unidad al producto seleccionado, suma el precio del producto al
	 * contador de precio total en la pesta�a de "carrito", muestra el producto en
	 * la lista de productos del carrito y habilita los botones "pagar", "recibo", y
	 * "exportar" en la pesta�a de "carrito2
	 * 
	 * @param producto Representa el producto del cu�l se desea a�adir una unidad
	 */
	private void addProducto(Producto producto) {
		// Sumar uno al conteo de unidades (cantidad) del producto
		producto.agregarUnidad();
		// Sumar el precio unitario del producto al precio total
		vista.precioTotal += producto.getPrecio();
		// Comprobar si el producto cuenta con una �nica unidad.
		if (producto.getCantidad() == 1) {
			// A�adir el producto al DefaultListModel
			vista.dlmCarritoCompra.addElement(producto);
		}
		vista.pagar.setEnabled(true);
		vista.recibo.setEnabled(true);
		vista.reciboPDF.setEnabled(true);
	}

	/**
	 * Elimina una unidad del producto seleccionado, si el total de unidades de
	 * dicho producto es 0, lo elimina de la lista de productos en el carrito.
	 * Tambi�n actualiza el precio en la etiqueta de precio total
	 */
	private void eliminarUnidad() {
		// Comprobar que hay un elemento seleccionado
		if (vista.carritoCompra.getSelectedValue() != null) {
			// Comprobar si el n�mero de unidades del producto (cantidad), es mayor que 0
			if (vista.dlmCarritoCompra.get(vista.carritoCompra.getSelectedIndex()).getCantidad() > 0) {
				// Restar una unidad del producto y restar al precio total el precio unitario
				// del producto
				vista.dlmCarritoCompra.get(vista.carritoCompra.getSelectedIndex()).eliminarUnidad();
				vista.precioTotal -= vista.dlmCarritoCompra.get(vista.carritoCompra.getSelectedIndex()).getPrecio();
			}
			actualizarCarrito();
		}
		// Comprobar el precioTotal para determinar si hay alg�n producto en el carrito
		// de la compra.
		if (vista.precioTotal == 0.0) {
			vista.pagar.setEnabled(false);
			vista.recibo.setEnabled(false);
			vista.reciboPDF.setEnabled(false);
		}
	}

	/**
	 * Elimina todos los elementos del carrito de la compra, devuelve a 0 la
	 * cantidad de unidades de todos los productos, reinicia el contador de precio
	 * total y habilida los botones de "pagar", "recibo" y "exportar" en la pesta�a
	 * de "carrito"
	 */
	private void eliminarTodos() {
		// Recorrer el DefaultListModel que contiene los productos
		for (int i = 0; i < vista.dlmCarritoCompra.size(); i++) {
			// Establecer en 0 el n�mero de unidades (cantidad) de cada producto
			vista.dlmCarritoCompra.get(i).setCantidad(0);
		}
		// Eliminar todos los elementos del DefaultListModel
		vista.dlmCarritoCompra.removeAllElements();
		vista.listaProductos.clear();
		// Actualizar el precio total
		vista.precioTotal = 0;
		vista.pagar.setEnabled(false);
		vista.recibo.setEnabled(false);
		vista.reciboPDF.setEnabled(false);
	}

	/**
	 * Actualiza los elementos en el carrito de la compra y el precio total en la
	 * pesta�a de "carrito"
	 */
	private void actualizarCarrito() {
		// Recorrer el DefaultListModel que contiene los productos
		for (int i = 0; i < vista.dlmCarritoCompra.size(); i++) {
			// Comprobar si el n�mero de unidades del producto (cantidad), es igual a 0
			if (vista.dlmCarritoCompra.get(i).getCantidad() == 0) {
				// Eliminar el producto de la lista
				vista.dlmCarritoCompra.remove(i);
			}
		}
		// Repintar el precio y la lista de productos
		vista.precioTotalTxt.repaint();
		vista.carritoCompra.repaint();
	}

	/**
	 * Muestra por pantalla un informe con todos los datos de la compra a realizar
	 */
	private void generarRecibo() {
		for (int i = 0; i < vista.dlmCarritoCompra.getSize(); i++) {
			if (vista.dlmCarritoCompra.get(i).getCantidad() > 0) {
				for (int j = 0; j < vista.dlmCarritoCompra.get(i).getCantidad(); j++) {
					vista.listaProductos.add(vista.dlmCarritoCompra.get(i));
				}
			}
		}
		try {
			HashMap<String, Object> parametros = new HashMap<>();
			parametros.put("titulo", "SECCION 31");
			parametros.put("subtitulo", "Factura de venta");
			informeRecibo = JasperFillManager.fillReport("ReciboS31.jasper", parametros,
					new JRBeanCollectionDataSource(vista.listaProductos));
			JasperViewer jasperViewer = new JasperViewer(informeRecibo, false);
			jasperViewer.setVisible(true);
		} catch (JRException e) {
			e.printStackTrace();
		}
		vista.listaProductos.clear();
	}

	/**
	 * Muestra por pantalla un informe con los datos referentes a las ofertas
	 * actuales y a los productos que se ven afectados por dichas ofertas
	 */
	private void generarInformeOfertas() {
		for (int i = 0; i < vista.dlmOfertas.getSize(); i++) {
			vista.ofertasActuales.add(vista.dlmOfertas.get(i));
		}
		try {
			HashMap<String, Object> parametros = new HashMap<>();
			parametros.put("titulo", "SECCION 31");
			parametros.put("subtitulo", "Ofertas actuales");
			informeOfertas = JasperFillManager.fillReport("OfertasS31.jasper", parametros,
					new JRBeanCollectionDataSource(vista.ofertasActuales));
			JasperViewer jasperViewer = new JasperViewer(informeOfertas, false);
			jasperViewer.setVisible(true);
		} catch (JRException e) {
			e.printStackTrace();
		}
		vista.ofertasActuales.clear();
	}

	/**
	 * Muestra por pantalla un informe de estad�sticas de ventas de productos
	 */
	private void generarInformeStats() {
		ArrayList<ProductoStats> productoStats = new ArrayList<>();
		productoStats.add(new ProductoStats("Atardecer Samariano", 8, 29));
		productoStats.add(new ProductoStats("Brandy Sauriano", 6, 20));
		productoStats.add(new ProductoStats("Cerveza Romulana", 2.5f, 41));
		productoStats.add(new ProductoStats("Kanar", 2, 17));
		productoStats.add(new ProductoStats("T� Plomeek", 3, 12));
		try {
			HashMap<String, Object> parametros = new HashMap<>();
			parametros.put("titulo", "SECCION 31");
			parametros.put("subtitulo", "Informe de estad�sticas");
			informeEstadisticas = JasperFillManager.fillReport("EstadisticasS31.jasper", parametros,
					new JRBeanCollectionDataSource(productoStats));
			JasperViewer jasperViewer = new JasperViewer(informeEstadisticas, false);
			jasperViewer.setVisible(true);
		} catch (JRException e) {
			e.printStackTrace();
		}
		productoStats.clear();
	}

	/**
	 * Muestra un di�logo preguntando por la ruta de guardado deseada, y exporta (en
	 * la ruta seleccionada) el informe que reciba como par�metro a un archivo .pdf
	 * 
	 * @param informe Representa el informe que se desea exportar a .pdf
	 */
	private void exportarInformePDF(JasperPrint informe) {
		try {
			JFileChooser selector = new JFileChooser();
			if (selector.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				JRPdfExporter exportador = new JRPdfExporter();
				exportador.setExporterInput(new SimpleExporterInput(informe));
				exportador
						.setExporterOutput(new SimpleOutputStreamExporterOutput(selector.getSelectedFile().getPath()));
				SimplePdfExporterConfiguration configuracion = new SimplePdfExporterConfiguration();
				exportador.setConfiguration(configuracion);
				exportador.exportReport();
			}
		} catch (JRException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Se ejecuta al abrirse la ventana principal del programa, recupera todos los
	 * datos del programa de un archivo "datos.bin".
	 */
	@Override
	public void windowOpened(WindowEvent arg0) {
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(new FileInputStream("datos.bin"));
			@SuppressWarnings("unchecked")
			DefaultListModel<Oferta> ofertasArchivo = (DefaultListModel<Oferta>) ois.readObject();
			ois.close();
			for (int i = 0; i < ofertasArchivo.getSize(); i++) {
				vista.dlmOfertas.addElement(ofertasArchivo.get(i));
			}
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,
					"No se ha encontrado el archivo de datos, se generar� uno nuevo al cerrar la aplicaci�n", "Error",
					JOptionPane.ERROR_MESSAGE);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Se ejecuta al cerrarse la ventana principal del programa, guarda todos los
	 * datos del programa en un archivo "datos.bin"
	 */
	@Override
	public void windowClosing(WindowEvent arg0) {
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(new FileOutputStream("datos.bin"));
			oos.writeObject(vista.dlmOfertas);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// M�todos no utilizados
	@Override
	public void windowActivated(WindowEvent arg0) {
	}

	@Override
	public void windowClosed(WindowEvent arg0) {
	}

	@Override
	public void windowDeactivated(WindowEvent arg0) {
	}

	@Override
	public void windowDeiconified(WindowEvent arg0) {
	}

	@Override
	public void windowIconified(WindowEvent arg0) {
	}
}

package com.apalacios.seccion31.app;

import java.awt.Dimension;
import java.io.IOException;

/**
 * Clase principal del programa, se encarga de lanzar la iterfaz gr�fica y
 * cargar las preferencias
 * 
 * @author Adri�n Palacios
 */
public class Main {

	/**
	 * M�todo principal del programa, crea un objeto de constantes (se encarga de
	 * cargar las preferencias), lanza la interfaz gr�fica y le establece las
	 * dimensiones adecuadas
	 * 
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			Constantes constantes = new Constantes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Vista vista = new Vista();
		Controlador controlador = new Controlador(vista);
		vista.setPreferredSize(new Dimension(1200, 750));
		vista.setTitle("Secci�n 31 - Bar Gaming");
		vista.setLocationRelativeTo(null);
		vista.setResizable(false);
		vista.setDefaultCloseOperation(3);
		vista.pack();
		vista.setVisible(true);
	}
}

package base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 * Objeto que representa a una oferta dentro del programa
 * 
 * @author Adri�n Palacios
 */
@SuppressWarnings("serial")
public class Oferta implements Serializable {
	private String imagen;
	private String titulo;
	private String descripcion;
	private ArrayList<Producto> productos;

	/**
	 * Constructor de la clase, inicializa sus par�metros y establece los productos
	 * afectados por la oferta
	 * 
	 * @param imagen      Representa la direcci�n del archivo de imagen deseado para
	 *                    la oferta
	 * @param titulo      Representa el t�tulo o nombre de la oferta
	 * @param descripcion Representa la descripci�n o propiedades de la oferta
	 */
	public Oferta(String imagen, String titulo, String descripcion) {
		this.imagen = imagen;
		this.titulo = titulo;
		this.descripcion = descripcion;
		productos = new ArrayList<>();
		obtenerProductos();
	}

	/**
	 * Devuelve los productos que se ven afectados por la oferta (necesario para la
	 * generaci�n de informes)
	 * 
	 * @return JRDataSource que contiene un listado de productos
	 */
	public JRDataSource getProductosDS() {
		return new JRBeanCollectionDataSource(productos);
	}

	/**
	 * A�ade una cantidad aleatoria de productos aleatorios a la oferta, simulando
	 * que estos son contenidos por la misma
	 */
	private void obtenerProductos() {
		ArrayList<Integer> numGenerados = new ArrayList<>();
		int nProductos = 0;

		Random random = new Random();
		nProductos = random.nextInt(5) + 1;
		while (numGenerados.size() != nProductos) {
			int numeroActual = random.nextInt(5) + 1;
			if (!numGenerados.contains(numeroActual)) {
				numGenerados.add(numeroActual);
				switch (numeroActual) {
				case 1:
					productos.add(new Producto("Atardecer Samariano", 8));
					System.out.println("Producto 1");
					break;
				case 2:
					productos.add(new Producto("Brandy Sauriano", 6));
					System.out.println("Producto 2");
					break;
				case 3:
					productos.add(new Producto("Cerveza Romulana", 2.5f));
					System.out.println("Producto 3");
					break;
				case 4:
					productos.add(new Producto("Kanar", 2));
					System.out.println("Producto 4");
					break;
				case 5:
					productos.add(new Producto("T� Plomeek", 3));
					System.out.println("Producto 5");
					break;
				}
			}
		}
	}

	/**
	 * Devuelve la ruta del archivo de imagen para la oferta
	 * 
	 * @return String que representa la direcci�n del archivo de imagen de la oferta
	 */
	public String getImagen() {
		return imagen;
	}

	/**
	 * Devuelve el t�tulo de la oferta
	 * 
	 * @return String que representa el t�tulo de la oferta
	 */
	public String getTitulo() {
		return titulo;
	}

	/**
	 * Establece la ruta del archivo de imagen de la oferta al especificado
	 * 
	 * @param imagen Representa la ruta del archivo de imagen deseado para la oferta
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	/**
	 * Establece el titulo de la oferta al especificado
	 * 
	 * @param titulo Representa el titulo deseado para la oferta
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	 * Devuelve la descripci�n de la oferta
	 * 
	 * @return String que representa la descripci�n de la oferta
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Establece la descripci�n de la oferta a la especificada
	 * 
	 * @param descripcion Representa la descripci�n deseada para la oferta
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}

package base;

import java.io.Serializable;

/**
 * Clase que representa a un producto dentro del programa
 * 
 * @author Adri�n Palacios
 */
@SuppressWarnings("serial")
public class Producto implements Serializable {
	private String nombre;
	private String imagen;
	private float precio;
	private int cantidad;

	/**
	 * Constructor de la clase, inicializa sus par�metros
	 * 
	 * @param nombre   Representa el nombre del producto
	 * @param imagen   Representa la ruta del archivo de imagen del producto
	 * @param precio   Representa el precio del producto
	 * @param cantidad Representa la cantidad de unidades a�adidas del producto
	 */
	public Producto(String nombre, String imagen, float precio, int cantidad) {
		this.nombre = nombre;
		this.imagen = imagen;
		this.precio = precio;
		this.cantidad = cantidad;
	}

	/**
	 * Constructor alternativo de la clase (usado para la generaci�n de informes)
	 * Inicializa todos los par�metros del objeto, pero solo recibe como par�metros
	 * el nombre y el precio
	 * 
	 * @param nombre Representa el nombre del producto
	 * @param precio Representa el precio del producto
	 */
	public Producto(String nombre, float precio) {
		this.nombre = nombre;
		this.imagen = "";
		this.precio = precio;
		this.cantidad = 0;
	}

	/**
	 * Devuelve el nombre del producto
	 * 
	 * @return String que representa el nombre del producto
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Devuelve la ruta del archivo de imagen del producto
	 * 
	 * @return String que representa la ruta del archivo de imagen del producto
	 */
	public String getImagen() {
		return imagen;
	}

	/**
	 * Devuelve el precio del producto
	 * 
	 * @return float que representa el precio del producto
	 */
	public float getPrecio() {
		return precio;
	}

	/**
	 * Devuelve la cantidad de unidades a�adidas del producto
	 * 
	 * @return int que representa la cantidad de unidades a�adidas del producto
	 */
	public int getCantidad() {
		return cantidad;
	}

	/**
	 * Establece el nombre del producto al recibido como par�metro
	 * 
	 * @param nombre Representa el nombre deseado para el producto
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Establece la ruta del archivo de imagen para el producto
	 * 
	 * @param imagen Representa la ruta del archivo de imagen deseada para el
	 *               producto
	 */
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	/**
	 * Establece el precio del producto al recibido como par�metro
	 * 
	 * @param precio Representa el precio deseado para el producto
	 */
	public void setPrecio(float precio) {
		this.precio = precio;
	}

	/**
	 * Establece la cantidad de unidades a�adidas del producto a la recibida como
	 * par�metro
	 * 
	 * @param cantidad Representa la cantidad de unidades a�adidas del producto
	 */
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	/**
	 * A�ade una unidad del producto (aumenta en 1 el par�metro de "cantidad")
	 */
	public void agregarUnidad() {
		this.cantidad += 1;
	}

	/**
	 * Elimina una unidad del producto (reduce en 1 el par�metro de "cantidad")
	 */
	public void eliminarUnidad() {
		this.cantidad -= 1;
	}
}
